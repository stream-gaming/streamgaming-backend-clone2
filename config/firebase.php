<?php

return [
    'service_account'   => env('FIREBASE_CONFIG','firebase/config.json'),
    'database_url'      => env('FIREBASE_ACCOUNT','https://laravel-firebase-a1043-default-rtdb.firebaseio.com/'),
];
