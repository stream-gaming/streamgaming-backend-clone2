#!/bin/sh
# activate maintenance mode
#echo $HOME;
#export COMPOSER_HOME="$HOME/.config/composer";
php artisan down
# update source code
ssh-agent bash -c 'ssh-add /home/ubuntu/public_html/.ssh/stream-gaming-backend-development-manual; git pull origin development'
# update PHP dependencies
composer install --no-interaction --prefer-dist
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.
# update database
php artisan migrate --force
# --force  Required to run when in production.
# stop maintenance mode
php artisan up
php artisan scribe:generate
php artisan optimize
