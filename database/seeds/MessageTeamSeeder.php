<?php

use Illuminate\Database\Seeder;

class MessageTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\MessageTeamModel::class,20)->create();
    }
}
