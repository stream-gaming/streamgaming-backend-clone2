<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

    	for($i = 0; $i < 11; $i++) {
    		$d = new stdClass;
            $d->user_id = uniqid();
    		$data = [
    			'user_id'	=> $d->user_id,
    			'fullname' 	=> $faker->name,
    			'username' 	=> $faker->userName,
    			'password'	=> password_hash('admin', PASSWORD_DEFAULT), 
    			'email' 	=> $faker->email,
                'is_verified' => 1,
                'status' 	=> 'player' 
            ];
            $data2 = [
            	'user_id'	=> $d->user_id,
            	'id_card'	=> '',
            	'id_card_number'	=> '',
            	'image'		=> '',
            	'link_image'	=> '',
            	'id_status'	=> 0
            ];	 
    		App\Model\User::create($data);
    		App\Model\UsersDetailModel::create($data2); 
            App\Model\BalanceUsersModel::addBalanceUsers($d); 
            App\Model\AuthenticateModel::addAuthenticate($d);
            App\Model\UsersGameModel::addUsersGame($d);

    	}
    } 
}

