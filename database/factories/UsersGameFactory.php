<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\UsersGameModel;
use Faker\Generator as Faker;

$factory->define(UsersGameModel::class, function (Faker $faker) {
    return [
        'users_id'  => uniqid(),
        'game_id'   => uniqid()
    ];
});
