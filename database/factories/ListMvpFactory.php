<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\ListMvpModel;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(ListMvpModel::class, function (Faker $faker) {
    return [
        'tournament_id' => factory(TournamentModel::class),
        'name_player'   => $faker->userName(),
        'user_id'       => factory(User::class)->create(),
        'team_name'     => $faker->userName(),
        // 'team_id'       => factory(TeamModel::class)->create(),
        'team_id'       => uniqid(),
        'kda_score'     => $faker->randomFloat(null,0,95),
        'strike_mvp'    => random_int(0,4),
        'mvp_type'      => $faker->randomElement(['strike_mvp','kda_score']),
        'sequence'      => rand(1,5)
    ];
});
