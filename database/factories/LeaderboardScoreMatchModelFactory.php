<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(LeaderboardScoreMatchModel::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id])->team_id;
    return [
        'id_score_match' => uniqid(),
        'id_leaderboard' => factory(LeaderboardModel::class)->create()->id_leaderboard,
        'qualification_sequence' => '0',
        'group_sequence' => '0',
        'match_sequence' => '0',
        'id_team'     => $team,
        'id_player'   => $user->user_id,
        'id_urutan_team'     => rand(1, 20),
        'jumlah_kill' => '2',
        'score' => '2',
        'is_win' => '1',
        'tgl_input'  => $faker->dateTime()
    ];
});
