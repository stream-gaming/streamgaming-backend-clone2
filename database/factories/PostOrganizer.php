<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\OrganizerModel;
use Faker\Generator as Faker;

$factory->define(OrganizerModel::class, function (Faker $faker) {
    return [
        'id'                => uniqid(),    
        'first_name'        => $faker->firstName,
        'last_name'         => $faker->lastName,
        'email'             => $faker->lastName,
        'company'           => $faker->company,
        'industry'          => $faker->streetName,
        'position'          => $faker->cityPrefix,
        'city'              => $faker->city,
        'phone_number'      => $faker->phoneNumber,
        'manager_tour'      => rand(1,5),
        'status'            => "pending"
    ];
});
