<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Model\GameModel;
use App\Model\TicketModel;
use Faker\Generator as Faker;

$factory->define(TicketModel::class, function (Faker $faker) {
    $maxGiven = $faker->numberBetween(1, 100000);
    return [
        'id_ticket' => uniqid(),
        'names' => 'Ticket Lama',
        'functions' => '3',
        'unit' => '1',
        'limits' => $maxGiven,
        'gived_limit' => $faker->numberBetween(1, $maxGiven),
        'gived_ticket' => $faker->numberBetween(1, $maxGiven),
        'expired' => date('Y-m-d H:i:s', strtotime("+7 day")),
        'create_date' => date('Y-m-d H:i:s', strtotime("+7 day")),
        'ticket_end' => $faker->dateTime(),
        'kompetisi' => factory(GameModel::class)->make()->id_game_list,
    ];
});
