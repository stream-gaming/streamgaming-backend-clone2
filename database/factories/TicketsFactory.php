<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\TeamModel;
use App\Model\TicketsModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TicketsModel::class, function (Faker $faker) {
    return [
        'user_id'  => factory(User::class)->create()->user_id,
        'team_id'   => factory(TeamModel::class)->create()->team_id,
        'ticket_id' => factory(AllTicketModel::class)->create()->id,
        'is_used'   => rand(0, 1)
    ];
});
