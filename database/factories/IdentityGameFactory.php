<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\User;
use App\Model\GameModel;
use App\Model\UsersGameModel;
use Faker\Generator as Faker;
use App\Model\IdentityGameModel;

$factory->define(IdentityGameModel::class, function (Faker $faker) {
    return [
        'id'    => uniqid(),
        'users_id'  => factory(User::class)->create(),
        'game_id'   => factory(GameModel::class)->create(['player_on_team' => 5, 'substitute_player' => 2]),
        'id_ingame' => $faker->uuid,
        'username_ingame'   => $faker->userName
    ];
});

// $factory->afterCreating(IdentityGameModel::class, function ($user_game, $faker) {
//     factory(UsersGameModel::class)->create(['users_id' => $user_game->users_id, 'game_id' => $user_game->game_id]);
// });
