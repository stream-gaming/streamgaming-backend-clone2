<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\LeaderboardModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(PlayerPayment::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    return [
        //
        'id_player_payment' => uniqid(),
        'id_tournament' => factory(LeaderboardModel::class)->create()->id_leaderboard,
        'id_team' => factory(TeamModel::class)->create(['leader_id' => $user->user_id])->team_id,
        'player_id' => $user->user_id,
        'price_of_payment' => 'Free',
        'amount' => '-',
        'type_payment' => '-',
        'ticket_id' => 0,
        'status' => 'Lunas',
        'date_payment' => $faker->dateTime('now'),
    ];
});
