<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TournamentGroupPlayerModel::class, function (Faker $faker) {
    $tournamentGroup = TournamenGroupModel::pluck('id_team','id_group')->all();
    return [
        'id_player' => factory(User::class)->create()->user_id,
        'id_group'  => $faker->randomElement(array_keys($tournamentGroup)),
        'id_team'   => $faker->randomElement($tournamentGroup),
        'username_ingame' => $faker->userName,
        'id_ingame' => uniqid(),
        'player_status' => rand(1,10)
    ];
});
