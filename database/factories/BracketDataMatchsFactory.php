<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\BracketDataMatchs;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use Faker\Generator as Faker;

$factory->define(BracketDataMatchs::class, function (Faker $faker) {
    $tournament = TournamentModel::first();
    return [
        'id_data_matchs' => uniqid(),
        'id_data_kill' => uniqid(),
        'id_tournament' => $tournament->id_tournament,
        'id_round' => uniqid(),
        'team1' => uniqid(),
        'team2' => uniqid(),
        'matchs' => rand(1,5)
    ];
});
