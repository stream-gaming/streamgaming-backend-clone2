<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'user_id'   => uniqid(),
        'fullname'  => $faker->name,
        'username'  => $faker->userName,
        'email'     => $faker->unique()->safeEmail,
        'password'  => bcrypt('admin'), // password
        'is_verified'   => 1,
        'status'        => 'player'
    ];
});
