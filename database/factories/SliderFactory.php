<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\SliderModel;
use Faker\Generator as Faker;

$factory->define(SliderModel::class, function (Faker $faker) {
    return [
        'title'     => $faker->name,
        'image'     => 'https://www.streamgaming.id/img/402a52c6bf640816ddbd6735a5b3b97d.c39b33c2.png',
        'status'    => rand(1,2),
        'url'       => 'https://www.streamgaming.id',
        'in_app'    => 0
    ];
});
