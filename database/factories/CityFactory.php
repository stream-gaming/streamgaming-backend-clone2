<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\CityModel;
use App\Model\ProvinceModel;
use Faker\Generator as Faker;

$factory->define(CityModel::class, function (Faker $faker) {
    return [
        'id_kab'    => function() {
            $max = (int) CityModel::max('id_kab');
            return ($max+1);
        },
        'id_prov'   => factory(ProvinceModel::class)->create()->id_prov,
        'nama'      => $faker->city,
        'id_jenis'  => ''
    ];
});
