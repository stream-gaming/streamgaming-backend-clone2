<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AllTicketModel;
use App\Model\TeamModel;
use App\Model\TIcketPurchaseHistoryModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TIcketPurchaseHistoryModel::class, function (Faker $faker) {
    return [
        'order_id'  => random_int(100000,999999),
        'xendit_invoice_id' => random_int(100000,999999),
        'user_id'   => factory(User::class)->create()->user_id,
        'team_id' => factory(TeamModel::class)->create()->team_id,
        'ticket_id' => factory(AllTicketModel::class)->create()->id,
        'quantity'  => random_int(1,10),
        'amount'    => random_int(1000,100000),
        'provider'  => $faker->title(),
        'payment_method' => $faker->title(),
        'status'    => $faker->boolean(),
        'expired_at' => $faker->date(),
        'created_at' => $faker->date(),
        'updated_at' => $faker->date(),
    ];
});
