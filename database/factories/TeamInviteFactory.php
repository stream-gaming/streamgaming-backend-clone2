<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use Faker\Generator as Faker;
use App\Model\TeamInviteModel;
use App\Model\IdentityGameModel;

$factory->define(TeamInviteModel::class, function (Faker $faker) {
    return [
        'invite_id' => uniqid(),
        'team_id'   => uniqid(),
        'identity_id'   => uniqid(),
        'users_id'      => factory(IdentityGameModel::class)->create(['game_id' => GameModel::first()->id_game_list])->users_id,
        'status'        => 1
    ];
});
