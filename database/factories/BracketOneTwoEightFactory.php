<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\BracketOneTwoEight;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use Faker\Generator as Faker;

$factory->define(BracketOneTwoEight::class, function (Faker $faker) {
    $team1 = factory(TeamModel::class)->create()->team_id;
    $team2 = factory(TeamModel::class)->create()->team_id;
    return [
        'id_round128'   => uniqid(),
        'id_tournament' => factory(TournamentModel::class)->create()->id_tournament,
        'team1'     => $team1,
        'team2'     => $team2,
        'score1' => '1',
        'score2' => '0',
        'score_match1' => '1',
        'score_match2' => '0',
        'winner'   => $team1,
        'lose'      => $team2,
        'matchs'         => rand(1, 2),
        'id_data_kill'   => rand(1, 5),
        'date'      => $faker->dateTime()
    ];
});
