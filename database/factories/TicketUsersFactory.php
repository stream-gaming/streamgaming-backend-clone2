<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\TicketModel;
use App\Model\TicketUserModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TicketUserModel::class, function (Faker $faker) {
    return [
        'id_users'  => factory(User::class)->create()->user_id,
        'id_ticket' => factory(TicketModel::class)->create()->id_ticket,
        'ticket'    => rand(1, 1000)
    ];
});
