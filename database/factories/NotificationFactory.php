<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\UserNotificationModel;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(UserNotificationModel::class, function (Faker $faker) {
    return [
        'id_notification'   => uniqid(),
        'id_user'           => uniqid(),
        'keterangan'        => $faker->jobTitle,
        'link'              => 'testing',
        'is_read'           => 0,
        'deleted_at'        => now()
    ];
});
