<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\TeamModel;
use App\Model\TeamNameHistoryModel;
use Faker\Generator as Faker;

$factory->define(TeamNameHistoryModel::class, function (Faker $faker) {
    return [
        'team_id' => factory(TeamModel::class),
        'old_name' => $faker->userName(),
        'new_name' => $faker->userName(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
