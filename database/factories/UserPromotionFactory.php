<?php

use App\Model\PromotionModel;
use App\Model\UserPromotionModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(UserPromotionModel::class, function (Faker $faker) {
    return [
        'promotion_id' => factory(PromotionModel::class)->create()->id,
        'user_id' => factory(User::class)->create(['is_active' => 1])->user_id,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
