<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Model\AllTicketModel;
use App\Model\GameModel;
use Faker\Generator as Faker;

$factory->define(AllTicketModel::class, function (Faker $faker) {
    $maxGiven = $faker->numberBetween(1, 100000);
    return [
        //
        'name' => 'Ticket Premium',
        'type' => '2',
        'sub_type' => '3',
        'unit' => '1',
        'max_given' => $maxGiven,
        'total_given' => $faker->numberBetween(1, $maxGiven),
        'expired' => date('Y-m-d H:i:s', strtotime("+7 day")),
        'event_end' => $faker->dateTime(),
        'competition' => factory(GameModel::class)->make()->id_game_list,
        'can_traded' => '1',
        'market_value' => $faker->numberBetween(100, 1000000),
        'giving_target'=> $faker->numberBetween(1, 3),
        'mode' => $faker->numberBetween(1, 3),
        'max_has' => $faker->numberBetween(1, 3)
    ];
});
