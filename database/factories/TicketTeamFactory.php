<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\TeamModel;
use App\Model\TicketModel;
use App\Model\TicketTeamModel;
use Faker\Generator as Faker;

$factory->define(TicketTeamModel::class, function (Faker $faker) {
    return [
        'id_team'   => factory(TeamModel::class)->create()->team_id,
        'id_ticket' => factory(TicketModel::class)->create()->id_ticket,
        'ticket'    => rand(1, 1000)
    ];
});
