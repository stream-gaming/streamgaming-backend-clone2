<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Management;
use Faker\Generator as Faker;

$factory->define(Management::class, function (Faker $faker) {
    return [
        'stream_id' => rand(),
        'fullname'  => $faker->name('male'),
        'username'  => $faker->userName,
        'email'     => $faker->email,
        'password'  => bcrypt('admin')
    ];
});
