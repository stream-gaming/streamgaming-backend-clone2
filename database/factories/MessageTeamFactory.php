<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(MessageTeamModel::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'team_id' => factory(TeamModel::class),
        'message' => $faker->text(),
        'created_at' => $faker->dateTime()
    ];
});
