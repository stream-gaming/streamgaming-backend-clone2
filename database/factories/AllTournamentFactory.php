<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AllTournamentModel;
use App\Model\LeaderboardModel;
use Faker\Generator as Faker;

$factory->define(AllTournamentModel::class, function (Faker $faker) {
    return [
        'id_all_tournament' => uniqid('faker_'),
        'id_tournament'     => factory(LeaderboardModel::class),
        'format'            => 'Leaderboard'
    ];
});
