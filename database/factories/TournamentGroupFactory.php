<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\CityModel;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use Faker\Generator as Faker;

$factory->define(TournamenGroupModel::class, function (Faker $faker) {
    return [
        'id_team'   => factory(TeamModel::class)->create(['venue' => ''])->team_id,
        'id_group'  => factory(TournamentModel::class)->create()->id_group,
        'nama_team' => $faker->name(),
        'logo_team' => $faker->url,
        'nama_kota' => $faker->city,
        'matchs'    => rand(1,5),
        'win'       => uniqid(),
        'date'      => $faker->dateTime()
    ];
});
