<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use App\Model\TournamentLiga;
use Faker\Generator as Faker;

$factory->define(TournamentLiga::class, function (Faker $faker) {
    return [
        'nama_liga'             => $faker->title(),
        'nama_penyelenggara'    => $faker->company(),
        'logo_penyelenggara'    => $faker->imageUrl(),
        'start_tour'            => $faker->date('Y-m-d'),
        'end_tour'              => $faker->date('Y-m-d'),
        'slot_tersedia'         => $faker->randomNumber(),
        'jumlah_hadiah'         => $faker->randomNumber(),
        'kompetisi'             => factory(GameModel::class)->create(),
        'id_tab_liga'           => rand(1, 100),
        'deskripsi'             => $faker->paragraph(),
        'jumlah'                => $faker->randomDigit(),
        'logo_liga'             => $faker->imageUrl(),
        'background_liga'       => $faker->imageUrl(),
        'banner_liga'           => $faker->imageUrl(),
        'on_view'               => 1,
        'authord'               => $faker->name(),
        'url'                   => $faker->url(),
        'created_at'            => $faker->dateTime()
    ];
});
