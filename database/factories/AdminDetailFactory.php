<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\ManagementDetailModel;
use Faker\Generator as Faker;

$factory->define(ManagementDetailModel::class, function (Faker $faker) {
    return [
        'access_id' => '1'
    ];
});
