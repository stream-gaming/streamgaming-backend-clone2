<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TeamModel::class, function (Faker $faker) {
    return [
        'team_id'   => uniqid(),
        'game_id'   => factory(GameModel::class)->create(['player_on_team' => 5, 'substitute_player' => 2])->id_game_list,
        'leader_id'  => factory(User::class)->create()->user_id,
        'team_name' => $faker->name,
        'team_description'  => $faker->text,
        'team_logo' => $faker->name,
        'venue'     => factory(CityModel::class)->create()->nama
    ];
});

// $factory->afterCreating(TeamModel::class, function ($team, $faker) {
//     factory(TeamPlayerModel::class)->create(['team_id' => $team->team_id, 'player_id' => $team->leader_id, 'substitute_id'=>null]);
// });
