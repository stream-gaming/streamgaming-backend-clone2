<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\TournamentStreamingModel;
use Faker\Generator as Faker;

$factory->define(TournamentStreamingModel::class, function (Faker $faker) {
    return [
        //
        "id_tournament" => uniqid(),
        "title" => $faker->text(20),
        "url" => $faker->url,
        "sequence" => "1",
    ];
});
