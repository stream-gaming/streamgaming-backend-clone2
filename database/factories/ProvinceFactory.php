<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\ProvinceModel;
use Faker\Generator as Faker;

$factory->define(ProvinceModel::class, function (Faker $faker) {
    return [
        'id_prov'   => function() {
            $max = ProvinceModel::max('id_prov');
            return $max+1;
        },
        'nama'      => $faker->state
    ];
});
