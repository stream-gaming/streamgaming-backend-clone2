<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(TeamRequestModel::class, function (Faker $faker) {
    return [
        'request_id'    => uniqid(),
        'team_id'       => uniqid(),
        'users_id'      => factory(IdentityGameModel::class)->create(['game_id' => GameModel::first()->id_game_list])->users_id,
        'status'        => 1
    ];
});
