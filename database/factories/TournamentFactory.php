<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\GameModel;
use App\Model\TournamentModel;
use Faker\Generator as Faker;

$factory->define(TournamentModel::class, function (Faker $faker) {
    return [
        'id_tournament'     => uniqid(),
        'tournament_name'   => $faker->name,
        'date'              => $faker->dateTime(),
        'date_close'        => $faker->dateTime(),
        'created_at'        => $faker->dateTime(),
        'kompetisi'         => factory(GameModel::class)->create(['player_on_team' => 5, 'substitute_player' => 2]),
        'system_tournament' => $faker->randomElement($array = array (4,5)),
        'tourname_type'     => 'single',
        'game_option'       => $faker->randomElement($array = array (1,3)),
        'id_group'          => uniqid(),
        'total_team'        => $faker->randomElement($array = array (8,16,32)),
        'filled_slot'       => 0,
        'status'            => "end_registration",
        'start_registration'=> $faker->dateTime(),
        'end_registration'  => $faker->dateTime(),
        'competition_format'=> "Online",
        'is_custom_prize'   => 0,
        'prize_mvp'         => 1,
        'system_payment'    => "Free",
        'url'               => uniqid(),
        'is_simultaneously' => 0,

    ];
});
