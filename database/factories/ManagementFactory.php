<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Management;
use App\Model\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Management::class, function (Faker $faker) {
    return [
        'stream_id' => uniqid(),
        'fullname'  => $faker->name,
        'username'  => $faker->userName,
        'email'     => $faker->unique()->safeEmail,
        'password'  => bcrypt('admin'), // password
        'role_access'   => rand(1, 2),
        'created_at' => $faker->dateTime()
    ];
});
