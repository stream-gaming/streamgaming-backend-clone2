<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use Faker\Generator as Faker;

$factory->define(GameModel::class, function (Faker $faker) {
    return [
        'id_game_list'  => rand(),
        'name'  => $faker->userName,
        'title' => $faker->name('male'),
        'content'   => $faker->text()
    ];
});
