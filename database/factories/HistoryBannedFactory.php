<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\HistoryBannedModel;
use App\Model\IdentityGameModel;
use App\Model\Management;
use App\Model\TeamModel;
use Faker\Generator as Faker;

$factory->define(HistoryBannedModel::class, function (Faker $faker) {
    return [
        'identity_id' => factory(IdentityGameModel::class),
        'team_id'     => factory(TeamModel::class),
        'ban_until'   => $faker->dateTime,
        'reason_ban'  => $faker->text(),
        'admin_id'    => factory(Management::class),
        'created_at' => $faker->dateTime()
    ];
});
