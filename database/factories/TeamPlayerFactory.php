<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\TeamPlayerModel;
use Faker\Generator as Faker;

$factory->define(TeamPlayerModel::class, function (Faker $faker) {
    return [
        'team_id'   => uniqid(),
        'player_id' => uniqid(),
        'substitute_id' => uniqid(),
    ];
});
