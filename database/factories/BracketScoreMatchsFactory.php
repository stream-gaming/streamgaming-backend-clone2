<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\BracketDataMatchs;
use App\Model\ScoreMatchsModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use Faker\Generator as Faker;

$factory->define(ScoreMatchsModel::class, function (Faker $faker) {
    $datamatchs = BracketDataMatchs::pluck('id_data_matchs')->toArray();
    $groupplayer = TournamentGroupPlayerModel::pluck('id_team','id_player')->toArray();

    $id_player = $faker->randomElement(array_keys($groupplayer));
    $id_team = $groupplayer[$id_player];
    return [
        'id_score_matchs' => uniqid(),
        'id_data_matchs' => $faker->randomElement($datamatchs),
        'id_team'        => $id_team,
        'kompetisi'      => uniqid(),
        'player_id'      => $id_player,
        'hero'           => uniqid(),
        'kills'          => rand(0,20),
        'death'          => rand(0,20),
        'assist'         => rand(0,20),
    ];
});
