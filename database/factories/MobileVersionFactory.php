<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\MobileVersion;
use Faker\Generator as Faker;

$factory->define(MobileVersion::class, function (Faker $faker) {
    return [
        'appId' => rand(1, 2),
        'versionName' => $faker->name(),
        'versionCode' => rand(1,100),
        'recommendUpdate' => rand(0,1),
        'forceUpdate' => rand(0,1),
        'changeLogs' => $faker->paragraph(),
        'url' => null,
    ];
});
