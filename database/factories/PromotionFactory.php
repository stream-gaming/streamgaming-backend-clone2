<?php

use App\Model\Management;
use App\Model\PromotionModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(PromotionModel::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'content' => $faker->text(),
        'link' => null,
        'status' => $faker->numberBetween(1, 2),
        'inputed_by' => factory(Management::class)->create()->stream_id,
        'created_at' => Carbon::now()
    ];
});
