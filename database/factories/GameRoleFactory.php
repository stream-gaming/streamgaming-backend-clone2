<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use App\Model\GameRoleModel;
use Faker\Generator as Faker;

$factory->define(GameRoleModel::class, function (Faker $faker) {
    return [
        'game_id'  => '',
        'role_name'=> $faker->name('male'),
    ];
});
