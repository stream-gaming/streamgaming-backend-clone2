<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\TeamPlayerNewModel;
use Faker\Generator as Faker;

$factory->define(TeamPlayerNewModel::class, function (Faker $faker) {
    return [
        'team_id' => uniqid(),
        'player_id' => uniqid(),
        'player_status' => 1,
    ];
});
