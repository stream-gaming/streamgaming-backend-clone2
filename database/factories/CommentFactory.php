<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\CommentsModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(CommentsModel::class, function (Faker $faker) {
    return [
        //
        'user_status' => 'user',
        'user_id' => factory(User::class)->create()->user_id,
        'comment' => $faker->text(200),
        'commentable_id' => uniqid(),
        'commentable_type' => 'tournament'
    ];
});
