<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\GameModel;
use App\Model\LeaderboardModel;
use Faker\Generator as Faker;

$factory->define(LeaderboardModel::class, function (Faker $faker) {
    return [
        //
        'id_leaderboard' => uniqid(),
        'tournament_category' => 2,
        'is_no_qualification' => 1,
        'tournament_name' => $faker->userName,
        'tournament_mode' => $faker->text(),
        'city' => $faker->text(),
        'tournament_form' => 'free',
        'ticket' => '',
        'ticket_unit' => '',
        'is_new_ticket_feature' => '',
        'tournament_fee' => 0,
        'organizer_name' => $faker->text(),
        'tournament_date' => '2022-04-25 08:37:17',
        'tournament_end_date' => '2022-04-25 08:37:17',
        'regist_start' => $faker->dateTime('now'),
        'regist_end' => '2022-04-25 08:37:17',
        'prizepool_type' => 'Manual',
        'prize_type' => 'default',
        'isTicketPrize' => 0,
        'is_ticket_prize_new' => 0,
        'is_custom_prize' => 0,
        'url' => $faker->text(),
        'card_banner' => $faker->text(),
        'game_competition' => factory(GameModel::class)->create(['system_kompetisi' => 'LeaderBoards'])->id_game_list,
        'game_option' => '1',
        'status_tournament' => '1',
        'filled_slot' => '100',
        'total_team' => '120',
        'input_date' => $faker->dateTime('now')

    ];
});
