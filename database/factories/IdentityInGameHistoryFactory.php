<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\GameModel;
use App\Model\IdentityIngameHistoryModel;
use App\Model\User;
use Faker\Generator as Faker;

$factory->define(IdentityIngameHistoryModel::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'game_id' => factory(GameModel::class),
        'before'  => json_encode([
            'id_ingame' => $faker->userName(),
            'username_ingame' => $faker->userName()
        ]),
        'after'   => json_encode([
            'id_ingame' => $faker->userName(),
            'username_ingame' => $faker->userName()
        ]),
        'created_at' => $faker->date()
    ];
});
