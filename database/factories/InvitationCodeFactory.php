<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\InvitationCode;
use App\Model\TeamModel;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(InvitationCode::class, function (Faker $faker) {
    return [
        'team_id'   => factory(TeamModel::class),
        'code'      => strtoupper(Str::random(7)),
        'expired_at' => $faker->dateTimeBetween('now', Carbon::now()->addHour(6)->toDateTimeString())
    ];
});
