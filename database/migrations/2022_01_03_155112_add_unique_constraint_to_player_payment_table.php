<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddUniqueConstraintToPlayerPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('player_payment')) {
            DB::statement('ALTER TABLE player_payment ADD UNIQUE `unique_combination`(`id_tournament`,`id_team`,`player_id`,`unique_flag`)');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('player_payment')) {
            DB::statement('DROP INDEX unique_combination ON player_payment');
        }
    }
}
