<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeCollationColumnIdIngame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('identity_ingame', function (Blueprint $table) {
            if (Schema::hasColumn('identity_ingame', 'id_ingame')) {
                DB::statement('ALTER TABLE identity_ingame MODIFY id_ingame VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('identity_ingame', function (Blueprint $table) {
            if (Schema::hasColumn('identity_ingame', 'id_ingame')) {
                DB::statement('ALTER TABLE identity_ingame MODIFY id_ingame VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci;');
            }
        });
    }
}





