<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_promotion', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->id();
            $table->unsignedBigInteger('promotion_id');
            $table->foreign('promotion_id')->references('id')->on('promotion');
            $table->char('user_id', 128);
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('action_read')->default(0)->comment('0=not read,1=read');
            $table->integer('action_delete')->default(0)->comment('0=not delete,1=delete');
            $table->timestamps();
        });

        if(Schema::hasTable('user_promotion')){
            DB::statement('ALTER TABLE user_promotion ADD UNIQUE `unique_user_promotion_combination`(`promotion_id`,`user_id`)');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_promotion');
    }
}
