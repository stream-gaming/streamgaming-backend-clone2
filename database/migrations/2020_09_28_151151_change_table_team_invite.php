<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableTeamInvite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('team_invite', 'deleted_at')) {
            Schema::table('team_invite', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();  
            });
        } else {
            Schema::table('team_invite', function (Blueprint $table) {
                $table->string('deleted_at')->nullable(); 
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_invite', function (Blueprint $table) {
            // $table->string('deleted_at')->nullable(false)->change();  
        });
    }
}
