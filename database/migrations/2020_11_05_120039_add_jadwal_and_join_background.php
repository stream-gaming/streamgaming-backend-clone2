<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJadwalAndJoinBackground extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->text('stream_url')->after('peraturan_umum');
            $table->text('jadwal')->after('stream_url');
            $table->text('join_background')->after('jadwal');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->dropColumn(['jadwal', 'join_background','stream_url']);
        });
    }
}
