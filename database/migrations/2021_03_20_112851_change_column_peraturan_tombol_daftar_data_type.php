<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnPeraturanTombolDaftarDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('leaderboard')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->longText('button_rules')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasColumn('leaderboard', 'button_rules')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->dropColumn('button_rules');
            });
        }
    }
}
