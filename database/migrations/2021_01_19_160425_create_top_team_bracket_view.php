<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTopTeamBracketView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS top_team_bracket_view');
        DB::statement("CREATE VIEW top_team_bracket_view AS
        SELECT team.game_id, team.team_id, team.team_name, team.team_logo,
            SUM( tournament_group.win ) AS total_win,
            SUM( tournament_group.matchs ) AS total_match,
            IFNULL(tsm.total_kill, 0) as total_kill,
            ( CAST( SUM( tournament_group.win ) AS UNSIGNED ) / CAST( SUM( tournament_group.matchs ) AS UNSIGNED ) * 100 ) AS win_rate
        FROM
            team
            LEFT JOIN tournament_group ON tournament_group.id_team = team.team_id
            LEFT JOIN ( SELECT sum( tsm.kills ) AS total_kill, id_team FROM tournament_score_matchs tsm GROUP BY id_team ) tsm ON tsm.id_team = team.team_id
        WHERE
            team.deleted_at IS NULL
        GROUP BY
            tournament_group.id_team
        ORDER BY
            total_win DESC");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_team_bracket_view');
    }
}
