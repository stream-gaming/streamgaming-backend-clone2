<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdWinnerSquuenceANDIsSimultaneously extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament', 'is_simultaneously') AND !Schema::hasColumn('tournament', 'id_winner_sequence') ) {
        Schema::table('tournament', function (Blueprint $table) {
            $table->string('id_winner_sequence')->after('id_liga');
            $table->integer('is_simultaneously');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament', function (Blueprint $table) {
            $table->dropColumn(['id_winner_sequence','is_simultaneously']);
        });
    }
}
