<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DeleteViewTopTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('DROP VIEW IF EXISTS top_team_leaderboards_view');
        DB::statement('DROP VIEW IF EXISTS top_team_bracket_view');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('DROP VIEW IF EXISTS top_team_leaderboards_view');
        DB::statement('DROP VIEW IF EXISTS top_team_bracket_view');
    }
}
