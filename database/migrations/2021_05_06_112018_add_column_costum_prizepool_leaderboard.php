<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCostumPrizepoolLeaderboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasColumn('leaderboard', 'is_costum_prize')) {
            Schema::table('leaderboard', function(Blueprint $table){
                $table->integer('is_custom_prize')->after('is_ticket_prize_new');
            });

        };

        if(!Schema::hasColumn('tournament', 'is_custom_prize')) {
            Schema::table('tournament', function(Blueprint $table){
                $table->integer('is_custom_prize')->after('is_ticket_prize_new');
            });

        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
