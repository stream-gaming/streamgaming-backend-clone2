<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxHasTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('all_ticket', 'max_has')) {
            Schema::table('all_ticket', function (Blueprint $table) {
                $table->integer('max_has')->default('1')->after('mode');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('all_ticket', function (Blueprint $table) {
            $table->dropColumn('max_has');

        });
    }
}
