<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTopTeamLeaderboardsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS top_team_leaderboards_view');
        DB::statement("CREATE VIEW top_team_leaderboards_view AS
        SELECT team.game_id, team_id, team_name, team_logo,
        IFNULL( SUM( ( CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 END ) ), 0 ) AS total_win,
        SUM( leaderboard_score_team.total_kill ) AS total_kill,
        COUNT( leaderboard_score_team.id_team ) AS total_match,
        ( CAST( SUM( ( CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 END ) ) AS UNSIGNED ) / CAST( SUM( leaderboard_score_team.total_kill ) AS UNSIGNED ) * 100 ) AS win_rate
    FROM
        leaderboard_score_team
        LEFT JOIN leaderboard_score_sequence ON leaderboard_score_sequence.id_score_sequence = leaderboard_score_team.id_score_sequence
        LEFT JOIN team ON team.team_id = leaderboard_score_team.id_team
    WHERE
        game_id IS NOT NULL
        AND team.deleted_at IS NULL
    GROUP BY
        leaderboard_score_team.id_team");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_team_leaderboards_view');
    }
}
