<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsernameColumnMessageTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message_teams', function (Blueprint $table) {
            $table->string('user_id',128)->change();
            $table->string('username',128)->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_teams', function (Blueprint $table) {
            $table->string('user_id')->change();
            $table->dropColumn('username');
        });
    }
}
