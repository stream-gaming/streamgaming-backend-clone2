<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryBroadcastTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('history_broadcast_ticket')) {
                Schema::create('history_broadcast_ticket', function (Blueprint $table) {
                    $table->id();
                    $table->integer('id_ticket');
                    $table->integer('giving_target');
                    $table->integer('unit');
                    $table->integer('total_given');
                    $table->string('distributor');
                    $table->timestamps();
                });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_broadcast_ticket');
    }
}
