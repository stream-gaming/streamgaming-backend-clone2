<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexHistoryBanned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_banned', function (Blueprint $table) {
            $table->index('identity_id');
            $table->index('team_id');
            $table->index('admin_id');
            $table->index('ban_until');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_banned', function (Blueprint $table) {
            $table->dropIndex('identity_id');
            $table->dropIndex('team_id');
            $table->dropIndex('admin_id');
            $table->dropIndex('ban_until');
        });
    }
}
