<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDeviceOnFcmNotifTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('fcm_notif_token')) {
            if(!Schema::hasColumn('fcm_notif_token', 'device')) {
                Schema::table('fcm_notif_token', function (Blueprint $table) {
                    $table->integer('device')->default(1)->comment('1 = mobile, 2 = web')->after('token');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fcm_notif_token', function (Blueprint $table) {
            $table->dropColumn('device');
        });
    }
}
