<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('history_tickets')) {
            Schema::create('history_tickets', function (Blueprint $table) {
                $table->id();
                $table->string('user_id');
                $table->string('team_id');
                $table->integer('ticket_id');
                $table->enum('type', ['Increase ticket','Decrease ticket']);
                $table->enum('source', ['from buying','tour payment', 'from refund', 'tour gift', 'admin gift']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_tickets');
    }
}
