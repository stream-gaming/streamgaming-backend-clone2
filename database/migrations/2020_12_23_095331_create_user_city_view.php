<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserCityView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS user_city_view');
        DB::statement('
            CREATE VIEW user_city_view AS
            SELECT u.user_id, k.nama as kota
            FROM users AS u LEFT JOIN kabupaten AS k
            ON BINARY u.kota = BINARY k.id_kab
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_city_view');
    }
}
