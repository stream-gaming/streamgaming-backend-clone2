<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrizeInventoryHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prize_inventory_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pi_id');
            $table->string('tournament_id');
            $table->integer('used_unit');
            $table->enum('purpose', ['used', 'return']);
            $table->timestamps();
            $table->foreign('pi_id')->references('id')->on('prize_inventory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prize_inventory_history');
    }
}
