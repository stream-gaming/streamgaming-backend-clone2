<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTournamentMvp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament_mvp', 'kda_rate') ) {
            Schema::table('tournament_mvp', function (Blueprint $table) {
                $table->double('kda_rate')->nullable();
                $table->integer('jumlah')->nullable()->change();
            });
        } 
      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tournament_mvp', 'kda_rate') ) {
            Schema::table('tournament_mvp', function (Blueprint $table) {
                $table->dropColumn('kda_rate');
                $table->string('jumlah')->nullable(false)->change();
            });
        }
    }
}
