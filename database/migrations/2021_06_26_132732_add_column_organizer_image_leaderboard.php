<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOrganizerImageLeaderboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
            if(!Schema::hasColumn('leaderboard', 'organizer_image')) {

                Schema::table('leaderboard', function (Blueprint $table) {
                    $table->text('organizer_image')->after('organizer_name');

                });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if(Schema::hasColumn('leaderboard', 'organizer_image')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->dropColumn('organizer_image');
            });
        }
    }
}
