<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ms_faqs')) {
        Schema::create('ms_faqs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->longText('keterangan');
            $table->string('icon');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_faqs');
    }
}
