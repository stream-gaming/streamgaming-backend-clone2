<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDiscussionStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('leaderboard', 'discussion_status')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->boolean('discussion_status')->default('0');
            });
        }
        if (!Schema::hasColumn('tournament', 'discussion_status')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->boolean('discussion_status')->default('0');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('leaderboard', function (Blueprint $table) {
            $table->dropColumn('discussion_status');
        });

        Schema::table('tournament', function (Blueprint $table) {
            $table->dropColumn('discussion_status');
        });
    }
}
