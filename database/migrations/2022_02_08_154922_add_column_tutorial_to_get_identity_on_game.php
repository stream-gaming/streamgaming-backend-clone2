<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTutorialToGetIdentityOnGame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('game_list', 'identity_game_tutorial')) {
            Schema::table('game_list', function (Blueprint $table) {
                $table->text('identity_game_tutorial')->nullable()->after('content');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('game_list', 'identity_game_tutorial')) {
            Schema::table('game_list', function (Blueprint $table) {
                $table->dropColumn('identity_game_tutorial');
            });
        }

    }
}
