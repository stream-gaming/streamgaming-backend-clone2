<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->string('deleted_at')->nullable()->change(); 
            $table->string('is_banned')->default('0')->change(); 
            $table->string('ban_until')->nullable()->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->string('deleted_at')->nullable(false)->change(); 
            $table->string('is_banned')->default('0')->change(); 
            $table->string('ban_until')->nullable()->change(); 
        });
    }
}
