<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeEnumForGame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(Schema::hasTable('game_list')){
            DB::statement(
                "ALTER TABLE game_list
                 MODIFY COLUMN
                 system_kompetisi
                 enum('Bracket','LeaderBoards','Multisystem')
                 NOT NULL AFTER substitute_player
                "
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}



