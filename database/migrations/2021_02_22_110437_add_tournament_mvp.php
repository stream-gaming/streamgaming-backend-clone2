<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTournamentMvp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tournament_mvp')) {
            Schema::create('tournament_mvp', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('id_tournament');
                $table->string('id_player');
                $table->string('nama_player');
                $table->string('jumlah');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_mvp');
    }
}
