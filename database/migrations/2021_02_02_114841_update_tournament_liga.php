<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTournamentLiga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament_liga', 'slot_tersedia') AND !Schema::hasColumn('tournament_liga', 'jumlah_hadiah') ) {
        Schema::table('tournament_liga', function (Blueprint $table) {
            $table->integer('slot_tersedia');
            $table->string('jumlah_hadiah');
            $table->string('kompetisi');
            $table->string('id_tab_liga');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament_liga', function (Blueprint $table) {
            //
        });
    }
}
