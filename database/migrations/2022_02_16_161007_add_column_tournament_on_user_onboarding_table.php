<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTournamentOnUserOnboardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_onboarding')) {
            if(!Schema::hasColumn('user_onboarding', 'tournament_page')) {
                Schema::table('user_onboarding', function (Blueprint $table) {
                    $table->integer('tournament_page')->default(0)->comment('Visit tournament page: 0=false, 1=true')->after('status');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_onboarding', function (Blueprint $table) {
            $table->dropColumn('tournament_page');
        });
    }
}
