<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableTeamRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('team_request', 'deleted_at')) {
            Schema::table('team_request', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();  
            });
        } else {
            Schema::table('team_request', function (Blueprint $table) {
                $table->string('deleted_at')->nullable(); 
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // if (Schema::hasColumn('deleted_at')) {
            //     $table->string('deleted_at')->nullable(); 
            // }
            // $table->string('deleted_at')->nullable(false)->change(); 
        });
    }
}
