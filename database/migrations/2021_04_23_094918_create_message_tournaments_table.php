<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_tournaments', function (Blueprint $table) {
            $table->id();
            $table->string('room_id')->comment('ID Room [id Tournament]');;
            $table->string('user_id');
            $table->string('username');
            $table->enum('user_status', [0, 1])->default('0')->comment('0=User,1=Admin');
            $table->string('team_id')->nullable();
            $table->longText('message')->nullable();
            $table->string('attachment')->nullable();
            $table->string('user_read')->comment('User yang sudah membaca pesan');;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_tournaments');
    }
}
