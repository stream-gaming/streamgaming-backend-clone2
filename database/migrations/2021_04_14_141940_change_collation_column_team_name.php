<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeCollationColumnTeamName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function (Blueprint $table) {
            if (Schema::hasColumn('team', 'team_name')) {
                DB::statement('ALTER TABLE team MODIFY team_name VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function (Blueprint $table) {
            if (Schema::hasColumn('team', 'team_name')) {
                DB::statement('ALTER TABLE team MODIFY team_name VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci;');
            }
        });
    }
}




