<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeIdAttributeOnPortalNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        if (Schema::hasTable('portal_notification')) {
            Schema::table('portal_notification', function (Blueprint $table) {
                DB::statement("ALTER TABLE portal_notification MODIFY id INT NOT NULL PRIMARY KEY AUTO_INCREMENT");

            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
