<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeFaqsAtFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faqs', 'type_faqs') ) {
            Schema::table('faqs', function (Blueprint $table) {
                $table->integer('is_sub_menu')->after('id_ms_faqs');
                $table->integer('type_faqs')->after('id_ms_faqs');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('faqs', 'type_faqs') ) {
            Schema::table('faqs', function (Blueprint $table) {
                $table->dropColumn('type_faqs');
                $table->dropColumn('is_sub_menu');
            });
        }
    }
}
