<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageColumnOnTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('all_ticket', 'image')) {
            Schema::table('all_ticket', function (Blueprint $table) {
                $table->string('image')->nullable()->after('competition');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if(!Schema::hasColumn('all_ticket', 'image')) {
            Schema::table('all_ticket', function (Blueprint $table) {
                $table->dropColumn('image');
            });
        }

    }
}
