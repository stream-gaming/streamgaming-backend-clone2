<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsNotifiedFirebaseOnNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasColumn('notification', 'notified_on_firebase') AND !Schema::hasColumn('notification', 'source')) {
            Schema::table('notification', function(Blueprint $table){
                $table->integer('source')->default(0)->after('date')->comment('0 = from streamgaming, 1 = from portal');
                $table->integer('notified_on_firebase')->default(0)->after('source')->comment('Used to indicate that this data has been sent to firebase');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasColumn('notification', 'notified_on_firebase') AND Schema::hasColumn('notification', 'source') ) {
            Schema::table('notification', function (Blueprint $table) {
                $table->dropColumn(['notified_on_firebase', 'source']);
            });
        }
    }
}
