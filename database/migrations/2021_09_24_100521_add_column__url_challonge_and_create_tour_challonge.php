<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUrlChallongeAndCreateTourChallonge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('tournament', 'create_tour_challonge_from') AND !Schema::hasColumn('tournament', 'url_challonge')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->boolean('is_challonge')->nullable()->after('is_simultaneously');
                $table->enum('create_tour_challonge_from', ["stream-gaming", "challonge"])->nullable()->after('prize_mvp');
                $table->string('url_challonge')->nullable()->after('url_livestreams');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('tournament', 'create_tour_challonge_from') AND Schema::hasColumn('tournament', 'url_challonge')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->dropColumn('create_tour_challonge_from');
                $table->dropColumn('url_challonge');
                $table->dropColumn('is_challonge');
            });
        };
    }
}
