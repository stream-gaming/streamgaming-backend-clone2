<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPeraturanTombolDaftar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament', 'peraturan_tombol_daftar')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->longText('peraturan_tombol_daftar')->after('peraturan_khusus');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tournament', 'peraturan_tombol_daftar')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->dropColumn('peraturan_tombol_daftar');
            });
        }
    }
}
