<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddUniqueFlagColumnPlayerPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //set unique flag column
        if(!Schema::hasColumn('player_payment', 'unique_flag')) {
            Schema::table('player_payment', function (Blueprint $table) {
                $table->integer('unique_flag')->default('1')->nullable()->after('status');
            });
        }

        //set unique_flag to null to all cancelled status player_payment
        if(Schema::hasColumn('player_payment', 'unique_flag')) {
            DB::statement('UPDATE player_payment SET unique_flag=null WHERE status="Cancelled"');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('player_payment', function (Blueprint $table) {
            $table->dropColumn('unique_flag');
        });
    }
}
