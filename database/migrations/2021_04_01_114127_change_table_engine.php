<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeTableEngine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'api_logs',
            'gallery_like',
            'management_docs',
            'news_like',
            'oauth',
            'online_log',
            'portal_notification',
            'team_player',
            'team_request',
            'users_friend',
            'users_game',
        ];

        foreach ($tables as $table) {
            DB::statement('ALTER TABLE ' . $table . ' ENGINE = InnoDB');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'api_logs',
            'gallery_like',
            'management_docs',
            'news_like',
            'oauth',
            'online_log',
            'portal_notification',
            'team_player',
            'team_request',
            'users_friend',
            'users_game',
        ];

        foreach ($tables as $table) {
            DB::statement('ALTER TABLE ' . $table . ' ENGINE = MyISAM');
        }
    }
}
