<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdParticipantChallonge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('tournament_group', 'id_participant_challonge')) {
            Schema::table('tournament_group', function (Blueprint $table) {
                $table->string('id_participant_challonge')->nullable()->after('win');
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('tournament_group', 'id_participant_challonge') ) {
            Schema::table('tournament_group', function (Blueprint $table) {
                $table->dropColumn('id_participant_challonge');
            });
        };
    }
}
