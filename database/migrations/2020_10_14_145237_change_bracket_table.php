<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBracketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('tournament', 'game_option')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();
                $table->integer('game_option')->comment('1 = solo, 2 = duo, 3 >= Squad')->change();
            });
        }
        else{
            Schema::table('tournament', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();
                $table->integer('game_option')->after('tourname_type')->comment('1 = solo, 2 = duo, 3 >= Squad');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tournament', 'game_option')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();
                $table->dropColumn('game_option');
            });
        }
        else{
            Schema::table('tournament', function (Blueprint $table) {
                $table->string('deleted_at')->nullable()->change();
            });
        }
    }
}
