<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserOnboardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_onboarding', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->id();
            $table->char('user_id', 128)->unique();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->integer('preference')->default(0)->comment('0=undefined, 1=solo, 2=team');
            $table->integer('step')->default(0)->comment('Passed step: 1=add game, 2=choosing role solo/team, 3=create/join/find team');
            $table->integer('status')->default(0)->comment('0=on progress, 1=finished, 2=skipped');
            $table->integer('tournament_page')->default(0)->comment('Visit tournament page: 0=false, 1=true');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_onboarding');
    }
}
