<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTournamentMenuLiga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tournament_menu_liga')) {
        Schema::create('tournament_menu_liga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_tab_liga');
            $table->string('nama_menu_liga');
            $table->longText('deskripsi_menu_liga');
        });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_menu_liga');
    }
}
