<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableTeamPlayer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_player', function (Blueprint $table) {
            $table->string('deleted_at')->nullable()->change(); 
            $table->string('player_id')->nullable()->change(); 
            $table->string('substitute_id')->nullable()->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_player', function (Blueprint $table) {
            $table->string('deleted_at')->nullable(false)->change(); 
            $table->string('player_id')->nullable(false)->change(); 
            $table->string('substitute_id')->nullable(false)->change(); 
        });
    }
}
