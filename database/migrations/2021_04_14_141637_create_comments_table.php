<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->enum('user_status', ['admin', 'user'])->comment('User Role');
            $table->string('user_id');
            $table->integer('parent_id')->nullable()->comment('ID Parent Komentar yang di Reply');
            $table->integer('reply_id')->nullable()->comment('ID Komentar yang di Reply');
            $table->longText('comment');
            $table->string('commentable_id')->comment('ID Tournament');
            $table->string('commentable_type')->comment('Tipe Komentar (komentar ini untuk turnamen atau tidak)');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
