<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableAllInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('all_invoice')) {
            Schema::create('all_invoice', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('id_tournament');
                $table->enum('system_tournament', ['Bracket','Leaderboard']);
                $table->string('organizer_name');
                $table->string('date_end');
                $table->integer('total_prize');
                $table->string('game');
                $table->string('status');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_invoice');
    }
}
