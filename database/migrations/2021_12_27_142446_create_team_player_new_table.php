<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTeamPlayerNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('team', function(Blueprint $table){
            DB::statement('ALTER TABLE team ENGINE = InnoDB');
            $table->string('team_id')->charset('utf8')->collation('utf8_general_ci')->change();
        });

        Schema::create('team_player_new', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('team_id', 100)->charset('utf8')->collation('utf8_general_ci');
            $table->foreign('team_id')->references('team_id')->on('team');
            $table->string('player_id');
            $table->integer('player_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_player_new');
    }
}
