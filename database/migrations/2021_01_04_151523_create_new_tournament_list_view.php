<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNewTournamentListView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS list_tournament');
        DB::statement("
        CREATE VIEW list_tournament
        AS
            SELECT
            CASE
                    t.STATUS
                    WHEN 'pending' THEN
                    0
                    WHEN 'open_registration' THEN
                    1
                    WHEN 'end_registration' THEN
                    2
                    WHEN 'starting' THEN
                    3
                    WHEN 'complete' THEN
                    4 ELSE 5
                END AS statuss,
                t.id_tournament AS id_tournament,
                'Bracket' AS format_tournament,
                STR_TO_DATE(t.date,'%d-%m-%Y %H:%i') AS tournament_date,
                tournament_name,
                url,
                t.organizer_name,
                start_registration AS regist_start,
                end_registration AS regist_end,
                kompetisi as game_competition,
                banner_tournament AS card_banner,
                game_option,
                payment as tournament_fee,
                'default' as prize_type,
                total_prize,
                LOWER(system_payment) as payment_type,
                2 as category,
                t.total_team as total_team,
                filled_slot,
                id_liga,
                created_at,
                                ROUND(((100 / t.total_team) * filled_slot),2) as filled

            FROM
                tournament AS t
            WHERE
                t.deleted_at IS NULL UNION ALL
            SELECT
                l.status_tournament AS statuss,
                l.id_leaderboard AS id_tournament,
                'Leaderboard' AS format_tournament,
                STR_TO_DATE(l.tournament_date,'%d-%m-%Y %H:%i'),
                tournament_name,
                url,
                organizer_name,
                regist_start,
                regist_end,
                game_competition,
                card_banner,
                game_option,
                tournament_fee,
                prize_type,
                (firstprize + secondprize + thirdprize + most_kill) AS total_prize,
                tournament_form as payment_type,
                tournament_category as category,
                (CASE WHEN l.total_team = 0 THEN lq.total_team ELSE l.total_team END) AS total_team,
                filled_slot,
                id_liga,
                input_date AS created_at,
                                ROUND(((100 / (CASE WHEN l.total_team = 0 THEN lq.total_team ELSE l.total_team END)) * filled_slot),2) as filled
            FROM
                leaderboard AS l
                                LEFT JOIN leaderboard_qualification as lq ON lq.id_leaderboard = l.id_leaderboard AND lq.qualification_sequence=1
            WHERE
                l.deleted_at IS NULL
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_tournament_list_view');
    }
}
