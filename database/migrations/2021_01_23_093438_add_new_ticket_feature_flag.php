<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewTicketFeatureFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('tournament', 'is_new_ticket_feature')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->integer('is_new_ticket_feature')->default('0')->after('system_payment');
            });
        }

        if (!Schema::hasColumn('leaderboard', 'is_new_ticket_feature')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->integer('is_new_ticket_feature')->default('0')->after('ticket_unit');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tournament', function (Blueprint $table) {
            $table->dropColumn('is_new_ticket_feature');

        });
        Schema::table('leaderboard', function (Blueprint $table) {
            $table->dropColumn('is_new_ticket_feature');

        });
    }
}
