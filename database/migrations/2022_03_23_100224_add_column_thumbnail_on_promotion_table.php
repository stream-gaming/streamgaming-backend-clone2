<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnThumbnailOnPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('promotion')) {
            if(!Schema::hasColumn('promotion', 'thumbnail')) {
                Schema::table('promotion', function (Blueprint $table) {
                    $table->text('thumbnail')->after('link');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotion', function (Blueprint $table) {
            $table->dropColumn('thumbnail');
        });
    }
}
