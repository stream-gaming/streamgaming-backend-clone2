<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPortal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('log_portal')) {
            Schema::create('log_portal', function (Blueprint $table) {
                $table->id();
                $table->text('description')->comment('description of user activity eg: Insert tournament leaderboard');
                $table->string('category', 300)->comment('category of user activity eg: insert, update or delete');
                $table->string('menu', 150)->comment('menu where user is doing the activity eg: leaderboard or ticket');
                $table->string('route', 300)->comment('route where user is doing the activity eg: detail-leaderboard');
                $table->string('super_user_id')->comment('id of super user/admin who is doing the activity');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_portal');
    }
}
