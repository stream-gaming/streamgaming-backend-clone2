<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTournament extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ticket_tournament')) {
            Schema::create('ticket_tournament', function (Blueprint $table) {
                $table->id();
                $table->string('tournament_id');
                $table->integer('ticket_id');
                $table->integer('is_payment');
                $table->integer('is_prize');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_tournament');
    }
}
