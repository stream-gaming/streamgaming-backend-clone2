<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListMvp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('list_mvp')) {
            Schema::create('list_mvp', function (Blueprint $table) {
                $table->id();
                $table->string('tournament_id')->references('id_tournament')->on('tournament');;
                $table->string('name_player');
                $table->string('user_id');
                $table->string('team_name');
                $table->string('team_id');
                $table->double('kda_score');
                $table->integer('strike_mvp');
                $table->enum('mvp_type', ['strike_mvp', 'kda_score']);
                $table->integer('sequence');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        if (Schema::hasTable('list_mvp')) {
            Schema::dropIfExists('list_mvp');
        }
    }
}
