<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateCollectionTournamentWinner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('tournament_winner_sequence', function (Blueprint $table) {
        //     $table->string('id_receiver')->collation('latin1_swedish_ci')->change();
        // });
        DB::statement("ALTER TABLE tournament_winner_sequence MODIFY id_receiver VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('tournament_winner_sequence', function (Blueprint $table) {
        //     $table->string('id_receiver')->collation('utf8mb4_unicode_ci')->change();
        // });
        DB::statement("ALTER TABLE tournament_winner_sequence MODIFY id_receiver VARCHAR(255) CHARACTER SET latin1 COLLATE utf8mb4_unicode_ci");
    }
}
