<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketPurchaseHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_purchase_history', function (Blueprint $table) {
            $table->id();
            $table->string('order_id');
            $table->string('xendit_invoice_id');
            $table->string('user_id');
            $table->string('team_id')->nullable();
            $table->unsignedBigInteger('ticket_id');
            $table->integer('quantity');
            $table->integer('amount')->comment('amount  = ticket_prize * quantity');
            $table->string('provider')->comment('example: Xendit or Stream Cash');
            $table->string('payment_method')->nullable();
            $table->integer('status')->comment('1 = for success status, 0 = for pending status only if expired_at > now');
            $table->timestamp('expired_at')->nullable()->comment('if expired_at < date_now then the status labeled as an expired invoice');
            $table->timestamps();


            $table->foreign('ticket_id')->references('id')->on('all_ticket');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_purchase_history');
    }
}
