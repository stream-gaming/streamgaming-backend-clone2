<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedExpressionFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('expression_faqs')) {
            Schema::create('expression_faqs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('id_faqs');
                $table->string('users_id');
                $table->integer('like');
                $table->integer('dislike');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expression_faqs', function (Blueprint $table) {
            //
        });
    }
}
