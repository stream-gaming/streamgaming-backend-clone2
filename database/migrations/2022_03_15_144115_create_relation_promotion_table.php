<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRelationPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_promotion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('promotion_id');
            $table->foreign('promotion_id')->references('id')->on('promotion');
            $table->unsignedBigInteger('target_id');
            $table->foreign('target_id')->references('id')->on('target_promotion');
        });

        //add unique constraint
        if (Schema::hasTable('relation_promotion')) {
            DB::statement('ALTER TABLE relation_promotion ADD UNIQUE `unique_relation_promotion_combination`(`promotion_id`,`target_id`)');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_promotion');
    }
}
