<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketClaimsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_claims', function (Blueprint $table) {
            $table->id();
            $table->integer('ticket_id')->comment('Id Tiket Yang ingin disebar');
            $table->integer('max_claim')->comment('Maksimal user bisa klaim berapa tiket sesuai periode');
            $table->enum('period', ['Daily','Weekly','Monthly'])->default('Daily')->comment('Periode tiket disebar/di Klaim');
            $table->date('event_start')->comment('Event tiket mulai disebar');
            $table->date('event_end')->comment('Event tiket akhir disebar');
            $table->enum('is_active', [1, 0])->default('1')->comment('0=NonAktif,1=Aktif');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_claims');
    }
}



