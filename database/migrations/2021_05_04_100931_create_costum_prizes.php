<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostumPrizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costum_prizes', function (Blueprint $table) {
            $table->id();
            $table->string('tournament_id');
            $table->unsignedBigInteger('pi_id');
            $table->integer('sequence');
            $table->integer('quantity');
            $table->timestamps();
            $table->foreign('pi_id')->references('id')->on('prize_inventory');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costum_prizes');
    }
}
