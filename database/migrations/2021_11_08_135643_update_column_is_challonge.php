<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnIsChallonge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tournament', function(Blueprint $table){
            $table->integer('is_challonge')->default(0)->after('is_simultaneously')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament', function(Blueprint $table){
            $table->boolean('is_challonge')->nullable()->after('is_simultaneously');
        });
    }
}
