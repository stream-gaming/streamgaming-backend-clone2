<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryBannedTeamTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_banned_team', function (Blueprint $table) {
            $table->id();
            $table->string('team_id');
            $table->string('ban_until');
            $table->text('reason_ban');
            $table->string('admin_id')->comment('ID Admin yang melakukan ban');
            $table->timestamps();
            $table->index('team_id');
            $table->index('admin_id');
            $table->index('ban_until');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_banned_team');
    }
}
