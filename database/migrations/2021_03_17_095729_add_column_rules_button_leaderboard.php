<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRulesButtonLeaderboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('leaderboard')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->string('button_rules')->after('rules');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasTable('leaderboard')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->dropColumn('button_rules');
            });
     }
    }
}
