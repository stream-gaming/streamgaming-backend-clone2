<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeGiftTicketBracket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament', 'lots_of_ticket')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->integer('lots_of_ticket')->default('0')->after('gift_ticket');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament', function (Blueprint $table) {
            $table->dropColumn('lots_of_ticket');
        });
    }
}
