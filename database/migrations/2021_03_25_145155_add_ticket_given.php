<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTicketGiven extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament_ticket_sequence', 'id_ticket_given')) {
            Schema::table('tournament_ticket_sequence', function (Blueprint $table) {
                $table->string('id_ticket_given')->after('id_ticket_squence');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournament_ticket_sequence', function (Blueprint $table) {
            $table->dropColumn('ticket_given');
        });
    }

}
