<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsTicketPrizeNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tournament', 'is_ticket_prize_new')) {
            Schema::table('tournament', function (Blueprint $table) {
                $table->integer('is_ticket_prize_new')->default('0')->after('gift_ticket');
            });
        }

        if (!Schema::hasColumn('leaderboard', 'is_ticket_prize_new')) {
            Schema::table('leaderboard', function (Blueprint $table) {
                $table->integer('is_ticket_prize_new')->default('0')->after('isTicketPrize');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
