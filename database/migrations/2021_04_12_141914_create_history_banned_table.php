<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryBannedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_banned', function (Blueprint $table) {
            $table->id();
            $table->string('identity_id')->comment('Identity id yang dibanned, jika solo')->nullable();
            $table->string('team_id')->nullable();
            $table->string('ban_until');
            $table->text('reason_ban');
            $table->string('admin_id')->comment('ID Admin yang melakukan ban');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_banned');
    }
}
