<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTicketColumnPlayerPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('player_payment', 'ticket_id')) {
            Schema::table('player_payment', function (Blueprint $table) {
                $table->integer('ticket_id')->default('0')->after('type_payment');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('player_payment', function (Blueprint $table) {
            $table->dropColumn('ticket_id');

        });
    }
}
