<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_versions', function (Blueprint $table) {
            $table->id();
            $table->integer('appId')->comment('1 => android, 2=> ios');
            $table->string('versionName');
            $table->bigInteger('versionCode');
            $table->boolean('recommendUpdate');
            $table->boolean('forceUpdate');
            $table->text('changeLogs');
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_versions');
    }
}
