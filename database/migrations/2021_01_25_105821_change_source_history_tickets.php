<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeSourceHistoryTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                //
                if (Schema::hasTable('history_tickets')) {

                    Schema::table('history_tickets', function(Blueprint $table) {

                        $table->integer('source')->comment('
                        1 : from buying
                        2 : tour payment
                        3 : from refund
                        4 : tour gift
                        5 : admin gift
                        ')->change();

                        $table->string('type')->comment('
                        add : Increase ticket
                        cut : Decrease ticket
                        ')->change();

                    });
                }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
