<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTournamentWinnerSequence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tournament_winner_sequence')) {
        Schema::create('tournament_winner_sequence', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_winner_sequence');
            $table->integer('orders');
            $table->string('amount_given');
            $table->string('id_receiver');
            $table->string('date');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_winner_sequence');
    }
}
