<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexIdentityIngame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('identity_ingame', function (Blueprint $table) {
            $table->index('game_id');
            $table->index('username_ingame');
            $table->index('is_banned');
            $table->index('ban_until');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('identity_ingame', function (Blueprint $table) {
            $table->dropIndex('game_id');
            $table->dropIndex('username_ingame');
            $table->dropIndex('is_banned');
            $table->dropIndex('ban_until');
        });
    }
}
