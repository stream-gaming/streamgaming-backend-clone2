<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBannedInIdentityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('identity_ingame', function (Blueprint $table) {
            $table->integer('is_banned')->after('username_ingame')->default(0);
            $table->string('ban_until')->after('is_banned')->nullable();
            $table->string('reason_ban')->after('ban_until')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('identity_ingame', 'is_banned')) {
            Schema::table('identity_ingame', function (Blueprint $table) {
                $table->dropColumn(['is_banned', 'ban_until', 'reason_ban']);
            });
        }
    }
}
