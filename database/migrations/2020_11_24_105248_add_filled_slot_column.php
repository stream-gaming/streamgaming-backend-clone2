<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilledSlotColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->integer('filled_slot')->after('peraturan_khusus');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->dropColumn(['filled_slot']);
        });
    }
}
