<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalPlayerAndTournamentCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('game_list', 'total_player')) {
            Schema::table('game_list', function (Blueprint $table) {
                $table->integer('total_player')->after('url');
            });
        }
        if (!Schema::hasColumn('game_list', 'total_tournament')) {
            Schema::table('game_list', function (Blueprint $table) {
                $table->integer('total_tournament')->after('total_player');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('game_list', function (Blueprint $table) {
            $table->dropColumn('total_player');
            $table->dropColumn('total_tournament');
        });
    }
}
