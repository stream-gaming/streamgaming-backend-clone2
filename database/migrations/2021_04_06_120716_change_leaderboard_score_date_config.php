<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLeaderboardScoreDateConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasColumn('leaderboard_score_team', 'date')) {
            Schema::table('leaderboard_score_team', function (Blueprint $table) {
                $table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'))->change();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}




