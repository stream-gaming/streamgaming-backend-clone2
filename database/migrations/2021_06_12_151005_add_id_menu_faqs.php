<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdMenuFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('faqs', 'id_menu_faqs') ) {
            Schema::table('faqs', function (Blueprint $table) {
                $table->string('id_menu_faqs')->after('is_sub_menu');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        if (Schema::hasColumn('faqs', 'id_menu_faqs') ) {
            Schema::table('faqs', function (Blueprint $table) {
                $table->dropColumn('id_menu_faqs');
            });
        }
    }
}
