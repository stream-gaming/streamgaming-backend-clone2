<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeTypeSubtypeAllTicketValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('all_ticket')) {

            Schema::table('all_ticket', function(Blueprint $table) {
                $table->integer('type')->comment('
                1: Reguler
                2: Premium
                3: Platinum')->change();

                $table->integer('sub_type')->comment('
                1: New User
                2: As Tournament Prize
                3: Payment Tournament/Tournament Free')->change();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
