<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('all_ticket')) {
            Schema::create('all_ticket', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->enum('type', ['Reguler','Premium','Platinum']);
                $table->enum('sub_type', ['New User','As Tournament Prize','Payment Tournament']);
                $table->integer('unit');
                $table->integer('max_given');
                $table->integer('total_given');
                $table->timestamp('expired', $precision=0);
                $table->timestamp('event_end', $precision=0);
                $table->string('competition');
                $table->integer('can_traded');
                $table->integer('market_value');
                $table->integer('giving_target');
                $table->integer('mode');
                $table->timestamps();
                $table->softDeletes('deleted_at', 0);


            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('all_ticket');
    }
}
