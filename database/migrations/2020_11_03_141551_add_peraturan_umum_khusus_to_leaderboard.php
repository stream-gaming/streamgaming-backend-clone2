<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPeraturanUmumKhususToLeaderboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->text('peraturan_umum')->after('rules');
            $table->text('peraturan_khusus')->after('peraturan_umum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaderboard', function (Blueprint $table) {
            //
            $table->dropColumn(['peraturan_umum', 'peraturan_khusus']);
        });
    }
}
