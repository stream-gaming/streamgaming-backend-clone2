<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => [
        'not'           => 'Email Not Registered.',
        'verify'        => 'Please Verify Email.',
        'inactive'      => 'Account is Inactive.',
        'incorrect'     => 'Password Incorrect.',
        'failed'        => 'Login Failed.'
    ],

    'change-password' => [
        'current'       => 'Current password is incorrect!!.',
        'similar'       => 'The new password cannot be the same as the old password.',
        'success'       => 'Successfully changed password.',
        'failed'        => 'Failed changed password.'
    ],

    'register' => [
        'already'       => 'Email Already Registered.',
        'success'       => 'User Registration Success.',
        'failed'        => 'User Registration Failed!',
    ],

    'logout' => [
        'success'       => 'User logged out successfully.',
        'failed'        => 'Sorry, the user cannot be logged out!',
    ],

    'refresh' => [
        'token' => [
            'invalid'           => 'This token is invalid. Please Login. :code',
            'expired'           => 'Session expired. :code',
            'unauthenticated'   => 'Unauthenticated. :code',
            'failed'            => 'Token cannot be refreshed, please Login again :code',
            'blacklist'         => 'The token has been blacklisted'

        ]
    ]

];
