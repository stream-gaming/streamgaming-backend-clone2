<?php

return [

    'throttle' => 'Too many Request attempts. Please try again in :seconds seconds.',

    'expired' => "Link is Expired.",
    'invalid' => "Invalid/Expired url provided.",
    'already' => "Link Already Use.",
    'success' => "Successful email verification.",

    'resend' => [
        'sent' => 'Email verification link sent on your email.',
        'already' => "Email already verified"
    ],

    'check' => [
        'has' => "Email has been verified",
        'not' => "Email has not been verified"
    ],

    'notify' => [
        'error' => "Whoops! ",
        'greeting' => "Hello! ",
        'visit' => "Visit Our Company Profile",
        'click' => "Click Here",
        'annoying' => "If these emails get annoying, please feel free to  ",
        'unsubscribe' => "unsubscribe",
        'button' => [
            'verify' => "Confirm Email",
            'forget' => "Reset Password"
        ],
        'verify' =>  [
            'subject' => 'Please Verify Your Email',
            'title' => 'Verify your email',
            'one' => "To confirm your new account at Stream Gaming, click the button below. If you didn't register this account, please let us know.",
            'two' => "By confirming your account, you accept",
            'three' => "Terms of Service and Privacy Policy",
            'expired' => "This link will expire in :count minutes.",
        ],
        'forget' =>  [
            'subject' => 'Stream Gaming Password Reset',
            'title' => 'TROUBLE SIGN IN?',
            'one' => "You have made a request to reset your Stream Gaming Account password. The reset link will only be valid if it was opened on the same browser and device when making the reset request. To continue the process click the button below.",
            'expired' => "This link will expire in :count minutes.",
        ],
        'trouble' => [
            'click' => "If you can't click the button above, copy and paste the link below : ",
        ],
    ]

];
