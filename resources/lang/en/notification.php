<?php

return [

	'team'	=> [
		'join'		=> [
			'request'	=> '<b>:Username</b> asked to join your team.',
			'accept'	=> 'Your request to join the <b>:Team </b> team has been approved.',
			'decline'	=> 'Your request to join the <b>:Team </b> team has been rejected.'
		],

		'invite'	=> [
			'request'	=> 'Team <b>:Team </b> invites you to join.',
			'accept'	=> '<b>:Username </b> accept an invitation to join your team.',
			'decline'	=> '<b>:Username </b> declined the invitation to join your team.'
		],
		'leave'		=> [
			'new-leader'	=> 'The leader team <b>:Team</b> is now assigned to you.',
	    	'members'		=> 'Your leader has left the team, leadership is given to <b>: username </b>.',
	    	'member'		=> '<b>:Username</b> has left the team.'
		],
		'kick'		=> 'You have been kicked out of the <b>:Team </b> team.',
		'banned' => 'Your Team <b>:team</b> has been banned, :reason until :until',
		'unbanned'	=> 'The period of banned team <b>:Team </b> has ended, now you can use the feature.'
	],
	'claim'     => [
        'ticket'    => 'You have got new :ticket'
	],
    'ticket' => [
        'purchase' => 'Your purchase of :unit of :ticket_name for :prefix :amount unit :payment_method is successful',
        'purchase_xendit_invoice' => 'You have just generated invoice for :ticket_name (ticket) payment with amount Rp. :amount. Your invoice id is :invoice_id.',
        'purchase_xendit_invoice_paid' => 'We have received your payment for invoice with id :invoice_id. The ticket has been added to your ticket inventory. Thank you!'
    ],
    'comment' => [
        'reply' => ':sender_name replied to your comment on the :tour_name tournament',
        'delete' => ':sender_name has deleted your comment on the :tour_name tournament'
    ],
];
