<?php

return [
    'error' => [
        '500' => 'Internal server problem, please try again later'
    ],
 ];
