<?php
return [
    'success' => [
        'finish_onboarding' => 'Congratulations! You have just finished our basic onboarding process.
                                To start joining the tournament you must claim
                                the tickets by clicking this notification'
    ],
    'failed' => [
        'preference' => 'Failed to set preference',
    ]
];
