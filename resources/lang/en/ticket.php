<?php

return [
    'from buying' => 'Ticket purchase successful',
    'tour payment' => 'Tournament registration using ticket was successful',
    'from refund' => 'You receive a ticket refund',
    'tour gift' => 'You received a ticket prize',
    'ticket_limit' => 'Failed to purchase tickets because the number of tickets for your team has reached the limit.',
    'not enough_ticket' => 'Your ticket is not enough',
    'not_enough_ticket_solo' => "Failed to register for the tournament because you don't have tickets yet!",
    'not_enough_ticket_team' => "Failed to register for the tournament because your team doesn't have tickets yet!",
    'expired_ticket' => 'Ticket not valid or already expired!',
    'xendit_payment' => 'Purchase :ticket_name for :quantity unit',
];
