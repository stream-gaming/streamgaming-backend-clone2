<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tournament Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'status' => [
        'incoming'              => 'incoming',
        'open_registration'     => 'open registration',
        'close_registration'    => 'close registration',
        'in_progress'           => 'in progress',
        'complete'              => 'complete'
    ],
    'page' => [
        'title'         => 'Tournament',
        'description'   => 'Live tournaments, match highlights and your favorite pro players all in one place'
    ],
    'tabname' => [
        'all'       => 'all',
        'thisweek'  => 'this week',
        'upcoming'  => 'upcoming',
        'finished'  => 'finished'
    ]
];
