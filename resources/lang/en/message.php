<?php

use Illuminate\Auth\Events\Failed;

return [

    'failed'   => 'The identity does not match our data.',
    'throttle' => 'Too many attempts to get in. Please try again in :seconds seconds.',

    'delete' => [
		'success'   => 'messages delete successfuly',
		'failed'    => 'messages delete failed',
	],

    'data' => [
        'add'    => [
            'success'    => 'Succesfully Added :data',
            'failed'    => 'Add :data Failed!',
        ],
        'edit'    => [
            'success'    => 'Succesfully Updated :data',
            'failed'    => 'Update :data Failed!',
        ],
    ],

    'game' => [
        'not'    => 'Game not found',
        'not-has'    => 'You have not added this game yet',
        'has'     => 'This game has been added',
        'used'    => 'ID and Nickname Game has been used',
        'add'    => [
            'success'    => 'Successfully connected game account',
            'failed'    => 'Failed connected game account',
        ],
        'edit'    => [
            'success'    => 'Successfully changed game account',
            'failed'    => 'Failed  changed game account',
        ],
        'banned'    => 'Access Denied, game account is banned!'
    ],

    'news'    => [
        'not'   => 'News not found',
        'like'    => [
            'has'   => 'You already like this news',
            'not'    => 'You havent liked this news yet.',
            'post'    => [
                'success'    => 'Success like this news',
                'failed'    => 'Failed to like this news',
            ]
        ]
    ],

    'comment' => [
        'not'   => 'Comment not found',
        'add'   => [
            'success'   => 'Successfully posted comments',
            'failed'    => 'Failed posted comments',
        ]
    ],

    'socmed' => [
        'not'       => 'Social media not found',
        'failed'    => 'Failed get social media',
        'add'    => [
            'failed'    => 'Failed add social media',
            'success'    => 'Success add social media',
        ],
        'edit'    => [
            'failed'    => 'Failed edit social media',
            'success'    => 'Success edit social media',
        ],
        'delete'    => [
            'failed'    => 'Failed delete social media',
            'success'    => 'Success delete social media',
        ],
    ],

    'user' => [
        'not'   => 'User not found',
        'profile'    => [
            'failed'    => 'Failed edit profile',
            'success'    => 'Success edit profile',
            'upload'    => [
                'failed'    => 'Failed change picture',
                'success'    => 'Success change picture',
            ],
        ],
        'account'    => [
            'failed'    => 'Failed update account settings',
            'success'    => 'Success update account settings',
        ],
        'preferences'    => [
            'failed'    => 'Failed update user preferences',
            'success'    => 'Success update user preferences',
        ],
    ],

    'balance' => [
        'not'   => 'Balance not found',
        'cash' => [
            'not-enough'   => 'Cash is not enough'
        ],
        'point' => [
            'not-enough'   => 'Point is not enough'
        ],
    ],

    'notif' => [
        'not'   => 'Notification not found',
    ],

    'player' => [
        'not'   => 'You are not player'
    ],

    'social' => [
        'get'    => [
            'success'    => 'Get User Success.',
            'failed'    => 'Get User Failed.',
        ],
        'login'    => [
            'failed'    => 'Please Login Manually',
        ],
        'register'    => [
            'success'    => 'User Registration Success',
            'failed'    => 'User Registration Failed!',
        ],
        'denied'   => 'Access Denied, Try Again!',
    ],

    'tournament' => [
        'not'       => 'Tournament not found',
        'match'     => 'Match',
        'total'     => 'Total',
        'most_kill' => 'Most Kill',
        'stage'     => 'Tahap',
        'group_final'     => 'Group Final',
        'final'     => 'Final',
        'group'     => 'Group',
        'slot-full' => 'Failed to register for the tournament because the slot is full!',
        'not-open' => 'Registrasi Closed!!',
        'cash' => [
            'payment' => [
                'failed' => 'Already Made a Payment!'
            ]
        ],
        'notification' => [
            'leader-pay' => 'Your team registration in the tournament :tournament_name is pending until all of your team members complete the payment',
            'paid'  => 'You have successfully made a payment for the tournament :tournament_name for :fee',
            'member-pay' => 'Your team leader has submitted an application for registration at the tournament :tournament_name please pay the full amount for :fee',
            'success-join' => 'Your team has been registered for the tournament :tournament_name'

        ],
        'is_registered' => 'Access denied, the team in question is in the tournament!',
        'not-found' => 'Tournament not found',
        'discussion' => [
            'not-active' => 'Tournament discussion room is not active',
        ],
        'not-leader' => "You are not a Leader",
        'not-member' => "You are not a member of the team that has joined",
    ],

    'payment' => [
        'not'   => 'Payment Not Found',
        'success'   => 'Successful tournament payment',
        'failed'   => 'Failed tournament payment',
        'lunas'   => 'Payment Success',
        'belum-lunas'  => 'Waiting for Payment',
        'cancelled'  => 'Payment Cancelled',
    ],

    'team'    => [
        'not'    => 'Team not found',
        'not-member'    => 'You are not a member of this team',
        'not-member-other'    => 'The user is not a member of this team',
        'has'     => 'Already have a team for this game',
        'full'    => 'Team is full',
        'leader'    => 'You are not the team leader',
        'add'    => [
            'success'    => 'Team created successfully',
            'failed'    => 'Team failed to create',
        ],
        'top'    => [
            'failed'    => 'Top Team not found',
        ],
        'edit'    => [
            'success'    => 'Team data changed successfully',
            'failed'    => 'Team data failed to change',
            'role'        => 'Team member list successfully changed',
            'leader'    => 'Team leader successfully changed'
        ],
        'join'    => [
            'request'    => [
                'success'    => 'Join request successfully sent',
                'failed'    => 'Join request failed to send',
                'has'        => 'Waiting for confirmation from team leader',
                'not'        => 'Request not found'
            ],
            'accept'    => 'Join request successfully approved',
            'decline'    => 'Join request has been rejected successfully',
        ],
        'invite'    => [
            'request'    => [
                'success'    => 'Invitation to join successfully sent',
                'failed'    => 'Invitation to join failed to send',
                'has'        => 'Waiting for confirmation from the user',
                'not'        => 'Invitation not found'
            ],
            'accept'    => 'Invitation to join successfully approved',
            'decline'    => 'The invitation to join was rejected successfully',
        ],
        'leave'    => 'You made it out of the team',
        'kick'    => 'Team member successfully removed',
        'denied'    => 'Access denied, try again!',
        'banned'    => 'Access denied, team has been banned, ban will expire :time',
        'leader-cannot-substitute'  => "Leaders can't be substitutes",
        'total-not-same'    => "Total players don't match"
    ],

    'notification'    => [
        'markAsRead'    => 'Success'
    ],

    'comment' => [
        'tournament' => [
            'create' => [
                'success'   => 'Successfully posted comments',
                'failed'    => 'Failed posted comments',
            ],
            'delete' => [
                'success'   => 'Successfully deleted comments',
                'failed'    => 'Failed deleted comments',
            ],
            'not-found' => 'Comment not found',
            'no-access'    => 'You have no right to delete this comment'
        ]
    ],

    'bracket' => [
        'details'   => [
            'not'       => 'The tournament you were looking for could not be found',
            'schedule'  => [
                'not'       => 'Schedule is not yet available'
            ],
            'bracket'  => [
                'not'       => 'Brackets not yet available'
            ]
        ],
        'matchoverview' => [
            'not' => 'Match overview not available'
        ]
    ],

    'slider' => [
        'add'   => [
            'success'   => 'Slider uploaded successfully',
            'failed'   => 'Slider failed to upload'
        ],
        'update'   => [
            'success'   => 'Slider updated successfully',
            'failed'   => 'Slider failed to update'
        ]
    ],

    'throttle' => 'Too many access attempts. Please try again in :seconds seconds.',

    'block' => [
        'error' => 'An error occurred, please try again later'
    ],

    "ticket" => [
        'info' => 'Total Number of Tickets per Tab',
        'not' => "You Don't Have a Ticket",
        'not-found' => 'Ticket Not Found',
        'period' => [
            "Daily" => 'Daily',
            'Weekly' => "Weekly",
            'Monthly' => "Monthly",
        ],
        "claims" => [
            "not-found" => "Ticket not found",
            "reached" => [
                "has"     => "Failed to claim ticket, slot :ticket full. Maximum slots :amount ticket",
                "claim"   => "Failed to claim ticket, slot :ticket full. Maximum claim :amount ticket :period",
                "given"   => "This ticket has reached the maximum claim",
            ],

        ]
    ],

];
