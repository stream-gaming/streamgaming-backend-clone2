<?php

/**
 * The lang message resource for team endpoint
 */

 return [
     'name' => [
         'limit' => 'Please change the team name after :days days again'
     ],
     'invitation_code' => [
         'expired' => 'validity period has expired',
         'not_expired' => 'there is still an active code'
     ],
     'slot_availability' => [
        'full' => 'The team slot is full',
     ],
     'not_have_related_game' => 'You dont have related game',
     'join_tournament' => 'This team is still on ongoing tournament',
     'has_team' => 'You already have team for this game',
     'join_success' => 'Success join to the team',
     'user_join_accepted' => 'Qualified to join on this team',
 ];
