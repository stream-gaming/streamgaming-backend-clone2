<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset'     => 'Your password has been reset!',
    'sent'      => "We've sent an email with a link to reset your password!",
    
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that email address.",

    'sent' => [
        'success' => 'We have emailed your password reset link!',
        'error' => 'There was an error sending password reset email.'
    ],

    'reset' => [
        'success' => 'Your password has been reset!',
        'error'   => 'There was an error resetting the password',
        'invalid' => 'This password reset token is invalid. Please try again'
    ],



];
