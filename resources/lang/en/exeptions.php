<?php

return [
    'not_allowed'       => 'Invalid API.',
    'model_not_found'   => 'Resource item not found.',
    'http_not_found'    => 'Resource not found.',
    'validation'        => 'The given data was invalid.',
];
