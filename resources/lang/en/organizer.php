<?php

/**
 * the return message for contact us
 */
return [
    "success" => "Your response has been sent, Thank you",
    "error"   => [
        "429"   => "An error occurred, please try again later",
        "422"   => "The data provided is invalid"
    ],
    "required" => [
        "manager_tour"      => "Please select your destination"
    ],
    "regex" => [
        "phone_number"      => "Invalid phone number",
    ],
    "min" => [
        "phone_number"      => "Phone number must be at least 9 characters"
    ]
];
