<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Identitas tersebut tidak cocok dengan data kami.',
    'throttle' => 'Terlalu banyak percobaan masuk. Silahkan coba lagi dalam :seconds detik.',

    'login' => [
        'not'           => 'Email Tidak Terdaftar.',
        'verify'        => 'Silahkan Verifikasi Email.',
        'inactive'      => 'Akun Tidak Aktif.',
        'incorrect'     => 'Password Salah.',
        'failed'        => 'Login Gagal.'
    ],

    'change-password' => [
        'current'       => 'Password lama salah!!.',
        'similar'       => 'Password baru tidak boleh sama dengan password lama.',
        'success'       => 'Berhasil ganti password.',
        'failed'        => 'Gagal ganti password.'
    ],

    'register' => [
        'already'       => 'Email Sudah Terdaftar.',
        'success'       => 'Pendaftaran User Baru Berhasil.',
        'failed'        => 'Pendaftaran User Baru Gagal!',
    ],

    'logout' => [
        'success'       => 'User Berhasil Keluar.',
        'failed'        => 'Maaf, User tidak dapat keluar!',
    ],

    'refresh' => [
        'token' => [
            'invalid'           => 'Token tidak valid. Silahkan login. :code',
            'expired'           => 'Sesi Kadaluarsa. :code',
            'unauthenticated'   => 'Tidak diautentikasi. :code',
            'failed'            => 'Token tidak dapat disegarkan, silakan Login lagi :code',
            'blacklist'         => 'Token telah masuk daftar hitam'

        ]
    ]

];
