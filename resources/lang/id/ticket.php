<?php

return [
    'from buying' => 'Pembelian tiket berhasil',
    'tour payment' => 'Pendaftaran turnamen menggunakan tiket berhasil',
    'from refund' => 'Anda menerima pengembalian tiket',
    'tour gift' => 'Anda menerima hadiah tiket',
    'ticket_limit' => 'Gagal melakukan pembelian tiket dikarenakan jumlah tiket tim Anda sudah mencapai limit.',
    'not enough_ticket' => 'Tiket Anda tidak cukup',
    'not_enough_ticket_solo' => 'Gagal melakukan pendaftaran turnamen dikarenakan Anda belum memiliki tiket!',
    'not_enough_ticket_team' => 'Gagal melakukan pendaftaran turnamen dikarenakan tim Anda belum memiliki tiket!',
    'expired_ticket' => 'Tiket tidak valid atau sudah kadaluarsa!',
    'xendit_payment' => 'Pembelian :ticket_name sejumlah :quantity unit',
];
