<?php

return [

	'team'	=> [
		'join'		=> [
			'request'	=> '<b>:Username</b> meminta untuk bergabung dengan tim anda.',
			'accept'	=> 'Permintaan anda untuk bergabung dengan tim <b>:team</b> telah disetujui.',
			'decline'	=> 'Permintaan anda untuk bergabung dengan tim <b>:team</b> ditolak.'
		],

		'invite'	=> [
			'request'	=> 'Tim <b>:Team</b> mengundang anda untuk bergabung.',
			'accept'	=> '<b>:Username</b> menerima undangan untuk bergabung dengan Tim anda',
			'decline'	=> '<b>:Username</b> menolak undangan untuk bergabung dengan Tim anda'
		],
		'leave'		=> [
			'new-leader'	=> 'Pemimpin tim <b>:Team</b> sekarang diberikan kepada anda.',
	    	'members'		=> 'Pemimpin anda telah meninggalkan tim, kepemimpinan diberikan kepada <b>:username</b>',
	    	'member'		=> '<b>:Username</b> telah meninggalkan tim.'
		],
		'kick'		=> 'Kamu telah dikeluarkan dari tim <b>:Team</b>',
		'banned' => 'Tim anda <b>:team</b> telah dibanned, :reason sampai :until',
		'unbanned'	=> 'Masa banned tim <b>:team</b> telah berakhir, kini anda dapat gunakan fiturnya'
    ],
    'game'  => [
        'banned'    => 'Akun game <b>:game</b> anda diban sampai <b>:time</b> dengan alasan <b>:reason</b>',
        'unbanned'  => 'Masa banned game <b>:game</b> telah berakhir, kini anda dapat gunakan fiturnya'
    ],
    'claim'     => [
        'ticket'    => 'Kamu telah mendapatkan :ticket'
    ],
    'ticket' => [
        'purchase' => 'Berhasil melakukan pembelian :ticket_name sejumlah :unit unit sebesar :prefix :amount :payment_method',
        'purchase_xendit_invoice' => 'Anda baru saja membuat invoice untuk pembelian tiket :ticket_name dengan jumlah pembayaran Rp. :amount. Id invoice Anda adalah :invoice_id.',
        'purchase_xendit_invoice_paid' => 'kami telah menerima pembayaran Anda untuk invoice dengan id :invoice_id. Tiket telah ditambahkan ke inventori tiket Anda. Terima kasih!'
    ],
    'comment' => [
        'reply' => ':sender_name membalas komentar Anda pada turnamen :tour_name',
        'delete' => ':sender_name telah menghapus komentar Anda pada turnamen :tour_name'
    ],
    'bonus-refferal' => 'Kamu telah mendapatkan :amount :type dari Bonus Refferal'

];
