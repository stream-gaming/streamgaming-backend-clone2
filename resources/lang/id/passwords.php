<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset'     => 'Kata sandi Anda sudah direset!',
    'sent'      => 'Kami sudah mengirim email yang berisi tautan untuk mereset kata sandi Anda!',
    'throttled' => 'Harap tunggu sebelum mencoba lagi.',
    'token'     => 'Token pengaturan ulang kata sandi tidak sah.',
    'user'      => 'Kami tidak dapat menemukan pengguna dengan alamat email tersebut.',

    'sent' => [
        'success' => 'Kami telah mengirimkan email tautan pengaturan ulang kata sandi Anda!',
        'error'   => 'Terjadi kesalahan saat mengirim email pengaturan ulang kata sandi.'
    ],

    'reset' => [
        'success' => 'Kata sandi Anda telah disetel ulang!',
        'error'   => 'Terjadi kesalahan saat menyetel ulang sandi',
        'invalid'   => 'Token penyetelan ulang sandi ini tidak valid. Silakan coba lagi'
    ],

];
