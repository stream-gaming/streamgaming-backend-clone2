<?php
return [
    'success' => [
        'finish_onboarding' => 'Selamat! Anda baru saja menyelesaikan proses onboarding kami.
                                Untuk bergabung di turnamen, Anda harus mengklaim tiket terlebih dahulu
                                dengan cara mengklik notifikasi ini'
    ],
    'failed' => [
        'preference' => 'Gagal menambahkan preferensi',
    ]
];
