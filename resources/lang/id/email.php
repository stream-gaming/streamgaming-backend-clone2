<?php

return [

    'throttle' => 'Terlalu Banyak Permintaan. Silahkan Coba lagi dalam :seconds detik.',

    'expired' => "Link Kadaluarsa.",
    'invalid' => "Url Invalid.",
    'already' => "Link Sudah Digunakan.",
    'success' => "Berhasil Verifikasi Email.",

    'resend' => [
        'sent' => 'Verifikasi Email Sudah di kirim ke email.',
        'already' => "Email Sudah di Verifikasi"
    ],

    'check' => [
        'has' => "Email Sudah Verifikasi",
        'not' => "Email Belum terverifikasi"
    ],

    'notify' => [
        'error' => "Whoops! ",
        'greeting' => "Haloo! ",
        'visit' => "Kunjungi Profil Perusahaan Kami",
        'click' => "Klik Disini",
        'annoying' => "Jika email ini mengganggu, silahkan  ",
        'unsubscribe' => "unsubscribe",
        'button' => [
            'verify' => "Konfirmasi Email",
            'forget' => "Reset Password"
        ],
        'verify' =>  [
            'subject' => 'Silahkan Verifikasi Email Anda',
            'title' => 'VERIFIKASI EMAIL ANDA',
            'one' => "Untuk mengkonfirmasi akun baru Anda di Stream Gaming, klik tombol dibawah ini. Jika Anda tidak mendaftarkan akun ini, beri tahu kami.",
            'two' => "Dengan mengonfirmasi akun Anda, Anda menerima",
            'three' => "Ketentuan Layanan dan Kebijakan Privasi",
            'expired' => "Link kadaluarsa dalam :count menit.",
        ],
        'forget' =>  [
            'subject' => 'Stream Gaming Reset Password',
            'title' => 'BERMASALAH LOGIN?',
            'one' => "Anda telah melakukan permintaan untuk mereset password Akun Stream Gaming Anda. Link reset hanya akan valid jika dibuka di browser dan perangkat yang sama saat melakukan permintaan reset. Untuk melanjutkan prosesnya klik tombol dibawah ini.",
            'expired' => "Link kadaluarsa dalam :count menit.",
        ],
        'trouble' => [
            'click' => "Jika Anda tidak dapat mengklik tombol di atas, salin dan tempel link di bawah ini : ",
        ],
    ],

];
