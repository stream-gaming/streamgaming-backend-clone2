<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tournament Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'status' => [
        'incoming'              => 'akan datang',
        'open_registration'     => 'pendaftaran dibuka',
        'close_registration'    => 'pendaftaran ditutup',
        'in_progress'           => 'sedang berlangsung',
        'complete'              => 'selesai'
    ],
    'page' => [
        'title'         => 'Turnamen',
        'description'   => 'Turnamen langsung, sorotan pertandingan, dan pemain pro favorit Anda semuanya di satu tempat'
    ],
    'tabname' => [
        'all'       => 'semua',
        'thisweek'  => 'minggu ini',
        'upcoming'  => 'akan datang',
        'finished'  => 'selesai'
    ]
];
