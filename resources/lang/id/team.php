<?php

/**
 * The lang message resource for team endpoint
 */

 return [
     'name' => [
         'limit' => 'Silahkan ubah nama tim setelah :days hari lagi'
     ],
     'invitation_code' => [
         'expired' => 'Masa berlaku sudah habis',
         'not_expired' => 'Masih memiliki kode aktif'
     ],
     'slot_availability' => [
        'full' => 'Slot tim sudah penuh',
     ],
     'not_have_related_game' => 'Anda belum menambahkan game',
     'join_tournament' => 'Tim ini sedang mengikuti turnamen yang sedang berlangsung',
     'has_team' => 'Anda sudah memiliki tim pada game ini',
     'join_success' => 'Berhasil bergabung ke dalam tim',
     'user_join_accepted' => 'Memenuhi syarat untuk bergabung dengan tim ini',
 ];
