<?php

/**
 * the return message for contact us
 */
return [
    "success" => "Tanggapan Anda telah terkirim, Terima kasih",
    "error"   => [
        "429"   => "Terjadi kesalahan, coba lagi nanti",
        "422"   => "Data yang diberikan tidak valid"
    ],
    "required" => [
        "manager_tour"      => "Silahkan pilih tujuan anda"
    ],
    "regex" => [
        "phone_number"      => "Format nomor telepon tidak valid",
    ],
    "min" => [
        "phone_number"      => "Nomor telepon minimal berisi 9 karakter"
    ]






];
