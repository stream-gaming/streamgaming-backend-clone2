<?php

return [
    'not_allowed'       => 'API tidak valid.',
    'model_not_found'   => 'Item sumber daya tidak ditemukan.',
    'http_not_found'    => 'Sumber tidak ditemukan.',
    'validation'        => 'Data yang diberikan tidak valid.',
];
