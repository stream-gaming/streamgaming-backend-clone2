<?php

use Illuminate\Auth\Events\Failed;

return [

	'failed'   => 'Identitas tersebut tidak cocok dengan data kami.',
	'throttle' => 'Terlalu banyak percobaan masuk. Silahkan coba lagi dalam :seconds detik.',

	'delete' => [
		'success'   => 'Pesan berhasil dihapus',
		'failed'    => 'Pesan gagal dihapus',
	],

	'data' => [
		'add'    => [
			'success'    => 'Berhasil Tambah Data :data',
			'failed'    => 'Tambah Data :data Gagal!',
		],
		'edit'    => [
			'success'    => 'Berhasil Edit Data :data',
			'failed'    => 'Edit Data :data Gagal!',
		],
	],

	'game' => [
		'not'	=> 'Permainan tidak ditemukan',
		'not-has'	=> 'Belum menambahkan permainan ini',
		'has' 	=> 'Permainan ini sudah ditambahkan',
		'used'	=> 'ID dan Nickname Permainan sudah digunakan',
		'add'	=> [
			'success'	=> 'Berhasil menghubungkan akun permainan',
			'failed'	=> 'Gagal menghubungkan akun permainan',
		],
		'edit'	=> [
			'success'	=> 'Berhasil mengubah akun permainan',
			'failed'	=> 'Gagal mengubah akun permainan',
		],
		'banned'    => 'Akses ditolak, akun game sedang dibanned!'
	],

	'news'	=> [
		'not'	=> 'Berita tidak ditemukan',
		'like'	=> [
			'has'	=> 'Kamu sudah menyukai berita ini',
			'not'	=> 'Kamu belum menyukai berita ini',
			'post'	=> [
				'success'	=> 'Berhasil menyukai berita ini',
				'failed'	=> 'Gagal menyukai berita ini',
			]
		]
	],

	'comment' => [
		'not'	=> 'Komentar tidak ditemukan',
		'add'	=> [
			'success'	=> 'Berhasil mengirimkan komentar',
			'failed'	=> 'Gagal mengirimkan komentar',
		]
	],

	'socmed' => [
		'not'       => 'Sosial media tidak ditemukan',
		'failed'    => 'Gagal mendapatkan sosial media',
		'add'    => [
			'failed'    => 'Gagal tambah sosial media',
			'success'    => 'Berhasil tambah sosial media',
		],
		'edit'    => [
			'failed'    => 'Gagal ubah sosial media',
			'success'    => 'Berhasil ubah sosial media',
		],
		'delete'    => [
			'failed'    => 'Gagal hapus sosial media',
			'success'    => 'Berhasil hapus sosial media',
		],

	],

	'user' => [
		'not'   => 'User tidak ditemukan',
		'bio'    => [
			'failed'    => 'Gagal ubah biodata',
			'success'    => 'Berhasil ubah biodata',
		],
	],

	'balance' => [
		'not'   => 'Balance tidak ditemukan',
		'cash' => [
			'not-enough'   => 'Uang tidak cukup'
		],
		'point' => [
			'not-enough'   => 'Point tidak cukup'
		],
	],

	'notif' => [
		'not'   => 'Notif tidak ditemukan',
	],

	'player' => [
		'not'	=> 'Kamu bukan player'
	],

	'social' => [
		'get'    => [
			'success'    => 'Berhasil Ambil Data User.',
			'failed'    => 'Ambil Data User Gagal  .',
		],
		'login'    => [
			'failed'    => 'Silahkan Login Manual',
		],
		'register'    => [
			'success'    => 'Registrasi User Berhasil',
			'failed'    => 'Registrasi User Gagal!',
		],
		'denied'   => 'Akses Ditolak, Coba Lagi!',
	],

	'tournament' => [
		'not'   => 'Turnamen tidak ditemukan',
		'match'	=> 'Pertandingan',
		'total'	=> 'Total',
		'most_kill'	=> 'Kill terbanyak',
		'stage' => 'Stage',
		'group_final' => 'Grup Final',
		'final' => 'Final',
		'group' => 'Grup',
		'not-open' => 'Pendaftaran ditutup!!',
		'slot-full' => 'Gagal melakukan pendaftaran tournament dikarenakan slot sudah penuh!',
		'cash' => [
			'payment' => [
				'failed' => 'Sudah Melakukan Pembayaran!'
			]
		],
		'notification' => [
			'leader-pay' => 'Pengajuan tim pada tournament :tournament_name berhasil, tim Anda akan terdaftar pada tournament tersebut setelah seluruh member tim Anda melunasi pembayaran',
			'paid'  => 'Anda telah melakukan pembayaran untuk tournament :tournament_name sebesar :fee',
			'member-pay' => 'Ketua tim Anda telah melakukan pengajuan pendaftaran pada tournament :tournament_name harap melunasi pembayaran sebesar :fee',
			'success-join' => 'Tim Anda telah terdaftar pada tournament :tournament_name'

		],
		'is_registered' => 'Akses ditolak, tim yang bersangkutan sedang mengikuti turnamen!',
		'not-found' => 'Turnamen tidak ditemukan',
		'discussion' => [
			'not-active' => 'Room diskusi turnamen tidak aktif',
		],
		'not-leader' => "Anda bukan Leader",
		'not-member' => "Anda bukan anggota dari Team yang telah join",
	],

	'payment' => [
		'not'   => 'Pembayaran tidak ditemukan',
		'success'   => 'Pembayaran turnamen berhasil',
		'failed'   => 'Pembayaran turnamen gagal',
		'lunas'   => 'Sudah Dibayar',
		'belum-lunas'  => 'Belum Dibayar',
		'cancelled'  => 'Dibatalkan',
	],

	'team'	=> [
		'not'	=> 'Tim tidak ditemukan',
		'not-member'	=> 'Anda bukan anggota tim ini',
		'not-member-other'	=> 'Pengguna bukan anggota tim ini',
		'has' 	=> 'Sudah memiliki tim untuk permainan ini',
		'full'	=> 'Tim sudah penuh',
		'leader'	=> 'Kamu bukan pemimpin tim',
		'add'	=> [
			'success'	=> 'Tim berhasil dibuat',
			'failed'	=> 'Tim gagal dibuat',
		],
		'top'	=> [
			'failed'	=> 'Top Tim Tidak ada',
		],
		'edit'	=> [
			'success'	=> 'Data tim berhasil diubah',
			'failed'	=> 'Data tim gagal diubah',
			'role'		=> 'Daftar anggota tim berhasil diubah',
			'leader'	=> 'Pemimpin tim berhasil diubah'
		],
		'join'	=> [
			'request'	=> [
				'success'	=> 'Permintaan bergabung berhasil terkirim',
				'failed'	=> 'Permintaan bergabung gagal terkirim',
				'has'		=> 'Menunggu konfirmasi dari pemimpin tim',
				'not'		=> 'Permintaan tidak ditemukan'
			],
			'accept'	=> 'Permintaan bergabung berhasil disetujui',
			'decline'	=> 'Permintaan bergabung berhasil ditolak',
		],
		'invite'	=> [
			'request'	=> [
				'success'	=> 'Undangan bergabung berhasil terkirim',
				'failed'	=> 'Undangan bergabung gagal terkirim',
				'has'		=> 'Menunggu konfirmasi dari pengguna',
				'not'		=> 'Undangan tidak ditemukan'
			],
			'accept'	=> 'Undangan bergabung berhasil disetujui',
			'decline'	=> 'Undangan bergabung berhasil ditolak',
		],
		'leave'	=> 'Anda berhasil keluar dari tim',
		'kick'	=> 'Anggota tim berhasil dikeluarkan',
		'denied'	=> 'Akses ditolak, coba lagi!',
		'banned'	=> 'Akses ditolak, tim telah di ban',
		'leader-cannot-substitute'  => 'Pemimpin tidak boleh menjadi pemain cadangan',
		'total-not-same'    => 'Total pemain tidak sesuai'
	],

	'notification'	=> [
		'markAsRead'	=> 'Berhasil'
	],

	'comment' => [
		'tournament' => [
			'create' => [
				'success' => 'Berhasil tambah komentar',
				'failed' => 'Gagal tambah komentar',
			],
			'delete' => [
				'success' => 'Berhasil hapus komentar',
				'failed' => 'Gagal hapus komentar',
			],
			'not-found' => 'Komentar tidak ditemukan',
			'no-access'	=> 'Anda tidak berhak menghapus komentar ini'
		]
	],

	'bracket' => [
		'details'   => [
			'not'       => 'Tournament yang anda cari tidak ditemukan',
			'schedule'  => [
				'not'       => 'Schedule belum tersedia'
			],
			'bracket'  => [
				'not'       => 'Bracket belum tersedia'
			]
		],
		'matchoverview' => [
			'not' => 'Match overview tidak tersedia'
		]
	],

	'slider' => [
		'add'   => [
			'success'   => 'Slider berhasil diupload',
			'failed'   => 'Slider gagal diupload'
		],
		'update'   => [
			'success'   => 'Slider berhasil diperbarui',
			'failed'   => 'Slider gagal diperbarui'
		]
	],

	'throttle' => 'Terlalu banyak percobaan akses. Silahkan coba lagi dalam :seconds detik.',

	'block' => [
		'error' => 'Terjadi Kesalahan, coba beberapa saat lagi'
	],

	"ticket" => [
		'info' => 'Jumlah Total Tiket per Tab',
		'not' => 'Anda Belum Mempunyai Tiket',
		'not-found' => 'Tiket Tidak Ditemukan',
		'period' => [
			"Daily" => 'perhari',
			'Weekly' => "perminggu",
			'Monthly' => "perbulan",
		],
		"claims" => [
			"not-found" => "Tiket tidak ditemukan",
			"reached" => [
				"has"     => "Gagal klaim tiket, slot :ticket penuh. Maksimal slot :amount tiket",
				"claim"   => "Gagal klaim tiket, slot :ticket penuh. Maksimal klaim :amount tiket :period",
				"given"   => "Tiket ini sudah mencapai maksimal klaim",
			],

		]
	],

];
