<tr>
    <td class="header">
    </td>
</tr>
<tr>
    <td class="body" width="100%" cellpadding="0" cellspacing="0"
        style="background-color:  #1BD75E;border-bottom:none;border-top:none;">
        <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
            style="background-color:#4B3B60;border-radius: 10px 10px 0 0;">
            <!-- Body content -->
            <tr>
                <td class="content-cell">
                    <h1
                        style="background-color:#6A3BC0;font-family:'Roboto Condensed', sans-serif;font-size: 30px; font-weight: 700;color:#ffffff;font-style:italic;text-transform:uppercase;padding:10px 20px;border-radius:15px;text-align: center;margin: 10px 0;">
                        {{ $title ?? '' }}</h1>
                    <img src="{{ config('app.cm_apps') . 'v1/assets/images/esports/streamlogo12.png' }}" alt=""
                        style="margin: 0 0 -80px;">
                </td>
            </tr>
        </table>
    </td>
</tr>
