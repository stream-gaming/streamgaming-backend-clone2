<tr>
    <td bgcolor="#2B2236" align="center" style="padding: 30px 10px 0px 10px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 570px;" >
            <tr>
                <td bgcolor="#6A3BC0" align="center" style="
                padding: 30px 30px 30px 30px;  text-align:center; color: #ffffff; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                    <h2 style="text-align: center;font-size: 20px; color: #ffffff; margin: 0;">@lang('email.notify.visit')</h2>
                    <p style="text-align: center;margin: 0;"><a href="https://stream-universe.id/" style="color: #00FF5B;">@lang('email.notify.click')</a></p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td bgcolor="#2B2236" align="center" style="padding: 0px 10px 0px 10px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 570px;">
            <tr>
                <td bgcolor="#2B2236" align="left" style=" text-align:center; padding: 0px 30px 30px 30px; color: #ffffff; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;"> <br>
                    <p style="margin: 0;text-align: center;"text-align: center;>@lang('email.notify.annoying') <a href="javascript:void(0);" style="color: #00FF5B; font-weight: 700;">@lang('email.notify.unsubscribe')</a>.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td style="background-color: #2B2236;">
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>