<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="color: #fff">
    <tr>
        <td>
            {{ Illuminate\Mail\Markdown::parse($slot) }}
        </td>
    </tr>
</table>