<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Benchmark'], function () {
    Route::get('team',          'TeamController@index');
    Route::get('tournament',    'TournamentController@index');
    Route::get('game',          'GameController@index');
});
