<?php

use App\Auth\Passwords\PasswordBroker;
use App\Auth\Passwords\PasswordBrokerManager;
use App\Http\Controllers\ForgotPasswordController;
use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Support\Facades\Route;
use Illuminate\Mail\Markdown;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::post('deploy', 'DeployController@deploy');

// Route::middleware('localization')->group(function () use ($router) {

	Route::get('verify', 'PreviewMailController@verify');

	Route::get('forget', 'PreviewMailController@forget');

// });

Route::get('/debug-sentry', 'DebugController@index');

