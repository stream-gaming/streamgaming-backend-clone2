<?php

use App\Http\Middleware\RefreshTokenManual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use function Clue\StreamFilter\fun;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route Auth
//- Melly
Route::middleware(['localization', 'json.response', 'save.api'])->group(function () use ($router) {
    Route::group(
        ['prefix' => 'auth'],
        function () use ($router) {
            Route::group(['prefix' =>        'social-login'], function () use ($router) {
                Route::get('provider/{provider}',          'SocialController@redirectToProvider');
                Route::get('callback/{provider}', 'SocialController@handleProviderCallback');
                Route::post('response', 'SocialController@handleFrontEndCallback');
            });
            Route::post('login',            'AuthController@login');
            Route::post('register',         'AuthController@register')->middleware('set.user.onboarding')->name('regist.manual');
            Route::post('register-social',  'SocialController@registerSocial')->middleware('set.user.onboarding')->name('regist.third.party');
            Route::group(['prefix' =>   'email'], function () use ($router) {
                Route::get('verify',        'VerificationEmailController@verify')->name('verification.verify'); //Rute Generate Verifikasi Email
                Route::get('resend',        'VerificationEmailController@resend')->name('verification.resend');
            });
            Route::group(['prefix' =>   'password'], function () use ($router) {
                Route::post('reset-request', 'ForgotPasswordController@resetPasswordRequest');
                Route::post('reset',        'ForgotPasswordController@resetPassword')->name('password.reset');
            });
            Route::post('logout', 'AuthController@logout');
            Route::post('change-password', 'AuthController@ChangePassword');
        }
    );

    Route::prefix('socmed')->group(function () {
        Route::middleware('jwt')->group(function () {
            Route::get('/', 'SocmedController@getSocmed');
            Route::post('/', 'SocmedController@create');
            Route::get('/{id}', 'SocmedController@read');
            Route::put('edit', 'SocmedController@update');
            Route::delete('delete', 'SocmedController@delete');
        });
    });
    Route::namespace('Api')->group(function () {

        Route::prefix('game')->group(function () {
            Route::get('/',         'GameController@getAll');
            Route::get('all',       'GameController@getAllGame');
            Route::get('static',    'GameController@getStaticGame');
            Route::get('simple',    'GameController@getSimple');
            Route::get('role/{url}',    'GameController@getRole');
            Route::get('{url}',     'GameController@getDetail')->name('game.detail');
            Route::get('onboarding/{game_url}',    'GameController@getDetailOnboarding')->name('game.detail.onboarding');

        Route::middleware('jwt', 'players', 'is.join.tour', 'team.banned')->group(function () {
                Route::post('add/{id}',     'GameController@addGame')->middleware('add.game.onboarding')->name('add.game');
                Route::put('edit/{id}',     'GameController@editGame');
            });
        });

        Route::prefix('news')->group(function () {
            Route::get('/',         'NewsController@getAll')->name('news'); //wajib
            Route::get('simple',    'NewsController@getSimple');
            Route::get('latest',    'NewsController@latest');
            Route::get('most-comment',    'NewsController@most_comment');
            Route::get('popular',   'NewsController@popular');
            Route::get('{url}',     'NewsController@getDetail')->name('news.detail');

            Route::prefix('comment')->group(function () {
                Route::get('{news_id}',             'NewsController@getComment');
                Route::post('post/{news_id}',       'NewsController@postComment');
            });

            Route::middleware('jwt')->group(function () {
                Route::post('like/{news_id}',       'NewsController@likePost');
                Route::post('unlike/{news_id}',     'NewsController@unlikePost');
            });
        });

        Route::group(['prefix' => 'team', 'namespace' => 'Team'], function () {
            Route::get('join/user/validation/{team}', 'TeamV3Controller@validateUserJoin')
                    ->middleware('jwt')
                    ->name('team.join.user.validation');
            Route::get('/',             'TeamController@getAll')->name('team');
            Route::get('detail/{url}/{type?}',  'TeamController@getDetail')->name('team.detail');
            Route::get('v2/detail/{url}',  'TeamV2Controller@getDetailTeam')->name('team.detail.v2');

            Route::middleware('jwt', 'players')->group(function () {
                Route::post('create',  'TeamCreateController@create')->middleware('create.team.onboarding')->name('create.team');
                Route::post('join/invitation-code/specific',  'TeamV3Controller@joinUsingInvCodeSpecific')
                ->middleware('team.banned.inv.code', 'create.team.onboarding')->name('join.team.inv.code.specific');
                Route::post('join/invitation-code',  'TeamV3Controller@joinUsingInvCode')
                ->middleware('team.banned.inv.code', 'create.team.onboarding')->name('join.team.inv.code');

                Route::get('invitation-code/{team}', 'InvitationCodeController@getInvitationCode');
                Route::post('invitation-code/generate',      'InvitationCodeController@generateInvitationCode');

                Route::middleware('team.banned')->group(function () {

                    Route::post('join/{team_id}',       'TeamRequestJoinController@join');

                    Route::get('user/search/{team_id}',     'TeamSearchUserInviteController@searchUser');
                    Route::post('logo/{team_id?}',           'TeamSettingsController@changeLogo');

                    Route::middleware('is.join.tour')->group(function() {
                        Route::prefix('request')->group(function () {
                            Route::post('accepted/{req_id}',   'TeamRequestAcceptController@acceptJoin');
                            Route::post('declined/{req_id}',   'TeamRequestDeclineController@declineJoin');
                        });

                        Route::post('invite',                   'TeamInviteRequestController@invite');
                        Route::prefix('invite')->group(function () {
                            Route::post('accepted/{inv_id}',   'TeamInviteAcceptController@acceptInvite');
                            Route::post('declined/{inv_id}',   'TeamInviteDeclineController@declineInvite');
                        });

                        Route::put('setting/{team_id?}',        'TeamSettingsController@setting');
                        Route::prefix('change')->group(function () {
                            Route::put('role/{team_id}',       'TeamSettingsController@changeRole');
                            Route::put('leader/{team_id}',     'TeamSettingsController@changeLeader');
                        });
                        Route::delete('leave/{team_id}',        'TeamLeaveController@leave');
                        Route::delete('kick/{team_id}',         'TeamKickController@kick');
                    });
                });
            });
            Route::get('top/{limit?}',             'TopTeamController@getAllTeam')->name('team.top');
        });
        Route::group(['prefix' => 'Redis', 'namespace' => 'Redis','middleware' => 'portal.auth'], function () {
            Route::get('getData/{key}',                 'RedisInPortal@getData');
            Route::post('setData',                 'RedisInPortal@setData');
            Route::post('destroyData',             'RedisInPortal@destroyData');
        });

        Route::group(['prefix' => 'notification', 'middleware' => 'jwt'], function () {
            Route::get('/',                 'NotificationController@getAll');
            Route::get('read',                 'NotificationController@getRead');
            Route::post('mark-as-read',        'NotificationController@markAsRead');
            Route::post('read/{id_notification}',        'NotificationController@readNotif');
            Route::delete('delete/{id_notification}',        'NotificationController@deleteNotif');
        });

        Route::get('city',          'AddressController@city');
        Route::get('province',      'AddressController@province');
        Route::get('check-email/{email}',   'CheckValidEmailController@check');

        Route::get('sponsorship',   'SponsorshipController@get');

        Route::group([
            'prefix'        => 'friend',
            'middleware'    => ['jwt', 'players'],
            'namespace'     => 'Friendship'
        ], function () {

            Route::get('list',      'FriendshipController@listFriend');
            Route::post('add',      'FriendshipController@addFriend');
        });

        Route::group(['middleware' => 'jwt', 'namespace' => 'Tournament'], function () {
            Route::post('validate-join-tournament', 'UserJoinTournamentValidation@getValidation');
            Route::post('join-tournament', 'JoinTournamentController@join');
        });

        Route::group(['namespace' => 'Notification', 'middleware' => 'jwt'], function ()
        {
            Route::post('chat/firebase','WebPushController@saveToken');
            Route::post('notification/firebase','WebPushController@saveTokenNotif');
            Route::post('notification/firebase/web','WebPushController@saveTokenNotifWeb');
        });

        Route::group(['prefix' => 'chat'], function () {
            Route::group(['namespace' => 'ChatTeam', 'prefix' => 'team', 'middleware' => 'jwt'], function () {
                Route::get('list', 'ListMessageController@getListTeam');
                Route::get('list-team', 'ListTeamController@index');
                Route::get('fetch/{team_id}', 'ListMessageController@getMessages')->name('api.team.messages.fetch');
                Route::get('fetch/{teamId}/latest', 'FetchMessageController@latest')->name('api.team.messages.fetch.latest'); // Get Latest Message
                Route::get('fetch/{teamId}/earliest', 'FetchMessageController@earliest')->name('api.team.messages.fetch.earliest'); // Get Latest Message
                Route::post('send', 'SendMessagesController@send');
            });
            Route::group(['namespace' => 'ChatTournament', 'prefix' => 'tournament', 'middleware' => 'portal.auth'], function () {
                Route::post('fetch/{tournament_id}', 'FetchMessageController@getMessages');
                Route::get('list', 'TournamentDiscussionController@getList');
                Route::post('send', 'SendMessagesController@send');
                Route::delete('delete', 'DeleteMessagesController@destroy');
                Route::post('pin-tournament', 'TournamentDiscussionController@pinTournament');
                Route::post('unpin-tournament', 'TournamentDiscussionController@unpinTournament');

            });
            Route::group(['namespace' => 'ChatTournament', 'prefix' => 'front/tournament', 'middleware' => 'jwt'], function () {
                Route::post('fetch/{tournament_id}', 'FetchMessageController@getMessages')->name('api.tournament.messages.fetch');
                Route::get('list', 'TournamentDiscussionController@getList')->name('api.tournament.messages.list');
                Route::post('send', 'SendMessagesController@send');
                Route::delete('delete', 'DeleteMessagesController@destroy');
                Route::post('pin-tournament', 'TournamentDiscussionController@pinTournament')->name('api.tournament.pin');
                Route::post('unpin-tournament', 'TournamentDiscussionController@unpinTournament')->name('api.tournament.unpin');

            });
        });
        Route::group(['namespace' => 'Organizer'], function () {
            Route::post('send-organizer', 'OrganizerController@send');
        });




    });
    Route::group(['namespace' => 'Bracket'], function () {
        Route::post('setting-tournament', 'InsertSettingTournament@SettingTournament');
    });






    Route::group(['prefix' => 'account', 'middleware' => 'jwt'], function () {
        Route::get('profile', 'AccountController@profile');
        Route::group(['prefix' => 'onboarding', 'namespace' => 'Api'], function () {
            Route::get('check', 'OnboardingController@check');
            Route::post('set', 'OnboardingController@set');
        });
        Route::get('balance', 'AccountController@balanceUsers');
        Route::get('my-ticket/info/{type?}', 'AccountController@getMyTicket');
        Route::get('my-ticket/count', 'AccountController@getTotalMyTicket');
        Route::group(['prefix' => 'my-payment'], function () {
            Route::get('/tab/{type?}', 'AccountController@getMyPayment');
            Route::post('/pay-tournament', 'MemberPaymentController@tournament')->name('pay-tournament');
        });
        Route::get('my-game', 'AccountController@getMyGame');
        Route::group(['prefix' => 'my-team'], function () {
            Route::get('/', 'AccountController@getMyTeam');
            Route::get('/detail/{team_id}', 'AccountController@getDetailMyTeam');
            Route::get('/notif', 'AccountController@getNotifMyTeam');
        });
        Route::group(['prefix' => 'edit'], function () {
            Route::put('profile', 'AccountController@editProfile');
            Route::post('upload', 'AccountController@changeAvatar');
            Route::put('settings', 'AccountController@accountSettings');
            Route::put('preferences', 'AccountController@userPreferences');
        });


    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('overview/{username}/{version?}', 'UserController@overview')->name('user.overview');

        Route::group(['middleware' => 'portal.auth', 'namespace' => 'Admin\Users'], function () {
            Route::get('banned-history/{user_id}', 'HistoryBannedController@getListUser')->name('user.banned.history');
        });

        Route::group(['namespace' => 'Api\Player'], function () {
            Route::get('match/{username}/{version?}', 'PlayerMatchHistoryController@index')->name('user.match');
            Route::get('statistic/{username}/{version?}', 'PlayerStatisticController@index')->name('user.statistic');

            Route::group(['middleware' => 'jwt'], function () {
                Route::post('my-tournament', 'MyTournamentController@getMyTournament')->name('user.tournament');
            });
            Route::group(['middleware' => 'portal.auth'], function () {
                Route::post('my-tournament-portal', 'MyTournamentController@getMyTournamentPortal')->name('user.tournament.portal');
            });
        });
    });

    Route::group(['prefix' => 'team'], function () {
        Route::group(['middleware' => 'jwt', 'namespace' => 'Admin\Users'], function () {
            Route::get('banned-history/{team_id}', 'HistoryBannedController@getListTeam')->name('team.banned.history');
        });
    });

    //- Route Comments Tournament
    Route::group(['prefix' => 'comments'], function () {
        Route::group(['namespace' => 'Api\Comments'], function () {
            Route::get('list/{id_tournament}', 'TournamentCommentsController@list')->middleware([RefreshTokenManual::class]);
            // Route::get('upload', 'TournamentCommentsController@migrateComment')->middleware([RefreshTokenManual::class]);
            Route::post('list/{id_tournament}/reply/{comments_id}', 'TournamentCommentsController@moreReplies')->middleware([RefreshTokenManual::class]);
            Route::group(['middleware' => 'jwt'], function () {
                Route::post('list/{id_tournament}/add', 'TournamentCommentsController@addReply');
                Route::delete('list/{id_tournament}/delete/{comments_id}', 'TournamentCommentsController@deleteComment');
            });
        });
    });

    Route::group(['prefix' => 'comments-portal', 'middleware' => 'portal.auth'], function () {
        Route::group(['namespace' => 'Api\Comments'], function () {
            Route::get('list/{id_tournament}', 'TournamentCommentsController@list');
            Route::get('list/{id_tournament}/reply/{comments_id}', 'TournamentCommentsController@moreReplies');
            Route::post('list/{id_tournament}/add', 'TournamentCommentsController@addReply');
            Route::delete('list/{id_tournament}/delete/{comments_id}', 'TournamentCommentsController@deleteComment');

        });
    });


    //ticket testing
    Route::group(['prefix' => 'ticket'], function () {
        //- Tickets V3 *melly
        Route::group(['namespace' => 'Tickets'], function () {
            Route::group(['prefix' => 'v3'], function () {
                Route::middleware(['jwt'])->group(function () {
                    Route::get('claims/{type}', 'TicketsClaimsController@viewClaims');
                    Route::post('claims/{type}', 'TicketsClaimsController@claimTicket');
                });
                Route::middleware(['portal.auth'])->group(function () {
                    Route::post('giving/user', 'TicketsGivingController@giveTicketUser');
                    Route::post('giving/team', 'TicketsGivingController@giveTicketTeam');
                });
            });
        });
        Route::group(['namespace' => 'Api'], function () {
            Route::get('add',               'TicketServiceController@addTeamTicket');
            Route::post('get-ticket-portal', 'TicketServiceController@getUserTicket');
            Route::post('get-gift-ticket-portal', 'TicketServiceController@getGiftTicket');

            Route::group(['middleware' => 'jwt'], function () {
                Route::post('buy-ticket', 'TicketServiceController@buyTicket')->name('ticket.buy');
                Route::post('invoice/create', 'Payment\PaymentGatewayController@createInvoice')->name('invoice');
                Route::post('invoice/get', 'Payment\PaymentGatewayController@getInvoice')->name('invoice.get');
            });
            Route::post('invoice/callback', 'TicketServiceController@updateTicketInvStatus')->middleware('xendit.auth')->name('invoice.callback');

            Route::group(['middleware' => 'portal.auth'], function () {
                Route::delete('cancel-tournament/{id_tournament}', 'TicketServiceController@cancelTicketTournament')->name('cancel.tournament');
                Route::delete('cancel-tournament-bracket/{id_tournament}', 'TicketServiceController@cancelTicketTournamentBracket')->name('cancel.bracket');
                Route::post('kick-participant', 'TicketServiceController@kickParticipant')->name('kick.participant');
            });



        });

        Route::group(['namespace' => 'Tickets', 'prefix' => 'list-new'], function () {
            Route::get('user/{ticket}', 'TicketListController@listUserHaveTicketNew');
            Route::get('team/{ticket}', 'TicketListController@listTeamHaveTicketNew');
        });

        Route::group(['namespace' => 'Tickets', 'prefix' => 'list-old'], function () {
            Route::get('user/{ticket}', 'TicketListController@listUserHaveTicketOld');
            Route::get('team/{ticket}', 'TicketListController@listTeamHaveTicketOld');
        });

        Route::group(['namespace' => 'Tickets'], function () {
            Route::post('insert-ticket',                    'TicketsController@InsertTicket');
            Route::get('{ticket}',                          'TicketsController@show');
            Route::delete('delete-ticket/{ticket}',         'TicketsController@destroy');
        });

    });

    Route::group(['namespace' => 'Api', 'middleware' => 'portal.auth'], function () {
        Route::post('broadcast-ticket', 'BroadcasterController@send');
    });

    // Route::get('city',          'CityController@get');

    Route::group(['prefix' => 'tournament'], function () {
        Route::get('/', 'TournamenList@allTouranament')->name('tournament.list');
        Route::get('new', 'TournamentListNew@allTouranament')->name('tournament.list.new');
        Route::get('v3', 'TournamentListViii@frontPage')->name('tournament.list.viii');
        Route::get('v4', 'TournamentListViii@frontPageV2')->name('tournament.list.viv');
        Route::get('listv3', 'TournamentListDetailPage@allTournament')->name('tournament.list.lviii');
        Route::get('liga', 'TournamentLigaController@liga_list')->name('tournament.liga');
        Route::get('liga/{url?}', 'TournamentLigaController@show_liga')->name('tournament.list.liga');
        Route::get('{url}/{type?}', 'BracketController@getDetails')->name('bracket');
    });



    Route::get('matchoverview/{round}/{start}/{id}', 'BracketController@matchsOverview')->name('matchs.overview');

    Route::group(['prefix' => 'leaderboard'], function () {
        Route::get('{url}/{data?}/{stage?}', 'LeaderboardController@index')->name('leaderboard.stage');
        Route::get('detail/{url}/{stage?}/{group?}/{match?}', 'LeaderboardController@detailLeaderboard')->name('leaderboard.detail');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'jwt'], function () {
        Route::group(['prefix' => 'slider'], function () {
            Route::get('/',         'SliderController@index')->withoutMiddleware('jwt');
            Route::get('{slider}',  'SliderController@show');
            Route::post('create',   'SliderController@store');
            Route::post('update/{slider}',  'SliderController@update');
            Route::delete('{slider}',       'SliderController@destroy');
        });

        Route::post('get-token',     'AdminTokenController@create')->withoutMiddleware('jwt');
        Route::get('get-data',     'AdminTokenController@profile')->withoutMiddleware('jwt');

        Route::get('ban/team/list-member/{team_id}',     'BannedTeamController@getListMemberByTeamID');
        Route::post('ban/team/{team_id}',     'BannedTeamController@banTeamWithIdentityGame');

        Route::post('ban/game',       'BannedController@ban_game');
    });

    Route::get('/statuscode/{code}', 'StatusCodeController@example');
    Route::group(['prefix' => 'faqs'], function () {
        Route::get('/', 'FaqsController@index');
        Route::get('/{url}', 'FaqsController@getDetail')->name('faqs.content');
        Route::post('/search', 'FaqsController@listSearch');
    });
    // Route::get('faqs', 'FaqsController@index');
    // Route::get('faqs/{url}', 'FaqsController@getDetail')->name('faqs.content');
    // Route::post('faqs/search', 'FaqsController@store');
    Route::group(['middleware' => 'jwt','namespace' => 'Api'], function () {
        Route::post('expression_faqs', 'ExpressionFaqsController@store');
    });


    // Route::get('/firebase', 'FirebaseController@index');
    Route::group(['prefix' => 'all_invoice'], function () {
        Route::group(['namespace' => 'Api'], function () {

            Route::group(['middleware' => 'portal.auth'], function () {
                Route::get('getall',               'AllInvoiceServiceController@getAll');
                Route::post('getinvoice_tournament', 'AllInvoiceServiceController@get_invoice');
            });

        });

    });
    Route::group(['namespace' => 'Api'], function () {
        Route::group(['middleware' => 'portal.auth'], function () {
            Route::get('print_list/{id_tournament}/{round}',               'printRoundServiceController@printRound');
        });
    });



    Route::get('exist',  'CheckIfItIsUsedController');


    Route::group(['prefix'=> 'ticket-claim', 'namespace' => 'Admin\Ticket', 'middleware' => 'portal.auth'], function () {
        Route::get('data',  'TicketClaimController@data');
        Route::get('all-ticket', 'TicketClaimController@allTicket');
        Route::post('insert', 'TicketClaimController@insert');
        Route::get('edit/{id}', 'TicketClaimController@edit');
        Route::post('update/{id}', 'TicketClaimController@update');
        Route::delete('delete/{id}', 'TicketClaimController@delete');
    });

    Route::group(['prefix'=> 'ticket', 'namespace' => 'Api' ,'middleware' => 'portal.auth'], function () {
        Route::prefix('v3')->group(function () {
            Route::post('detail', 'AllTicketController@detail');
            Route::post('update/{ticket}', 'AllTicketController@edit');
        });
    });
    Route::group(['prefix'=> 'tournament-streaming', 'namespace' => 'Api' ,'middleware' => 'portal.auth'], function () {
        Route::post('insert',       'TournamentStreamingController@store');
        Route::post('update/{id}',  'TournamentStreamingController@update');
        Route::post('delete/{id}','TournamentStreamingController@delete');
    });

    Route::prefix('v2')->group(function () {
        Route::get('tournament/{url}/{type?}','BracketVerIIController@getDetails')->name('bracketv2');
    });

    Route::group(['prefix'=> 'prizepool-inventory', 'namespace' => 'Api\Tournament' ,'middleware' => 'portal.auth'], function () {
        Route::get('get-list',  'PrizepoolInventoryController@getInventoryList');
    });



    Route::post('send-contact-us', 'ContactUsController@send');

    Route::group(['prefix'=> 'notification', 'namespace' => 'Api' ,'middleware' => 'portal.auth'], function () {
        Route::get('send-notification',  'UserNotificationPortalController@sendNotif');
    });
    Route::group(['prefix'=> 'cache', 'namespace' => 'Api\CacheTrigger' ,'middleware' => 'portal.auth'], function () {
        Route::post('destroy',  'CacheTriggerController@deleteCache');
        Route::post('destroy-multi',  'CacheTriggerController@deleteCacheMulti');
    });
    Route::group(['namespace' => 'Bracket'], function () {
        Route::get('get-bracket-mvp/{id}','BracketMVPController@getFiveMVP');
        Route::post('move-first-mvp-bracket/{id}','BracketMVPController@moveToMvp');
        Route::get('get-tournament-mvp/{id}', 'BracketGetMvpController@mvpList');
    });
    Route::group(['namespace' => 'Admin\Users'],function () {
        Route::get('user/teams/{userName}', 'ListTeamUserController@getListTeamUsers');
    });

    Route::group(['namespace' => 'Admin\Statistik'],function () {
        Route::get('admin/get-total-user-have-team', 'StatistikJoinTeamController@data');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
        Route::get('user/game-idenity/{userId}','IdentityGameHistoryController@get');
    });
    Route::group(['prefix' => 'admin', 'middleware' => 'portal.auth'],function () {
        Route::get('team/history/{teamId}', 'HistoryTeamNameController@getHistory');
        Route::get('mobile/version/all/{app?}', 'MobileManagementController@index');
        Route::post('mobile/version/{app?}', 'MobileManagementController@store');
        Route::get('mobile/version/edit/{mobileVersion}', 'MobileManagementController@edit');
        Route::post('mobile/version/update/{mobileVersion}', 'MobileManagementController@update');
    });

    Route::get('mobile/version/{app?}', 'MobileManagementController@show');

    Route::get('order-history','Api\OrderHistoryController@getMyHistory');

    Route::group(['prefix' => 'onboarding', 'middleware' => 'jwt'], function () {
        Route::put('user/preference', 'PreferenceOnboardingController@update')->name('preference.update');
        Route::get('user/preference', 'PreferenceOnboardingController@get')->name('preference.get');
        Route::get('user/status', 'UserOnboardingController@getOnboardingStatus')->name('user.onboarding.status.get');
        Route::put('user/status', 'UserOnboardingController@updateOnboardingStatus')->name('user.onboarding.status.update');
        Route::put('user/tournament', 'UserOnboardingController@updateOnboardingTournamentStatus')->name('user.onboarding.tour.update');
    });

    Route::group(['prefix' => 'promotion', 'middleware' => 'jwt', 'namespace' => 'Api\UserPromotion'], function () {
        Route::get('/', 'UserPromotionController@getAllUserPromotion')->name('promotion.get.all');
        Route::get('/{promotionId}', 'UserPromotionController@getPromotionById')->name('promotion.get.detail');
        Route::put('read/{promotionId}', 'UserPromotionController@readUserPromotion')->name('promotion.read.single');
        Route::put('read', 'UserPromotionController@readAllUserPromotion')->name('promotion.read.all');
        Route::delete('delete/{promotionId}', 'UserPromotionController@deleteUserPromotion')->name('promotion.delete.single');
    });

    Route::group(['prefix' => 'push-notification', 'middleware' => 'portal.auth', 'namespace' => 'Api\Notification'], function () {
        Route::put('promotion/{promotion}', 'PromotionPushNotifController@pushNotification')->name('promotion.push.notif');
    });
});



