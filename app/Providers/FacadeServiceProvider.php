<?php

namespace App\Providers;

use App\Helpers\Builders\NotificationBuilder;
use App\Helpers\Builders\NotificationBuilderInterface;
use App\Modules\Promotion\Interfaces\UserPromotionServiceInterface;
use App\Modules\Promotion\Services\UserPromotionService;
use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserPromotionServiceInterface::class, UserPromotionService::class);
        $this->app->bind(NotificationBuilderInterface::class, NotificationBuilder::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
