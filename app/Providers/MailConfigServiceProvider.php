<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

/*
*   Providers Untuk Multiple Smtp Account
*/

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom Smtp Account
        $time = strtotime(Carbon::now()->toTimeString()); //00:00:00

        switch ($time) {
            case ($time <= strtotime('14:00')):
                $smtp = 'smtp_1';
                break;
            case ($time <= strtotime('16:00')):
                $smtp = 'smtp_2';
                break;
            case ($time <= strtotime('18:00')):
                $smtp = 'smtp_3';
                break;
            case ($time <= strtotime('22:00')):
                $smtp = 'smtp_4';
                break;
            case ($time <= strtotime('23:59')):
                $smtp = 'smtp_5';
                break;
            default:
                $smtp = 'smtp_1';
                break;
        }
        Config::set('mail.default', $smtp);
    }
}
