<?php

namespace App\Providers;

use App\Modules\FCM\Interfaces\FCMNotifTokenRepositoryInterface;
use App\Modules\FCM\Repositories\FCMNotifTokenRepository;
use App\Modules\Promotion\Interfaces\PromoPushNotifHistoryRepositoryInterface;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use App\Modules\Promotion\Interfaces\UserPromotionRepositoryInterface;
use App\Modules\Promotion\Repositories\PromoPushNotifHistoryRepository;
use App\Modules\Promotion\Repositories\PromotionRepository;
use App\Modules\Promotion\Repositories\UserPromotionRepository;
use App\Modules\Team\Interfaces\InvitationCodeRepositoryInterface;
use App\Modules\Team\Repositories\InvitationCodeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserPromotionRepositoryInterface::class, UserPromotionRepository::class);
        $this->app->bind(PromotionRepositoryInterface::class, PromotionRepository::class);
        $this->app->bind(PromoPushNotifHistoryRepositoryInterface::class, PromoPushNotifHistoryRepository::class);
        $this->app->bind(FCMNotifTokenRepositoryInterface::class, FCMNotifTokenRepository::class);
        $this->app->bind(InvitationCodeRepositoryInterface::class, InvitationCodeRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
