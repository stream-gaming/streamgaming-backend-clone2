<?php

namespace App\Providers;

use App\Helpers\Firebase\FirebaseFactory;
use App\Helpers\Firebase\FirebaseFactoryInterface;
use Illuminate\Support\ServiceProvider;

class FirebaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FirebaseFactoryInterface::class, function ($app) {
            return new FirebaseFactory();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
