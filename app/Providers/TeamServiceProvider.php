<?php

namespace App\Providers;

use App\Modules\Team\Interfaces\TeamPlayerNewRepositoryInterface;
use App\Modules\Team\Interfaces\TeamPlayerRepositoryInterface;
use App\Modules\Team\Interfaces\TeamServiceInterface;
use App\Modules\Team\Interfaces\TeamValidatorBuilderInterface;
use App\Modules\Team\Repositories\TeamPlayerNewRepository;
use App\Modules\Team\Repositories\TeamPlayerRepository;
use App\Modules\Team\Services\TeamService;
use App\Modules\Team\Validators\TeamValidatorBuilder;
use Illuminate\Support\ServiceProvider;

class TeamServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TeamValidatorBuilderInterface::class,TeamValidatorBuilder::class);
        $this->app->bind(TeamPlayerRepositoryInterface::class,TeamPlayerRepository::class);
        $this->app->bind(TeamServiceInterface::class,TeamService::class);
        $this->app->bind(TeamPlayerNewRepositoryInterface::class, TeamPlayerNewRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
