<?php

namespace App\Providers;

use App\Http\Middleware\RefreshToken;
use App\Http\Middleware\RefreshTokenManual;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Broadcast::routes();
        Broadcast::routes(["middleware" => [RefreshToken::class]]);

        require base_path('routes/channels.php');
    }
}
