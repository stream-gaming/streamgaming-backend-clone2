<?php

namespace App\Traits;

trait CheckImageTraits
{
    /**
     * Check, is Image Exists
     * @param string $path
     * @param enum $type_account [user, team]
     * @return string path_image
     */
    public function isImageExists($path = '', $type_account = 'user')
    {
        switch ($type_account) {
            case 'user':
                $link_image = config('app.aws_s3_url')."/website/private/user.png";
                break;

            case 'team':
                //- Add your default image team if image is broken
                break;

            default:
                $link_image = config('app.aws_s3_url')."/website/private/streamgaming.png";
                break;
        }

        $cek_image = @fopen($path, "r");
 
        return $cek_image ? $path : $link_image;
    }
}
