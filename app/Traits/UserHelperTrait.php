<?php

namespace App\Traits;

use App\Model\Management;
use App\Model\ManagementDetailModel;
use App\Model\User;
use App\Model\UsersDetailModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

trait UserHelperTrait
{ 
    function createUser()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        UsersDetailModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return $this->createAdditionalUser();
    }

    function createAdditionalUser()
    {
        $user = factory(User::class)->make();
        $user->save();
        $user->detail()->save(factory(UsersDetailModel::class)->make());
        return $user;
    } 
    
    function createAdditionalAdmin()
    {
        $admin = factory(Management::class)->make();
        $admin->save();
        $admin->detail()->save(factory(ManagementDetailModel::class)->make());
        return $admin;
    } 

    function createAccessToken(User $user = null)
    {
        $access_token = JWTAuth::attempt(['email' => $user->email, 'password' => 'admin']);
        return $access_token;
    }
    
    function createAdmin()
    {
        Management::truncate();
        ManagementDetailModel::truncate();
        return $this->createAdditionalAdmin();
    }
}