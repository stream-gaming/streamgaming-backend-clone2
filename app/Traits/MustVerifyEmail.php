<?php

namespace App\Traits;

use App\Model\SendMailLogModel;
use App\Notifications\VerifyEmail;

trait MustVerifyEmail
{
    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return ($this->is_verified != 0);
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'is_verified' => 1,
            'status' => 'player',
        ])->save();
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotifications($status = null)
    {
        //Tambah Log
        $data = [
            'email' => $this->email,
            'type' => 'verify'
        ];
        (new SendMailLogModel())->addLogMail($data);

        $this->notify(new VerifyEmail($status));
    }

    /**
     * Get the email address that should be used for verification.
     *
     * @return string
     */
    public function getEmailForVerification()
    {
        return $this->email;
    }
}
