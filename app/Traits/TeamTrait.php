<?php

namespace App\Traits;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Repositories\TeamPlayerNewRepository;

trait TeamTrait{

    public function destroyCacheForNewPlayer(TeamModel $team, User $user, TeamPlayerNewRepository $teamPlayerNewRepository){
        //Destroy cache
        RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
        RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);
        RedisHelper::destroyWithPattern('my:team:user_id:' . $team->leader_id . ':');
        RedisHelper::destroyWithPattern('my:team:user_id:' . $user->user_id . ':');
        RedisHelper::destroyWithPattern('team:list:page:');

        //Destroy cache for new team player
        if(config('app.team_player_new')){
            $teamPlayer = $teamPlayerNewRepository->getAllTeamPlayer($team->team_id)->pluck('player_id')->all();
            RedisHelper::destroyDataWithArray('my:team:user_id', $teamPlayer);
        }
    }

}
