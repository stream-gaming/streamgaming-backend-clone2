<?php

namespace App\Traits;

use App\Helpers\Ticket\TicketRole;
use App\Model\TicketsModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait TicketClaimsTrait
{
    public function getHas($type, $id, $ticket)
    {
        switch ($type) {
            case 'user':
                $ticket_has = TicketsModel::where('user_id', $id)->where('is_used', 0);
                break;

            case 'team':
                $ticket_has = TicketsModel::where('team_id', $id)->where('is_used', 0);
                break;
        }

        return $ticket_has->where('ticket_id', $ticket->ticket_id);
    }

    //- fungsi untuk build query awal berdasarkan tipe
    function getClaim($type, $ticket = null, $team_id = null)
    {
        switch ($type) {
            case 'user':
                $this->has_claim = TicketsModel::join('history_tickets', function ($q) {
                    $q->on('history_tickets.ticket_id', '=', 'tickets.id');
                })
                    ->where("history_tickets.source", TicketRole::CLAIMS_SOURCE)
                    ->where('tickets.user_id', Auth::user()->user_id);

                $this->has_claim_junk = TicketsModel::join('history_tickets', function ($q) {
                    $q->on('history_tickets.ticket_id', '=', 'tickets.id');
                })
                    ->where("history_tickets.source", TicketRole::CLAIMS_SOURCE)
                    ->where('tickets.user_id', Auth::user()->user_id);

                break;

            case 'team':
                $this->has_claim = TicketsModel::join('history_tickets', function ($q) {
                    $q->on('history_tickets.ticket_id', '=', 'tickets.id');
                })
                    ->where("history_tickets.source", TicketRole::CLAIMS_SOURCE)
                    ->where("tickets.team_id", $team_id);

                $this->has_claim_junk = TicketsModel::join('history_tickets', function ($q) {
                    $q->on('history_tickets.ticket_id', '=', 'tickets.id');
                })
                    ->where("history_tickets.source", TicketRole::CLAIMS_SOURCE)
                    ->where("tickets.team_id", $team_id);

                break;
        }

        return $this->hasClaim($ticket);
    }

    //- Return Boolean, Untuk pengecekan Ticket Junk
    function cekJunk($ticket, $dayBefore = 1)
    {
        $now  = new Carbon();
        $bool = false;

        if ($ticket->event_start < $now->now()->format("Y-m-d")) {
            switch ($ticket->period) {
                case 'Daily':
                    $bool = true;
                    break;
                case 'Weekly':
                    if ($now->now()->subDays(7)->endOfWeek()->addDay($dayBefore)->format("Y-m-d") == $now->format("Y-m-d")) {
                        $bool = true;
                    }
                    break;
                case 'Monthly':
                    $date = new Carbon('last day of last month');
                    if ($date->endOfMonth()->addDay($dayBefore)->format("Y-m-d") == $now->format("Y-m-d")) {
                        $bool = true;
                    }
                    break;
            }
        }
        return $bool;
    }

    //- Untuk mengecek apakah User sudah Klaim Tiket sesuai periode
    function hasClaim($ticket)
    {
        $now = new Carbon();

        switch ($ticket->period) {
            case 'Daily':
                $has_claim = $this->has_claim->whereDate("tickets.created_at", $now);
                break;
            case 'Weekly':
                $has_claim = $this->has_claim->whereBetween("tickets.created_at", [$now->now()->startOfWeek(), $now->now()->endOfWeek()]);
                break;
            case 'Monthly':
                $has_claim = $this->has_claim->whereBetween("tickets.created_at", [$now->now()->startOfMonth(), $now->now()->endOfMonth()]);
                break;
        }

        return $has_claim->where('tickets.ticket_id', $ticket->ticket_id)->count();
    }

    //- Untuk mengecek apakah User sudah Klaim tiket yang batas klaim sudah lewat
    function hasClaimJunk($ticket)
    {
        $now = new Carbon();

        switch ($ticket->period) {
            case 'Daily':
                $has_claim_junk = $this->has_claim_junk->whereDate("tickets.created_at", $now->yesterday());
                break;
            case 'Weekly':
                $has_claim_junk = $this->has_claim_junk->whereBetween("tickets.created_at", [$now->now()->subDays(7)->startOfWeek(), $now->now()->subDays(7)->endOfWeek()]);
                break;
            case 'Monthly':
                $date = new Carbon('first day of last month');
                $has_claim_junk = $this->has_claim_junk->whereBetween("tickets.created_at", [$date->startOfMonth()->format('Y-m-d H:i'), $date->endOfMonth()->format('Y-m-d H:i')]);
                break;
        }

        return $has_claim_junk->where('tickets.ticket_id', $ticket->ticket_id)->count();
    }
}
//keep
