<?php

namespace App\Traits;

trait TournamentMessagesTraits
{
    use CheckImageTraits;
    public function getImage($detail, $type_account = '0')
    {
        switch ($type_account) {
            case '0':
                if (!empty($detail->link_image) and empty($detail->image)) {
                    return $this->isImageExists($detail->link_image);
					// return $detail->link_image;
                } else if (!empty($detail->image) and !empty($detail->link_image)) {
                    return $this->isImageExists($this->MyApps->cdn([
                        'user_id'    => $detail->user_id,
                        'image'      => $detail->image
                    ], 'encrypt', 'users'));
                } else if (!empty($detail->image) and empty($detail->link_image)) {
                    return $this->isImageExists($this->MyApps->cdn([
                        'user_id'    => $detail->user_id,
                        'image'      => $detail->image
                    ], 'encrypt', 'users'));
                } else {
                    return config('app.cm_apps') . 'private/user.png';
                }
                break;

            case '1':
                if (!empty($detail->image)) {
                    return $this->MyApps->cdn([
                        'user_id'    => $detail->access_id,
                        'image'      => $detail->image
                    ], 'encrypt', 'users');
                } else {
                    return config('app.cm_apps') . 'private/user.png';
                }
        }
    }
}
//keep
