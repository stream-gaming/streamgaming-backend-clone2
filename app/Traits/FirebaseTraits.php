<?php

namespace App\Traits;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

trait FirebaseTraits
{
    function firebase()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
        return (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()
            ->getDatabase();
    }
}
