<?php

namespace App\Traits;

use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use Exception;

trait GameTraits
{
    public function createArrayGameRole($game_role_id, $id_game)
    {
        $array_game_role = [];
        foreach ($game_role_id as $m) {
            if (GameRoleModel::where('id', $m)->where('game_id', $id_game)->exists()) {
                $array_game_role[] = new GameUsersRoleModel([
                    'game_role_id' => $m
                ]);
            } else {
                throw new Exception(trans("message.game.add.role.not-found"), 404);
            }
        }
        return $array_game_role;
    }
}
//keep
