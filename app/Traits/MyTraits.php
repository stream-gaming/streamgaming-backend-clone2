<?php

namespace App\Traits;

trait MyTraits
{
    use CheckImageTraits;
    
    public function getImage($detail, $type_account = '0')
    {
        switch ($type_account) {
            case '0':
                if (!empty($detail->link_image) && empty($detail->image)) {
                    return $this->isImageExists($detail->link_image);
					// return $detail->link_image;
                } else if (!empty($detail->image) && !empty($detail->link_image)) {
                    return $this->isImageExists($this->MyApps->cdn([
                        'user_id'    => $detail->user_id,
                        'image'      => $detail->image
                    ], 'encrypt', 'users'));
                } else if (!empty($detail->image) && empty($detail->link_image)) {
                    return $this->isImageExists($this->MyApps->cdn([
                        'user_id'    => $detail->user_id,
                        'image'      => $detail->image
                    ], 'encrypt', 'users'));
                } else {
                    return config('app.cm_apps') . 'private/user.png';
                }
                break;

            case '1':
                return config('app.aws_s3_url')."/website/private/streamgaming.png";
        }
    }

    function paginateCollection($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (\Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
        return new \Illuminate\Pagination\LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
//keep
