<?php

namespace App\Jobs;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CacheTriggerPortalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $cacheKey;
    public $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cacheKey, $type)
    {
        //
        $this->cacheKey = $cacheKey;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        try{
            switch($this->type){
                case 'single':
                    $destroyCache =  RedisHelper::destroyData($this->cacheKey);
                break;
                case 'multi':
                    foreach($this->cacheKey as $key){
                        $destroyCache =  RedisHelper::destroyData($key);
                    }
                break;
                case 'pattern':
                    $destroyCache =  RedisHelper::destroyWithPattern($this->cacheKey);
                break;

            }

        }catch(\Exception $e){
            Log::debug($e->getMessage());
        }

    }
}
