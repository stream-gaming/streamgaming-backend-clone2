<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use App\Model\IdentityGameModel;
use App\Helpers\Redis\RedisHelper;
use Illuminate\Support\Facades\DB;
use App\Helpers\UpdateStatusBanned;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Helpers\Validation\CheckTeam;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Api\UserNotificationController;

class CheckStatusSoloBanned implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        App::setLocale('id');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $result = (new UpdateStatusBanned)->solo();
            return $result;

            Log::info([
                'status'    => $result,
                'message'   => 'Job executed successfully'
            ]);
        } catch (\Exception $e) {
            Log::error([
                'error'    => [
                    'message' => $e->getMessage(),
                    'file'     => $e->getFile(),
                    'line'     => $e->getLine(),
                    'trace'    => $e->getTrace()
                ]
            ]);
        }
    }

}
