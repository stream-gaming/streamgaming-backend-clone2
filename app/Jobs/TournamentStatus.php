<?php

namespace App\Jobs;

use App\Model\LeaderboardModel;
use App\Model\PlayerPayment;
use App\Model\TournamentModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\CacheTriggerPortalJob;

class TournamentStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
                //cache trigger
                CacheTriggerPortalJob::dispatch('tournament:list:','pattern');

                //Leaderboard
                $getLeaderboard = LeaderboardModel::select('id_leaderboard','status_tournament','regist_start','regist_end','tournament_date','prizepool_type','tournament_category')
                ->whereRaw('status_tournament < 4')->get();

                //check and update leaderboard status
                foreach($getLeaderboard as $leaderboard):

                    if($leaderboard->status_tournament == 0):

                        $waktu = strtotime($leaderboard->regist_start);

                    if(strtotime(date('F d Y H:i')) >= $waktu):
                        $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                            ->update(['status_tournament' => 1]);
                    endif;

                    elseif($leaderboard->status_tournament == 1):
                        $waktu = strtotime($leaderboard->regist_end);

                    if(strtotime(date('F d Y H:i')) >= $waktu):
                        $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                            ->update(['status_tournament' => 2]);
                    endif;

                    //incase user's registration prizepool
                    if($leaderboard->prizepool_type == 'User_Registration'):

                    $getPrizepool = PlayerPayment::where('id_tournament', $leaderboard->id_leaderboard)
                            ->where('status', 'Lunas')
                            ->groupBy('id_tournament')
                            ->sum('price_of_payment');

                    if($getPrizepool):

                        $prizePool = $getPrizepool - ((10/100) * $getPrizepool);
                        $firstprize = (50/100) * $prizePool;
                        $secondprize = (30/100) * $prizePool;
                        $thirdprize = (20/100) * $prizePool;


                    else:
                        $firstprize = 0;
                        $secondprize = 0;
                        $thirdprize = 0;

                    endif;

                        $prizePoolFinal = [
                        "firstprize" => $firstprize,
                        "secondprize" => $secondprize,
                        "thirdprize" => $thirdprize
                        ];
                        $updatePrizePool = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                    ->update($prizePoolFinal);

                    endif;

                    elseif($leaderboard->status_tournament == 2 AND $leaderboard->tournament_category != 3):
                        $waktu = strtotime($leaderboard->tournament_date);

                        if(strtotime(date('F d Y H:i')) >= $waktu):
                            $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                            ->update(['status_tournament' => 3]);
                        endif;

                    endif;
                endforeach;



                //Bracket
                $getBracket = TournamentModel::select('id_tournament','status','start_registration','end_registration','date')
                ->where('status','!=', 'complete')->get();

                foreach($getBracket as $bracket):

                    if($bracket['status'] == 'pending'):
                        $waktu = strtotime($bracket->start_registration);

                        if(strtotime(date('F d Y H:i')) >= $waktu):
                            $updateStatusTournament = TournamentModel::where('id_tournament', $bracket->id_tournament)
                                        ->update(['status' => 'open_registration']);
                        endif;
                    elseif($bracket['status'] == 'open_registration'):
                        $waktu = strtotime($bracket->end_registration);

                        if(strtotime(date('F d Y H:i')) >= $waktu):
                            $updateStatusTournament = TournamentModel::where('id_tournament', $bracket->id_tournament)
                                        ->update(['status' => 'end_registration']);
                        endif;
                    endif;

                endforeach;

        }catch(\Exception $e){
                //cache trigger
                CacheTriggerPortalJob::dispatch('tournament:list:','pattern');
        }



    }
}
