<?php

namespace App\Jobs;

use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TournamentFilledSlot implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $getLeaderboard = LeaderboardModel::select('id_leaderboard','is_no_qualification','status_tournament','regist_start','regist_end','tournament_date','prizepool_type','tournament_category')
        ->get();

        foreach($getLeaderboard as $leaderboard){
            if($leaderboard->tournament_category == 3){
                $count = LeaderboardTeamModel::where('id_leaderboard', $leaderboard->id_leaderboard)
                ->where('qualification_sequence', '1')
                ->count();

                $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                                                ->update(['filled_slot' => $count]);

            }else if($leaderboard->tournament_category == 2 AND $leaderboard->is_no_qualification  != 1){

                $count = LeaderboardTeamModel::where('id_leaderboard', $leaderboard->id_leaderboard)
                ->where('qualification_sequence', '0')
                ->count();

                $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                                                ->update(['filled_slot' => $count]);

            }else if($leaderboard->tournament_category == 2 AND $leaderboard->is_no_qualification  == 1){
                $count = LeaderboardTeamModel::where('id_leaderboard', $leaderboard->id_leaderboard)
                ->where('qualification_sequence', '0')
                ->count();

                $updateStatusTournament = LeaderboardModel::where('id_leaderboard',$leaderboard->id_leaderboard)
                                                                ->update(['filled_slot' => $count]);
            }
        }

        $tournament = TournamentModel::selectRaw('tournament.id_group')
        ->where('status','!=','canceled')
        ->get();
        foreach($tournament AS $id_group){
            $tournament_group = (new TournamenGroupModel)->countFilledSlots($id_group->id_group);
            TournamentModel::where('id_group',$id_group->id_group)
                            ->update(['filled_slot' => $tournament_group]);
        }


    }
}
