<?php

namespace App\Jobs;

use App\Model\AllTicketModel;
use App\Model\ApiLogModel;
use App\Model\TicketsModel;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AllTicketTotalGiven implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     * Job Untuk mengupdate total given sesuai dengan yang telah diberikan
     * @return void
     */
    public function handle()
    {
        $all_ticket = AllTicketModel::where('expired', '>=', Carbon::now()->format('Y-m-d'))->get();

        foreach ($all_ticket as $ticket) {
            $ticket->total_given = TicketsModel::where('ticket_id', $ticket->id)->count();
            $ticket->save();
        }
    }
}
