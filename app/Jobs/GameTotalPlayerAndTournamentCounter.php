<?php

namespace App\Jobs;

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\TournamentModel;
use App\Model\UsersGameModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GameTotalPlayerAndTournamentCounter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        try{

            $getGame = GameModel::get(['id_game_list','name','system_kompetisi']);

            if(!empty($getGame)){
                foreach($getGame as $game){
                    $totalPlayer = IdentityGameModel::where('identity_ingame.game_id','=', $game->id_game_list)
                                                      ->count();

                    $totalTournament = 0;

                    if($game->system_kompetisi == "Bracket"){
                        $getBracket = TournamentModel::where('tournament.kompetisi','=',$game->id_game_list)
                                        ->where('tournament.deleted_at','=', null)
                                        ->count();

                        $totalTournament += $getBracket;

                    }elseif($game->system_kompetisi == "LeaderBoards"){
                        $getLeaderboard = LeaderboardModel::where('leaderboard.game_competition', '=', $game->id_game_list)
                                            ->where('leaderboard.deleted_at','=', null)
                                            ->count();
                        $totalTournament += $getLeaderboard;

                    }elseif($game->system_kompetisi == "Multisystem"){
                        $getBracket = TournamentModel::where('tournament.kompetisi','=',$game->id_game_list)
                                        ->where('tournament.deleted_at','=', null)
                                        ->count();
                        $totalTournament += $getBracket;

                        $getLeaderboard = LeaderboardModel::where('leaderboard.game_competition', '=', $game->id_game_list)
                                            ->where('leaderboard.deleted_at','=', null)
                                            ->count();
                        $totalTournament += $getLeaderboard;

                    }


                    GameModel::where('id_game_list','=',$game->id_game_list)

                               ->update(['total_player' => $totalPlayer, 'total_tournament' => $totalTournament]);

                }
            }

            //cache trigger
            CacheTriggerPortalJob::dispatch('game--all','single');
            //end of cache trigger
        }catch(\Exception $e){
            Log::debug($e);
        }



    }
}
