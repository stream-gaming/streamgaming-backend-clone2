<?php

namespace App\Jobs;

use App\Helpers\Notification;
use App\Helpers\Ticket\TeamTicketFactory;
use App\Helpers\Ticket\TicketRole;
use App\Helpers\Ticket\TicketValidation;
use App\Helpers\Ticket\UserTicketFactory;
use App\Model\TicketsModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class TicketBroadcaster implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $userOrTeamData;
    private $gameOption;
    private $ticket_id;
    private $unit;
    private $maxHas;
    private $ticketName;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userOrTeamData, $gameOption, $ticket_id, $unit, $maxHas, $ticketName)
    {
        //
        $this->userOrTeamData = $userOrTeamData;
        $this->gameOption = $gameOption;
        $this->ticket_id = $ticket_id;
        $this->unit = $unit;
        $this->maxHas = $maxHas;
        $this->ticketName = $ticketName;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        try{
            //broadcast ticket for solo
            if($this->gameOption == 1){
                $factory = new UserTicketFactory;
                $notification = new Notification();

                    $getUserTicket = TicketsModel::where('user_id', $this->userOrTeamData->user_id)
                                                  ->where('ticket_id', $this->ticket_id)
                                                  ->count();
                    $allowedUnit = $this->maxHas - $getUserTicket;
                    $givenUnit = $allowedUnit >= $this->unit ? $this->unit : $allowedUnit;

                    $factory->addTicket($this->userOrTeamData->user_id, $this->ticket_id, TicketRole::ADMINGIFT_SOURCE, $givenUnit);

                    $link = 'myaccount';
                    $message = 'Selamat Anda mendapatkan tiket '.$this->ticketName.' sejumlah '.$givenUnit.' unit dari Stream Gaming';
                    $notification->singleNotification($this->userOrTeamData->user_id, $link, $message);
            //broadcast ticket for team
            }else{
                $factory = new TeamTicketFactory;
                $notification = new Notification();

                    $getTeamTicket = TicketsModel::where('team_id', $this->userOrTeamData->team_id)
                                                ->where('ticket_id', $this->ticket_id)
                                                ->count();
                    $allowedUnit = $this->maxHas - $getTeamTicket;
                    $givenUnit = $allowedUnit >= $this->unit ? $this->unit : $allowedUnit;


                    $factory->addTicket($this->userOrTeamData->team_id, $this->ticket_id, TicketRole::ADMINGIFT_SOURCE, $givenUnit);

                    $link = 'myaccount';
                    $message = 'Selamat tim Anda mendapatkan tiket '.$this->ticketName.' sejumlah '.$givenUnit.' unit dari Stream Gaming';
                    $notification->singleNotification($this->userOrTeamData->leader_id, $link, $message);


            }

        }catch(\Exception $e){
            Log::debug($e);
        }


    }
}
