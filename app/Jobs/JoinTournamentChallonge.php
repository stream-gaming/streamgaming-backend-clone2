<?php

namespace App\Jobs;

use App\Helpers\InsertParticipantChallonge;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class JoinTournamentChallonge implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $tournament;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tournament, $data)
    {
        //
        $this->tournament = $tournament;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        InsertParticipantChallonge::InsertParticipant($this->tournament, $this->data);
    }
}
