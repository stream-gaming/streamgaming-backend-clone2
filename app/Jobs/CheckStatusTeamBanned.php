<?php

namespace App\Jobs;

use App\Helpers\Redis\RedisHelper;
use App\Helpers\UpdateStatusBanned;
use Carbon\Carbon;
use App\Model\TeamModel;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Api\UserNotificationController;

class CheckStatusTeamBanned implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        App::setLocale('id');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $result = (new UpdateStatusBanned)->team();
            return $result;

            Log::info([
                'status'    => $result,
                'message'   => 'Job executed successfully'
            ]);
        } catch (\Exception $e) {
            Log::error([
                'error'    => [
                    'message' => "Something error in job : -" . $e->getMessage(),
                    'file'     => $e->getFile(),
                    'line'     => $e->getLine(),
                    'trace'    => $e->getTrace()
                ]
            ]);
        }
    }
}
