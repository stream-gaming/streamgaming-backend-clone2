<?php

namespace App\Rules;

use App\Model\MobileVersion;
use App\Modules\MobileManagement\MobileAppId;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Lang;

class MobileVersionRule implements Rule
{
    protected $device;
    protected $appId;
    protected $attribute;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->device = request()->segment(5,'android');

        $this->appId = (new MobileAppId)->get($this->device);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mobile = MobileVersion::where('appId',$this->appId);

        if ($attribute == 'name') {
            $this->attribute = 'versionName';
            $mobile = $mobile->where('versionName', $value);
        }

        if ($attribute == 'code') {
            $this->attribute = 'versionCode';
            $mobile = $mobile->where('versionCode', $value);
        }

        $mobile = $mobile->count();

        return $mobile == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Lang::get('validation.unique', ['attribute' => $this->attribute]);
    }
}
