<?php

namespace App\Rules;

use App\Modules\Chat\TimeModeEnum;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class TimeStampCheck implements Rule
{
    protected $timeMode;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Carbon::createFromTimestamp($value)->lessThan(Carbon::now());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':Attribute harus kurang dari waktu saat ini';
    }
}
