<?php

namespace App\Rules;

use App\Modules\Team\Interfaces\InvitationCodeRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;

class InvCodeValidation implements Rule
{
    protected $invCode;
    protected $teamId;
    protected $invCodeRepo;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->invCode = request('invitation_code');
        $this->teamId = request('team_id');
        $this->invCodeRepo = App::make(InvitationCodeRepositoryInterface::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !is_null($this->invCodeRepo->findByCodeAndTeamId($this->invCode, $this->teamId));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return Lang::get('exeptions.validation');
    }
}
