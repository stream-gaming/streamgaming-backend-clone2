<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\AllTournamentModel;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\TeamRequestModel;
use App\Model\TournamentModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class MyPaymentTransform extends TransformerAbstract
{

    public function transform($payment)
    {
        $team             = TeamModel::find($payment->id_team);
        $tournament_type  = AllTournamentModel::where('id_tournament', $payment->id_tournament)->first()->format;
        $my_apps          = (new MyApps);

        $list_payment = [
            'id_payment'            => $payment->id_player_payment,
            'team'        => [
                'team_id'       => (string) $team->team_id,
                'team_name'     => $team->team_name,

            ],
            'price'        => $payment->price_of_payment,
            'amount'       => $payment->amount,
            'status'       => $payment->status,
            'status_payment'=>$my_apps->getStatusPayment($payment->status),
            'type_payment' => $payment->type_payment,
            'date_payment' => ($payment->date_payment) ? date('Y-m-d H:i', strtotime($payment->date_payment)) : NULL,
            'refund_date'  => ($payment->refund_date) ? date('Y-m-d H:i', strtotime($payment->refund_date)) : NULL,
            'order_date'   => Carbon::parse($payment->created_at),
            'url'          => $my_apps->getPathRoute('pay-tournament'),

        ];

        switch ($tournament_type) {
            case 'Leaderboard':
                $tournament = LeaderboardModel::find($payment->id_tournament);
                break;
            case 'Bracket':
                $tournament = TournamentModel::find($payment->id_tournament);
                break;
        }

        $list_tournament = [
            'tournament'        => [
                'id_tournament'     => $payment->id_tournament,
                'tournament_name'   => $tournament->tournament_name,
                'tournament_type'   => $tournament_type
            ],
        ];

        return array_merge($list_payment, $list_tournament);
    }
}
