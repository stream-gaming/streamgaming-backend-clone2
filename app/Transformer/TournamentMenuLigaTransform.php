<?php

namespace App\Transformer;

use App\Model\TournamentMenuLiga;
use League\Fractal\TransformerAbstract;


class TournamentMenuLigaTransform extends TransformerAbstract
{

    public function transform(TournamentMenuLiga $data)
    {
        return [
            "title"       => $data->nama_menu_liga,
            "description" => $data->deskripsi_menu_liga
        ];
    }
}
