<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TournamentLiga;
use League\Fractal\TransformerAbstract;

class TournamentLigaNewTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TournamentLiga $data)
    {
        $myapps = new MyApps();

        return [
            "title" => $data->nama_liga,
            "type" => strtolower($data->game->system_kompetisi),
            "total_prize" => (int) $data->jumlah_hadiah,
            "total_slot" => (int) $data->slot_tersedia,
            "image" => $data->background_liga,
            "date_start" => date('Y-m-d', strtotime($data->start_tour)),
            "date_end" => date('Y-m-d', strtotime($data->end_tour)),
            "url_page"=> $myapps->getPathRoute('tournament.list.liga',['url' => $data->url]),
            "organizer" => [
                "name" => $data->nama_penyelenggara,
                "logo" => $data->logo_penyelenggara
            ],
            "game"  => [
                "id" => $data->game->id_game_list,
                "name" => $data->game->name,
                "logo" => $myapps->cdn($data->game->logo, 'encrypt', 'game'),
            ]
        ];
    }
}
