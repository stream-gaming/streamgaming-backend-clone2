<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\LeaderboardTeamModel;

use League\Fractal\TransformerAbstract;

class ParticipantSoloTransform extends TransformerAbstract
{


	public function transform(LeaderboardTeamModel $leaderboard_team)
	{



            //participant transformer for solo tournament
            return [
                'team_logo' 	=> (!empty($leaderboard_team->image) ? (new MyApps)->cdn(['user_id' => $leaderboard_team->id_team_leaderboard,'image' => $leaderboard_team->image], 'encrypt', 'users') : (new MyApps)->DefaultUserPath()),
                'team_name' 	=> $leaderboard_team->nama_team,
                'venue' 		=> (($leaderboard_team->kota != null OR !empty($leaderboard_team->kota)) ? $leaderboard_team->kota : '--' ),
                'team_id' 		=> $leaderboard_team->id_team_leaderboard
            ];


    }



}
