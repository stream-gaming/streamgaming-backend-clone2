<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use App\Model\UsersDetailModel;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TournamenBracketSoloTransform extends TransformerAbstract
{

    private $myApps;
    public function __construct()
    {
        $this->myApps = new MyApps;
    }
	public function transform(TournamenGroupModel $team)
    {
        $user = User::with('detail')->where('user_id',$team->id_team)->first() ;
        $result = [
            'team_id'   => (string) $team->id_team,
            'team_name' => $team->nama_team,
            'city'      => $team->nama_kota,
            'logo'      => $this->myApps->getImage($team->id_team),
            'url'       => '/overview/'.$user->username
        ];

        return $result;
    }




}
