<?php

namespace App\Transformer;

use App\Model\BracketScoreMatchs;
use App\Model\HeroMobileLegendsModel;
use App\Model\IdentityGameModel;
use App\Model\ItemMobileLegend;
use App\Model\TournamenGroupModel;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Str;

class ScoreMatchsTransform extends TransformerAbstract
{
    public function transform(BracketScoreMatchs $data)
    {
        $item       = ItemMobileLegend::whereIn('id_gear',[$data->item1,$data->item2,$data->item3,$data->item4,$data->item5,$data->item6])->get();
        $Items      = new Collection($item,(new ItemListTransform));
        $itemDatas  = (new Manager)->createData($Items)->toArray();

        $username   = IdentityGameModel::where('users_id',$data->player_id)->first();
        $hero       = HeroMobileLegendsModel::where('id_hero',$data->hero)->firstOrFail();

        $teamName   = TournamenGroupModel::where('id_team','=',$data->id_team)->first();
        $result     = [
            'username'  => $username->username_ingame,
            'hero'      => [
                'hero_name' => $hero->nama_hero,
                'role'      => $hero->role,
                'image'     => $hero->url_image
            ],
            'kda'       => [
                'kills'     => $data->kills,
                'death'     => $data->death,
                'assist'    => $data->assist,
            ],
            'item'      => current($itemDatas),
            'is_mvp'    => $data->mvp,
            'team_name' => $teamName->nama_team,
            'index'     => str_replace(' ','_',Str::lower($teamName->nama_team))
        ];

        return $result;
    }
}
