<?php

namespace App\Transformer;

use App\Model\PromotionModel;
use League\Fractal\TransformerAbstract;

class UserPromotionTransformer extends TransformerAbstract
{

	public function transform(PromotionModel $userPromotion)
	{
        return [
            'promotion_id' => $userPromotion->id,
            'title' => $userPromotion->title,
            'is_read' => isset($userPromotion->action_read)? true:false,
            'base_url' => empty($userPromotion->link) ? null : $userPromotion->link,
            'created_at' => date('Y-m-d H:i:s', strtotime($userPromotion->created_at)),
        ];
    }

}
