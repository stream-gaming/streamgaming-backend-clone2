<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use App\Model\TournamentListModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use League\Fractal\TransformerAbstract;

class MyTournamentTransform extends TransformerAbstract
{
    private $asset;
    private $date;
    private $leaderboardModel;

    public function __construct()
    {

        $this->asset = new MyApps;
        $this->date =  new Carbon;
        $this->leaderboardModel = new LeaderboardModel;

    }

    public function transform($data){

            return ($data->getLeaderboard != null ? $this->leaderboard($data) : ($this->bracket($data) != null ? $this->bracket($data) : [] ));


    }

    private function leaderboard($data){

        try{

            $data->getLeaderboard->card_banner = empty($data->getLeaderboard->card_banner) ?
            $this->asset->cdn($data->getLeaderboard->logo_game_list, 'encrypt', 'game') :
            $this->asset->cdn($data->getLeaderboard->card_banner, 'encrypt', 'banner_tournament');

        }catch(\Exception $e){
            Log::debug($e);
            $data->getLeaderboard->card_banner = "";
        }


        //Tournament status conversion
        $tournamentStatus =(object) $this->leaderboardModel->tournamentStatus($data->getLeaderboard->status_tournament);
        $total_team = $data->getLeaderboard->tournament_category == 3 ? $data->total_team : $data->getLeaderboard->total_team;

        //Prizepool aggregation
        if($data->getLeaderboard->prize_type == 'dynamic'){
            $prizepool = $data->getLeaderboard->dynamic_prize_leaderboard + $data->getLeaderboard->most_kill;
        }else{
            //check tournament form type
            if($data->getLeaderboard->tournament_form == 'pay'){
                if($data->getLeaderboard->status_tournament >= 2 ){
                    $prizepool = ($data->getLeaderboard->tournament_fee * $data->getLeaderboard->filled_slot) - (($data->getLeaderboard->tournament_fee * $data->getLeaderboard->filled_slot) * (10/100));
                }else{
                    $prizepool = ($data->getLeaderboard->tournament_fee * $total_team) - (($data->getLeaderboard->tournament_fee * $total_team) * (10/100)).' UP TO';
                }
            }else{

                $prizepool = $data->getLeaderboard->firstprize + $data->getLeaderboard->secondprize + $data->getLeaderboard->thirdprize + $data->getLeaderboard->most_kill;
            }
        }


        $result = [
            'title'         => $data->getLeaderboard->tournament_name,
            'card_banner'   => $data->getLeaderboard->card_banner,
            'is_free'       => $data->getLeaderboard->tournament_form == 'free' ? true : false,
            'match_start'   => date('Y-m-d H:i', strtotime($data->getLeaderboard->tournament_date)),
            'total_prize'   => $prizepool,
            'hosted_by'     => strtoupper($data->getLeaderboard->organizer_name),
            'match_info'    => [
                'game'      => strtoupper($data->getLeaderboard->name),
                'game_on'   => $this->gamesOn($data->getLeaderboard->games_on),
                'type'      => 'LEADERBOARD',
                'mode'      => $data->getLeaderboard->game_option == 1 ? 'SOLO' : ($data->getLeaderboard->game_option == 2 ? 'DUO' : 'SQUAD')
            ],
            'status'        => [
                'text'      => $data->getLeaderboard->filled_slot == $total_team ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $data->getLeaderboard->filled_slot == $total_team ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                ],
            'slots'         => [
                'filled'    =>  $data->getLeaderboard->filled_slot,
                'available' => $total_team,
                'slotstatus'=> ($data->getLeaderboard->filled_slot > 0 ? (100 / $total_team) * $data->getLeaderboard->filled_slot : $total_team)
            ],
            'url'           => $data->getLeaderboard->url,
            'full_url'      => $this->asset->getPathRoute('leaderboard.stage', ['url' => $data->getLeaderboard->url]),
            'game_url'      => '-',
            'create_at'     => date('d-m-Y h:i', strtotime($data->getLeaderboard->input_date)),
            'time_value'    => strtotime($data->getLeaderboard->tournament_date),
            'time'          => ($data->getLeaderboard->status_tournament < 4 AND $data->getLeaderboard->status_tournament != 5) == true ? 'on_going' : 'finished'
        ];

        return $result;

    }

    private function bracket($data){

        try{
            $data->getBracket->banner_tournament = empty($data->getBracket->banner_tournament) ?
            $this->asset->cdn($data->getBracket->logo_game_list, 'encrypt', 'game') :
            $this->asset->cdn($data->getBracket->banner_tournament, 'encrypt', 'banner_tournament');
        }catch(\Exception $e){
            Log::debug($e);
        }



        $prize =   explode(',',$data->getBracket->total_prize);
        $prizepool = array_sum($prize);


        //Tournament status conversion
        if($data->getBracket->status == 'pending'){
            $tourStatus = 0;
        }elseif($data->getBracket->status == 'open_registration'){
            $tourStatus = 1;

        }elseif($data->getBracket->status == 'end_registration'){
            $tourStatus = 2;

        }elseif($data->getBracket->status == 'starting'){
            $tourStatus = 3;

        }elseif($data->getBracket->status == 'complete'){
            $tourStatus = 4;
        }
        elseif($data->getBracket->status == 'canceled'){
            $tourStatus = 5;
        }

        $tournamentStatus =(object) $this->leaderboardModel->tournamentStatus($tourStatus);

        $result = [
            'title'         => $data->getBracket->tournament_name,
            'card_banner'   => $data->getBracket->banner_tournament,
            'is_free'       => $data->getBracket->system_payment == 'Free' ? true : false,
            'match_start'   => date('Y-m-d H:i', strtotime($data->getBracket->date)),
            'total_prize'   => $prizepool,
            'hosted_by'     => strtoupper($data->getBracket->organizer_name),
            'match_info'    => [
                'game'      => strtoupper($data->getBracket->name),
                'game_on'   => $this->gamesOn($data->getBracket->games_on),
                'type'      => 'BRACKET',
                'mode'      => $data->getBracket->game_option == 1 ? 'SOLO' : ($data->game_option == 2 ? 'DUO' : 'SQUAD')
            ],
            'status'        => [
                'text'      => $data->getBracket->filled_slot == $data->getBracket->total_team ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $data->getBracket->filled_slot == $data->getBracket->total_team  ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                ],
            'slots'         => [
                'filled'    => ($data->getBracket->filled_slot == null ? '0' : $data->getBracket->filled_slot) ,
                'available' => $data->getBracket->total_team,
                'slotstatus'=> ($data->getBracket->filled_slot > 0 ? (100 / $data->getBracket->total_team) * $data->getBracket->filled_slot : $data->getBracket->total_team)
            ],
            'url'           => $data->getBracket->url,
            'full_url'      => $this->asset->getPathRoute('bracket', ['url' => $data->getBracket->url]),
            'game_url'      => '',
            'create_at'     => date('d-m-Y h:i', strtotime($data->getBracket->created_at)),
            'time_value'    => strtotime($data->getBracket->date),
            'time'          => (($data->getBracket->status != 'complete' AND $data->getBracket->status != 'canceled') ? 'on_going' : 'finished')
        ];

        return $result;

    }

    private function gamesOn($games_on)
    {
        switch ($games_on) {
            case 1:
                return 'PC';
                break;
            case 2:
                return 'Mobile';
                break;
            case 3:
                return 'Console';
                break;
            default:
                return $games_on;
        }
    }
}
