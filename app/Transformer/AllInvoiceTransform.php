<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\User;
use App\Model\TeamModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use App\Model\TournamentListModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use League\Fractal\TransformerAbstract;

class AllInvoiceTransform extends TransformerAbstract
{
    private $asset;
    private $date;
    private $leaderboardModel;

    public function __construct()
    {

        $this->asset = new MyApps;
        $this->date =  new Carbon;
        $this->leaderboardModel = new LeaderboardModel;

    }

    public function transform($data){
            // return  $data;
            return ($data->getLeaderboard == null ? $this->bracket($data) : $this->leaderboard($data) );
    }

    private function leaderboard($data){

        $result = [
            'id_tournament'     =>  $data->getLeaderboard->id_leaderboard,
            'title'             =>  $data->getLeaderboard->tournament_name,
            'penyelenggara'     =>  $data->organizer_name,
            'tanggal_selesai'   =>  $data->date_end,
            'total_hadiah'      =>  $data->total_prize,
            "format"            =>  "Leaderboard",
            'game'              =>  strtoupper($data->getLeaderboard->name),
            'status'            =>  $data->status
        ];

        return $result;

    }

    private function bracket($data){
        // dump($data);
        $result = [
            'id_tournament'     =>  $data->getBracket->id_tournament,
            'title'             =>  $data->getBracket->tournament_name,
            'penyelenggara'     =>  $data->organizer_name,
            'tanggal_selesai'   =>  $data->date_end,
            'total_hadiah'      =>  $data->total_prize,
            "format"            =>  "Bracket",
            'game'              =>  strtoupper($data->getBracket->name),
            'status'            =>  $data->status
        ];

        return $result;

    }



}
