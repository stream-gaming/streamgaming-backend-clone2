<?php

namespace App\Transformer;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use League\Fractal\TransformerAbstract;

class UserOnboardingInfoTransformer extends TransformerAbstract
{

    protected $user;


	public function transform(User $user)
	{
        $this->user = $user;
        $data = [];

        if($this->user->userOnboarding){
            $data = $this->newUser($data);
        }else{
            $data = $this->existingUser($data);
        }

        return $data;
    }

    protected function existingUser($data){

        return array_merge($data, [
            'status' => true,
            'onboarding' => true,
            'tournament' => true,
            'progress' => '5'
        ]);
    }

    protected function newUser($data){
        $userOnboardingStatus = $this->user->userOnboarding->status;
        $userTournamentStatus = $this->user->userOnboarding->tournament_page;
        $onboardingStatus = $userOnboardingStatus != OnboardingEnum::ONPROGRESS_STATUS ? true:false;
        $tournamentStatus = $userTournamentStatus == 1 ? true:false;

        return array_merge($data, [
            'status' => true,
            'onboarding' => $onboardingStatus,
            'tournament' => $tournamentStatus,
            'progress' => $onboardingStatus ? '5' : ($this->user->userOnboarding->step+1)
        ]);
    }
}
