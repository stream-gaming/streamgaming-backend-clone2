<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Traits\TournamentMessagesTraits;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TournamentMessagesTransform extends TransformerAbstract
{
    use TournamentMessagesTraits;

    protected $admin;

    public function __construct($admin)
    {
        $this->MyApps = new MyApps();
        $this->admin  = $admin;
    }

    public function transform($message)
    {
        $detail = $message->detail_user ?? $message->detail_admin;

        if ($this->admin != false) {
            $is_me  = ($message->user_id == $this->admin->stream_id ? true : false);
        } else {
            $is_me  = ($message->user_id == Auth::user()->user_id ? true : false);
        }

        $result  = [
            'message_id' => $message->id,
            'user'      => [
                'username' => $message->username,
                'team'     => $message->team->team_name ?? "-",
                'picture'  => $this->getImage($detail, $message->user_status),
                'status'   => $message->user_status == '0' ? 'user' : 'admin',
            ],
            'is_me'      => $is_me,
            'is_delete'  => $message->deleted_at != null ? true : false,
            'message'    => $message->message,
            'created_at' => date('Y-m-d H:i:s', strtotime($message->created_at)),
            'attachment' => $message->attachment,
        ];

        if ($message->deleted_at != null) {
            $result['message']    = null;
            $result['attachment'] = null;
        }

        return $result;
    }
}
