<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TournamenGroupModel;
use App\Model\User;
use League\Fractal\TransformerAbstract;

class TournamentBracketSoloViiTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TournamenGroupModel $team)
    {
        //participant transformer for solo tournament

        $user = User::with('detail')->where('user_id',$team->id_team)->first() ;

        return [
            'team_logo' 	=> (new MyApps)->getImage($team->id_team),
            'team_name' 	=> $team->nama_team,
            'venue' 		=> $team->nama_kota,
            'team_id' 		=> (string) $team->id_team,
            'url'           => '/overview/'.$user->username
        ];
    }
}
