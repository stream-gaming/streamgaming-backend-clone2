<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TournamentMvpModel;
use League\Fractal\TransformerAbstract;

class TournamentMvpTransform extends TransformerAbstract
{   
    
    public function transform(TournamentMvpModel $data)
    {
        return [
            'team'      => $this->team($data->PlayerPayment),
            'user'      => $this->users($data)
        ];

    }
    public function team($data){
        return [
            'nama_team' => $data->getTeam->nama_team,
            'nama_kota' => $data->getTeam->nama_kota,
            'logo_team' => $data->getTeam->logo_team
        ];
    }
    public function users($data){
        return [
            'nama_player' => $data->nama_player,
            'logo_player' => (new MyApps)->getImage($data->id_player),
            'jumlah_mvp' => $data->jumlah
        ];
    }
}