<?php

namespace App\Transformer;

use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use League\Fractal\TransformerAbstract;

class LeaderboardAllInvoiceTransform extends TransformerAbstract
{
	public function transform($data)
    {
        //
        try{
            if(is_null($data->id_team)){
                if($data->game_option == 1){
                    $team = null;
                }else{
                    $playerPayment = LeaderboardTeamPlayerModel::where('id_leaderboard', $data->id_tournament)
                                                    ->where('id_teamplayer_leaderboard', $data->users_id)
                                                    ->first();

                    $idTeam = $playerPayment->id_team_leaderboard;
                    $team = TeamModel::withTrashed()->find($idTeam)->team_name;
                }
            }else{
                $team = $data->game_option != 1 ? TeamModel::withTrashed()->find($data->id_team)->team_name : null;
            }

            $mostkill_information = 'Pembagian hadiah Most Kill tournament '.$data->tournament_name;
            $result = [
                'id_tournament'     =>  $data->id_tournament,
                'name_tour'         =>  $data->tournament_name,
                "juara"             =>  $data->information == $mostkill_information ? 'Most Kill': $data->sequence,
                'id_team'           =>  '',
                "nama_team"         =>  $data->game_option != 1 ? $team : '--',
                'id_player'         =>  $data->users_id,
                'nama_player'       =>  $data->username,
                'email'             =>  !is_null($data->email) ? $data->email : null,
                'metode'            =>  $data->transaction_type,
                'jumlah'            =>  $data->balance,
                'waktu'             =>  $data->date,
                'status'            =>  "Sukses"
            ];


            return $result;
        }catch(\Exception $e){
            dd($e->getMessage(), $data);
        }
    }
}
