<?php
namespace App\Transformer;

use App\Helpers\MyApps;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TicketsTransform extends TransformerAbstract
{
    public function transform($data)
    {
        return [
            'name'          =>  $data->name,
            'type'          =>  $data->type,
            'sub_type'      =>  $data->sub_type,
            'unit'          =>  $data->unit,
            'max_given'     =>  $data->max_given,
            'total_given'   =>  $data->total_given,
            'expired'       =>  $data->expired,
            'event_end'     =>  $data->event_end,
            'competition'   =>  $data->competition,
            'can_traded'    =>  $data->can_traded,
            'market_value'  =>  $data->market_value,
            'giving_target' =>  $data->giving_target,
            'mode'          =>  $data->mode,
            'max_has'       =>  $data->max_has,
            'created_at'    =>  $data->created_at
        ];
    }
}
