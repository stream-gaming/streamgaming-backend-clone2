<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\CommentsModel;
use App\Traits\MyTraits;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class CommentsReplyListTournamentTransform extends TransformerAbstract
{
    use MyTraits;

    public function __construct()
    {
        $this->MyApps = new MyApps();
    }

    public function transform($comment)
    {
        $user = Auth::check() ? Auth::user()->user_id : null;

        $data = [
            'comments_id'   => $comment->id,
            'role'          => $comment->user_status,
            'parent_id'     => $comment->parent_id,
            'comment'       => $comment->comment,
            'created_at'    => $comment->created_at,
        ];

        if (!is_null($comment->user)) {
            $data['user'] = [
                'user_id'    => $comment->user->user_id,
                'full_name'  => $comment->user->fullname,
                'username'   => $comment->user->username,
                'picture'    => $this->getImage($comment->user->detail, '0'),
            ];

            // $data['action_menu'] = [
            //     "delete"   => $user == $comment->user->user_id ? true : false,
            // ];
        } else {
            $data['user'] = [
                'user_id'    => $comment->admin->stream_id,
                'full_name'  => $comment->admin->fullname,
                'username'   => $comment->admin->username,
                'picture'    => $this->getImage($comment->admin->detail, '1'),
            ];

            // $data['action_menu'] = [
            //     "delete"   => false,
            // ];
        }


        if ($comment->reply) {
            if ($comment->reply->user_status == 'user') {
                $data['reply_from'] = [
                    'reply_id'   => $comment->reply_id,
                    'user_id'    => $comment->reply->user_id,
                    'full_name'  => $comment->reply->fullname,
                    'username'   => $comment->reply->username,
                    'user_stat'     => '1',
                ];
            } else {
                $data['reply_from'] = [
                    'reply_id'   => $comment->reply_id,
                    'user_id'    => $comment->reply->stream_id,
                    'full_name'  => $comment->reply->adm_fullname,
                    'username'   => $comment->reply->adm_username,
                    'user_stat'     => '2',

                ];
            }
        } else {
            if ($comment->parent->user_status == 'user') {
                $data['reply_from'] = [
                    'reply_id'   => $comment->parent_id,
                    'user_id'    => $comment->parent->user_id,
                    'full_name'  => $comment->parent->fullname,
                    'username'   => $comment->parent->username,
                    'user_stat'     => '1',
                ];
            } else {
                $data['reply_from'] = [
                    'reply_id'   => $comment->parent_id,
                    'user_id'    => $comment->parent->stream_id,
                    'full_name'  => $comment->parent->adm_fullname,
                    'username'   => $comment->parent->adm_username,
                    'user_stat'     => '2',

                ];
            }
        }


        return $data;
    }
}
