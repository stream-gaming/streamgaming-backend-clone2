<?php

namespace App\Transformer;

use App\Helpers\Ticket\TicketRole;
use App\Helpers\Validation\CheckTeam;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TicketPaymentTransform extends TransformerAbstract
{
	public function transform($data, $system)
    {
        $isLogin = Auth::check();

        if($data->game_option == 1){
            $game = 'Solo';
        }else if($data->game_option == 2){
            $game = 'Duo';
        }else if($data->game_option == 3 OR $data->game_option == 4){
            $game = 'Team';
        }

        $ticketType = TicketRole::checkTicketType($data->type);
        $ticketGame = $data->competition == 'all' ? 'ALL GAME' :
        GameModel::where('id_game_list', $data->competition)->firstOr(function(){
            return null;
        })->name;

        if($system=='Leaderboard'){
            $result = [
                'price' => (int) $data->market_value,
                'ticket_name' => (string) $data->name,
                'tournament_date' => (string) $data->tournament_date,
                'game_option' => (string) $game,
                'game_id' => $data->game,
                'is_leader' => ($isLogin AND $game != 'Solo') ? ((new CheckTeam)->isLeaderOnTeam($data->game, Auth::user()->user_id) != false ? true : false ) : false,
                'expired_ticket' => $data->expired,
                'ticket_limit' => ($data->max_given <= 0) ? -1 : ($data->max_given - $data->total_given),
                'image'         => $data->image,
                'ticket_type'   => $ticketType,
                'game'          => $ticketGame
            ];
        }else if($system == 'Bracket'){
            $result = [
                'price' => (int) $data->market_value,
                'ticket_name' => (string) $data->name,
                'tournament_date' => (string) $data->date,
                'game_option' => (string) $game,
                'game_id' => $data->game,
                'is_leader' => ($isLogin AND $game != 'Solo') ? ((new CheckTeam)->isLeaderOnTeam($data->game, Auth::user()->user_id) != false ? true : false ) : false,
                'expired_ticket' => $data->expired,
                'ticket_limit' => ($data->max_given <= 0) ? -1 : ($data->max_given - $data->total_given),
                'image'         => $data->image,
                'ticket_type'   => $ticketType,
                'game'          => $ticketGame

            ];
        }


        return $result;
    }
}
