<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\MsFaqsModel;
use League\Fractal\TransformerAbstract;

class FaqsTransform extends TransformerAbstract
{
    public function transform(MsFaqsModel $data)
    {

        return [
            'id_ms_faqs'    => $data->id,
            'nama'          => $data->nama,
            'keterangan'    => $data->keterangan,
            'icon'          => $data->icon,
            'full_url'      => $data->faqs == null ? null : route('faqs.content', $data->faqs->url),
            'url'   => $data->faqs == null ? null : $data->faqs->url
        ];
    }
}
