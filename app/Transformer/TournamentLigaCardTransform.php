<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TournamentLiga;
use League\Fractal\TransformerAbstract;

class TournamentLigaCardTransform extends TransformerAbstract
{
    public function transform(TournamentLiga $data)
    {
        $myapps = new MyApps();
        return [
            "image"     => $data->background_liga,
            "url_page"  => $myapps->getPathRoute('tournament.list.liga',['url' => $data->url]),
            "date_start"=> date('Y-m-d H:i', strtotime($data->start_tour)),
            "date_end"  => date('Y-m-d H:i', strtotime($data->end_tour)),
            "name"      => $data->nama_liga,
            "prize"     => $data->jumlah_hadiah,
            "slot"      => $data->slot_tersedia,
            "type"      => $data->game != null ? strtolower($data->game->system_kompetisi) : null,
            "organizer" => $data->nama_penyelenggara,
            "logo"      => $data->logo_penyelenggara,
            "game_name" => $data->game != null ? $data->game->name : null,
            "game_id"   => $data->game != null ? $data->game->id_game_list : null
        ];
    }
}
