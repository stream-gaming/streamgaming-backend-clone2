<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class GameAllTransform extends TransformerAbstract
{
	public function transform($data)
	{
        $MyApps = new MyApps;
		switch($data->games_on) {
			case 1:
				$games_on = 'PC';
				break;
			case 2:
				$games_on = 'Mobile';
				break;
			case 3:
				$games_on = 'Console';
				break;
		}
		$system = Str::lower($data->system_kompetisi);
		return [
			// 'id_game_list'	=> $data->id_game_list,
			'name'			=> $data->name,
			'games_on'		=> $games_on,
			'logo'			=> $MyApps->cdn($data->logo, 'encrypt', 'game'),
			'background_logo1'  => $MyApps->cdn($data->logo_game_list, 'encrypt', 'game'),
			'background_logo2'	=> $MyApps->cdn($data->logo_account, 'encrypt', 'game'),
			'url'			=> $data->url,
			'full_url'		=> route('game.detail', ['url'=>$data->url]),
			'total_player'	=> $data->total_player,
			'total_tournament'	=> $data->total_tournament
		];
	}
}
