<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\LeaderboardGroupModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardScoreTeamModel;
use App\Model\LeaderboardTeamModel;
use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;

class LeaderboardGroupTransform extends TransformerAbstract
{
	public function transform(LeaderboardGroupModel $leaderboard_group)
	{
		$leaderboard_group->stage 	= $leaderboard_group->qualification_sequence;
		$top_score 					= (new LeaderboardScoreTeamModel)->getTopScore($leaderboard_group);

		$team                       = new LeaderboardTeamModel();
		$total_team					= $team->getTotalTeamGroup($leaderboard_group);
		$total_team_final			= $team->getTotalFinalTeamGroup($leaderboard_group);

		$leaderboard 				= LeaderboardModel::find($leaderboard_group->id_leaderboard);

		if ($leaderboard->tournament_category == 3) {
			if ($leaderboard_group->qualification_sequence == 0) {
				$leaderboard_group->qualification_sequence = 'final';
				$group_sequence				= 0;
			} else {
				$group_sequence				= ($leaderboard_group->group_sequence == '100' || $leaderboard_group->group_sequence == '1000' ? 'final' : $leaderboard_group->group_sequence);
			}
		} else {
			$group_sequence					= ($leaderboard_group->group_sequence == '100' || $leaderboard_group->group_sequence == '1000' ? 'final' : $leaderboard_group->group_sequence);
		}

		$my_apps 	= (new MyApps);
		$parameters	= ['url' => $leaderboard->url, 'stage' => $leaderboard_group->qualification_sequence, 'group' => $group_sequence];

        if($leaderboard->is_no_qualification == 1){
            $leaderboard_group->max_team = $leaderboard->total_team;
        }
		return [
			'group' 					=> $my_apps->getStatusCardLeaderboard($leaderboard_group->group_sequence),
			'total_team'				=> (($leaderboard_group->group_sequence != 100) ? $total_team : $total_team_final),
			'top_score'					=> [
				'team_name' => (($top_score) ? $top_score->nama_team : '--'),
				'total' 	=> (($top_score) ? $top_score->total : '--'),
			],
			'group_sequence' 			=> $leaderboard_group->group_sequence,
			'max_team' 					=> $leaderboard_group->max_team,
			'status_group' 				=> $leaderboard_group->status_group,
			'path' 						=> $my_apps->getPathRoute('leaderboard.detail', $parameters)
		];
	}
}
