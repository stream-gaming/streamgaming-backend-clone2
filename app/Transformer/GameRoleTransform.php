<?php

namespace App\Transformer;

use App\Model\GameRoleModel;
use League\Fractal\TransformerAbstract;

class GameRoleTransform extends TransformerAbstract
{
    public function transform(GameRoleModel $data)
    {
        return [
            'role_id'    => $data->id,
            'role_name'  => $data->role_name,
            'game_id'    => $data->game_id,
        ];
    }
}
