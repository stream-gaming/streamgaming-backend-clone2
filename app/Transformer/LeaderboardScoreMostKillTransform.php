<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\LeaderboardMostKillModel;
use League\Fractal\TransformerAbstract;

class LeaderboardScoreMostKillTransform extends TransformerAbstract
{

    public function transform($mostkill)
    {

        $result = [
            'player_name'     => $mostkill->player_name,
            'team_logo'       => (new MyApps)->cdn($mostkill->team_logo, 'encrypt', 'team'),
            'id_team'         => $mostkill->id_team,
            'nama_team'       => $mostkill->nama_team,
            'total_kill'      => $mostkill->total_kill,
        ];

        return $result;
    }
}
