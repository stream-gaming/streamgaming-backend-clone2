<?php

namespace App\Transformer;
 
use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Validator;
 
class SponsorshipTransform extends TransformerAbstract
{
	public function transform($data)
	{ 
		return [
			'title'	=> $data->title,
			'image'	=> (new MyApps)->cdn($data->image, 'encrypt', 'sponsorship'),
			'content'	=> $data->content,
			'link'		=> $data->link
		];  
	}
}