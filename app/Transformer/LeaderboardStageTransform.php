<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\LeaderboardModel;
use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;

class LeaderboardStageTransform extends TransformerAbstract
{
	public function transform($stage)
	{
		$status_stage   = $stage->qualification_sequence == 1 ? true : (($stage->leaderboard->status_tournament == 4 OR $stage->leaderboard->status_tournament == 3) ? true : false);

		return [
			'title'		=> ($stage->qualification_sequence != 0) ? Lang::get('message.tournament.stage') . '-' . $stage->qualification_sequence : $stage->qualification_name,
			'disabled'	=> $status_stage,
			'path'		=> (new MyApps)->getPathRoute('leaderboard.stage', ['url' => $stage->leaderboard->url, 'data' => 'leaderboard', 'stage' => $stage->qualification_sequence]),
		];
	}
}
