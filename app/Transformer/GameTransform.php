<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use App\Http\Controllers\Api\GameController;

class GameTransform extends TransformerAbstract
{
	static function list($data)
	{
		$dd['PC'] = [];
		$dd['Mobile'] = [];
		$dd['Console'] = [];

		foreach ($data as $d) {
			if ($d->games_on == 1) {
				$dd['PC'][] = [
					'id'	=> $d->id_game_list,
					'name'	=> $d->name,
					'url'	=> route('game.detail', ['url'=>$d->url])
				];
			} else if ($d->games_on == 2) {
				$dd['Mobile'][] = [
					'id'	=> $d->id_game_list,
					'name'	=> $d->name,
					'url'	=> route('game.detail', ['url'=>$d->url])
				];
			} else if ($d->games_on == 3) {
				$dd['Console'][] = [
					'id'	=> $d->id_game_list,
					'name'	=> $d->name,
					'url'	=> route('game.detail', ['url'=>$d->url])
				];
			}
		}

		return $dd;
	}

	static function simple($data)
	{
		foreach ($data as $d) {
			$datas[] = [
				'id'	=> $d->id_game_list,
				'name'	=> $d->name
			];
		}

		return $datas;
	}

	static function detail(GameModel $data, $other)
	{
        $MyApps = new MyApps;
        for ($i=0;$i<count($other);$i++) {
            $Game = GameModel::find($other[$i]->id_game_list);

            $system = Str::lower($Game->system_kompetisi);
			$other[$i]['full_url'] = route('game.detail', ['url'=>$Game->url]);
			$other[$i]['url'] = $Game->url;
			$other[$i]['logo'] = $MyApps->cdn($Game->logo, 'encrypt', 'game');
			$other[$i]['background_logo1'] = $MyApps->cdn($Game->logo_game_list, 'encrypt', 'game');
			$other[$i]['background_logo2'] = $MyApps->cdn($Game->logo_account, 'encrypt', 'game');
			$other[$i]['games_on'] = $Game->gamesOn($Game->games_on);
			$other[$i]['players'] = $Game->countPlayers();
			$other[$i]['total_player'] = $Game->players->count();
			$other[$i]['total_tournament'] = $Game->total_tournament($Game->id_game_list)->$system;
		}

		$system = Str::lower($data->system_kompetisi);
		$result = [
			'is_login'	=> Auth::check(),
			'detail' => [
				'id'	=> $data->id_game_list,
				'name'	=> $data->name,
				'content'	=> $data->content,
                'logo'		=> $MyApps->cdn($data->logo, 'encrypt', 'game'),
                'background_logo1'  => $MyApps->cdn($data->logo_game_list, 'encrypt', 'game'),
                'background_logo2'    => $MyApps->cdn($data->logo_account, 'encrypt', 'game'),
				'games_on' 	=> $data->gamesOn($data->games_on),
				'total_player' 		=> $data->players->count(),
				'total_tournament'	=> $data->total_tournament($data->id_game_list)->$system
			],
			'other'	=> $other
		];

		if (Auth::check()) {
			$result['detail']['is_added'] = (bool) (new GameController)->checkHaveGame($data->id_game_list);
		}

		return $result;
	}

	public function transform($data)
	{
		$Game = GameModel::find($data->game_id);
		return [
			'id'	=> $data->id,
			'game'	=> [
				'id_game'	=> $data->game_id,
				'name'		=> $Game->name,
				'logo'		=> (new MyApps)->cdn($Game->logo, 'encrypt', 'game') ,
			],
			'id_ingame'	=> $data->id_ingame,
			'username_ingame'	=> $data->username_ingame
		];
	}
}
