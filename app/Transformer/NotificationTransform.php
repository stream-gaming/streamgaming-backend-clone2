<?php

namespace App\Transformer;

use App\Model\NewsModel;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class NotificationTransform extends TransformerAbstract
{

	public function transform($data)
	{
		return [
			'id_notification'	=> $data->id_notification,
			'message'			=> $data->keterangan,
			'link'				=> $data->link,
			'is_read'			=> $data->is_read,
			'date'				=> date('Y-m-d H:i:s', strtotime($data->date))
		];
	}

}
