<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class GameStaticTransform extends TransformerAbstract
{
    public function transform($data)
    {
        $MyApps = new MyApps;
        return [
            'id_game_list'  => $data->id_game_list,
            'name'          => $data->name,
            'logo'          => $MyApps->cdn($data->logo, 'encrypt', 'game'),
            'background_logo1'  => $MyApps->cdn($data->logo_game_list, 'encrypt', 'game'),
            'background_logo2'    => $MyApps->cdn($data->logo_account, 'encrypt', 'game'),
            'url'            => $data->url,
            'full_url'        => route('game.detail', ['url' => $data->url])
        ];
    }
}
