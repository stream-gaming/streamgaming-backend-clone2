<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class TeamListMessagesTransform extends TransformerAbstract
{
    public function transform($team)
    {
        $MyApps     = new MyApps();
        $encrypt_id = $MyApps->onlyEncrypt($team->team_id);

        $result = [
            'team_id'      => $encrypt_id,
            'team_name'    => $team->team_name,
            'logo'         => $MyApps->cdn($team->team_logo, 'encrypt', 'team'),
            'url'          => $MyApps->getPathRoute('api.team.messages.fetch', ['team_id' => $encrypt_id]),
            'unread'       => $team->notif_count,
            'typing'       => null,
        ];

        $last_chat = [
            'name'      => null,
            'message'   => null,
            'attachment'=> null
        ];

        $result['date'] = date('Y-m-d H:i:s', strtotime($team->created_at));

        if (!is_null($team->lastMessage)) {
            $result['date'] = date('Y-m-d H:i:s', strtotime($team->lastMessage->created_at));
            $last_chat = [
                'name'      => $team->lastMessage->username,
                'message'   => $team->lastMessage->message,
                'attachment'=> $team->lastMessage->attachment
            ];
        }

        $result['last_chat'] = $last_chat;

        $result['team_url'] = route('team.detail', $MyApps->onlyEncrypt($team->team_id));
        $result['total_member'] = $team->countMember();

        return $result;
    }
}
