<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Str;

class TournamentListBracketTransform extends TransformerAbstract
{
    private $asset;
    public function __construct()
    {
        $this->asset = new MyApps;
    }

	public function transform(TournamentModel $data)
    {
        $date = new Carbon();
        $dateweekstart  = $date->startOfWeek(Carbon::MONDAY)->format('Y-m-d H:i');
        $dateweekend    = $date->endOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i');

        $prize = $payment = '';
        $paymentType = Str::lower($data->system_payment);

        $game_on = (new GameModel())->gamesOn($data->game->games_on);

        $slotsAvailable = $data->total_team;

        $slotsFilled = (new TournamenGroupModel)->countFilledSlots($data->id_group);

        if($paymentType == 'free'){
            $payment = $paymentType;
            $prize = array_sum(explode(',',$data->total_prize));
        }
        elseif ($paymentType == 'pay') {
            $payment = $data->tournament_fee;
            if($data->status_tournament >= 2){
                $prize = $payment * $slotsFilled;
            }
            else {
                $prizeCounter = $payment * $slotsAvailable;
                $prize = 'UP TO '.$prizeCounter;
            }
        }
        else {
            $payment = $data->payment.' TICKET';
            $prize = $data->firstprize + $data->secondprize + $data->thirdprize;
        }

        if($data->status == 'pending'){
            if($date->parse($data->start_registration)->lte($date->now())){
                $data->status = 'open_registration';
            }
        }

        $tournamentStatus = (object) $data->tournamentStatus($data->status);

        $data->banner_tournament = empty($data->banner_tournament) ?
                                    $this->asset->cdn($data->game->logo_game_list, 'encrypt', 'game') :
                                    $this->asset->cdn($data->banner_tournament, 'encrypt', 'banner_tournament');

        $result = [
            'title'         => $data->tournament_name,
            'card_banner'   => $data->banner_tournament,
            'is_free'       => $paymentType == 'free' ? true : false,
            'match_start'   => date('Y-m-d H:i', strtotime($data->date)),
            'total_prize'   => $prize,
            'hosted_by'     => $data->organizer_name,
            'match_info'    => [
                'game'      => $data->game->name,
                'game_on'   => $game_on,
                // 'type'      => 'BRACKET',
                // 'mode'      => $data->game_option == 1 ? 'SOLO' : ($data->game_option == 2 ? 'DUO' : 'SQUAD')
                ],
            'status'        => [
                'text'      => $slotsFilled == $slotsAvailable ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $slotsFilled == $slotsAvailable ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                ],
            'slots'         => [
                'filled'    => $slotsFilled,
                'available' => (int) $slotsAvailable,
                'slotstatus'=> (100 / $slotsAvailable) * $slotsFilled
            ],
            'url'           => $data->url,
            'full_url'      => $this->asset->getPathRoute('bracket', ['url' => $data->url]),
            'game_url'      => $data->game->url,
            'create_at'     => date('d-m-Y h:i', strtotime($data->created_at)),
            'time_value'    => strtotime($data->date),
            'time'          => $date->parse($data->date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->date)->isAfter($dateweekend) ? 'upcoming' : 'last')
        ];

        return $result;
    }



}
