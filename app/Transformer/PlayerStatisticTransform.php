<?php

namespace App\Transformer;

use App\Model\LeaderboardScoreMatchModel;
use League\Fractal\TransformerAbstract;

class PlayerStatisticTransform extends TransformerAbstract
{
    public function transform($statistik)
    {
        $data = [
            "jumlah_kill" => (int) $statistik->total_kill,
            "total_match" => (int) $statistik->total_match,
            "total_win" => (int) $statistik->win,
            "winrate" => (string) ($statistik->total_match > 0 ? (round((($statistik->win / $statistik->total_match) * 100),1))."%" : "0%")
        ];

        return $data;
    }
}
