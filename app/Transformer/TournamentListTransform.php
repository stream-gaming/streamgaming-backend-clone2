<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use App\Model\TicketClaimsModel;
use App\Model\TournamentListModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;

class TournamentListTransform extends TransformerAbstract
{
    private $asset;
    private $free_ticket_id;
    public function __construct()
    {
        $this->asset = new MyApps;
        $this->free_ticket_id = TicketClaimsModel::select('ticket_id')->get()->pluck('ticket_id')->all();
        array_push($this->free_ticket_id,'free');
    }

    public function transform(TournamentListModel $data){

        $date = new Carbon();
        $dateweekstart  = $date->now()->startOfWeek(Carbon::MONDAY)->format('Y-m-d H:i');
        $dateweekend    = $date->now()->endOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i');

        $time = $date->parse($data->tournament_date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->tournament_date)->isAfter($dateweekend) ? 'upcoming' : 'last');

            $game_on = (new GameModel())->gamesOn($data->game->games_on);

            if($data->prize_type == "default"){
                if($data->payment_type == "free"){
                    $prize = array_sum(explode(',',$data->total_prize));
                }
                elseif ($data->payment_type == 'pay') {
                    $payment = $data->tournament_fee;
                    if($data->status_tournament >= 2){
                        $prize = $payment * $data->filled_slot;
                    }
                    else {
                        $prizeCounter = $payment * $data->total_team;
                        $prize = 'UP TO '.$prizeCounter;
                    }
                }
                else{
                    $prize = array_sum(explode(',',$data->total_prize));
                }
            } else{
                $prize = LeaderboardPrizePoolModel::where('id_leaderboard',$data->id_tournament)->sum('prize');
                $prize += $data->total_prize;
            }

            $tournamentStatus = (object) (new LeaderboardModel)->tournamentStatus($data->statuss);

            $data->card_banner = empty($data->card_banner) ?
                                    $this->asset->cdn($data->game->logo_game_list, 'encrypt', 'game') :
                                    $this->asset->cdn($data->card_banner, 'encrypt', 'banner_tournament');

            $result = [
                'title'         => $data->tournament_name,
                'card_banner'   => $data->card_banner,
                'is_free'       => $data->payment_type == 'free' || in_array($data->ticket, $this->free_ticket_id) ? true : false,
                'match_start'   => date('Y-m-d H:i', strtotime($data->tournament_date)),
                'total_prize'   => $prize + $data->total_prize2,
                'hosted_by'     => $data->organizer_name,
                'match_info'    => [
                    'game'      => $data->game->name,
                    'game_on'   => $game_on,
                    // 'type'      => 'LEADERBOARD',
                    'mode'      => $data->game_option == 1 ? 'solo' : 'squad'
                ],
                'status'        => [
                    'text'      => $data->filled_slot == $data->total_team ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                    'number'    => $data->filled_slot == $data->total_team ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                    ],
                'slots'         => [
                    'filled'    => $data->filled_slot,
                    'available' => $data->total_team,
                    'slotstatus'=> (100 / $data->total_team) * $data->filled_slot
                ],
                'url'           => $data->url,
                'full_url'      => $data->format_tournament == 'Leaderboard' ? $this->asset->getPathRoute('leaderboard.stage', ['url' => $data->url]) : $this->asset->getPathRoute('bracket', ['url' => $data->url]),
                'game_url'      => $data->game->url,
                'create_at'     => date('d-m-Y h:i', strtotime($data->input_date)),
                'time_value'    => strtotime($data->tournament_date),
                'time'          => $date->parse($data->tournament_date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->tournament_date)->isAfter($dateweekend) ? 'upcoming' : 'last')
            ];

        return $result;
    }

    public function list($data)
    {
        $date = new Carbon();
        $dateweekstart  = $date->now()->startOfWeek(Carbon::MONDAY)->format('Y-m-d H:i');
        $dateweekend    = $date->now()->endOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i');

        $result = [
        ];

        foreach ($data as $data) {

            $time = $date->parse($data->tournament_date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->tournament_date)->isAfter($dateweekend) ? 'upcoming' : 'last');

            $game_on = (new GameModel())->gamesOn($data->game->games_on);

            if($data->prize_type == "default"){
                if($data->payment_type == "free"){
                    $prize = array_sum(explode(',',$data->total_prize));
                }
                elseif ($data->payment_type == 'pay') {
                    $payment = $data->tournament_fee;
                    if($data->status_tournament >= 2){
                        $prize = $payment * $data->filled_slot;
                    }
                    else {
                        $prizeCounter = $payment * $data->total_team;
                        $prize = 'UP TO '.$prizeCounter;
                    }
                }
                else{
                    $prize = array_sum(explode(',',$data->total_prize));
                }
            } else{
                $prize = LeaderboardPrizePoolModel::where('id_leaderboard',$data->id_leaderboard)->sum('prize');
                $prize += $data->total_prize;
            }

            $tournamentStatus = (object) (new LeaderboardModel)->tournamentStatus($data->statuss);

            $result[$time] = [
                'title'         => $data->tournament_name,
                'card_banner'   => $data->card_banner,
                'is_free'       => $data->payment_type == 'free' ? true : false,
                'match_start'   => date('Y-m-d H:i', strtotime($data->tournament_date)),
                'total_prize'   => $prize,
                'hosted_by'     => $data->organizer_name,
                'match_info'    => [
                    'game'      => $data->game->name,
                    'game_on'   => $game_on,
                    // 'type'      => 'LEADERBOARD',
                    // 'mode'      => $data->game_option == 1 ? 'SOLO' : ($data->game_option == 2 ? 'DUO' : 'SQUAD')
                ],
                'status'        => [
                    'text'      => $data->filled_slot == $data->total_team ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                    'number'    => $data->filled_slot == $data->total_team ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                    ],
                'slots'         => [
                    'filled'    => $data->filled_slot,
                    'available' => $data->total_team,
                    'slotstatus'=> (100 / $data->total_team) * $data->filled_slot
                ],
                'url'           => $data->url,
                'full_url'      => $this->asset->getPathRoute('leaderboard.stage', ['url' => $data->url]),
                'game_url'      => $data->game->url,
                'create_at'     => date('d-m-Y h:i', strtotime($data->input_date)),
                'time_value'    => strtotime($data->tournament_date),
                'time'          => $date->parse($data->tournament_date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->tournament_date)->isAfter($dateweekend) ? 'upcoming' : 'last')
            ];
        }

        return $result;
    }
}
