<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\TeamInviteModel;
use App\Model\TeamRequestModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class TeamInviteTransform extends TransformerAbstract
{

    public function transform(TeamInviteModel $team_invite)
    {
        $team = TeamModel::find($team_invite->team_id);
        $result = [
            'invite_id'   => (string) $team_invite->invite_id,
            'team'      => [
                'game'      => $team->game->name,
                'leader'    => $team->leader->username,
                'team_name' => $team->team_name,
                'venue'     => $team->venue,
                'logo'      => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
                'team_url'  => (new MyApps)->onlyEncrypt($team->team_id)
            ],
            'identity_id'    => $team_invite->identity_id,
            'status'        => $team_invite->status,
            'status_name'   => (new MyApps)->getStatusRequest($team_invite->status),
        ];

        return $result;
    }
}
