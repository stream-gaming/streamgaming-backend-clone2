<?php

namespace App\Transformer;

use App\Model\ItemMobileLegend;
use League\Fractal\TransformerAbstract;

class ItemListTransform extends TransformerAbstract
{
    public function transform(ItemMobileLegend $item)
    {
        $result = [
            'nama_gear' => $item->nama_gear,
            'image' => 'https://cmfg.streamgaming.id/public/content/tournament/gear_mobile_legends/'.$item->image,
            'attribut' => $item->attribut,
            'deskripsi' => $item->keterangan
        ];

        return $result;
    }
}
