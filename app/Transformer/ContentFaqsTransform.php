<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\FaqsModel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ContentFaqsTransform extends TransformerAbstract
{
    public function transform(FaqsModel $data)
    {
        return [
            'id_faqs'               => $data->id_faqs,
            'type_faqs'             => $data->type_faqs,
            'title'                 => $data->title,
            'inheritance'           => $data->type_faqs == 3 ? ($data->is_sub_menu == 1 ?  TRUE : FALSE)  : FALSE,
            'inheritance_title'     => $data->type_faqs == 3 ? ($data->is_sub_menu == 1 ?  $data->SubMenuFaqs->MenuFaqs->faqs->title : null) : null,
            'inheritance_url'       => $data->type_faqs == 3 ? ($data->is_sub_menu == 1 ?  $data->SubMenuFaqs->MenuFaqs->faqs->url : null) : null,
            'created_at'            => Carbon::parse($data->created_at),
            'content'               => $data->type_faqs == 1 ? $data->contentFaqs :($data->type_faqs == 2  ? $this->menuFaqs($data->MenuFaqs) : ($data->type_faqs == 3 ? $this->article($data->contentFaqs) : '' ) ),
            'expression'            => $data->expression
        ];

        return $data;
    }
    function menuFaqs($data){
         $i=0;
         foreach ($data as $rows) {
             $data[$i] = [
                'title'     => $rows->title,
                'sub_menu'  => $this->SubMenuFaqs($rows->SubMenuFaqs)
             ];
             $i++;
         }
        return $data;
    }
    function SubMenuFaqs($data){
        $i=0;
        foreach ($data as $rows) {
            $data[$i] = [
            'title'             => $rows->faqs->title,
            'full_url'          => route('faqs.content', $rows->faqs->url),
            'url'               => $rows->faqs->url

            ];
            $i++;
        }
       return $data;
   }
    function article($data){
        $i=0;
        foreach ($data as $rows) {
            $data[$i] = [
            'article'     => $rows->content,
            ];
            $i++;
        }
       return $data;
    }
}
