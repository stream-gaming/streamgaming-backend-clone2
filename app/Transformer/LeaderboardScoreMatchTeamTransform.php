<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class LeaderboardScoreMatchTeamTransform extends TransformerAbstract
{


    public function transform($total_score)
    {
        $result = [
            'team_name'       => $total_score->nama_team,
            'team_logo'       => (new MyApps)->cdn($total_score->team_logos, 'encrypt', 'team'),
            'id_team'         => (isset($total_score->id_team_leaderboard) ? $total_score->id_team_leaderboard : $total_score->id_team),
            'sequence_score'  => (isset($total_score->sequence_score) ? $total_score->sequence_score : '--'),
            'total_kill'      => (isset($total_score->total_kill) ? $total_score->total_kill : '--'),
            'total_score'     => (isset($total_score->total_score) ? $total_score->total_score : '--'),
            'total'           => (isset($total_score->total) ? $total_score->total : '--'),
        ];

        return $result;
    }
}
