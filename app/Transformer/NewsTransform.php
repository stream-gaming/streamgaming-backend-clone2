<?php

namespace App\Transformer;

use App\Model\NewsModel;  
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth; 
use League\Fractal\TransformerAbstract;
 
class NewsTransform extends TransformerAbstract
{

	static function image_($data)
	{
		return (new \App\Helpers\MyApps)->cdn($data, 'encrypt', 'news');
	}

	static function url_($data)
	{
		return route('news.detail', ['url'=>$data]);
	}

	static function tags_($data)
	{
		return explode(',', $data);
	}

	static function sanitizeArr($data, $mini = '')
	{ 

		foreach ($data as $row) {
			$news = NewsModel::find($row->id_news_game);

			$row->judul = Str::limit($row->judul, 35, ' ...');
			$row->penulis = [
				'name'	=> $news->author->fullname,
				'image'	=> (new \App\Helpers\MyApps)->getImage($news->author->stream_id, 'admin'), 
			];
			$row->label = $news->label()->first();
			$row->gambar = static::image_($mini.$row->gambar);
			$row->url = $row->url; 
			$row->tag = static::tags_($row->tag);
			$row->likes = $row->like();	
			$row->is_like = Auth::check() ? $row->is_like() : false;

			$rows[] = $row;
		}

		return $rows; 
	}

	static function sanitizeSingle($row)
	{

		$news = NewsModel::find($row->id_news_game);

		$row->judul = Str::limit($row->judul, 35, ' ...');
		$row->penulis = [
			'name'	=> $news->author->fullname,
			'image'	=> (new \App\Helpers\MyApps)->getImage($row->author->stream_id, 'admin')
		];
		$row->label = $news->label()->first();
		$row->gambar = static::image_($row->gambar);
		$row->url = $row->url; 
		$row->tag = static::tags_($row->tag);
		$row->is_like = Auth::check() ? $row->is_like() : false;	

		return $row;

	}  

	static function simple($data)
	{ 
		for ($i=0;$i<count($data);$i++) {
			if ($i == 0) {
				$isNew = 1;
			} else {
				$isNew = 0;
			}
			$data[$i]->judul = Str::limit($data[$i]->judul, 50, ' ...');
			$data[$i]->label = NewsModel::find($data[$i]->id_news_game)->label()->first(); 
			$data[$i]->gambar = static::image_('mini-'.$data[$i]->gambar);
 			$data[$i]->url = $data[$i]->url;    
			$data[$i]->is_new	= $isNew;
 		}
		return $data;
	}

	static function detail(NewsModel $news, $related)
	{  
		// detail conversion 
		$news->label = $news->label()->first();
		$news->penulis = [
			'name'	=> $news->author->fullname,
			'image'	=> (new \App\Helpers\MyApps)->getImage($news->author->stream_id, 'admin'), 
		];
		$news->gambar = static::image_($news->gambar);
		$news->url = $news->url;
		$news->tag = static::tags_($news->tag); 
		$news->likes = (int) $news->like();
		$news->is_like = Auth::check() ? $news->is_like() : false; 
		unset($news->author);

		$data['detail'] = $news;

		//data related
		$data['related'] = static::sanitizeArr($related);

		return $data;
	}

	public function transform(NewsModel $news)
    { 
        return [
        	'id_news_game'		=> (string) $news->id_news_game,
            'judul' 			=> (string) Str::limit($news->judul, 35, ' ...'),
            'penulis'			=> [
            	'name'			=> $news->author->fullname,
            	'image'			=> (new \App\Helpers\MyApps)->getImage($news->author->stream_id, 'admin')
            ],
            'label'				=> $news->label()->first(),
            'gambar'			=> (string) static::image_($news->gambar),
            'url'				=> (string) $news->url, 
            'tag'				=> static::tags_($news->tag),
            'views'				=> (int) $news->views,
            'likes'				=> (int) $news->like(),
            'komentar'			=> (int) $news->komentar,
            'created_at'		=> $news->created_at,
            'is_like'			=> Auth::check() ? $news->is_like() : false
        ];
    }

    public static function comment($data)
    {
    	for($i=0;$i<count($data);$i++) {
    		$data[$i]->users = [
    			'name'	=> $data[$i]->author->username,
    			// 'image'	=> $data[$i]->authorDetail
    		];
    	}

    	return $data;
    }

}