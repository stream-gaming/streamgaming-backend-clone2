<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TeamModel;
use League\Fractal\TransformerAbstract;
use App\Model\TournamenGroupModel;

class TournamentBracketViiTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    private $myApps;
    public function __construct()
    {
        $this->myApps = new MyApps;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TournamenGroupModel $team)
    {
        //participant transformer for squad tournament
        $logo = TeamModel::withTrashed()->where('team_id',$team->id_team)->first()->team_logo;
        return [
            'team_logo' 	=> $this->myApps->cdn($logo, 'encrypt', 'team'),
            'team_name' 	=> $team->nama_team,
            'venue' 		=> $team->nama_kota,
            'team_id' 		=> (string) $team->id_team,
            'url'           => (new MyApps)->getPathRoute('team.detail', (new MyApps)->onlyEncrypt($team->id_team))
        ];

    }
}
