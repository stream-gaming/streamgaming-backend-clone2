<?php

namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class TeamHistoryTransform extends TransformerAbstract
{

	public function HistoryTeamBracket($data)
    {

        $result = [];
        $num = 0;
        foreach ($data as $rows) {
        $result[$num] = [
            "pertandingan" => $rows->id_team == $rows->winner ? "Win" : ($rows->id_team == $rows->lose ? "Lose" : "Live"),
            "tanggal"      => date('Y-m-d H:i:s', strtotime($rows->date)),
            "nama_tournament"         => $rows->tournament_name,
            "round"        => $rows->rounds,
            "id_round"     => $rows->id_round,
            "peringkat"    => "N/A"
        ];
        $num++;
        }
        return $result;
    }

    public function HistoryBracketPlayer($data){
        // $sada = [];
        for($i=0;$i<count($data);$i++) {
            if($data[$i] != null ){
                $data2 = $data[$i];
                for($y=0;$y<count($data2);$y++) {
                    $data3[] = $data[$i][$y];
                    // for($z=0;$z<count($data3);$z++) {
                    //     // $sada[] = $data[$i][$y][$z];
                    // }
                }
            }


        }
        // return $sada = [$sada];
        return $data3;
    }
    public function HistoryBracketTeam($data = []){
        if(count($data) != 0){
            for($i=0;$i<count($data);$i++) {
                if($data[$i] != null){
                    $tes = $data[$i];
                }
                for($y=0;$y<count($tes);$y++) {
                    if($data[$i][$y] != null){
                        $datas[] = $data[$i][$y];
                    }
                }
            }
            return $datas;
        }else{
            return [];
        }





    }

}
