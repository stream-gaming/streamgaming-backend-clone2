<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Http\Controllers\Api\GameController;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class GameAvailableTransform extends TransformerAbstract
{
	public function transform($data)
	{
		$MyApps = new MyApps();

		return [
			'id_game'	=> $data->id_game_list,
			'url'		=> $data->url,
			'name'		=> $data->name,
			'logo'		=> $MyApps->cdn($data->logo, 'encrypt', 'game'),
			'background_logo1'  => $MyApps->cdn($data->logo_game_list, 'encrypt', 'game'),
			'background_logo2'	=> $MyApps->cdn($data->logo_account, 'encrypt', 'game'),

		];
	}
}
