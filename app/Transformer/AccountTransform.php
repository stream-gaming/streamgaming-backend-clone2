<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\BalanceUsersModel;
use App\Model\GameModel;
use App\Model\GameUsersRoleModel;
use App\Model\IdentityGameModel;
use App\Model\PlayerPayment;
use App\Model\SocmedModel;
use App\Model\TeamInviteModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Model\TicketModel;
use App\Model\User;
use App\Model\UserPreferencesModel;
use App\Model\UsersGameModel;
use App\Model\UserTicketsModel;
use App\Serializer\GameSerialize;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;

class AccountTransform extends TransformerAbstract
{
    public function transform()
    {
        $user = User::where('user_id', Auth::user()->user_id)
            ->with(['province', 'city'])
            ->first();

        $email_notification    = UserPreferencesModel::where('user_id', $user->user_id)->first();

        return [
            'id'              => $user->user_id,
            'fullname'        => $user->fullname,
            'username'        => $user->username,
            'email'           => $user->email,
            'provinsi'        => $user->provinsi,
            'nama_provinsi'   => $user->province->nama ?? '-',
            'kota'            => $user->kota,
            'nama_kota'       => $user->city->nama ?? '-',
            'nomor_hp'        => $user->nomor_hp,
            'alamat'          => $user->alamat,
            'jenis_kelamin'   => $user->jenis_kelamin,
            'tgl_lahir'       => $user->tgl_lahir,
            'age'             => Carbon::parse($user->tgl_lahir)->age,
            'bio'             => $user->bio,
            'status'          => $user->status,
            'user_preferences' => [
                'email_notification' => (($email_notification) ? $email_notification->email_notification : 0),
            ],
            'picture'        => (new MyApps)->getImage($user->user_id),
            'socmed'         => $this->socmed($user->user_id),
            'role'           => SELF::getRoleGame($user)
        ];
    }

    public static function socmed($user_id)
    {
        $user_id = isset($user_id) ? $user_id : Auth::user()->user_id;

        $array_socmed = SocmedModel::$sosmed;

        $socmed = collect(SocmedModel::where('users_id', $user_id)
            ->whereIn('sosmed', $array_socmed)
            ->get());

        $result_socmed_array = $socmed->mapWithKeys(function ($item) {
            return [strtolower($item['sosmed']) => $item['url']];
        });

        for ($i = 0; $i < count($array_socmed); $i++) {
            $data[$i] = [
                'sosmed' => $array_socmed[$i],
                'status' => isset($result_socmed_array[$array_socmed[$i]]) ? true : false,
                'url'    => isset($result_socmed_array[$array_socmed[$i]]) ? $result_socmed_array[$array_socmed[$i]] : null
            ];
        }

        return $data;
    }
    public function overviewUser($username)
    {
        $user = User::where('username', $username)
            ->with(['province', 'city'])
            ->first();

        return [
            'id'            => $user->user_id,
            'fullname'      => $user->fullname,
            'username'      => $user->username,
            'jenis_kelamin'    => $user->jenis_kelamin,
            'provinsi'        => $user->provinsi,
            'nama_provinsi'   => $user->province->nama ?? '-',
            'kota'            => $user->kota,
            'nama_kota'       => $user->city->nama ?? '-',
            'tgl_lahir'        => $user->tgl_lahir,
            'age'           => Carbon::parse($user->tgl_lahir)->age,
            'bio'           => $user->bio,
            'status'        => $user->status,
            'picture'       => (new MyApps)->getImage($user->user_id),
            'socmed'        => $this->socmed($user->user_id),
            'role'          => $this->getRoleGame($user)
        ];
    }

    public static function getRoleGame($user, $game_id = null)
    {
        $role = GameUsersRoleModel::join('game_role', function ($q) {
            $q->on('game_role.id', '=', 'game_users_role.game_role_id');
        })
            ->where('user_id', $user->user_id);

        if ($game_id != null) {
            $role = $role->where('game_role.game_id', $game_id);
        }

        $role = $role->get();

        if (count($role) == 0) {
            return [];
        }

        foreach ($role as $k => $v) {
            $data[$k] = [
                'name' => $v->role_name
            ];
        }

        return $data;
    }

    public function user(User $user)
    {
        return [
            'id'            => $user->user_id,
            'fullname'      => $user->fullname,
            'jenis_kelamin' => $user->jenis_kelamin,
            'tgl_lahir'     => $user->tgl_lahir,
            'age'           => Carbon::parse($user->tgl_lahir)->age,
            'bio'           => $user->bio,
            'alamat'        => $user->nomor_hp,
            'nomor_hp'      => $user->alamat,
            'status'        => $user->status,
        ];
    }

    public function balance()
    {
        $user = Auth::user();

        $balance = BalanceUsersModel::where('users_id', $user->user_id)->first();

        $ticket = (new UserTicketsModel)->ticket($user->user_id);

        $result = [
            'id'            => $user->user_id,
            'fullname'      => $user->fullname,
            'username'      => $user->username,
            'picture'       => (new MyApps)->getImage($user->user_id),
            'balance'       => [
                'cash'          => $balance->balance,
                'point'         => $balance->point,
                'totalTicket'   => 0,
                'ticket'        => []
            ]
        ];

        $counter = 0;
        foreach ($ticket as $val) {
            $array = [
                'ticket_name'   => $val->names,
                'ticket_value'  => (int) $val->ticket
            ];

            $counter += $val->ticket;

            array_push($result['balance']['ticket'], $array);
        }

        $result['balance']['totalTicket'] = $counter;

        return $result;
    }

    public function notifMyTeam()
    {
        $user = Auth::user();

        return [
            'id'            => $user->user_id,
            'fullname'      => $user->fullname,
            'username'      => $user->username,
            'notif' => [
                'request'    => (new TeamModel)->countRequestByLeaderTeam(),
                'invite'     => collect((new TeamInviteModel)->InviteByUserLogin())->where('status', 1)->count(),
            ]
        ];
    }

    public function myGame($game)
    {
        $user           = Auth::user();
       
        $user_game      = UsersGameModel::where('users_id', $user->user_id)->first();
        $game_available = GameModel::whereNotIn('id_game_list', explode(',', $user_game->game_id))->get();
        $game           = new Collection($game, new MyGameTransform);
        $game_available = new Collection($game_available, new GameAvailableTransform);

        return [
            'id'             => $user->user_id,
            'fullname'       => $user->fullname,
            'username'       => $user->username,
            'game'           => current((new Manager())->createData($game)->toArray()),
            'game_available' => current((new Manager())->createData($game_available)->toArray())
        ];
    }

    public function myTeam()
    {
        $user           = Auth::user();
        $team_player    = TeamPlayerModel::where('player_id', 'LIKE', '%' . $user->user_id . '%')
            ->orWhere('substitute_id', 'LIKE', '%' . $user->user_id . '%')
            ->pluck('team_id')->toArray();
        $team             = TeamModel::whereIn('team_id', $team_player)->get();
        $response_team    = new Collection($team, new MyTeamTransform);

        $team            = (new TeamInviteModel)->InviteByUserLogin();
        $response_invite = new Collection($team, new TeamInviteTransform);

        return [
            'id'        => $user->user_id,
            'fullname'  => $user->fullname,
            'username'  => $user->username,
            'invite'    => current((new Manager())->createData($response_invite)->toArray()),
            'team'      => current((new Manager())->createData($response_team)->toArray())
        ];
    }
    public function myDetailTeam($team)
    {
        $user            = Auth::user();
        $response_team   = (new MyTeamTransform)->detail($team);

        return [
            'id'         => $user->user_id,
            'fullname'   => $user->fullname,
            'username'   => $user->username,
            'team'       => $response_team
        ];
    }
}
