<?php

namespace App\Transformer;

use App\Model\InvitationCode;
use App\Modules\Team\TeamInvitationCodeState;
use League\Fractal\TransformerAbstract;

class TeamInvitationCodeTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(InvitationCode $invitationCode)
    {
        return [
            'invitationCode' => $invitationCode->code,
            'expiredAt'      => date('Y-m-d H:i:s', strtotime($invitationCode->expired_at)),
            'state'          => TeamInvitationCodeState::getCurrentState($invitationCode->team)
        ];
    }
}
