<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class DiscussionListTransform extends TransformerAbstract
{
    public function transform($data)
    {
        $MyApps     = new MyApps();

        $result = [
                'room_id' => $data->id_tour,
                'tournament_name' => $data->tour_name,
                'tournament_logo' => $data->logo,
                'tournament_type' => $data->game_option == 1 ? 'solo' : 'squad',
                'is_pinned' => $data->pinned_tour != null ? true : false,
                'typing' => null,
                'unread' => $data->total_unread,
                'date' => $data->date_payment,
                'last_message' => null,
                'discussion_url' => $MyApps->getPathRoute('api.tournament.messages.fetch', ['tournament_id' => $data->id_tour])

            ];

        $result['last_message'] = [
            'name'      => null,
            'message'   => null,
            'attachment'=> null,
            'is_delete' => false
        ];
        if(!is_null($data->lastMessage)){
            $result['date'] = date('Y-m-d H:i:s', strtotime($data->lastMessage->created_at));
            $result['last_message'] = [
                'name'      => $data->lastMessage->username,
                'message'   => !is_null($data->lastMessage->deleted_at) ? null : $data->lastMessage->message,
                'attachment'=> !is_null($data->lastMessage->deleted_at) ? null : $data->lastMessage->attachment,
                'is_delete' => !is_null($data->lastMessage->deleted_at) ? true : false
            ];
        }

        return $result;
    }
}
