<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use App\Model\LeaderboardQualificationModel;
use App\Model\LeaderboardTeamModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use League\Fractal\TransformerAbstract;

class TournamentListLeaderboardTransform extends TransformerAbstract
{
    private $asset;
    public function __construct()
    {
        $this->asset = new MyApps;
    }

	public function transform(LeaderboardModel $data)
    {
        $date = new Carbon();
        $dateweekstart  = $date->now()->startOfWeek(Carbon::MONDAY)->format('Y-m-d H:i');
        $dateweekend    = $date->now()->endOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i');

        $prize = '';
        $category = $data->tournament_category;
        $paymentType = $data->tournament_form;


        $game_on = (new GameModel())->gamesOn($data->game->games_on);


        $qualification_sequence = $category == 3 ? 1 : 0;

        $slotsAvailable = $data->total_team;

        if($qualification_sequence){
            $slotsAvailable = $data->AvailableSlots->total_team;
        }

        $slotsFilled = (new LeaderboardTeamModel)->countFilledSlots($data->id_leaderboard, $qualification_sequence);

        if($data->prize_type == "default"){
            if($paymentType == "free"){
                $payment =  $paymentType;
                $prize = $data->firstprize + $data->secondprize + $data->thirdprize + $data->most_kill;
            }
            elseif ($paymentType == 'pay') {
                $payment = $data->tournament_fee;
                if($data->status_tournament >= 2){
                    $prize = $payment * $slotsFilled;
                }
                else {
                    $prizeCounter = $payment * $slotsAvailable;
                    $prize = 'UP TO '.$prizeCounter;
                }
            }
            elseif ($paymentType == 'ticket') {
                $payment = $data->ticket_unit.' TICKET';
                $prize = $data->firstprize + $data->secondprize + $data->thirdprize;
            }
        } else{
            $prize = LeaderboardPrizePoolModel::where('id_leaderboard',$data->id_leaderboard)->sum('prize');
            $prize += $data->most_kill;
        }

        if($data->status_tournament == 0){
            if($date->parse($data->regist_start)->lte($date->now())){
                $data->status_tournament = 1;
            }
        }
        $tournamentStatus = (object) $data->tournamentStatus($data->status_tournament);

        $data->card_banner = empty($data->card_banner) ?
                                    $this->asset->cdn($data->game->logo_game_list, 'encrypt', 'game') :
                                    $this->asset->cdn($data->card_banner, 'encrypt', 'banner_tournament');

        $result = [
            'title'         => $data->tournament_name,
            'card_banner'   => $data->card_banner,
            'is_free'       => $paymentType == 'free' ? true : false,
            'match_start'   => date('Y-m-d H:i', strtotime($data->tournament_date)),
            'total_prize'   => $prize,
            'hosted_by'     => $data->organizer_name,
            'match_info'    => [
                'game'      => $data->game->name,
                'game_on'   => $game_on,
                // 'type'      => 'LEADERBOARD',
                // 'mode'      => $data->game_option == 1 ? 'SOLO' : ($data->game_option == 2 ? 'DUO' : 'SQUAD')
            ],
            'status'        => [
                'text'      => $slotsFilled == $slotsAvailable ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $slotsFilled == $slotsAvailable ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                ],
            'slots'         => [
                'filled'    => $slotsFilled,
                'available' => $slotsAvailable,
                'slotstatus'=> (100 / $slotsAvailable) * $slotsFilled
            ],
            'url'           => $data->url,
            'full_url'      => $this->asset->getPathRoute('leaderboard.stage', ['url' => $data->url]),
            'game_url'      => $data->game->url,
            'create_at'     => date('d-m-Y h:i', strtotime($data->input_date)),
            'time_value'    => strtotime($data->tournament_date),
            'time'          => $date->parse($data->tournament_date)->between($dateweekstart,$dateweekend) ? 'this_week' : ($date->parse($data->tournament_date)->isAfter($dateweekend) ? 'upcoming' : 'last')
        ];

        return $result;
    }



}
