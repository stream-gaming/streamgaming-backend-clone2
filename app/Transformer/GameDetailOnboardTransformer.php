<?php
namespace App\Transformer;

use App\Model\GameModel;
use League\Fractal\TransformerAbstract;

class GameDetailOnboardTransformer extends TransformerAbstract
{
	public function transform(GameModel $game)
	{
	    return [
            'game' => [
                'id_game' => $game->id_game_list,
                'name' => $game->name,
                'logo' => $game->logo_game_list,
                'tutorial' => $game->identity_game_tutorial
            ],
            'role' => count($game->role) == 0 ? [] : $this->transformRole($game->role)
	    ];
	}

    public function transformRole($roles){

        foreach($roles as $role){
           $data[] = [
                'role_id'    => $role->id,
                'role_name'  => $role->role_name,
                'game_id'    => $role->game_id
           ];
        }

        return $data;
    }
}
