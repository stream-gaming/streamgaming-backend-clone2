<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use App\Model\UsersDetailModel;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TournamenBracketTransform extends TransformerAbstract
{

    private $myApps;
    public function __construct()
    {
        $this->myApps = new MyApps;
    }
	public function transform(TournamenGroupModel $team)
    {
        $logo = TeamModel::withTrashed()->where('team_id',$team->id_team)->first()->team_logo;
        $result = [
            'team_id'   => (string) $team->id_team,
            'team_name' => $team->nama_team,
            'city'      => $team->nama_kota,
            'logo'      => $this->myApps->cdn($logo, 'encrypt', 'team'),
            'url'       => $this->myApps->getPathRoute('team.detail', $this->myApps->onlyEncrypt($team->id_team)),
        ];

        return $result;
    }

    public static function overview(TournamentModel $details){

        $slotsAvailable = $details->total_team;

        $slotsFilled = (new TournamenGroupModel)->countFilledSlots($details->id_group);

        $details->banner = (new MyApps)->cdn($details->banner, 'encrypt', 'banner_tournament');

        $result = [
            'banner'                => $details->banner,
            'match_description'     => $details->description,
            'detail' => [
                'organizer_name'    => $details->organizer_name,
                'organizer_image'   => (!empty($details->organizer_image) ? $details->organizer_image : NULL),
                'total_slot'        => $slotsFilled.'/'.$slotsAvailable,
                'tournament_type'   => 'bracket'
            ]
        ];

        return $result;
    }

    public function TeamData($this_round,$start_at,$data,$id_tournament)
    {
        $datas = [];
        $num = 0;
        $arr_roundtext = [
            8 => 'id_quarter_finals',
            4 => 'id_semi_finals',
            3 => 'id_third_place',
            2 => 'id_final'
        ];

        $id_round = $this_round > 8 ? "id_round".$this_round : $arr_roundtext[$this_round];

        foreach ($data as $val) {
        $datas[$num] = [
                'match' =>[
                    'team_1' => [
                        'name' => $val->team_1,
                        'logo' => $this->Getlogo($id_tournament,$val->id_team1),
                        'city' => $val->nama_kota_1,
                        'score' => $val->score1
                        ],
                    'team_2' => [
                        'name' => $val->team_2,
                        'logo' => $this->Getlogo($id_tournament,$val->id_team2),
                        'city' => $val->nama_kota_2,
                        'score' => $val->score2
                        ]
                    ],
                'date' => $val->date != "" ? date('Y-m-d H:i', strtotime($val->date)) : null,
                'match-overview-url' => $val->$id_round != null ? (new MyApps())->getPathRoute('matchs.overview', [$this_round,$start_at,(new MyApps)->onlyEncrypt($val->$id_round)]) : ''
                ];
            $num++;
        }


        return $datas;
    }

    public function winner($data,$id_tournament)
    {
        $result = [
            'name' => $data->team_1,
            'logo' => $this->Getlogo($id_tournament,$data->id_team1),
            'city' => $data->nama_kota_1,
            'date' => date('Y-m-d H:i', strtotime($data->date))
        ];

        return $result;
    }


    public function matchOverview($data)
    {
        $name_a = $data->score1 > $data->score2 ? $data->a : $data->b;
        $name_b = $data->score2 > $data->score1 ? $data->a : $data->b;
        $result = [
            'team1' => [
                'name'  => $name_a,
                'index' => str_replace(' ','_',Str::lower($name_a)),
                'logo'  => (new MyApps)->cdn($data->logo_1, 'encrypt', 'team'),
                'city'  => $data->nama_kota_1,
                'score' => $data->score1,
                'index' => $data->score1 > $data->score2 ? 'W' : 'L'
            ],
            'team2' => [
                'name'  => $name_b,
                'index' => str_replace(' ','_',Str::lower($name_b)),
                'logo' => (new MyApps)->cdn($data->logo_2, 'encrypt', 'team'),
                'city' => $data->nama_kota_2,
                'score' => $data->score2,
                'index' => $data->score2 > $data->score1 ? 'W' : 'L'
            ],
            'date' => date('Y-m-d H:i', strtotime($data->date))
        ];

        return $result;
    }
    function Getlogo($id_tournament,$id_team){
        $tournament = TournamentModel::find($id_tournament);

        if($tournament->game_option == '1'){
            return $this->myApps->getImage($id_team);

        }else if($tournament->game_option == '3'){
            $logo = TeamModel::where('team_id',$id_team)->first();
            return isset($logo->team_logo) ? (new MyApps)->cdn($logo->team_logo, 'encrypt', 'team') : null;
        }
    }


}
