<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\TeamRequestModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class MyTeamDetailTransform extends TransformerAbstract
{

    public function transform(TeamModel $team)
    {
        $request_team = (new TeamRequestModel)->countRequestByTeamId($team->team_id);

        $result = [
            'team_id'   => (string) $team->team_id,
            'game'      => $team->game->name,
            'leader'    => $team->leader->username,
            'team_name' => $team->team_name,
            'venue'     => $team->venue,
            'notif_request' => $request_team,
            'logo'      => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
            'slot'      => [
                'total'   => $team->game->player_on_team + $team->game->substitute_player,
                'member'  => $team->countMember()
            ],
            'url'           => (new MyApps)->onlyEncrypt($team->team_id),
            'created_at'    => $team->created_at
        ];

        $username_game = new IdentityGameModel();
        for ($i = 0; $i < count($team->primaryPlayer()); $i++) {
            $User = User::find($team->primaryPlayer()[$i]);
            $result['member']['primary'][$i] = [
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => (new MyApps)->getImage($team->primaryPlayer()[$i]),
                'bio'       => $User->bio,
                'birthday'  => $User->tgl_lahir,
                'age'       => Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username_game->usernameInGame($User->user_id, $team->game_id),
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0
            ];
        }

        for ($ii = 0; $ii < count($team->substitutePlayer()); $ii++) {
            $User = User::find($team->substitutePlayer()[$ii]);
            $result['member']['substitute'][$ii] = [
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => (new MyApps)->getImage($team->substitutePlayer()[$ii]),
                'bio'       => $User->bio,
                'birthday'  => $User->tgl_lahir,
                'age'       => Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username_game->usernameInGame($User->user_id, $team->game_id),
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0
            ];
        }

        $request        = TeamRequestModel::where('team_id', $team->team_id)->get();
        $response       = new Collection($request, new TeamRequestTransform);

        $result['request'] = current((new Manager())->createData($response)->toArray());

        return $result;
    }
}
