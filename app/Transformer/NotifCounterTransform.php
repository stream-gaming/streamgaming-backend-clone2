<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\UserNotificationModel;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Validator;

class NotifCounterTransform extends TransformerAbstract
{
	public function transform(UserNotificationModel $data)
	{
		return [
            $data->id_user => [
                'counter' =>  $data->counter
            ]
		];
    }

    public function edit($data)
    {
        $return = [];

        foreach ($data as $key) {
            $return[$key->id_user] = [
                'counter' =>  $key->counter
            ];
        }

        return $return;
    }
}
