<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Traits\CheckImageTraits;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class TeamMessagesTransform extends TransformerAbstract
{
    use CheckImageTraits;

    protected $team;
    protected $user;

    public function __construct(TeamModel $team)
    {
        $this->team = $team;
        $this->MyApps = new MyApps();
        $this->user = Auth::user();
    }

    public function transform(MessageTeamModel $message)
    {
        $result = [
            'id'   => $message->id,
            'user'      => [
                'username' => $message->username,
                'role'     => $message->userInTeamPlayer ? (int) $message->userInTeamPlayer->player_status : 0,
                'picture'  => $this->getImage($message->detail_user),
            ],
            'is_me'      => $message->user_id == $this->user->user_id ? true : false,
            'is_read'    => collect(explode(",", $message->user_read))->contains($this->user->user_id),
            'message'    => $message->message,
            'created_at' => date('Y-m-d H:i:s', strtotime($message->created_at)),
            'attachment' => $message->attachment,
        ];

        return $result;
    }

    public function getImage($detail)
    {
        if (!empty($detail->link_image) && empty($detail->image)) {
            return $this->isImageExists($detail->link_image);
        } else if (!empty($detail->image) && !empty($detail->link_image)) {
            return $this->isImageExists($this->MyApps->cdn([
                'user_id'    => $detail->user_id,
                'image'      => $detail->image
            ], 'encrypt', 'users'));
        } else {
            return config('app.cm_apps') . 'private/user.png';
        }
    }
}
