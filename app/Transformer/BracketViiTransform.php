<?php

namespace App\Transformer;

use App\Helpers\JoinTournamentButton;
use App\Helpers\MyApps;
use App\Helpers\Ticket\TicketValidation;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Model\SpectatorModel;
use App\Model\TournamentModel;
use League\Fractal\TransformerAbstract;

class BracketViiTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform()
    {
        return [
            //
        ];
    }

    public function headInfo(TournamentModel $tournament, $slotsAvailable, $slotsFilled)
    {
        $register_fee      = (object)(new CheckTournamentPaymentMethod)->BracketPaymentMethod($tournament->id_tournament);
        $tournamentStatus  = (object) $tournament->tournamentStatus($tournament->status);
        $is_full = ($slotsAvailable != 0 && $slotsFilled != 0) ? ($slotsAvailable / $slotsFilled) : 0;
        $prizes = explode(',',$tournament->total_prize);
        $totalprize = array_sum($prizes);

        return [
            'id_tournament'      => $tournament->id_tournament,
            'tournament_name'    => $tournament->tournament_name,
            'tournament_date'    => date('Y-m-d H:i', strtotime($tournament->date)),
            'is_solo_tournament' => (($tournament->game_option == 1) ? true : false),
            'game' => [
                'name'    => $tournament->game->name,
                'logo'    => (new MyApps)->cdn($tournament->game->logo, 'encrypt', 'game'),
                'url'     => $tournament->game->url
            ],
            'register_end'         => date('Y-m-d H:i:s', strtotime($tournament->end_registration)),
            'register_open'        => date('Y-m-d H:i:s', strtotime($tournament->start_registration)),
            'register_fee'      => $register_fee,
            'status'        => [
                'text'      => $slotsFilled == $slotsAvailable ? (($tournamentStatus->text != trans('tournament.status.in_progress') && $tournamentStatus->text != trans('tournament.status.complete')) ? trans('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $slotsFilled == $slotsAvailable ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
            ],
            'button_join'   => JoinTournamentButton::getStatus($tournament->status, $is_full, $tournament->kompetisi, $tournament->id_tournament,'bracket' , $tournament),
            'background'    => (new MyApps)->cdn($tournament->background_tournament, 'encrypt', 'banner_tournament'),
            'total_prizes'  => (string) $totalprize,
            'live_stream'   => ($tournament->url_livestreams) ? $tournament->url_livestreams : null,
        ];
    }

    public function sideInfo(TournamentModel $bracket)
    {
        $spectators         = SpectatorModel::whereIn('id_ingame',explode(',',$bracket->spectators))->get();

        return [
            'important'         => $bracket->important,
            'tournament_date'   => date('Y-m-d H:i:s', strtotime($bracket->tournament_date)),
            'spectator'         => $spectators,
            'game' => [
                'name'    => $bracket->game->name,
                'logo'    => (new \App\Helpers\MyApps)->cdn($bracket->game->logo, 'encrypt', 'game'),
                'url'    => $bracket->game->url,
            ],
        ];
    }

    public function ticketInfo(TournamentModel $tournament){
        $ticket = new TicketValidation;
        $getTicket = $ticket->getTournamentTicketInfo($tournament->id_tournament, 'Bracket');

        if($getTicket['ticket_status'] == true){
        $buttonValidation = $ticket->buttonValidation($getTicket['data']['game_id'], $getTicket['data']['game_option']);

        //check button validation

            $getTicket['data']['user_detail'] = $buttonValidation['user_detail'];


        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => $buttonValidation['button'] ,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];

     }else{
        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => null,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];
     }

        return $dataTicket;
    }

    public function overview(TournamentModel $tournament, $slotsAvailable, $slotsFilled)
    {
        return [
            'banner'                 => (new MyApps)->cdn($tournament->banner, 'encrypt', 'banner_tournament'),
            'match_description'     => $tournament->description,
            'detail' => [
                'organizer_name'    => $tournament->organizer_name,
                'total_slot'        => $slotsFilled . '/' . $slotsAvailable,
                'tournament_type'   => 'bracket'
            ]
        ];
    }

    public function rules(TournamentModel $tournament)
    {
        return [
            'general'     => $tournament->peraturan_umum,
            'special'     => $tournament->peraturan_khusus
        ];
    }
}
