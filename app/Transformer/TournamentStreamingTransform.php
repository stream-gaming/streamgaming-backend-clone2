<?php

namespace App\Transformer;
 
use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Validator;
 
class TournamentStreamingTransform extends TransformerAbstract
{
	public function transform($data)
	{ 
		return [
			'title'	    => $data->title,
			'link'		=> $data->url
		];  
	}
}