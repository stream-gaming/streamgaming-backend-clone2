<?php

namespace App\Transformer;

use App\Model\User;
use App\Http\Controllers\ScoreMatchsController;
use App\Model\TeamModel;
use App\Model\SocmedModel;
use App\Model\TeamRequestModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use App\Http\Controllers\Api\Team\TeamHistoryMatchs;

class TeamTransform extends TransformerAbstract
{

    public function __construct($user_request)
    {
        $this->user_request = $user_request;
    }

	public function transform(TeamModel $team)
    {
        $MyApps = new MyApps;
        //count team member
        // $jumlah_player = $team->jumlah_player == null ? 0 : $team->jumlah_player;
        // $jumlah_cadangan = $team->jumlah_cadangan == null ? 0 : $team->jumlah_cadangan;

        $player = empty($team->player_id) ? [] : explode(',',$team->player_id);
        $subtitute = empty($team->substitute_id) ? [] : explode(',',$team->substitute_id);

        $result = [
            'team_id'   => (string) $team->team_id,
            'game'      => $team->game->name,
            'game_id'   => (string) $team->game->id_game_list,
            'leader'    => $team->leader->username,
            'team_name' => $team->team_name,
            'venue'     => $team->venue,
            'logo'      => $MyApps->cdn($team->team_logo, 'encrypt', 'team'),
            'slot'      => [
                'total'   => $team->game->player_on_team+$team->game->substitute_player,
                'member' => (count($player) + count($subtitute))
                // 'member'  => $team->countMember()
            ],
            'full_url'       => route('team.detail', $MyApps->onlyEncrypt($team->team_id)),
            'url'       => $MyApps->onlyEncrypt($team->team_id),
            'created_at'    => $team->created_at
        ];

        if (Auth::check()) {
            // $waiting = TeamRequestModel::where('team_id', $team->team_id)
            //                             ->where('users_id', Auth::user()->user_id)
            //                             ->where('status', 1)
            //                             ->count();
            $result['is_member'] = (collect($player)->contains(Auth::user()->user_id) OR collect($subtitute)->contains(Auth::user()->user_id));
            $result['is_waiting'] = false;

            if (collect($this->user_request)->contains($team->team_id)) {
                $result['is_waiting'] = true;
            }
        }

        return $result;
    }

    public static function detail(TeamModel $team, $isMultisystem = false)
    {
        $MyApps = new MyApps;
        $data_matchs = $team->dataTeam;
        $teamMatchHistory = (new TeamHistoryMatchs)->index($team->team_id, 5);

        $result = [
            'team_id'   => $team->team_id,
            'game'      => $team->game->name,
            'game_id'   => (string) $team->game->id_game_list,
            'leader'    => $team->leader->username,
            'team_name' => $team->team_name,
            'venue'     => $team->venue,
            'description'   => $team->team_description,
            'logo'      => $MyApps->cdn($team->team_logo, 'encrypt', 'team'),
            'slot'      => [
                'total'   => $team->game->player_on_team+$team->game->substitute_player,
                'member'  => $team->countMember()
            ],
            'created_at'    => $team->created_at,
            'data_matchs'   => $data_matchs,
            'history_pertandingan'  => $teamMatchHistory,
            'history_pertandingan_multi' => $isMultisystem ? (new TeamHistoryMatchs)->multiSystem($team->team_id, 5) : null,
            'member'    => [
                'primary'   => [],
                'substitute'    => []
            ]
        ];

        for($i=0;$i<count($team->primaryPlayer());$i++) {
            $User = User::find($team->primaryPlayer()[$i]);
            $data_history = (new ScoreMatchsController)->index($team->team_id,$User->user_id);
            // $data_history['killsrate'] =  ($data_history['kills'] / $data_matchs['total_kills'] ) * 100 ;
            $username = IdentityGameModel::where('users_id', $User->user_id)
            ->where('game_id', $team->game_id)
            ->first();

            $result['member']['primary'][$i] = [
                "player" => $User->user_id,
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => $MyApps->getImage($team->primaryPlayer()[$i]),
                'bio'       => $User->bio,
                'birthday'  => empty($User->tgl_lahir) ? '-' : $User->tgl_lahir,
                'age'       => empty($User->tgl_lahir) ? '-' : Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username->username_ingame,
                'is_banned'     => (bool) $username->is_banned,
                'ban_until'     => $username->ban_until,
                'socmed'    => AccountTransform::socmed($User->user_id),
                'role'      => AccountTransform::getRoleGame($User, $team->game_id),
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0,
                'data_history' => $data_history,
                'data_history_multi' => $isMultisystem ? (new ScoreMatchsController)->multiSystem($team->team_id,$User->user_id) : null
            ];
        }


        for($ii=0;$ii<count($team->substitutePlayer());$ii++) {
            $User = User::find($team->substitutePlayer()[$ii]);
            $data_history = (new ScoreMatchsController)->index($team->team_id,$User->user_id);
            // dd($dataHi);
            $username = IdentityGameModel::where('users_id', $User->user_id)
            ->where('game_id', $team->game_id)
            ->first();

            $result['member']['substitute'][$ii] = [
                "player" => $User->user_id,
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => $MyApps->getImage($team->substitutePlayer()[$ii]),
                'bio'       => $User->bio,
                'birthday'  => empty($User->tgl_lahir) ? '-' : $User->tgl_lahir,
                'age'       => empty($User->tgl_lahir) ? '-' : Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username->username_ingame,
                'is_banned'     => (bool) $username->is_banned,
                'ban_until'     => $username->ban_until,
                'socmed'    => AccountTransform::socmed($User->user_id),
                'role'      => AccountTransform::getRoleGame($User, $team->game_id),
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0,
                'data_history' => $data_history
            ];
        }

        return $result;
    }

}

//- MDC
