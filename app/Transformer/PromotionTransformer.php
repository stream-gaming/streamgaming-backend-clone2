<?php

namespace App\Transformer;

use App\Model\PromotionModel;
use League\Fractal\TransformerAbstract;

class PromotionTransformer extends TransformerAbstract
{

	public function transform(PromotionModel $promotion)
	{
        return [
            'promotion_id' => $promotion->id,
            'title' => $promotion->title,
            'content' => $promotion->content,
        ];
    }

}
