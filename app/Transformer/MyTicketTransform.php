<?php

namespace App\Transformer;

use App\Helpers\Ticket\TicketRole;
use App\Model\GameModel;
use App\Model\HistoryTicketsModel;
use App\Model\TicketsModel;
use App\Traits\TicketClaimsTrait;
use App\Traits\UserHelperTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use PhpParser\Node\Stmt\Switch_;

class MyTicketTransform extends TransformerAbstract
{
    public function __construct($mode)
    {
        $this->mode = $mode;
    }

    public function transform($ticket)
    {
        $ticketType = $this->mode == 'new' ? TicketRole::checkTicketType($ticket->type) : 'REGULAR';
        $competition = $this->mode == 'new' ? $ticket->competition : $ticket->kompetisi;
        $ticketGame = $competition == 'all' ? 'ALL GAME' :
        GameModel::where('id_game_list', $competition)->firstOr(function(){
            return null;
        })->name;

        if ($this->mode == "new") {
            $data = [
                'ticket_id' => $ticket->id,
                'name'      => $ticket->name,
                'expired'   => $ticket->expired,
                'image'         => $ticket->image,
                'ticket_type'   => $ticketType,
                'game'          => $ticketGame
            ];
        } else {
            for ($i = 0; $i < $ticket->ticket; $i++) {
                $data[$i] = [
                    'ticket_id' => $ticket->id_ticket,
                    'name'      => $ticket->names,
                    'expired'   => $ticket->expired,
                    'image'         => null,
                    'ticket_type'   => $ticketType,
                    'game'          => $ticketGame
                ];
            }
        }

        return $data;
    }
}
