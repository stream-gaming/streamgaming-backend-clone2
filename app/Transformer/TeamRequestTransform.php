<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\User;
use App\Model\TeamRequestModel;
use League\Fractal\TransformerAbstract;

class TeamRequestTransform extends TransformerAbstract
{

    public function transform(TeamRequestModel $team_request)
    {
        $user = User::findOrFail($team_request->users_id);
        $team = TeamModel::findOrFail($team_request->team_id);
        $result = [
            'id'            => (string) $team_request->request_id,
            'user_id'       => $team_request->users_id,
            'username_game' => (new IdentityGameModel)->usernameInGame($team_request->users_id, $team->game_id),
            'picture'       => (new MyApps)->getImage($user->user_id),
            'fullname'      => $user->fullname,
            'status'        => $team_request->status,
            'status_name'   => (new MyApps)->getStatusRequest($team_request->status),
            'created_at'    => $team_request->created_at
        ];


        return $result;
    }
}
