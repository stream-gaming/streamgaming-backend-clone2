<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TeamModel;
use League\Fractal\TransformerAbstract;

class TopTeamTransform extends TransformerAbstract
{
    public function transform($data)
    {
        $MyApps =  new MyApps();
        $data = [
            'game_id'   => $data->game_id,
            'team_id'   => $data->team_id,
            'team_name' => $data->team_name,
            'team_logo' => $MyApps->cdn($data->team_logo, 'encrypt', 'team'),
            'url'       => $MyApps->onlyEncrypt($data->team_id),
            'statistik' => [
                'total_win'    => (int) $data->total_win,
                'total_lose'   => (int) $data->total_match - $data->total_win,
                'total_match'  => (int) $data->total_match,
                'total_kill'   => (int) $data->total_kill,
                'win_rate'     => (int) $data->win_rate,
            ]
        ];

        return $data;
    }
}
