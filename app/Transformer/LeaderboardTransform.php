<?php

namespace App\Transformer;

use App\Helpers\JoinTournamentButton;
use App\Helpers\MyApps;
use App\Helpers\Ticket\TicketValidation;
use App\Helpers\Validation\CheckGame;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Model\AllTicketModel;
use App\Model\CustomPrizesModel;
use App\Model\GameModel;
use App\Model\LeaderboardGroupModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTicketPrizeModel;
use App\Model\PrizeInventoryModel;
use App\Model\Spectator;
use App\Model\SpectatorModel;
use App\Model\TicketModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentStreamingModel;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class LeaderboardTransform extends TransformerAbstract
{
    public function transform(LeaderboardModel $leaderboard)
    {
        // return [
        // 	'comment_id'	=> $comment->comment_id,
        // 	'comment'		=> $comment->comment,
        // 	'user'			=> [
        // 		'name'		=> $comment->author->username,
        // 		'picture'	=> (new \App\Helpers\MyApps)->getImage($comment->authorDetail->user_id)
        // 	],
        // 	'created_at'	=> $comment->date,
        // 	'reply'			=> $this->replied($comment->replied($comment->comment_id)),
        // ];
    }

    public function overview(LeaderboardModel $leaderboard, $slotsAvailable, $slotsFilled)
    {
        return [
            'banner'                 => (new MyApps)->cdn($leaderboard->banner, 'encrypt', 'banner_tournament'),
            'match_description'     => $leaderboard->description,
            'detail' => [
                'organizer_name'    => $leaderboard->organizer_name,
                'organizer_image'   => !empty($leaderboard->organizer_image) ? $leaderboard->organizer_image : null,
                'total_slot'        => $slotsFilled . '/' . $slotsAvailable,
                'tournament_type'   => 'LEADERBOARD'
            ]
        ];
    }

    public function rules(LeaderboardModel $leaderboard)
    {
        return [
            'general'     => $leaderboard->peraturan_umum,
            'special'     => $leaderboard->peraturan_khusus
        ];
    }

    public function leaderboardInfo($url)
    {
        $leader_info = (new LeaderboardModel)->leaderboardInfo($url);

        return [
            'tournament_name'    => $leader_info->tournament_name,
            'name'               => $leader_info->game->name,
            'is_solo_tournament' => (($leader_info->game_option == 1) ? true : false )
        ];
    }

    public function prizes(LeaderboardModel $leaderboard)
    {

        switch ($leaderboard->prize_type) {
            case 'dynamic':
                $get_prizes = LeaderboardPrizePoolModel::select('sequence', 'prize')->where('id_leaderboard', $leaderboard->id_leaderboard)->get();
                foreach ($get_prizes as $key => $p) {
                    $prizes[$key] = [
                        'sequence'        => (string) $p->sequence,
                        'prize'           => (string) $p->prize
                    ];
                }
                break;

            default:
                $prizes[] = [
                    'sequence'        => (string) 1,
                    'prize'           => (string) $leaderboard->firstprize
                ];
                $prizes[] = [
                    'sequence'        => (string) 2,
                    'prize'           => (string) $leaderboard->secondprize
                ];
                $prizes[] = [
                    'sequence'        => (string) 3,
                    'prize'           => (string) $leaderboard->thirdprize
                ];
                break;
        }
        if (($leaderboard->most_kill)) {
            $prizes[] = [
                'sequence' => 'most_kill',
                'prize'    => (string) $leaderboard->most_kill
            ];
        }
        return $prizes;
    }

    public function prizesV2(LeaderboardModel $leaderboard){
        $sequence = 0;
        $prizes = [];
        switch ($leaderboard->prize_type) {

            //for dinamic prizes
            case 'dynamic':
                $get_prizes = LeaderboardPrizePoolModel::select('sequence', 'prize')
                                                        ->where('id_leaderboard', $leaderboard->id_leaderboard)
                                                        ->orderBy('sequence')
                                                        ->get();
                foreach ($get_prizes as $key => $p) {

                        $prizes[$key]["sequence"] = (string) $p->sequence;
                        $prizes[$key]["title"] = (string) "Juara ".$p->sequence;
                        $prizes[$key]["description"] = "";


                            $prizes[$key]["data"][] = [
                                'title'           => (string) "Stream Cash",
                                'level'           => (string) "Hadiah Utama",
                                'category'        => (string) "stream_cash",
                                'image'           => null,
                                'unit'            => (string) "1",
                                'prize'           => (string) $p->prize
                            ];

                        $sequence++;

                }
            break;
            //for static
            default:
                $prizeName = ["firstprize", "secondprize", "thirdprize"];

                for($i=0; $i < 3; $i++){
                    if($leaderboard[$prizeName[$i]] > 0){
                        $prizes[$i]["sequence"] = (string) ($i+1);
                        $prizes[$i]["title"] = (string) "Juara ".($i+1);
                        $prizes[$i]["description"] = "";

                            $prizes[$i]["data"][] = [
                                'title'           => (string) "Stream Cash",
                                'level'           => (string) "Hadiah Utama",
                                'category'        => (string) "stream_cash",
                                'image'           => null,
                                'unit'            => (string) "1",
                                'prize'           => (string) $leaderboard[$prizeName[$i]]
                            ];

                        $sequence++;
                    }
                }

            break;
        }


        //check if this ticket prize or not
        if($leaderboard->isTicketPrize == 1){

            switch($leaderboard->is_ticket_prize_new){
                case '1':
                    try{
                        $getTicket = LeaderboardTicketPrizeModel::where('id_leaderboard', '=', $leaderboard->id_leaderboard)
                                                                 ->orderBy('sequence')
                                                                 ->get();

                        $getTicketInfo = AllTicketModel::find($getTicket[0]->id_ticket)->name;

                        foreach ($getTicket as $key => $p) {

                            if(isset($prizes[$key])){
                                $prizes[$key]["data"][] = [
                                    'title'           => (string) $getTicketInfo,
                                    'level'           => (string) "Hadiah Utama",
                                    'category'        => (string) "ticket",
                                    'image'           => null,
                                    'unit'            => (string) $p->given_ticket,
                                    'prize'           => (string) "0"
                                ];
                            }else{
                                $prizes[$key]["sequence"] = (string) $p->sequence;
                                $prizes[$key]["title"] = (string) "Juara ".$p->sequence;
                                $prizes[$key]["description"] = "";
                                $prizes[$key]["data"][] = [
                                    'title'           => (string) $getTicketInfo,
                                    'level'           => (string) "Hadiah Utama",
                                    'category'        => (string) "ticket",
                                    'image'           => null,
                                    'unit'            => (string) $p->given_ticket,
                                    'prize'           => (string) "0"
                                ];
                                $sequence++;
                            }

                        }

                    }catch(\Exception $e){
                        dd($e);
                    }

                break;
                case '0':
                    try{
                        $getTicket = LeaderboardTicketPrizeModel::where('id_leaderboard', '=', $leaderboard->id_leaderboard)
                                                                 ->orderBy('sequence')
                                                                 ->get();

                        $getTicketInfo = TicketModel::where('id_ticket', '=', $getTicket[0]->id_ticket)->first();

                        foreach ($getTicket as $key => $p) {

                            if(isset($prizes[$key])){
                                $prizes[$key]["data"][] = [
                                    'title'           => (string) $getTicketInfo->names,
                                    'level'           => (string) "Hadiah Utama",
                                    'category'        => (string) "ticket",
                                    'image'           => null,
                                    'unit'            => (string) $p->given_ticket,
                                    'prize'           => (string) "0"
                                ];
                            }else{
                                $prizes[$key]["sequence"] = (string) $p->sequence;
                                $prizes[$key]["title"] = (string) "Juara ".$p->sequence;
                                $prizes[$key]["description"] = "";
                                $prizes[$key]["data"][] = [
                                    'title'           => (string) $getTicketInfo->names,
                                    'level'           => (string) "Hadiah Utama",
                                    'category'        => (string) "ticket",
                                    'image'           => null,
                                    'unit'            => (string) $p->given_ticket,
                                    'prize'           => (string) "0"
                                ];
                                $sequence++;
                            }

                        }

                    }catch(\Exception $e){
                        dd($e);
                    }
                break;
            }
        }

        //for custom prize
        if($leaderboard->is_custom_prize == 1){
            try{
                $getCustomPrize = CustomPrizesModel::where('tournament_id', '=', $leaderboard->id_leaderboard)
                                                    ->orderBy('sequence')
                                                    ->get();
                foreach ($getCustomPrize as $key => $p) {
                    $getPrizeInfo = PrizeInventoryModel::withTrashed()->find($p->pi_id, ['title', 'valuation', 'image']);
                    if(isset($prizes[$key])){
                        $prizes[$key]["data"][] = [
                            'title'           => (string) $getPrizeInfo->title,
                            'level'           => (string) "Hadiah Utama",
                            'category'        => (string) "custom_prize",
                            'image'           => (string) $getPrizeInfo->image,
                            'unit'            => (string) $p->quantity,
                            'prize'           => (string) $getPrizeInfo->valuation
                        ];
                    }else{
                        $prizes[$key]["sequence"] = (string) $p->sequence;
                        $prizes[$key]["title"] = (string) "Juara ".$p->sequence;
                        $prizes[$key]["description"] = "";
                        $prizes[$key]["data"][] = [
                            'title'           => (string) $getPrizeInfo->title,
                            'level'           => (string) "Hadiah Utama",
                            'category'        => (string) "custom_prize",
                            'image'           => (string) $getPrizeInfo->image,
                            'unit'            => (string) $p->quantity,
                            'prize'           => (string) $getPrizeInfo->valuation
                        ];
                        $sequence++;
                    }

                }
            }catch(\Exception $e){
                dd($e);
            }
        }

        if ($leaderboard->most_kill > 0) {

            $prizes[$sequence]["sequence"] = (string) ($sequence+1);
            $prizes[$sequence]["title"] = (string) "Most Kill";
            $prizes[$sequence]["description"] = "";
            $prizes[$sequence]["data"][] = [
                'title'           => (string) "Stream Cash",
                'level'           => (string) "Hadiah Utama",
                'category'        => (string) "stream_cash",
                'image'           => null,
                'unit'            => (string) "1",
                'prize'           => (string) $leaderboard->most_kill
            ];
        }


        return $prizes;
    }


    // - Get Side Info
    public function sideInfo(LeaderboardModel $leaderboard)
    {
        return [
            'important'         => $leaderboard->important,
            'tournament_date'   => date('Y-m-d H:i:s', strtotime($leaderboard->tournament_date)),
            'spectator'         => (new SpectatorModel)->getSpectatorByTournament($leaderboard->id_leaderboard),
            'game' => [
                'name'    => $leaderboard->game->name,
                'logo'    => (new \App\Helpers\MyApps)->cdn($leaderboard->game->logo, 'encrypt', 'game'),
            ],
        ];
    }


    public function ticketInfo(LeaderboardModel $leaderboard){
        $ticket = new TicketValidation;
        $getTicket = $ticket->getTournamentTicketInfo($leaderboard->id_leaderboard, 'Leaderboard');

        if($getTicket['ticket_status'] == true){
        $buttonValidation = $ticket->buttonValidation($getTicket['data']['game_id'], $getTicket['data']['game_option']);

        //check button validation

            $getTicket['data']['user_detail'] = $buttonValidation['user_detail'];


        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => $buttonValidation['button'] ,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];

     }else{
        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => null,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];
     }

        return $dataTicket;
    }




    // - Get Header Information
    public function headInfo(LeaderboardModel $leaderboard, $slotsAvailable, $slotsFilled)
    {
        $register_fee      = (object)(new CheckTournamentPaymentMethod)->LeaderboardPaymentMethod($leaderboard->id_leaderboard);
        $tournamentStatus  = (object) $leaderboard->tournamentStatus($leaderboard->status_tournament);

        $head_info = [
            'id_tournament'      => $leaderboard->id_leaderboard,
            'tournament_name'    => $leaderboard->tournament_name,
            'tournament_date'    => date('Y-m-d H:i:s', strtotime($leaderboard->tournament_date)),
            'is_solo_tournament' => (($leaderboard->game_option == 1) ? true : false ),
            'game' => [
                'name'    => $leaderboard->game->name,
                'logo'    => (new \App\Helpers\MyApps)->cdn($leaderboard->game->logo, 'encrypt', 'game'),
                'url'    => $leaderboard->game->url
            ],
            'register_end'         => date('Y-m-d H:i:s', strtotime($leaderboard->regist_end)),
            'register_open'        => date('Y-m-d H:i:s', strtotime($leaderboard->regist_start)),
            'register_fee'      => $register_fee,
            'status'        => [
                'text'      => $slotsFilled == $slotsAvailable ? (($tournamentStatus->text != trans('tournament.status.in_progress') && $tournamentStatus->text != trans('tournament.status.complete')) ? trans('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                'number'    => $slotsFilled == $slotsAvailable ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
            ],
        ];

        $is_full = ($slotsAvailable != 0 && $slotsFilled != 0) ? ($slotsAvailable / $slotsFilled) : 0;

        $head_info['button_join'] = JoinTournamentButton::getStatus($leaderboard->status_tournament, $is_full, $leaderboard->game->id_game_list, $leaderboard->id_leaderboard,'leaderboard', $leaderboard);

        if (($leaderboard->join_background)) {
            $leaderboard->custom_background = (new MyApps)->cdn($leaderboard->join_background, 'encrypt', 'banner_tournament');
            $head_info['background']        = $leaderboard->custom_background;
        }

        switch ($leaderboard->prize_type) {
            case 'dynamic':
                $get_prizes = LeaderboardPrizePoolModel::select('sequence', 'prize')->where('id_leaderboard', $leaderboard->id_leaderboard)->get();
                $total_prizes = 0;
                foreach ($get_prizes as $key => $p) {
                    $total_prizes += $p->prize;
                }
                $head_info['total_prizes']       = $total_prizes + $leaderboard->most_kill;
                break;
            default:
                $head_info['total_prizes']       = ($leaderboard->firstprize + $leaderboard->secondprize + $leaderboard->thirdprize + $leaderboard->most_kill);
                break;
        }



        if (($leaderboard->stream_url)) {
            $head_info['live_stream'] = $leaderboard->stream_url;
        }
        //live stream v2
        $liveStream = $this->LiveStreaming($leaderboard->id_leaderboard);
        $head_info['live_stream_v2'] = $liveStream != false? $liveStream : null;
        if($leaderboard->is_custom_prize == 1){
            $customPrize = CustomPrizesModel::select('quantity','id', 'pi_id')->with(['prizeInventory'])
                                            ->where('tournament_id', '=', $leaderboard->id_leaderboard)->get();

            $totalCustomPrize = 0;
            foreach($customPrize as $c){
                $totalCustomPrize += $c->quantity * $c->prizeInventory->valuation;
            }
            if(isset($head_info['total_prizes'])){
                $head_info['total_prizes'] += $totalCustomPrize;
            }else{
                $head_info['total_prizes'] = $totalCustomPrize;
            }
        }
        return $head_info;
    }

    public function LiveStreaming($id_tournament){
        try{
            $get_streming =  TournamentStreamingModel::where('id_tournament',$id_tournament)->get();

            if($get_streming->isEmpty()){
                return false;
            }
            $response   = new Collection($get_streming, new TournamentStreamingTransform);
            $data = (new Manager)->createData($response)->toArray();

            return $data;
        } catch (\Throwable $th) {
          return false;
        }
    }

}
