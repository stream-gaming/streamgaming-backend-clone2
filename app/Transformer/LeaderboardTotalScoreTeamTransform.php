<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class LeaderboardTotalScoreTeamTransform extends TransformerAbstract
{


    public function transform($total_score)
    {

        $result = [
            'id_score_team'   => (string) $total_score->id_leaderboard_score_team,
            'status_group'    => $total_score->status_group,
            'total_match'     => $total_score->total_match,
            'sequence_score'  => $total_score->sequence_score,
            'team_name'       => $total_score->nama_team,
            'team_logo'       => (new MyApps)->cdn($total_score->team_logos, 'encrypt', 'team'),
            'id_team'         => $total_score->id_team,
            'total_kill'      => $total_score->total_kill,
            'total_score'     => $total_score->total_score,
            'total'           => $total_score->total,
        ];

        return $result;
    }
}
