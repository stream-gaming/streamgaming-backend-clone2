<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\Spectator;
use App\Model\SpectatorModel;
use League\Fractal\TransformerAbstract;

class ParticipantTransform extends TransformerAbstract
{


	public function transform(LeaderboardTeamModel $leaderboard_team)
	{

            //participant transformer for squad tournament
            return [
                'team_logo' 	=> (new MyApps)->cdn($leaderboard_team->team_logo, 'encrypt', 'team'),
                'team_name' 	=> $leaderboard_team->team_name,
                'venue' 		=> $leaderboard_team->venue,
                'team_id' 		=> $leaderboard_team->team_id
            ];


	}
}
