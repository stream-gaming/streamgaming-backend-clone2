<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use App\Http\Controllers\Api\GameController;

class GameTransformNewVersion extends TransformerAbstract
{

	public function transform($game)
	{
        $MyApps = new MyApps;
        $otherGame = [];
        $i=0;

        //extract other game
        foreach($game->other_game as $g){
            $otherGame[$i]['full_url'] = route('game.detail', ['url'=>$g->url]);
            $otherGame[$i]['url'] = $g->url;
            $otherGame[$i]['logo'] = $MyApps->cdn($g->logo, 'encrypt', 'game');
			$otherGame[$i]['background_logo1'] = $MyApps->cdn($g->logo_game_list, 'encrypt', 'game');
			$otherGame[$i]['background_logo2'] = $MyApps->cdn($g->logo_account, 'encrypt', 'game');
			$otherGame[$i]['games_on'] = $g->games_on;
			$otherGame[$i]['players'] = $g->total_player;
			$otherGame[$i]['total_player'] =  $g->total_player;
            $otherGame[$i]['total_tournament'] = $g->total_tournament;

            $i++;
        }

        $result = [
            'is_login'	=> Auth::check(),
            'detail' => [
				'id'	=> $game->id_game_list,
				'name'	=> $game->name,
				'content'	=> $game->content,
                'logo'		=> $MyApps->cdn($game->logo, 'encrypt', 'game'),
                'background_logo1'  => $MyApps->cdn($game->logo_game_list, 'encrypt', 'game'),
                'background_logo2'    => $MyApps->cdn($game->logo_account, 'encrypt', 'game'),
				'games_on' 	=> $game->gamesOn($game->games_on),
				'total_player' 		=> $game->total_player,
                'total_tournament'	=> $game->total_tournament

            ],
            'other'	=> $otherGame
        ];


        if (Auth::check()) {
			$result['detail']['is_added'] = (bool) (new GameController)->checkHaveGame($game->id_game_list);
		}

        return $result;
    }
}
