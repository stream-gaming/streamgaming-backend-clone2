<?php

namespace App\Transformer;

use App\Model\GameModel;
use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use League\Fractal\TransformerAbstract;

class UserPreferenceOnboardingTransformer extends TransformerAbstract
{

    protected $includes;
    protected $user;

    public function __construct(array $includes = []){
        $this->includes = $includes;
    }

	public function transform(User $user)
	{
        $this->user = $user;

        $userOnboarding = $this->user->userOnboarding;
        $userGame = $user->game;
        $game = GameModel::where('id_game_list', $userGame[0]->game_id)->first();
        $data = [];

        //check include additional property
        foreach($this->includes as $include){
            if(method_exists(UserPreferenceOnboardingTransformer::class, $include)){
                $data = $this->$include($data);
            }

        }

		return array_merge($data, [
            'preference' => [
                'type' => $userOnboarding->preference,
                'game' => [
                    'id' => $game->id_game_list,
                    'name' => $game->name
                ]
            ]
        ]);

	}

    protected function status($data){
        return array_merge($data, [
            'status' => true
        ]);
    }

    protected function progress($data){
        //Set progress 5 if status finished
        $progress = $this->user->userOnboarding->step + 1; //add 1 to specify front-end component
        if($this->user->userOnboarding->status == OnboardingEnum::FINISHED_STATUS){
            $progress = 5;
        }

        return array_merge($data,[
            'progress' => (string) $progress
        ]);
    }

}
