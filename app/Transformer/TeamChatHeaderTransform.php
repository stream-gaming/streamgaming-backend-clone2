<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TeamModel;
use League\Fractal\TransformerAbstract;

class TeamChatHeaderTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TeamModel $team)
    {
        $myApps = new MyApps();

        return [
            "team_id"      => $myApps->onlyEncrypt($team->team_id),
            'team_name'    => $team->team_name,
            'logo'         => $myApps->cdn($team->team_logo, 'encrypt', 'team'),
            'member'       => $team->countMember()
        ];
    }
}
