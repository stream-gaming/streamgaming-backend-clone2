<?php

namespace App\Transformer;

use App\Model\User;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\TeamRequestModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class MyTeamTransform extends TransformerAbstract
{

    public function transform(TeamModel $team)
    {
        $request_team = (new TeamRequestModel)->countRequestByTeamId($team->team_id);

        $result = [
            'game'          => $team->game->name,
            'team_name'     => $team->team_name,
            'description'   => $team->team_description,
            'is_banned'     => ($team->is_banned != '0') ? true : false,
            'ban_until'     => $team->ban_until,
            'notif_request' => $request_team,
            'is_leader'     => ($team->leader_id == Auth::user()->user_id) ? true : false,
            'logo'          => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
            'game'          => [
                'id_game' => $team->game_id,
                'name'    => $team->game->name,
            ],
            'slot'          => [
                'total'      => $team->game->player_on_team + $team->game->substitute_player,
                'primary'    => $team->game->player_on_team,
                'substitute' => $team->game->substitute_player,
                'member'     => $team->countMember()
            ],
            'url'           => (new MyApps)->onlyEncrypt($team->team_id),
            'created_at'    => $team->created_at
        ];

        return $result;
    }

    public function detail(TeamModel $team)
    {
        $request_team = (new TeamRequestModel)->countRequestByTeamId($team->team_id);

        $result = [
            'team_id'       => (string) $team->team_id,
            'game'          => $team->game->name,
            'game_id'       => (string) $team->game->id_game_list,
            'description'   => $team->team_description,
            'leader'        => $team->leader->username,
            'is_banned'     => ($team->is_banned) ? true : false,
            'ban_until'     => $team->ban_until,
            'is_leader'     => ($team->leader_id == Auth::user()->user_id) ? true : false,
            'team_name'     => $team->team_name,
            'venue'         => $team->venue,
            'notif_request' => $request_team,
            'logo'          => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
            'slot'          => [
                'total'     => $team->game->player_on_team + $team->game->substitute_player,
                'primary'    => $team->game->player_on_team,
                'substitute' => $team->game->substitute_player,
                'member'    => $team->countMember()
            ],
            'url'           => (new MyApps)->onlyEncrypt($team->team_id),
            'created_at'    => $team->created_at
        ];

        for ($i = 0; $i < count($team->primaryPlayer()); $i++) {
            $User = User::find($team->primaryPlayer()[$i]);
            $username = IdentityGameModel::where('users_id', $User->user_id)
                ->where('game_id', $team->game_id)
                ->first();

            $result['member']['primary'][$i] = [
                'users_id'  => $User->user_id,
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => (new MyApps)->getImage($team->primaryPlayer()[$i]),
                'bio'       => $User->bio,
                'birthday'  => $User->tgl_lahir,
                'age'       => Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username->username_ingame,
                'is_banned'     => (bool) $username->is_banned,
                'ban_until'     => $username->ban_until,
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0,
                'is_primary' => 1,
                'socmed'    => AccountTransform::socmed($User->user_id),
                'role'      => AccountTransform::getRoleGame($User, $team->game_id)
            ];
        }

        for ($ii = 0; $ii < count($team->substitutePlayer()); $ii++) {
            $User = User::find($team->substitutePlayer()[$ii]);
            $username = IdentityGameModel::where('users_id', $User->user_id)
                ->where('game_id', $team->game_id)
                ->first();

            $result['member']['substitute'][$ii] = [
                'users_id'  => $User->user_id,
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => (new MyApps)->getImage($team->substitutePlayer()[$ii]),
                'bio'       => $User->bio,
                'birthday'  => $User->tgl_lahir,
                'age'       => Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username->username_ingame,
                'is_banned'     => (bool) $username->is_banned,
                'ban_until'     => $username->ban_until,
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0,
                'is_primary' => 0,
                'socmed'    => AccountTransform::socmed($User->user_id),
                'role'      => AccountTransform::getRoleGame($User, $team->game_id)
            ];
        }

        for ($iii = 0; $iii < count($team->allPlayer()); $iii++) {
            $User = User::find($team->allPlayer()[$iii]);
            $username = IdentityGameModel::where('users_id', $User->user_id)
                ->where('game_id', $team->game_id)
                ->first();

            $result['all-member'][$iii] = [
                'users_id'  => $User->user_id,
                'username'  => $User->username,
                'fullname'  => $User->fullname,
                'image'     => (new MyApps)->getImage($team->allPlayer()[$iii]),
                'bio'       => $User->bio,
                'birthday'  => $User->tgl_lahir,
                'age'       => Carbon::parse($User->tgl_lahir)->age,
                'username_game' => $username->username_ingame,
                'is_banned'     => (bool) $username->is_banned,
                'ban_until'     => $username->ban_until,
                'is_leader' => ($team->leader_id == $User->user_id) ? 1 : 0,
                'is_primary' => ($team->isPrimaryPlayer($User->user_id)) ? 1 : 0,
                'socmed'    => AccountTransform::socmed($User->user_id),
                'role'      => AccountTransform::getRoleGame($User, $team->game_id)
            ];
        }

        $request        = TeamRequestModel::where('team_id', $team->team_id)->orderByRaw('status = 0, status')->get();
        $response       = new Collection($request, new TeamRequestTransform);

        $result['request'] = current((new Manager())->createData($response)->toArray());

        return $result;
    }
}
