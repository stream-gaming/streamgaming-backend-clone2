<?php

namespace App\Transformer;

use App\Model\TournamenGroupModel;
use League\Fractal\TransformerAbstract;

class ScheduleChallongeTransform extends TransformerAbstract
{
    public function transform($data){
        foreach ($data as $key => $value) {
            $id_team1  =  $value['match']['player1_id'];
            $id_team2  =  $value['match']['player2_id'];
            if(!empty($id_team1)){
                $dataTeam1 = TournamenGroupModel::where('id_participant_challonge',$id_team1)->with(['Team'])->first();
            }else{
                $dataTeam1 = '';
            }
            if(!empty($id_team2)){
                $dataTeam2 = TournamenGroupModel::where('id_participant_challonge',$id_team2)->with(['Team'])->first();
            }else{
                $dataTeam2 = '';
            }
            $score = $value['match']['scores_csv'];
            $explode = (empty($score) ? null : explode('-',$score));
            $round = $value['match']['round'];
            $resp[] = [
                "round"     => $round,
                "matchs"    =>  [
                    "team_1" => [
                        "name"  => (empty($dataTeam1->team->team_name)? null : $dataTeam1->team->team_name),
                        "logo"  => (empty($dataTeam1->team->team_logo)? null : $dataTeam1->team->team_logo),
                        "city"  =>  (empty($dataTeam1->team->venue)? null : $dataTeam1->team->venue),
                        "score" => (empty($explode)? $explode : $explode[0])
                    ],
                    "team_2" => [
                        "name"  => (empty($dataTeam2->team->team_name)? null : $dataTeam2->team->team_name),
                        "logo"  => (empty($dataTeam2->team->team_logo)? null : $dataTeam2->team->team_logo),
                        "city"  =>  (empty($dataTeam2->team->venue)? null : $dataTeam2->team->venue),
                        "score" => (empty($explode)? $explode : $explode[1])
                    ]
                ],
                "date"      =>  (empty($value['match']['scheduled_time'])? null : $value['match']['scheduled_time'])
            ];
        }
        return $resp;
    }
}
