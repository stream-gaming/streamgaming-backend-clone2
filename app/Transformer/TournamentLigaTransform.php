<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\TournamentLiga;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use League\Fractal\Manager;


class TournamentLigaTransform extends TransformerAbstract
{
    private $asset;
    private $manager;
    private $routename;
    public function __construct()
    {
        $this->asset   = new MyApps;
        $this->manager = new Manager();
        $this->routename = "tournament.list.liga";
    }

    public function transform(TournamentLiga $data)
    {
        $tournament = new Collection($data->tournament, (new TournamentListTransform));
        return [
            'nama_liga'     => $data->nama_liga,
            'url'           => $this->asset->getPathRoute($this->routename, ['url' => $data->url]),
            'tournament'    => current($this->manager->createData($tournament)->toArray())
        ];
    }

    public function single(TournamentLiga $data)
    {
        $tournament     = new Collection($data->tournament, (new TournamentListTransform));
        $tournamentdata = current($this->manager->createData($tournament)->toArray());

        $description     = new Collection($data->tabmenu,(new TournamentMenuLigaTransform));
        $descriptiondata = current($this->manager->createData($description)->toArray());

        return [
            "title"         => $data->nama_liga,
            "description"   => $data->deskripsi,
            "liga_logo"     => $data->logo_liga,
            "organizer"     => [
                "name"      => $data->nama_penyelenggara,
                "logo"      => $data->logo_penyelenggara
                ],
            'background'    => $data->background_liga,
            'banner'        => $data->banner_liga,
            "total_tournament"  => $data->jumlah,
            "tournament_date"   => [
                "date_start"    => $data->start_tour,
                "date_end"      => $data->end_tour
            ],
            "tab_data" => [
                [
                    "tab_name" => Lang::get('tournament.tabname.all'),
                    "url"      => 'all'
                ],
                [
                    "tab_name" => Lang::get('tournament.tabname.thisweek'),
                    "url"      => 'this_week'
                ],
                [
                    "tab_name" => Lang::get('tournament.tabname.upcoming'),
                    "url"      => 'upcoming'
                ],
                [
                    "tab_name" => Lang::get('tournament.tabname.finished'),
                    "url"      => 'finished'
                ]
            ],
            "tab_description"   => $descriptiondata,
            'tournament'        => $tournamentdata
        ];
    }
}
