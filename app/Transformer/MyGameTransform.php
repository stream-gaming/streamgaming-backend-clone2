<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Http\Controllers\Api\GameController;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class MyGameTransform extends TransformerAbstract
{
	public function transform($data)
	{
		$MyApps = new MyApps();

		return [
			'id'	=> $data->id,
			'url'	=> $data->game->url,
			'game'	=> [
				'id_game'	=> $data->game_id,
				'name'		=> $data->game->name,
				'logo'		=> $MyApps->cdn($data->game->logo_account, 'encrypt', 'game'),
				'logo2'				=> $MyApps->cdn($data->game->logo, 'encrypt', 'game'),
				'background_logo1'  => $MyApps->cdn($data->game->logo_game_list, 'encrypt', 'game'),
				'background_logo2'	=> $MyApps->cdn($data->game->logo_account, 'encrypt', 'game'),
			],
			'id_ingame'			=> $data->id_ingame,
			'username_ingame'	=> $data->username_ingame,
			'is_banned'			=> (bool) $data->is_banned,
			'ban_until'			=> $data->ban_until,
			'role'	=> current(fractal()
				->collection($data->role)
				->transformWith(new GameRoleTransform())
				->toArray())
		];
	}
}
