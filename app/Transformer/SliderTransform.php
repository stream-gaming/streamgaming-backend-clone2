<?php

namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class SliderTransform extends TransformerAbstract
{
    public function transform($data)
    {
        return [
            'id'    => (int) $data->id,
            'title' => $data->title,
            'image' => $data->image,
            'status'    => $data->status == 1 ? true : false,
            'pop_up'    => $data->status == 2 ? true : false,
            'url'       => $data->url,
            'in_app'    => (bool) $data->in_app == 1 ? true : false,
            'last_updated'  => $data->updated_at
        ];
    }
}
