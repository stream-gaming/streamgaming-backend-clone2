<?php

namespace App\Transformer;

use App\Model\LeaderboardScoreTeamModel;
use League\Fractal\TransformerAbstract;

class TeamHistoryTransformLeaderboard extends TransformerAbstract
{
    public function transform(LeaderboardScoreTeamModel $rows){

        $alphabet = array(
            "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
            "BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
            "CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM","CN","CO","CP","CQ","CR","CS","CT","CU","CV","CW","CX","CY","CZ",
            "DA","DB","DC","DD","DE","DF","DG","DH","DI","DJ","DK","DL","DM","DN","DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX","DY","DZ",
            "EA","EB","EC","ED","EE","EF","EG","EH","EI","EJ","EK","EL","EM","EN","EO","EP","EQ","ER","ES","ET","EU","EV","EW","EX","EY","EZ",
            "FA","FB","FC","FD","FE","FF","FG","FH","FI","FJ","FK","FL","FM","FN","FO","FP","FQ","FR","FS","FT","FU","FV","FW","FX","FY","FZ",
            "GA","GB","GC","GD","GE","GF","GG","GH","GI","GJ","GK","GL","GM","GN","GO","GP","GQ","GR","GS","GT","GU","GV","GW","GX","GY","GZ",
            "HA","HB","HC","HD","HE","HF","HG","HH","HI","HJ","HK","HL","HM","HN","HO","HP","HQ","HR","HS","HT","HU","HV","HW","HX","HY","HZ",
            "IA","IB","IC","ID","IE","IF","IG","IH","II","IJ","IK","IL","IM","IN","IO","IP","IQ","IR","IS","IT","IU","IV","IW","IX","IY","IZ",
            "JA","JB","JC","JD","JE","JF","JG","JH","JI","JJ","JK","JL","JM","JN","JO","JP","JQ","JR","JS","JT","JU","JV","JW","JX","JY","JZ",
            "KA","KB","KC","KD","KE","KF","KG","KH","KI","KJ","KK","KL","KM","KN","KO","KP","KQ","KR","KS","KT","KU","KV","KW","KX","KY","KZ"
        );



            //change the 100th index on alphabet incase this tournament is not full costum (to indicate the final match)
            if($rows->Leaderboard->tournament_category != 3){
                $alphabet[100] = 'Final';
            }else{
            //change the 1000th index on alphabet incase this tournament is full costum (to indicate the final match)
                $alphabet[1000] = 'Final';
            }


            //Serializing the round of leaderboard tournament
            $roundLeaderboard = $rows->qualification_sequence > 0 ? "Stage ".$rows->qualification_sequence." - Group ".$alphabet[$rows->group_sequence]." - Match ".($rows->match_sequence+1): "Group ".$alphabet[$rows->group_sequence]." - Match ".($rows->match_sequence+1);

            $result = [
                "pertandingan" => "N/A",
                "tanggal"      => $rows->date == null ? '--' : $rows->date,
                "nama_tournament"         => $rows->Leaderboard->tournament_name,
                "round"        => $roundLeaderboard,
                "peringkat"    => $rows->LeaderboardScoreSequence->urutan
            ];

            return $result;

    }
}
