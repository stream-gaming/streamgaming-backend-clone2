<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\LeaderboardMostKillModel;
use League\Fractal\TransformerAbstract;

class LeaderboardScoreMostKillSoloTransform extends TransformerAbstract
{

    public function transform($mostkill)
    {

        $result = [
            'player_name'     => $mostkill->player_name,
            'team_logo'       => (!empty($mostkill->image) ? (new MyApps)->cdn(['user_id' => $mostkill->id_team,'image' => $mostkill->image], 'encrypt', 'users') : (new MyApps)->DefaultUserPath()),
            'id_team'         => $mostkill->id_team,
            'nama_team'       => $mostkill->nama_team,
            'total_kill'      => $mostkill->total_kill,
        ];

        return $result;
    }
}
