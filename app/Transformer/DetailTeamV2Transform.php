<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\TeamNameHistoryModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class DetailTeamV2Transform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TeamModel $team)
    {
        return [];

    }

    public function detail(TeamModel $team, $is_in_team=false)
    {

        $user            = Auth::check() ? Auth::user() : false;
        $response_team   = $this->detailTeam($team, $is_in_team);

        $user_id = $user ? $user->user_id : null;

        $user_request = TeamRequestModel::where('team_id', $team->team_id)
                                        ->where('users_id', $user_id)
                                        ->where('status', 1)
                                        ->count();

        return [
            'id'         => $user ? $user->user_id : null,
            'fullname'   => $user ? $user->fullname : null,
            'username'   => $user ? $user->username : null,
            'is_in_team' => $is_in_team,
            'is_waiting' => boolval($user_request),
            'team'       => $response_team
        ];
    }

    public function detailTeam(TeamModel $team, $is_in_team=false)
    {
        $MyApps = new MyApps;
        $request_team = (new TeamRequestModel)->countRequestByTeamId($team->team_id);

        $date = new Carbon();
        $dayinmonth = $date->now()->subDays(30);

        $limited = TeamNameHistoryModel::where('team_id',$team->team_id)
                                     ->whereDate('created_at','>=',$dayinmonth)
                                     ->orderBy('created_at','DESC')
                                     ->first();
        $diffdays = 0;

        if($limited != null){

            $diffdays = $date->parse($limited->created_at)->diffInDays($dayinmonth);
        }

        $user_id = Auth::check() ? Auth::user()->user_id : null;

        $result = [
            'team_id'       => (string) $team->team_id,
            'game'          => $team->game->name,
            'game_logo'     => $MyApps->cdn($team->game->logo, 'encrypt', 'game'),
            'game_url'     => $team->game->url,
            'description'   => $team->team_description,
            'leader'        => $team->leader->username,
            'is_banned'     => ($team->is_banned) ? true : false,
            'ban_until'     => $team->ban_until,
            'is_leader'     => ($team->leader_id == $user_id) ? true : false,
            'team_name'     => $team->team_name,
            'venue'         => $team->venue,
            'notif_request' => $request_team,
            'logo'          => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
            'slot'          => [
                'total'     => $team->game->player_on_team + $team->game->substitute_player,
                'primary'    => $team->game->player_on_team,
                'substitute' => $team->game->substitute_player,
                'member'    => $team->countMember()
            ],
            'url'           => (new MyApps)->onlyEncrypt($team->team_id),
            'created_at'    => $team->created_at,
            'day'           => $diffdays,
            'member'        => [
                'primary'    => [],
                'substitute' => [],
            ],
            'all_member' => [],
            'request'   => []
        ];

        $users_primary = User::with(['game' => function($q)use($team){
            $q->where('game_id',$team->game_id);
        }])->whereIn('user_id',$team->pengurutanTeamPrimary())->get();

        foreach ($users_primary as $user) {
            $result['member']['primary'][] = [
                'users_id'  => $user->user_id,
                'username'  => $user->username,
                'fullname'  => $user->fullname,
                'image'     => (new MyApps)->getImage($user->user_id),
                'bio'       => $user->bio,
                'birthday'  => $user->tgl_lahir,
                'age'       => Carbon::parse($user->tgl_lahir)->age,
                'username_game' => $user->game[0]->username_ingame,
                'is_banned'     => (bool) $user->game[0]->is_banned,
                'ban_until'     => $user->game[0]->ban_until,
                'is_leader' => ($team->leader_id == $user->user_id) ? 1 : 0,
                'is_primary'=> 1,
                'socmed'    => AccountTransform::socmed($user->user_id),
                'role'      => AccountTransform::getRoleGame($user, $team->game_id)
            ];
        }

        $users_subtitute = User::with(['game' => function($q)use($team){
            $q->where('game_id',$team->game_id);
        }])->whereIn('user_id',$team->substitutePlayer())->get();

        foreach ($users_subtitute as  $user) {
            $result['member']['substitute'][] = [
                'users_id'  => $user->user_id,
                'username'  => $user->username,
                'fullname'  => $user->fullname,
                'image'     => (new MyApps)->getImage($user->user_id),
                'bio'       => $user->bio,
                'birthday'  => $user->tgl_lahir,
                'age'       => Carbon::parse($user->tgl_lahir)->age,
                'username_game' => $user->game[0]->username_ingame,
                'is_banned'     => (bool) $user->game[0]->is_banned,
                'ban_until'     => $user->game[0]->ban_until,
                'is_leader' => ($team->leader_id == $user->user_id) ? 1 : 0,
                'is_primary'=> 0,
                'socmed'    => AccountTransform::socmed($user->user_id),
                'role'      => AccountTransform::getRoleGame($user, $team->game_id)
            ];
        }

        foreach ($users_primary as $user) {
            $result['all_member'][] = [
                'users_id'  => $user->user_id,
                'username'  => $user->username,
                'fullname'  => $user->fullname,
                'image'     => (new MyApps)->getImage($user->user_id),
                'bio'       => $user->bio,
                'birthday'  => $user->tgl_lahir,
                'age'       => Carbon::parse($user->tgl_lahir)->age,
                'username_game' => $user->game[0]->username_ingame,
                'is_banned'     => (bool) $user->game[0]->is_banned,
                'ban_until'     => $user->game[0]->ban_until,
                'is_leader' => ($team->leader_id == $user->user_id) ? 1 : 0,
                'is_primary'=> ($team->isPrimaryPlayer($user->user_id)) ? 1 : 0,
                'socmed'    => AccountTransform::socmed($user->user_id),
                'role'      => AccountTransform::getRoleGame($user, $team->game_id)
            ];
        }

        foreach ($users_subtitute as $user) {
            $result['all_member'][] = [
                'users_id'  => $user->user_id,
                'username'  => $user->username,
                'fullname'  => $user->fullname,
                'image'     => (new MyApps)->getImage($user->user_id),
                'bio'       => $user->bio,
                'birthday'  => $user->tgl_lahir,
                'age'       => Carbon::parse($user->tgl_lahir)->age,
                'username_game' => $user->game[0]->username_ingame,
                'is_banned'     => (bool) $user->game[0]->is_banned,
                'ban_until'     => $user->game[0]->ban_until,
                'is_leader' => ($team->leader_id == $user->user_id) ? 1 : 0,
                'is_primary'=> ($team->isPrimaryPlayer($user->user_id)) ? 1 : 0,
                'socmed'    => AccountTransform::socmed($user->user_id),
                'role'      => AccountTransform::getRoleGame($user, $team->game_id)
            ];
        }

        if($is_in_team){
            $request        = TeamRequestModel::where('team_id', $team->team_id)->orderByRaw('status = 0, status')->get();
            $response       = new Collection($request, new TeamRequestTransform);

            $result['request'] = current((new Manager())->createData($response)->toArray());
        }

        return $result;
    }
}
