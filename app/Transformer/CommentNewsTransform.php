<?php

namespace App\Transformer;

use Illuminate\Support\Str;
use App\Model\NewsCommentModel;  
use League\Fractal\TransformerAbstract;
 
class CommentNewsTransform extends TransformerAbstract
{
	public function transform (NewsCommentModel $comment)
	{
		return [
			'comment_id'	=> $comment->comment_id,
			'comment'		=> $comment->comment,
			'user'			=> [
				'name'		=> $comment->author->username, 
				'picture'	=> (new \App\Helpers\MyApps)->getImage($comment->authorDetail->user_id)
			],
			'created_at'	=> $comment->date,
			'reply'			=> $this->replied($comment->replied($comment->comment_id)),
		];
	}

	public function replied($data)
	{
		for($i=0;$i<count($data);$i++) {
			$data[$i] = [
				'comment_id'	=> $data[$i]->comment_id,
				'comment'		=> $data[$i]->comment,
				'user'			=> [
					'name'		=> $data[$i]->author->username, 
					'picture'	=> (new \App\Helpers\MyApps)->getImage($data[$i]->authorDetail->user_id)
				],
				'created_at'	=> $data[$i]->date,
			];
		}
		return $data;
	}
} 