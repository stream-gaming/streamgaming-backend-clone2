<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\User;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class DetailAllInvoiceTranform extends TransformerAbstract
{
    public function __construct()
    {

        $this->asset = new MyApps;
        $this->date =  new Carbon;

    }

    public function transform($data){
        // $nama = TeamModel::where('team_id',$data['id_team'])->select('team_name')->first();
        if($data['id_team'] == $data['juara1']){
            $juara = '1';
        }else if($data['id_team'] == $data['juara2']){
            $juara = '2';
        }else if($data['id_team'] == $data['juara3']){
            $juara = '3';
        }
        $tour = TournamentModel::find($data['id_tournament']);
        $user = User::find($data['player_id']);
        if(is_null($data['id_team'])){
            $team = null;
        }else{
            $team = $tour->game_option == 1 ? null : TeamModel::find($data['id_team'])->team_name;
        }

        $result = [
            'id_tournament'     =>  $data['id_tournament'],
            'name_tour'         =>  $tour->tournament_name,
            'juara'             =>  $juara,
            'id_team'           =>  $data['id_team'],
            "nama_team"         =>  $team,
            'id_player'         =>  $data['player_id'],
            'nama_player'       =>  $user->username,
            'email'             =>  !is_null($user->email) ? $user->email : null,
            'metode'            =>  $data['transaction_type'],
            'jumlah'            =>  $data['balance'],
            'waktu'             =>  $data['date'],
            'status'            =>  "Sukses"
        ];
        // return ($data->winner_tour_sequence != null ? $this->winnerTourSequence($data) : $this->winnerTour($data));
        return $result;
    }

}
