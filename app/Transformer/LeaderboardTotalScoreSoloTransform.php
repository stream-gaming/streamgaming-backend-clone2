<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use League\Fractal\TransformerAbstract;

class LeaderboardTotalScoreSoloTransform extends TransformerAbstract
{


    public function transform($total_score)
    {

        $result = [
            'id_score_team'   => (string) $total_score->id_leaderboard_score_team,
            'status_group'    => $total_score->status_group,
            'total_match'     => $total_score->total_match,
            'sequence_score'  => $total_score->sequence_score,
            'team_name'       => $total_score->nama_team,
            'team_logo'       => (!empty($total_score->image) ? (new MyApps)->cdn(['user_id' => $total_score->id_team,'image' => $total_score->image], 'encrypt', 'users') : (new MyApps)->DefaultUserPath()),
            'id_team'         => $total_score->id_team,
            'total_kill'      => $total_score->total_kill,
            'total_score'     => $total_score->total_score,
            'total'           => $total_score->total,
        ];

        return $result;
    }
}
