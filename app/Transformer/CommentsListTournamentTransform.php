<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Http\Controllers\Api\Comments\TournamentCommentsController;
use App\Model\CommentsModel;
use App\Traits\MyTraits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class CommentsListTournamentTransform extends TransformerAbstract
{
    use MyTraits;

    public function __construct()
    {
        $this->MyApps = new MyApps();
    }

    public function transform($comment)
    {
        $user = Auth::check() ? Auth::user()->user_id : null;

        $data = [
            'comments_id'   => $comment->id,
            'role'          => $comment->user_status,
            'comment'       => $comment->comment,
            'created_at'    => $comment->created_at,

        ];

        if (!is_null($comment->user)) {
            $data['user'] = [
                'user_id'    => $comment->user->user_id,
                'full_name'  => $comment->user->fullname,
                'username'   => $comment->user->username,
                'picture'    => $this->getImage($comment->user->detail, '0'),
            ];

            // $data['action_menu'] = [
            //     "delete"   => $user == $comment->user->user_id ? true : false,
            // ];
        } else {
            $data['user'] = [
                'user_id'    => $comment->admin->stream_id,
                'full_name'  => $comment->admin->fullname,
                'username'   => $comment->admin->username,
                'picture'    => $this->getImage($comment->admin->detail, '1'),
            ];

            // $data['action_menu'] = [
            //     "delete"   => false,
            // ];
        }

        $reply = CommentsModel::with(['user', 'admin', 'reply' => function ($q) {
            $q->leftJoin('users', function ($join) {
                $join->on('comments.user_id', '=', 'users.user_id');
            });
            $q->leftJoin('management_access', function ($join) {
                $join->on('comments.user_id', '=', DB::raw('management_access.stream_id COLLATE utf8mb4_unicode_ci'));
            });
            $q->selectRaw('comments.*, users.* , management_access.stream_id, management_access.username as adm_username, management_access.fullname as adm_fullname');
        }, 'parent' => function ($q) {
            $q->leftJoin('users', function ($join) {
                $join->on('comments.user_id', '=', 'users.user_id');
            });
            $q->leftJoin('management_access', function ($join) {
                $join->on('comments.user_id', '=', DB::raw('management_access.stream_id COLLATE utf8mb4_unicode_ci'));
            });
            $q->selectRaw('comments.*, users.* , management_access.stream_id, management_access.username as adm_username, management_access.fullname as adm_fullname');
        }])
            ->where('commentable_type', 'tournament')
            ->where('parent_id', $comment->id)
            ->orderBy('created_at', 'ASC');

        // $data['more'] = $reply->count() > TournamentCommentsController::LIMIT_REPLY ? true : false;


        $data['replies'] = collect(current(fractal()
            ->collection($reply->get())
            ->transformWith(new CommentsReplyListTournamentTransform)
            ->toArray()));

        $data['replies'] = $data['replies']->mapWithKeys(function ($list, $key) {
            return [$list['comments_id'] => $list];
        });

        return $data;
    }
}
