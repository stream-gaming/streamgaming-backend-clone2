<?php

namespace App\Transformer;

use App\Helpers\MyApps;
use App\Model\FaqsModel;
use League\Fractal\TransformerAbstract;

class FaqsSearchTransform extends TransformerAbstract
{
    public function transform(FaqsModel $data)
    {

        return [
            'title'            => $data->title,
            'full_url'          => $data->url == null ? null : route('faqs.content', $data->url),
            'url'               => $data->url == null ? null : $data->url
        ];

    }
}
