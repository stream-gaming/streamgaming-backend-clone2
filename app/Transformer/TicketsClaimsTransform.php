<?php

namespace App\Transformer;

use App\Helpers\Ticket\TicketRole;
use App\Model\GameModel;
use App\Model\HistoryTicketsModel;
use App\Model\TicketsModel;
use App\Traits\TicketClaimsTrait;
use App\Traits\UserHelperTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use PhpParser\Node\Stmt\Switch_;

class TicketsClaimsTransform extends TransformerAbstract
{
    use TicketClaimsTrait;

    public function __construct($type = "user", $team_id = null)
    {
        $this->type    = $type;
        $this->team_id = $team_id;
    }

    public function transform($ticket)
    {
        $team_id = $this->type == "user" ? null : $this->team_id[$ticket->competition];
        $has_claim = $this->getClaim($this->type, $ticket, $team_id);

        $ticketType = TicketRole::checkTicketType($ticket->type);
        $ticketGame = $ticket->competition == 'all' ? 'ALL GAME' :
        GameModel::where('id_game_list', $ticket->competition)->firstOr(function(){
            return null;
        })->name;

        for ($i = 0; $i < $ticket->max_claim; $i++) {
            $is_available =  ($ticket->max_given != 0 ? (($ticket->total_given + $i) >= $ticket->max_given ? false : true) : true);

            $claim_today[$i] = [
                'type'          => "claim_today",
                'is_available'  => $has_claim > 0 ? false : $is_available,
                'ticket_id'     => $ticket->ticket_id,
                'name'          => $ticket->name,
                'expired'       => date('Y-m-d H:i', strtotime($ticket->expired)),
                'period'        => $ticket->period,
                'is_claim'      => $has_claim > 0 ? true : false,
                'claim_until'   => $ticket->claimDate(),
                'image'         => $ticket->image,
                'ticket_type'   => $ticketType,
                'game'          => $ticketGame
            ];
            $has_claim--;
        }

        //- Klaim Tiket yang sudah lewat batas Klaim 1 Hari Sebelumnya
        if ($this->cekJunk($ticket, 1)) {
            $claim_today = collect([$claim_today, $this->getClaimJunk($ticket)])->collapse()->all();
        }

        return $claim_today;
    }

    //- Get Tiket yang Belum diklaim setelah batas tanggal klaim
    public function getClaimJunk($ticket)
    {
        $has_claim_junk = $this->hasClaimJunk($ticket);

        $ticketType = TicketRole::checkTicketType($ticket->type);
        $ticketGame = $ticket->competition == 'all' ? 'ALL GAME' :
        GameModel::where('id_game_list', $ticket->competition)->firstOr(function(){
            return null;
        })->name;

        for ($i = 0; $i < $ticket->max_claim; $i++) {
            $claim_today[$i] = [
                'type'          => "claim_junk",
                'is_available'  => false,
                'ticket_id'     => $ticket->ticket_id,
                'name'          => $ticket->name,
                'expired'       => date('Y-m-d H:i', strtotime($ticket->expired)),
                'period'        => $ticket->period,
                'is_claim'      => $has_claim_junk > 0 ? true : false,
                'claim_until'   => $ticket->claimJunk(),
                'image'         => $ticket->image,
                'ticket_type'   => $ticketType,
                'game'          => $ticketGame
            ];
            $has_claim_junk--;
        }
        return $claim_today;
    }

}
//keep
