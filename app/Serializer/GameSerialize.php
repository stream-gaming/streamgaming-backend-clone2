<?php 

namespace App\Serializer;

use Illuminate\Support\Facades\Auth;
use League\Fractal\Serializer\ArraySerializer;
// use League\Fractal\Serializer\SerializerAbstract;

class GameSerialize extends ArraySerializer 
{

	public function collection($resourceKey, array $data)
    {
        return ['data' => $data, 'available' => null];
    }

    public function item($resourceKey, array $data)
    {
        return ['data' => $data];
    }

    public function null()
    {
        return ['data' => []];
    }

}