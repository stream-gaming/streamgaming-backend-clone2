<?php 

namespace App\Serializer;

use Illuminate\Support\Facades\Auth;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Pagination\PaginatorInterface;
// use League\Fractal\Serializer\SerializerAbstract;

class NewsSerialize extends ArraySerializer 
{

	public function collection($resourceKey, array $data)
    {
        return ['data' => $data];
    }

    public function item($resourceKey, array $data)
    {
        return ['data' => $data];
    }

    public function null()
    {
        return ['data' => []];
    }

    public function paginator(PaginatorInterface $paginator)
    {
        $currentPage = (int) $paginator->getCurrentPage();
        $lastPage = (int) $paginator->getLastPage();

        $pagination = [
            'total' => (int) $paginator->getTotal(),
            'count' => (int) $paginator->getCount(),
            'per_page' => (int) $paginator->getPerPage(),
            'current_page' => $currentPage,
            'total_pages' => $lastPage,
        ];

        $pagination['links'] = [];
        $pagination['links']['default'] = route('news');

        if ($currentPage > 1) {
            $pagination['links']['previous'] = $paginator->getUrl($currentPage - 1);
        }

        if ($currentPage < $lastPage) {
            $pagination['links']['next'] = $paginator->getUrl($currentPage + 1);
        }

        return ['pagination' => $pagination];
    }

}