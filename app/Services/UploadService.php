<?php

namespace App\Services;

use Exception;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UploadService
{
    protected $path;
    protected $mini;

    public function __construct()
    {
        $this->path = config('app.aws_s3_folder');
    }

    public function upload($path, $file, $old_image = null, $options = null)
    {
        try {
            $image_name = uniqid() . '.' . $file->getClientOriginalExtension();

            $resize_image = Image::make($file->getRealPath());
            if ($options['options'] == 'resize') {
                $resize_image->resize($options['width'], $options['height']);
            } else if ($options['options'] == 'resize-ratio') {
                $resize_image->resize($options['width'], $options['height'], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize_image->encode(null, 75);

            $upload = Storage::disk('local')->put("public/{$path}/{$image_name}", $resize_image->__toString()); //save to loca
            if (!is_null($old_image) && @fopen($old_image, "r")) {
                $delete_old = Storage::delete(str_replace(Storage::url('/'), '', $old_image)); //delete old

                if (!is_null($this->mini)) {
                    $delete_old_mini = Storage::delete($this->path . $path . '/' . str_replace(Storage::url('/' . $this->path . $path . '/'), 'mini-', $old_image)); //delete old mini
                }
            }
            $upload = Storage::url(Storage::putFile($this->path . $path, new File(storage_path("app/public/{$path}/{$image_name}")), 'public')); //move from local to S3

            if (!is_null($this->mini)) {
                // $mini = Storage::copy(str_replace(Storage::url('/'), '', $upload), str_replace(Storage::url('/'.$this->path.$path.'/'), $this->path.$path.'/mini-', $upload)); //copy mini
                $new_mini = Image::make(new File(storage_path("app/public/{$path}/{$image_name}")));
                $new_mini->resize($this->mini['width'], $this->mini['height']);
                $new_mini->encode();

                $upload_mini_local = Storage::disk('local')->put("public/{$path}/mini-{$image_name}", $new_mini->__toString()); //upload mini to local

                $upload_mini = Storage::putFileAs(
                    $this->path . $path,
                    new File(storage_path("app/public/{$path}/mini-{$image_name}")),
                    str_replace(Storage::url('/' . $this->path . $path . '/'), 'mini-', $upload)
                );

                $delete = Storage::disk('local')->delete("public/{$path}/mini-{$image_name}"); //delete in local
            }

            $delete = Storage::disk('local')->delete("public/{$path}/{$image_name}"); //delete in local

            return $upload;
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Something Error',
                'error' => $e->getMessage(),
                'full_error' => $e
            ], 409);
        }
    }

    public function upload_v2($path, $file, $width, $height){

        $image_name = uniqid() . '.webp';

        $image = Image::make($file->getRealPath());
        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $size = $image->filesize()/1000;

        $qualityWeb = (int) $size>=700 ? 50 : round(90-(($size-100)/10));
        $qualityMobile = (int) $size>=400 ? 30 : round(60-(($size-70)/7));

        //for web version
        $image->encode('webp', $qualityWeb);
        $upload = Storage::disk('local')->put("public/{$path}/{$image_name}", $image->__toString());
        $upload = Storage::url(Storage::putFileAs($this->path . $path, new File(storage_path("app/public/{$path}/{$image_name}")), $image_name.'.webp', 'public'));

        //for mobile version
        $image->encode('webp', $qualityMobile);
        $upload = Storage::disk('local')->put("public/{$path}/{$image_name}", $image->__toString());
        $upload = Storage::url(Storage::putFileAs($this->path . $path, new File(storage_path("app/public/{$path}/{$image_name}")), $image_name ,'public'));

        //mobile image as the default
        return $upload;
    }

    public function mini($width = null, $height = null)
    {
        if (!is_null($width) and !is_null($height)) {
            $this->mini = ['width' => $width, 'height' => $height];
            return $this;
        }
    }

    public function delete($image)
    {
        $delete = Storage::delete(str_replace(Storage::url('/'), '', $image));
        return $delete;
    }
}
