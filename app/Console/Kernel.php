<?php

namespace App\Console;

use App\Jobs\CheckStatusSoloBanned;
use App\Jobs\CleanDataApiLogs;
use App\Jobs\TournamentStatus;
use App\Jobs\TournamentFilledSlot;
use App\Jobs\AllTicketTotalGiven;
use App\Jobs\CheckStatusTeamBanned;
use App\Jobs\GameTotalPlayerAndTournamentCounter;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->job(new CheckStatusTeamBanned)->everyMinute();
        $schedule->job(new CheckStatusSoloBanned)->everyMinute();
        $schedule->job(new TournamentStatus)->everyMinute();
        $schedule->job(new AllTicketTotalGiven)->hourly();
        $schedule->job(new CleanDataApiLogs)->cron('* * */5 * *');

        $schedule->job(new GameTotalPlayerAndTournamentCounter)->dailyAt('01:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
