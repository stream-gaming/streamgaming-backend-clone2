<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('exeptions.not_allowed')
            ], 400);
        }
        if ($exception instanceof ModelNotFoundException) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('exeptions.model_not_found')
            ], 404);
        }
        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('exeptions.http_not_found')
            ], 404);
        }
        if ($exception instanceof ValidationException) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('exeptions.validation'),
                'errors' => $exception->validator->getMessageBag()
            ], 422); //type your error code.
        }

        return parent::render($request, $exception);
    }
}
