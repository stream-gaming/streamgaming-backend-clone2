<?php

namespace App\Helpers;

use App\Model\IdentityIngameHistoryModel;
use Illuminate\Support\Facades\Auth;

trait SaveIdentityIngameHistory{

    /**
     * save identity in game mode
     * @return void
     */
    public function saveHistory(array $before, array $after, String $gameId, String $userid = null)
    {
        $save = false;
        if($userid == null){
            if(Auth::check()){
                $userid = Auth::user()->user_id;
                $save = true;
            }
        }else{
            $save = true;
        }

        if(!empty($before) && !empty($after) && $save){
            IdentityIngameHistoryModel::create([
                'user_id' => $userid,
                'game_id' => $gameId,
                'before'  => json_encode($before),
                'after'   => json_encode($after)
            ]);
        }
    }
}
