<?php

namespace App\Helpers;


class FaqLinkHelper{

    public function getFaqPath(){
            $faqPath = [
                'game' => '/pusat-bantuan/MENAMBAHKAN_GAME',
                'team' => '/pusat-bantuan/ANGGOTA_TIM_BELUM_MENCUKUPI',
                'team_leader' => '/pusat-bantuan/PENDAFTARAN_TURNAMEN_DILAKUKAN_KETUA_TIM',
                'ticket' => '/klaim-tiket',
                'banned' => '/pusat-bantuan/TERDAPAT_ANGGOTA_TIM_YANG_DIBAN'
            ];

            return $faqPath;
    }
}
