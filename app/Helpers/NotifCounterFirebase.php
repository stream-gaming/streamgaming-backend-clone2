<?php

namespace App\Helpers;

use App\Model\UserNotificationModel;
use App\Transformer\NotifCounterTransform;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class NotifCounterFirebase
{
    /**
     * This api use to up total unread notification to firebase specific user
     * @param mixed $id_user
     * @return array
     */
    public function upData($id_user)
    {
        $pattern = "/[.$#[\]]/i";
        $check = preg_match($pattern,$id_user);

        if(!$check):
            $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
            $firebase = (new Factory)
                            ->withServiceAccount($serviceAccount)
                            ->withDatabaseUri(config('firebase.database_url'))
                            ->create()->getDatabase();
            $count = UserNotificationModel::where('is_read',0)->where('id_user',$id_user)->count();

            $database = $firebase;

            $data = $database

            ->getReference($id_user)
            ->update([
                'counter' => $count
            ]);
            return $data->getvalue();
        else:
            return false;
        endif;
    }

    /**
     * This api use to up total unread notification to firebase all user
     * @return array
     */
    public function updateAll()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
        $firebase = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri(config('firebase.database_url'))
                        ->create()->getDatabase();
        $count = UserNotificationModel::selectRaw('id_user,count(is_read) as counter')->where('is_read',0)->where('id_user','!=','')->groupBy('id_user')->get();


        $datas = (new NotifCounterTransform)->edit($count);
        $database = $firebase;

        $data = $database

        ->getReference()
        ->update(
            $datas
        );
        return $data->getvalue();
    }

    /**
     * This api use to up total unread notification to firebase for all user filtered by notified_on_firebase column
     * @return array
     */
    public static function notifyAll($param=null)
    {
        try{
            $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
            $firebase = (new Factory)
                            ->withServiceAccount($serviceAccount)
                            ->withDatabaseUri(config('firebase.database_url'))
                            ->create()->getDatabase();
            DB::beginTransaction();
            $getPlayer = UserNotificationModel::selectRaw('id_user')
                                            ->where('is_read',0)
                                            ->where('id_user','!=','')
                                            ->where('source',1)
                                            ->where('notified_on_firebase',0)
                                            ->groupBy('id_user')->get()->toArray();

            $getNotification = json_decode(json_encode(UserNotificationModel::selectRaw('id_user, keterangan, link')
                                            ->with('fcm_firebase_token')
                                            ->where('is_read',0)
                                            ->where('id_user','!=','')
                                            ->where('source',1)
                                            ->where('notified_on_firebase',0)->get()->toArray()));

            $push = new PushNotification();

            foreach ($getNotification as  $val) {
                $token = collect($val->fcm_firebase_token)->pluck('token')->all();

                if(count($token)){
                    $push->multiSendPush([
                        'title' => 'StreamGaming',
                        'body'  => $val->keterangan,
                        'data'  => [
                            'title' => 'StreamGaming',
                            'body'  => $val->keterangan
                        ]
                    ], $token);
                }
            }

            $count = UserNotificationModel::selectRaw('id_user,count(is_read) as counter')
                                            ->where('is_read',0)
                                            ->where('id_user','!=','')
                                            ->whereIn('id_user', $getPlayer)
                                            ->groupBy('id_user')->get();

            $update = UserNotificationModel::selectRaw('id_user')
                                            ->where('is_read',0)
                                            ->where('id_user','!=','')
                                            ->where('source',1)
                                            ->where('notified_on_firebase',0)
                                            ->update(['notified_on_firebase' => 1]);
            DB::commit();
            $datas = (new NotifCounterTransform)->edit($count);
            $database = $firebase;

            $data = $database

            ->getReference()
            ->update(
                $datas
            );
            return $data->getvalue();

        }catch(\Exception $e){
            DB::rollBack();
            Log::debug($e);
        }


    }
}
