<?php
namespace App\Helpers\Team;

use App\Helpers\Team\Interfaces\PlayerManagerSingleInterface;
use App\Model\TeamPlayerNewModel;

class LeaderManager extends PlayerManagerSingleInterface{

    public function changeStatus() : bool
    {
        if(!is_null($this->team_id) && !is_null($this->player_id)){
           $changeStatus=TeamPlayerNewModel::where('team_id', $this->team_id)
                                ->where('player_id', $this->player_id)
                                ->update(['player_status' => 1]);
            return $changeStatus == 1 ? true : false;
        }

        return false;
    }

}
