<?php
namespace App\Helpers\Team;

use App\Helpers\Team\Interfaces\PlayerManagerBatchInterface;
use App\Model\TeamPlayerNewModel;
use Carbon\Carbon;

class SubsPlayerBatchManager extends PlayerManagerBatchInterface{

    public function changeStatus() : bool
    {
        if(!is_null($this->team_id) && !is_null($this->arrOfPlayer)){
           $changeStatus=TeamPlayerNewModel::where('team_id', $this->team_id)
                                ->whereIn('player_id', $this->arrOfPlayer)
                                ->update([
                                    'player_status' => 3,
                                    'updated_at' => Carbon::now()
                                ]);
            return true;
        }

        return false;
    }

}
