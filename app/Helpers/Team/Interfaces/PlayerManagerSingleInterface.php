<?php
namespace App\Helpers\Team\Interfaces;


abstract class PlayerManagerSingleInterface extends TeamInterface{
    public $player_id= null;
    public function __construct($team_id, $player_id)
    {
        $this->team_id = $team_id;
        $this->player_id = $player_id;
    }


}
