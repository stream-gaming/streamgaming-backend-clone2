<?php
namespace App\Helpers\Team\Interfaces;


abstract class TeamInterface{
    public $team_id = null;
    abstract function changeStatus() : bool;
}
