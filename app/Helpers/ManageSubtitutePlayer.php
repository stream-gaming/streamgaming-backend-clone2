<?php

namespace App\Helpers;

use App\Model\IdentityGameModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\TeamPlayerModel;
use App\Model\TournamentGroupPlayerModel;
use Exception;

class ManageSubtitutePlayer{

    public function insertToLeaderboard($id_leaderboard, $team_id, $game){
        try{
            $getTeam = $this->getSubstitutePlayer($team_id);

            foreach($getTeam as $p){
                $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $game);
                $leaderboardTeamPlayer = new LeaderboardTeamPlayerModel();
                $leaderboardTeamPlayer->id_teamplayer_leaderboard = $p;
                $leaderboardTeamPlayer->id_team_leaderboard = $team_id;
                $leaderboardTeamPlayer->id_leaderboard = $id_leaderboard;
                $leaderboardTeamPlayer->player_name = $usernameIngame;
                $leaderboardTeamPlayer->player_status = 2;
                $leaderboardTeamPlayer->payment_status = 'Lunas';
                $leaderboardTeamPlayer->save();
            }

            return true;
        }catch(\Exception $e){

            return $e;
        }
    }

    public function insertToBracket($id_tournament, $id_group, $team_id, $game){
        try{
            $getTeam = $this->getSubstitutePlayer($team_id);

            foreach($getTeam as $p){
                $usernameIngame = (new IdentityGameModel)->usernameAndIdInGame($p, $game);

                $tournamentGroupPlayer = new TournamentGroupPlayerModel();
                $tournamentGroupPlayer->id_player = $p;
                $tournamentGroupPlayer->id_group = $id_group;
                $tournamentGroupPlayer->id_team = $team_id;
                $tournamentGroupPlayer->username_ingame = $usernameIngame->username_ingame;
                $tournamentGroupPlayer->id_ingame = $usernameIngame->id_ingame;
                $tournamentGroupPlayer->player_status = 2;
                $tournamentGroupPlayer->save();
            }

            return true;
        }catch(\Exception $e){

            return $e;
        }
    }

    public function getSubstitutePlayer($team_id){
        try{
            $getData = TeamPlayerModel::where('team_id', $team_id)->first();
            $extractSubsPlayer = explode(',', $getData->substitute_id);

            return $extractSubsPlayer;
        }catch(\Exception $e){

            return $e;
        }

    }
}
