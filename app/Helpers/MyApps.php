<?php

namespace App\Helpers;

use App\Traits\CheckImageTraits;
use App\Model\Portal;
use App\Model\UsersDetailModel;
use App\Model\Management;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Lang;

class MyApps
{
	use CheckImageTraits;

	private $base;
	private $content_management;
	private $content;

	public function __construct()
	{
		// $this->base = (Portal::where('name', 'base_url')->first())->content;
		// $this->cdn = (Portal::where('name', 'content_management')->first())->content;
		$this->base = config('app.baseurl_apps');
		$this->cdn = config('app.cm_apps');
	}

	public function checkContent($content)
	{
		return $this->asset = $content;
    }

    public function DefaultUserPath(){

        return $this->cdn . 'private/user.png';
    }

	public function onlyEncrypt($data)
	{
		$data = json_encode($data);
		$ciphering = "AES-128-CTR";
		$iv_length = openssl_cipher_iv_length($ciphering);
		$options = 0;
		$encryption_iv = '1234567891011121';
		$encryption_key = $this->base;
		$encryption = openssl_encrypt($data, $ciphering, $encryption_key, $options, $encryption_iv);
		$encryption = rtrim(strtr($encryption, '+/', '-_'), '=');
		return $encryption;
	}

	public function onlyDecrypt($data)
	{
		$data = json_encode($data);
		$ciphering = "AES-128-CTR";
		$iv_length = openssl_cipher_iv_length($ciphering);
		$options = 0;
		$encryption_iv = '1234567891011121';
		$encryption_key = $this->base;
		$decryption = openssl_decrypt(
			strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4),
			$ciphering,
			$encryption_key,
			$options,
			$encryption_iv
		);
		$decryption = json_decode($decryption);
		return $decryption;
	}

	public function encrypt($data)
	{
		if ($this->content == 'game') {
			$uri = 'logo/game/';
		} else if ($this->content == 'news') {
			$uri = 'news/thumbnail/';
		} else if ($this->content == 'users') {
            $uri = 'picture/user/';
            $data = is_string(filter_var($data['image'], FILTER_VALIDATE_URL)) ? $data['image'] : $data;
		} else if ($this->content == 'team') {
			$uri = 'logo/team/';
		} else if ($this->content == 'sponsorship') {
			$uri = 'logo/sponsorship/';
		} else if ($this->content == 'banner_tournament') {
			$uri = 'banner/tournament/';
		}
        $encryption = $this->onlyEncrypt($data);
        $result = $this->cdn . $uri . $encryption;
        $validateURL = filter_var($data, FILTER_VALIDATE_URL);

        return !is_bool($validateURL) ? $validateURL : $result;
	}

	public function simple($data)
	{
		return $this->cdn . $this->content . $data;
	}

	public function filter($data, $type, $content)
	{
		$this->content = $content;
		return $this->$type($data);
	}

	public function cdn($data, $type = 'simple', $content = null)
	{
		return $this->filter($data, $type, $content);
	}

	public function getImage($usersdetail /*user_id or mangement_id*/, $type_account = 'user' )
	{
		switch ($type_account) {
			case 'user':
                $detail = UsersDetailModel::find($usersdetail);
                // return !empty($detail->image) and !empty($detail->link_image);
				if (!empty($detail->link_image) and empty($detail->image)) {
					return $this->isImageExists($detail->link_image);
					// return $detail->link_image;
				} else if (!empty($detail->image) and !empty($detail->link_image)) {
					return $this->isImageExists($this->cdn([
						'user_id'	=> $detail->user_id,
						'image'		=> $detail->image
					], 'encrypt', 'users'));

				} else if (!empty($detail->image) and empty($detail->link_image)) {
					return $this->isImageExists($this->cdn([
						'user_id'	=> $detail->user_id,
						'image'		=> $detail->image
					], 'encrypt', 'users'));
					
				} else {
					return $this->cdn . 'private/user.png';
                }

			case 'admin':
				$detail = Management::find($usersdetail);
				if (!empty($detail->detail->image)) {
					return $this->cdn([
						'user_id'	=> $detail->detail->access_id,
						'image'		=> 'mini-' . $detail->detail->image
					], 'encrypt', 'users');
				} else {
					return $this->cdn . 'private/user.png';
				}
		}
	}

	public function getDir($name)
	{
		switch ($name) {
			case 'team':
				return Portal::where('name', 'dir_path')->first()->content . Portal::where('name', 'backup_team')->first()->content;
			case 'user':
				return Portal::where('name', 'dir_path')->first()->content . Portal::where('name', 'backup_users')->first()->content;
		}
	}

	public function getAlphabetGroup($kode)
	{
		foreach (range('A', 'ZZ') as $letter) {
			$alphabet[] = $letter;
		}

		return $alphabet[$kode];
	}

	public function getStatusCardLeaderboard($group_sequence)
	{
		if ($group_sequence == 100 or $group_sequence == 1000) {
			$group = Lang::get('message.tournament.group_final');
		} else {
			$group = Lang::get('message.tournament.group') . ' ' .  $this->getAlphabetGroup($group_sequence);
		}

		return $group;
	}

	public static function getEloquentSqlWithBindings($query)
	{
		return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
			$binding = addslashes($binding);
			return is_numeric($binding) ? $binding : "'{$binding}'";
		})->toArray());
	}

	public static function getPathRoute($rute_name, $parameters = null)
	{
		// get Route without domain and /api
		return substr(route($rute_name, $parameters, false), 4);
		// return route($rute_name, $parameters);
	}

	public function getStatusRequest($status)
	{
		$data = [
			0 => 'Decline',
			1 => 'Pending',
			2 => 'Accept'
		];
		return $data[$status];
	}

	public function getStatusPayment($status)
	{
		$data = [
			'Belum-Lunas' 	=> trans('message.payment.belum-lunas'),
			'Lunas' 		=> trans('message.payment.lunas'),
			'Waiting' 		=> trans('message.payment.lunas'),
			'Cancelled' 	=> trans('message.payment.cancelled'),
		];

		return $data[$status];
	}
	public function GetUrlChallonge($setApi,$urlChallonge=null,$dataKey=null){
		$baseUrlChallonges 		=  	env('URL_CHALLONGE', 'https://challonge.com/');
		$pathUrlChallonge 		= 	env('URL_API_CHALLONGE', 'https://api.challonge.com/');
		switch ($setApi) {
			case 'moduleBracket':
				return  $baseUrlChallonges.$urlChallonge."/module";
				break;
			case 'matches':
				return Http::get($pathUrlChallonge.'v1/tournaments/'.$urlChallonge.'/matches.json', $dataKey);
				break;
			case 'createParticipant':
				return Http::post($pathUrlChallonge.'v1/tournaments/'.$urlChallonge.'/participants.json', $dataKey);
				break;
			default:
				return false;
				break;
		}


	}
}
