<?php

namespace App\Helpers\Validation;

use App\Model\LeaderboardModel;
use App\Model\LeaderboardQualificationModel;
use App\Model\LeaderboardTeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;


class isSlotFull{
    /**
     * Checking if the tournament slot is full or not
     *
     * @param string $idTournament
     * @param string $type
     * @return bool
     */

    public function check($idTournament, $type){

        switch($type){

            case 'Leaderboard':
                $model = LeaderboardModel::where('id_leaderboard', $idTournament)->first(['tournament_category','total_team']);

                //Login for full costum mode
                if($model->tournament_category == 3){
                    $count = LeaderboardTeamModel::where('id_leaderboard', $idTournament)
                                                ->where('qualification_sequence', '1')
                                                ->count();
                    $getQualificationSlot = LeaderboardQualificationModel::where('id_leaderboard', $idTournament)
                                            ->where('qualification_sequence','1')
                                            ->first(['total_team']);

                    if($count >= $getQualificationSlot->total_team){
                            return true;
                    }

                }else{
                    //logic for costum mode
                    $count = LeaderboardTeamModel::where('id_leaderboard', $idTournament)
                    ->where('qualification_sequence', '0')
                    ->count();

                    if($count >= $model->total_team){
                        return true;
                    }
                }




                return false;
            break;

            case 'Bracket':

                $model = TournamentModel::where('id_tournament', $idTournament)->first(['id_group','total_team']);
                $count = TournamenGroupModel::where('id_group', $model->id_group)->count();

                if($count >= $model->total_team){
                    return true;
                }

                return false;
            break;
        }
    }
}
