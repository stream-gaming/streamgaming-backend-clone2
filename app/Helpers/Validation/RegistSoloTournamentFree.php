<?php
namespace App\Helpers\Validation;

use App\Helpers\MyApps;
use App\Helpers\Notification;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TournamentGroupPlayerModel;
use Illuminate\Support\Facades\DB;
use App\Model\TournamenGroupModel;

class RegistSoloTournamentFree{

    public function regist($tournament, $userId, $tournamentType){

        switch($tournamentType){
            case 'Leaderboard':
                return $this->Leaderboard($tournament, $userId);
            break;

            case 'Bracket':
                return $this->Bracket($tournament, $userId);
            break;
        }

    }

    public function Leaderboard($tournament, $userId){
        try{
            $getPlayerData = IdentityGameModel::where('users_id',$userId)
                                                ->where('game_id',$tournament->game_competition)
                                                ->first();
            $getPlayerImage = (new MyApps)->getImage($userId);

        }catch(\Exception $e){

            return $e;

        }


        try{

            DB::transaction(function () use($tournament, $userId, $getPlayerData, $getPlayerImage) {

                $availableGroup = new CheckAvailableGroup();
                $leaderboardTeam = new LeaderboardTeamModel();
                $notification = new Notification();
                $group = $availableGroup->check($tournament);


                if($tournament->tournament_category != 3 AND $tournament->is_no_qualification != 1){
                    $qualification_sequence = 0;
                    $isFinal = 0;
                }elseif($tournament->tournament_category != 3 AND $tournament->is_no_qualification == 1){
                    $qualification_sequence = 0;
                    $isFinal = 1;
                }
                else{
                    $qualification_sequence = 1;
                    $isFinal = 0;
                }



                    $paymentHistory = new PlayerPayment();
                    $paymentHistory->id_player_payment = uniqid();
                    $paymentHistory->id_tournament = $tournament->id_leaderboard;
                    $paymentHistory->id_team = $userId;
                    $paymentHistory->player_id = $userId;
                    $paymentHistory->price_of_payment = 'Free';
                    $paymentHistory->amount = '-';
                    $paymentHistory->type_payment = '-';
                    $paymentHistory->status = 'Lunas';
                    $paymentHistory->date_payment = date('Y-m-d H:i:s');
                    $paymentHistory->save();


                    //notification
                    $link = 'leaderboard/'.$tournament->url;
                    $message = 'Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                    $notification->singleNotification($userId, $link, $message);

                    $leaderboardTeamPlayer = new LeaderboardTeamPlayerModel();
                    $leaderboardTeamPlayer->id_teamplayer_leaderboard = $userId;
                    $leaderboardTeamPlayer->id_team_leaderboard = $userId;
                    $leaderboardTeamPlayer->id_leaderboard = $tournament->id_leaderboard;
                    $leaderboardTeamPlayer->player_name = $getPlayerData->username_ingame;


                    $leaderboardTeamPlayer->player_status = 1;


                    $leaderboardTeamPlayer->payment_status = 'Lunas';
                    $leaderboardTeamPlayer->save();




                $leaderboardTeam->id_team_leaderboard = $userId;
                $leaderboardTeam->id_leaderboard = $tournament->id_leaderboard;
                $leaderboardTeam->qualification_sequence = $qualification_sequence;
                $leaderboardTeam->urutan_grup = $group;
                $leaderboardTeam->nama_team = $getPlayerData->username_ingame;
                $leaderboardTeam->team_logo = $getPlayerImage;
                $leaderboardTeam->is_final = $isFinal;
                $leaderboardTeam->registrasi_status = 'Sudah_Registrasi';

                $leaderboardTeam->save();

            });

            }catch(\Exception $e){

                return $e;

            }

            return true;

    }

    private function Bracket($tournament, $userId){
        try{
            $getPlayerData = IdentityGameModel::where('users_id',$userId)
                                                ->where('game_id',$tournament->kompetisi)
                                                ->first();
            $getPlayerImage = (new MyApps)->getImage($userId);

        }catch(\Exception $e){
            return $e;
        }
        try{

            DB::transaction(function () use($tournament, $userId, $getPlayerData, $getPlayerImage) {
                $paymentHistory = new PlayerPayment();
                $paymentHistory->id_player_payment = uniqid();
                $paymentHistory->id_tournament = $tournament->id_tournament;
                $paymentHistory->id_team = $userId;
                $paymentHistory->player_id = $userId;
                $paymentHistory->price_of_payment = 'Free';
                $paymentHistory->amount = '-';
                $paymentHistory->type_payment = '-';
                $paymentHistory->status = 'Lunas';
                $paymentHistory->date_payment = date('Y-m-d H:i:s');
                $paymentHistory->save();

                $tournamentGroupPlayer = new TournamentGroupPlayerModel();
                $tournamentGroupPlayer->id_player = $userId;
                $tournamentGroupPlayer->id_group = $tournament->id_group;
                $tournamentGroupPlayer->id_team = '-';
                $tournamentGroupPlayer->username_ingame = $getPlayerData->username_ingame;
                $tournamentGroupPlayer->id_ingame = $getPlayerData->id_ingame;
                $tournamentGroupPlayer->player_status = 1;
                $tournamentGroupPlayer->save();

                $tournamentGroup = new TournamenGroupModel();
                $tournamentGroup->id_group = $tournament->id_group;
                $tournamentGroup->id_team = $userId;
                $tournamentGroup->nama_team =  $getPlayerData->username_ingame;
                $tournamentGroup->logo_team = $getPlayerImage;
                $tournamentGroup->date = date('Y-m-d H:i:s');
                $tournamentGroup->save();
            });
        return true;
        }catch(\Exception $e){

            return $e;

        }
    }


}
