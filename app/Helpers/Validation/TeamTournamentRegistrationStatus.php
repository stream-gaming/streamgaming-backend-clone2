<?php
namespace App\Helpers\Validation;
use App\Model\AllTournamentModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use Illuminate\Support\Facades\DB;

class TeamTournamentRegistrationStatus{

    public function check($userId, $gameId){

        try{

        $getTeam = TeamModel::where('leader_id',$userId)
                             ->where('game_id',$gameId)
                             ->firstOrFail(['team_id']);

        }catch(\Exception $e){
            return false;
        }


        try{
        $isInTournament = 0;
        $checkOnLeaderboard = AllTournamentModel::selectRaw('COUNT(player_payment.id_tournament) as hasil')
                                     ->join('leaderboard', function($join) use ($gameId){
                                         $join->on('all_tournament.id_tournament','=','leaderboard.id_leaderboard');
                                         $join->where('leaderboard.status_tournament','<','4');
                                         $join->where('leaderboard.game_competition','=',$gameId);

                                     })
                                     ->join('player_payment', function($join) use ($getTeam,$userId){
                                         $join->on('all_tournament.id_tournament','=','player_payment.id_tournament');
                                         $join->where('player_payment.status','!=','Cancelled');
                                         $join->where('player_payment.id_team',$getTeam->team_id);
                                         $join->where('player_payment.player_id',$userId);
                                     })
                                     ->where('all_tournament.format','=','Leaderboard')
                                     ->first();
        $checkOnBracket  =  AllTournamentModel::selectRaw('COUNT(player_payment.id_tournament) as hasil')
                                     ->join('tournament', function($join) use ($gameId){
                                         $join->on('all_tournament.id_tournament','=','tournament.id_tournament');
                                         $join->where('tournament.status','!=','complete');
                                         $join->where('tournament.kompetisi','=',$gameId);

                                     })
                                     ->join('player_payment', function($join) use ($userId){
                                         $join->on('all_tournament.id_tournament','=','player_payment.id_tournament');
                                         $join->where('player_payment.status','!=','Cancelled');

                                         $join->where('player_payment.player_id',$userId);
                                     })
                                     ->where('all_tournament.format','=','Bracket')
                                     ->first();
            if($checkOnLeaderboard){
                $isInTournament += $checkOnLeaderboard->hasil;
            }

            if($checkOnBracket){
                $isInTournament += $checkOnBracket->hasil;
            }

            return $isInTournament > 0 ? true : false;
        }catch(\Exception $e){
            return $e;
        }


    }

}
