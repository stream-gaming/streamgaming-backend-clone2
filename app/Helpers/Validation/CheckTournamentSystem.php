<?php

namespace App\Helpers\Validation;

use App\Model\AllTournamentModel;
use App\Model\LeaderboardModel;
use App\Model\TournamentModel;

class CheckTournamentSystem
{
    public function check($idtournament)
    {

        try {

            $typeTournament = AllTournamentModel::where('id_tournament',$idtournament)->firstOrfail();

            if($typeTournament->format == "Leaderboard"){
                return [
                    'id_tournament'     => $idtournament,
                    'tournament_system' => $typeTournament->format,
                    'id_group'          => null
                ];
            }

            else{
                $getBracket = TournamentModel::findOrfail($idtournament);

                return [
                    'id_tournament'     => $idtournament,
                    'tournament_system' => $typeTournament->format,
                    'id_group'          => $getBracket->id_group
                ];
            }

        } catch (\Throwable $th) {

            return false;

        }



    }

    public function checkAndGet($idtournament)
    {

        try {

            $typeTournament = AllTournamentModel::where('id_tournament',$idtournament)->firstOrfail();

            if($typeTournament->format == "Leaderboard"){
                $getLeaderboard = LeaderboardModel::findOrfail($idtournament);
                return [
                    'id_tournament'     => $idtournament,
                    'tournament_name'   => $getLeaderboard->tournament_name,
                    'tour_url'          => $getLeaderboard->url,
                    'tournament_system' => $typeTournament->format,
                    'id_group'          => null
                ];
            }

            else{
                $getBracket = TournamentModel::findOrfail($idtournament);

                return [
                    'id_tournament'     => $idtournament,
                    'tournament_name'   => $getBracket->tournament_name,
                    'tour_url'          => $getBracket->url,
                    'tournament_system' => $typeTournament->format,
                    'id_group'          => $getBracket->id_group
                ];
            }

        } catch (\Throwable $th) {

            return false;

        }



    }
}
