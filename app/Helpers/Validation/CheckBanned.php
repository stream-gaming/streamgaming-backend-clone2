<?php

namespace App\Helpers\Validation;

use Exception;
use Carbon\Carbon;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Model\HistoryBannedModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CheckBanned
{
    /**
     * Check has team been banned
     * @param string $team_id
     * @return bool
     */
    public function isBanned($team_id)
    {
        $team = TeamModel::find($team_id);

        return $team->is_banned ? true : false;
    }

    public function isSoloBanned($game_id, $team_id = null, $user_id = null, $json = true)
    {
        if (!is_null($team_id)) {
            /** Jika request pengecekan dari data tim, team_id tidak boleh kosong */
            $check = HistoryBannedModel::where('team_id', $team_id)->where('is_banned', 1)->count();
            $status = $check > 0 ? true : false;
        } elseif (is_null($team_id)) {
            /** jika request pengecekan dari data request/invite join, game_id tidak boleh kosong */
            $check = IdentityGameModel::where('game_id', $game_id)->where('users_id', $user_id ?? auth()->user()->user_id)->first();
            $status = $check->is_banned == 1 ? true : false;
        }

        if ($json and $status) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.banned')
            ], 400);
        } elseif ($json and $status == false) {
        } elseif ($json == false) {
            return $status;
        }
    }
}
