<?php

namespace App\Helpers\Validation;

use App\Http\Controllers\TournamenList;
use App\Model\checkTournamentTypeModel;
use App\Model\TournamentListModel;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;

class checkTournamentType
{
    public function check($id_tournament, $format)
    {
        $check = (new checkTournamentTypeModel)->check($id_tournament, $format);
        if ($check == '1') {
            $data = 'Solo';
        } elseif ($check == '2') {
            $data = 'Duo';
        } elseif ($check == '3') {
            $data = 'Squad';
        } elseif ($check == '4') {
            $data = 'Special_Squad';
        } elseif ($check == '5') {
            $data = 'Trio';
        }
       return [
            'status'            => true,
            'type_tournament'   => $data
        ];
    }

    /**
     * Check tournament type solo or squad
     * @param string $id_tournament
     * @return integer
     */
    public function checkType($id_tournament)
    {
        $check = TournamentListModel::where('id_tournament',$id_tournament)->first(['game_option','format_tournament']);
        if(!$check){
            return [
                'game_option' => null,
                'format' => null
            ];
        }
        return [
                'game_option' => $check->game_option,
                'format' => $check->format_tournament
            ];
    }
}
