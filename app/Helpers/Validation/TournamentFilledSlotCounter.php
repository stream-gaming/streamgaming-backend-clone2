<?php

namespace App\Helpers\Validation;

use App\Model\LeaderboardModel;
use App\Model\TournamentModel;

class TournamentFilledSlotCounter{

    public function sum($idTournament, $currentFilledSlot, $tournamentType){
                switch($tournamentType){
                    case 'Leaderboard':

                        try{
                            //before
                            // LeaderboardModel::where('id_leaderboard',$idTournament)
                            //                     ->update(['filled_slot' => ($currentFilledSlot+1)]);

                            //after
                            LeaderboardModel::where('id_leaderboard',$idTournament)
                                            ->increment('filled_slot');
                             return true;

                        }catch(\Exception $e){

                            return false;
                        }
                    break;

                    case 'Bracket':

                        try{

                            //before
                            // TournamentModel::where('id_tournament', $idTournament)
                            //                               ->update(['filled_slot' => ($currentFilledSlot+1)]);

                            //after
                            TournamentModel::where('id_tournament', $idTournament)
                                            ->increment('filled_slot');
                            return true;
                        }catch(\Exception $e){

                            return false;
                        }
                    break;
                }
    }

}
