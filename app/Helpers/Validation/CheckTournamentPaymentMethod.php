<?php

namespace App\Helpers\Validation;

use App\Model\AllTicketModel;
use App\Model\LeaderboardModel;
use App\Model\TicketModel;
use App\Model\TournamentModel;
use Illuminate\Support\Str;

class CheckTournamentPaymentMethod
{
    /**
     * Get Tournament Payment Method for Bracket Type
     * @param string $tournamentID
     * @return array
     */
    public function BracketPaymentMethod($tournamentID)
    {
        $paymentMethod  = TournamentModel::where('id_tournament',$tournamentID)->firstOrfail(['system_payment','payment','is_new_ticket_feature']);

        $sysPayment = Str::lower($paymentMethod->system_payment);

        if($sysPayment == 'free' || $sysPayment == 'pay'){
            $payment = [
                'payment_method'    => $sysPayment,
                'value'             => $sysPayment == 'free' ? 0 : $paymentMethod->payment,
                'name'              => $sysPayment == 'free' ? 'free' : 'cash/point'
            ];
        }
        else{

            $ticketId = $paymentMethod->is_new_ticket_feature == 1 ? AllTicketModel::find($paymentMethod->system_payment)->name : TicketModel::find($paymentMethod->system_payment)->names;

            $payment = [
                'payment_method'    => 'ticket',
                'value'             => $paymentMethod->payment,
                'ticket_id' 	    => $sysPayment,
                'name'              => $ticketId
            ];
        }

        return $payment;

    }

    /**
     * Get Tournament Payment Method for Leaderboard Type
     * @param string $tournamentID
     * @return array
     */
    public function LeaderboardPaymentMethod($tournamentID)
    {
        $paymentMethod  = LeaderboardModel::where('id_leaderboard',$tournamentID)->firstOrfail(['tournament_form','ticket','ticket_unit','tournament_fee','is_new_ticket_feature']);

        $method = $paymentMethod->tournament_form;

        $payment = [
            'payment_method'    => $method,
        ];

        if ($method=='free' || $method=='pay') {
            $payment['value']   = $paymentMethod->tournament_fee;
            switch ($method) {
                case 'pay':
                    $payment['name'] = 'Cash / Point';
                    break;
                default:
                    $payment['name'] = 'Free';
                    break;
            }
        }
        else {
            $payment['value']       = $paymentMethod->ticket_unit;
            $payment['ticket_id']   = $paymentMethod->ticket;
            $payment['name']        = $paymentMethod->is_new_ticket_feature == 1 ? AllTicketModel::find($paymentMethod->ticket)->name : TicketModel::find($paymentMethod->ticket)->names;
        }

        return $payment;
    }
}
