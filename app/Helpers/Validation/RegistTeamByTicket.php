<?php

namespace App\Helpers\Validation;

use App\Helpers\InsertParticipantChallonge;
use App\Helpers\ManageSubtitutePlayer;
use App\Helpers\Notification;
use App\Helpers\Ticket\TeamTicketFactory;
use App\Helpers\Ticket\TicketRole;
use App\Jobs\JoinTournamentChallonge;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TicketModel;
use App\Model\TicketTeamModel;
use App\model\TicketUserModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use Symfony\Component\CssSelector\Parser\Handler\StringHandler;

class RegistTeamByTicket {
      /**
     * Registering Team using ticket
     * @param object $tournament
     * @param string $teamId
     * @param string $ticketId
     * @param int $amountOfTicket
     * @param string $tournamentType
     * @return bool
     */

    public function regist( $tournament, $teamId,  $ticketId,$amountOfTicket,$tournamentType, $isNewTicketFeature=0){
        try{

            if($isNewTicketFeature == 0){
                $getTicket = TicketModel::where('id_ticket', $ticketId)->first(['functions']);
            }


        }catch(\Exception $e){

            return false;

        }

        if($isNewTicketFeature == 0){
            if($getTicket->functions == 1){

                return $this->registByTicketUser($tournament, $teamId, $ticketId, $amountOfTicket, $tournamentType, $isNewTicketFeature);

            }elseif($getTicket->functions == 3){

                return $this->registByTicketTeam($tournament, $teamId, $ticketId, $amountOfTicket, $tournamentType, $isNewTicketFeature);
            }
        }else{
            //in case this ticket uses new ticket feature system
            return $this->registByTicketTeam($tournament, $teamId, $ticketId, $amountOfTicket, $tournamentType, $isNewTicketFeature);
        }


            return false;

    }

    private function registByTicketUser($tournament, $teamId, $ticketId, $amountOfTicket, $tournamentType){

        try{
            $getTeam = TeamModel::where('team_id', $teamId)
                        ->with('player')->first();

            $players = explode(',',$getTeam->player->player_id);

        }catch(\Exception $e){

            return $e;

        }

        if($tournamentType == 'Leaderboard'){

         try{

            DB::transaction(function () use($tournament,$players,$teamId,$ticketId,$amountOfTicket,$getTeam) {

                $availableGroup = new CheckAvailableGroup();
                $leaderboardTeam = new LeaderboardTeamModel();
                $notification = new Notification();

                $group = $availableGroup->check($tournament);


                if($tournament->tournament_category != 3 AND $tournament->is_no_qualification != 1){
                    $qualification_sequence = 0;
                    $isFinal = 0;
                }elseif($tournament->tournament_category != 3 AND $tournament->is_no_qualification == 1){
                    $qualification_sequence = 0;
                    $isFinal = 1;
                }
                else{
                    $qualification_sequence = 1;
                    $isFinal = 0;
                }

                foreach($players as $p){

                    $userTicketBalance = TicketUserModel::where('id_users',$p)
                                        ->where('id_ticket', $ticketId)
                                        ->first();
                    TicketUserModel::where('id_users',$p)
                                    ->where('id_ticket', $ticketId)
                                    ->update(['ticket' => $userTicketBalance->ticket - $amountOfTicket]);

                        // dd($userTicketBalance);
                    $paymentHistory = new PlayerPayment();
                    $paymentHistory->id_player_payment = uniqid();
                    $paymentHistory->id_tournament = $tournament->id_leaderboard;
                    $paymentHistory->id_team = $teamId;
                    $paymentHistory->player_id = $p;
                    $paymentHistory->price_of_payment = $amountOfTicket;
                    $paymentHistory->type_payment = 'Stream_Ticket';
                    $paymentHistory->status = 'Lunas';
                    $paymentHistory->date_payment = date('Y-m-d H:i:s');
                    $paymentHistory->save();

                    //notification
                    $link = 'leaderboard/'.$tournament->url;
                    $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                    $notification->singleNotification($p, $link, $message);

                    $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $tournament->game_competition);
                    $leaderboardTeamPlayer = new LeaderboardTeamPlayerModel();
                    $leaderboardTeamPlayer->id_teamplayer_leaderboard = $p;
                    $leaderboardTeamPlayer->id_team_leaderboard = $getTeam->team_id;
                    $leaderboardTeamPlayer->id_leaderboard = $tournament->id_leaderboard;
                    $leaderboardTeamPlayer->player_name = $usernameIngame;

                    if($p == $getTeam->leader_id){
                        $leaderboardTeamPlayer->player_status = 1;
                    }else{
                        $leaderboardTeamPlayer->player_status = 0;
                    }

                    $leaderboardTeamPlayer->payment_status = 'Lunas';
                    $leaderboardTeamPlayer->save();


                }

                $leaderboardTeam->id_team_leaderboard = $getTeam->team_id;
                $leaderboardTeam->id_leaderboard = $tournament->id_leaderboard;
                $leaderboardTeam->qualification_sequence = $qualification_sequence;
                $leaderboardTeam->urutan_grup = $group;
                $leaderboardTeam->nama_team = $getTeam->team_name;
                $leaderboardTeam->team_logo = $getTeam->team_logo;
                $leaderboardTeam->is_final = $isFinal;
                $leaderboardTeam->registrasi_status = 'Sudah_Registrasi';

                $leaderboardTeam->save();

                //add substitute player
                $substitutePlayer = (new ManageSubtitutePlayer)->insertToLeaderboard($tournament->id_leaderboard, $getTeam->team_id, $tournament->game_competition);
                if($substitutePlayer != true){
                    throw new Exception('Gagal menambahkan substitute player');
                }
            });

            }catch(\Exception $e){

                return $e;

            }

            return true;
        }else{

            //bracket
            try{

                DB::transaction(function () use($tournament,$players,$teamId,$ticketId,$amountOfTicket,$getTeam) {

                         $tournamentGroup = new TournamenGroupModel();
                         $notification = new Notification();

                         foreach($players as $p){
                            $userTicketBalance = TicketUserModel::where('id_users',$p)
                                        ->where('id_ticket', $ticketId)
                                        ->first();
                            TicketUserModel::where('id_users',$p)
                                    ->where('id_ticket', $ticketId)
                                    ->update(['ticket' => $userTicketBalance->ticket - $amountOfTicket]);

                             $paymentHistory = new PlayerPayment();
                             $paymentHistory->id_player_payment = uniqid();
                             $paymentHistory->id_tournament = $tournament->id_tournament;
                             $paymentHistory->id_team = $teamId;
                             $paymentHistory->player_id = $p;
                             $paymentHistory->price_of_payment = $amountOfTicket;
                             $paymentHistory->amount = $amountOfTicket * count($players);
                             $paymentHistory->type_payment = 'Stream_Ticket';
                             $paymentHistory->status = 'Lunas';
                             $paymentHistory->date_payment = date('Y-m-d H:i:s');
                             $paymentHistory->save();

                            //notification
                            $link = 'tournament/'.$tournament->url;
                            $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                            $notification->singleNotification($p, $link, $message);

                             $usernameIngame = (new IdentityGameModel)::where('users_id', $p)
                                                ->where('game_id', $tournament->kompetisi)->first();


                             $tournamentGroupPlayer = new TournamentGroupPlayerModel();
                             $tournamentGroupPlayer->id_player = $p;
                             $tournamentGroupPlayer->id_group = $tournament->id_group;
                             $tournamentGroupPlayer->id_team = $getTeam->team_id;
                             $tournamentGroupPlayer->username_ingame = $usernameIngame->username_ingame;
                             $tournamentGroupPlayer->id_ingame = $usernameIngame->id_ingame;

                             if($p == $getTeam->leader_id){

                                $tournamentGroupPlayer->player_status = 1;

                             }else{

                                $tournamentGroupPlayer->player_status = 0;

                             }

                             $tournamentGroupPlayer->save();


                         }

                         $tournamentGroup->id_group = $tournament->id_group;
                         $tournamentGroup->id_team = $getTeam->team_id;
                         $tournamentGroup->nama_team =  $getTeam->team_name;
                         $tournamentGroup->logo_team = $getTeam->team_logo;
                         $tournamentGroup->nama_kota = $getTeam->venue;
                         $tournamentGroup->save();
                        
                        //add substitute player
                        $substitutePlayer = (new ManageSubtitutePlayer)->insertToBracket($tournament->id_tournament, $tournament->id_group, $getTeam->team_id, $tournament->kompetisi);
                        if($substitutePlayer != true){
                            throw new Exception('Gagal menambahkan substitute player');
                        } //keep

                        //insert to challonge
                        if($tournament->is_challonge == 1){
                          $InsertChallonge =  (new InsertParticipantChallonge)->InsertParticipant($tournament, ['name' => $getTeam->team_name, 'id_team' => $getTeam->team_id]);
                            if($InsertChallonge != true){
                                throw new Exception('Gagal menambahkan Team Di challonge');
                            } //keep
                        }
                   
                  });

             }catch(\Exception $e){
                 return $e;

             }

        }

    }

    private function registByTicketTeam($tournament, $teamId, $ticketId, $amountOfTicket, $tournamentType, $isNewTicketFeature){

        try{
            $getTeam = TeamModel::where('team_id', $teamId)
                        ->with('player')->first();

            $players = explode(',',$getTeam->player->player_id);

        }catch(\Exception $e){

            return $e;

        }

        if($tournamentType == 'Leaderboard'){

            try{

               DB::transaction(function () use($tournament,$players,$teamId,$ticketId,$amountOfTicket,$getTeam, $isNewTicketFeature) {
                        $availableGroup = new CheckAvailableGroup();
                        $leaderboardTeam = new LeaderboardTeamModel();
                        $notification = new Notification();

                        $group = $availableGroup->check($tournament);


                        if($tournament->tournament_category != 3 AND $tournament->is_no_qualification != 1){
                            $qualification_sequence = 0;
                            $isFinal = 0;
                        }elseif($tournament->tournament_category != 3 AND $tournament->is_no_qualification == 1){
                            $qualification_sequence = 0;
                            $isFinal = 1;
                        }
                        else{
                            $qualification_sequence = 1;
                            $isFinal = 0;
                        }



                        if($isNewTicketFeature == 1){

                            $teamTicketBalance = (new TeamTicketFactory)->reduceTicket($getTeam->team_id, $ticketId, TicketRole::PAYTOUR_SOURCE, $amountOfTicket);
                            $ticketsId = $teamTicketBalance['message'];


                        }else{
                            $teamTicketBalance = TicketTeamModel::where('id_team',$getTeam->team_id)
                            ->where('id_ticket', $ticketId)
                            ->first();

                            TicketTeamModel::where('id_team',$getTeam->team_id)
                            ->where('id_ticket', $ticketId)
                            ->update(['ticket' => $teamTicketBalance->ticket - $amountOfTicket]);
                            $ticketsId =0;
                          }

                        foreach($players as $p){

                            $paymentHistory = new PlayerPayment();
                            $paymentHistory->id_player_payment = uniqid();
                            $paymentHistory->id_tournament = $tournament->id_leaderboard;
                            $paymentHistory->id_team = $teamId;
                            $paymentHistory->player_id = $p;
                            $paymentHistory->amount = $amountOfTicket;
                            $paymentHistory->type_payment = 'Stream_Ticket';
                            $paymentHistory->status = 'Lunas';
                            $paymentHistory->ticket_id = $ticketsId;
                            $paymentHistory->date_payment = date('Y-m-d H:i:s');
                            $paymentHistory->save();
                            //notification
                            $link = 'tournament/'.$tournament->url;
                            $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                            $notification->singleNotification($p, $link, $message);


                            $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $tournament->game_competition);
                            $leaderboardTeamPlayer = new LeaderboardTeamPlayerModel();
                            $leaderboardTeamPlayer->id_teamplayer_leaderboard = $p;
                            $leaderboardTeamPlayer->id_team_leaderboard = $getTeam->team_id;
                            $leaderboardTeamPlayer->id_leaderboard = $tournament->id_leaderboard;
                            $leaderboardTeamPlayer->player_name = $usernameIngame;
                            if($p == $getTeam->leader_id){
                                $leaderboardTeamPlayer->player_status = 1;
                            }else{
                                $leaderboardTeamPlayer->player_status = 0;
                            }

                            $leaderboardTeamPlayer->payment_status = 'Lunas';
                            $leaderboardTeamPlayer->save();
                        }

                        $leaderboardTeam->id_team_leaderboard = $getTeam->team_id;
                        $leaderboardTeam->id_leaderboard = $tournament->id_leaderboard;
                        $leaderboardTeam->qualification_sequence = $qualification_sequence;
                        $leaderboardTeam->urutan_grup = $group;
                        $leaderboardTeam->nama_team = $getTeam->team_name;
                        $leaderboardTeam->team_logo = $getTeam->team_logo;
                        $leaderboardTeam->is_final = $isFinal;
                        $leaderboardTeam->registrasi_status = 'Sudah_Registrasi';

                        $leaderboardTeam->save();


                    //add substitute player
                    $substitutePlayer = (new ManageSubtitutePlayer)->insertToLeaderboard($tournament->id_leaderboard, $getTeam->team_id, $tournament->game_competition);
                    if($substitutePlayer != true){
                        throw new Exception('Gagal menambahkan substitute player');
                    }

                 });

            }catch(\Exception $e){

                return $e;

            }


        }else{
            //bracket
            try{ 

                DB::transaction(function () use($tournament,$players,$teamId,$ticketId,$amountOfTicket,$getTeam, $isNewTicketFeature) {

                         $tournamentGroup = new TournamenGroupModel();
                         $notification = new Notification();

                         if($isNewTicketFeature == 1){

                            $teamTicketBalance = (new TeamTicketFactory)->reduceTicket($getTeam->team_id, $ticketId, TicketRole::PAYTOUR_SOURCE, $amountOfTicket);
                            $ticketsId = $teamTicketBalance['message'];


                        }else{
                            $teamTicketBalance = TicketTeamModel::where('id_team',$getTeam->team_id)
                            ->where('id_ticket', $ticketId)
                            ->first();

                            TicketTeamModel::where('id_team',$getTeam->team_id)
                            ->where('id_ticket', $ticketId)
                            ->update(['ticket' => $teamTicketBalance->ticket - $amountOfTicket]);
                            $ticketsId =0;
                          }


                         foreach($players as $p){


                             $paymentHistory = new PlayerPayment();
                             $paymentHistory->id_player_payment = uniqid();
                             $paymentHistory->id_tournament = $tournament->id_tournament;
                             $paymentHistory->id_team = $teamId;
                             $paymentHistory->player_id = $p;
                             $paymentHistory->amount = $amountOfTicket;
                             $paymentHistory->type_payment = 'Stream_Ticket';
                             $paymentHistory->status = 'Lunas';
                             $paymentHistory->ticket_id = $ticketsId;
                             $paymentHistory->date_payment = date('Y-m-d H:i:s');
                             $paymentHistory->save();
                            //notification
                            $link = 'tournament/'.$tournament->url;
                            $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                            $notification->singleNotification($p, $link, $message);

                             $usernameIngame = (new IdentityGameModel)::where('users_id', $p)
                                                ->where('game_id', $tournament->kompetisi)->first();


                             $tournamentGroupPlayer = new TournamentGroupPlayerModel();
                             $tournamentGroupPlayer->id_player = $p;
                             $tournamentGroupPlayer->id_group = $tournament->id_group;
                             $tournamentGroupPlayer->id_team = $getTeam->team_id;
                             $tournamentGroupPlayer->username_ingame = $usernameIngame->username_ingame;
                             $tournamentGroupPlayer->id_ingame = $usernameIngame->id_ingame;

                             if($p == $getTeam->leader_id){

                                $tournamentGroupPlayer->player_status = 1;

                             }else{

                                $tournamentGroupPlayer->player_status = 0;

                             }

                             $tournamentGroupPlayer->save();


                         }

                         $tournamentGroup->id_group = $tournament->id_group;
                         $tournamentGroup->id_team = $getTeam->team_id;
                         $tournamentGroup->nama_team =  $getTeam->team_name;
                         $tournamentGroup->logo_team = $getTeam->team_logo;
                         $tournamentGroup->nama_kota = $getTeam->venue;
                         $tournamentGroup->save();

                         //add substitute player
                        $substitutePlayer = (new ManageSubtitutePlayer)->insertToBracket($tournament->id_tournament, $tournament->id_group, $getTeam->team_id, $tournament->kompetisi);
                        if($substitutePlayer != true){
                            throw new Exception('Gagal menambahkan substitute player');
                        }
                        //insert to challonge
                        if($tournament->is_challonge == 1){
                            $InsertChallonge =  (new InsertParticipantChallonge)->InsertParticipant($tournament, ['name' => $getTeam->team_name, 'id_team' => $getTeam->team_id]);
                            if($InsertChallonge != true){
                                throw new Exception('Gagal menambahkan Team Di challonge');
                            } //keep
                        }
                  });

             }catch(\Exception $e){

                 return $e;

             }

        }

        return true;
    }



}
