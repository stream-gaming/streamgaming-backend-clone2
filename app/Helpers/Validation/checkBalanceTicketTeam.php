<?php

namespace App\Helpers\Validation;

use App\Model\LeaderboardModel;
use App\Model\TicketModel;
use App\Model\TicketTeamModel;
use App\Model\TournamentModel;

class checkBalanceTicketTeam{

    /**
     * Checking if ticket balance of team is enough or not
     *
     * @param $idTournament = tournament ID (example: 5f7562ac34b07)
     * @param $type = tournament system type (example: 'Leaderboard' or 'Bracket')
     * @param $teamId = id of team to be checked (example: 5f699c9cdf3d3)
     * @return boolean
     */

    public function check($idTournament,$type,$teamId){

        switch($type){

            case 'Leaderboard':
                $model = LeaderboardModel::where('id_leaderboard', $idTournament)->first(['tournament_form','ticket','ticket_unit']);

                if($model->tournament_form == 'ticket'){
                    $ticketId = $model->ticket;
                    $ticketUnit = $model->ticket_unit;


                        $checkBalance = TicketTeamModel::where('id_team', $teamId)
                                        ->where('id_ticket', $ticketId)
                                        ->first(['ticket']);

                        try{

                            if($checkBalance->ticket >= $ticketUnit){
                                return true;
                            }

                        }catch(\Exception $e){

                            return false;
                        }

                    return false;

                }

            break;

            case 'Bracket':
                $model = TournamentModel::where('id_tournament',$idTournament)->first(['system_payment','payment']);

                if($model->system_payment != 'Free' OR $model->system_payment != 'Pay'){
                    $ticketId = $model->system_payment;
                    $ticketUnit = $model->payment;



                        $checkBalance = TicketTeamModel::where('id_team', $teamId)
                                                         ->where('id_ticket', $ticketId)
                                                         ->first(['ticket']);

                    try{

                        if($checkBalance->ticket >= $ticketUnit){
                            return true;
                        }

                    }catch(\Exception $e){

                        return false;
                    }

                        return false;


                }

            break;


        }
    }

    /**
     * Checking if ticket balance of team is enough or not
     *
     * @param string $ticketId = ticketId
     * @param string $teamId = id of team to be checked
     * @param int $value = value to be compare with ticket team balance
     * @return boolean
     */
    public static function checkByticket($ticketId, $teamId, $value=null /* optional*/){

        $checkBalance = TicketTeamModel::where('id_team', $teamId)
                        ->where('id_ticket', $ticketId)
                        ->firstOr(['ticket'],function(){
                            return false;
                        });

        $value = $value == null ? 0 : $value;

        if($checkBalance != null){
            if($checkBalance->ticket >= $value){
                return true;
            }
        }


        return false;

    }
}
