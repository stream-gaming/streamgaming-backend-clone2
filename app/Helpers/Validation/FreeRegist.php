<?php

namespace App\Helpers\Validation;

use App\Helpers\ManageSubtitutePlayer;
use App\Helpers\Notification;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use Exception;
use Illuminate\Support\Facades\DB;

class FreeRegist{

    public function regist($tournament, $teamId, $tournamentType){

                switch($tournamentType){
                    case 'Leaderboard':
                        return $this->Leaderboard($tournament, $teamId);
                    break;

                    case 'Bracket':
                        return $this->Bracket($tournament, $teamId);
                    break;
                }
    }

    private function Leaderboard($tournament, $teamId){
        try{
            $getTeam = TeamModel::where('team_id', $teamId)
                        ->with('player')->first();

            $players = explode(',',$getTeam->player->player_id);

        }catch(\Exception $e){

            return $e;

        }

        try{

            DB::transaction(function () use($tournament, $teamId, $players,$getTeam) {

                $availableGroup = new CheckAvailableGroup();
                $leaderboardTeam = new LeaderboardTeamModel();
                $notification = new Notification();
                $group = $availableGroup->check($tournament);


                if($tournament->tournament_category != 3 AND $tournament->is_no_qualification != 1){
                    $qualification_sequence = 0;
                    $isFinal = 0;
                }elseif($tournament->tournament_category != 3 AND $tournament->is_no_qualification == 1){
                    $qualification_sequence = 0;
                    $isFinal = 1;
                }
                else{
                    $qualification_sequence = 1;
                    $isFinal = 0;
                }


                foreach($players as $p){


                        // dd($userTicketBalance);
                    $paymentHistory = new PlayerPayment();
                    $paymentHistory->id_player_payment = uniqid();
                    $paymentHistory->id_tournament = $tournament->id_leaderboard;
                    $paymentHistory->id_team = $teamId;
                    $paymentHistory->player_id = $p;
                    $paymentHistory->price_of_payment = 'Free';
                    $paymentHistory->amount = '-';
                    $paymentHistory->type_payment = '-';
                    $paymentHistory->status = 'Lunas';
                    $paymentHistory->date_payment = date('Y-m-d H:i:s');
                    $paymentHistory->save();


                    //notification
                    $link = 'leaderboard/'.$tournament->url;
                    $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                    $notification->singleNotification($p, $link, $message);

                    $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $tournament->game_competition);
                    $leaderboardTeamPlayer = new LeaderboardTeamPlayerModel();
                    $leaderboardTeamPlayer->id_teamplayer_leaderboard = $p;
                    $leaderboardTeamPlayer->id_team_leaderboard = $getTeam->team_id;
                    $leaderboardTeamPlayer->id_leaderboard = $tournament->id_leaderboard;
                    $leaderboardTeamPlayer->player_name = $usernameIngame;

                    if($p == $getTeam->leader_id){
                        $leaderboardTeamPlayer->player_status = 1;
                    }else{
                        $leaderboardTeamPlayer->player_status = 0;
                    }

                    $leaderboardTeamPlayer->payment_status = 'Lunas';
                    $leaderboardTeamPlayer->save();


                }

                $leaderboardTeam->id_team_leaderboard = $getTeam->team_id;
                $leaderboardTeam->id_leaderboard = $tournament->id_leaderboard;
                $leaderboardTeam->qualification_sequence = $qualification_sequence;
                $leaderboardTeam->urutan_grup = $group;
                $leaderboardTeam->nama_team = $getTeam->team_name;
                $leaderboardTeam->team_logo = $getTeam->team_logo;
                $leaderboardTeam->is_final = $isFinal;
                $leaderboardTeam->registrasi_status = 'Sudah_Registrasi';

                $leaderboardTeam->save();

                //add substitute player
                $substitutePlayer = (new ManageSubtitutePlayer)->insertToLeaderboard($tournament->id_leaderboard, $getTeam->team_id, $tournament->game_competition);
                if($substitutePlayer != true){
                    throw new Exception('Gagal menambahkan substitute player');
                }
            });

            }catch(\Exception $e){

                return $e;

            }

            return true;
    }

    private function Bracket($tournament, $teamId){
        try{
            $getTeam = TeamModel::where('team_id', $teamId)
                        ->with('player')->first();

            $players = explode(',',$getTeam->player->player_id);

        }catch(\Exception $e){

            return $e;

        }

        try{

            DB::transaction(function () use($tournament,$players,$teamId,$getTeam) {

                     $tournamentGroup = new TournamenGroupModel();
                     $notification = new Notification();
                     foreach($players as $p){


                        $paymentHistory = new PlayerPayment();
                        $paymentHistory->id_player_payment = uniqid();
                        $paymentHistory->id_tournament = $tournament->id_tournament;
                        $paymentHistory->id_team = $teamId;
                        $paymentHistory->player_id = $p;
                        $paymentHistory->price_of_payment = 'Free';
                        $paymentHistory->amount = '-';
                        $paymentHistory->type_payment = '-';
                        $paymentHistory->status = 'Lunas';
                        $paymentHistory->date_payment = date('Y-m-d H:i:s');
                        $paymentHistory->save();


                        //notification
                        $link = 'tournament/'.$tournament->url;
                        $message = 'Tim Anda telah terdaftar pada tournament '.$tournament->tournament_name;
                        $notification->singleNotification($p, $link, $message);

                         $usernameIngame = (new IdentityGameModel)::where('users_id', $p)
                                            ->where('game_id', $tournament->kompetisi)->first();


                         $tournamentGroupPlayer = new TournamentGroupPlayerModel();
                         $tournamentGroupPlayer->id_player = $p;
                         $tournamentGroupPlayer->id_group = $tournament->id_group;
                         $tournamentGroupPlayer->id_team = $getTeam->team_id;
                         $tournamentGroupPlayer->username_ingame = $usernameIngame->username_ingame;
                         $tournamentGroupPlayer->id_ingame = $usernameIngame->id_ingame;

                         if($p == $getTeam->leader_id){

                            $tournamentGroupPlayer->player_status = 1;

                         }else{

                            $tournamentGroupPlayer->player_status = 0;

                         }

                         $tournamentGroupPlayer->save();


                     }

                     $tournamentGroup->id_group = $tournament->id_group;
                     $tournamentGroup->id_team = $getTeam->team_id;
                     $tournamentGroup->nama_team =  $getTeam->team_name;
                     $tournamentGroup->logo_team = $getTeam->team_logo;
                     $tournamentGroup->nama_kota = $getTeam->venue;
                     $tournamentGroup->save();

                     //add substitute player
                    $substitutePlayer = (new ManageSubtitutePlayer)->insertToBracket($tournament->id_tournament, $tournament->id_group, $getTeam->team_id, $tournament->kompetisi);
                    if($substitutePlayer != true){
                        throw new Exception('Gagal menambahkan substitute player');
                    }


                    });

         }catch(\Exception $e){

             return $e;

         }

         return true;
//keep
    }

}
