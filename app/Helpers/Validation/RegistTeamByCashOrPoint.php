<?php

namespace App\Helpers\Validation;

use App\Http\Controllers\Api\UserNotificationController;
use App\Model\BalanceHistoryModel;
use App\Model\BalanceUsersModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use Carbon\Carbon;
use Carbon\Traits\Timestamp;
use Illuminate\Support\Facades\DB;

class RegistTeamByCashOrPoint
{
    /**
     * Register Tournament by cash or point
     * Pay by Leader
     */
    public function register($tournament, $teamId, $amount, $tournamentType, $cashorpoint)
    {
        try {
            $getTeam = TeamModel::where('team_id', $teamId)
            ->with('player')->firstOrfail();
            $players = explode(',',$getTeam->player->player_id);
        } catch (\Throwable $th) {
            return false;
        }

        if($tournamentType == 'Leaderboard'){

            try{

               DB::transaction(function () use($tournament,$players,$teamId,$amount,$getTeam, $cashorpoint) {

                   $availableGroup = new CheckAvailableGroup();
                   $leaderboardTeam = new LeaderboardTeamModel();

                   $group = $availableGroup->check($tournament);


                   if($tournament->tournament_category != 3 ){
                       $qualification_sequence = 0;
                       $isFinal = 0;
                   }elseif($tournament->tournament_category != 3 AND $tournament->is_no_qualification == 1){
                       $qualification_sequence = 0;
                       $isFinal = 1;
                   }
                   else{
                       $qualification_sequence = 1;
                       $isFinal = 0;
                   }

                   $array_PaymentHistory        = [];
                   $array_leaderboardTeamPlayer = [];

                   foreach($players as $p){

                        $arrayPayment = [
                            'id_player_payment' => uniqid(),
                            'id_tournament'     => $tournament->id_leaderboard,
                            'id_team'           => $teamId,
                            'player_id'         => $p,
                            'price_of_payment'  => $p == $getTeam->leader_id ? $amount : 0,
                            'amount'            => $amount,
                            'type_payment'      => 'Stream_'.$cashorpoint,
                            'status'            => 'Lunas',
                            'date_payment'      => date('Y-m-d H:i:s')
                        ];

                        array_push($array_PaymentHistory,$arrayPayment);

                        $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $tournament->game_competition);

                       $arrayTeamPlayer = [
                           'id_teamplayer_leaderboard'  => $p,
                           'id_team_leaderboard'        => $getTeam->team_id,
                           'id_leaderboard'             => $tournament->id_leaderboard,
                           'player_name'                => $usernameIngame,
                           'player_status'              => $p == $getTeam->leader_id ? 1 : 0,
                           'payment_status'             => 'Lunas'
                       ];

                       array_push($array_leaderboardTeamPlayer,$arrayTeamPlayer);
                   }

                   PlayerPayment::insert($array_PaymentHistory);

                   $balance = BalanceUsersModel::find($getTeam->leader_id);

                   if($cashorpoint == 'cash'){
                       $balance->balance = $balance->balance - $amount;
                       $type = 1;
                   }else{
                       $balance->point   = $balance->point - $amount;
                       $type = 2;
                   }

                   $balance->save();

                   $balanceHistory = new BalanceHistoryModel();

                   $balanceHistory->transactions_id     = uniqid();
                   $balanceHistory->users_id           = $getTeam->leader_id;
                   $balanceHistory->date               = Carbon::now()->toDateTimeString();
                   $balanceHistory->type               = 'cut';
                   $balanceHistory->transaction_type   = $type;
                   $balanceHistory->balance            = $amount;
                   $balanceHistory->information        = 'Paying Tournaments';
                   $balanceHistory->id_tournament      = $tournament->id_leaderboard;

                   $balanceHistory->save();


                   LeaderboardTeamPlayerModel::insert($array_leaderboardTeamPlayer);

                   $leaderboardTeam->id_team_leaderboard = $getTeam->team_id;
                   $leaderboardTeam->id_leaderboard = $tournament->id_leaderboard;
                   $leaderboardTeam->qualification_sequence = $qualification_sequence;
                   $leaderboardTeam->urutan_grup = $group;
                   $leaderboardTeam->nama_team = $getTeam->team_name;
                   $leaderboardTeam->team_logo = $getTeam->team_logo;
                   $leaderboardTeam->is_final = $isFinal;
                   $leaderboardTeam->registrasi_status = 'Sudah_Registrasi';

                   $leaderboardTeam->save();

                   $data = (object) [];
                   $data->tournament_name   = $tournament->tournament_name;
                   $data->url               = 'leaderboard/'.$tournament->url;
                   $data->player            = $players;

                   $notification = new UserNotificationController('tournamentRegitration_success',$data);



               });

               }catch(\Exception $e){

                   return false;

               }

               return true;
           }else{

               //bracket
               try{

                   DB::transaction(function () use($tournament,$players,$teamId,$amount,$getTeam,$cashorpoint) {

                        $tournamentGroup                = new TournamenGroupModel();

                        $array_PaymentHistory           = [];
                        $array_tournamentGroupPlayer    = [];

                        foreach($players as $p){

                            $arrayPayment = [
                                'id_player_payment' => uniqid(),
                                'id_tournament'     => $tournament->id_tournament,
                                'id_team'           => $teamId,
                                'player_id'         => $p,
                                'price_of_payment'  => $p == $getTeam->leader_id ? $amount : 0,
                                'amount'            => $amount,
                                'type_payment'      => 'Stream_'.$cashorpoint,
                                'status'            => 'Lunas',
                                'date_payment'      => date('Y-m-d H:i:s')
                            ];

                            array_push($array_PaymentHistory,$arrayPayment);

                            $usernameIngame = IdentityGameModel::where('users_id', $p)
                                                                ->where('game_id', $tournament->kompetisi)
                                                                ->first();

                            $arrayTeamPlayer = [
                                'id_player'        => $p,
                                'id_group'         => $tournament->id_group,
                                'id_team'          => $getTeam->team_id,
                                'username_ingame'  => $usernameIngame->username_ingame,
                                'id_ingame'        => $usernameIngame->id_ingame,
                                'player_status'    => $p == $getTeam->leader_id ? 1 : 0,
                            ];

                            array_push($array_tournamentGroupPlayer,$arrayTeamPlayer);

                        }

                        PlayerPayment::insert($array_PaymentHistory);

                        $balance = BalanceUsersModel::find($getTeam->leader_id);

                        if($cashorpoint == 'cash'){
                            $balance->balance = $balance->balance - $amount;
                            $type = 1;
                        }else{
                            $balance->point   = $balance->point - $amount;
                            $type = 2;
                        }

                        $balance->save();

                        $balanceHistory = new BalanceHistoryModel();

                        $balanceHistory->transactions_id     = uniqid();
                        $balanceHistory->users_id           = $getTeam->leader_id;
                        $balanceHistory->date               = Carbon::now()->toDateTimeString();
                        $balanceHistory->type               = 'cut';
                        $balanceHistory->transaction_type   = $type;
                        $balanceHistory->balance            = $amount;
                        $balanceHistory->information        = 'Paying Tournaments';
                        $balanceHistory->id_tournament      = $tournament->id_tournament;

                        $balanceHistory->save();



                        TournamentGroupPlayerModel::insert($array_tournamentGroupPlayer);

                        $tournamentGroup->id_group  = $tournament->id_group;
                        $tournamentGroup->id_team   = $getTeam->team_id;
                        $tournamentGroup->nama_team = $getTeam->team_name;
                        $tournamentGroup->logo_team = $getTeam->team_logo;
                        $tournamentGroup->nama_kota = $getTeam->venue;
                        $tournamentGroup->save();

                        $data = (object) [];
                        $data->tournament_name   = $tournament->tournament_name;
                        $data->url               = 'tournament/'.$tournament->url;
                        $data->player            = $players;

                        $notification = new UserNotificationController('tournamentRegitration_success',$data);

                    });

                }catch(\Exception $e){
                    return false;

                }

           }
    }
}
