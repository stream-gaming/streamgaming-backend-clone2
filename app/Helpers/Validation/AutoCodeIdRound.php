<?php

namespace App\Helpers\Validation;
use Illuminate\Support\Facades\DB;

class AutoCodeIdRound
{
    public function AutoCode($tabel,$inisial,$field ){
        $query = DB::table($tabel)->max($field);
        if ($query=="") {
            $inisials         = $inisial."0001";
            return $hasilkode = $inisials;
        }else{
            $nilaikode    = substr($query, strlen($inisial));
            $kode         = (int) $nilaikode;
            $kode         = $kode + 1;
            return $hasilkode = $inisial. str_pad($kode, 4, "0", STR_PAD_LEFT);
        }
    }
}
