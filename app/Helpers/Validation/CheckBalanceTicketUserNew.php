<?php

namespace App\Helpers\Validation;

use App\Model\TicketsModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CheckBalanceTicketUserNew
{
    public function check($idUser, $idTicket){
        try{

            $result = [];
            //get ticket user info
            $getUserTicket = TicketsModel::select(DB::raw('SUM(CASE WHEN is_used = 0 THEN 1 END) as unused_ticket, SUM(CASE WHEN is_used = 1 THEN 1 END) as used_ticket'))
                                        ->where('user_id', $idUser)
                                        ->where('ticket_id', $idTicket)
                                        ->groupBy('ticket_id')
                                        ->get();


            foreach($getUserTicket as $t){
                if($t->unused_ticket == null OR $t->unused_ticket == 0){
                    $result['status'] = false;
                    $result['message'] = Lang::get('ticket.not_enough_ticket_solo');
                }else{
                    $result['status'] = true;
                    $result['message'] = 'Tiket dapat digunakan';
                }
            }

            return $result;
        }catch(\Exception $e){

            return false;
        }
    }

    public function checkv2($idUser, $idTicket, $quantity){
        try{

            $result = [];
            //get ticket user info
            $getUserTicket = TicketsModel::where('is_used', 0)
                                        ->where('user_id', $idUser)
                                        ->where('ticket_id', $idTicket)
                                        ->count();


                if($getUserTicket < $quantity){
                    $result['status'] = false;
                    $result['message'] = Lang::get('ticket.not_enough_ticket_solo');
                }else{
                    $result['status'] = true;
                    $result['message'] = 'Tiket dapat digunakan';
                }

            return $result;
        }catch(\Exception $e){

            return false;
        }
    } //keep

}
