<?php

namespace App\Helpers\Validation;

use App\Model\BalanceUsersModel;
use App\Model\TeamPlayerModel;
use Illuminate\Support\Facades\Auth;

class CheckBalanceCashOrPoint
{
    /**
     * Check, is user's balance sufficient
     * @param float $tournamentPrice
     * @param int $numberOfMember
     * @param string $request
     * @return bool
     */
    public function isSufficient(float $tournamentPrice=0, int $numberOfMember=1 ,$request='cash')
    {
        $user = Auth::user();

        $balance = BalanceUsersModel::where('users_id',$user->user_id)->first(['balance','point']);

        $tournamentPrice = $tournamentPrice / $numberOfMember;

        if($request=='cash'){
            $isSufficient = $balance->balance >= $tournamentPrice;
        }
        elseif ($request=='point') {
            $isSufficient = $balance->point >= $tournamentPrice;
        }

        return $isSufficient;
    }
}
