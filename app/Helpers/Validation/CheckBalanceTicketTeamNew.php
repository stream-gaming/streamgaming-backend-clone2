<?php

namespace App\Helpers\Validation;

use App\Model\TicketsModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CheckBalanceTicketTeamNew{

    public function check($idTeam,$idTicket){
        try{
            $result = [];

            $getTeamTicket = TicketsModel::select(DB::raw('SUM(CASE WHEN is_used = 0 THEN 1 END) as unused_ticket, SUM(CASE WHEN is_used = 1 THEN 1 END) as used_ticket'))
                ->where('team_id', $idTeam)
                ->where('ticket_id', $idTicket)
                ->groupBy('ticket_id')
                ->get();

            if(count($getTeamTicket) == 0){
                    $result['status'] = false;
                    $result['message'] = Lang::get('ticket.not_enough_ticket_team');
                    return $result;
            }

            foreach($getTeamTicket as $t){
                if($t->unused_ticket == null OR $t->unused_ticket == 0){
                    $result['status'] = false;
                    $result['message'] = Lang::get('ticket.not_enough_ticket_team');
                }else{
                    $result['status'] = true;
                    $result['message'] = 'Tiket dapat digunakan';
                }

            }

            return $result;

        }catch(\Exception $e){

            return false;
        }
    }

    public function checkv2($idTeam,$idTicket, $quantity){
        try{
            $result = [];

            $getTeamTicket = TicketsModel::where('is_used', 0)
                ->where('team_id', $idTeam)
                ->where('ticket_id', $idTicket)
                ->count();



                if($getTeamTicket < $quantity){
                    $result['status'] = false;
                    $result['message'] = Lang::get('ticket.not_enough_ticket_team');
                }else{
                    $result['status'] = true;
                    $result['message'] = 'Tiket dapat digunakan';
                }


            return $result;

        }catch(\Exception $e){

            return false;
        } //keep
    }
}
