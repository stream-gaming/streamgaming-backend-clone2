<?php

namespace App\Helpers\Validation;

use App\Http\Controllers\TournamenList;
use App\Model\hasJoinedOtherTournamentModel;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;

class hasJoinedOtherTournament
{
    public function check($useriId,$tournamentDate)
    {

        $check = (new hasJoinedOtherTournamentModel)->hasJoinedOtherTournament($useriId,$tournamentDate);
        return $check;
    }
}
