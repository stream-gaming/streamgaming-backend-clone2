<?php

namespace App\Helpers\Validation;

use App\Helpers\Notification;
use App\Model\BalanceHistoryModel;
use App\Model\BalanceUsersModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\UserNotificationModel;
use Carbon\Carbon;
use Carbon\Traits\Timestamp;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegistMemberByCashOrPoint
{
    /**
     * Helper For Registrastion Tournament by Member Payment
     * Date Created 21 Oct 2020
     * @return string
     */
    public function register($tournament, $team_id, $tournament_type, $cash_point)
    {
        try {
            $getTeam = TeamModel::where('team_id', $team_id)
                ->with('player')->firstOrfail();
            $players = explode(',', $getTeam->player->player_id);
        } catch (\Throwable $th) {
            return false;
        }

        switch ($tournament_type) {
            case 'Leaderboard':
                return $this->leaderboard($tournament, $players, $getTeam, $cash_point);
                break;
            case 'Bracket':
                return $this->bracket($tournament, $players, $getTeam, $cash_point);
                break;
        }
    }

    public function leaderboard($tournament, $players, $getTeam, $cash_point)
    {
        // Leaderboard Tournament
        DB::beginTransaction();
        $notification = new Notification();
        $amount       = ($tournament->tournament_fee / count($players));

        try {
            $array_player_payment       = [];
            foreach ($players as $p) {

                $player_payment = [
                    'id_player_payment' => uniqid(),
                    'id_tournament'     => $tournament->id_leaderboard,
                    'id_team'           => $getTeam->team_id,
                    'player_id'         => $p,
                    'price_of_payment'  => $amount,
                    'amount'            => $tournament->tournament_fee,
                    'type_payment'      => $p == $getTeam->leader_id ? 'Stream_' . $cash_point : '',
                    'status'            => $p == $getTeam->leader_id ? 'Waiting' : 'Belum-Lunas',
                    'date_payment'      => $p == $getTeam->leader_id ? date('Y-m-d H:i:s') : '',
                    'created_at'        => Carbon::now()->toDateTimeString()

                ];

                array_push($array_player_payment, $player_payment);
            }

            $array_notification = $notification->joinTournamentRegistMember($tournament, $players, $getTeam, $amount);
            UserNotificationModel::insert($array_notification);

            PlayerPayment::insert($array_player_payment);

            $balance = BalanceUsersModel::find($getTeam->leader_id);

            if ($cash_point == 'cash') {
                if ($balance->balance < $amount) {
                    throw new Exception(trans('message.balance.cash.not-enough'));
                }
                $balance->balance = $balance->balance - $amount;
                $type = 1;
            } else {
                if ($balance->point < $amount) {
                    throw new Exception(trans('message.balance.point.not-enough'));
                }
                $balance->point   = $balance->point - $amount;
                $type = 2;
            }

            $balance->save();

            BalanceHistoryModel::create([
                'transactions_id'    => uniqid(),
                'users_id'           => $getTeam->leader_id,
                'date'               => Carbon::now()->toDateTimeString(),
                'type'               => 'cut',
                'transaction_type'   => $type,
                'balance'            => $amount,
                'information'        => 'Paying Tournaments',
                'id_tournament'      => $tournament->id_leaderboard,
            ]);


            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        return true;
    }

    public function bracket($tournament, $players, $getTeam, $cash_point)
    {
        //Bracket Tournament
        DB::beginTransaction();
        $notification = new Notification();
        $amount       = ($tournament->payment / count($players));

        try {
            $array_player_payment           = [];

            foreach ($players as $p) {

                $player_payment = [
                    'id_player_payment' => uniqid(),
                    'id_tournament'     => $tournament->id_tournament,
                    'id_team'           => $getTeam->team_id,
                    'player_id'         => $p,
                    'price_of_payment'  => $amount,
                    'amount'            => $tournament->payment,
                    'type_payment'      => $p == $getTeam->leader_id ? 'Stream_' . $cash_point : '',
                    'status'            => $p == $getTeam->leader_id ? 'Waiting' : 'Belum-Lunas',
                    'date_payment'      => $p == $getTeam->leader_id ? date('Y-m-d H:i:s') : '',
                    'created_at'        => Carbon::now()->toDateTimeString()
                ];

                array_push($array_player_payment, $player_payment);
            }

            $array_notification = $notification->joinTournamentRegistMember($tournament, $players, $getTeam, $amount);
            UserNotificationModel::insert($array_notification);

            PlayerPayment::insert($array_player_payment);

            $balance = BalanceUsersModel::find($getTeam->leader_id);

            if ($cash_point == 'cash') {
                if ($balance->balance < $amount) {
                    throw new Exception(trans('message.balance.cash.not-enough'));
                }
                $balance->balance = $balance->balance - $amount;
                $type = 1;
            } else {
                if ($balance->point < $amount) {
                    throw new Exception(trans('message.balance.point.not-enough'));
                }
                $balance->point   = $balance->point - $amount;
                $type = 2;
            }

            $balance->save();

            BalanceHistoryModel::create([
                'transactions_id'    => uniqid(),
                'users_id'           => $getTeam->leader_id,
                'date'               => Carbon::now()->toDateTimeString(),
                'type'               => 'cut',
                'transaction_type'   => $type,
                'balance'            => $amount,
                'information'        => 'Paying Tournaments',
                'id_tournament'      => $tournament->id_tournament,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        return true;
    }
}
