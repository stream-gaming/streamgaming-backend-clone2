<?php

namespace App\Helpers\Validation;

use App\Model\LeaderboardModel;
use App\Model\TournamentModel;


class tournamentStatus{

    /**
     * Checking tournament status [upcoming,registration_open,registration_closed,inprogress,completed]
     *
     * @param string $idTournament
     * @param string $type
     *  @return {
		    {
                "tournament_status": "completed"
            }
		}
     */

    public function check(string $idTournament,string $type){
        $data = [];

        switch($type){
            case 'Leaderboard':
             //Leaderboard data retrieval
             $model = LeaderboardModel::where('id_leaderboard', $idTournament)->first(['status_tournament']);

                    if($model->status_tournament == 0):
                        $data['tournament_status'] = 'upcoming';
                    elseif($model->status_tournament == 1):
                        $data['tournament_status'] = 'registration_open';
                    elseif($model->status_tournament == 2):
                        $data['tournament_status'] = 'registration_closed';
                    elseif($model->status_tournament == 3):
                        $data['tournament_status'] = 'inprogress';
                    elseif($model->status_tournament == 4):
                        $data['tournament_status'] = 'completed';
                    endif;

             break;
            case 'Bracket':
            //Bracket data retrieval
             $model = TournamentModel::where('id_tournament', $idTournament)->first(['status']);

                    if($model->status == 'pending'):
                        $data['tournament_status'] = 'upcoming';
                    elseif($model->status == 'open_registration'):
                        $data['tournament_status'] = 'registration_open';
                    elseif($model->status == 'end_registration'):
                        $data['tournament_status'] = 'registration_closed';
                    elseif($model->status == 'starting'):
                        $data['tournament_status'] = 'inprogress';
                    elseif($model->status == 'complete'):
                        $data['tournament_status'] = 'completed';
                    endif;
             break;
        }

      return $data;
    }


}
