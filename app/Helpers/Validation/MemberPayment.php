<?php

namespace App\Helpers\Validation;

use App\Helpers\Notification;
use App\Model\BalanceHistoryModel;
use App\Model\BalanceUsersModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use App\Model\UserNotificationModel;
use Carbon\Carbon;
use Carbon\Traits\Timestamp;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MemberPayment
{

    /**
     * Helper For Member Payment
     * Date Created 22 Oct 2020
     * @return string
     */
    public function playerPayment(PlayerPayment $player_payment, $tournament_type, $cash_point)
    {
        try {
            DB::beginTransaction();

            $getTeam    = TeamModel::where('team_id', $player_payment->id_team)
                ->with('player')->firstOrfail();
            $players    = explode(',', $getTeam->player->player_id);
            $amount     = $player_payment->price_of_payment;
            $notification       = new Notification();

            //- Cek Status Tournament
            $cek_status_tournament = (object) (new tournamentStatus)->check($player_payment->id_tournament, $tournament_type);
            if ($cek_status_tournament->tournament_status != 'registration_open') {
                throw new Exception(trans('message.tournament.not-open'));
            }

            //- checking if the slot is available
            $is_slot_full = (new isSlotFull)->check($player_payment->id_tournament, $tournament_type);
            if ($is_slot_full) {
                throw new Exception(trans('message.tournament.slot-full'));
            }

            if ($player_payment->status != 'Belum-Lunas') {
                throw new Exception(trans('message.tournament.cash.payment.failed'));
            }

            // - Update Status Player Payment
            $player_payment->status       = 'Waiting';
            $player_payment->type_payment = 'Stream_' . $cash_point;
            $player_payment->date_payment = date('Y-m-d H:i:s');
            $player_payment->save();

            //- Cut Balance
            $balance = BalanceUsersModel::find($player_payment->player_id);

            if ($cash_point == 'cash') {
                if ($balance->balance < $amount) {
                    throw new Exception(trans('message.balance.cash.not-enough'));
                }
                $balance->balance = $balance->balance - $amount;
                $type = 1;
            } else {
                if ($balance->point < $amount) {
                    throw new Exception(trans('message.balance.point.not-enough'));
                }
                $balance->point   = $balance->point - $amount;
                $type = 2;
            }

            $balance->save();

            //- Add History Balance
            BalanceHistoryModel::create([
                'transactions_id'    => uniqid(),
                'users_id'           => $player_payment->player_id,
                'date'               => Carbon::now()->toDateTimeString(),
                'type'               => 'cut',
                'transaction_type'   => $type,
                'balance'            => $amount,
                'information'        => 'Paying Tournaments',
                'id_tournament'      => $player_payment->id_tournament,
            ]);

            switch ($tournament_type) {
                case 'Leaderboard':
                    $tournament   = LeaderboardModel::find($player_payment->id_tournament);
                    $type_switch  = 'leaderboard';
                    break;
                case 'Bracket':
                    $tournament   = TournamentModel::find($player_payment->id_tournament);
                    $type_switch  = 'bracket';
                    break;
            }

            $is_paid_off = (new PlayerPayment)->isPaidOff($player_payment);

            //- If All Member is paid off
            if (count($players) == ($is_paid_off->count())) {

                $is_paid_off->update(['status' => 'Lunas']);

                $this->$type_switch($tournament, $players, $getTeam);

                //- Create Notification Success Join Tournament
                $array_notification = $notification->joinTournament($tournament, $players);
                UserNotificationModel::insert($array_notification);
            }

            //- Create Notification Success payment Member
            $link       = 'pay-tournament';
            $message    = trans('message.tournament.notification.paid', [
                'tournament_name' => $tournament->tournament_name,
                'fee'             => $amount,
            ]);
            $notification->singleNotification($player_payment->player_id, $link, $message);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'status'     => false,
                'message'    => $e->getMessage(),
            ];
        }
        return [
            'status'     => true,
            'message'    => trans('message.payment.success'),
        ];
    }

    public function leaderboard($tournament, $players, $getTeam)
    {
        $group = (new CheckAvailableGroup())->check($tournament);

        if ($tournament->tournament_category != 3 and $tournament->is_no_qualification != 1) {
            $qualification_sequence = 0;
            $isFinal = 0;
        } elseif ($tournament->tournament_category != 3 and $tournament->is_no_qualification == 1) {
            $qualification_sequence = 0;
            $isFinal = 1;
        } else {
            $qualification_sequence = 1;
            $isFinal = 0;
        }

        $array_leaderboardTeamPlayer = [];

        foreach ($players as $p) {

            $usernameIngame = (new IdentityGameModel)->usernameInGame($p, $tournament->game_competition);

            $arrayTeamPlayer = [
                'id_teamplayer_leaderboard'  => $p,
                'id_team_leaderboard'        => $getTeam->team_id,
                'id_leaderboard'             => $tournament->id_leaderboard,
                'player_name'                => $usernameIngame,
                'player_status'              => $p == $getTeam->leader_id ? 1 : 0,
                'payment_status'             => 'Lunas'
            ];

            array_push($array_leaderboardTeamPlayer, $arrayTeamPlayer);
        }
        LeaderboardTeamPlayerModel::insert($array_leaderboardTeamPlayer);

        $leaderboardTeam = new LeaderboardTeamModel();
        $leaderboardTeam->id_team_leaderboard    = $getTeam->team_id;
        $leaderboardTeam->id_leaderboard         = $tournament->id_leaderboard;
        $leaderboardTeam->qualification_sequence = $qualification_sequence;
        $leaderboardTeam->urutan_grup            = $group;
        $leaderboardTeam->nama_team              = $getTeam->team_name;
        $leaderboardTeam->team_logo              = $getTeam->team_logo;
        $leaderboardTeam->is_final               = $isFinal;
        $leaderboardTeam->registrasi_status      = 'Sudah_Registrasi';
        $leaderboardTeam->save();
    }

    public function bracket($tournament, $players, $getTeam)
    {
        $array_tournament_group_player    = [];

        foreach ($players as $p) {

            $usernameIngame = IdentityGameModel::where('users_id', $p)
                ->where('game_id', $tournament->kompetisi)
                ->first();

            $array_team_player = [
                'id_player'        => $p,
                'id_group'         => $tournament->id_group,
                'id_team'          => $getTeam->team_id,
                'username_ingame'  => $usernameIngame->username_ingame,
                'id_ingame'        => $usernameIngame->id_ingame,
                'player_status'    => $p == $getTeam->leader_id ? 1 : 0,
            ];

            array_push($array_tournament_group_player, $array_team_player);
        }

        TournamentGroupPlayerModel::insert($array_tournament_group_player);

        $tournamentGroup            = new TournamenGroupModel();
        $tournamentGroup->id_group  = $tournament->id_group;
        $tournamentGroup->id_team   = $getTeam->team_id;
        $tournamentGroup->nama_team = $getTeam->team_name;
        $tournamentGroup->logo_team = $getTeam->team_logo;
        $tournamentGroup->nama_kota = $getTeam->venue;
        $tournamentGroup->save();
    }
}
