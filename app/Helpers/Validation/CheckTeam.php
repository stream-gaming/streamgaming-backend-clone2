<?php

namespace App\Helpers\Validation;

use App\Model\LeaderboardModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TournamentModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class CheckTeam
{
    /**
     * Check If user ALready Has Team in Game
     * @param string $tournamentURL
     * @return array
     */
    public function hasTeam($game_id, $user_id = null /*optional user*/) //has team in same game
    {
        if (is_null($user_id)) {
            $team = TeamModel::where('game_id', $game_id)
                ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
                ->where('player_id', 'LIKE', '%' . Auth::user()->user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . Auth::user()->user_id . '%')
                ->count();
        } else {
            $team = TeamModel::where('game_id', $game_id)
                ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
                ->where('player_id', 'LIKE', '%' . $user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . $user_id . '%')
                ->count();
        }

        return ($team > 0) ? true : false;
    }

    public static function isLeader($team)
    {
        if ($team->leader_id != Auth::user()->user_id) :
            return false;
        else :
            return true;
        endif;
    }

    /**
     * Check is user have team in game and the leader
     * @param string $game_id
     * @param string $user_id
     * @return mixed
     */
    public function isLeaderOnTeam($game_id, $user_id = null /*optional user*/)
    {
        if (is_null($user_id)) {
            $team = TeamModel::with('identityInGameLeader')
                                ->where('game_id', $game_id)
                                ->where('leader_id', Auth::user()->user_id)
                                ->firstOr(function () {
                                    return false;
                                });
        } else {
            $team = TeamModel::with('identityInGameLeader')
                                ->where('game_id', $game_id)
                                ->where('leader_id', $user_id)
                                ->firstOr(function () {
                                    return false;
                                });
        }

        return $team;
    }

    /**
     * GetTeam data or false
     * @param string $game_id
     * @param string $user_id
     * @return mixed
     */
    public function getTeamPlayer($game_id, $user_id = null)
    {
        if (is_null($user_id)) {
            $team = TeamPlayerModel::join(
                'team',
                function ($join) use ($game_id) {
                    $join->on('team_player.team_id', 'team.team_id')
                        ->where('game_id', $game_id);
                }
            )
                ->where('player_id', 'LIKE', '%' . Auth::user()->user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . Auth::user()->user_id  . '%')

                ->firstOr(function () {
                    return false;
                });
        } else {
            $team = TeamPlayerModel::join(
                'team',
                function ($join) use ($game_id) {
                    $join->on('team_player.team_id', 'team.team_id')
                        ->where('game_id', $game_id);
                }
            )
                ->where('player_id', 'LIKE', '%' . $user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . $user_id . '%')

                ->firstOr(function () {
                    return false;
                });
        }

        return $team;
    }
}
