<?php

namespace App\Helpers\Validation;


use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;

class CheckAvailableGroup{

    public function check($tournament){

        if($tournament->tournament_category != 3){
            $groupInfo = LeaderboardModel::where('id_leaderboard', $tournament->id_leaderboard)->with('getGroup')->first();

            if($tournament->is_no_qualification == 1){
                $getJoinedTeam = LeaderboardTeamModel::where('id_leaderboard', $tournament->id_leaderboard)
                                ->where('urutan_grup', '100')->count();

                if($getJoinedTeam < $tournament->total_team){
                    return 100;
                }else{
                    return false;
                }

            }else{

                foreach($groupInfo->getGroup as $group){

                    if($group->group_sequence != '100'){

                        $getJoinedTeam = LeaderboardTeamModel::where('id_leaderboard', $tournament->id_leaderboard)
                        ->where('urutan_grup', $group->group_sequence)->count();


                        if($getJoinedTeam < $group->max_team){
                            return $group->group_sequence;

                        }else{
                            continue;
                        }


                    }

                }

                return false;
            }

            return false;
        }else{


            $groupInfo = LeaderboardModel::where('id_leaderboard', $tournament->id_leaderboard)
                        ->with(['getGroup' => function($q){
                            $q->where('qualification_sequence', 1);
                        }])
                        ->first();

            foreach($groupInfo->getGroup as $group){

                if($group->group_sequence != '1000'){

                    $getJoinedTeam = LeaderboardTeamModel::where('id_leaderboard', $tournament->id_leaderboard)
                    ->where('urutan_grup', $group->group_sequence)->count();


                    if($getJoinedTeam < $group->max_team){
                        return $group->group_sequence;

                    }else{
                        continue;
                    }

                }
        }

        return false;
    }
}

}
