<?php

namespace App\Helpers\Validation;

use App\Model\BalanceUsersModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\TournamentModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;

use App\Model\TicketModel;
use App\Model\TicketUserModel;
use Illuminate\Support\Facades\DB;

class checkBalanceTicketUser
{

    /**
     * Checking if ticket balance of team is enough or not
     *
     * @param $idTournament = tournament ID (example: 5f7562ac34b07)
     * @param $type = tournament system type (example: 'Leaderboard' or 'Bracket')
     * @param $id_users = id of team to be checked (example: 5f699c9cdf3d3)
     * @return boolean
     */

    public function check( $idTournament, $type, $leader_id)
    {
        $data['status'] = true;
        $isfailed = false;
        $jumlahI = 1;
        switch ($type) {

            case 'Leaderboard':
                try {
                $model = LeaderboardModel::where('id_leaderboard', $idTournament)->firstOrFail(['tournament_form', 'ticket', 'ticket_unit', 'game_competition']);
                } catch (\Throwable $th) {
                    return "Data Tournament Tidak Ditemukan";
                }
                // dump($model);
                if ($model->tournament_form == 'ticket') {
                    $team           = (new TeamModel)->getLeaderTeam($leader_id, $model->game_competition);

                    if($team == false){
                        return "Data Team Gagal Ditemukan";
                    }
                    $team_player    = (new TeamPlayerModel)->ExplodeTeam($team->player_id);
                    foreach ($team_player as $rowb) {
                        $checkBalance = TicketUserModel::where('id_users', $rowb)
                            ->where('id_ticket', $model->ticket)
                            ->first(['ticket']);
                        $usenameIngame = DB::table('identity_ingame')
                            ->where('users_id', $rowb)
                            ->where('game_id', $model->game_competition)
                            ->first(['username_ingame']);
                        if ($checkBalance == true) {
                            if ($checkBalance->ticket >= $model->ticket_unit) {
                                $data['status'] = 'true';
                            } else {
                                $data['status'] = 'false';
                                $data['message'][] =  $usenameIngame->username_ingame . ' Tidak memiliki Tiket yang cukup ';
                                $jumlahI++;
                                $isfailed = true;
                            }
                        } else {
                            $data['status'] = 'false';
                            $data['message'][] =  $usenameIngame->username_ingame . '  Tidak Memiliki Ticket ';
                            $jumlahI++;
                            $isfailed = true;
                        }
                        if ($isfailed) {
                            $data['status'] = 'false';
                        }
                        // $tes[] = $rowb->player_id;
                    }
                    return $data;
                }else{
                    return false;
                }

                break;

            case 'Bracket':
                try {
                      $model = TournamentModel::where('id_tournament', $idTournament)->firstOrFail(['system_payment', 'payment', 'kompetisi']);
                } catch (\Throwable $th) {
                    return "Data Tournament Tidak Ditemukan";
                }
                    if ($model->system_payment != 'Free' or $model->system_payment != 'Pay') {

                    $team           = (new TeamModel)->getLeaderTeam($leader_id, $model->kompetisi);
                    if($team == false){
                        return "Data Team Gagal Ditemukan";
                    }
                    $team_player    = (new TeamPlayerModel)->ExplodeTeam($team->player_id);
                    // dd($team_player);
                    foreach ($team_player as $rowb) {
                        $checkBalance = TicketUserModel::where('id_users', $rowb)
                            ->where('id_ticket', $model->system_payment)
                            ->first(['ticket']);
                        $usenameIngame = DB::table('identity_ingame')
                            ->where('users_id', $rowb)
                            ->where('game_id', $model->kompetisi)
                            ->first(['username_ingame']);
                        if ($checkBalance == true) {
                            if ($checkBalance->ticket >= $model->payment) {
                                $data['status'] = 'true';
                            } else {
                                $data['status'] = 'false';
                                $data['message'][] = $usenameIngame->username_ingame . ' Tidak memiliki Tiket yang cukup ';
                                $jumlahI++;
                                $isfailed = true;
                            }
                        } else {
                            $data['status'] = 'false';
                            $data['message'][] = $usenameIngame->username_ingame . '  Tidak Memiliki Ticket ';
                            $jumlahI++;
                            $isfailed = true;
                        }
                        if ($isfailed) {
                            $data['status'] = 'false';
                        }
                        // $tes[] = $rowb->player_id;
                    }
                    return $data;
                    }else{
                        return false;
                    }


                break;

            default:
            return "Format tidak Ditemukan";
            break;
        }
    }

    /**
     * Checking if ticket balance of team is enough or not
     *
     * @param string $ticketId = ticketId
     * @param string $ticketname
     * @param string $teamId = id of team to be checked
     * @param int $value = value to be compare with ticket team balance
     * @return array
     */

    public static function checkByticket($ticketId, $ticketname, $teamId, $gameId, $value=null /*optional*/)
    {
        $result      = [];
        $value       = $value == null ? 0 : $value;
        $team_player = TeamModel::find($teamId)->primaryPlayer();

        foreach ($team_player as $rowb) {

            $checkBalance = TicketUserModel::where('id_users', $rowb)
                ->where('id_ticket', $ticketId)
                ->first(['ticket']);

            $usenameIngame = (new IdentityGameModel)->usernameInGame($rowb,$gameId);

            if($usenameIngame != false){
                if ($checkBalance == true) {

                    if ($checkBalance->ticket >= $value) {
                        $data = [
                            'status' => true,
                            'text'   => $usenameIngame . ' memiliki Tiket '.$ticketname.' yang cukup'
                        ];
                    } else {
                        $data = [
                            'status' => false,
                            'text'   => $usenameIngame . ' memiliki Tiket '.$ticketname.' yang cukup'
                        ];
                    }
                    array_push($result,$data);
                } else {
                    $data = [
                        'status' => false,
                        'text'   => $usenameIngame . ' memiliki Tiket '.$ticketname.' yang cukup'
                    ];
                    array_push($result,$data);
                }
            }
        }

        return $result;
    }
}
