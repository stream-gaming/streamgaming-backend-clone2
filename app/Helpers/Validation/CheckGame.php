<?php

namespace App\Helpers\Validation;

use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class CheckGame
{
    /**
     * Check If user ALready Has Team in Game
     * @param string $tournamentURL
     * @return array
     */
    public function hasGame($game_id, $user_id = null /*optional*/)
    {
        if (is_null($user_id)) {
            $identity = IdentityGameModel::where('game_id', $game_id)
                ->where('users_id', Auth::user()->user_id)
                ->count();
        } else {
            $identity = IdentityGameModel::where('game_id', $game_id)
                ->where('users_id', $user_id)
                ->count();
        }

        return ($identity > 0) ? true : false;
    }
}
