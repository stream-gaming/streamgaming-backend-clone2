<?php

namespace App\Helpers\Validation;

use App\Helpers\Notification;
use  App\Helpers\Validation\AutoCodeIdRound;
use App\Model\BracketFinal;
use App\Model\BracketQuarterFinal;
use App\Model\BracketSemiFinal;
use App\Model\BracketSixteen;
use App\Model\BracketSixtyFour;
use App\Model\BracketThirdPlace;
use App\Model\BracketThirtyTwo;
use App\Model\TournamentGroupSolo;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use App\Model\BracketOneTwoEight;
use Illuminate\Support\Facades\DB;

class InsertSettingSolo{

    public function InsertSettingSolo($id_group){
        // try{
        $tournament = TournamentModel::where('id_group',$id_group)->first();
        $group = TournamenGroupModel::where('id_group',$id_group)->get();
        if($group == false OR $tournament == false){
            return false;
        }
        if($tournament->is_random > 0){
            return false;
        }
        if($tournament->system_tournament == 1 ){
            $total_group = count($group) * $tournament->payment;
            TournamentModel::where('id_group', $id_group)
            ->update(['total_prize' => $total_group]);
        }
        foreach($group AS $rows){
            $array[] = $rows['id_team'];
        }
        shuffle($array);

        $jumlahPlayer = count($array);
        // dd($array);
        if($jumlahPlayer > 64 ){
            $bot = 128 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }else if ($jumlahPlayer > 32 ) {
            $bot = 64 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }else if ($jumlahPlayer > 16 ) {
            $bot = 32 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }else if ($jumlahPlayer > 8 ) {
        $bot = 16 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }else if ($jumlahPlayer > 4 ) {
            $bot = 8 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }else if ($jumlahPlayer <= 4) {
        $bot = 4 - $jumlahPlayer;
            for ($i=0; $i < $bot ; $i++) {
                $array[] = null;
            }
        }
        $jumlahPlayer = count($array);
        $bagi 	= ceil($jumlahPlayer / 2) ;
        for ($i=0; $i <$bagi ; $i++) {
            $player1[] = $array[$i];
            shuffle($player1);
        }
        for ($i= $bagi; $i < $jumlahPlayer; $i++) {
            $player2[] =$array[$i];
            shuffle($player2);
        }
        for ($i=0; $i < $bagi ; $i++) {
            if($jumlahPlayer > 64 ){
                $starting = 128;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_round128','round128','id_round128');
                    $round128 = new BracketOneTwoEight();
                    $round128->id_round128      = $id_round;
                    $round128->id_tournament    = $tournament->id_tournament;
                    $round128->team1            = $player1[$i];
                    $round128->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round128->score2           = '1';
                    $round128->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round128->score1           = '1';
                    $round128->winner           = $player1[$i];
                }
                    $round128->save();
            }else if ($jumlahPlayer > 32 ) {
                $starting = 64;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_round64','round64','id_round64');
                    $round64 = new BracketSixtyFour();
                    $round64->id_round64       = $id_round;
                    $round64->id_tournament    = $tournament->id_tournament;
                    $round64->team1            = $player1[$i];
                    $round64->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round64->score2           = '1';
                    $round64->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round64->score1           = '1';
                    $round64->winner           = $player1[$i];
                }
                    $round64->save();
            }else if ($jumlahPlayer > 16 ) {
                $starting = 32;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_round32','round32','id_round32');
                    $round32 = new BracketThirtyTwo();
                    $round32->id_round32       = $id_round;
                    $round32->id_tournament    = $tournament->id_tournament;
                    $round32->team1            = $player1[$i];
                    $round32->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round32->score2           = '1';
                    $round32->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round32->score1           = '1';
                    $round32->winner           = $player1[$i];
                }
                    $round32->save();
            }else if ($jumlahPlayer > 8 ) {
                $starting = 16;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_round16','round16','id_round16');
                    $round16 = new BracketSixteen();
                    $round16->id_round16        = $id_round;
                    $round16->id_tournament    = $tournament->id_tournament;
                    $round16->team1            = $player1[$i];
                    $round16->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round16->score2           = '1';
                    $round16->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round16->score1           = '1';
                    $round16->winner           = $player1[$i];
                }
                    $round16->save();
            }else if ($jumlahPlayer > 4 ) {
                $starting = 8;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_quarter_finals','QTF','id_quarter_finals');
                    $round8 = new BracketQuarterFinal();
                    $round8->id_quarter_finals        = $id_round;
                    $round8->id_tournament    = $tournament->id_tournament;
                    $round8->team1            = $player1[$i];
                    $round8->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round8->score2           = '1';
                    $round8->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round8->score1           = '1';
                    $round8->winner           = $player1[$i];
                }
                    $round8->save();
            }else if ($jumlahPlayer <= 4) {
                $starting = 4;
                $id_round = (new AutoCodeIdRound)->AutoCode('tournament_semi_finals','SMF','id_semi_finals');
                    $round4 = new BracketSemiFinal();
                    $round4->id_semi_finals        = $id_round;
                    $round4->id_tournament    = $tournament->id_tournament;
                    $round4->team1            = $player1[$i];
                    $round4->team2            = $player2[$i];
                if($player1[$i] == null ){
                    $round4->score2           = '1';
                    $round4->winner           = $player2[$i];
                }else if ($player2[$i] == null){
                    $round4->score1           = '1';
                    $round4->winner           = $player1[$i];
                }
                $round4->save();
            }
        }
        if($starting > 64){
            $round128 = BracketOneTwoEight::where('id_tournament',$tournament->id_tournament)->get();
            if($round128 == true){
                (new BracketSixtyFour)->InsertSetting($round128,$tournament->id_tournament);
            }
        }
        if($starting > 32){
            $round64 = BracketSixtyFour::where('id_tournament',$tournament->id_tournament)->get();
            if($round64 == true){
                (new BracketThirtyTwo)->InsertSetting($round64,$tournament->id_tournament);
            }
        }
        if($starting > 16){
            $round32 = BracketThirtyTwo::where('id_tournament',$tournament->id_tournament)->get();
            if($round32 == true){
                (new BracketSixteen)->InsertSetting($round32,$tournament->id_tournament);
            }
        }
        if($starting > 8){
            $round16 = BracketSixteen::where('id_tournament',$tournament->id_tournament)->get();
            if($round16 == true){
                (new BracketQuarterFinal)->InsertSetting($round16,$tournament->id_tournament);
            }
        }
        if($starting > 4){
            $round8 = BracketQuarterFinal::where('id_tournament',$tournament->id_tournament)->get();
            if($round8 == true){
                (new BracketSemiFinal)->InsertSetting($round8,$tournament->id_tournament);
            }
        }
        if($starting > 4){
            $round4 = BracketSemiFinal::where('id_tournament',$tournament->id_tournament)->get();
            if($round4 == true){
                (new BracketThirdPlace)->InsertSetting($round4,$tournament->id_tournament);
                (new BracketFinal)->InsertSetting($round4,$tournament->id_tournament);
            }
        }
        TournamentModel::where('id_tournament', $tournament->id_tournament)
                        ->update(['status' => 'starting','is_random' => 1]);

        return true;

    // }catch(\Exception $e){
    //     return false;
    // }

    }
}
