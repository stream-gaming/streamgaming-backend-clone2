<?php

namespace App\Helpers\Validation;

use App\Model\hasJoinedTournamentModel;

class hasJoinedTournament{
  /**
     * Checking if the tournament slot is full or not
     *
     * @param string $user_id
     * @param string $idTournament
     * @return bool
     */

    public function check(string $user_id,string $idTournament){
        $model = new hasJoinedTournamentModel();
        $data = $model->checkRegistration($user_id,$idTournament);

        return $data;
    }
}
