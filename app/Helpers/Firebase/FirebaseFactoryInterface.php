<?php

namespace App\Helpers\Firebase;

use App\Model\User;

interface FirebaseFactoryInterface
{
    public function updateData(User $user,array $data);
    public function updateDataV2($reference,array $data);
    public function setData(User $user,array $data);
}
