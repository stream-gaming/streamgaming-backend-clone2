<?php

namespace App\Helpers\Firebase;

use App\Model\User;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseFactory implements FirebaseFactoryInterface{

    private $dbInstance=null;

    public function setData(User $user,array $data)
    {
        $dbInstance = $this->getFirebaseDBInstance();
        $data = $dbInstance->getReference($user->user_id)->set($data);

        return $data->getValue();
    }

    public function updateData(User $user,array $data)
    {
        $dbInstance = $this->getFirebaseDBInstance();
        $data = $dbInstance->getReference($user->user_id)->update($data);

        return $data->getValue();
    }

    public function updateDataV2($reference,array $data)
    {
        $dbInstance = $this->getFirebaseDBInstance();
        $data = $dbInstance->getReference($reference)->update($data);

        return $data->getValue();
    }

    private function getFirebaseDBInstance(): Database{

        if(is_null($this->dbInstance)){
            $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
            $firebase = (new Factory)
                            ->withServiceAccount($serviceAccount)
                            ->withDatabaseUri(config('firebase.database_url'))
                            ->create()->getDatabase();
            $this->dbInstance = $firebase;
        }

        return $this->dbInstance;

    }




}
