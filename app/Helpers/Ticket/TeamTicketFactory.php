<?php

namespace App\Helpers\Ticket;

use App\Model\AllTicketModel;
use App\Model\HistoryTicketsModel;
use App\Model\TicketsModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class TeamTicketFactory implements TicketFactoryInterface{



    public function addTicket($idTeam, $idTicket, $source , $quantity=1){
        try{

            DB::transaction(function () use($idTeam, $idTicket, $source,$quantity) {

                for($i=0; $i<$quantity; $i++){
                    //add ticket
                    $tickets = new TicketsModel;
                    $tickets->team_id = $idTeam;
                    $tickets->ticket_id = $idTicket;
                    $tickets->save();
                    $idTickets = $tickets->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel;
                    $historyTickets->team_id = $idTeam;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }
                AllTicketModel::where('id', $idTicket)->increment('total_given', $quantity);
            });

            return [
                'status' => true,
                'message' => Lang::get('ticket.'.$source)
            ];
        }catch(\Exception $e){

            return false;
        }

    }

    public function addTicketV2($idTeam, $idTicket, $source , $quantity=1){
        try{
                for($i=0; $i<$quantity; $i++){
                    //add ticket
                    $tickets = new TicketsModel;
                    $tickets->team_id = $idTeam;
                    $tickets->ticket_id = $idTicket;
                    $tickets->save();
                    $idTickets = $tickets->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel;
                    $historyTickets->team_id = $idTeam;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }
                AllTicketModel::where('id', $idTicket)->increment('total_given', $quantity);

            return true;
        }catch(\Exception $e){
            return false;
        }

    }

    public function reduceTicket($idTeam, $idTicket, $source , $quantity=1){

                $reduceTicket = TicketsModel::where('team_id', $idTeam)
                            ->where('ticket_id', $idTicket)
                            ->where('is_used', '0')
                            ->orderBy('created_at', 'desc')
                            ->limit($quantity)
                            ->update(['is_used' => '1']);
                $idTickets = $idTicket;

                for($i=0; $i<$quantity; $i++){
                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel;
                    $historyTickets->team_id = $idTeam;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::DECREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }

            return [
                'status' => true,
                'message' => $idTickets
            ];



    }




    public function updateTicket($idTeam, $idTicket, $source , $quantity=1){
        try{

            DB::transaction(function () use($idTeam, $idTicket, $source,$quantity) {

                for($i=0; $i<$quantity; $i++){
                    $reduceTicket = TicketsModel::where('team_id', $idTeam)
                    ->where('ticket_id', $idTicket)
                    ->where('is_used', '1')
                    ->first();

                    $reduceTicket->update(['is_used' => '0']);
                    $idTickets = $reduceTicket->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel;
                    $historyTickets->team_id = $idTeam;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save(); //save again
                }

            });

            return [
                'status' => true,
                'message' => Lang::get('ticket.'.$source)
            ];
        }catch(\Exception $e){

            return $e;
        }

    }



}
