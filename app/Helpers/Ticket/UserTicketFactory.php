<?php

namespace App\Helpers\Ticket;

use App\Model\AllTicketModel;
use App\Model\HistoryTicketsModel;
use App\Model\TicketsModel;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class UserTicketFactory implements TicketFactoryInterface{


    public function addTicket($idUser, $idTicket, $source , $quantity=1){
        try{


            DB::transaction(function () use($idUser, $idTicket, $source,$quantity) {

                for($i=0; $i<$quantity; $i++){
                    //add ticket
                    $tickets = new TicketsModel();
                    $tickets->user_id = $idUser;
                    $tickets->ticket_id = $idTicket;
                    $tickets->save();
                    $idTickets = $tickets->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel();
                    $historyTickets->user_id = $idUser;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }
                AllTicketModel::where('id', $idTicket)->increment('total_given', $quantity);

            });

            return [
                'status' => true,
                'message' => Lang::get('ticket.'.$source)
            ];
        }catch(\Exception $e){

            return false;
        }
    }

    public function addTicketV2($idUser, $idTicket, $source , $quantity=1){
        try{

                for($i=0; $i<$quantity; $i++){
                    //add ticket
                    $tickets = new TicketsModel();
                    $tickets->user_id = $idUser;
                    $tickets->ticket_id = $idTicket;
                    $tickets->save();
                    $idTickets = $tickets->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel();
                    $historyTickets->user_id = $idUser;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }
                AllTicketModel::where('id', $idTicket)->increment('total_given', $quantity);

            return true;
        }catch(\Exception $e){
            Log::debug($e->getMessage());
            return false;
        }
    }

    public function reduceTicket($idUser, $idTicket, $source, $quantity=1){


               $reduceTicket = TicketsModel::where('user_id', $idUser)
                            ->where('ticket_id', $idTicket)
                            ->where('is_used', '0')
                            ->orderBy('created_at', 'desc')
                            ->limit($quantity)
                            ->update(['is_used' => '1']);
            $idTickets = $idTicket;

                for($i=0; $i<$quantity; $i++){
                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel;
                    $historyTickets->user_id = $idUser;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::DECREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }

                return [
                    'status' => true,
                    'message' => $idTickets
                ];



    }
// keep


    public function updateTicket($idUser, $idTicket, $source , $quantity=1){
        try{



            DB::transaction(function () use($idUser, $idTicket, $source,$quantity) {

                for($i=0; $i<$quantity; $i++){

                    $updateTicket = TicketsModel::where('user_id', $idUser)
                    ->where('ticket_id', $idTicket)
                    ->where('is_used', '1')
                    ->first();

                    $updateTicket->update(['is_used' => '0']);
                    $idTickets = $updateTicket->id;

                    //add ticket's history
                    $historyTickets = new HistoryTicketsModel();
                    $historyTickets->user_id = $idUser;
                    $historyTickets->ticket_id = $idTickets;
                    $historyTickets->type = TicketRole::INCREASE;
                    $historyTickets->source = $source;
                    $historyTickets->save();
                }


            });

            return [
                'status' => true,
                'message' => Lang::get('ticket.'.$source)
            ];
        }catch(\Exception $e){

            return $e;
        }
    }



}
