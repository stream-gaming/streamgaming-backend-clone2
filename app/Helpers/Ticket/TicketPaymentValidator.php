<?php

namespace App\Helpers\Ticket;

use App\Helpers\Validation\CheckTeam;
use Exception;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;


class TicketPaymentValidator{

    public function validateTicketPayment($user, $requestData){

            $req = $requestData;
            $ticketValidation = new TicketValidation;

            //validate ticket
            $ticket  = $ticketValidation->ticketValidation($req->ticket_id);
            if($ticket == false) throw new Exception(Lang::get('ticket.expired_ticket'));
            if($ticket->market_value <= 0) throw new Exception(Lang::get('ticket.expired_ticket'));

            //user validation
            if($ticket->mode == 1){
                $validation = $ticketValidation->isThisUserAllowed($ticket, $user->user_id, $req->quantity);

            }else if($ticket->mode == 2 OR $ticket->mode == 3){
                //validate user's game team
                if($ticket->competition != 'all' && $ticket->competition != $req->game_id){
                    throw new Exception('transaction is not valid', 400);
                }

                //validate user's status on team
                $getTeam = (new CheckTeam)->isLeaderOnTeam($req->game_id, $user->user_id);
                if(!$getTeam) throw new Exception("Only team leader allowed to make this transaction", 401);

                $validation = $ticketValidation->isThisTeamAllowed($ticket, $user->user_id, $req->quantity);

            }

            if(!$validation['status']) throw new Exception($validation['message']);

            return $ticket;

    }
}
