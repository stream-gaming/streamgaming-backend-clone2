<?php

namespace App\Helpers\Ticket;

class TicketRole{

   //As ticket limit
   const LIMIT = 8;

   //As type for history_tickets table
   const INCREASE = 'add';
   const DECREASE = 'cut';

   //As source for history tickets table
   const BUY_SOURCE = '1';
   const PAYTOUR_SOURCE = '2';
   const REFUND_SOURCE = '3';
   const TOURGIFT_SOURCE = '4';
   const ADMINGIFT_SOURCE = '5';
   const CLAIMS_SOURCE = '6';

   const MODE_SOLO ='1';
   const MODE_DUO ='2';
   const MODE_SQUAD ='3';
//keep
    public static function checkTicketType(int $type){
        switch($type){
            case '1':
                return 'REGULAR';
            break;
            case '2':
                return 'PREMIUM';
            break;
            case '3':
                return 'PLATINUM';
            break;
        }
    }
}
