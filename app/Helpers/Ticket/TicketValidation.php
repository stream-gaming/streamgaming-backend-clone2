<?php

namespace App\Helpers\Ticket;

use App\Helpers\Validation\CheckTeam;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Model\AllTicketModel;
use App\Model\BalanceUsersModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\TeamModel;
use App\Model\TicketsModel;
use App\Model\TournamentModel;
use App\Transformer\TicketPaymentTransform;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TicketValidation{

    private $maxPerTicket = 8;

    //Range ticket limit uses the month unit
    private $rangeTicketLimit = 1;

    public function maxPerTicketSetter($limit){
        $this->maxPerTicket = $limit;
    }

    public function rangeTicketLimitSetter($limit){
        $this->rangeTicketLimit = $limit;
    }

    public function isThisUserAllowed($ticket, $idUser, $quantity=1){
        try{

        //This condition is to check if this ticket has correct mode (1 indicates ticket user)
            if($ticket->mode != 1){
                return [
                    'status' => false,
                    'message' => 'mode tiket tidak sesuai'
                ];

            }

            /* This condition is to check if total given ticket has reach max given
            limitation or not */
            if($ticket->max_given != 0){
                if(($ticket->total_given + $quantity) > $ticket->max_given){
                    if($quantity == 1){
                        $restUnit = 1;
                    }

                    if($quantity > 1){
                        $restUnit = $ticket->max_given - $ticket->total_given;
                    }

                    /* this condition will execute incase total given ticket has reach
                    max given ticket limitation */
                    return [
                        'status' => false,
                        'message' => 'Hanya tersisa '.$restUnit.' unit tiket yang dapat dibeli'
                    ];
                }
            }

            /* This will be returned in case all of validation has been passed*/
            return [
                'status' => true,
                'message' => 'this user is allowed'
            ];

        }catch(\Exception $e){
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function isThisTeamAllowed( $ticket, $idTeam, $quantity=1){
        try{

        //This condition is to check if this ticket has correct mode (2 or 3 indicates ticket team)
        if($ticket->mode != 2 AND $ticket->mode != 3 ){
            return [
                'status' => false,
                'message' => 'mode tiket tidak sesuai'
            ];

        }

        /* This condition is to check if total given ticket has reach max given
        limitation or not */
        if($ticket->max_given != 0){
            if(($ticket->total_given + $quantity) > $ticket->max_given){
                if($quantity == 1){
                    $restUnit = 1;
                }

                if($quantity > 1){
                    $restUnit = $ticket->max_given - $ticket->total_given;
                }

                /* this condition will execute incase total given ticket has reach
                max given ticket limitation */
                return [
                    'status' => false,
                    'message' => 'Hanya tersisa '.$restUnit.' unit tiket yang dapat dibeli'
                ];
            }
        }



    /* This will be returned in case all of validation has been passed*/
        return [
            'status' => true,
            'message' => 'this user is allowed'
        ];

    }catch(\Exception $e){
        return [
            'status' => false,
            'message' => $e->getMessage()
        ];
    }

    }


    public function getTicketUserInfo($idUser, $idTicket, $rangeTicketLimit = 1){
        try{

            if($rangeTicketLimit > 1) {
                $dateS = Carbon::now()->startOfMonth()->subMonth($rangeTicketLimit);
                $dateE = Carbon::now()->startOfMonth();
            }


            $result = [
                "unused_ticket" => 0,
                "used_ticket" => 0
                ];
            //get ticket team info
            $getUserTicket = TicketsModel::select(DB::raw('SUM(CASE WHEN is_used = 0 THEN 1 END) as unused_ticket, SUM(CASE WHEN is_used = 1 THEN 1 END) as used_ticket'))
                                        ->where('user_id', $idUser)
                                        ->where('ticket_id', $idTicket)
                                        ->groupBy('ticket_id');

            if($rangeTicketLimit > 1) {
                $getUserTicket = $getUserTicket->whereBetween('created_at',[$dateS,$dateE]);
            }else{
                $getUserTicket = $getUserTicket->whereYear('created_at', Carbon::now()->year);
                $getUserTicket = $getUserTicket->whereMonth('created_at', Carbon::now()->month);
            }


            $getUserTicket = $getUserTicket->get();

            foreach($getUserTicket as $t){
                $result["unused_ticket"] = $t->unused_ticket;
                $result["used_ticket"] = $t->used_ticket;
            }

            return $result;
        }catch(\Exception $e){

            return false;
        }
    }


    public function getTicketTeamInfo($idTeam, $idTicket, $rangeTicketLimit){
        try{

            if($rangeTicketLimit > 1) {
                $dateS = Carbon::now()->startOfMonth()->subMonth($rangeTicketLimit);
                $dateE = Carbon::now()->startOfMonth();
            }


            $result = [
                "unused_ticket" => 0,
                "used_ticket" => 0
                ];
            //get ticket team info
            $getTeamTicket = TicketsModel::select(DB::raw('SUM(CASE WHEN is_used = 0 THEN 1 END) as unused_ticket, SUM(CASE WHEN is_used = 1 THEN 1 END) as used_ticket'))
                                        ->where('team_id', $idTeam)
                                        ->where('ticket_id', $idTicket)
                                        ->groupBy('ticket_id');

            if($rangeTicketLimit > 1) {
                $getTeamTicket = $getTeamTicket->whereBetween('created_at',[$dateS,$dateE]);
            }else{
                $getTeamTicket = $getTeamTicket->whereYear('created_at', Carbon::now()->year);
                $getTeamTicket = $getTeamTicket->whereMonth('created_at', Carbon::now()->month);
            }

            $getTeamTicket = $getTeamTicket->get();

            foreach($getTeamTicket as $t){
                $result["unused_ticket"] = $t->unused_ticket;
                $result["used_ticket"] = $t->used_ticket;
            }

            return $result;
        }catch(\Exception $e){

            return false;
        }

    }


    public function getTournamentTicketInfo($idTournament, $tournamentSystem){
        try{

            switch($tournamentSystem){
                case 'Leaderboard':
                    $checkPaymentMethod = (new CheckTournamentPaymentMethod)->LeaderboardPaymentMethod($idTournament);

                    if($checkPaymentMethod['payment_method'] == 'ticket'){
                        $getTicketData = LeaderboardModel::select(DB::raw('all_ticket.type, all_ticket.competition, all_ticket.image, all_ticket.max_given,all_ticket.total_given,all_ticket.expired,all_ticket.market_value, all_ticket.name,leaderboard.tournament_date,leaderboard.game_option,leaderboard.game_competition AS game'))
                                                         ->leftJoin('all_ticket', function($q){
                                                             $q->on('all_ticket.id', '=', 'leaderboard.ticket')
                                                               ->where('all_ticket.market_value','!=', '0');

                                                         })
                                                         ->where('id_leaderboard', '=', $idTournament)
                                                         ->where('is_new_ticket_feature', 1)
                                                         ->whereDate('expired','>=', Carbon::now()->toDateString())
                                                         ->first();

                        $transformTicket = (new TicketPaymentTransform)->transform($getTicketData, 'Leaderboard');

                        return [
                            'ticket_status' => true,
                            'data' => $transformTicket
                        ];
                    }

                    return [
                        'ticket_status' => false,
                        'data' => []
                    ];
                break;

                case 'Bracket':

                    $checkPaymentMethod = (new CheckTournamentPaymentMethod)->BracketPaymentMethod($idTournament);

                    if($checkPaymentMethod['payment_method'] == 'ticket'){
                        $getTicketData = TournamentModel::select(DB::raw('all_ticket.type, all_ticket.competition, all_ticket.image,all_ticket.max_given,all_ticket.total_given,all_ticket.expired,all_ticket.market_value, all_ticket.name,tournament.date,tournament.game_option, tournament.kompetisi AS game'))
                                                         ->leftJoin('all_ticket', function($q){
                                                             $q->on('all_ticket.id', '=', 'tournament.system_payment')
                                                               ->where('all_ticket.market_value','!=', '0');
                                                         })
                                                         ->where('id_tournament', '=', $idTournament)
                                                         ->where('is_new_ticket_feature', 1)
                                                         ->whereDate('expired','>=', Carbon::now()->toDateString())
                                                         ->first();

                        $transformTicket = (new TicketPaymentTransform)->transform($getTicketData, 'Bracket');

                        return [
                            'ticket_status' => true,
                            'data' => $transformTicket
                        ];
                    }

                    return [
                        'ticket_status' => false,
                        'data' => []
                    ];

                break;
            }

        }catch(\Exception $e){

            return [
                'ticket_status' => false,
                'data' => []
            ];
        }
    }

    public function buttonValidation($gameId, $game){
        $isLogin = Auth::check();
        $buttonStatus = [];
        $userDetail = [];


        //set button status incase the user has not login yet
        if(!$isLogin){
            $buttonStatus['status'] = false;
            $buttonStatus['message'] = 'Login required!';
        }else{
            if($game == 'Solo'){
                try{
                        $hasGame = IdentityGameModel::select(DB::raw('identity_ingame.username_ingame, balance_users.balance,balance_users.point '))
                                                    ->leftJoin('balance_users','balance_users.users_id', '=' ,'identity_ingame.users_id')
                                                    ->where('game_id', $gameId)
                                                    ->where('identity_ingame.users_id', Auth::user()->user_id)
                                                    ->firstOrFail();

                        $buttonStatus['status'] = true;
                        $buttonStatus['message'] = '';
                        $buttonStatus['link_payment'] = route('ticket.buy');

                        $userDetail['player_name'] = $hasGame->username_ingame;
                        $userDetail['stream_cash'] = $hasGame->balance;
                        $userDetail['stream_point'] = $hasGame->point;


                    return [
                        'button' => $buttonStatus,
                        'user_detail' => $userDetail
                    ];
                    }catch(\Exception $e){
                        $buttonStatus['status'] = false;
                        $buttonStatus['message'] = 'Belum memiliki game terkait pada akun Anda!';
                    }
            }else{

                try{
                    $hasTeam = TeamModel::where('game_id', $gameId)
                                        ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
                                        ->where('player_id', 'LIKE', '%' . Auth::user()->user_id . '%')
                                        ->orWhere('substitute_id', 'LIKE', '%' . Auth::user()->user_id . '%')
                                        ->firstOrFail();
                    if(!$hasTeam){
                        $buttonStatus['status'] = false;
                        $buttonStatus['message'] = 'Belum memiliki tim pada akun Anda!';
                    }

                    $isLeader = (new CheckTeam)->isLeader($hasTeam);

                    if(!$isLeader){
                        $buttonStatus['status'] = false;
                        $buttonStatus['message'] = 'Hanya leader tim yang dapat membeli tiket ini';
                    }

                    $buttonStatus['status'] = true;
                    $buttonStatus['message'] = '';
                    $buttonStatus['link_payment'] = route('ticket.buy');

                    $leader = IdentityGameModel::select(DB::raw('identity_ingame.username_ingame, balance_users.balance,balance_users.point '))
                                                ->leftJoin('balance_users','balance_users.users_id', '=' ,'identity_ingame.users_id')
                                                ->where('game_id', $gameId)
                                                ->where('identity_ingame.users_id', Auth::user()->user_id)
                                                ->firstOrFail();

                    $userDetail['team_name'] = $hasTeam->team_name;
                    $userDetail['leader'] = $leader->username_ingame;
                    $userDetail['stream_cash'] = $leader->balance;
                    $userDetail['stream_point'] = $leader->point;




                }catch(\Exception $e){
                    $buttonStatus['status'] = false;
                    $buttonStatus['message'] = $e;
                }

            }
        }

        if(empty($userDetail)){
            return [
                'button' => $buttonStatus,
                'user_detail' => null
            ];
        }
            return [
                'button' => $buttonStatus,
                'user_detail' => $userDetail
            ];

    }

    public function ticketValidation($idTicket){
        try{

                $getTicket = AllTicketModel::where('id', $idTicket)
                ->whereDate('expired','>=', Carbon::now()->toDateString())
                ->firstOrFail();

                return $getTicket;

        }catch(\Exception $e){
            return false;
        }
    }


}
