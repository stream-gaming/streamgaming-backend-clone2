<?php

namespace App\Helpers\Ticket;

interface TicketFactoryInterface{

    function addTicket($idUserOrTeam, $idTicket, $source, $quantity);
    function reduceTicket($idUserOrTeam, $idTicket, $source, $quantity);


}
