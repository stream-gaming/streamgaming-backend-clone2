<?php

namespace App\Helpers\Ticket;

use App\Model\AllTicketModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class InsertTicket {

    public function InsertTicket($data){

        // if($data->type_ticket == "1"){
        //     return true;
        // }else if($data->type_ticket == "2"){
        //     return true;
        // }else if($data->type_ticket == "3"){
        //     return true;
        // }else{
        //     return false;
        // }
        // return true;
        switch($data->type_ticket){
            case 1:
                if($data->sub_type == 1){
                    $check = AllTicketModel::where('sub_type' , 1)->first();
                    if($check > 0){
                        return false;
                    }
                    if($data->can_traded != 0 AND $data->market_value != 0 AND $data->giving_target != 0){
                        return false;
                    }
                    return true;
                }else if($data->sub_type == 2 OR $data->sub_type == 3){
                    if($data->can_traded != 0 AND $data->market_value != 0 AND $data->unit != 0){
                        return false;
                    }
                    return true;
                }else{
                    return false;
                }
                break;
            case 2 :
                if($data->sub_type == 1 ){
                    return false;
                }
                return true;
                break;
            case 3 :
                if($data->sub_type == 1 ){
                    return false;
                }
                if($data->can_traded != 0 AND $data->market_value != 0){
                    return false;
                }
                return true;
                break;
        }

    }
}
