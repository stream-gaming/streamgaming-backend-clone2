<?php

namespace App\Helpers;

use App\Model\UserNotificationModel;

class Notification
{

    public function singleNotification($player_id, $link, $message)
    {
        try {

            $model = new UserNotificationModel();

            $model->id_notification = uniqid();
            $model->id_user = $player_id;
            $model->keterangan = $message;
            $model->link = $link;
            $model->is_read = 0;
            $model->date = date('Y-m-d H:i:s');
            $model->save();




            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function joinTournamentRegistMember($tournament, $players, $getTeam, $amount)
    {
        $array_player_notification  = [];

        foreach ($players as $p) {
            //Create Notification
            if ($p == $getTeam->leader_id) {
                $link       = 'pay-tournament';
                $message    = trans('message.tournament.notification.leader-pay', [
                    'tournament_name' => $tournament->tournament_name,
                ]);
                $player_notification = [
                    'id_notification' => uniqid(),
                    'id_user'         => $p,
                    'keterangan'      => $message,
                    'link'            => $link,
                    'is_read'         => 0,
                    'date'            => date('Y-m-d H:i:s'),
                ];
                array_push($array_player_notification, $player_notification);
                $message    = trans('message.tournament.notification.paid', [
                    'tournament_name' => $tournament->tournament_name,
                    'fee'             => $amount,
                ]);
                $player_notification = [
                    'id_notification' => uniqid(),
                    'id_user'         => $p,
                    'keterangan'      => $message,
                    'link'            => $link,
                    'is_read'         => 0,
                    'date'            => date('Y-m-d H:i:s'),
                ];
                array_push($array_player_notification, $player_notification);
            } else {
                $link       = 'pay-tournament';
                $message    = trans('message.tournament.notification.member-pay', [
                    'tournament_name' => $tournament->tournament_name,
                    'fee'             => $amount,
                ]);
                $player_notification = [
                    'id_notification' => uniqid(),
                    'id_user'         => $p,
                    'keterangan'      => $message,
                    'link'            => $link,
                    'is_read'         => 0,
                    'date'            => date('Y-m-d H:i:s'),
                ];
                array_push($array_player_notification, $player_notification);
            }
        }
        return $array_player_notification;
    }

    public function joinTournament($tournament, $players)
    {
        $array_player_notification  = [];

        foreach ($players as $p) {
            //Create Notification
            $link       = 'tournament/'.$tournament->url;
            $message    = trans('message.tournament.notification.success-join', [
                'tournament_name' => $tournament->tournament_name,
            ]);
            $player_notification = [
                'id_notification' => uniqid(),
                'id_user'         => $p,
                'keterangan'      => $message,
                'link'            => $link,
                'is_read'         => 0,
                'date'            => date('Y-m-d H:i:s'),
            ];
            array_push($array_player_notification, $player_notification);
        }
        return $array_player_notification;
    }

    public function teamNotification(string $team_id, string $link, string $message)
    {

        try {
        } catch (\Exception $e) {
        }
    }
}
