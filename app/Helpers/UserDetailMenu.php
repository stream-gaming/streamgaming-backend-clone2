<?php

namespace App\Helpers;



class UserDetailMenu
{

    public function menu($username, $version=null){
        $version = $version == 'mobile' ? '/'.$version : '';
        $userDetailMenu = [
            'overview' => route('user.overview', ['username' => $username]).$version,
            'match' => route('user.match', ['username' => $username]).$version,
            'statistic' => route('user.statistic', ['username' => $username]).$version
        ];


        return $userDetailMenu;
    }

}
