<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\Model\TeamModel;
use App\Model\IdentityGameModel;
use App\Helpers\Redis\RedisHelper;
use Illuminate\Support\Facades\Log;
use App\Helpers\Validation\CheckTeam;
use App\Http\Controllers\Api\UserNotificationController;

class UpdateStatusBanned
{

    public function solo()
    {
        try {
            $identities = IdentityGameModel::where('is_banned', 1)->get();

            foreach ($identities as $identity) {
                if (strtotime($identity->ban_until) < Carbon::now()->timestamp) {
                    $identity->is_banned = 0;
                    $identity->ban_until = NULL;
                    $identity->reason_ban = NULL;
                    $identity->save();

                    $identity->history_banned->is_banned = 0;
                    $identity->history_banned->save();

                    new UserNotificationController('game_unbanned', $identity);

                    //- Trigger Delete
                    RedisHelper::destroyData('my:game:user_id:' . $identity->users_id);
                    $getTeam = (new CheckTeam())->getTeamPlayer($identity->game_id, $identity->users_id);
                    if($getTeam){
                        RedisHelper::destroyData('team:detail:team_id:' . $getTeam->team_id);
                    }
                }
            }

            return true;
        } catch (\Exception $e) {
            return false;

            Log::error([
                'error'    => [
                    'message' => "Something error in helper : -" . $e->getMessage(),
                    'file'     => $e->getFile(),
                    'line'     => $e->getLine(),
                    'trace'    => $e->getTrace()
                ]
            ]);
        }
    }

    public function team()
    {
        try {
            $teams = TeamModel::where('is_banned', 1)
                ->get();

            foreach ($teams as $team) {
                if (strtotime($team->ban_until) < Carbon::now()->timestamp) {
                    $team->is_banned = 0;
                    $team->ban_until = NULL;
                    $team->reason_ban = NULL;
                    $team->save();

                    new UserNotificationController('team_unbanned', $team);
                    RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);
                    RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
                }
            }

            return true;
        } catch (\Exception $e) {
            return false;

            Log::error([
                'error'    => [
                    'message' => "Something error in helper : -" . $e->getMessage(),
                    'file'     => $e->getFile(),
                    'line'     => $e->getLine(),
                    'trace'    => $e->getTrace()
                ]
            ]);
        }
    }
}
