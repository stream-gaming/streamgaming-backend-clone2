<?php

namespace App\Helpers\Builders;

use App\Http\Controllers\Api\UserNotificationController;
use Exception;

class NotificationBuilder implements NotificationBuilderInterface
{
    protected $method;
    protected $message;

    public function setMethod(string $method)
    {
        $this->method = $method;
        return $this;
    }

    public function setMessage(array $message)
    {
        $this->message = $message;
        return $this;
    }

    public function sendNotif(): bool
    {
        if(is_null($this->method)){
            throw new Exception("you have to set method", 1);
        }

        if(is_null($this->message)){
            throw new Exception("you have to set message", 1);
        }

        //Send notification to user
        new UserNotificationController($this->method,(object) $this->message);

        return true;
    }
}
