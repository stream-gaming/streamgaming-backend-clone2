<?php

namespace App\Helpers\Builders;

interface NotificationBuilderInterface
{
    public function setMethod(string $method);
    public function setMessage(array $message);
    public function sendNotif();
}
