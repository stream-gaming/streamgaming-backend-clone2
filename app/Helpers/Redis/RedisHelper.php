<?php

namespace App\Helpers\Redis;

use App\Jobs\CacheTriggerPortalJob;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class RedisHelper{

    /**
     * @return boolean|json
     */
    public static function getData(string $key=null) {
        if(!is_null($key)){
            if(Redis::exists($key)){
                return response()->json(json_decode(Redis::get($key)));
            }
            return false;
        }
        return false;
    }

        /**
     * @return boolean|json
     */
    public static function getDataCustom(string $key=null) {
        if(!is_null($key)){
            if(Redis::exists($key)){
                return json_decode(Redis::get($key));
            }
            return false;
        }
        return false;
    }

    /**
     * @return void|boolean
     */
    public static function setData(string $key=null, array $arrayData=null, int $expTimeInSecond=null){
        if(is_null($key) OR is_null($arrayData)) return false;

        if(!is_null($expTimeInSecond)){
            Redis::set($key, json_encode($arrayData), 'EX', $expTimeInSecond);
        }else{
            Redis::set($key, json_encode($arrayData));
        }

    }

    /**
     * @return void
     */
    public static function destroyData(string $key=null){
        if(is_null($key)) return false;

        Redis::del($key);
    }

    public static function destroyDataWithArray($key, $array, $pattern = true)
    {
        $keys = [];
        if (!empty($array)) {
            foreach($array as $k => $item){
                if($pattern){
                    $key_custom = $key . ':' . $item . ":";
                    self::destroyWithPattern($key_custom);
                }else{
                    $key_custom = $key . ':' . $item;
                    Redis::del($key_custom);
                }
                $keys[$k] = $key_custom;
            }
        }
        return $keys;
    }

    public static function destroyDataWithPattern($key){
        $keys = Redis::keys($key);

        if ( !empty( $keys ) ){
            $keys = array_map(function ($k){
                return str_replace(config("cache.prefix").'_', '', $k);
            }, $keys);

            Redis::del($keys);
        }
        return $keys;
    }
    
    /**
     * This code is used to destroy multiple cache data with pattern,
     * example if you want to delete all of caches named with leaderboard.xxx prefix
     * you can just pass the key parameter with 'leaderboard' string
     * @return void
     */
    public static function destroyWithPattern(string $key=null){
        if(is_null($key)) return false;

        $prefix = config("cache.prefix").'_';
        $getCache = Redis::keys($key.'*');
        foreach($getCache as $v){
            $removePrefix = explode($prefix,$v);
            if(!isset($removePrefix[1])) continue;
            Redis::del($removePrefix[1]);
        }

    }
}
