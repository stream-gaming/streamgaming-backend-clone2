<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

/**
 * Class Fcm
 */
class FirebaseCloudMessaging implements ShouldQueue
{
    use Queueable;

    const ENDPOINT = 'https://fcm.googleapis.com/fcm/send';

    protected $recipients;
    protected $topic;
    protected $data;
    protected $notification;
    protected $timeToLive;
    protected $priority;

    protected $serverKey;

    public function __construct()
    {
        $this->serverKey = config("fcm.server_key");
        $this->client    = new Client();
    }

    public function to(string $recipients)
    {
        $this->recipients = $recipients;

        return $this;
    }

    public function toTopic(array $topic)
    {
        $this->topic = $topic;

        return $this;
    }

    public function data(array $data = [])
    {
        $this->data = $data;

        return $this;
    }

    public function notification(array $notification = [])
    {
        $this->notification = $notification;

        return $this;
    }

    public function priority(string $priority)
    {
        $this->priority = $priority;

        return $this;
    }

    public function timeToLive(int $timeToLive)
    {
        if ($timeToLive < 0) {
            $timeToLive = 0; // (0 seconds)
        }
        if ($timeToLive > 2419200) {
            $timeToLive = 2419200; // (28 days)
        }

        $this->timeToLive = $timeToLive;

        return $this;
    }

    public function send()
    {
        $payloads = [
            'content_available' => true,
            'priority' => isset($this->priority) ? $this->priority : 'high',
            'data' => $this->data,
            'notification' => $this->notification
        ];

        if ($this->topic) {
            $payloads['registration_ids'] = $this->topic;
        } else {
            $payloads['to'] = $this->recipients;
        }

        if ($this->timeToLive !== null && $this->timeToLive >= 0) {
            $payloads['time_to_live'] = (int) $this->timeToLive;
        }

        $headers = [
            'Authorization' => "key=" . $this->serverKey,
            'Content-Type'  => 'application/json',
        ];

        $request = $this->client->post(self::ENDPOINT, [
            'headers' => $headers,
            RequestOptions::JSON => $payloads
        ]);

        return $request->getBody()->getContents();
    }

    public function sendToMobile()
    {
        $payloads = [
            'content_available' => true,
            'priority' => isset($this->priority) ? $this->priority : 'high',
            'data' => $this->data,
        ];

        if ($this->topic) {
            $payloads['registration_ids'] = $this->topic;
        } else {
            $payloads['to'] = $this->recipients;
        }

        if ($this->timeToLive !== null && $this->timeToLive >= 0) {
            $payloads['time_to_live'] = (int) $this->timeToLive;
        }

        $headers = [
            'Authorization' => "key=" . $this->serverKey,
            'Content-Type'  => 'application/json',
        ];

        $request = $this->client->post(self::ENDPOINT, [
            'headers' => $headers,
            RequestOptions::JSON => $payloads
        ]);

        return $request->getBody()->getContents();
    }
}
