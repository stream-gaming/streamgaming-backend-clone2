<?php

namespace App\Helpers;

use App\Helpers\Validation\CheckBanned;
use App\Helpers\Validation\CheckGame;
use App\Helpers\Validation\CheckTeam;
use App\Helpers\Validation\hasJoinedTournament;
use App\Http\Controllers\Api\Tournament\UserJoinTournamentValidation;
use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\Auth;

class JoinTournamentButton
{
    /**
     * Get JoinTournamentButton Status
     * @param mixed $is_open
     * @param float $is_full (the result of division slot filled and slot available)
     * @param string $game_id
     * @param string $id_tournament
     * @return mixed
     */
    static public function getStatus($is_open, $is_full, $game_id, $id_tournament, $tournament_type, $tournament)
    {

        // dah login belum?
        if(!Auth::check()){
            return [
                'status'     => false,
                'message'    => "Authentication required",
                'code'       => 1,
                'rules_button' => null
            ];
        }

        $closed = [
            'status'     => false,
            'message'    => "Tournament registation closed",
            'code'       => 2,
            'rules_button' => null
        ];

        // tournamen open gak?
        if($tournament_type == "leaderboard"){
            if($is_open != 1 || $is_open==0){
                return $closed;
            }
            $quantity = $tournament->ticket_unit;
        } else if($tournament_type == "bracket"){
            if($is_open != "open_registration"){
                return $closed;
            }
            $quantity = $tournament->payment;
        }

        // slot masih ada gak?
        if($is_full == 1){
            return [
                'status'     => false,
                'message'    => "Oh no, you late. No more slots available :(",
                'code'       => 3,
                'rules_button' => null
            ];
        }


        // leader gak? punya team gak?
        // $team = (new CheckTeam)->isLeaderOnTeam($game_id);
        // if(!$team){
        //     return [
        //         'status'     => false,
        //         'message'    => "nolep sekali anda , gak punya team :v",
        //         'code'       => 4
        //     ];
        // }

        // udah join belum?
        if((new hasJoinedTournament)->check(Auth::user()->user_id,$id_tournament)){
            return [
                'status'     => false,
                'message'    => "Owh, you has joined this tournament",
                'code'       => 5,
                'rules_button' => null
            ];
        }

        //Check new ticket feature validation
        if($tournament->is_new_ticket_feature == 1){

            // $hasGame    = (new CheckGame)->hasGame($game_id);

            // if(!$hasGame){
            //     return [
            //         'status'     => false,
            //         'message'    => "Belum Menautkan Akun Game",
            //         'code'       => 6,
            //         'rules_button' => null
            //     ];
            // }

            $identyty   = (new IdentityGameModel())->usernameInGame(Auth::user()->user_id, $game_id);
            $userOrTeam = '';
            $idUserOrTeam = '';

            if($tournament_type == "leaderboard"){
                $idTicket = $tournament->ticket;
            }else if($tournament_type == "bracket"){
                $idTicket = $tournament->system_payment;
            }

            if($tournament->game_option == 1){
                $userOrTeam ='solo';
                $idUserOrTeam = Auth::user()->user_id;

            }else{
                $userOrTeam ='team';
                // $getTeam    = $hasGame == false ? false : (new CheckTeam)->getTeamPlayer($game_id);


                // if($getTeam == false){
                //     return [
                //         'status'     => false,
                //         'message'    => "Anda Belum Memiliki Tim",
                //         'code'       => 6,
                //         'rules_button' => null
                //     ];
                // }

                // $idUserOrTeam = $getTeam->team_id;

            }




            // $checkBalanceTicket = (new UserJoinTournamentValidation)->newTicketValidation($id_tournament, $idTicket, $idUserOrTeam, $userOrTeam, $quantity);

            // if($checkBalanceTicket["status"] == false){
            //     return [
            //         'status'     => false,
            //         'message'    => "Tiket Anda Belum Mencukupi",
            //         'code'       => 6,
            //         'rules_button' => null
            //     ];
            // }


        }

        if($tournament->game_option == 1){
            $hasGame    = (new CheckGame)->hasGame($game_id);
            if($hasGame){
                $isPlayerBanned = (new CheckBanned)->isSoloBanned($game_id, null, Auth::user()->user_id , null, false);
                if($isPlayerBanned){
                    return [
                        'status'     => false,
                        'message'    => "Anda sedang diban",
                        'code'       => 6,
                        'rules_button' => null
                    ];
                }
            }

        }else{
            $hasGame    = (new CheckGame)->hasGame($game_id);
            $getTeam    = $hasGame == false ? false : (new CheckTeam)->getTeamPlayer($game_id);

            if($getTeam){
                $cekBan = (new CheckBanned)->isBanned($getTeam->team_id);
                if($cekBan){
                    return [
                        'status'     => false,
                        'message'    => "Tim Anda sedang diban",
                        'code'       => 6,
                        'rules_button' => null
                    ];
                }

            }
        }

        if($tournament_type == "leaderboard"){
            return [
                'status'     => true,
                'message'    => "enable",
                'code'       => 0,
                'game_id'    => $game_id,
                'rules_button' => (empty($tournament->button_rules) ? null : $tournament->button_rules)
            ];
        }else{
            return [
                'status'     => true,
                'message'    => "enable",
                'code'       => 0,
                'game_id'    => $game_id,
                'rules_button' => (empty($tournament->peraturan_tombol_daftar) ? null : $tournament->peraturan_tombol_daftar)
            ];
        }

    }
}
