<?php

namespace App\Helpers;

/**
 * Helper for Send Push Notification via Firebase Cloud Messaging
 */
class PushNotification
{
    public $fcm;

    public function __construct()
    {
        $this->fcm = new FirebaseCloudMessaging();
    }

    public function send($request, $token)
    {
        if ($token == null) {
            return null;
        }

        $count_token = count($token);
        switch ($count_token) {
            case $count_token > 1:
                return $this->multiSendPush($request, $token);
                break;

            default:
                return $this->sendPush($request, $token[0]);
                break;
        }
    }

    public function sendPush(Array $request, String $token)
    {
        return $this->fcm
            ->to($token)
            ->priority('high')
            ->timeToLive(604800)
            ->data($request['data'])
            ->notification([
                'title' => $request['title'],
                'body'  => $request['body'],
            ])
            ->send();
    }

    public function sendPushToMobile(Array $request, $topic)
    {

        return $this->fcm
            ->to($topic)
            ->priority('high')
            ->timeToLive(604800)
            ->data($request['data'])
            ->sendToMobile();
    }

    public function multiSendPush(Array $request, Array $token)
    {
        return $this->fcm
            ->toTopic($token)
            ->priority('high')
            ->timeToLive(604800)
            ->data($request['data'])
            ->notification([
                'title' => $request['title'],
                'body'  => $request['body'],
            ])
            ->send();
    }

    public function multiSendPush2(Array $request, Array $token)
    {

        return $this->fcm
            ->toTopic($token)
            ->priority('high')
            ->timeToLive(604800)
            ->data($request['data'])
            ->notification([
                'title' => $request['title'],
                'body'  => $request['body'],
                'image' => $request['thumbnail'],
                'click_action' => $request['destination'],
            ])
            ->send();
    }
}
