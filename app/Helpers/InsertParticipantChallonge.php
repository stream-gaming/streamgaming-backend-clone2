<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Http;
use App\Model\AccountChallongeModel;
use Illuminate\Support\Facades\DB;
use App\Model\TournamenGroupModel;
use Exception;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use PhpParser\Node\Stmt\Return_;
use App\Helpers\MyApps;
/**
 * Class Fcm
 */
class InsertParticipantChallonge
{

    public function InsertParticipant($tournament,$data){

        /* Request Data
        data = [
             'name'   => string
             'id_team =>  string
            ];
        tournament = get tournament  */
        Log::debug($tournament);
        try{

            $account= AccountChallongeModel::first('api_key');
            $dataPart = [
               'api_key' => $account['api_key'],
               'name'    => $data['name']
            ];
            $url        = $tournament->url_challonge;
            $id_group   = $tournament->id_group;
            $id_team    = $data['id_team'];
            $response = (new MyApps)->GetUrlChallonge('createParticipant',$url,$dataPart);
            $decode = json_decode($response,true);
            if(!isset($decode['errors'])){
                $id = $decode["participant"]["id"];
                $update = TournamenGroupModel::where('id_group',$id_group)
                ->where('id_team',$id_team)
                ->update(['id_participant_challonge' => $id]);
                
            }else{
                Log::debug($decode);
                throw new Exception('Gagal menambahkan Team');
            }
            return true;
        }catch(\Exception $e){       
            Log::debug($e);
            return false;
        }

    }


}
