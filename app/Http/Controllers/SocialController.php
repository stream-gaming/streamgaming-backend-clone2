<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateUserRegistrationSocial;
use App\Http\Requests\ValidateUserSocialLogin;
use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\OAuthModel;
use App\Model\User;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Exception;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * @group Authentication
 *
 * APIs for the Social Login/Register
 */
class SocialController extends AuthController
{

    /**
     * Handle Social Login/Register.
     * <aside class="notice">APIs Method for Handle Frontend for Social Login/Register process | Endpoint untuk handle proses login atau register dari sosial login</aside>
     * @bodyParam fullname string required Full Name | Nama Lengkap. Example: Melli Tiara
     * @bodyParam email email required Email | Email pengguna. Example: melly.tiara92@gmail.com
     * @bodyParam oauth_id oauth_id required oauth id data from google id user | oauth id dari sosial login berkaitan. Example: 1111111111
     * @bodyParam link_image url required link image profil picture | url gambar profil sosial login. Example: picture.jpg
     * @responseFile status=200 scenario="success login" responses/auth.login.json
     * @response scenario="failed login social | gagal login" status="400" {
            "status" : false,
            "message" : "Please Login Manually",
        }
     * @response scenario="return value for register social | nilai kembalian untuk frontend" status="200" {
            "status": true,
            "data": {
                "fullname": "Melli Tiara",
                "email": "melly.tiara92@gmail.com",
                "oauth_id": 11111111111,
                "link_image": "picture.jpg",
                "username": "mellydecyber"
            },
            "message": "Get User Success.",
            "registered": true
        }
     * @response scenario="failed register social | gagal pendaftaran" status="400" {
            "status": false,
            "message": "Get User Failed.",
            "error": {error}
        }
     */
    public function handleFrontEndCallback(ValidateUserSocialLogin $request)
    {
        try {
            $user = User::where('email', $request->input('email'))->first();
            //- Cek Apakah User ada

            if ($user) {
                // Cek Apakah Oauth Id Ada
                $user_login = OAuthModel::where('oauth_id', $request->input('oauth_id'))
                    ->where('users_id', $user->user_id)
                    ->first();
                if ($user_login) {
                    //Create New Token
                    $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->fromUser($user);
                    // Return New Token
                    return $this->createNewToken($token, $user);
                }
                //- Notif Gagal Login
                return response()->json([
                    'status' => false,
                    'message' => Lang::get('message.social.login.failed')
                ], $this->HTTP_BAD);
            }
            $data = [
                'fullname'   => $request->input('fullname'),
                'email'      => $request->input('email'),
                'oauth_id'   => $request->input('oauth_id'),
                'link_image' => $request->input('link_image'),
                'username'   => str_replace(' ', '_', $request->input('fullname')) . rand(1000, 9999),
            ];

            return response()->json([
                'status'     => true,
                'data'       => $data,
                'message'    => Lang::get('message.social.get.success'),
                'registered' => true
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.social.get.failed'),
                'error'   => $e
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Register Account By Social.
     * <aside class="notice">APIs Method for Register New User By Social Login process | Endpoint untuk menyimpan pendaftaran user baru via social register</aside>
     * @bodyParam fullname string required Full Name | Nama Lengkap. Example: Melli Tiara
     * @bodyParam username string required Username | Username. Example: mellydecyber
     * @bodyParam jenis_kelamin string required The value must be one of laki-laki or perempuan | nilai harus 'laki-laki' atau 'perempuan'. Example: perempuan
     * @bodyParam email email required Email | Email pengguna. Example: melly.tiara92@gmail.com
     * @bodyParam tgl_lahir string required The value must be a valid date in the format "Y-m-d" | format tanggal harus "Y-m-d". Example: 1999-12-27
     * @bodyParam nomor_hp number required for Number Phone. Example: 080009990007
     * @bodyParam password password required Password for register. Example: streamgaming
     * @bodyParam password_confirmation password required Password Confirmation | Konfirmasi password. Example: streamgaming
     * @bodyParam link_image url required Image Link Profil Picture | url link gambar profil. Example: picture.jpg
     * @bodyParam provider_id provider_id required Provider id | provider yang digunakan untuk pendaftaran. Example: google.com
     * @bodyParam oauth_id oauth_id required oauth id data from google id user | oauth id dari provider. Example: 111111111
     * @responseFile status=200 scenario="success login" responses/auth.login.json
     * @response scenario="validation form input" status="422" {
            "message": "The given data was invalid.",
            "errors": {
                "email": [
                    "Email sudah digunakan."
                ],
                "password": [
                    "Format Password tidak valid."
                ]
            }
        }
     * @response scenario="register failed" status="409" {
            "status": false,
            "message": "User Registration Failed!",
            "error": {
                "errorInfo": [
                    "23000",
                    1048,
                ],
                "xdebug_message": "{message error}"
            }
        }
     */
    public function registerSocial(ValidateUserRegistrationSocial $request)
    {
        try {
            if ($request->input('oauth_email') && $request->input('email')) {
                return response()->json([
                    'status'  => false,
                    'message' => Lang::get('message.social.denied')
                ], $this->HTTP_BAD);
            }

            // Initiate Class
            DB::beginTransaction();
            // - Input Data User
            $user = User::create([
                'user_id'       => uniqid(),
                'fullname'      => $request->input('fullname'),
                'username'      => $request->input('username'),
                'jenis_kelamin' => $request->input('jenis_kelamin'),
                'email'         => $request->input('email'),
                'tgl_lahir'     => $request->input('tgl_lahir'),
                'alamat'        => $request->input('alamat'),
                'bio'           => $request->input('bio'),
                'provinsi'      => $request->input('provinsi'),
                'kota'          => $request->input('kota'),
                'status'        => 'player',
                'is_verified'   => 1,
                'is_active'     => 1,
                'password'      => password_hash($request->input('password'), PASSWORD_DEFAULT),
            ]);

            $user->detail()->save(new UsersDetailModel([
                'id_card'         => '',
                'id_card_number'  => '',
                'image'           => '',
                'link_image'      => $request->input('link_image', ''),
                'id_status'       => 0,
            ]));

            $user->balance()->save(new BalanceUsersModel([
                'balance'         => 0,
                'point'           => 0,
            ]));

            $user->authenticate()->save(new AuthenticateModel([
                'authenticate'     => '',
                'activity'         => 1,
            ]));

            $user->usersGame()->save(new UsersGameModel([
                'game_id'  =>   '',
            ]));

            $user->preferences()->save(new UserPreferencesModel([
                'users_preferences_id'  => uniqid(),
                'email_notification'    => '0'
            ]));

            $user->oAuth()->save(new OAuthModel([
                'provider_id' => $request->input('provider_id'),
                'oauth_id'    => $request->input('oauth_id'),
            ]));

            // $request->user_id = uniqid();
            // $user             = (new User())->addUser($request, $data);
            //- Input Data Tambahan User
            // (new UsersDetailModel)->addUsersDetail($request);
            // (new BalanceUsersModel)->addBalanceUsers($request);
            // (new AuthenticateModel)->addAuthenticate($request);
            // (new UsersGameModel)->addUsersGame($request);
            // (new UserPreferencesModel())->addPreferencesUsers($request);
            // (new OAuthModel)->addOauth($request);

            DB::commit();

            //Create New Token
            $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->fromUser($user);
            // Return New Token
            return $this->createNewToken($token, $user);
        } catch (\Throwable $e) {
            DB::rollBack();
            //return error message gagal
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.social.register.failed'),
                'error'   => $e,
                'messageError'  => $e->getMessage()
            ], $this->HTTP_BAD);
        }
    }
}
