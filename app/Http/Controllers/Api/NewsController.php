<?php

namespace App\Http\Controllers\Api;

use App\Model\NewsLike;
use App\Model\NewsModel;
use App\Model\NewsCommentModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidatePostComment;
use App\Serializer\NewsSerialize;
use App\Transformer\NewsTransform;
use App\Transformer\CommentNewsTransform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * @group  News
 *
 * APIs for managing news
 */
class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt', ['only' => ['postComment']]);
        $this->middleware(\App\Http\Middleware\RefreshTokenManual::class, ['only' => ['getDetail']]);
    }

    /**
     * Get All News
     *
     * @queryParam  limit The limit show data per page. Example: 1
     * @queryParam  search The search keyword news. Default: 6. Example: kode
     *
     * @response {
            "data": [
                {
                    "id_news_game": "5eeb19dcc3d921",
                    "judul": "Divonis Langgar Kode Etik, Firli: S ...",
                    "penulis": {
                        "name": "Mhd Ramadhan Arvin",
                        "image": "http://localhost/Github/Cmfg0/private/user.png"
                    },
                    "label": {
                        "id_kategori": 6,
                        "nama_kategori": "Update",
                        "color": 1,
                        "gambar_kategori": "5e60e5450e618.png"
                    },
                    "gambar": "http://localhost/Github/Cmfg0/news/thumbnail/s2qERmyj32DWGIzVbLoC0JzNBw",
                    "url": "http://localhost:8000/api/news/aawd",
                    "tag": [
                        "a"
                    ],
                    "views": 13326,
                    "likes": 0,
                    "komentar": 1233,
                    "created_at": "2020-06-19 14:38:04"
                }
            ],
            "meta": {
                "pagination": {
                    "total": 2,
                    "count": 1,
                    "per_page": 1,
                    "current_page": 1,
                    "total_pages": 2,
                    "links": {
                        "next": "http://localhost:8000/api/news?limit=1&search=kode&page=2"
                    }
                }
            }
        }
     */
    public function getAll(Request $request)
    {
        $fractal = new Manager();
        $fractal->setSerializer(new NewsSerialize());
        $limit = request()->query('limit') ? request()->query('limit') : 6;

        $paginator = NewsModel::when(request()->query('search'), function ($query) use ($request) {
            $query->where('judul', 'LIKE', "%{$request->search}%");
        })->latest()->paginate($limit);

        $news = $paginator->appends(request()->query())->getCollection();
        $resource = new Collection($news,(new NewsTransform));
        $data = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $fractal->createData($data)->toArray();
    }


    /**
     * Get Simple News
     *
     * @queryParam  limit The limit show data per page. Default: 4. Example: 2
     *
     * @response {
            "data": [
                {
                    "id_news_game": "5eeb19dcc32d921",
                    "judul": "Respons Keren Mourinho Saat Ada yang Minta Foto de ...",
                    "label": {
                        "id_kategori": 6,
                        "nama_kategori": "Update",
                        "color": 1,
                        "gambar_kategori": "5e60e5450e618.png"
                    },
                    "gambar": "http://localhost/Github/Cmfg0/news/thumbnail/s2qERmyj32DWGIzVbLoC0JzNBw",
                    "url": "http://localhost:8000/api/news/ab",
                    "created_at": "2020-06-20 14:38:04",
                    "is_new": 1
                },
                {
                    "id_news_game": "5eeb19dcc3d921",
                    "judul": "Divonis Langgar Kode Etik, Firli: Saya Mohon Maaf ...",
                    "label": {
                        "id_kategori": 6,
                        "nama_kategori": "Update",
                        "color": 1,
                        "gambar_kategori": "5e60e5450e618.png"
                    },
                    "gambar": "http://localhost/Github/Cmfg0/news/thumbnail/s2qERmyj32DWGIzVbLoC0JzNBw",
                    "url": "http://localhost:8000/api/news/aawd",
                    "created_at": "2020-06-19 14:38:04",
                    "is_new": 0
                }
            ]
        }
     *
     */
    public function getSimple()
    {
        $limit = request()->query('limit') ? request()->query('limit') : 4;
        $news = NewsModel::latest()->limit($limit)->get(['id_news_game', 'judul', 'label', 'gambar', 'url', 'created_at']);

        return response()->json([
            'data'=>NewsTransform::simple($news)
        ]);
    }


    /**
     * Get Detail News
     *
     * @urlParam  url Url news to see detail. Example: tips-trik
     *
     * @response 404 {
            "message": "Data Not Found"
        }
     *
     */
    public function getDetail($url)
    {
        $news = NewsModel::where('url', $url)->first();

        if ($news == null) {
            return response()->json(['message' => 'Data Not Found'], 404);
        }

        $related = NewsModel::where('url', '!=', $url)->limit(2)->get();
        $news->increment('views');

        return response()->json([
            'data'=>NewsTransform::detail($news, $related)
        ]);
    }


    /**
     * Get Latest News
     *
     */
    public function latest(NewsModel $news)
    {
        $latest = $news::latest()->limit(5)->exclude(['isi', 'views', 'komentar'])->get();

        return response()->json([
            'data'=>NewsTransform::sanitizeArr($latest, '')
        ]);
    }


    /**
     * Get Most Comment News
     */
    public function most_comment(NewsModel $news)
    {
        $most_comment = $news::orderBy('komentar', 'desc')->limit(5)->exclude(['isi', 'views', 'komentar'])->get();

        return response()->json([
            'data'=>NewsTransform::sanitizeArr($most_comment, '')
        ]);
    }


    /**
     * Get Popular News
     */
    public function popular(NewsModel $news)
    {
        $most_comment = $news::orderBy('views', 'desc')->limit(5)->exclude(['isi', 'views', 'komentar'])->get();

        return response()->json([
            'data'=>NewsTransform::sanitizeArr($most_comment, '')
        ]);
    }


    /**
     * Get Comment News
     *
     * @urlParam news_id required ID in news game. Example: 5eeb19dcc32d921
     *
     * @response 404 {
            "status": false,
            "message": "Berita tidak ditemukan"
        }
     */
    public function getComment($news_id)
    {
        $news = $this->getNews($news_id);
        if (!is_null($news->original)) {
            return $news;
        }

        $fractal = new Manager();
        $paginator = $news->comment()->paginate(6);
        $news = $paginator->getCollection();
        $resource = new Collection($news,(new CommentNewsTransform));
        $data = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $fractal->createData($data)->toArray();
    }


    /**
     * Post Comment and Reply Comment
     *
     * @authenticated
     *
     * @urlParam news_id required ID in news game. Example: 5eeb19dcc32d921
     * @bodyParam  comment string required Comment want you post
     * @bodyParam  comment_id string ID Comment want you replied
     *
     *
     * @response 404 {
            "status": false,
            "message": "Berita tidak ditemukan"
        }
     * @response {
            "status": true,
            "message": "Berhasil mengirimkan komentar"
        }
     */
    public function postComment(ValidatePostComment $request, $news_id)
    {
        $news = $this->getNews($news_id);
        if (!is_null($news->original)) {
            return $news;
        }
        $news->increment('komentar');

        if (!is_null($request->comment_id)) {
            $comment = NewsCommentModel::find($request->comment_id);

            if (is_null($comment)) {
                return response()->json([
                    'message'   => Lang::get('message.comment.not')
                ], 404);
            }
        }

        try {

            $NewsCommentModel = new NewsCommentModel;
            $NewsCommentModel->comment_id = uniqid();
            $NewsCommentModel->reply_comment_id = $request->comment_id;
            $NewsCommentModel->news_id = $news->id_news_game;
            $NewsCommentModel->users_id = Auth::user()->user_id;
            $NewsCommentModel->comment = $request->comment;
            $NewsCommentModel->date = date('Y-m-d H:i:s');
            $NewsCommentModel->save();

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.comment.add.success')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.comment.add.failed'),
                'error'     => $e
            ], 409);
        }
    }

    /**
     * Like News
     *
     * @authenticated
     *
     * @urlParam news_id required ID in news game. Example: 5eeb19dcc32d921
     *
     *
     * @response 404 {
            "status": false,
            "message": "Berita tidak ditemukan"
        }
     * @response 400 {
            "status": false,
            "message": "Kamu sudah menyukai berita ini"
        }
     * @response {
            "status": true,
            "data": 1
        }
     */
    public function likePost ($news_id)
    {
        $News = $this->getNews($news_id);
        if (!is_null($News->original)) {
            return $News;
        }

        $PostLike = new NewsLike;
        if ($PostLike->where('news_id', $news_id)
                    ->where('users_id', Auth::user()->user_id)
                    ->count()
                    == 1
        ) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.news.like.has')
            ], $this->HTTP_BAD );
        }
        $PostLike->id = uniqid();
        $PostLike->news_id = $news_id;
        $PostLike->users_id = Auth::user()->user_id;
        $PostLike->save();

        return response()->json([
            'status'    => true,
            'data'      => $News->like()
        ]);
    }

    /**
     * UnLike News
     *
     * @authenticated
     *
     * @urlParam news_id required ID in news game. Example: 5eeb19dcc32d921
     *
     *
     * @response 404 {
            "status": false,
            "message": "Berita tidak ditemukan"
        }
     * @response 400 {
            "status": false,
            "message": "Kamu belum menyukai berita ini"
        }
     * @response {
            "status": true,
            "data": 0
        }
     */
    public function unLikePost($news_id)
    {
        $News = $this->getNews($news_id);
        if (!is_null($News->original)) {
            return $News;
        }

        $PostLike = NewsLike::where('news_id', $news_id)
                            ->where('users_id', Auth::user()->user_id);

        if ($PostLike->count() < 1 ) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.news.like.not')
            ], $this->HTTP_BAD);
        }

        $PostLike->delete();

        return response()->json([
            'status'    => true,
            'data'      => $News->like()
        ]);
    }






    public function getNews($news_id)
    {
        $News = NewsModel::find($news_id);

        if (is_null($News)) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.news.not')
            ], 404);
        }

        return $News;
    }
}
