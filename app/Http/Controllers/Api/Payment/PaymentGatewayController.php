<?php

namespace App\Http\Controllers\Api\Payment;

use App\Helpers\Ticket\TicketPaymentValidator;
use App\Http\Controllers\Api\UserNotificationController;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Requests\GetInvoiceRequest;
use App\Model\TeamModel;
use App\Model\TIcketPurchaseHistoryModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Xendit\Invoice;
use Xendit\Xendit;
/**
 * @group Ticket
 * @authenticated
 */

class PaymentGatewayController extends Controller
{

    public function __construct()
    {
        //Initialize api key
        Xendit::setApiKey(config('app.xendit_auth_token'));
    }

   /**
     * Pay ticket (create invoice)
     *
     * @authenticated
     * This Endpoint lets you create invoice for ticket payment
     * <aside class="notice">Dont forget to add XENDIT_AUTH_TOKEN= and XENDIT_CALLBACK_TOKEN= token key on your env file before you use this service</aside>
     * @bodyParam ticket_id integer required the id of ticket
     * @bodyParam quantity integer required the quantity of ticket that want to buy
     * @bodyParam game_id string required id game of ticket
     *
     *@response status=200 scenario="success" {
        "status": true,
        "invoice_url": "https://invoice.xendit.co/web/invoices/595b6248c763ac05592e3eb4"
     }
     * @response status=422 scenario="Unprocessable Entity" {
        "message": "Data yang diberikan tidak valid.",
        "errors": {
            "ticket_id": [
                "Ticket id wajib diisi."
            ],
            "quantity": [
                "Quantity wajib diisi."
            ],
            "game_id": [
                "ID Permainan wajib diisi."
            ]
        }
     }
     * @response status=400 scenario="Bad Request" {
        "status": false,
        "message": "error messages"
     }
     *@response status=500 scenario="Internal Server Error!" {
        "status": false,
        "message": "error messages"
     }
     * */
    public function createInvoice(CreateInvoiceRequest $req){

        DB::beginTransaction();
        try{
            //get user data
            $user  = $req->user();

            //get ticket data
            $ticket = (new TicketPaymentValidator)->validateTicketPayment($user, $req);
            //payment amount
            $amount = ($ticket->market_value * $req->quantity);
            if($amount < 10000) throw new Exception('Minimum transaction amount is 10000',400);

            //set invoice expired time
            $ticketExpDate = Carbon::parse($ticket->expired);
            $defaultExp = Carbon::now()->addHours(24);
            $now = Carbon::now();
            $expiredInv =  ($ticketExpDate > $defaultExp) ? $defaultExp->diffInSeconds($now) : $ticketExpDate->diffInSeconds($now);

            //xendit playload initiation
            $payload = [
                'external_id' => '#'.strtotime(Carbon::now()),
                'invoice_duration' => $expiredInv,
                'payer_email' => $user->email,
                'description' =>  Lang::get('ticket.xendit_payment', ['ticket_name' => $ticket->name, 'quantity' => $req->quantity]),
                'amount' => $amount,
                'customer' => ['email' => $user->email]
            ];

            //set additional payload incase using xenplatform account
            if(config('app.xendit_account_type') == 'xenplatform'){
                $payload['for-user-id'] = config('app.xendit_xenplatform_user_id');
            }
            //create invoice request to payment gateway (xendit)
            $createInvoice = Invoice::create($payload);

            //insert invoice to our system
            $purchaseInv = new TicketPurchaseHistoryModel;
                $purchaseInv->order_id = $createInvoice['external_id'];
                $purchaseInv->xendit_invoice_id = $createInvoice['id'];
                $purchaseInv->user_id = $user->user_id;
                $purchaseInv->ticket_id = $ticket->id;
                $purchaseInv->quantity = $req->quantity;
                $purchaseInv->amount = $createInvoice['amount'];
                $purchaseInv->provider = $createInvoice['merchant_name'].'-Xendit';
                $purchaseInv->status = 0;
                $purchaseInv->expired_at = Carbon::parse($createInvoice['expiry_date'])->setTimezone('Asia/Jakarta');

            //If this user pay for ticket team then save user's current team id
            if($ticket->mode == 2 OR $ticket->mode == 3){
                $getTeam = TeamModel::where('game_id', $req->game_id)
                                    ->where('leader_id', $user->user_id)
                                    ->firstOrFail();
                $purchaseInv->team_id = $getTeam->team_id;
            }
            $purchaseInv->save();


            //send notification to the user
            $notifData = (object) [
                'users_id' => $user->user_id,
                'ticket_name' => $ticket->name,
                'amount' => $amount,
                'invoice_id' => $createInvoice['external_id'],
                'link' => 'myticket/payment'
            ];
            $notif = new UserNotificationController('ticket_payment_xendit', $notifData);

            //commit all process
            DB::commit();
            return Response()->json([
                'status' => true,
                'invoice_url' => $createInvoice['invoice_url']
            ], 201);

        }catch(\Exception $e){
            DB::rollBack();
            $statusCode = $e->getCode() == 0 ? 500 : $e->getCode();
            $message = $e->getMessage();

            //save error log
            Log::debug($message);

            return Response()->json([
                'status' => false,
                'message' => $message
            ], $statusCode);
        }

    }

   /**
     * Recall pending invoice (get invoice)
     *
     * @authenticated
     * This Endpoint lets you recall your previous pending invoice for ticket payment (only if the status not expired)
     * <aside class="notice">Dont forget to add XENDIT_AUTH_TOKEN= and XENDIT_CALLBACK_TOKEN= token key on your env file before you use this service</aside>
     * @bodyParam invoice_id integer required the id of pending invoice
     *
     * @response status=200 scenario="success" {
        "status": true,
        "invoice_url": "https://invoice.xendit.co/web/invoices/595b6248c763ac05592e3eb4"
     }
     * @response status=422 scenario="Unprocessable Entity" {
        "message": "Data yang diberikan tidak valid.",
        "errors": {
            "invoice_id": [
                "Ticket id wajib diisi."
            ]
        }
     }
     * @response status=404 scenario="Invoice Not Found" {
        "status": false,
        "message": "error messages"
     }
     *@response status=500 scenario="Internal Server Error!" {
        "status": false,
        "message": "error messages"
     }
     *
     * */
    public function getInvoice(GetInvoiceRequest $req){
        try{
            //get user data
            $user  = $req->user();

            //validate if this inv exists in our db
            $getInv = TIcketPurchaseHistoryModel::select('xendit_invoice_id')->where('order_id', $req->invoice_id)
                                                    ->where('user_id', $user->user_id)
                                                    ->where('status', 0)
                                                    ->whereDate('expired_at','>=', Carbon::now()->toDateString())
                                                    ->firstOr(function(){
                                                        throw new Exception('Invoice data not found!', 404);
                                                    });

            //get invoice from xendit
            if(config('app.xendit_account_type') == 'xenplatform'){
                $getXenditInv = Http::withBasicAuth(Xendit::$apiKey, '')
                                    ->withHeaders([
                                        'for-user-id' => config('app.xendit_xenplatform_user_id')
                                    ])
                                    ->get(Xendit::$apiBase.'/v2/invoices/'.$getInv->xendit_invoice_id)->throw();
            }else{
                $getXenditInv = Invoice::retrieve($getInv->xendit_invoice_id);
            }


            return Response()->json([
                'status' => true,
                'invoice_url' => $getXenditInv['invoice_url']
            ], 200);
        }catch(\Exception $e){
            $statusCode = $e->getCode() == 0 ? 500 : $e->getCode();
            $message = $e->getMessage();

            //save error log
            Log::debug($message);

            return Response()->json([
                'status' => false,
                'message' => $message
            ], $statusCode);
        }
    }
}
