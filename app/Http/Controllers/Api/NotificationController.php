<?php

namespace App\Http\Controllers\Api;

use League\Fractal\Manager;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessFirebaseCounter;
use App\Model\UserNotificationModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Resource\Collection;
use App\Transformer\NotificationTransform;


/**
 * @group Notification
 *
 *
 */
class NotificationController extends Controller
{

	/**
	 * Get UnRead Notification
	 * @authenticated
	 * @response {
		    "data": [
		        {
		            "id_notification": "5f69a78ee8da4",
		            "message": "Pemimpin tim <b>Tester aja kali 4123</b> sekarang diberikan kepada anda.",
		            "is_read": 0,
		            "date": "2020-09-29 10:13:40"
		        },
		        {
		            "id_notification": "5f69ae99b4421",
		            "message": "Pemimpin tim <b>Tester aja kali 4123</b> sekarang diberikan kepada anda.",
		            "is_read": 0,
		            "date": "2020-09-29 10:13:41"
		        }
		    ]
		}
	 */
	public function getAll()
	{
		$notification = UserNotificationModel::where('id_user', Auth::user()->user_id)
                                                ->where('is_read', 0)
                                                ->latest()
												->get();
        $response   = new Collection($notification, new NotificationTransform);

        return (new Manager)->createData($response)->toArray();
    }

    /**
     * Get Read Notification
     * @authenticated
     * @response {
		    "data": [
		        {
		            "id_notification": "5f69a78ee8da4",
		            "message": "Pemimpin tim <b>Tester aja kali 4123</b> sekarang diberikan kepada anda.",
		            "is_read": 0,
		            "date": "2020-09-29 10:13:40"
		        },
		        {
		            "id_notification": "5f69ae99b4421",
		            "message": "Pemimpin tim <b>Tester aja kali 4123</b> sekarang diberikan kepada anda.",
		            "is_read": 0,
		            "date": "2020-09-29 10:13:41"
		        }
		    ]
		}
     */
    public function getRead()
    {
        $notification = UserNotificationModel::where('id_user', Auth::user()->user_id)
                                                ->where('is_read', 1)
                                                ->latest()
                                                ->get();
        $response   = new Collection($notification, new NotificationTransform);

        return (new Manager)->createData($response)->toArray();
    }


	/**
	 * Read All Notification
	 * @authenticated
	 * @response {
		    "status": true,
		    "message": "Berhasil"
		}
	 */
 	public function markAsRead()
 	{
 		$notification = UserNotificationModel::where('id_user',	Auth::user()->user_id)->get();

 		foreach($notification as $notif) {
 			$notif->is_read = 1;
 			$notif->save();
        }

        ProcessFirebaseCounter::dispatch(auth()->user()->user_id);

 		return response()->json([
 			'status'	=> true,
 			'message'	=> Lang::get('message.notification.markAsRead')
 		]);
 	}


 	/**
	 * Read Specific Notification
	 * @authenticated
	 * @urlParam id_notification required ID of the notification. Example: 5f69a78ee8da4
	 * @response {
		    "status": true,
		    "message": "Berhasil"
		}
	 *
	 */
 	public function readNotif($id_notification)
 	{
         //set to read notif
 		$notification = UserNotificationModel::withTrashed()->findOrFail($id_notification);
 		$notification->is_read = 1;
        $notification->save();

        //send to queue firebase
        ProcessFirebaseCounter::dispatch(auth()->user()->user_id);

 		return response()->json([
 			'status'	=> true,
 			'message'	=> Lang::get('message.notification.markAsRead')
 		]);
    }

    /**
     * Delete Notification
     * @authenticated
     * @urlParam id_notification required ID of the notification. Example: 5f69a78ee8da4
     * @response {
		    "status": true,
		    "message": "Berhasil"
		}
     *
     */
    public function deleteNotif(UserNotificationModel $id_notification)
    {
        try {

            DB::beginTransaction();
            $id_notification->delete();
            ProcessFirebaseCounter::dispatch(auth()->user()->user_id);
            DB::commit();


            return response()->json([
                'status'=>true
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message'   => 'Something Error',
                'error'     => $e->getMessage()
            ], 409);
        }
    }
}
