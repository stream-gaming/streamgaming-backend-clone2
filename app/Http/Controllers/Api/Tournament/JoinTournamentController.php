<?php

namespace App\Http\Controllers\Api\Tournament;


use App\Helpers\Validation\CheckBalanceCashOrPoint;
use App\Helpers\Validation\checkBalanceTicketTeam;
use App\Helpers\Validation\CheckBalanceTicketTeamNew;
use App\Helpers\Validation\checkBalanceTicketUser;
use App\Helpers\Validation\CheckBalanceTicketUserNew;
use App\Helpers\Validation\CheckBanned;
use App\Helpers\Validation\CheckGame;
use App\Helpers\Validation\CheckTeam;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Helpers\Validation\CheckTournamentSystem;
use App\Helpers\Validation\checkTournamentType;
use App\Helpers\Validation\FreeRegist;
use App\Helpers\Validation\hasJoinedOtherTournament;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Validation\hasJoinedTournament;
use App\Helpers\Validation\isSlotFull;
use App\Helpers\Validation\RegistMemberByCashOrPoint;
use App\Helpers\Validation\RegistSoloTournamentFree;
use App\Helpers\Validation\RegistSoloTournamentTicket;
use App\Helpers\Validation\RegistTeamByCashOrPoint;
use App\Helpers\Validation\RegistTeamByTicket;
use App\Helpers\Validation\TournamentFilledSlotCounter;
use App\Helpers\Validation\tournamentStatus;
use App\Http\Requests\ValidateJoinTournament;
use App\Jobs\CacheTriggerPortalJob;
use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TicketModel;
use App\Model\TournamentModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;


/**
 * @group Tournament
 *
 * API for get tournament information
 */
class JoinTournamentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['checkpin','guard.request']);
    }

    /**
     * Join Tournament
     *
     * this endpoint is used to join to any tournament Bracket or Leaderboard
     * @authenticated
     * @bodyParam id_tournament string required The id of tournament. Example: 12345
     * @bodyParam payment_method string cash or point choosen by user (only required for paid tournament). No-example
     * @bodyParam treat string self or all choosen by user (only required for paid tournament). No-example
     * @bodyParam pin string (only required for paid tournament)
     * @response status="200" scenario="success" {
            "status": true,
            "message": "Pendaftaran tournament berhasil!"
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Pendaftaran tournament gagal"
        }
     */
    public function join(ValidateJoinTournament $request){



        $user = Auth::user();
        $url = $request->url;
        $idTournament = $request->id_tournament;
        $choosenPaymentMethod = $request->payment_method;
        $treat = $request->treat;
        $errorMsg = [];



        try {



        //Check tournament system 'Leaderboard' Or 'Bracket'
        $checkTournamentSystem = (new CheckTournamentSystem)->check($idTournament);

        if($checkTournamentSystem == false){
            throw new Exception('Terjadi kesalahan, segarkan halaman!1');
        }else{
            //getting tournament data
            switch($checkTournamentSystem['tournament_system']){
                case 'Leaderboard':
                    $tournament = LeaderboardModel::where('id_leaderboard', $idTournament)->first();
                    if(!$tournament){
                        //incase the the tournament not found
                        throw new Exception('Terjadi kesalahan, segarkan halaman!2');
                    }
                    $tournament_date = $tournament->tournament_date;
                    $game_id = $tournament->game_competition;
                    $filledSlot = $tournament->filled_slot;
                break;

                case 'Bracket':
                    $tournament = TournamentModel::where('id_tournament', $idTournament)->first();
                    if(!$tournament){
                         //incase the the tournament not found
                        throw new Exception('Terjadi kesalahan, segarkan halaman!3');
                    }
                    $tournament_date = $tournament->date;
                    $game_id = $tournament->kompetisi;
                    $filledSlot = $tournament->filled_slot;
                break;
            }
        }


        //check if this tournament is still open
        $tournamentStatus = (new tournamentStatus)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($tournamentStatus['tournament_status'] != 'registration_open' ){

            throw new Exception('Terjadi kesalahan, segarkan halaman!4');
        }

        //Check tournament payment method
        switch($checkTournamentSystem['tournament_system']){
            case 'Leaderboard':
                $checkTournamentMethod = (new CheckTournamentPaymentMethod)->LeaderboardPaymentMethod($idTournament);

                if(!$checkTournamentMethod){
                    throw new Exception('Terjadi kesalahan, segarkan halaman!5');
                }
            break;

            case 'Bracket':
                $checkTournamentMethod = (new CheckTournamentPaymentMethod)->BracketPaymentMethod($idTournament);

                if(!$checkTournamentMethod){
                    throw new Exception('Terjadi kesalahan, segarkan halaman!6');
                }
            break;
        }

        //validating the choosen payment method
        if($checkTournamentMethod['payment_method'] == 'pay'){
            if($choosenPaymentMethod != 'cash' AND $choosenPaymentMethod != 'point'){
                //incase the payment method is not set
                throw new Exception('Anda belum memilih metode pembayaran!');
            }
            //validating the treat
            if($treat != 'self' AND $treat != 'all'){
                //incase the payment method is not set
                throw new Exception('Terjadi kesalahan, segarkan halaman!8');
            }
        }


        //checking if the user has already joined on this tournament
        $hasJoinedThisTournament = (new hasJoinedTournament)->check($user->user_id, $idTournament);

        if($hasJoinedThisTournament == true){
            //incase the user has already joined on this tournament
            throw new Exception('Anda telah terdaftar pada tournament ini!');
        }


        //checking if the user in on other tournament
        $hasJoinedOtherTournament = (new hasJoinedOtherTournament)->check($user->user_id, $tournament_date);
        if($hasJoinedOtherTournament == true){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda sedang mengikuti tournament lain pada waktu yang sama!');
        }



        //checking if the slot is available
        $isSlotFull = (new isSlotFull)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($isSlotFull == true){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan slot sudah penuh!');
        }

        //checking if this user has game that correlate to the tournament
        $checkGame = (new CheckGame)->hasGame($game_id, $user->user_id);

        if($checkGame == false){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda belum memiliki game tersebut di daftar game Anda!');
        }


        //checking type of tournament (solo, duo,trio or squad)
        $tournamentType = (new checkTournamentType)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($tournamentType['status'] != true){
            throw new Exception('Terjadi kesalahan, segarkan halaman!10');
        }


        //Execute this block of code incase the tournament is squad
        if($tournamentType['type_tournament'] == 'Squad' OR $tournamentType['type_tournament'] == 'Special_Squad'){

            //checking if this user has game that correlate to the tournament
            $checkTeam = (new CheckTeam)->hasTeam($game_id, $user->user_id);

            if($checkTeam == false){
                throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda belum memiliki team untuk game tersebut!');
            }


            //checking if this user is leader or not
            $getTeam = TeamModel::where('game_id', $game_id)->where('leader_id', $user->user_id)->first(['team_id']);
            if($getTeam == null){
                throw new Exception('Hanya leader team yang dapat melakukan pendaftaran tournament!');
            }

            //checking if this user team is banned or not
            $isBanned = (new CheckBanned)->isBanned($getTeam->team_id);
            if($isBanned == true){
                throw new Exception('Tidak dapat melakukan pendaftaran tournament dikarenakan team Anda telah di ban!');
            }

            $isPlayerTeamBanned = (new CheckBanned)->isSoloBanned($game_id, $getTeam->team_id , null, false);
            if($isPlayerTeamBanned == true){
                throw new Exception('Tidak dapat melakukan pendaftaran tournament dikarenakan salah satu anggota tim Anda telah diban!');
            }


            //count total of team member
            $countPlayer = TeamPlayerModel::where('team_id', $getTeam->team_id)->first(['player_id']);
            if($countPlayer == null){
                throw new Exception('Terjadi kesalahan, segarkan halaman!9');
            }

            if($tournamentType['type_tournament'] == 'Squad' OR $tournamentType['type_tournament'] == 'Special_Squad'){
                $player_on_team = GameModel::where('id_game_list', $game_id)->first(['player_on_team']);
                $player = explode(',',$countPlayer->player_id);
                $no = count($player);
                if($no < $player_on_team->player_on_team){
                    throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan member tim Anda belum penuh');
                }
            }

            //checking what treat type used by this user
            if($treat == 'self'){
                $no = 1;
            }elseif($treat == 'all'){
                $player = explode(',',$countPlayer->player_id);
                $no = count($player);
            }

      }else if($tournamentType['type_tournament'] == 'Solo'){
        //check ban status for solo turnamen
        $isPlayerBanned = (new CheckBanned)->isSoloBanned($game_id, null, $user->user_id , null, false);
        if($isPlayerBanned == true){
            throw new Exception('Tidak dapat melakukan pendaftaran tournament dikarenakan akun Anda telah diban!');
        }
      }


    //Execute this block of code incase the tournament is squad
    if($tournamentType['type_tournament'] == 'Squad' OR $tournamentType['type_tournament'] == 'Special_Squad'){

        //check user's balance
        switch($checkTournamentMethod['payment_method']){

            case 'pay':
                $checkBalanceOrPoint = (new CheckBalanceCashOrPoint)->isSufficient($checkTournamentMethod['value'], $no, $choosenPaymentMethod);
                if(!$checkBalanceOrPoint){
                    throw new Exception('Stream cash atau point tidak mencukupi!');
                }

                $this->payTournament($request,$treat,$tournament, $getTeam,$checkTournamentSystem,$checkTournamentMethod,$choosenPaymentMethod);

                //Registration process

            break;

            case 'ticket':

                if($tournament['is_new_ticket_feature'] == 1){

                    $ticket = $this->newTicketFeature($checkTournamentMethod['ticket_id'], 'team', $getTeam->team_id);

                    if($ticket['status'] == false){
                        throw new Exception($ticket['message']);
                    }

                    $ticketv2 = $this->newTicketFeatureV2($checkTournamentMethod['ticket_id'], 'team', $getTeam->team_id, $checkTournamentMethod['value']);

                    if($ticketv2['status'] == false){
                        throw new Exception($ticketv2['message']);
                    }


                    $regist = new RegistTeamByTicket();

                    //Registration process
                    $reg =  $regist->regist($tournament , $getTeam->team_id, $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value'],$checkTournamentSystem['tournament_system'],1);
                    if($reg == false){
                        throw new Exception('Gagal melakukan pendaftaran tournament');
                    }


                }else{

                    $ticketType = TicketModel::where('id_ticket', $checkTournamentMethod['ticket_id'])->first(['functions']);

                    if($ticketType == null ){
                        throw new Exception('Terjadi kesalahan, segarkan halaman!10');
                    }

                    if($ticketType->functions == 1){
                        $checkBalanceTicket = (new checkBalanceTicketUser)->check($idTournament, $checkTournamentSystem['tournament_system'], $user->user_id);

                        if($checkBalanceTicket == null OR $checkBalanceTicket == false){

                            throw new Exception('Ticket tidak mencukupi');
                        }

                        if($checkBalanceTicket['status'] == 'false'){
                            $errorMsg = $checkBalanceTicket['message'];
                            throw new Exception('ticket_user');
                        }



                        $regist = new RegistTeamByTicket();

                        //Registration process
                        $reg =  $regist->regist($tournament , $getTeam->team_id, $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value'],$checkTournamentSystem['tournament_system']);
                        if($reg == false){
                            throw new Exception('Gagal melakukan pendaftaran tournament');
                        }

                    }elseif($ticketType->functions == 3){
                        $checkBalanceTicket = (new checkBalanceTicketTeam)->check($idTournament, $checkTournamentSystem['tournament_system'], $getTeam->team_id);

                        if($checkBalanceTicket == null OR $checkBalanceTicket == false){

                            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan ticket tim Anda tidak mencukupi!');
                        }

                        $regist = new RegistTeamByTicket();

                        //Registration process
                        $reg =  $regist->regist($tournament , $getTeam->team_id, $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value'],$checkTournamentSystem['tournament_system']);
                        if($reg == false){
                            throw new Exception('Gagal melakukan pendaftaran tournament');
                        }
                    }else{
                        $checkBalanceTicket = false;
                    }

            }
            break;

            case 'free':
                $regist = (new FreeRegist)->regist($tournament, $getTeam->team_id, $checkTournamentSystem['tournament_system']);
                if($regist == false){
                    throw new Exception('Gagal melakukan pendaftaran tournament');
                }
            break;
        }

    }else if($tournamentType['type_tournament'] == 'Solo'){
        //incase the tournament is solo
        switch($checkTournamentMethod['payment_method']){
            case 'free':
                $regist = (new RegistSoloTournamentFree)->regist($tournament, $user->user_id, $checkTournamentSystem['tournament_system']);

                if($regist == false){
                    throw new Exception('Gagal melakukan pendaftaran tournament');
                }
            break;

            case 'pay':
                throw new Exception('Gagal melakukan pendaftaran tournament (solo 1)');
            break;

            case 'ticket':

                $ticket = $this->newTicketFeature($checkTournamentMethod['ticket_id'], 'user', $user->user_id);
                if($ticket['status'] == false){
                    throw new Exception($ticket['message']);
                }

                $ticketv2 = $this->newTicketFeatureV2($checkTournamentMethod['ticket_id'], 'user', $user->user_id, $checkTournamentMethod['value']);

                if($ticketv2['status'] == false){
                    throw new Exception($ticketv2['message']);
                }

                $regist = (new RegistSoloTournamentTicket)->regist($tournament, $user->user_id, $checkTournamentSystem['tournament_system'], $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value']);

                if($regist == false){
                    throw new Exception('Gagal melakukan pendaftaran tournament');
                }
            break;

        }
    }


        //Updating filled slot on the tournament
        $tournamentFilledSlot = (new TournamentFilledSlotCounter)->sum($idTournament,$filledSlot,$checkTournamentSystem['tournament_system']);



        $request->session()->flush();

        //cache leaderboard trigger
        if($checkTournamentSystem['tournament_system'] == 'Leaderboard'){
            $key = [
                'leaderboard.'.$idTournament.'.overview',
                'leaderboard.'.$idTournament.'.participant',
                'leaderboard.'.$idTournament.'.leaderboard',
                'leaderboard.'.$idTournament.'.leaderboard.1'
            ];
            CacheTriggerPortalJob::dispatch($key,'multi');
            CacheTriggerPortalJob::dispatch('leaderboard.score.'.$idTournament,'pattern');
        }else if($checkTournamentSystem['tournament_system'] == 'Bracket'){
            $url = $tournament->url;
            CacheTriggerPortalJob::dispatch('bracket.participant.'.$url,'single');
            CacheTriggerPortalJob::dispatch('bracket.overview.'.$url,'single');
        }
        //end of cache leaderboard trigger

        //tournament list trigger
            CacheTriggerPortalJob::dispatch('tournament:list:','pattern');
        //end of tournament list trigger

        //my tournament trigger
        if($tournamentType['type_tournament'] == 'Squad' OR $tournamentType['type_tournament'] == 'Special_Squad'){
            $key = [];
            foreach($player as $p){
                array_push($key, 'mytournament.'.$p);
            }
            CacheTriggerPortalJob::dispatch($key,'multi');
        }else if($tournamentType['type_tournament'] == 'Solo'){
            CacheTriggerPortalJob::dispatch('mytournament.'.$user->user_id,'single');
        }
        //end of my tournament trigger

        return response()->json([
            'status'    => true,
            'message'   => 'Pendaftaran tournament berhasil!'
        ]);


        }catch(\Exception $e){

            if($e->getMessage() == 'ticket_user'){

                return response()->json([
                    'status'    => false,
                    'message'   => $errorMsg
                ], $this->HTTP_NOT_FOUND);

            }else{

                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ], $this->HTTP_NOT_FOUND);

            }

        }




    }

    public function payTournament(Request $request,$treat,$tournament, $getTeam,$checkTournamentSystem,$checkTournamentMethod,$choosenPaymentMethod)
    {
        if($treat == 'self'){
            $pay = (new RegistTeamByCashOrPoint)->register($tournament,$getTeam->team_id,$checkTournamentMethod['value'],$checkTournamentSystem['tournament_system'],$choosenPaymentMethod);

            if(!$pay){
                throw new Exception('Gagal melakukan pendaftaran tournament');
            }
        }
        else{

            $pay = (new RegistMemberByCashOrPoint)->register($tournament,$getTeam->team_id,$checkTournamentSystem['tournament_system'],$choosenPaymentMethod);

            if(!$pay){
                throw new Exception('Gagal melakukan pendaftaran tournament');
            }
        }
    }


    private function newTicketFeature($idTicket, $userOrTeam, $idUserOrTeam){
        try{
            $now = Carbon::now();
            $getTicketInfo = AllTicketModel::where('id', $idTicket)->firstOrFail();

            if($getTicketInfo->expired < $now){

                return [
                        'status' => false,
                        'message' => 'Tiket sudah kadaluarsa'
                        ];
            }

            switch($userOrTeam){

                case 'user':
                    $check = (new CheckBalanceTicketUserNew)->check($idUserOrTeam, $idTicket);
                    if($check == false){
                        return [
                            'status' => false,
                            'message' => 'Terjadi kesalahan, segarkan halaman!'
                            ];
                    }
                    return $check;
                break;

                case 'team':

                    $check = (new CheckBalanceTicketTeamNew)->check($idUserOrTeam, $idTicket);

                    if($check == false){
                        return [
                            'status' => false,
                            'message' => 'Terjadi kesalahan, segarkan halaman!'
                            ];
                    }
                    return $check;
                break;

            }
        }catch(\Exception $e){

            return [
                    'status' => false,
                    'message' => 'Terjadi kesalahan, segarkan halaman!'
                    ];
        }

    }
    //- Fitur Join Tournament yang bisa menggunakan lebih dari 1 tiket
    private function newTicketFeatureV2($idTicket, $userOrTeam, $idUserOrTeam, $amountOfTicket){
        try{
            $now = Carbon::now();
            $getTicketInfo = AllTicketModel::where('id', $idTicket)->firstOrFail();

            if($getTicketInfo->expired < $now){

                return [
                        'status' => false,
                        'message' => 'Tiket sudah kadaluarsa'
                        ];
            }

            switch($userOrTeam){

                case 'user':
                    $check = (new CheckBalanceTicketUserNew)->checkv2($idUserOrTeam, $idTicket, $amountOfTicket);
                    if($check == false){
                        return [
                            'status' => false,
                            'message' => 'Terjadi kesalahan, segarkan halaman!'
                            ];
                    }
                    return $check;
                break;

                case 'team':

                    $check = (new CheckBalanceTicketTeamNew)->checkV2($idUserOrTeam, $idTicket, $amountOfTicket);

                    if($check == false){
                        return [
                            'status' => false,
                            'message' => 'Terjadi kesalahan, segarkan halaman!'
                            ];
                    }
                    return $check;
                break;

            }
        }catch(\Exception $e){

            return [
                    'status' => false,
                    'message' => 'Terjadi kesalahan, segarkan halaman!'
                    ];
        }

    }
}
