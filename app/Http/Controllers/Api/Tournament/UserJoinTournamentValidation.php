<?php

namespace App\Http\Controllers\Api\Tournament;

use App\Helpers\FaqLinkHelper;
use App\Helpers\Ticket\UserTicketFactory;
use App\Helpers\Validation\checkBalanceTicketTeam;
use App\Helpers\Validation\CheckBalanceTicketTeamNew;
use App\Helpers\Validation\checkBalanceTicketUser;
use App\Helpers\Validation\CheckBalanceTicketUserNew;
use App\Helpers\Validation\CheckBanned;
use App\Helpers\Validation\CheckGame;
use App\Helpers\Validation\CheckTeam;
use App\Helpers\Validation\checkTournamentType;
use App\Http\Controllers\Controller;
use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\TicketModel;
use App\Model\TournamentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class UserJoinTournamentValidation extends Controller
{

    /**
     * Validate Join Tournament
     *
     * this endpoint is used to validate user before join to any tournament Bracket or Leaderboard
     * @authenticated
     * @bodyParam game_id string required The id of game. Example: 12345
     * @bodyParam tournament_id required The id of tournament. Example: caf80f7f10fa875f86e55f2888fba1690dc532cf
     * @bodyParam ticket_id id ticket if payment method is ticket
     * @bodyParam ticket_value value of ticket
     * @response status="200" scenario="success" {
            "status": true,
            "data": {
                "main_requirement": [
                    {
                        "status": false,
                        "text": "Terhubung dengan game MOBILE LEGEND"
                    },
                    {
                        "status": true,
                        "text": "Memiliki Team minimal 5 orang"
                    },
                    {
                        "status": false,
                        "text": "Pendaftaran dilakukan ketua tim"
                    }
                ],
                "team_identity": {
                    "team_name": "Brotherhood",
                    "leader_name": "CROOZ_Kombet17"
                }
            }
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Permainan tidak ditemukan"
        }
     */
    public function getValidation(Request $request)
    {
        $faqLinkHelper = (new FaqLinkHelper)->getFaqPath();
        $request->validate([
            'game_id'       => 'required',
            'tournament_id' => 'required',
            'ticket_id'     => 'nullable',
            'ticket_value'  => 'required_with:ticket_id'
        ]);
        $getTeam = false;

        $game = GameModel::find($request->game_id);

        if($game == null):

            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.not')
            ], $this->HTTP_NOT_FOUND);

        else:

            $hasGame    = (new CheckGame)->hasGame($request->game_id);
            $identyty   = (new IdentityGameModel())->usernameInGame(Auth::user()->user_id, $request->game_id);

            $tournamentType = (new checkTournamentType)->checkType($request->tournament_id);

            $requirement = [
                [
                    'status'    => $hasGame,
                    'text'      => 'Terhubung dengan game '.$game->name,
                    'helpdesk_link' => $hasGame == false ? $faqLinkHelper['game'] : ''
                ]
            ];

            $team = [
                'team_name'     => $identyty,
                'leader_name'   => $identyty
            ];

            if($tournamentType['game_option'] != 1){
                $getTeam    = $hasGame == false ? false : (new CheckTeam)->getTeamPlayer($request->game_id);
                $hasTeam    = false;
                $isLeader   = false;
                $teamId = null;
                $arrayPlayer = (empty($getTeam->player_id)) ? [] : explode(',' ,$getTeam->player_id);

                if($getTeam!= false){
                    if(collect($arrayPlayer)->count() >= $game->player_on_team){
                        $hasTeam= true;
                        $teamId = $getTeam->team_id;
                    }
                    else{
                        $hasTeam= false;
                    }
                    $isLeader   = (new CheckTeam)->isLeader($getTeam);
                }
                $team_requirement = [
                    [
                        'status'    => $hasTeam,
                        'text'      => 'Memiliki Team minimal '.$game->player_on_team.' orang',
                        'helpdesk_link' => $hasTeam == false ? $faqLinkHelper['team'] : ''
                    ],
                    [
                        'status'    => $isLeader,
                        'text'      => 'Pendaftaran dilakukan ketua tim',
                        'helpdesk_link' => $isLeader == false ? $faqLinkHelper['team_leader'] : ''
                    ]
                ];

                $requirement = array_merge($requirement,$team_requirement);
                $team = [
                    'team_name'     => $getTeam!=false ? $getTeam->team_name : null,
                    'leader_name'   => $getTeam!=false ? IdentityGameModel::where('users_id',$getTeam->leader_id)
                                                        ->where('game_id', $request->game_id)
                                                        ->first()->username_ingame : null
                ];
            }





            if($request->ticket_id != null && $tournamentType['format'] != null){

                $getTournament = $tournamentType['format'] == 'Bracket' ?  TournamentModel::where('id_tournament', $request->tournament_id)->first(['is_new_ticket_feature']) : LeaderboardModel::where('id_leaderboard', $request->tournament_id)->first(['is_new_ticket_feature']);

                if($getTournament->is_new_ticket_feature == 1) {
                    //join tournament validation for new ticket feature
                    try{

                        $idUserOrTeamTemp = $getTeam!=false? $getTeam->team_id : Auth::user()->user_id;
                        $userOrTeamTemp = $getTeam!=false ? 'team' : 'solo';

                        $getTicketInfo = $this->newTicketValidation($request->tournament_id,$request->ticket_id,$idUserOrTeamTemp,$userOrTeamTemp, $request->ticket_value);

                        array_push($requirement,$getTicketInfo);
                    }catch(\Exception $e){

                        return [
                            'status'    => false,
                            'text'      => 'Ups. there is a problem on ticket feature!'
                        ];
                    }




                }else{

                        $tickettype = TicketModel::find($request->ticket_id);

                        if($tickettype == null){
                            return response()->json([
                                'status'    => false,
                                'message'   => Lang::get('ticket tidak ada')
                            ], $this->HTTP_NOT_FOUND);
                        }
                        else{
                            switch ($tickettype->functions){
                                case 1:
                                    if($hasTeam!=false){
                                        $ticket = checkBalanceTicketUser::checkByticket($tickettype->id_ticket, $tickettype->names,$getTeam->team_id, $game->id_game_list ,$request->ticket_value);
                                        if(!empty($ticket)){
                                            $requirement = array_merge($requirement,$ticket);
                                        }
                                    }
                                    else{
                                        $array  = [
                                            'status' => false,
                                            'text'   => 'Semua Anggota Team memiliki ticket '.$tickettype->names.' yang cukup',
                                            'helpdesk_link' => $faqLinkHelper['ticket']
                                            ];
                                        array_push($requirement,$array);
                                    }
                                    break;
                                case 3:
                                    if($hasTeam!=false){
                                        $ticket = checkBalanceTicketTeam::checkByticket($tickettype->id_ticket, $getTeam->team_id, $request->ticket_value);
                                        $array  = [
                                            'status' => $ticket,
                                            'text'   => 'Team memiliki ticket '.$tickettype->names.' yang cukup',
                                            'helpdesk_link' => $ticket == false ? $faqLinkHelper['ticket'] : ''
                                            ];
                                        array_push($requirement,$array);
                                    }
                                    else{
                                        $array  = [
                                            'status' => false,
                                            'text'   => 'Team memiliki ticket '.$tickettype->names.' yang cukup',
                                            'helpdesk_link' => $faqLinkHelper['ticket']
                                            ];
                                        array_push($requirement,$array);
                                    }
                                    break;
                                default:
                                    return response()->json([
                                        'status'    => false,
                                        'message'   => Lang::get('ticket tidak ada')
                                    ], $this->HTTP_NOT_FOUND);
                                break;
                            }
                        }
                }
            }

            //for ban status
            if($tournamentType['game_option'] != 1){
                if($hasGame){
                    $isPlayerTeamBanned = (new CheckBanned)->isSoloBanned($request->game_id, $teamId , null, false);
                    $array  = [
                            'status' => true,
                            'text'   => 'Tidak ada anggota tim yang terbanned',
                            'helpdesk_link' => ''
                        ];
                    if($isPlayerTeamBanned){
                        $array['status'] = false;
                        $array['helpdesk_link'] = $faqLinkHelper['banned'];
                    }
                    array_push($requirement,$array);
                }
            }

            return response()->json([
                'status' => true,
                'data'   => [
                    'main_requirement' => $requirement,
                    'team_identity'    => $team
                ]
            ]);

        endif;
    }

    public function newTicketValidation($idTournament, $idTicket, $idUserOrTeam, $userOrTeam, $quantity = 1){
        $faqLinkHelper = (new FaqLinkHelper)->getFaqPath();
        try{
            $getTicket = AllTicketModel::find($idTicket);

            switch($userOrTeam){
                case 'solo':
                    $getTicketUser = (new CheckBalanceTicketUserNew)->checkv2($idUserOrTeam, $idTicket, $quantity);
                    if($getTicketUser['status'] == false){
                        return [
                            'status'    => false,
                            'text'      => 'Player memiliki '.$quantity.' tiket '.$getTicket->name,
                            'helpdesk_link' => $faqLinkHelper['ticket'].'?type=solo&search='.$getTicket->name
                        ];
                    }

                    return [
                        'status'    => true,
                        'text'      => 'Player memiliki '.$quantity.' tiket '.$getTicket->name,
                        'helpdesk_link' => ''
                    ];
                break;

                case 'team':
                    $getTicketTeam = (new CheckBalanceTicketTeamNew)->checkv2($idUserOrTeam, $idTicket, $quantity);
                    if($getTicketTeam['status'] == false){
                        return [
                            'status'    => false,
                            'text'      => 'Tim memiliki '.$quantity.' tiket '.$getTicket->name,
                            'helpdesk_link' => $faqLinkHelper['ticket'].'?type=tim&search='.$getTicket->name
                        ];
                    }

                    return [
                        'status'    => true,
                        'text'      => 'Tim memiliki '.$quantity.' tiket '.$getTicket->name,
                        'helpdesk_link' => ''
                    ];
                break;
            }

        }catch(\Exception $e){
            return [
                'status'    => false,
                'text'      => 'Ups. there is a problem on ticket feature!'
            ];

        }



    }
}
