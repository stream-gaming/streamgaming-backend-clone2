<?php

namespace App\Http\Controllers\Api\Tournament;

use App\Helpers\Validation\CheckAvailableGroup;
use App\Helpers\Validation\CheckBalanceCashOrPoint;
use App\Helpers\Validation\checkBalanceTicketTeam;
use App\Helpers\Validation\checkBalanceTicketUser;
use App\Helpers\Validation\CheckBanned;
use App\Helpers\Validation\CheckGame;
use App\Helpers\Validation\CheckTeam;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Helpers\Validation\CheckTournamentSystem;
use App\Helpers\Validation\checkTournamentType;
use App\Helpers\Validation\FreeRegist;
use App\Helpers\Validation\hasJoinedOtherTournament;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Validation\hasJoinedTournament;
use App\Helpers\Validation\isSlotFull;
use App\Helpers\Validation\RegistMemberByCashOrPoint;
use App\Helpers\Validation\RegistSoloTournamentFree;
use App\Helpers\Validation\RegistTeamByCashOrPoint;
use App\Helpers\Validation\RegistTeamByTicket;
use App\Helpers\Validation\TournamentFilledSlotCounter;
use App\Helpers\Validation\tournamentStatus;
use App\Http\Requests\ValidateJoinTournament;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TicketModel;
use App\Model\TournamentModel;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class JoinTournamentSoloController extends Controller
{

    public function __construct()
    {
        $this->middleware('checkpin');
    }

    /**
     * Join Tournament
     *
     * this endpoint is used to join to any tournament Bracket or Leaderboard
     * @authenticated
     * @bodyParam id_tournament string required The id of tournament. Example: 12345
     * @bodyParam payment_method string cash or point choosen by user (only required for paid tournament). No-example
     * @bodyParam treat string self or all choosen by user (only required for paid tournament). No-example
     * @bodyParam pin string (only required for paid tournament)
     * @response status="200" scenario="success" {
            "status": true,
            "message": "Pendaftaran tournament berhasil!"
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Pendaftaran tournament gagal"
        }
     */
    public function join(ValidateJoinTournament $request){



        $user = Auth::user();
        $url = $request->url;
        $idTournament = $request->id_tournament;
        $choosenPaymentMethod = $request->payment_method;
        $treat = $request->treat;
        $errorMsg = [];



        try {



        //Check tournament system 'Leaderboard' Or 'Bracket'
        $checkTournamentSystem = (new CheckTournamentSystem)->check($idTournament);

        if($checkTournamentSystem == false){
            throw new Exception('Terjadi kesalahan, segarkan halaman!1');
        }else{
            //getting tournament data
            switch($checkTournamentSystem['tournament_system']){
                case 'Leaderboard':
                    $tournament = LeaderboardModel::where('id_leaderboard', $idTournament)->first();
                    if(!$tournament){
                        //incase the the tournament not found
                        throw new Exception('Terjadi kesalahan, segarkan halaman!2');
                    }
                    $tournament_date = $tournament->tournament_date;
                    $game_id = $tournament->game_competition;
                    $filledSlot = $tournament->filled_slot;
                break;

                case 'Bracket':
                    $tournament = TournamentModel::where('id_tournament', $idTournament)->first();
                    if(!$tournament){
                         //incase the the tournament not found
                        throw new Exception('Terjadi kesalahan, segarkan halaman!3');
                    }
                    $tournament_date = $tournament->date;
                    $game_id = $tournament->kompetisi;
                    $filledSlot = $tournament->filled_slot;
                break;
            }
        }


        //check if this tournament is still open
        $tournamentStatus = (new tournamentStatus)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($tournamentStatus['tournament_status'] != 'registration_open' ){

            throw new Exception('Terjadi kesalahan, segarkan halaman!4');
        }

        //Check tournament payment method
        switch($checkTournamentSystem['tournament_system']){
            case 'Leaderboard':
                $checkTournamentMethod = (new CheckTournamentPaymentMethod)->LeaderboardPaymentMethod($idTournament);

                if(!$checkTournamentMethod){
                    throw new Exception('Terjadi kesalahan, segarkan halaman!5');
                }
            break;

            case 'Bracket':
                $checkTournamentMethod = (new CheckTournamentPaymentMethod)->BracketPaymentMethod($idTournament);

                if(!$checkTournamentMethod){
                    throw new Exception('Terjadi kesalahan, segarkan halaman!6');
                }
            break;
        }

        //validating the choosen payment method
        if($checkTournamentMethod['payment_method'] == 'pay'){
            if($choosenPaymentMethod != 'cash' AND $choosenPaymentMethod != 'point'){
                //incase the payment method is not set
                throw new Exception('Anda belum memilih metode pembayaran!');
            }
            //validating the treat
            if($treat != 'self' AND $treat != 'all'){
                //incase the payment method is not set
                throw new Exception('Terjadi kesalahan, segarkan halaman!8');
            }
        }


        //checking if the user has already joined on this tournament
        $hasJoinedThisTournament = (new hasJoinedTournament)->check($user->user_id, $idTournament);

        if($hasJoinedThisTournament == true){
            //incase the user has already joined on this tournament
            throw new Exception('Anda telah terdaftar pada tournament ini!');
        }


        //checking if the user in on other tournament
        $hasJoinedOtherTournament = (new hasJoinedOtherTournament)->check($user->user_id, $tournament_date);
        if($hasJoinedOtherTournament == true){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda sedang mengikuti tournament lain pada waktu yang sama!');
        }



        //checking if the slot is available
        $isSlotFull = (new isSlotFull)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($isSlotFull == true){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan slot sudah penuh!');
        }

        //checking if this user has game that correlate to the tournament
        $checkGame = (new CheckGame)->hasGame($game_id, $user->user_id);

        if($checkGame == false){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda belum memiliki game tersebut di daftar game Anda!');
        }




        //checking type of tournament (solo, duo,trio or squad)
        $tournamentType = (new checkTournamentType)->check($idTournament, $checkTournamentSystem['tournament_system']);

        if($tournamentType['status'] != true){
            throw new Exception('Terjadi kesalahan, segarkan halaman!10');
        }

        if($tournamentType['type_tournament'] == 'Squad'){
                    //checking if this user has game that correlate to the tournament
        $checkTeam = (new CheckTeam)->hasTeam($game_id, $user->user_id);

        if($checkTeam == false){
            throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan Anda belum memiliki team untuk game tersebut!');
        }


        //checking if this user is leader or not
        $getTeam = TeamModel::where('game_id', $game_id)->where('leader_id', $user->user_id)->first(['team_id']);
        if($getTeam == null){
            throw new Exception('Hanya leader team yang dapat melakukan pendaftaran tournament!');
        }

        //checking if this user team is banned or not
        $isBanned = (new CheckBanned)->isBanned($getTeam->team_id);
        if($isBanned == true){
            throw new Exception('Tidak dapat melakukan pendaftaran tournament dikarenakan team Anda telah di ban!');
        }


        //count total of team member
        $countPlayer = TeamPlayerModel::where('team_id', $getTeam->team_id)->first(['player_id']);
        if($countPlayer == null){
            throw new Exception('Terjadi kesalahan, segarkan halaman!9');
        }

            $player_on_team = GameModel::where('id_game_list', $game_id)->first(['player_on_team']);
            $player = explode(',',$countPlayer->player_id);
            $no = count($player);
            if($no < $player_on_team->player_on_team){
                throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan member tim Anda belum penuh');
            }


            //checking what treat type used by this user
            if($treat == 'self'){
                $no = 1;
            }elseif($treat == 'all'){
                $player = explode(',',$countPlayer->player_id);
                $no = count($player);
            }
        }


        if($tournamentType['type_tournament'] == 'Squad'){

        //check user's balance
        switch($checkTournamentMethod['payment_method']){

            case 'pay':
                $checkBalanceOrPoint = (new CheckBalanceCashOrPoint)->isSufficient($checkTournamentMethod['value'], $no, $choosenPaymentMethod);
                if(!$checkBalanceOrPoint){
                    throw new Exception('Stream cash atau point tidak mencukupi!');
                }

                $this->payTournament($request,$treat,$tournament, $getTeam,$checkTournamentSystem,$checkTournamentMethod,$choosenPaymentMethod);

                //Registration process

            break;

            case 'ticket':
                $ticketType = TicketModel::where('id_ticket', $checkTournamentMethod['ticket_id'])->first(['functions']);

                if($ticketType == null ){
                    throw new Exception('Terjadi kesalahan, segarkan halaman!10');
                }

                if($ticketType->functions == 1){
                    $checkBalanceTicket = (new checkBalanceTicketUser)->check($idTournament, $checkTournamentSystem['tournament_system'], $user->user_id);

                    if($checkBalanceTicket == null OR $checkBalanceTicket == false){

                        throw new Exception('Ticket tidak mencukupi');
                    }

                    if($checkBalanceTicket['status'] == 'false'){
                        $errorMsg = $checkBalanceTicket['message'];
                        throw new Exception('ticket_user');
                    }



                    $regist = new RegistTeamByTicket();

                    //Registration process
                    $reg =  $regist->regist($tournament , $getTeam->team_id, $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value'],$checkTournamentSystem['tournament_system']);
                    if($reg == false){
                        throw new Exception('Gagal melakukan pendaftaran tournament');
                    }

                }elseif($ticketType->functions == 3){
                    $checkBalanceTicket = (new checkBalanceTicketTeam)->check($idTournament, $checkTournamentSystem['tournament_system'], $getTeam->team_id);

                    if($checkBalanceTicket == null OR $checkBalanceTicket == false){

                        throw new Exception('Gagal melakukan pendaftaran tournament dikarenakan ticket tim Anda tidak mencukupi!');
                    }

                    $regist = new RegistTeamByTicket();

                    //Registration process
                    $reg =  $regist->regist($tournament , $getTeam->team_id, $checkTournamentMethod['ticket_id'], $checkTournamentMethod['value'],$checkTournamentSystem['tournament_system']);
                    if($reg == false){
                        throw new Exception('Gagal melakukan pendaftaran tournament');
                    }
                }else{
                    $checkBalanceTicket = false;
                }

            break;

            case 'free':
                $regist = (new FreeRegist)->regist($tournament, $getTeam->team_id, $checkTournamentSystem['tournament_system']);
                if($regist == false){
                    throw new Exception('Gagal melakukan pendaftaran tournament');
                }
            break;
        }

    }else if($tournamentType['type_tournament'] == 'Solo'){

        switch($checkTournamentMethod['payment_method']){
            case 'free':
                $regist = (new RegistSoloTournamentFree)->regist($tournament, $user->user_id, $checkTournamentSystem['tournament_system']);

                if($regist == false){
                    throw new Exception('Gagal melakukan pendaftaran tournament');
                }
            break;

        }

    }

        //Updating filled slot on the tournament
        $tournamentFilledSlot = (new TournamentFilledSlotCounter)->sum($idTournament,$filledSlot,$checkTournamentSystem['tournament_system']);



        $request->session()->flush();
        return response()->json([
            'status'    => true,
            'message'   => 'Pendaftaran tournament berhasil!'
        ]);


        }catch(\Exception $e){

            if($e->getMessage() == 'ticket_user'){

                return response()->json([
                    'status'    => false,
                    'message'   => $errorMsg
                ], $this->HTTP_NOT_FOUND);

            }else{

                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ], $this->HTTP_NOT_FOUND);

            }

        }




    }

    public function payTournament(Request $request,$treat,$tournament, $getTeam,$checkTournamentSystem,$checkTournamentMethod,$choosenPaymentMethod)
    {
        if($treat == 'self'){
            $pay = (new RegistTeamByCashOrPoint)->register($tournament,$getTeam->team_id,$checkTournamentMethod['value'],$checkTournamentSystem['tournament_system'],$choosenPaymentMethod);

            if(!$pay){
                throw new Exception('Gagal melakukan pendaftaran tournament');
            }
        }
        else{

            $pay = (new RegistMemberByCashOrPoint)->register($tournament,$getTeam->team_id,$checkTournamentSystem['tournament_system'],$choosenPaymentMethod);

            if(!$pay){
                throw new Exception('Gagal melakukan pendaftaran tournament');
            }
        }
    }
}
