<?php

namespace App\Http\Controllers\Api\Tournament;

use App\Http\Controllers\Controller;
use App\Model\PrizeInventoryModel;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;

class PrizepoolInventoryController extends Controller
{
    //
    public function getInventoryList(){
            try{
                $inventoryList = PrizeInventoryModel::whereRaw('quantity > used_unit')
                                                        ->orderBy('created_at', 'desc')
                                                        ->get(['id','title', 'quantity', 'image', 'used_unit']);
                 if($inventoryList){
                    return Response()->json([
                        'status' => true,
                        'data' => $inventoryList
                    ]);
                 }

                 return Response()->json([
                    'status' => true,
                    'data' => []
                ]);

            }catch(\Exception $e){
                return Response()->json([
                    'status' => false,
                    'message' => 'Failed to load data'
                ]);
            }
    }



}
