<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\IdentityGameModel;
use App\Model\OnboardingModel;
use App\Model\TeamModel;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Onboarding
 *
 * APIs for the Onboarding
 */
class OnboardingController extends Controller
{

	/**
	 * Get Status Onboarding.
	 * <aside class="notice">APIs Method for get onboarding status| untuk melihat status onboarding</aside>
	 * @urlParam app required The value must be one of ['web', 'ios', 'android'] the source where you came from | tipe app yang digunakan. Example: web
	 * @response scenario="failed" status="200" {
			"status": false
		}
	 */
	public function check(Request $request)
	{
		$request->validate([
			'app'    => 'required|in:web,ios,android',
		]);

		$status = false;

		$user = $request->user();

		$is_have_game = IdentityGameModel::where('users_id', $user->user_id)
			->exists();

		if ($is_have_game) {
			$status = true;
		}

		$is_have_team = TeamModel::leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
			->where('player_id', 'LIKE', '%' . $user->user_id . '%')
			->where('substitute_id', 'LIKE', '%' . $user->user_id . '%')
			->exists();

		if ($is_have_team) {
			$status = true;
		}

		$onboarding = OnboardingModel::where('user_id', $user->user_id)
			->where('app', $request->app)
			->firstOr(function () use ($user, $request, $status) {
				return OnboardingModel::create([
					'user_id' 	=> $user->user_id,
					'app' 		=> Str::lower($request->app),
					'status' 	=> $status
				]);
			});

		return response()->json([
			'status' => (bool) $onboarding->status
		], Response::HTTP_OK);
	}

	/**
	 * Update Status Onboarding.
	 * <aside class="notice">APIs Method for Update Status Onboarding | Endpoint untuk merubah status onboarding</aside>
	 * @bodyParam status The value must be one of 1 or 0, custom value (if you want to change status to 0 = false again, the default is 1 = true) | nilai '1' atau '0' (jika ingin merubah status ke 0 = false lagi, nilai default nya 1 = true). Example: 1
	 * @bodyParam app required The value must be one of ['web', 'ios', 'android'] the source where you came from | tipe app yang digunakan. Example: web
	 * @response scenario="success" status="200" {
            "status": true,
        }
	 * @response scenario="failed" status="400" {
            "status": false,
            "error": "error message"
        }
	 */
	public function set(Request $request)
	{

		$request->validate([
			'app'    => 'required|in:web,ios,android',
		]);

		try {
			$status = $request->status ?? '1';

			$user = $request->user();

			$onboarding_cek = OnboardingModel::where('user_id', $user->user_id)
				->where('app', $request->app)
				->firstOr(function () use ($user, $request, $status) {
					return OnboardingModel::create([
						'user_id' 	=> $user->user_id,
						'app' 		=> Str::lower($request->app),
						'status' 	=> $status
					]);
				});

			$onboarding_cek->update(['status' => $status]);;

			return response()->json([
				'status' => (bool) $onboarding_cek->status
			], Response::HTTP_OK);
		} catch (\Exception $e) {
			return response()->json([
				'status'	=> false,
				'error' 	=> $e->getMessage()
			], Response::HTTP_BAD_REQUEST);
		}
	}
}
