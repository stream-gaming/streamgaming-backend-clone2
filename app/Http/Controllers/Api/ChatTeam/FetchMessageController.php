<?php

namespace App\Http\Controllers\Api\ChatTeam;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Http\Requests\EarliestMessageRequest;
use App\Http\Requests\LatestMessageRequest;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Modules\Team\Repositories\TeamPlayerNewRepository;
use App\Transformer\TeamChatHeaderTransform;
use App\Transformer\TeamMessagesTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Team | Endpoint untuk pesan Tim
 */
class FetchMessageController extends Controller
{

    /**
	 * Get Latest Messages Team.
	 * <aside class="notice">APIs Method for get Latest Message on team Chat | Endpoint untuk ambil isi pesan terbaru di chatroom team</aside>
     * @queryParam date integer optional parameter to get latest data after the specified timestamp. Example: 1616996160
     * @queryParam limit integer optional parameter to limit the number of rows of data received. Default to 15. Example: 10
	 * @responseFile status=200 scenario="success" responses/messages.team.messages.latest.success.json
	 * @response scenario="failed" status="404" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
	 * @response scenario="not-member" status="403" {
			"status": false,
			"message": "Anda bukan anggota tim ini"
		}
	 */
    public function latest(LatestMessageRequest $request, $teamId)
    {
        $date = $request->get('date');
        $limit = $request->get('limit', 15);

        try {
            $team	= TeamModel::findOrFail((new MyApps)->onlyDecrypt($teamId));

            $player = (new TeamPlayerNewRepository)->getAllTeamPlayer($team->team_id);

            if (!collect($player)->contains('player_id','=',Auth::user()->user_id)) {
				throw new  Exception(Lang::get('message.team.not-member'), 403);
			}

            $resource = new Item($team, new TeamChatHeaderTransform);
            $manager = (new Manager())->setSerializer(new ArraySerializer);

            $header_data = $manager->createData($resource)->toArray();

            $messages = MessageTeamModel::select(['id', 'team_id', 'user_id', 'username', 'message', 'attachment', 'message_teams.created_at',  'user_read'])
                ->with('detailUser')
				->where("team_id", $team->team_id)
                ->when($date ,function ($query, $date)
                {
                    return $query->where('created_at','>',Carbon::createFromTimestamp($date));
                })
                ->orderBy('created_at', 'desc');

            $count = $messages->count();

            $messages = $messages->limit($limit)->get();


            $header_array = array_merge($header_data ,['reachedLimit' => $count > $limit]);

            $messages = fractal()
                        ->collection($messages)
                        ->transformWith(new TeamMessagesTransform($team))
                        ->toArray();

            return response()->json([
                'header'    => $header_array,
                'messages'  => $messages,
            ], 200);

        } catch (\Exception $e) {
            Log::debug($e->getMessage());

            if ($e->getCode() == 403) {
                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage()
                ], 403);
            }

            return response()->json([
                'status' => false,
                'message' => Lang::get('message.team.not')
            ], 404);
        }
    }

    /**
	 * Get Earliest Messages Team.
	 * <aside class="notice">APIs Method for get Earliest Message on team Chat | Endpoint untuk ambil isi pesan terlama di chatroom team</aside>
     * @queryParam date integer required parameter to get earliest data after the specified timestamp. Example: 1616996160
     * @queryParam limit integer optional parameter to limit the number of rows of data received. Default to 15. Example: 10
	 * @responseFile status=200 scenario="success" responses/messages.team.messages.earliest.success.json
	 * @response scenario="failed" status="404" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
	 * @response scenario="not-member" status="403" {
			"status": false,
			"message": "Anda bukan anggota tim ini"
		}
	 */
    public function earliest(EarliestMessageRequest $request, $teamId)
    {
        $date = $request->get('date');
        $limit = $request->get('limit', 15);

        try {
            $team	= TeamModel::findOrFail((new MyApps)->onlyDecrypt($teamId));

            $player = (new TeamPlayerNewRepository)->getAllTeamPlayer($team->team_id);

            if (!collect($player)->contains('player_id','=',Auth::user()->user_id)) {
				throw new  Exception(Lang::get('message.team.not-member'), 403);
			}

            $resource = new Item($team, new TeamChatHeaderTransform);
            $manager = (new Manager())->setSerializer(new ArraySerializer);

            $header_data = $manager->createData($resource)->toArray();

            $messages = MessageTeamModel::select(['id', 'team_id', 'user_id', 'username', 'message', 'attachment', 'message_teams.created_at', 'user_read'])
                ->with('detailUser')
				->where("team_id", $team->team_id)
                ->where('created_at','<',Carbon::createFromTimestamp($date))
                ->orderBy('created_at', 'desc');

            $count = $messages->count();

            $messages = $messages->limit($limit)->get();


            $header_array = array_merge($header_data ,['reachedLimit' => $count < $limit]);

            $messages = fractal()
                        ->collection($messages)
                        ->transformWith(new TeamMessagesTransform($team))
                        ->toArray();

            return response()->json([
                'header'    => $header_array,
                'messages'  => $messages,
            ], 200);

        } catch (\Exception $e) {
            Log::debug($e->getMessage());

            if ($e->getCode() == 403) {
                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage()
                ], 403);
            }

            return response()->json([
                'status' => false,
                'message' => Lang::get('message.team.not')
            ], 404);
        }
    }
}
