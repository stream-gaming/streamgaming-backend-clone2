<?php

namespace App\Http\Controllers\Api\ChatTeam;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Model\MessageTeamModel;
use App\Model\TeamPlayerNewModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Team | Endpoint untuk pesan Tim
 */
class ListTeamController extends Controller
{
    protected $MyApps;

    public function __construct()
    {
        $this->MyApps = new MyApps();
    }

    /**
	 * Get List Team No Last Message.
	 * <aside class="notice">APIs Method for get list Team user have without last message | Endpoint untuk ambil list team yang kita punya tanpa pesan terakhir</aside>
	 * @responseFile status=200 scenario="success" {
     *     "data" : [
     *          {
     *              "team_id": "eX4mPl_e",
     *              "unread": 1
     *          },
     *          {
     *              "team_id": "Ex4MpLE",
     *              "unread": 0
     *          },
     *      ]
     * }
	 */
    public function index()
    {
        $user = Auth::user();

        $team_notif = MessageTeamModel::selectRaw("count(message_teams.user_id) as notif_count, message_teams.team_id")
				->whereNull('user_read')
				->orWhere('user_read', 'NOT LIKE', '%' . $user->user_id . '%')
				->groupBy('message_teams.team_id');

        $team = TeamPlayerNewModel::select(['team_player_new.team_id', 'notif_count'])
                                                ->where('player_id', $user->user_id)
                                                ->leftJoin(DB::raw('(' . $this->MyApps->getEloquentSqlWithBindings($team_notif) . ') AS tsm'), 'tsm.team_id', '=', 'team_player_new.team_id')
                                                ->get();

        $team = collect($team)->transform(function($item) {
            return [
                'team_id' => $this->MyApps->onlyEncrypt($item->team_id),
                'unread'  => (int) $item->notif_count
            ];
        })->all();

        return response()->json([
            'data' => $team,
            'user' => $user
        ]);
    }
}
