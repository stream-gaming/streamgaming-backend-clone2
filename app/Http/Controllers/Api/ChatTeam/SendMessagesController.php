<?php

namespace App\Http\Controllers\Api\ChatTeam;

use App\Events\MessageTeam;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendMessagesRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\MyApps;
use App\Http\Controllers\Api\Notification\SendNotification;
use App\Model\TeamModel;
use App\Model\ForumFirebaseToken;
use App\Services\UploadService;
use Exception;
use Illuminate\Support\Facades\Lang;
use Intervention\Image\ImageManagerStatic as Image;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;


/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Team | Endpoint untuk pesan Tim
 */
class SendMessagesController extends Controller
{
    public function __construct()
    {
        $this->MyApps = new MyApps();
    }

    /**
     * Send Message Team
     * This API let you to send message for your team
     *
     * @bodyParam team_id string required the encrypted team id
     * @bodyParam message string required the text message
     * @bodyParam attach file the image attachment. The extension must be one of JPG, JPEG, PNG.
     *
     * @response 200 {
     *      "message" : "OK"
     * }
     */
    public function send(SendMessagesRequest $request)
    {
        try {

            $myapps = new MyApps();
            $user = Auth::user();

            $team_id = $myapps->onlyDecrypt($request->team_id);

            $team = TeamModel::where('team_id', $team_id)->firstOr(function () {
                throw new Exception(Lang::get('message.team.not'), $this->HTTP_NOT_FOUND);
            });

            $check = in_array($user->user_id, $team->allPlayer());

            if (!$check) {
                throw new  Exception(Lang::get('message.team.not-member'), 403);
            }

            if ($request->attach != null) {
                $attachment = $this->upload($request);
                if ($attachment == false) {
                    throw new Exception(Lang::get('image.fail.upload'), 500);
                }
            } else {
                $attachment = null;
            }


            DB::beginTransaction();

            $message = $user->messageTeam()->create([
                'team_id'       => $team_id,
                'username'      => $user->username,
                'message'       => $request->message,
                'attachment'    => $attachment,
                'user_read'     => $request->xxx . ',' //user yang sedang online dan berada didalam room chat ini secara ototmatis dianggap sudah membaca pesan
            ]);

            DB::commit();

            $message = $message->load('user');

            $result = (object) [
                'chat' => (object) [
                    'id'        => $message->id,
                    'user'      => [
                        'username' => $message->user->username,
                        'role'     => $team->role($message->user->user_id),
                        'picture'  => $myapps->getImage($message->user_id),
                    ],
                    'is_me'      => false,
                    'message'    => $message->message,
                    'created_at' => date('Y-m-d H:i:s', strtotime($message->created_at)),
                    'attachment' => $message->attachment,
                    'team_id'    => $myapps->onlyEncrypt($message->team_id),
                    'team_name'  => $team->team_name,
                    'logo'       => $this->MyApps->cdn($team->team_logo, 'encrypt', 'team'),
                ],
            ];

            $this->sendFirebase($result);

            // broadcast(new MessageTeam($result))->toOthers();

            $array_player = $team->allPlayer();

            $key = array_search($user->user_id, $array_player);

            unset($array_player[$key]);

            $token = ForumFirebaseToken::whereIn('user_id', $array_player)->get();

            $alltoken = [];

            foreach ($token as $val) {
                $alltoken[] = $val->token;
            }

            $push = new SendNotification;

            $messages = $message->message == null ? "mengirim gambar" : $message->message;

            if(count($alltoken)){
                $push->multiSendPush([
                    'title' => "Stream Gaming",
                    'body'  => $user->username . " : " . $messages,
                    'image' => $message->attachment,
                    'icon'  => $myapps->cdn($team->team_logo, 'encrypt', 'team'),
                    'data'  => collect($result)->toArray(),
                    'tag'   => $request->team_id
                ],$alltoken);
            }


            //requestnya orang front end
            $result->chat->is_me = true;
            return response()->json([
                "message" => "OK",
                "data"    => $result
            ], 201);
        } catch (\Throwable $th) {
            DB::rollBack();
            if ($th->getCode() != 0) {
                return response()->json([
                    "message" => $th->getMessage()
                ], $th->getCode());
            } else {
                return $th;
            }
        }
    }

    public function upload($request)
    {
        $upload = new UploadService;

        $image = $request->file('attach');

        $result = $upload->upload('public/content/chat/team', $image, null, [
            'options' => 'resize-ratio',
            'width'   => Image::make($image)->width(),
            'height'  => Image::make($image)->height()
        ]);

        if (!is_string($result)) {
            $result = false;
        }

        return $result;
    }

    private function sendFirebase($content)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()->getDatabase();

        return $firebase->getReference('team/' . $content->chat->team_id)
            ->set($content)->getvalue();
    }
}
