<?php

namespace App\Http\Controllers\Api\ChatTeam;

use App\Helpers\MyApps;
use App\Model\User;
use App\Http\Requests\ValidateAddFriend;
use App\Http\Controllers\Controller;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Modules\Team\Repositories\TeamPlayerNewRepository;
use App\Transformer\TeamListMessagesTransform;
use App\Transformer\TeamMessagesTransform;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Team | Endpoint untuk pesan Tim
 */
class ListMessageController extends Controller
{

	public function __construct()
	{
		$this->MyApps = new MyApps();
	}

	/**
	 * Get List Team.
	 * <aside class="notice">APIs Method for get list Team user have | Endpoint untuk ambil list team yang kita punya</aside>
	 * @responseFile status=200 scenario="success" responses/messages.team.list.success.json
	 * @response scenario="failed" status="404" {
            "status": false,
            "message": "Belum ada tim"
        }
	 */
	public function getListTeam(Request $request)
	{
		try {
			$user = $request->user();

			$team_notif = MessageTeamModel::selectRaw("count(message_teams.user_id) as notif_count, team_id")
				->whereNull('user_read')
				->orWhere('user_read', 'NOT LIKE', '%' . $user->user_id . '%')
				->groupBy('message_teams.team_id');

			//- Get List team list that user has and the last chat on that team
			$team_player = TeamModel::selectRaw('team.team_id, team_logo, team_name, notif_count, team.created_at')
				->join('team_player_new', function ($join) use($user) {
                    $join->on('team.team_id', '=', 'team_player_new.team_id')
                         ->where('team_player_new.player_id', $user->user_id);
                })
				->leftJoin(DB::raw('(' . $this->MyApps->getEloquentSqlWithBindings($team_notif) . ') AS tsm'), 'tsm.team_id', '=', 'team.team_id')
				//- get Last Messages on Team
				->with(['lastMessage' => function ($query) {
					// $query->orderBy('message_teams.created_at', 'desc');
					$query->select(['team_id', 'user_id', 'username', 'message', 'attachment', 'message_teams.created_at']);
                    $query->latest();
				}])
				->orderBy('team.created_at', 'DESC')
				->get()
				->sortByDesc('lastMessage.created_at');

			if (!$team_player) {
				throw new Exception('Belum ada Tim');
			}

			$data = fractal()
				->collection($team_player)
				->transformWith(new TeamListMessagesTransform())
				->toArray();

			$data['user'] = $user;

			return response()->json($data, $this->HTTP_OK);
		} catch (\Exception $e) {
			return response()->json([
				'status' 	=> false,
				'message'   => $e->getMessage()
			], $this->HTTP_BAD);
		}
	}

	/**
	 * Get Messages Team.
	 * <aside class="notice">APIs Method for get Message on team Chat| Endpoint untuk ambil isi pesan di chatroom team </aside>
	 * @responseFile status=200 scenario="success" responses/messages.team.messages.success.json
	 * @response scenario="failed" status="404" {
            "status": false,
            "message": "Belum ada Pesan"
        }
	 * @response scenario="not-member" status="403" {
			"status": false,
			"message": "Anda bukan anggota tim ini"
		}
	 */
	public function getMessages(Request $request, $team_id)
	{
		try {
			$user 	= $request->user();
			$limit  = $request->limit != null ? $request->limit : 25;
			$team	= TeamModel::find($this->MyApps->onlyDecrypt($team_id));

			if (is_null($team)) {
				return response()->json([
					'status'    => false,
					'message'   => Lang::get('message.team.not')
				], $this->HTTP_NOT_FOUND);
			}

            $player = (new TeamPlayerNewRepository)->getAllTeamPlayer($team->team_id);

			if (!collect($player)->contains('player_id','=',Auth::user()->user_id)) {
				throw new  Exception(Lang::get('message.team.not-member'), 403);
			}

			if ($request->notif == "true") {
				MessageTeamModel::where('team_id', $team->team_id)
					->where('user_read', 'NOT LIKE', '%' . $user->user_id . '%')
					->update([
						'user_read' => DB::raw("CONCAT(COALESCE(user_read,''),'$user->user_id,')")
					]);
			}

			$messages = MessageTeamModel::select(['id','team_id', 'user_id', 'username', 'user_read' ,'message', 'attachment', 'message_teams.created_at'])
				->orderBy('message_teams.created_at', 'desc')
				->with('detailUser')
                ->with(['userInTeamPlayer' => function ($q) use($team) {
                    $q->where('team_id', $team->team_id);
                }])
				->where("team_id", $team->team_id)
				->paginate($limit);

			$data['header'] = [
				'team_id'      => $team_id,
				'team_name'    => $team->team_name,
				'logo'         => $this->MyApps->cdn($team->team_logo, 'encrypt', 'team'),
				'member'       => TeamPlayerNewModel::where('team_id', $team->team_id)->count()
			];

			$data["messages"] = fractal()
				->collection($messages)
				->transformWith(new TeamMessagesTransform($team))
				->paginateWith(new IlluminatePaginatorAdapter($messages))
				->toArray();

			return response()->json($data, $this->HTTP_OK);
		} catch (\Throwable $e) {
			$code = $e->getCode() == 0 ? $this->HTTP_BAD : $e->getCode();
			return response()->json([
				'status' 	=> false,
				'message'   => $e->getMessage()
			], $code);
		}
	}
}
