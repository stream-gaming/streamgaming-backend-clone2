<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\AllTicketModel;
use Illuminate\Http\Request;
use App\Model\User;
use App\Services\UploadService;
use Illuminate\Support\Facades\DB;

/**
 * @hideFromAPIDocumentation
 * @group Ticket
 * @authenticated
 * APIs for the All Ticket Model
 */
class AllTicketController extends Controller
{

    /**
     * Get Detail Ticket.
     * <aside class="notice">APIs Method for get Detail ticket | Endpoint untuk melihat detail tiket</aside>
     * @urlParam id number required id of ticket.
     * @response scenario="success" status="200" {
            "status": true,
            "data": {
                "id": 5,
                "name": "huha",
                "type": 1,
                "sub_type": 2,
                "unit": 0,
                "max_given": "301",
                "total_given": 254,
                "expired": "2021-04-30 08:21:00",
                "event_end": "2021-04-30 08:21:00",
                "competition": "501085427",
                "can_traded": 0,
                "market_value": 0,
                "giving_target": 0,
                "mode": 3,
                "max_has": "6",
                "created_at": "2021-03-18T11:22:46.000000Z",
                "updated_at": "2021-04-06T03:23:14.000000Z",
                "deleted_at": null
            }
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Ticket Tidak ditemukan"
        }
     */
    public function detail(Request $request)
    {
        $ticket = AllTicketModel::find($request->id);

        if (is_null($ticket)) {
            return response()->json([
                'status'    => false,
                'message'   => trans('message.ticket.not-found'),
            ], $this->HTTP_BAD);
        }

        return response()->json([
            'status'    => true,
            'data'      => $ticket,
        ]);
    }

    /**
     * Update Tiket.
     * <aside class="notice">APIs Method for Update Tiket | Endpoint untuk merubah tiket</aside>
     * @urlParam id number required id of ticket.
     * @bodyParam name string required name of ticket.
     * @bodyParam max_given string required max_given of ticket, must be greater than or equal, value before.
     * @bodyParam max_has string required max_has of ticket, must be greater than or equal, value before.
     * @bodyParam event_end string required event_end of ticket, must be greater than or equal, value before.
     * @bodyParam expired string required expired of ticket, must be greater than or equal, value before.
     * @response scenario="success" status="200" {
            "status": true,
            "data": {
                "id": 5,
                "name": "huha",
                "type": 1,
                "sub_type": 2,
                "unit": 0,
                "max_given": "301",
                "total_given": 254,
                "expired": "2021-04-30 08:21:00",
                "event_end": "2021-04-30 08:21:00",
                "competition": "501085427",
                "can_traded": 0,
                "market_value": 0,
                "giving_target": 0,
                "mode": 3,
                "max_has": "6",
                "created_at": "2021-03-18T11:22:46.000000Z",
                "updated_at": "2021-04-06T03:23:14.000000Z",
                "deleted_at": null
            }
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Ticket Tidak ditemukan"
        }
     */
    public function edit(Request $request, AllTicketModel $ticket)
    {
        $request->validate([
            'name'      => "required",
            'max_given' => 'required|gte:' . $ticket->max_given,
            'max_has'   => 'required|gte:' . $ticket->max_has,
            'event_end' => 'required|after_or_equal:' . $ticket->event_end,
            'expired'   => 'required|after_or_equal:' . date('Y-m-d h:i',strtotime($ticket->expired)),
        ]);
        if ($request->attach) {
            $file = $request->attach;
            $image = (new UploadService)->upload('public/content/ticket', $file, $ticket->image, [
                'options' => 'resize',
                'width' => 336,
                'height' => 146
            ]);

            //check error when upload
            if (is_object($image)) {
                return $image;
            }
            $ticket->image = $image;
        }

    
        try {
            DB::transaction(function () use ($request, $ticket) {
                $ticket->name = $request->name;
                $ticket->max_given = $request->max_given;
                $ticket->max_has = $request->max_has;
                $ticket->event_end = $request->event_end;
                $ticket->expired = $request->expired;
                $ticket->save();
            });
            return response()->json([
                'status'    => true,
                'data'      => $ticket,
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => trans('message.ticket.not-found')
            ]);
        }
    }
}
//keep
