<?php

namespace App\Http\Controllers\Api;

use App\Model\IdentityGameModel;  
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth; 

class IdentityGameController extends Controller
{
	public function hasIdentity($game_id, $user_id=null /*optional*/)
	{
		if (is_null($user_id)) {
	 		$Identity = IdentityGameModel::where('game_id', $game_id)
											->where('users_id', Auth::user()->user_id)
											->count();
		} else {
	 		$Identity = IdentityGameModel::where('game_id', $game_id)
											->where('users_id', $user_id)
											->count();
		}

		if ($Identity != 1) {
			return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.not-has')
            ], 400);
		} 
	}
}
