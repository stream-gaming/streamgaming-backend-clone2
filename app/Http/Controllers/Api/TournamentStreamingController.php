<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\TournamentStreamingModel;
use Illuminate\Http\Request;

/**
 * @group Tournament
 *
 * APIs for the Tournament Streaming
 */
class TournamentStreamingController extends Controller
{


   /**
     * Tournament Streaming - Insert Data
     *
     * @bodyParam id_tournament string  The id_tournament
     * @bodyParam title string for The title
     * @bodyParam url string for url Streaming
     * @bodyParam sequence integer for The sequence
     *
     * @response 201 {
     *      "status": true,
     *      "message": "messages Insert data Streaming Success"
     *  }
     *
     * @response 402 {
     *       "status": false,
     *       "message": "messages Insert data failed"
     *   }
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_tournament' => 'required',
            'title'         => 'required|string|max:255',
            'url'           => 'required|max:255',
            'sequence'      => 'required',
        ]);
        try {
            $Streaming = new TournamentStreamingModel();
            $Streaming->id_tournament   = $request->id_tournament;
            $Streaming->title           = $request->title;
            $Streaming->url             = $request->url;
            $Streaming->sequence        = $request->sequence;
            $Streaming->save();
            return response()->json([
                'status'  => true, 
                'message' => "messages Insert data Streaming Success"
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => false,
                'message' => "messages Insert data failed",
                'error'   => $th->__toString()
            ],402);
        }
    }
     /**
     * Tournament Streaming - Update Data.
    * @urlParam tournament_streaming required The ID of tournament_streaming. Example:1
     * @bodyParam title string for The title
     * @bodyParam url string for url Streaming
    * @response 201 {
     *      "status": true,
     *      "message": "messages Update data Streaming Success"
     *  }
     *
     * @response 402 {
     *       "status": false,
     *       "message": "messages Update data failed"
     *   }
     */
    public function update(Request $request){
        $request->validate([
            'title'         => 'string|max:255',
            'url'           => 'max:255',
        ]);
        try {
            $data = [
                "title"         => $request->title,
                "url"           => $request->url,
            ];
            $Streaming = TournamentStreamingModel::where('id',$request->id)->update($data);
            if($Streaming){
                return response()->json([
                    'status'  => true,
                    'message' => "messages Update data Streaming Success"
                ], 200);
            }else{
                return response()->json([
                    'status'  => true,
                    'message' => "Data Tournament Streaming Tidak di temukan "
                ], 404);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => false,
                'message' => "messages Update data failed",
                'error'   => $th->__toString()
            ],500);
        }
    }
     /**
    * Tournament Streaming - Update Data.
     *
    * @urlParam tournament_streaming required The ID of tournament_streaming. Example:1
     *
     * @response 201 {
     *      "status": true,
     *      "message": "messages Delete data Streaming Success""
     *  }
     *
     * @response 402 {
     *       "status": false,
     *       "message": "messages Delete data Streaming Success"
     *   }
     */
    public function delete($id){
        try {
            $Streaming = TournamentStreamingModel::where('id', $id)->delete();
            if($Streaming){
                return response()->json([
                    'status' => true,
                    'message' => "messages Delete data Streaming Success"
                ]);
            }else{
                return response()->json([
                    'status'  => true,
                    'message' => "Data Tournament Streaming Tidak di temukan "
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => "messages Delete data Streaming Success"
            ]);
        }
    }
}
