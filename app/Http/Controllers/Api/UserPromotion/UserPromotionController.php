<?php

namespace App\Http\Controllers\Api\UserPromotion;

use App\Http\Controllers\Controller;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use App\Modules\Promotion\Interfaces\UserPromotionServiceInterface;
use App\Transformer\PromotionTransformer;
use App\Transformer\UserPromotionTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
/**
 * @group  User Promotion
 *
 * APIs for managing user's promotion
 */
class UserPromotionController extends Controller
{
    protected $userPromotionService;

    public function __construct(UserPromotionServiceInterface $userPromotionService)
    {
        $this->userPromotionService = $userPromotionService;
    }

    /**
     * Get all user promotion
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/user.promotion.get.all.success.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function getAllUserPromotion()
    {
        try{
            $userPromotion = $this->userPromotionService->getAllUserPromotion(Auth::user());

            //Transform user's promotion
            $resource = new Collection($userPromotion, new UserPromotionTransformer);
            $manager = (new Manager)->setSerializer(new ArraySerializer());
            $data = $manager->createData($resource)->toArray();

            return response()->json($data);

        }catch(\Throwable $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            return response()->json([
                'status' => false,
                'message' => Lang::get('server.error.500')
            ], 500);
        }

    }

    /**
     * Get detail promotion
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/promotion.get.detail.success.json
     * @responseFile status=404 scenario="not found" responses/resource.not.found.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function getPromotionById($promotionId, PromotionRepositoryInterface $promotionRepo)
    {
        try{
            $promotion = $promotionRepo->findById($promotionId);

            //Transform detail promotion
            $resource = new Item($promotion, new PromotionTransformer);
            $manager = (new Manager)->setSerializer(new ArraySerializer());
            $data = $manager->createData($resource)->toArray();

            return response()->json($data);

        }catch(\Throwable $e){
            $message = Lang::get('server.error.500');
            $code = 500;
            if($e instanceof ModelNotFoundException)
            {
                $message = Lang::get('exeptions.model_not_found');
                $code = 404;
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $message
            ], $code);
        }
    }

    /**
     * Read user promotion
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/user.promotion.update.success.json
     * @responseFile status=404 scenario="not found" responses/resource.not.found.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function readUserPromotion($promotionId, PromotionRepositoryInterface $promotionRepo)
    {
        try{

            $promotionRepo->findById($promotionId);
            $this->userPromotionService->readUserPromotion($promotionId, Auth::user());

            return response()->json([
                'status' => true
            ],200);

        }catch(\Throwable $e){
            $message = Lang::get('server.error.500');
            $code = 500;
            if($e instanceof ModelNotFoundException)
            {
                $message = Lang::get('exeptions.model_not_found');
                $code = 404;
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $message
            ], $code);
        }

    }

    /**
     * Read all user promotion
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/user.promotion.update.success.json
     * @responseFile status=404 scenario="not found" responses/resource.not.found.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function readAllUserPromotion()
    {
        try{
            $this->userPromotionService->readAllUserPromotion(Auth::user());

            return response()->json([
                'status' => true
            ],200);

        }catch(\Throwable $e){
            $message = Lang::get('server.error.500');
            $code = 500;
            if($e instanceof ModelNotFoundException)
            {
                $message = Lang::get('exeptions.model_not_found');
                $code = 404;
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $message
            ], $code);
        }

    }

    /**
     * Delete user promotion
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/user.promotion.update.success.json
     * @responseFile status=404 scenario="not found" responses/resource.not.found.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function deleteUserPromotion($promotionId, PromotionRepositoryInterface $promotionRepo)
    {
        try{
            $promotionRepo->findById($promotionId);
            $this->userPromotionService->deleteUserPromotion($promotionId, Auth::user());

            return response()->json([
                'status' => true
            ],200);

        }catch(\Throwable $e){
            $message = Lang::get('server.error.500');
            $code = 500;
            if($e instanceof ModelNotFoundException)
            {
                $message = Lang::get('exeptions.model_not_found');
                $code = 404;
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $message
            ], $code);
        }
    }
}
