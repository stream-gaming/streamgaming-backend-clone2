<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessFirebaseCounterPortal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserNotificationPortalController extends Controller
{
    //

    public function sendNotif(){
        try{

            ProcessFirebaseCounterPortal::dispatch('parameter');

            return response()->json([
                "status" => true,
                "message" => "notification sent successfully"
            ], 200);


        }catch(\Exception $e){

			return response()->json([
                "status" => false,
                "message" => "failed to send notif"
            ], 409);

        }
    }
}
