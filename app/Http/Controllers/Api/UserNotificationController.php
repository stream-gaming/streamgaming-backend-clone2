<?php

namespace App\Http\Controllers\Api;

use App\Helpers\PushNotification;
use Carbon\Carbon;
use App\Model\TeamModel;
use Illuminate\Http\Request;
use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessFirebaseCounter;
use App\Model\FcmNotifTokenModel;
use App\Model\UserNotificationModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class UserNotificationController extends Controller
{
	public function __construct($type, $data)
	{
		$this->data = $data;
        $this->$type();
	}

	public function send()
	{
		try {
            //send notif to user
			DB::beginTransaction();
            //send count notif to firebase server using QUEUE
            ProcessFirebaseCounter::dispatch($this->submit['id_user']);

			$notification = new UserNotificationModel;
			$notification->id_notification = uniqid();
			$notification->id_user = $this->submit['id_user'];
			$notification->keterangan = $this->submit['keterangan'];
			$notification->link = $this->submit['link'];
			$notification->is_read = 0;
			$notification->save();
			DB::commit();

            $push = new PushNotification;

            $token = FcmNotifTokenModel::select('token')->where('user_id',$this->submit['id_user'])->get()->pluck('token')->all();

            if(count($token)){
                $push->multiSendPush([
                        'title' => 'StreamGaming',
                        'body'  => $this->submit['keterangan'],
                        'data'  => [
                            'title' => 'StreamGaming',
                            'body'  => $this->submit['keterangan']
                        ]
                    ], $token);
            }





		} catch (\Exception $e) {
			DB::rollBack();
			return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
		}
	}

	public function multiSend()
	{
        try {
            foreach($this->submit as $data) {
                //send count notif to firebase server using QUEUE
                ProcessFirebaseCounter::dispatch($data['id_user']);
            }
			DB::transaction(function () {
				$notification = UserNotificationModel::insert($this->submit);
			});
		} catch (\Exception $e) {
			return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
		}
	}

	public function request_join()
	{
		$identity = IdentityGameModel::where('game_id', $this->data->game_id)
    									->where('users_id', Auth::user()->user_id)
    									->first();
		$this->submit = [
			'id_user'		=> $this->data->leader_id,
			'keterangan'	=> Lang::get('notification.team.join.request', ['username'=>$identity->username_ingame]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function accept_join()
	{
		$team = TeamModel::find($this->data->team_id);
		$this->submit = [
			'id_user'		=> $this->data->users_id,
			'keterangan'	=> Lang::get('notification.team.join.accept', ['team'=>$team->team_name]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function decline_join()
	{
		$team = TeamModel::find($this->data->team_id);
		$this->submit = [
			'id_user'		=> $this->data->users_id,
			'keterangan'	=> Lang::get('notification.team.join.decline', ['team'=>$team->team_name]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function request_invite()
	{
		$team = TeamModel::find($this->data->team_id);
		$this->submit = [
			'id_user'		=> $this->data->users_id,
			'keterangan'	=> Lang::get('notification.team.invite.request', ['team'=>$team->team_name]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function accept_invite()
	{
		$team = TeamModel::find($this->data->team_id);
		$identity = IdentityGameModel::where('game_id', $team->game_id)
									->where('users_id', $this->data->users_id)
									->first();

		$this->submit = [
			'id_user'		=> $team->leader_id,
			'keterangan'	=> Lang::get('notification.team.invite.accept', ['username'=>$identity->username_ingame]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function decline_invite()
	{
		$team = TeamModel::find($this->data->team_id);
        $identity = IdentityGameModel::where('game_id', $team->game_id)
                              		->where('users_id', $this->data->users_id)
                            		->first();
     	$this->submit = [
     		'id_user'		=> $team->leader_id,
     		'keterangan'	=> Lang::get('notification.team.invite.decline', ['username'=>$identity->username_ingame]),
     		'link'			=> 'my-team'
     	];
     	$this->send();
	}

	public function leave_team()
	{
        $notifications = [];
		if (isset($this->data->new_lead)) {
			foreach ($this->data->newPlayer as $p) {
                if (empty($p)) continue;
          		//data game leader baru
				$identity = IdentityGameModel::where('game_id', $this->data->game_id)
											->where('users_id', $this->data->new_lead)
											->first();

               	if ($p == $this->data->new_lead) { //kirim untuk leader baru
               		$notification = [
               			'id_notification'   => uniqid(),
               			'id_user'           => $p,
               			'keterangan'        => Lang::get('notification.team.leave.new-leader', ['team'=>$this->data->team_name]),
               			'link'              => 'my-team',
               			'is_read'           => 0,
               			'date'              => date('Y-m-d H:i:s')
	               	];
	    		} else { //kirim untuk semua
	    			$notification = [
	    				'id_notification'   => uniqid(),
	    				'id_user'           => $p,
	    				'keterangan'        => Lang::get('notification.team.leave.members', ['username'=> $identity->username_ingame]),
	    				'link'              => 'my-team',
	    				'is_read'           => 0,
	    				'date'              => date('Y-m-d H:i:s')
	    			];
	    		}
	    		$notifications[] = $notification;
	    	}
	    	$this->submit = $notifications;
	    	$this->multiSend();
	    } else {
	    	foreach ($this->data->newPlayer as $p) {
                if (empty($p)) continue;
	    		$identity = IdentityGameModel::where('game_id', $this->data->game_id)
								    		->where('users_id', $this->data->mail)
								    		->first();
	    		$notification = [
	    			'id_notification'   => uniqid(),
	    			'id_user'           => $p,
	    			'keterangan'        => Lang::get('notification.team.leave.member', ['username'=> $identity->username_ingame]),
	    			'link'              => 'my-team',
	    			'is_read'           => 0,
	    			'date'              => date('Y-m-d H:i:s')
	    		];

	    		$notifications[] = $notification;
            }
            $this->submit = $notifications;
	    	$this->multiSend();
	    }
	}

	public function kick_team()
	{
		$team = TeamModel::find($this->data->team_id);
		$this->submit = [
     		'id_user'		=> $this->data->users_dest,
     		'keterangan'	=> Lang::get('notification.team.kick', ['team'=>$team->team_name]),
     		'link'			=> 'my-team'
     	];
     	$this->send();
	}

	public function change_leader()
	{
		$team = TeamModel::find($this->data->team_id);
		$this->submit = [
     		'id_user'		=> $this->data->leader_id,
     		'keterangan'	=> Lang::get('notification.team.leave.new-leader', ['team'=>$team->team_name]),
     		'link'			=> 'my-team'
     	];
     	$this->send();
	}

	public function team_banned()
	{
		$reason_msg = empty($this->data->reason_ban) ? 'Tanpa alasan' : 'Dengan alasan ' . $this->data->reason_ban;

		$this->submit = [
			'id_user'		=> $this->data->leader_id,
			'keterangan'	=> Lang::get('notification.team.banned', ['team' => $this->data->game->name, 'until' => ($this->data->ban_until == 'forever') ? 'forever' : Carbon::parse($this->data->ban_until)->toFormattedDateString(), 'reason' => $reason_msg]),
			'link'			=> 'my-team'
		];
		$this->send();
	}

	public function team_unbanned()
	{
		$this->submit = [
     		'id_user'		=> $this->data->leader_id,
     		'keterangan'	=> Lang::get('notification.team.unbanned', ['team'=>$this->data->team_name]),
     		'link'			=> 'my-team'
     	];
     	$this->send();
    }

    public function claim_ticket()
    {
        $this->submit = [
            'id_user'       => Auth::user()->user_id,
            'keterangan'    => Lang::get('notification.claim.ticket', ['ticket' => $this->data->name]),
            'link'          => 'my-ticket'
        ];
        $this->send();
    }

    public function tournamentRegitration_success()
    {
        $notification = [];
        $temp         = [
            'id_user'		=> null,
            'keterangan'	=> Lang::get('Tim '),
            'link'			=> $this->data->url
        ];
        foreach ($this->data->player as $p) {
            $temp['id_user'] = $p;

            array_push($notification, $temp);
        }
        $this->submit = $notification;
        $this->multiSend();
    }

    public function game_banned()
    {
        $this->submit = [
             'id_user'		=> $this->data->users_id,
             'keterangan'	=> Lang::get('notification.game.banned', ['game'=>$this->data->game->name, 'time'=> ($this->data->ban_until == 'forever') ? 'forever' : Carbon::parse($this->data->ban_until)->toFormattedDateString(), 'reason' => $this->data->reason_ban]),
             'link'			=> 'myaccount'
        ];
        $this->send();
    }

    public function game_unbanned()
    {
        $this->submit = [
             'id_user'		=> $this->data->users_id,
             'keterangan'	=> Lang::get('notification.game.unbanned', ['game'=>$this->data->game->name]),
             'link'			=> 'myaccount'
        ];
        $this->send();
    }

    public function ticket_payment()
    {
        $notifData = [
            'unit' => $this->data->unit,
            'ticket_name' => $this->data->ticket_name,
            'prefix' => $this->data->prefix,
            'amount' => $this->data->amount,
            'payment_method' => $this->data->payment_method

        ];
        $this->submit = [
             'id_user'		=> $this->data->users_id,
             'keterangan'	=> Lang::get('notification.ticket.purchase', $notifData),
             'link'			=> ''
        ];
        $this->send();
    }

    public function ticket_payment_xendit()
    {
        $notifData = [
            'ticket_name' => $this->data->ticket_name,
            'amount' => $this->data->amount,
            'invoice_id' => $this->data->invoice_id,
            'link' => $this->data->link
        ];
        $this->submit = [
             'id_user'		=> $this->data->users_id,
             'keterangan'	=> Lang::get('notification.ticket.purchase_xendit_invoice', $notifData),
             'link'			=> $this->data->link
        ];
        $this->send();
    }

    public function ticket_payment_xendit_paid()
    {
        $notifData = [
            'invoice_id' => $this->data->invoice_id,
            'link' => $this->data->link
        ];
        $this->submit = [
             'id_user'		=> $this->data->users_id,
             'keterangan'	=> Lang::get('notification.ticket.purchase_xendit_invoice_paid', $notifData),
             'link'			=> $this->data->link
        ];
        $this->send();
    }

    public function comment_reply(){

        $notifData = [
            'sender_name' => $this->data->sender_name,
            'tour_name' => $this->data->tour_name

        ];

        $this->submit = [
            'id_user'		=> $this->data->receiver_id,
            'keterangan'	=> Lang::get('notification.comment.reply', $notifData),
            'link'			=> $this->data->tour_link
       ];
       $this->send();
    }

    public function comment_delete(){

        $notifData = [
            'sender_name' => $this->data->admin_name,
            'tour_name' => $this->data->tour_name

        ];

        $this->submit = [
            'id_user'		=> $this->data->receiver_id,
            'keterangan'	=> Lang::get('notification.comment.delete', $notifData),
            'link'			=> $this->data->tour_link
       ];
       $this->send();
    }

    public function finish_onboarding(){

        $this->submit = [
            'id_user'		=> $this->data->user_id,
            'keterangan'	=> Lang::get('onboarding.success.finish_onboarding'),
            'link'			=> $this->data->ticket_link
       ];
       $this->send();
    }
}
