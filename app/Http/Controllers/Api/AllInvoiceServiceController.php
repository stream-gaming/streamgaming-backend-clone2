<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\AllInvoiceModel;
use App\Model\BalanceHistoryModel;
use App\Model\TournamentModel;
use App\Model\TournamentWinnerModel;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

use App\Transformer\AllInvoiceTransform;
use App\Transformer\DetailAllInvoiceTranform;
use App\Transformer\LeaderboardAllInvoiceTransform;
use App\Transformer\WinnerSequnceTransform;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use Illuminate\Http\Request;

/**
 * @hideFromAPIDocumentation
 */
class AllInvoiceServiceController extends Controller
{

    public function getAll(Request $request){
        $search = $request->search;
        $query = AllInvoiceModel::with(['getLeaderboard' => function($q) use($search){
                $q->where('deleted_at', '=', null);

                $q->leftJoin('game_list','game_list.id_game_list','=','leaderboard.game_competition');
            }]);
            $query->with(['getBracket' => function($q) use($search){
                $q->leftJoin('game_list','game_list.id_game_list','=','tournament.kompetisi');
                }]);
            if ($search != null) {
                 $query->whereHas('getLeaderboard', function ($q) use ($search) {
                    $q->where('tournament_name', 'LIKE', '%' . $search . '%');
                });
                 $query->orWhereHas('getBracket', function ($q) use ($search) {
                    $q->where('tournament_name', 'LIKE', '%' . $search . '%');
                });
            }
            $query->orderBy('date_end', 'desc');

            $get = $query->paginate(10);
            if(!empty($get)){
                $resource = new Collection($get, (new AllInvoiceTransform));
                $data     = (new Manager())->createData($resource)->toArray();
            }else{
                $data = [];
            }
        $data = [
            "current_page"      => $get->currentPage(),
            "firstItem"         => $get->firstItem(),
            "lastItem"          => $get->lastItem(),
            "lastPage"          => $get->lastPage(),
            "nextPageUrl"       => $get->nextPageUrl(),
            "previousPageUrl"   => $get->previousPageUrl(),
            "total"             => $get->total(),
            "perPage"           => $get->perPage(),
            "data"              => $data
        ];
        return $data;
    }
    public function get_invoice(Request $request){
        $format = $request->format;

        switch ($format) {
            case 'Leaderboard':
                $data = $this->leaderboard($request->id_tournament);
                break;


            case 'Bracket':

                    $data = $this->bracket($request->id_tournament);

                break;
            default:
                throw new Exception('message.bracket.details.not');
                break;
        }
        return $data;
    }
    public function bracket($id_tournament){
        $get= TournamentModel::with(['winnerTour' => function($q)use($id_tournament){
                $q->leftjoin('player_payment', function($join) use($id_tournament){
                    $join->on('player_payment.id_team','=','tournament_winner.juara1')->where("player_payment.id_tournament",$id_tournament); // i want to join the player_paymentayer_payment table with either of these columns
                    $join->orOn('player_payment.id_team','=','tournament_winner.juara2')->where("player_payment.id_tournament",$id_tournament);
                    $join->orOn('player_payment.id_team','=','tournament_winner.juara3')->where("player_payment.id_tournament",$id_tournament);
                })
                ->leftjoin('balance_history', function($join2) use($id_tournament){
                    $join2->on('balance_history.users_id','=','player_payment.player_id')->where("balance_history.id_tournament",$id_tournament); // i want to join the player_paymentayer_payment table with either of these columns
                })
                ->get('tournament_name');
            }])
            ->with(['winnerTourSequence' => function($q) use($id_tournament){
                $q->leftjoin('player_payment', function($join2) use($id_tournament){
                    $join2->on('player_payment.id_team','=','tournament_winner_sequence.id_receiver')->where("player_payment.id_tournament",$id_tournament);

                })
                ->get();
            }])
            ->where("tournament.id_tournament",$id_tournament)
            ->firstOrFail()->toArray();
            // return $get;
            if(!empty($get['winner_tour_sequence']) OR !empty($get['winner_tour'])){

                if($get['winner_tour_sequence'] == []){

                    $resource = new Collection($get['winner_tour'],(new DetailAllInvoiceTranform));

                    $data     = (new Manager)->createData($resource)->toArray();

                }else if(!empty($get['winner_tour_sequence'])){
                    $resource = new Collection($get['winner_tour_sequence'],(new WinnerSequnceTransform));

                    $data     = (new Manager)->createData($resource)->toArray();

                }else{
                    $data = false;
                }
            }else{
                $data = false;
            }
            return response()->json($data);
        // return $data;
    }
    public function leaderboard($id_tournament){
        $getWinner = BalanceHistoryModel::selectRaw(
                    'balance_history.id_tournament,
                     balance_history.transaction_type,
                     balance_history.information,
                     balance_history.date,
                     balance_history.balance,
                     users.username,
                     users.email,
                     leaderboard.most_kill,
                     leaderboard.game_option,
                     leaderboard.tournament_name,
                     leaderboard_winner.sequence,
                     leaderboard_winner.id_player,
                     leaderboard_winner.id_team
                    '
                    )
                     ->leftJoin('leaderboard_winner', function($join) use($id_tournament){
                            $join->on('balance_history.users_id', '=', 'leaderboard_winner.id_player')
                                 ->where('leaderboard_winner.id_leaderboard', '=', $id_tournament);

                     })
                     ->leftJoin('users','users.user_id', '=', 'balance_history.users_id')
                     ->leftJoin('leaderboard', 'leaderboard.id_leaderboard', '=', 'balance_history.id_tournament')
                     ->where('balance_history.id_tournament', $id_tournament)
                     ->get();
    // dd($getWinner);
        $resource = new Collection($getWinner,(new LeaderboardAllInvoiceTransform));

        $data     = (new Manager)->createData($resource)->toArray();
        // dd($getWinner);
        return response()->json($data);

    }
}
