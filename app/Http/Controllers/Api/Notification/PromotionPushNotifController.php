<?php

namespace App\Http\Controllers\Api\Notification;

use App\Helpers\Firebase\FirebaseFactoryInterface;
use App\Http\Controllers\Controller;
use App\Modules\FCM\Interfaces\FCMNotifTokenRepositoryInterface;
use App\Modules\Promotion\Interfaces\PromoPushNotifHistoryRepositoryInterface;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
/**
 * @group  Promotion
 *
 * APIs for pushing promotion notification
 */
class PromotionPushNotifController extends Controller
{
    protected $promotionRepo;
    protected $fcmNotifTokenRepo;
    protected $promoPushNotifHistoryRepo;
    protected $firebaseDBInstance;

    public function __construct(
        PromotionRepositoryInterface $promotionRepo,
        FCMNotifTokenRepositoryInterface $fcmNotifTokenRepo,
        PromoPushNotifHistoryRepositoryInterface $promoPushNotifHistoryRepo,
        FirebaseFactoryInterface $firebaseDBInstance
        )
    {
        $this->promotionRepo = $promotionRepo;
        $this->fcmNotifTokenRepo = $fcmNotifTokenRepo;
        $this->promoPushNotifHistoryRepo = $promoPushNotifHistoryRepo;
        $this->firebaseDBInstance = $firebaseDBInstance;
    }

    /**
     * Push notification to users
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/promotion.push.notification.success.json
     * @responseFile status=500 scenario="failed" responses/server.internal.problem.json
     **/
    public function pushNotification($promotion)
    {
        try{
            $promotion = $this->promotionRepo->findById($promotion);
            $promoPushNotifHistory = $this->promoPushNotifHistoryRepo->findById($promotion->id);

            if($promoPushNotifHistory)
            {
                if($promoPushNotifHistory->attempt > 0)
                {
                    throw new Exception(Lang::get('server.error.500'));
                }

            }

            //update counter promotion on firebase
            $this->firebaseDBInstance->updateDataV2('promotion', [
                'all' => $this->promotionRepo->count()
            ]);

            //send push notification
            $this->fcmNotifTokenRepo->chunkWithPushNotif(200, (object) [
                'title' => $promotion->title,
                'content' => $promotion->content,
                'destination' => config("app.url_frontend").'?promotion='.$promotion->id,
                'thumbnail' => $promotion->thumbnail,
            ]);

            if(!$promoPushNotifHistory)
            {
                $this->promoPushNotifHistoryRepo->create($promotion->id);
                $this->promoPushNotifHistoryRepo->increaseAttempt($promotion->id);
            }

            return response()->json([
                'status' => true
            ], 200);

        }catch(\Throwable $e){
            $message = Lang::get('server.error.500');
            $code = 500;
            if($e instanceof ModelNotFoundException)
            {
                $message = Lang::get('exeptions.model_not_found');
                $code = 404;
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $message
            ], $code);
        }
    }
}
