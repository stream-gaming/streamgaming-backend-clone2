<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveFcmTokenRequest;
use App\Model\FcmNotifTokenModel;
use App\Model\ForumFirebaseToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;


/**
 * @group Notification
 *
 * APIs for Notification
 */

/*
    Create Date : 24 Februari 2021
    By : #mdc-xii
*/

class WebPushController extends Controller
{

    /**
     * Save Token Firebase.
     * @authenticated
     * <aside class="notice">APIs Method for Save Token Firebase | Endpoint Menyimpan Firebase Token [WEB PUSH]</aside>
     * @bodyParam firebase_token string required token of Firebase | Firebase Token. Example: fNLVxrxkJvuBng02bTloVg:APA91bF8nCNZiP6b_KfnlGgo002IRN8zybDHcPEAHo4jR-qosj5kxF9UnAuHQioFeaVJJ_J9B5JbbEGWEBCZzJCBVBO7SPORLyE3oO0-4GhytKs-WrgYPzRqsm-l6CxhUClEj_6X7yAa
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Firebase token berhasil disimpan"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Firebase token gagal disimpan"
        }
     */
    public function saveToken(Request $request)
    {
        try {
            //- Simpan Token Firebase Baru
            $token = new ForumFirebaseToken(['token' => $request->firebase_token]);
            $user  = $request->user();

            $user->firebaseToken()->save($token);

            return response()->json([
                "status"    => true,
                "message"   => trans("notification.create.token.success"),
            ]);
        } catch (Throwable $e) {
            return response()->json([
                "status"    => false,
                "message"   => trans("notification.create.token.error"),
                "error"     => $e->getMessage()
            ]);
        }
    }

    /**
     * Save Token FCM Notification.
     * @authenticated
     * <aside class="notice">APIs Method for Save FCM Token | Endpoint Menyimpan Token FCM [WEB PUSH NOTIFICATION]</aside>
     * @bodyParam firebase_token string required token of Firebase | Firebase Token. Example: fNLVxrxkJvuBng02bTloVg:APA91bF8nCNZiP6b_KfnlGgo002IRN8zybDHcPEAHo4jR-qosj5kxF9UnAuHQioFeaVJJ_J9B5JbbEGWEBCZzJCBVBO7SPORLyE3oO0-4GhytKs-WrgYPzRqsm-l6CxhUClEj_6X7yAa
     * @response scenario="success" status="201" {
            "status": true,
            "message": "Firebase token berhasil disimpan"
        }
     * @response scenario="failed" status="422" {
            "message": "Data yang diberikan tidak valid.",
            "errors": {
                "firebase_token": [
                    "Firebase harus diisi."
                ]
            }
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Firebase token gagal disimpan"
        }
     */
    public function saveTokenNotif(SaveFcmTokenRequest $request)
    {
        try {
            //- Simpan Token Firebase Baru
            $check = FcmNotifTokenModel::where('user_id',Auth::user()->user_id)->where('token',$request->firebase_token)->count();
            if(!$check){
                $token = new FcmNotifTokenModel(['token' => $request->firebase_token]);
                $user  = $request->user();

                $user->fcmFirebaseToken()->save($token);
            }

            return response()->json([
                "status"    => true,
                "message"   => trans("notification.create.token.success"),
            ],$this->HTTP_CREATE);
        } catch (Throwable $e) {
            return response()->json([
                "status"    => false,
                "message"   => trans("notification.create.token.error"),
                "error"     => $e->getMessage()
            ],$this->HTTP_BAD);
        }
    }

    /**
     * Save Token FCM Notification for web.
     * @authenticated
     * <aside class="notice">APIs Method for Save FCM Token | Endpoint Menyimpan Token FCM [WEB PUSH NOTIFICATION]</aside>
     * @bodyParam firebase_token string required token of Firebase | Firebase Token. Example: fNLVxrxkJvuBng02bTloVg:APA91bF8nCNZiP6b_KfnlGgo002IRN8zybDHcPEAHo4jR-qosj5kxF9UnAuHQioFeaVJJ_J9B5JbbEGWEBCZzJCBVBO7SPORLyE3oO0-4GhytKs-WrgYPzRqsm-l6CxhUClEj_6X7yAa
     * @response scenario="success" status="201" {
            "status": true,
            "message": "Firebase token berhasil disimpan"
        }
     * @response scenario="failed" status="422" {
            "message": "Data yang diberikan tidak valid.",
            "errors": {
                "firebase_token": [
                    "Firebase harus diisi."
                ]
            }
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Firebase token gagal disimpan"
        }
     */
    public function saveTokenNotifWeb(SaveFcmTokenRequest $request)
    {
        try {
            //- Simpan Token Firebase Baru
            $check = FcmNotifTokenModel::where('token',$request->firebase_token)->count();
            if(!$check){
                $token = new FcmNotifTokenModel(['token' => $request->firebase_token, 'device' => 2]);
                $user  = $request->user();

                $user->fcmFirebaseToken()->save($token);
            }

            return response()->json([
                "status"    => true,
                "message"   => trans("notification.create.token.success"),
            ],$this->HTTP_CREATE);
        } catch (Throwable $e) {
            return response()->json([
                "status"    => false,
                "message"   => trans("notification.create.token.error")
            ],$this->HTTP_BAD);
        }
    }
}
