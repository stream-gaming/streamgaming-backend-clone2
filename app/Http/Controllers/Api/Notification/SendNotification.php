<?php

namespace App\Http\Controllers\Api\Notification;

use App\Helpers\FirebaseCloudMessaging;
use App\Model\ForumFirebaseToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class SendNotification
{

    public $fcm;

    public function __construct()
    {
        $this->fcm = new FirebaseCloudMessaging();
    }

    public function send($request, $token)
    {
        if ($token == null) {
            return null;
        }

        $count_token = count($token);
        switch ($count_token) {
            case $count_token > 1:
                return $this->multiSendPush($request, $token);
                break;

            default:
                return $this->sendPush($request, $token);
                break;
        }
    }

    public function sendPush($request, $token)
    {
        $this->fcm
            ->to($token[0])
            ->priority('high')
            ->timeToLive(5)
            ->notification([
                'title' => $request['title'],
                'body'  => $request['body'],
            ])
            ->send();
    }

    public function multiSendPush($request, $token)
    {
        $this->fcm
            ->toTopic($token)
            ->priority('high')
            ->timeToLive(5)
            ->data($request['data'])
            ->notification([
                'title' => $request['title'],
                'body'  => $request['body'],
                'icon'  => $request['icon'],
                'image' => $request['image'],
                'tag'   => $request['tag'],
                'click_action'   => config('app.url_frontend'),
                'renotify' => true
            ])
            ->send();
    }

}
