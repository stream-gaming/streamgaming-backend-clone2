<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\TIcketPurchaseHistoryModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @group Ticket
 * @authenticated
 */
class OrderHistoryController extends Controller
{
    public $date_time_format = 'Y-m-d H:i:s';
    public $date_format = 'Y-m-d';
    public function __construct() {
        $this->middleware('jwt');
    }

    /**
     * Ticket Purchase History
     * @authenticated
     *
     * This endpoint serves to retrieve ticket purchase history data
     *
     * @queryParam page retrieve data by page
     * @queryParam limit retrieve data with limit. Default to 15 max 100. Example:5
     * @queryParam order_date ['today','three_days','week'] retrieve data with order_date. Default to all. Example:all
     * @queryParam status ['success','expired','pending'] retrieve data with status. Default to all. Example:all
     * @response status=200 scenario="success" {
            "status": true,
            "data": [
                {
                    "status": "expired",
                    "orderid": "101011",
                    "ticket": "Ticket beli",
                    "qty": 2,
                    "price": 10000,
                    "payment_method": "-",
                    "order_date": "1970-01-01 07:00:00",
                    "payment_date": "1970-01-01 07:00:00",
                    "expired_date": "1970-01-01 07:00:00"
                }
            ],
            "meta": {
                "pagination": {
                    "total": 2,
                    "count": 1,
                    "per_page": 1,
                    "current_page": 2,
                    "total_pages": 2,
                    "links": {
                        "previous": "http://localhost:8000/api/order-history?page=1"
                    }
                }
            }
        }
     */
    public function getMyHistory(Request $request)
    {
        $date = new Carbon();

        $limit         = $request->limit != null ? $request->limit : 15;
        $order_date    = $request->order_date != null ? $request->order_date : 'all';
        $status_trans  = $request->status != null ? $request->status : 'all';

        if ($limit > 100) {
            $limit = 100;
        }

        try {

            $history_data = TIcketPurchaseHistoryModel::with(['ticket'=> function ($q)
            {
                $q->select('id','name');
            }])->where('user_id',Auth::id());

            #Filter by order date
            if ($order_date == 'today') {
                $history_data = $history_data->whereDate('created_at',$date->now()->format($this->date_format));
            } else if ($order_date == 'three_days') {
                $history_data = $history_data->whereDate('created_at','<=',$date->now()->format($this->date_format))->whereDate('created_at','>=',$date->now()->subDays(3)->format($this->date_format));
            } else if ($order_date == 'week') {
                $history_data = $history_data->whereDate('created_at','<=',$date->now()->format($this->date_format))->whereDate('created_at','>=',$date->now()->subDays(7)->format($this->date_format));
            }

            #Filter by transaction status
            if ($status_trans == 'success') {
                $history_data = $history_data->where('status',1);
            } else if ($status_trans == 'pending') {
                $history_data = $history_data->where('status',0)
                        ->Where('expired_at','>',$date->now()->format($this->date_time_format));
            }  else if ($status_trans == 'expired') {
                $history_data = $history_data->where('status',0)
                        ->Where('expired_at','<=',$date->now()->format($this->date_time_format));
            }

            $fetch_data = $history_data->orderBy('created_at','DESC')->paginate($limit);

            $data = [];

            foreach ($fetch_data as $value) {

                $statuses = $date->now()->isAfter($date->parse($value->expired_at)) ? 'expired' : 'pending';
                $status = $value->status ? 'success' : $statuses;
                $data[] = [
                    "status"            => $status,
                    "orderid"           => $value->order_id,
                    "ticket"            => $value->ticket->name,
                    "qty"               => (int) $value->quantity,
                    "price"             => (int) $value->amount,
                    "payment_method"    => $value->payment_method,
                    "order_date"        => date($this->date_time_format, strtotime($value->created_at)),
                    "payment_date"      => date($this->date_time_format, strtotime($value->updated_at)),
                    "expired_date"      => date($this->date_time_format, strtotime($value->expired_at)),
                ];
            }

            $links = [];

            if ($fetch_data->currentPage()<$fetch_data->lastPage()) {
                $links['next'] = $fetch_data->nextPageUrl();
            }

            if ($fetch_data->currentPage()>1) {
                $links['previous'] = $fetch_data->previousPageUrl();
            }

            return response()->json([
                "status"  => true,
                "data" => $data,
                "meta" => [
                    "pagination"        => [
                        "total"         => $fetch_data->total(),
                        "count"         => $fetch_data->perPage(),
                        "per_page"      => $fetch_data->perPage(),
                        "current_page"  => $fetch_data->currentPage(),
                        "total_pages"   => $fetch_data->lastPage(),
                        "links"         => $links
                    ]
                ]
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                "status" => false,
                "message" => "error when retrieve data"
            ], 409);
        }

    }
}
