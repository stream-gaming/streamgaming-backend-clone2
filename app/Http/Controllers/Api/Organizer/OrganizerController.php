<?php

namespace App\Http\Controllers\Api\Organizer;

use App\Helpers\ThrottlesAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateOrganizer;
use App\Model\OrganizerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class OrganizerController extends Controller
{
    /**
     * @group Organizer
     *
     * The APIs for Organizer content
     */
    use ThrottlesAccess;

    //- Max Attempts
    protected $maxAttempts;

    //- Block Time Per Minute
    protected $decayMinutes;
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $request->ip(),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function incrementAttempts(Request $request)
    {
        $this->limiter()->hit(
            $request->ip(),
            $this->decayMinutes() * 60
        );
    }


    /**
     * Organizer - Send
     *
     * @bodyParam first_name string required The first_name of user
     * @bodyParam last_name  string required The last_name of user
     * @bodyParam email string required The email of user
     * @bodyParam company string required The company of user
     * @bodyParam industry string required The industry of user
     * @bodyParam position string required The position of user
     * @bodyParam city string required The city of user
     * @bodyParam phone_number integer required The phone_number of user and min 9 
     * @bodyParam manager_tour string required The manager_tour of user
     *
     * @response 201 {
     *      "status": true,
     *      "message": "Pesan anda berhasil dikirim"
     *  }
     *
     * @response 429 {
     *       "message": "Terjadi Kesalahan, coba beberapa saat lagi"
     *   }
     * 
     * @response 422 {
     *      "message": "Data yang diberikan tidak valid.",
     *       "errors": {
     *           "phone_number": [
*                           "Format phone number tidak valid.",
*                           "Phone number minimal berisi 9 karakter."
*                           ]
     *       }
     *   }
     */
    public function send(ValidateOrganizer $request){

        try{
            $this->decayMinutes = 30;

            $this->maxAttempts  = 5;
    
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return response()->json([
                    "message" => Lang::get('organizer.error.429')
                ], 429);
            }
    
            $Organizer = new OrganizerModel();
            $Organizer->first_name      = $request->first_name;
            $Organizer->last_name       = $request->last_name;
            $Organizer->email           = $request->email;
            $Organizer->company         = $request->company;
            $Organizer->industry        = $request->industry;
            $Organizer->position        = $request->position;
            $Organizer->city            = $request->city;
            $Organizer->phone_number    = $request->phone_number;
            $Organizer->manager_tour    = $request->manager_tour;
            $Organizer->status          = 'pending';
            $Organizer->save();

            $this->incrementAttempts($request);
            return response()->json([
                "status"  => true,
                "message" => Lang::get('organizer.success')
            ], 201);
        }catch(\Exception $e){
            return response()->json([
                "status" => false,
                "message" => $e->getMessage()
            ],409);
        }

    }
}
