<?php

namespace App\Http\Controllers\Api\Comments;

use App\Helpers\MyApps;
use App\Helpers\Redis\RedisHelper;
use App\Helpers\Validation\CheckTournamentSystem;
use App\Http\Controllers\Api\UserNotificationController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateCommentsTournament;
use App\Model\CommentsModel;
use App\Model\Management;
use App\Model\ManagementDetailModel;
use App\Model\PortalNotificationModel;
use App\Model\User;
use App\Traits\CheckImageTraits;
use App\Traits\MyTraits;
use App\Transformer\CommentsListFirebaseTournamentTransform;
use App\Transformer\CommentsListTournamentTransform;
use App\Transformer\CommentsReplyListTournamentTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\ServiceAccount;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Kreait\Firebase\Factory;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Kreait\Firebase\Database\Transaction;

/**
 * @group Comments
 *
 * APIs for the Comments Tournament
 */
class TournamentCommentsController extends Controller
{
    use MyTraits, CheckImageTraits;
    const LIMIT_REPLY = 3;
    const TAKE_REPLY  = 25;

    public function __construct()
    {
        $this->MyApps = new MyApps();

        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));
        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()
            ->getDatabase();
    }

    /**
     * Get List Comments Tournament
     *
     * This API let you to get a Comments Tournament
     *
     * @urlParam $id_tournament required the ID of Tournament. Example: 6033977abddaf
     *
     * @responseFIle status="200" responses/comments.tournament.list.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "message error"
        }
     */
    public function list(Request $request)
    {
        // if ($cache = RedisHelper::getData('tournament:' . $request->id_tournament . ':comment:page:' . $request->page)) {
        //     return $cache;
        // }
        try {
            $is_login = Auth::check();
            $user     = Auth::user();

            $user_profile = $this->firebase
                ->getReference('tournament/comment/user_profile')
                ->getSnapshot()
                ->getValue();

            $comment = collect($this->firebase
                ->getReference('tournament/comment/list/' . $request->id_tournament)
                ->orderByChild('created_at')
                ->getSnapshot()
                ->getValue())->reverse();


            $comment->transform(function ($data, $index) use ($user_profile, $user, $is_login) {
                $c = [
                    'comments_id' => $index,
                    'role'        => $data['role'],
                    'comment'     => $data['comment'],
                    'created_at'  => $data['created_at'],
                    'user'  => [
                        'user_id'   => $data['user']['user_id'],
                        'full_name' => $data['user']['full_name'],
                        'username'  => $data['user']['username'],
                        'picture'   => $user_profile[$data['user']['user_id']],
                    ],
                    'action_menu' => [
                        'delete'  => (!$is_login ? false : ($user->user_id == $data['user']['user_id'] ? true : false))
                    ],
                ];

                $c['replies'] = [];

                if (isset($data['replies'])) {
                    $replies = collect($data['replies']);
                    $c['replies'] = $replies->transform(function ($data, $i) use ($index, $user_profile, $user, $is_login) {
                        $r = [
                            'comments_id' => $i,
                            'role'        => $data['role'],
                            'parent_id'   => $index,
                            'comment'     => $data['comment'],
                            'created_at'  => $data['created_at'],
                            'user'  => [
                                'user_id'   => $data['user']['user_id'],
                                'full_name' => $data['user']['full_name'],
                                'username'  => $data['user']['username'],
                                'picture'   => $user_profile[$data['user']['user_id']],
                            ],
                            'reply_from'  => [
                                'user_id'   => $data['reply_from']['user_id'],
                                'reply_id'  => isset($data['reply_from']['reply_id']) ? $data['reply_from']['reply_id'] : "",
                                'full_name' => $data['reply_from']['full_name'],
                                'username'  => $data['reply_from']['username'],
                                'user_stat' => $data['reply_from']['user_stat'],
                            ],
                            'action_menu' => [
                                'delete' => (!$is_login ? false : ($user->user_id == $data['user']['user_id'] ? true : false))
                            ],
                        ];

                        return $r;
                    })->slice(0, SELF::LIMIT_REPLY)->values();
                }

                $c['more'] = isset($data['replies']) ? (count($data['replies']) > SELF::LIMIT_REPLY ? true : false) : false;

                return $c;
            });

            $limit   = $request->limit != null ? $request->limit : 10;

            $comment = $this->paginateCollection($comment->values(), $limit, $request->page, []);

            $data = fractal()
                ->collection($comment)
                ->transformWith(new CommentsListFirebaseTournamentTransform())
                ->paginateWith(new IlluminatePaginatorAdapter($comment))
                ->toArray();


            $data['meta']['is_login']  = $is_login;

            if (Auth::check()) {
                $data['meta']['user']  = [
                    'user_id'    => $user->user_id,
                    'username'   => $user->username,
                    'picture'    => $this->MyApps->getImage($user->user_id),
                ];
            }

            //     RedisHelper::setData('tournament:' . $request->id_tournament . ':comment:page:' . $request->page, $data);
            return response()->json($data);
        } catch (\Throwable $e) {
            $code = $e->getCode() == 0 ? $this->HTTP_BAD : $e->getCode();
            return response()->json([
                "status"   => false,
                "message"  => $e->getMessage()
            ], $code);
        }
    }

    public function migrateComment()
    {
        $comments = CommentsModel::where('commentable_type', 'tournament')
            ->groupBy('commentable_id')
            ->select(['commentable_id'])
            ->get();


        $comments->transform(function ($data, $index) {
            $comments = CommentsModel::with(['user', 'parent', 'admin'])
                ->where('commentable_id', $data->commentable_id)
                ->where('commentable_type', 'tournament')
                ->whereNull('parent_id')
                ->orderBy('created_at', 'DESC')
                ->get();

            $list = collect(current(fractal()
                ->collection($comments)
                ->transformWith(new CommentsListTournamentTransform)
                ->toArray()));

            $list = $list->mapWithKeys(function ($list, $key) {
                return [$list['comments_id'] => $list];
            });

            return [$data->commentable_id => $list];
        });

        $users_list = CommentsModel::with(['user', 'admin'])
            ->where('commentable_type', 'tournament')
            ->groupBy('user_id')
            ->select(['user_id', 'user_status'])
            ->get();

        $users_list->transform(function ($comment, $index) {
            if ($comment->user_status == 'user') {
                return [$comment->user_id => $this->getImage($comment->user->detail, '0')];
            } else {
                return [$comment->user_id => $this->getImage($comment->admin->detail, '1')];
            }
        });

        $data = [
            'list'         => $comments->collapse(),
            'user_profile' => $users_list->collapse()
        ];

        return $this->firebase
            ->getReference('tournament/comment')
            ->set($data)
            ->getvalue();
    }

    /**
     * Get List Reply Comments Tournament
     *
     * This API let you to get a Comments Tournament
     *
     * @urlParam id_tournament required the ID of Tournament. Example: 6033977abddaf
     * @urlParam comments_id required the ID Comments Parent. Example: 3
     * @bodyParam skip number the number skip comment | jumlah komentar yang mau di skip. Example: 3
     * @bodyParam take number the number take comment | jumlah komentar yang mau di ambil. Example: 10
     *
     * @responseFIle status="200" responses/reply.comments.tournament.list.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "message error"
        }
     */
    public function moreReplies(Request $request)
    {
        $skip    = $request->skip ?? SELF::LIMIT_REPLY;
        $take    = $request->take ?? SELF::TAKE_REPLY;

        $is_login = Auth::check();
        $user     = Auth::user();

        // if ($cache = RedisHelper::getData('tournament:' . $request->id_tournament . ':comment:id:' . $request->comments_id . ':skip:' . $skip . ':take:' . $take)) {
        //     return $cache;
        // }
        try {

            $user_profile = $this->firebase
                ->getReference('tournament/comment/user_profile')
                ->getSnapshot()
                ->getValue();

            $reply_comment = collect($this->firebase
                ->getReference('tournament/comment/list/' . $request->id_tournament . '/' . $request->comments_id . '/replies')
                ->orderByChild('created_at')
                ->getSnapshot()
                ->getValue());

            $data['replies'] = $reply_comment->transform(function ($data, $i) use ($request, $user_profile, $user, $is_login) {
                $r = [
                    'comments_id' => $i,
                    'role'        => $data['role'],
                    'parent_id'   => $request->comments_id,
                    'comment'     => $data['comment'],
                    'created_at'  => $data['created_at'],
                    'user'  => [
                        'user_id'   => $data['user']['user_id'],
                        'full_name' => $data['user']['full_name'],
                        'username'  => $data['user']['username'],
                        'picture'   => $user_profile[$data['user']['user_id']],
                    ],
                    'reply_from'  => [
                        'user_id'   => $data['reply_from']['user_id'],
                        'reply_id'  => isset($data['reply_from']['reply_id']) ? $data['reply_from']['reply_id'] : "",
                        'full_name' => $data['reply_from']['full_name'],
                        'username'  => $data['reply_from']['username'],
                        'user_stat' => $data['reply_from']['user_stat'],
                    ],
                    'action_menu' => [
                        'delete' => (!$is_login ? false : ($user->user_id == $data['user']['user_id'] ? true : false))
                    ],
                ];

                return $r;
            })->slice($skip, $take)->values();

            $data['more'] = (count($reply_comment) > ($skip + $take) ? true : false);

            // RedisHelper::setData('tournament:' . $request->id_tournament . ':comment:id:' . $request->comments_id . ':skip:' . $skip . ':take:' . $take, $data);
            return response()->json($data);
        } catch (\Throwable $e) {
            $code = $e->getCode() == 0 ? $this->HTTP_BAD : $e->getCode();
            return response()->json([
                "status"   => false,
                "message"  => $e->getMessage()
            ], $code);
        }
    }

    /**
     * Add Comments
     *
     * This API let you to post a reply thread
     *
     * @authenticated
     *
     * @urlParam $id_tournament required the Thread URL. Example: 6033977abddaf
     *
     * @bodyParam user_status string | tipe role member [user,admin]. Example: user
     * @bodyParam stream_id string | Wajib di isi jika role member sebagai Admin. Example: 111131
     * @bodyParam parent_id int The id of the Parent Comment / commented on | ID Parent Komentar yang dibalas (Komentar Level 1). Example: 3
     * @bodyParam reply_id string The id of the Reply Comment / commented on | ID Balasan Komentar yang dibalas (Komentar Level 2). Example: 3
     * @bodyParam reply_user_id string The id of the User who replied  / commented on | ID User Komentar yang dibalas (Komentar Level 2). Example: 5f6216e363c3f
     * @bodyParam reply_user_status string The user_status of the User who replied  / commented on | Status User Komentar yang dibalas (Komentar Level 2) [user, admin]. Example: user
     * @bodyParam comment string required The Comment max : 1000 characters | Komentar maksimal 1000 karakter
     * @response scenario="success" status="201" {
            "status": true,
            "message": "Berhasil tambah komentar",
            "data": {
                "user_status": "user",
                "user_id": "5f4719a790543",
                "parent_id": "1",
                "reply_id": "3",
                "comment": "Tidak Punya SIM",
                "commentable_id": "6033977abddaf",
                "commentable_type": "tournament",
                "updated_at": "2021-04-15T02:33:27.000000Z",
                "created_at": "2021-04-15T02:33:27.000000Z",
                "id": 7
            }
        }
     *
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "gagal tambah komentar",
            "error": "error"
        }
     *
     * @response scenario="unprocessable-entity" status="422" {
            "message": "Data yang diberikan tidak valid.",
            "errors": {
                "user_status": [
                    "Jenis User yang dipilih tidak valid."
                ],
                "comment": [
                    "Komentar wajib diisi."
                ]
            }
        }
     */
    public function addReply(ValidateCommentsTournament $request)
    {
        try {
            DB::beginTransaction();

            $user_id = $request->user_status == 'user' ? Auth::user()->user_id : $request->stream_id;
            $new_reply = [];
            $path = 'tournament/comment/list/' . $request->id_tournament;

            if ($request->parent_id == "") {
                if ($request->user_status == 'user') {
                    $user = User::where('user_id', Auth::user()->user_id)
                        ->with(['detail'])
                        ->first();
                    $picture = $this->getImage($user->detail, '0');

                    $new_reply = [
                        'comment'       => $request->comment,
                        'created_at'    => Carbon::now()->toDateTimeString(),
                        'role'          => $request->user_status,
                        'user'          => [
                            'full_name' => $user->fullname,
                            'picture'   => $picture,
                            'user_id'   => $user->user_id,
                            'username'  => $user->username
                        ]
                    ];
                } else {
                    $user = Management::where('stream_id', $request->stream_id)
                        ->with(['detail'])
                        ->first();

                    $picture = $this->getImage($user->detail, '1');

                    $new_reply = [
                        'comment'       => $request->comment,
                        'created_at'    => Carbon::now()->toDateTimeString(),
                        'role'          => $request->user_status,
                        'user'          => [
                            'full_name' => $user->fullname,
                            'picture'   => $picture,
                            'user_id'   => $request->stream_id,
                            'username'  => $user->username
                        ]
                    ];
                }
            } else if (($request->reply_id != "") or ($request->parent_id != "")) {

                $path .= '/' . $request->parent_id . '/replies';

                if ($request->reply_user_status == 'user') {
                    $reply_from = User::where('user_id', $request->reply_user_id)
                        ->with(['detail'])
                        ->first();
                } else {
                    $reply_from = Management::where('stream_id', $request->reply_user_id)
                        ->with(['detail'])
                        ->first();
                }

                if ($request->user_status == 'user') {
                    $user = User::where('user_id', Auth::user()->user_id)
                        ->with(['detail'])
                        ->first();
                    $picture = $this->getImage($user->detail, '0');

                    $new_reply = [
                        'comment'       => $request->comment,
                        'created_at'    => Carbon::now()->toDateTimeString(),
                        'role'          => $request->user_status,
                        'user'          => [
                            'full_name' => $user->fullname,
                            'picture'   => $picture,
                            'user_id'   => $user->user_id,
                            'username'  => $user->username
                        ],
                        'reply_from' => [
                            'reply_id'   => $request->reply_id,
                            'user_id'    => $request->reply_user_id,
                            'full_name'  => $reply_from->fullname,
                            'username'   => $reply_from->username,
                            'user_stat'  => $request->reply_user_status == 'user' ? '0' : '1',

                        ]
                    ];
                } else {
                    $user = Management::where('stream_id', $request->stream_id)
                        ->with(['detail'])
                        ->first();
                    $picture = $this->getImage($user->detail, '1');

                    $new_reply = [
                        'comment'       => $request->comment,
                        'created_at'    => Carbon::now()->toDateTimeString(),
                        'role'          => $request->user_status,
                        'user'          => [
                            'full_name' => $user->fullname,
                            'picture'   => $picture,
                            'user_id'   => $request->stream_id,
                            'username'  => $user->username
                        ],
                        'reply_from' => [
                            'reply_id'   => $request->reply_id,
                            'user_id'    => $request->reply_user_id,
                            'full_name'  => $reply_from->fullname,
                            'username'   => $reply_from->username,
                            'user_stat'  => $request->reply_user_status == 'user' ? '0' : '1',
                        ]
                    ];
                }
            }

            if (!empty($new_reply)) {
                $new_reply['id'] = $this->firebase
                    ->getReference($path)
                    ->push($new_reply)
                    ->getKey();

                $this->firebase
                    ->getReference('tournament/comment/user_profile/' . $user_id)
                    ->set($picture);
            }


            //Used to notify the users once their comment get replied
            $receiver = is_null($request->reply_id) ? $request->parent_id : $request->reply_id;
            $notifyComment = $request->parent_id != null ? $this->notifyComment($user_id, $request->reply_user_id, $request->id_tournament, $request->user_status, $request->reply_user_status) : null;
            DB::commit();

            // RedisHelper::destroyDataWithPattern('tournament:' . $request->id_tournament . ':comment:*');
            return response()->json([
                "status"  => true,
                "message" => trans('message.comment.tournament.create.success'),
                "data"    => $new_reply,
                'user'    => [
                    'user_id'    => $user_id,
                    'username'   => $user->username,
                    'picture'    => $picture,
                ],
            ], $this->HTTP_CREATE);
        } catch (\Throwable $th) {
            DB::rollBack();

            $code = $th->getCode() == 0 ? $this->HTTP_BAD : $th->getCode();
            return response()->json([
                "status"   => false,
                "message"  => trans('message.comment.tournament.create.failed'),
                "error"    => $th->getMessage()
            ], $code);
        }
    }


    /**
     * Delete Comment Tournament
     *
     * This API let you to delete a Comment Tournament
     *
     * @authenticated
     *
     * @urlParam id_tournament required the Thread URL. Example: 6033977abddaf
     * @urlParam comments_id int The id of the Sub threads | ID Comment Tournament . Example: 3
     * @urlParam path string The id of the Sub threads | ID Comment Tournament . Example: 3
     * @bodyParam stream_id string | Wajib di isi jika menggunakan Basic Auth dari portal. Example: 111131
     * @response scenario="failed" status="200" {
            "status": true,
            "message": "Komentar berhasil dihapus"
        }
     * @response scenario="failed" status="400" {
            "status": true,
            "message": "Komentar gagal dihapus"
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Komentar tidak ditemukan"
        }
     * @response scenario="failed" status="401" {
            "status": false,
            "message": "Gagal hapus komentar",
            "error": "Anda tidak berhak menghapus komentar ini"
        }
     */
    public function deleteComment(Request $request)
    {
        try {

            $comments = $this->firebase
                ->getReference('tournament/comment/list/' . $request->id_tournament . '/' . $request->path)
                ->getSnapshot()
                ->getValue();


            if (is_null($comments)) {
                throw new Exception(trans("message.comment.tournament.not-found"), $this->HTTP_NOT_FOUND);
            }

            if (!is_null($request->stream_id)) {
                $admin = Management::find($request->stream_id);
                //for deletion notification
                $this->notifyDelete($request->stream_id, $comments['user']['user_id'], $request->id_tournament);
            }

            if (!isset($admin->stream_id)) {
                if ($comments['user']['user_id'] != Auth::user()->user_id) {
                    throw new Exception(trans("message.comment.tournament.no-access"), $this->HTTP_UNAUTHORIZED);
                }
            }

            $comments = $this->firebase
                ->getReference('tournament/comment/list/' . $request->id_tournament . '/' . $request->path)
                ->remove();

            // RedisHelper::destroyDataWithPattern('tournament:' . $request->id_tournament . ':comment:*');

            return response()->json([
                "status"  => true,
                "message" => trans('message.comment.tournament.delete.success'),
            ], $this->HTTP_OK);
        } catch (\Throwable $th) {
            $code = $th->getCode() == 0 ? $this->HTTP_BAD : $th->getCode();
            return response()->json([
                "status"   => false,
                "message"  => trans('message.comment.tournament.delete.failed'),
                "error"    => $th->getMessage()
            ], $code);
        }
    }

    public function notifyComment($senderId, $receiverId, $tournament_id, $sender_status, $receiver_status)
    {
        try {
            if ($senderId != $receiverId) {
                $getSenderData = $sender_status == 'user' ? User::find($senderId) : Management::find($senderId);
                $getTourData = (new CheckTournamentSystem)->checkAndGet($tournament_id);
                $tourUrl = $getTourData['tournament_system'] == 'Leaderboard' ? 'leaderboard/' . $getTourData['tour_url'] : 'tournament/' . $getTourData['tour_url'];
                $dataNotif = (object) [
                    'receiver_id' => $receiverId,
                    'sender_name' => $sender_status != 'user' ? 'Admin ' . $getSenderData->username : $getSenderData->username,
                    'tour_name' => $getTourData['tournament_name'],
                    'tour_link' => $tourUrl
                ];

                if ($receiver_status == 'user') {
                    $Notification = new UserNotificationController('comment_reply', $dataNotif);
                } else {
                    $notifData = [
                        'sender_name' => $dataNotif->sender_name,
                        'tour_name' => $dataNotif->tour_name

                    ];
                    PortalNotificationModel::create([
                        'time' => strtotime("now"),
                        'access_id' => '1',
                        'message' => Lang::get('notification.comment.reply', $notifData),
                        'is_read' => 0,
                        'updated_at' => strtotime("now")
                    ]);
                }
            }
        } catch (\Exception $e) {

            Log::debug($e->getMessage());
        }
    }

    public function notifyDelete($adminId, $getReceiverData, $tournament_id)
    {
        try {
            $adminData = Management::find($adminId);
            // $getReceiverData = CommentsModel::find($commentId);
            $getTourData = (new CheckTournamentSystem)->checkAndGet($tournament_id);
            $tourUrl = $getTourData['tournament_system'] == 'Leaderboard' ? 'leaderboard/' . $getTourData['tour_url'] : 'tournament/' . $getTourData['tour_url'];

            if ($getReceiverData != $adminId) {
                $dataNotif = (object) [
                    "admin_name" => 'Admin ' . $adminData->username,
                    "receiver_id" => $getReceiverData,
                    'tour_name' => $getTourData['tournament_name'],
                    'tour_link' => $tourUrl
                ];
                $Notification = new UserNotificationController('comment_delete', $dataNotif);
            }
        } catch (\Exception $e) {

            Log::debug($e->getMessage());
        }
    }

    private function sendFirebase($content)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()
            ->getDatabase();

        return $firebase
            ->getReference('tournament/comment/' . $content['tournament_id'])
            ->set($content['data'])
            ->getvalue();
    }
}
