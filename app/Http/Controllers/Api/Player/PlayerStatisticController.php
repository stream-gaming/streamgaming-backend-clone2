<?php

namespace App\Http\Controllers\Api\Player;

use App\Helpers\MyApps;
use App\Helpers\UserDetailMenu;
use App\Http\Controllers\Controller;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\User;
use App\Model\ScoreMatchsModel;
use App\Model\PlayerPayment;
use App\Transformer\PlayerStatisticTransform;
use App\Model\TournamenGroupModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Public
 *
 * APIs for the Public Access
 */
class PlayerStatisticController extends Controller
{



     /**
     * Get User Statistic.
     * <aside class="notice">To see and get user statistic data</aside>
     * @urlParam username required use username as parameter to get user statistic information
     * @responseFile status=200 scenario="success" responses/user.statistic.json
     * @responseFile status=200 scenario="success v2" responses/user.statistic_v2.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "User not found"
        }
     */

    public function index($username, Request $request){

        //get user data
        $getData = User::where('username', $username)->first(['user_id']);
        if(empty($getData)){
            return 	response()->json([
                'status'	=> false,
                'message' => 'User not found'

            ]);
        }
        $userId = $getData->user_id;
        $MyApps = new MyApps;
        //User detail menu
        $userDetailMenu = (new UserDetailMenu)->menu($username, $request->version);

        $games =  IdentityGameModel::where('users_id', $userId)->with('game')->get();

        //-----READ ME ----

        //The place for buffering statistic data
        /*
        the example format data to be buffered
            Leaderboard:
            [
                'jumlah_kill' => 123,
                'total_match' => 22
            ]

            Bracket:
            [
                'jumlah_kill' => 123,
                'death' => 22,
                'assist' => 2,
                'total_match' => 222
            ]
        */
        $dataStatistic = [

        ];
        $i=0;
        foreach($games as $game){
            $isMultisystem = $game->game->system_kompetisi == 'Multisystem' ? true : false;

            $gameId =  $game->game_id;
            if($game->game->system_kompetisi == "Bracket" OR $isMultisystem == true){
                $mathcs = 0;
                $wins = 0;
                //bracket player statistic
                $statistik = ScoreMatchsModel::selectRaw('SUM(tournament_score_matchs.kills) AS kills, SUM(tournament_score_matchs.death) AS death,  SUM(tournament_score_matchs.assist) AS assist')
                                            ->where('kompetisi',$gameId)
                                            ->where('player_id',$userId)
                                            ->first();
                $getTeam   = PlayerPayment::selectRaw('player_payment.id_team')
                                        ->join('tournament' , function($s) use($gameId){
                                        $s->on('tournament.id_tournament', '=', 'player_payment.id_tournament')
                                        ->where('tournament.kompetisi',$gameId);
                                        })
                                    ->where('player_id', '=' , $userId)
                                    ->groupBy('player_payment.id_team')
                                    ->get();
                foreach($getTeam AS $id_teamm){
                    $dataMatchs = TournamenGroupModel::selectRaw('SUM(matchs) AS matchs,SUM(win) AS Wins')
                                    ->Where('id_team' ,$id_teamm['id_team'])
                                    ->first();
                    $wins += $dataMatchs['Wins'];
                    $mathcs += $dataMatchs['matchs'];
                }
                $getStatistic = [
                    "jumlah_kill"   => (int)($statistik['kills'] != null ? $statistik['kills'] : 0),
                    "death"         => (int)($statistik['death'] != null ? $statistik['death'] : 0),
                    "assist"        => (int)($statistik['assist'] != null ? $statistik['assist'] : 0),
                    "total_match"   => (int)($mathcs != null ? $mathcs : 0),
                    "total_win"     => (int)($wins != null ? $wins : 0),
                    "winrate"       => (string) ($mathcs != 0 ? round((($wins / $mathcs) * 100),1) : 0).'%'

                ];
                $dataStatistic[$i]["game_id"] =  $game->game_id;
                $dataStatistic[$i]["game_name"] =  $game->game->name;
                $dataStatistic[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                $dataStatistic[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                $dataStatistic[$i]["system"] = $isMultisystem ? null : "bracket";
                $dataStatistic[$i]["is_multisystem"] = $isMultisystem ? true : false;
                $dataStatistic[$i]["statistic"] = $isMultisystem == false ? $getStatistic : null;
                if($isMultisystem){
                    $dataStatistic[$i]["statistic_multisystem"]['bracket'] = $getStatistic;
                }else{
                    $dataStatistic[$i]["statistic_multisystem"] = null;
                }


                if($isMultisystem == false){
                    $i++;
                }


            }
            if($game->game->system_kompetisi == "LeaderBoards" OR $isMultisystem == true){

                //Leaderboard player statistic
                $statistik = LeaderboardScoreMatchModel::selectRaw('SUM(jumlah_kill) AS total_kill, COUNT(id_player) AS total_match, SUM(CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 ELSE 0 END) AS win')
                                                        ->whereHas('Leaderboard', function($q) use($gameId){
                                                            return $q->where('game_competition',$gameId);
                                                        })
                                                        ->leftJoin('leaderboard_score_sequence','leaderboard_score_sequence.id_score_sequence','=','leaderboard_score_match.id_urutan_team')
                                                        ->where('id_player',$userId)
                                                        ->groupBy('id_player')
                                                        ->first();



                if(!empty($statistik)){
                    $dataStatistic[$i]["game_id"] =  $game->game_id;
                    $dataStatistic[$i]["game_name"] =  $game->game->name;
                    $dataStatistic[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $dataStatistic[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $dataStatistic[$i]["system"] = $isMultisystem ? null : "leaderboard";
                    $dataStatistic[$i]["is_multisystem"] = $isMultisystem ? true : false;
                    $dataStatistic[$i]["statistic"] = $isMultisystem == false ? (new PlayerStatisticTransform)->transform($statistik) : null;
                    if($isMultisystem){
                        $dataStatistic[$i]["statistic_multisystem"]['leaderboard'] =  (new PlayerStatisticTransform)->transform($statistik);
                    }else{
                        $dataStatistic[$i]["statistic_multisystem"] = null;
                    }
                    $i++;
                }else{
                    $dataStatistic[$i]["game_id"] =  $game->game_id;
                    $dataStatistic[$i]["game_name"] =  $game->game->name;
                    $dataStatistic[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $dataStatistic[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $dataStatistic[$i]["system"] = $isMultisystem ? null : "leaderboard";
                    $dataStatistic[$i]["is_multisystem"] = $isMultisystem ? true : false;
                    $dataStatistic[$i]["statistic"] = $isMultisystem == false ?  (object) [
                        "jumlah_kill" => (int) 0,
                        "total_match" => (int) 0,
                        "total_win" => (int) 0,
                        "winrate" => (string) '0%'
                    ] : null;
                    if($isMultisystem){
                        $dataStatistic[$i]["statistic_multisystem"]['leaderboard'] = (object) [
                            "jumlah_kill" => (int) 0,
                            "total_match" => (int) 0,
                            "total_win" => (int) 0,
                            "winrate" => (string) '0%'
                        ];
                    }else{
                        $dataStatistic[$i]["statistic_multisystem"] = null;
                    }
                    $i++;
                }

            }
        }

        return 	response()->json([
            'status'	=> true,
            'active_menu' => 'statistic',
            'menu'     =>   $userDetailMenu,
            'data'	=> $dataStatistic
        ]);



    }
}
