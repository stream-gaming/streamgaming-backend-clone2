<?php

namespace App\Http\Controllers\Api\Player;

use App\Helpers\MyApps;
use App\Helpers\Pagination;
use App\Helpers\Redis\RedisHelper;
use App\Http\Controllers\Controller;
use App\Model\PlayerPayment;
use App\Transformer\MyTournamentTransform;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;


/**
 * @group Account
 * @authenticated
 *
 */
class MyTournamentController extends Controller
{


     /**
     * Get My Tournament.
     * <aside class="notice">To get tournament history that's being followed by the user </aside>
     * @responseFile status=200 scenario="success" responses/mytournament.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "User not found"
        }
     */
    public function getMyTournament()
    {
        $user = Auth::user();
        $player_id = $user->user_id;

        //my tournament cache
        $cacheKey = 'mytournament.'.$player_id;
        $checkAndGetData =  RedisHelper::getData($cacheKey);
        if($checkAndGetData != false){
            return $checkAndGetData;
        }


        $getMyTournament = PlayerPayment::with(['getLeaderboard' => function ($q) {

            $q->where('deleted_at', '=', null);
            $q->leftJoin('game_list', 'game_list.id_game_list', '=', 'leaderboard.game_competition')
                ->get();
        }])
            ->with(['getBracket' => function ($q) {
                $q->leftJoin('game_list', 'game_list.id_game_list', '=', 'tournament.kompetisi')
                    ->get();
            }])
            ->leftJoin('leaderboard_qualification', function ($join) {
                $join->on('player_payment.id_tournament', '=', 'leaderboard_qualification.id_leaderboard')
                    ->where('leaderboard_qualification.qualification_sequence', '=', '1');
            })
            ->leftJoin(DB::raw('(SELECT leaderboard_prizepool.id_leaderboard,SUM(leaderboard_prizepool.prize) AS dynamic_prize_leaderboard FROM leaderboard_prizepool GROUP BY leaderboard_prizepool.id_leaderboard) AS ldp'), 'ldp.id_leaderboard', '=', 'player_payment.id_tournament')
            ->where('status', '!=', 'Cancelled')
            ->where('player_id', $player_id)
            ->get();


        if (!$getMyTournament->isEmpty()) {
            $resource = new Collection($getMyTournament, (new MyTournamentTransform));
            $data     = (new Manager())->createData($resource)->toArray();
            $collection = collect($data['data'])->groupBy('time')->toArray();

            if(!Arr::exists($collection, 'finished')){
                $collection['finished'] = [];
            }
            if(!Arr::exists($collection, 'on_going')){
                $collection['on_going'] = [];
            }
        } else {
            $collection = [
                "on_going" => [],
                "finished" => []
            ];
        }

        $expTime = 2*3600;
        RedisHelper::setData($cacheKey,[
            'status'  => true,
            'data'    => $collection
        ], $expTime);

        return response()->json([
            'status'  => true,
            'data'    => $collection
        ]);
    }

    public function getMyTournamentPortal(Request $req){

        try{
            $player_id = $req->user_id;


            $getMyTournament = PlayerPayment::with(['getLeaderboard' => function($q){

                    $q->where('deleted_at', '=', null);
                    $q->leftJoin('game_list','game_list.id_game_list','=','leaderboard.game_competition')
                        ->get();

                    }])
            ->with(['getBracket' => function($q){
                    $q->leftJoin('game_list','game_list.id_game_list','=','tournament.kompetisi')
                        ->get();
                    }])
            ->leftJoin('leaderboard_qualification', function($join){
                    $join->on('player_payment.id_tournament' , '=' , 'leaderboard_qualification.id_leaderboard')
                    ->where('leaderboard_qualification.qualification_sequence','=','1');
                    })
            ->leftJoin(DB::raw('(SELECT leaderboard_prizepool.id_leaderboard,SUM(leaderboard_prizepool.prize) AS dynamic_prize_leaderboard FROM leaderboard_prizepool GROUP BY leaderboard_prizepool.id_leaderboard) AS ldp'),'ldp.id_leaderboard' , '=' , 'player_payment.id_tournament')
            ->where('status','!=','Cancelled')
            ->where('player_id',$player_id)
            ->get();

            if(!empty($getMyTournament)){

                $resource = new Collection($getMyTournament, (new MyTournamentTransform));
                $data     = (new Manager())->createData($resource)->toArray();
                // dd($data);
                $collection = collect($data['data'])->groupBy('time')->toArray();

            }else{

                $collection = [];

            }


            return 	response()->json([
                'status'	=> true,
                'data'	=> $collection
            ]);
        }catch(\Exception $e){
            return 	response()->json([
                'status'	=> false,
                'data'	=> []
            ]);
        }
}

    public function detailMyTournament($player_id){

        $getMyTournament = PlayerPayment::with(['getLeaderboard' => function($q){

                $q->where('deleted_at', '=', null);
                $q->leftJoin('game_list','game_list.id_game_list','=','leaderboard.game_competition')
                    ->get(['game_list.name']);

            }])
            ->with(['getBracket' => function($q){
                    $q->leftJoin('game_list','game_list.id_game_list','=','tournament.kompetisi')
                        ->get(['game_list.name']);
            }])
            ->leftJoin('leaderboard_qualification', function($join){
                    $join->on('player_payment.id_tournament' , '=' , 'leaderboard_qualification.id_leaderboard')
                    ->where('leaderboard_qualification.qualification_sequence','=','1');
            })
            ->where('status','!=','Cancelled')
            ->where('player_id',$player_id)
            ->paginate(2);

            return 	response()->json([
                'status'	=> true,
                'data'	=> $getMyTournament
            ]);

    }





}
