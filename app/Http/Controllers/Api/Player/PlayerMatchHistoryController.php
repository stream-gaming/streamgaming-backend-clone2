<?php

namespace App\Http\Controllers\Api\Player;

use App\Helpers\MyApps;
use App\Helpers\UserDetailMenu;
use App\Http\Controllers\Controller;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\PlayerPayment;
use App\Transformer\PlayerMatchHistoryTransform;
use App\Http\Controllers\Api\Team\TeamHistoryMatchs;
use App\Model\User;
Use App\Transformer\TeamHistoryTransform;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\Manager;
use Illuminate\Http\Request;


/**
 * @group Public
 *
 * APIs for the Public Access
 */
class PlayerMatchHistoryController extends Controller
{

    /**
     * Get User Match History.
     * <aside class="notice">To see and get user match history data</aside>
     * @urlParam username required use username as parameter to get user match history
     * @responseFile status=200 scenario="success" responses/user.history.json
     * @responseFile status=200 scenario="success v2" responses/user.history_v2.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "User not found"
        }
     */
    public function index($username, $limit = 5, Request $request){



        //get user data
        $getData = User::where('username', $username)->first(['user_id']);

        if(empty($getData)){
            return 	response()->json([
                'status'	=> false

            ]);
        }
        $userId = $getData->user_id;

        //User detail menu
        $userDetailMenu = (new UserDetailMenu)->menu($username, $request->version);

        $games =  IdentityGameModel::where('users_id', $userId)->with('game')->get(['game_id']);
        $MyApps = new MyApps;
        $data = [];
        $i=0;
        foreach($games as $game){
            $gameId =  $game->game_id;
            if($game->game->system_kompetisi == "Bracket"){
                //To make sure there is no error
               try{

                //bracket player history
                $data[$i]["game_id"] =  $game->game_id;
                $data[$i]["game_name"] =  $game->game->name;
                $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                $data[$i]["is_multisystem"] = false;
                $data[$i]["tab_menu"] = "";

                $get = PlayerPayment::selectRaw('player_payment.id_team')
                                        ->join('tournament' , function($s) use($gameId){
                                        $s->on('tournament.id_tournament', '=', 'player_payment.id_tournament')
                                        ->where('tournament.kompetisi',$gameId);
                                        })
                                    ->where('player_id', '=' , $userId)
                                    ->groupBy('player_payment.id_team')
                                    ->get();

                if(!$get->isEmpty()){

                    for ($zi=0; $zi < count($get); $zi++) {
                        $history[$zi] = (new TeamHistoryMatchs)->PlayerMatchs($get[$zi]['id_team']);
                    }
                    $collection = collect($history)->collapse()->all();
                    $data[$i]["history"] = $collection;
                    $data[$i]["history_multisystem"] =
                    $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];
                }else{
                    $data[$i]["history"] = [];
                    $data[$i]["history_multisystem"] =
                    $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];
                }
            }catch(\Exception $e){
                $data[$i]["game_id"] =  $game->game_id;
                $data[$i]["game_name"] =  $game->game->name;
                $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                $data[$i]["is_multisystem"] = false;
                $data[$i]["tab_menu"] = "";
                $data[$i]["history"] = [];
                $data[$i]["history_multisystem"] =
                $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];
            }

                $i++;
            }elseif($game->game->system_kompetisi == "LeaderBoards"){
                //Leaderboard player history
                    try{

                    $history = LeaderboardScoreMatchModel::whereHas('Leaderboard', function($q) use($gameId){
                        return $q->where('game_competition',$gameId);
                    })
                            ->with('Leaderboard')
                            ->with('LeaderboardScoreSequence')
                            ->where('id_player',$userId)
                            ->orderBy('tgl_input','desc')
                            ->limit($limit)
                            ->get();


                    $data[$i]["game_id"] =  $game->game_id;
                    $data[$i]["game_name"] =  $game->game->name;
                    $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $data[$i]["is_multisystem"] = false;
                    $data[$i]["tab_menu"] = "";
                    $data[$i]["history"] = (new PlayerMatchHistoryTransform)->transform($history);
                    $data[$i]["history_multisystem"] =
                    $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];

                    $i++;

                }catch(\Exception $e){
                    $data[$i]["game_id"] =  $game->game_id;
                    $data[$i]["game_name"] =  $game->game->name;
                    $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $data[$i]["is_multisystem"] = false;
                    $data[$i]["tab_menu"] = "";
                    $data[$i]["history"] = [];
                    $data[$i]["history_multisystem"] =
                    $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];

                }

            }else if($game->game->system_kompetisi == "Multisystem"){
                try{
                    $data[$i]["game_id"] =  $game->game_id;
                    $data[$i]["game_name"] =  $game->game->name;
                    $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $data[$i]["is_multisystem"] = true;
                    $data[$i]["tab_menu"] = "";
                    $data[$i]["history"] = [];
                    $data[$i]["history_multisystem"] =
                    $request->version == 'mobile' ? ['bracket' => [],'leaderboard' => []] :[];


                    //for bracket
                    $get = PlayerPayment::selectRaw('player_payment.id_team')
                                            ->join('tournament' , function($s) use($gameId){
                                            $s->on('tournament.id_tournament', '=', 'player_payment.id_tournament')
                                            ->where('tournament.kompetisi',$gameId);
                                            })
                                        ->where('player_id', '=' , $userId)
                                        ->groupBy('player_payment.id_team')
                                        ->get();

                    if(!$get->isEmpty()){

                        for ($zi=0; $zi < count($get); $zi++) {
                            $history[$zi] = (new TeamHistoryMatchs)->PlayerMatchs($get[$zi]['id_team']);
                        }

                        $data[$i]["history_multisystem"]['bracket'] =  (new TeamHistoryTransform)->HistoryBracketPlayer($history);
                    }else{
                        $data[$i]["history_multisystem"]['bracket'] =[];
                    }


                    //for leaderboard
                    $history = LeaderboardScoreMatchModel::whereHas('Leaderboard', function($q) use($gameId){
                        return $q->where('game_competition',$gameId);
                    })
                            ->with('Leaderboard')
                            ->with('LeaderboardScoreSequence')
                            ->where('id_player',$userId)
                            ->orderBy('tgl_input','desc')
                            ->limit($limit)
                            ->get();

                    $data[$i]["history_multisystem"]['leaderboard'] = $history ? (new PlayerMatchHistoryTransform)->transform($history) : [];
                    $i++;

                }catch(\Exception $e){
                    $data[$i]["game_id"] =  $game->game_id;
                    $data[$i]["game_name"] =  $game->game->name;
                    $data[$i]["game_banner"] = $MyApps->cdn($game->game->logo_game_list, 'encrypt', 'game');
                    $data[$i]["game_logo"] = $MyApps->cdn($game->game->logo, 'encrypt', 'game');
                    $data[$i]["is_multisystem"] = true;
                    $data[$i]["tab_menu"] = "";
                    $data[$i]["history"] = [];
                    $data[$i]["history_multisystem"] = ['bracket' => [],'leaderboard' => []];

                }
            }

        }


        return 	response()->json([
            'status'	=> true,
            'active_menu' => 'match',
            'menu'     =>   $userDetailMenu,
            'data'	=> $data
        ]);
    }




}
