<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Redis\RedisHelper;
use App\Model\User;
use App\Model\GameModel;
use App\Traits\GameTraits;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Model\UsersGameModel;
use Illuminate\Http\Response;
use App\Model\IdentityGameModel;
use App\Model\GameUsersRoleModel;
use App\Transformer\GameTransform;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Transformer\GameAllTransform;
use Illuminate\Support\Facades\Redis;
use App\Transformer\GameRoleTransform;
use App\Helpers\Validation\CheckBanned;
use App\Http\Requests\ValidateAddGame;
use League\Fractal\Resource\Collection;
use App\Transformer\GameStaticTransform;
use Illuminate\Support\Facades\Validator;
use App\Transformer\GameTransformNewVersion;
use App\Helpers\SaveIdentityIngameHistory;
use App\Helpers\Validation\CheckTeam;
use App\Transformer\GameDetailOnboardTransformer;
use Exception;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

/**
 * @group  Game
 *
 * APIs for managing game for user or fetch data only
 */
class GameController extends Controller
{
    use GameTraits;
    use SaveIdentityIngameHistory;

    public function __construct()
    {
        $this->middleware('jwt', ['only' => ['addGame', 'editGame']]);
        $this->middleware(\App\Http\Middleware\RefreshTokenManual::class, ['only' => ['getDetail']]);
    }

    /**
     * Get All Game
     *
     * Ambil semua data game
     *
     * @response {
            "data": {
                "PC": [
                    {
                        "id": 604500562,
                        "name": "DOTA",
                        "url": "http://localhost:8000/api/game/dota"
                    },
                    {
                        "id": 1288638409,
                        "name": "Apex Legends",
                        "url": "http://localhost:8000/api/game/apex-legends"
                    }
                ],
                "Mobile": [
                    {
                        "id": 501085427,
                        "name": "PUBG MOBILE",
                        "url": "http://localhost:8000/api/game/pubg-mobile"
                    },
                    {
                        "id": 1086187021,
                        "name": "MOBILE LEGEND",
                        "url": "http://localhost:8000/api/game/mobile-legend"
                    }
                ],
                "Console": []
            }
        }
     */
    public function getAll()
    {
        if (Redis::exists('game.all')) {
            return response()->json([
                'data' => json_decode(Redis::get('game.all'))
            ]);
        }

        $games = GameModel::all('id_game_list','name', 'games_on', 'url');
        $data = GameTransform::list($games);

        Redis::set('game.all', json_encode($data), 'EX', 24 * 3600);

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Get All Game V2
     *
     * Ambil semua data game
     *
     * @queryParam search Search game with name. Example: mobile
     *
     * @response {
            "data": [
                {
                    "id_game_list": 501085427,
                    "name": "PUBG MOBILE",
                    "games_on": "Mobile",
                    "logo": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo1": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "url": "pubg-mobile",
                    "full_url": "http://localhost:8000/api/game/pubg-mobile",
                    "total_player": 1,
                    "total_tournament": 0
                },
                {
                    "id_game_list": 604500562,
                    "name": "DOTA",
                    "games_on": "PC",
                    "logo": "http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GbSHYfXP78C0JzNBw",
                    "background_logo1": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "url": "dota",
                    "full_url": "http://localhost:8000/api/game/dota",
                    "total_player": 1,
                    "total_tournament": 0
                }
            ]
        }
     */
    public function getAllGame(Request $request)
    {

        $cacheKey = 'game--all';
        //get cache
        if(!$request->query('search')){
            $checkAndGetData =  RedisHelper::getData($cacheKey);
            if($checkAndGetData != false){
                return $checkAndGetData;
            }
        }
        //end of get cache

        //after optimalization
        $games = GameModel::when($request->query('search'), function ($query) use ($request) {
            $query->where('name', 'like', "%{$request->search}%");
        })

            ->orderBy('total_player', 'desc')
            ->get();

        $response   = new Collection($games, new GameAllTransform);
        $result = (new Manager)->createData($response)->toArray();

        //set cache
        if(!$request->query('search')){
            $expTime = 24*3600;
            RedisHelper::setData($cacheKey, $result, $expTime);
        }
        //end of set cache
        return $result;
    }

    /**
     * Get Static Game
     *
     * Ambil semua data game
     *
     * @queryParam search Search game with name. Example: mobile
     *
     * @response {
            "data": [
                {
                    "name": "PUBG MOBILE",
                    "logo": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo1": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "url": "pubg-mobile",
                    "full_url": "http://localhost:8000/api/game/pubg-mobile"
                },
                {
                    "name": "DOTA",
                    "logo": "http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GbSHYfXP78C0JzNBw",
                    "background_logo1": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2": "http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "url": "dota",
                    "full_url": "http://localhost:8000/api/game/dota"
                }
            ]
        }
     */
    public function getStaticGame()
    {
        if (Redis::exists('game.static')) {
            return Redis::get('game.static');
        }

        $games = GameModel::all();
        $response   = new Collection($games, new GameStaticTransform);

        $result = (new Manager)->createData($response)->toArray();

        Redis::set('game.static', json_encode($result), 'EX', 3*3600);

        return $result;
    }

    /**
     * Get Simple Game
     *
     * Ambil data game untuk keperluan lain. Contoh penggunaan: pembuatan tim
     *
     */
    public function getSimple()
    {
        if (Redis::exists('game.simple')) {
            return response()->json([
                'data' => json_decode(Redis::get('game.simple'))
            ]);
        }

        $games = GameModel::all('id_game_list','name');
        $data = GameTransform::simple($games);

        Redis::set('game.simple', json_encode($data), 'EX', 3*3600);

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Get Detail Game
     *
     * Ambil data game untuk keperluan lain. Contoh penggunaan: pembuatan tim
     * @urlParam    url required The url of the game. Example: dota
     * @response status=200 scenario="without auth" {
        "data":{
            "is_login":false,
            "detail":{
                "id":604500562,
                "name":"DOTA",
                "content":"&amp;lt;p&amp;gt;The most-played game on &amp;lt;strong&amp;gt;Steam&amp;lt;/strong&amp;gt;. Every day, millions of players worldwide enter battle as one of over a hundred Dota heroes. And no matter if it&amp;amp;amp;amp;#39;s their 10th hour of play or 1,000th, there , always something new to discover. With regular updates that ensure a constant evolution of gameplay, features, and heroes, Dota 2 has truly taken on a life of its own.&amp;lt;/p&amp;gt;\r\n\r\n&amp;lt;div id=&amp;quot;gtx-trans&amp;quot; style=&amp;quot;left:394px; position:absolute; top:58px&amp;quot;&amp;gt;\r\n&amp;lt;div class=&amp;quot;gtx-trans-icon&amp;quot;&amp;gt;&amp;amp;nbsp;&amp;lt;/div&amp;gt;\r\n&amp;lt;/div&amp;gt;",
                "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GbSHYfXP78C0JzNBw",
                "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                "games_on":"PC",
                "total_player":1,
                "total_tournament":0
            },
            "other":[
                {
                    "id_game_list":604500562,
                    "full_url":"http://localhost:8000/api/game/dota",
                    "url":"dota",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GbSHYfXP78C0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"PC",
                    "total_player":4,
                    "total_tournament":10
                },
                {
                    "id_game_list":1086187021,
                    "full_url":"http://localhost:8000/api/game/mobile-legend",
                    "url":"mobile-legend",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GHRH43VOO8C0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"Mobile",
                    "total_player":4,
                    "total_tournament":10
                },
                {
                    "id_game_list":1288638409,
                    "full_url":"http://localhost:8000/api/game/apex-legends",
                    "url":"apex-legends",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx0TKHTtOGZugC0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"PC",
                    "total_player":4,
                    "total_tournament":10
                }
            ]
        }
    }
     * @response status=200 scenario="with auth" {
        "data":{
            "is_login":true,
            "detail":{
                "id":501085427,
                "name":"PUBG MOBILE",
                "content":"&amp;lt;p&amp;gt;&amp;amp;lt;p&amp;amp;gt;&amp;amp;amp;lt;p&amp;amp;amp;gt;&amp;amp;amp;amp;lt;p&amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;p&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;p&amp;amp;amp;amp;amp;amp;gt;PLAYERUNKNOWN&amp;amp;amp;amp;amp;amp;amp;#39;S BATTLEGROUNDS&amp;amp;amp;amp;amp;amp;lt;br /&amp;amp;amp;amp;amp;amp;gt; is officially designed exclusively for mobile devices. Play anywhere, anytime. PUBG MOBILE presents the most exciting free multiplayer action on mobile devices. Falls, prepare weapons, and complete. Survive in the classic 100 player battle, payload mode, fast-paced 4v4 team deathmatch mode, and zombie mode. Enduring is the key, and who is the last to be the winner. During a call of duty, shoot at will!&amp;amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/p&amp;amp;amp;gt;&amp;amp;lt;/p&amp;amp;gt;&amp;lt;/p&amp;gt;",
                "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                "games_on":"Mobile",
                "total_player":4,
                "total_tournament":0,
                "is_added":true
            },
            "other":[
                {
                    "id_game_list":604500562,
                    "full_url":"http://localhost:8000/api/game/dota",
                    "url":"dota",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GbSHYfXP78C0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"PC",
                    "total_player":4,
                    "total_tournament":10
                },
                {
                    "id_game_list":1086187021,
                    "full_url":"http://localhost:8000/api/game/mobile-legend",
                    "url":"mobile-legend",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFjig0GHRH43VOO8C0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"Mobile",
                    "total_player":4,
                    "total_tournament":10
                },
                {
                    "id_game_list":1288638409,
                    "full_url":"http://localhost:8000/api/game/apex-legends",
                    "url":"apex-legends",
                    "logo":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx0TKHTtOGZugC0JzNBw",
                    "background_logo1":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "background_logo2":"http://localhost/Github/Cmfg0/logo/game/s2qEFWrx3mXRH4GOOroC0JzNBw",
                    "games_on":"PC",
                    "total_player":4,
                    "total_tournament":10
                }
            ]
        }
    }
     *
     */
    public function getDetail($url)
    {
        $game = GameModel::where('url', $url)->first();
        if($game){
            $otherGame = GameModel::where('url', '!=', $url)
                ->orderBy('total_player', 'desc')
                ->get();
            $game['other_game'] = $otherGame;
        }


        if ($game == null) {
            return response()->json(['message' => Lang::get('message.game.not')], 404);
        }
        // dd($game);
        return response()->json([
            'data'=> (new GameTransformNewVersion)->transform($game)
        ]);
    }

    /**
     * Get Detail Game Onboarding
     *
     * @urlParam game_url required The url of the game
     * @responseFile status=200 responses/game.detail.success.json
     * @responseFile status=404 responses/game.detail.failed.json
     **/
    public function getDetailOnboarding($game_url)
    {
        try{
            $game = GameModel::where('url', $game_url)
                ->with(['role'])
                ->firstOrFail();

          $resource = new Item($game, new GameDetailOnboardTransformer);
          $manager = (new Manager)->setSerializer(new ArraySerializer());

          return response()->json([
             'status' => true,
             'data' => $manager->createData($resource)->toArray()
            ]);

        }catch(Exception $e){
            return response()->json([
                'status' => false,
                'message' => Lang::get('game.not_found')
            ], 404);
        }

    }

    /**
     * Get Role Game
     *
     * Ambil data game untuk keperluan lain. Contoh penggunaan: pembuatan tim
     * @urlParam url required The url of the game. Example: dota
     * @response status=200 {
            "data": [
                {
                    "role_id": 7,
                    "role_name": "Tanker",
                    "game_id": 302138957
                },
                {
                    "role_id": 8,
                    "role_name": "Support",
                    "game_id": 302138957
                },
                {
                    "role_id": 9,
                    "role_name": "Fighter",
                    "game_id": 302138957
                }
            ]
        }
     **/
    public function getRole($url)
    {
        $game = GameModel::where('url', $url)
            ->with(['role'])
            ->first();

        if ($game == null) {
            return response()->json(['message' => Lang::get('message.game.not')], 404);
        }

        $role = current(fractal()
            ->collection($game->role)
            ->transformWith(new GameRoleTransform())
            ->toArray());

        return response()->json([
            'data' => $role
        ]);
    }


    /**
     * Add Game
     *
     * @authenticated
     *
     * @urlParam  id required Game id. Example:501085427
     * @bodyParam  id_ingame string required ID in Game.
     * @bodyParam  username_ingame string required Username/Nickname in Game.
     * @bodyParam game_role_id array required List of the Role game | list role game [game_role_id].
     * @response {
            "status": true,
            "message": "Berhasil menghubungkan akun permainan"
        }
     * @response 404 {
            "status": false,
            "message": "Permainan tidak ditemukan"
        }
     * @response 400 {
            "status": false,
            "message": "Permainan ini sudah ditambahkan"
        }
     * @response 422 {
            "message": "The given data was invalid.",
            "errors": {
                "id_ingame": [
                    "ID Permainan sudah digunakan."
                ]
            }
        }
     * @response 409 {
            "status": false,
            "message": "Gagal menghubungkan akun permainan",
            "error": ""
        }
     */
    public function addGame(ValidateAddGame $request, $game_id)
    {

        $game = $this->foundGame($game_id);
        if (!is_null($game->original)) {
            return $game;
        }

        try {
            //cek sudah game punya ini
            $checkHaveGame = $this->checkHaveGame($game->id_game_list);

            if ($checkHaveGame > 0) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.game.has')
                ], $this->HTTP_BAD);
            }

            // cek id dan username sudah digunakan
            // $checkUsingIDGame = IdentityGameModel::where('id_ingame', $request->id_ingame)
            //                         ->where('username_ingame', $request->username_ingame)
            //                         ->count();

            // if ($checkUsingIDGame > 0) {
            //     return response()->json([
            //         'status'    => false,
            //         'message'   => Lang::get('message.game.used')
            //     ], $this->HTTP_BAD);
            // }

            // ambil data user game

            DB::beginTransaction();
            $user_game = UsersGameModel::where('users_id', Auth::user()->user_id)->first();

            // proses data user game
            if (empty($user_game->game_id)) {
                $user_game = $game->id_game_list;
            } else {
                $user_game = explode(',', $user_game->game_id);

                array_push($user_game, $game->id_game_list);
                $user_game = implode(',', $user_game);
            }

            // simpan data users game
            $UsersGame = UsersGameModel::find(Auth::user()->user_id);
            $UsersGame->game_id = $user_game;
            $UsersGame->save();

            if ($request->game_role_id != null) {

                $user = User::find(Auth::user()->user_id);

                $array_game_role = $this->createArrayGameRole($request->game_role_id, $game->id_game_list);

                $user->gameRole()->saveMany($array_game_role);
            }

            // simpan data identity game
            IdentityGameModel::create([
                'id'    => uniqid(),
                'users_id'  => Auth::user()->user_id,
                'game_id'   => $game->id_game_list,
                'id_ingame' => $request->id_ingame,
                'username_ingame' => $request->username_ingame
            ]);
            DB::commit();

            RedisHelper::destroyData('my:game:user_id:' . Auth::user()->user_id);

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.game.add.success')
            ], 201);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.add.failed'),
                'error'     => $e->getMessage()
            ], 409);
        }
    }

    /**
     * Update Data Game
     *
     * @authenticated
     *
     *
     * @urlParam  id required Game id. Example:501085427
     * @bodyParam  id_ingame string required ID in Game.
     * @bodyParam  username_ingame string required Username/Nickname in Game.
     * @bodyParam game_role_id array required List of the Role game | list role game [game_role_id].
     * @response {
            "status": true,
            "message": "Berhasil menghubungkan akun permainan"
        }
     * @response 404 {
            "status": false,
            "message": "Permainan tidak ditemukan"
        }
     * @response 400 {
            "status": false,
            "message": "Permainan ini sudah ditambahkan"
        }
     * @response 400 {
            "status": false,
            "message": "ID dan Nickname Permainan sudah digunakan"
        }
     * @response 409 {
            "status": false,
            "message": "Gagal menghubungkan akun permainan",
            "error": ""
        }
     */
    public function editGame(Request $request, $game_id)
    {
        $Identity = IdentityGameModel::where('users_id', $request->user()->user_id)
            ->where('game_id', $request->route()->parameter('id'))
            ->first();

        if (is_null($Identity)) {
            return response()->json([
                'status'    => false,
                'message'   => trans('message.game.not'),
            ], $this->HTTP_NOT_FOUND);
        }

        $rules = [
            'id_ingame'         => 'required|max:255|unique:App\Model\IdentityGameModel,id_ingame,' . $Identity->id,
            'username_ingame'   => 'required|max:255',
            'game_role_id'      => 'array|max:3'
        ];
        $message = [
            'id_ingame.required'        => Lang::get('validation.required'),
            'username_ingame.required'  => Lang::get('validation.required'),
            'id_ingame.max'             => Lang::get('validation.max.string'),
            'username_ingame.max'       => Lang::get('validation.max.string'),
            'game_role_id.max'          => Lang::get('validation.max.role'),
        ];
        $attributes = [
            'id_ingame'         => Lang::get('validation.attributes.id_ingame'),
            'username_ingame'   => Lang::get('validation.attributes.username_ingame'),
            'game_role_id'      => Lang::get('validation.attributes.game_role_id')
        ];
        $validator = Validator::make($request->all(), $rules, $message, $attributes)->validate();

        $game = $this->foundGame($game_id);

        if (!is_null($game->original)) {
            return $game;
        }

        $checkValidation = (new CheckBanned)->isSoloBanned($game_id, null, auth()->user()->user_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */

        try {

            // cek id dan username sudah digunakan
            // validasi dibuat di request form valdiate
            // $checkUsingIDGame = IdentityGameModel::where('users_id', '!=', Auth::user()->user_id)
            //                         ->where('id_ingame', $request->id_ingame)
            //                         ->count();

            // if ($checkUsingIDGame > 0) {
            //     return response()->json([
            //         'status'    => false,
            //         'message'   => Lang::get('message.game.used')
            //     ], $this->HTTP_BAD);
            // }

            // simpan data identity game
            $Identity = IdentityGameModel::where('users_id', Auth::user()->user_id)
                ->where('game_id', $game_id)
                ->first();

            /**
             * Put recently data into array
             */
            $identity_before = [
                'id_ingame' =>  $Identity->id_ingame,
                'username_ingame' => $Identity->username_ingame
            ];

            $Identity->id_ingame = $request->id_ingame;
            $Identity->username_ingame = $request->username_ingame;
            $Identity->save();

            /**
             * Put Request data into array
             */
            $Identity_after = [
                'id_ingame' => $request->id_ingame,
                'username_ingame' => $request->username_ingame
            ];

            if($Identity->wasChanged('id_ingame') || $Identity->wasChanged('username_ingame')){
                $this->saveHistory($identity_before, $Identity_after, $game_id);
            }

            if ($request->game_role_id != null) {
                $user = User::find(Auth::user()->user_id);

                $array_game_role = $this->createArrayGameRole($request->game_role_id, $game->id_game_list);

                GameUsersRoleModel::join('game_role', function ($q) {
                    $q->on('game_role.id', '=', 'game_users_role.game_role_id');
                })
                    ->where('game_role.game_id', $game_id)
                    ->where('user_id', $user->user_id)
                    ->delete();

                $user->gameRole()->saveMany($array_game_role);
            }

            RedisHelper::destroyData('my:game:user_id:' . Auth::user()->user_id);
            $getTeam = (new CheckTeam())->getTeamPlayer($game_id, auth()->user()->user_id);
            if($getTeam){
                RedisHelper::destroyData('team:detail:team_id:' . $getTeam->team_id);
            }

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.game.edit.success'),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.edit.failed'),
                'error'     => $e->getMessage()
            ], 409);
        }
    }

    public function checkHaveGame($game_id)
    {
        return IdentityGameModel::where('users_id', Auth::user()->user_id)
            ->where('game_id', $game_id)
            ->count();
    }

    public function foundGame($game_id, $status = true) // status = true | want return data when success
    {
        $game = GameModel::find($game_id);

        if ($game == null) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.game.not')
            ], 404);
        }

        if ($status) {
            return $game;
        } else {
            return null;
        }
    }
}
