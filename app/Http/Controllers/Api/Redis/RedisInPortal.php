<?php

namespace App\Http\Controllers\Api\Redis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redis;

class RedisInPortal extends Controller
{
   public function getData($key=null){
    if(!is_null($key)){
        if(Redis::exists($key)){
            return response()->json(json_decode(Redis::get($key)));
        }
        return false;
    }
    return false;
    }
    public function setData($key=null, $arrayData=null, $expTimeInSecond=null){
        if(is_null($key) OR is_null($arrayData)) return false;

        if(!is_null($expTimeInSecond)){
            Redis::set($key, json_encode($arrayData), 'EX', $expTimeInSecond);
        }else{
            Redis::set($key, json_encode($arrayData));
        }

    }

    public function destroyData(Request $request)
    {
        $key = $request->url;
        if(is_null($key)) return false;

        Redis::del($key);
    }
}
