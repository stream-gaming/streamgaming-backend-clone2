<?php

namespace App\Http\Controllers\Api\CacheTrigger;

use App\Helpers\Redis\RedisHelper;
use App\Http\Controllers\Controller;
use App\Jobs\CacheTriggerPortalJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CacheTriggerController extends Controller
{
    //
    public function deleteCache(Request $req){
        $validatedData = $req->validate([
            'key' => 'required|max:1000',
            'source' => 'required',
            'type' => 'required|in:single,pattern'
        ]);

        try{

            if($req->source === 'p0Rt4L-F4Hm1'){
                CacheTriggerPortalJob::dispatch($req->key, $req->type);
            }

            return response()->json([
                "status" => true,
                "message" => "cache destroyed successfully"
            ], 200);

        }catch(\Exception $e){

			return response()->json([
                "status" => false,
                "message" => $e->getMessage()
            ], 400);
        }
    }

    public function deleteCacheMulti(Request $req){
        $validatedData = $req->validate([
            'key' => 'required',
            'source' => 'required',
            'type' => 'required|in:multi'
        ]);

        try{

            if($req->source === 'p0Rt4L-F4Hm1'){
                CacheTriggerPortalJob::dispatch(json_decode($req->key), $req->type);
            }

            return response()->json([
                "status" => true,
                "message" => "cache destroyed successfully"
            ], 200);

        }catch(\Exception $e){

			return response()->json([
                "status" => false,
                "message" => $e->getMessage()
            ], 400);
        }
    }
}
