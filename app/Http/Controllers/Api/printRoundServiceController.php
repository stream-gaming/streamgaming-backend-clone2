<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\BracketOneTwoEight;
use App\Model\TournamentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class printRoundServiceController extends Controller
{
    public function printRound(Request $request){


        $id_tournament = $request->id_tournament;

        $tournament = TournamentModel::where('id_tournament',$request->id_tournament)->first();

        if($tournament){
            $id_group = $tournament->id_group;

            if (Redis::exists('print.round'.$id_group)) {
                return json_decode(Redis::get('print.round'.$id_group));
            }

            switch ($request->round) {
                case 'Firstround128':
                    $round = BracketOneTwoEight::selectRaw('team_1.nama_team AS team_1,team_2.nama_team AS team_2,tournament_round128.team1,tournament_round128.team2')
                    ->with(['tournamentGroupPlayer1' =>  function($tg1) use($id_group) {
                        $tg1->where('tournament_group_player.id_group','=',$id_group);
                    }])
                    ->with(['tournamentGroupPlayer2' =>  function($tg2) use($id_group) {
                        $tg2->where('tournament_group_player.id_group','=',$id_group);
                    }])
                    ->leftJoin('tournament as t','tournament_round128.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('tournament_round128.team1','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('tournament_round128.team2','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_round128.id_tournament','=',$id_tournament)

                    ->orderBy('id_round128','asc')
                    ->get();
                    if($round->count() == 0){
                        return false;
                    }
                    else{
                        Redis::set('print.round'.$id_group, json_encode($round), 'EX', (24 * 3600) * 3);
                        return $round;
                    }

                default:
                    # code...
                    break;
            }
        }


    }
}
