<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\TicketBroadcaster;
use App\Model\AllTicketModel;
use App\Model\HistoryBroadcastTicketModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @hideFromAPIDocumentation
 */
class BroadcasterController extends Controller
{
    //
    public function send(Request $request){

       try{
            $ticket_id = $request->ticket_id;
            $target = $request->target;
            $unit = $request->unit;
            $total_given = 0;

            $getTicket = AllTicketModel::where('id', $ticket_id)
                                        ->whereDate('expired','>=', Carbon::now()->toDateString())
                                        ->firstOrFail();

            $maxHas = $getTicket->max_has;
            $ticketName = $getTicket->name;
            //1 is to indicate for player
            if($target == 1){
                //this block of code will be executed incase the competition is specified to certain game
                if($getTicket->competition != 'all'){
                    $getUserData = IdentityGameModel::selectRaw('users_id as user_id')
                                        ->where('game_id', $getTicket->competition)
                                        ->chunk(200, function ($users) use($ticket_id, $unit, $maxHas, &$total_given, $ticketName){
                                            //this will count the total of rows
                                            $total_given += count($users);
                                            foreach ($users as $data) {
                                                //

                                                TicketBroadcaster::dispatch($data, '1', $ticket_id, $unit, $maxHas, $ticketName);
                                            }
                                        });

                //this block of code will be executed incase the competition is specified to all
                }else{
                    $getUserData = User::selectRaw('user_id')
                                        ->where('status', 'player')
                                        ->orderBy('created_at', 'desc')
                                        ->chunk(200, function ($users) use($ticket_id, $unit, $maxHas, &$total_given, $ticketName){
                                             //this will count the total of rows
                                            $total_given += count($users);
                                            foreach ($users as $data) {
                                                //
                                                TicketBroadcaster::dispatch($data, '1', $ticket_id, $unit, $maxHas, $ticketName);
                                            }
                                        });


                }


            }else{

                if($getTicket->competition != 'all'){
                    $getTeamData = TeamModel::selectRaw('team_id, game_id, leader_id')
                                    ->where('game_id', $getTicket->competition)
                                    ->chunk(200, function ($team) use($ticket_id, $unit, $maxHas, &$total_given, $ticketName){
                                        //this will count the total of rows
                                        $total_given += count($team);
                                        foreach ($team as $data) {
                                            //
                                            TicketBroadcaster::dispatch($data, '3', $ticket_id, $unit, $maxHas, $ticketName);
                                        }
                                    });
                }else{
                    $getTeamData = TeamModel::selectRaw('team_id, game_id, leader_id')
                                    ->chunk(200, function ($team) use($ticket_id, $unit, $maxHas, &$total_given, $ticketName){
                                    //this will count the total of rows
                                    $total_given += count($team);
                                    foreach ($team as $data) {
                                        //
                                        TicketBroadcaster::dispatch($data, '3', $ticket_id, $unit, $maxHas, $ticketName);
                                    }
                                });
                }


            }


           DB::beginTransaction();
           $broadcastHistory = new HistoryBroadcastTicketModel;
           $broadcastHistory->id_ticket = $request->ticket_id;
           $broadcastHistory->giving_target = $request->target;
           $broadcastHistory->unit = $request->unit;
           $broadcastHistory->total_given = $total_given;
           $broadcastHistory->distributor = $request->distributor;
           $broadcastHistory->save();
           DB::commit();

            return Response()->json([
                "status" => true,
                "message" => ""
            ],200);

       }catch(\Exception $e){
            DB::rollBack();
            return Response()->json([
                "status" => false,
                "message" => $e
            ],400);

       }

    }
}
