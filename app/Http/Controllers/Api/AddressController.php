<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Model\CityModel;
use App\Model\ProvinceModel;
use App\Http\Controllers\Controller;


/**
 * @group Address/Location
 * 
 */

class AddressController extends Controller
{

	/**
     * Get Data Province
     * @queryParam search Keyword province name to search. Example: sumatera
     * @queryParam id_prov The id of province to get data, you can using this to get data province user when edit profile. Example: 12
     *
     * @response {
		    "data": [
		        {
		            "id_prov": "11",
		            "nama": "Aceh"
		        },
		        {
		            "id_prov": "12",
		            "nama": "Sumatera Utara"
		        },
		        {
		            "id_prov": "13",
		            "nama": "Sumatera Barat"
		        },
		        {
		            "id_prov": "14",
		            "nama": "Riau"
		        },
		        {
		            "id_prov": "15",
		            "nama": "Jambi"
		        }
		    ]
		}
     * 
     *
     */
	public function province(Request $request)
	{
		$data = ProvinceModel::when(request()->query('search'), function ($query) use ($request) {
			$query->where('nama', 'LIKE', "%{$request->search}%");
		})->when($request->id_prov, function ($query) use ($request) {
            $query->where('id_prov', $request->id_prov);
        })->get();

		return response()->json([
			'data'	=>	$data
		]);
	} 


	/**
     * Get Data City
     * @queryParam search Keyword province name to search. Example: sumatera
     * @queryParam id_prov The id of province to get data, you can using this to get data province user when edit profile. Example: 12
     * @queryParam id_city The id of city to get data, you can using this to get data province user when edit profile. Example: 1271
     *
     * @response {
		    "data": [
		        {
		            "id_kab": 1271,
		            "id_prov": "12",
		            "nama": "KOTA MEDAN",
		            "id_jenis": 2
		        },
		        {
		            "id_kab": 3211,
		            "id_prov": "32",
		            "nama": "KAB. SUMEDANG",
		            "id_jenis": 1
		        }
		    ]
		}
     * 
     *
     */
	public function city(Request $request)
	{
		$data = CityModel::when(request()->query('search'), function ($query) use ($request) {
			$query->where('nama', 'LIKE', "%{$request->search}%"); 
		})->when($request->id_prov, function ($query) use ($request) {
            $query->where('id_prov', $request->id_prov);
        })->when($request->id_city, function ($query) use ($request) {
            $query->where('id_kab', $request->id_city);
        })->get();

		return response()->json([
			'data'	=>	$data
		]);
	} 
}
