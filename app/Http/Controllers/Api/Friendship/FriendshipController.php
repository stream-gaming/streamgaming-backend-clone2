<?php

namespace App\Http\Controllers\Api\Friendship;

use App\Model\User;
use App\Http\Requests\ValidateAddFriend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Lang; 

class FriendshipController extends Controller
{

	public function addFriend(Request $request)
	{ 
		$FriendshipHelperController = new FriendshipHelperController;
		$isValidUser = $FriendshipHelperController->checkValidUser($request->users_id);

		// //cek user_id valid
		if (!is_null($isValidUser)) {
			return $isValidUser;
		} 
		
		// cek bukan user id sendiri
		if ($request->users_id == Auth::user()->user_id) {
			return response()->json([
				'status'	=> false,
				'message'	=> Lang::get('message.access.denied')
			], $this->HTTP_BAD);
		}

		//check is friend
		$isFriend = $FriendshipHelperController->isFriend($request->users_id);
		if (!is_null($isFriend)) {
			return $isFriend;
		}


		return response()->json(0);
	}

}
