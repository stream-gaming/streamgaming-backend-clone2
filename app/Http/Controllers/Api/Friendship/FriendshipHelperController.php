<?php

namespace App\Http\Controllers\Api\Friendship;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Lang; 

class FriendshipHelperController extends Controller
{
	public function checkValidUser($user_id)
	{
		$User = User::find($user_id);

		//cek user_id valid
		if ($User == null) {
			return response()->json([
				'status'	=> false,
				'message'	=> Lang::get('message.user.not')
			], $this->HTTP_NOT_FOUND);
		} 
	}

	public function isFriend($user_id)
	{
		$User = User::find(Auth::user()->user_id);
		$isFriend = $User->isFriend($user_id);
		if (!$isFriend) {
			return response()->json([
				'status'	=> true,
				'message'	=> Lang::get('message.friend.is-friend')
			], $this->HTTP_BAD);
		}
	}
 
}
