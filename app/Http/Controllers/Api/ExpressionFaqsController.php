<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateExpressionFaqs;
use App\Model\ExpressionFaqsModel;
use App\Model\FaqsModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group Public
 *
 * APIs for the FAQ
 */
class ExpressionFaqsController extends Controller
{
     /**
     * Expression Faqs.
     * <aside class="notice">APIs Method for Insert data Expression Faqs </aside>
     *  @authenticated
     * @bodyParam id_faqs string required The id of faqs. Example: FAQS0001
     * @bodyParam expression integer like or dislike . No-example

    * @response  {
            "status": true,
            "message": "Expression Faqs berhasil ditambahkan!"
        }

     * @response scenario="failed" status="404" {
            "status": false,
        }
     */
    public function store(ValidateExpressionFaqs $request){
        try{
            $user_id    = Auth::user()->user_id;
            $faqs_id    = $request->input('id_faqs');
            $checkFaqs = $this->checkFaqs($faqs_id);
            if($checkFaqs == false){
                return response()->json([
                    'status'  => false,
                    'message' => trans('FAQ tidak ditemukan')
                    ], $this->HTTP_NOT_FOUND);
            }
            $checkUserExpression = $this->checkUserExpression($faqs_id,$user_id);
            if($checkUserExpression == true){
                return response()->json([
                    'status'  => false,
                    'message' => trans('Anda sudah melakukan request')
                    ], $this->HTTP_NOT_FOUND);
            }
            $expression = $request->input('expression');
            if($expression == 'like'){
                $like = 1;
                $dislike = 0;
            }else if($expression == 'dislike'){
                $like = 0;
                $dislike = 1;
            }
            $data = new ExpressionFaqsModel();
            $data->id_faqs  = $faqs_id;
            $data->users_id = $user_id;
            $data->like     = $like;
            $data->dislike  = $dislike;
            $data->save();
            return response()->json([
                'status'    => true,
                'message'   => 'Expression Faqs berhasil ditambahkan!'
            ]);

        }catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                ], $this->HTTP_NOT_FOUND);
        }
    }
    public function checkFaqs($id){
        try{
            $get_faqs = FaqsModel::where('id_faqs','=',$id)->firstOrFail();
            return true;
        }catch (\Exception $exception) {
            return false;
        }
    }
    public function checkUserExpression($id,$user_id){
         try{
            $get_faqs = ExpressionFaqsModel::where('id_faqs','=',$id)
                                            ->where('users_id','=',$user_id)->firstOrFail();

            return true;
        }catch (\Exception $exception) {
            return false;
        }
    }
}
