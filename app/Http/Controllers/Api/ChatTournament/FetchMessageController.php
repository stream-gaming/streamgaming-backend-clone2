<?php

namespace App\Http\Controllers\Api\ChatTournament;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Model\Management;
use App\Model\MessageTournamentModel;
use App\Model\TeamModel;
use App\Model\TournamentListModel;
use App\Transformer\TournamentMessagesTransform;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Tournament (User already join) | Endpoint untuk diskusi Tournament (pengguna yang sudah join)
 */
class FetchMessageController extends Controller
{
    const LIMIT_MESSAGES = 25;

    public function __construct()
    {
        $this->MyApps = new MyApps();
    }


    /**
     * Get Detail Messages Tournament.
     * <aside class="notice">APIs Method for get Message on Tournament Chat| Endpoint untuk ambil isi pesan di chatroom Tournament </aside>
     * @bodyParam user_status string required The Status of user [0,1] [0 = User : 1 = Admin]
     * @urlParam notif boolean required if message have a notif (for update notif to read) Example : true
     * @responseFile status=200 scenario="success" responses/messages.tournament.messages.success.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Belum ada Pesan"
        }
     * @response scenario="not-member" status="403" {
			"status": false,
			"message": "Anda bukan anggota tim ini"
		}
     */
    public function getMessages(Request $request, $tournament_id)
    {
        try {
            $user     = $request->user();
            $limit    = $request->limit != null ? $request->limit : SELF::LIMIT_MESSAGES;

            $tournament = TournamentListModel::where('list_tournament.id_tournament', $tournament_id);

            if ($request->user_status == '0') {
                $tournament = $tournament->join('player_payment', function ($q) use ($user) {
                    $q->on('player_payment.id_tournament', '=', 'list_tournament.id_tournament');
                    $q->where('player_payment.player_id', '=', $user->user_id);
                });
            }

            $tournament = $tournament->where('statuss', '!=', '5')
                ->firstOr(function () {
                    throw new Exception('message.tournament.not-found');
                });

            if (is_null($tournament)) {
                throw new  Exception(Lang::get('message.tournament.not-member'), 403);
            }

            if ($request->notif == "true") {
                MessageTournamentModel::where('room_id', $tournament->id_tournament)
                    ->where('user_read', 'NOT LIKE', '%' . $user->user_id . '%')
                    ->update([
                        'user_read' => DB::raw("CONCAT(COALESCE(user_read,''),'$user->user_id,')")
                    ]);
            }

            $messages = MessageTournamentModel::select(['id', 'room_id', 'user_id', 'username', 'message', 'attachment', 'message_tournaments.created_at', 'team_id', 'user_status', 'deleted_at'])
                ->orderBy('message_tournaments.created_at', 'desc')
                ->with(['detail_user', 'detail_admin',  'team'])
                ->where("room_id", $tournament->id_tournament)
                ->withTrashed()
                ->paginate($limit);

            $data['header'] = [
                'tournament_id'      => $tournament_id,
                'tournament_name'    => $tournament->tournament_name,
                'tournament_url'     => $tournament->url,
                'tournament_full_url'=> $tournament->format_tournament == 'Leaderboard' ? $this->MyApps->getPathRoute('leaderboard.stage', ['url' => $tournament->url]) : $this->MyApps->getPathRoute('bracket', ['url' => $tournament->url]),
                'logo'               => $this->MyApps->cdn($tournament->card_banner, 'encrypt', 'banner_tournament'),
                'member'             => $tournament->filled_slot
            ];

            //- Cek Apakah dia Leader
            if ($tournament->game_option != 1) {
                $team = TeamModel::where('team_id', $tournament->id_team)
                    ->where('leader_id', $tournament->player_id)
                    ->exists();
            }
            $is_leader = $team ?? false;

            //- Cek Apakah di Admin
            if ($request->stream_id != null) {
                $admin    = Management::find($request->stream_id);
            }
            $is_admin = $admin ?? false;

            $data['status_room'] = (($is_admin != false) ? true : ($tournament->discussion_status == '0' ? false : ($tournament->game_option == 1 ? true : ($is_leader ? true : false))));

            $data["messages"] = fractal()
                ->collection($messages)
                ->transformWith(new TournamentMessagesTransform($is_admin))
                ->paginateWith(new IlluminatePaginatorAdapter($messages))
                ->toArray();

            return response()->json($data, $this->HTTP_OK);
        } catch (\Exception $e) {
            $code = $e->getCode() == 0 ? $this->HTTP_BAD : $e->getCode();
            return response()->json([
                'status'     => false,
                'message'   => $e->getMessage()
            ], $code);
        }
    }
}
//
