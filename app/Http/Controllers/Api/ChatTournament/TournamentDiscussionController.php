<?php

namespace App\Http\Controllers\Api\ChatTournament;

use App\Http\Controllers\Controller;
use App\Model\PinnedDiscussionModel;
use App\Model\PlayerPayment;
use App\Transformer\DiscussionListTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * @group Messages
 * @authenticated
 * Endpoint for tournament discussion list | Endpoint untuk diskusi turnamen
 */
class TournamentDiscussionController extends Controller
{

    const DISCUSSION_LIMIT = 10;
    const PIN_LIMIT = 3;


    /**
     * Get Tournament Discussion List.
     * <aside class="notice">Endpoint for displaying user's tournament discussion list | Endpoint untuk menampilkan daftar diskusi turnamen yang diikuti user </aside>
     * @responseFile status=200 scenario="success" responses/discussion.tournament.list.success.json
     * @response scenario="failed" status="404" {
            "data": null
        }
     */
    public function getList(){
        try{
            $user = Auth::user();
            $player_id = $user->user_id;
            $username = $user->username;

            $q = 'COALESCE(leaderboard.card_banner, tournament.banner_tournament) as logo,';
            $q .= 'COALESCE(leaderboard.id_leaderboard, tournament.id_tournament) as id_tour,';
            $q .= 'COALESCE(leaderboard.tournament_name, tournament.tournament_name) as tour_name,';
            $q .= 'COALESCE(leaderboard.game_option, tournament.game_option) as game_option,';
            $q .= 'COALESCE(leaderboard.tournament_date, tournament.date) as tour_date,';
            $q .= 'pinned_discussion.created_at as pinned_tour,';
            $q .= 'player_payment.date_payment,';
            $q .= 'unread.total_unread';

            $countLastMessage = "SELECT COUNT(id) as total_unread, room_id FROM message_tournaments WHERE (user_read IS NULL OR user_read NOT LIKE '%".$player_id."%') AND deleted_at IS NULL GROUP BY room_id";

            $getMyTournament = PlayerPayment::selectRaw($q)
            ->leftJoin('leaderboard', function($q){
                    $q->on('player_payment.id_tournament', '=', 'leaderboard.id_leaderboard')->where('leaderboard.deleted_at', null);
                    })
            ->leftJoin('tournament', function($q){
                    $q->on('player_payment.id_tournament', '=', 'tournament.id_tournament')->where('tournament.deleted_at', null);
            })
            ->leftJoin('pinned_discussion' , function ($q){
                $q->on('user_id','=', DB::raw('player_payment.player_id COLLATE utf8mb4_unicode_ci'));
                $q->on('room_id', '=', DB::raw('player_payment.id_tournament COLLATE utf8mb4_unicode_ci'));

            })
            ->leftJoin(DB::raw('('.$countLastMessage.') as unread'), 'unread.room_id', '=', DB::raw('player_payment.id_tournament COLLATE utf8mb4_unicode_ci'))
            ->with(['lastMessage' => function($q){
                $q->orderBy('message_tournaments.created_at', 'desc');
                $q->select('room_id','team_id', 'user_id', 'username', 'user_status', 'message', 'attachment', 'message_tournaments.created_at', 'message_tournaments.deleted_at');
            }])
            ->where('player_payment.status','!=','Cancelled')
            ->where('player_payment.player_id',$player_id)
            ->orderBy('tour_date', 'DESC')
            ->get()
            ->sortByDesc('lastMessage.created_at')
            ->sortByDesc('pinned_tour');
            // ->paginate($this::DISCUSSION_LIMIT);

            // $getMyTournament->setCollection($getMyTournament->sortByDesc('lastMessage.created_at'));
            // $getMyTournament->setCollection($getMyTournament->sortByDesc('pinned_tour'));


            if(empty($getMyTournament)){
                return response()->json(['data' => null], 404);
            }


            $data = fractal()
            ->collection($getMyTournament)
            ->transformWith(new DiscussionListTransform($getMyTournament))
            // ->paginateWith(new IlluminatePaginatorAdapter($getMyTournament))
            ->toArray();

            $data['max_pin_limit'] = PinnedDiscussionModel::where('user_id', $player_id)->count() >= 3 ? true : false;
            $data['user'] = [
                'user_id' => $player_id,
                'username' => $username
            ];

            return response()->json($data, $this->HTTP_OK);
        }catch(\Exception $e){

            return response()->json([
                'status' 	=> false,
                'message'   => $e->getMessage()
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Pin tournament discussion
     * This endpoint is used to pin the tournament discussion
     *
     * @bodyParam room_id string required The ID of Tournament
     * @response scenario="success" status="200" {
            "status": true,
            "message": "turnamen berhasil disematkan"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Anda sudah mencapai batas maksimal penyematan!"
        }
     */
    public function pinTournament(Request $request){
        DB::beginTransaction();
        try{

            $user = Auth::user();
            $req = $request->all();
            $player_id = $user->user_id;
            $req['user_id'] = $player_id;

            if(count($req['room_id']) > $this::PIN_LIMIT ){
                throw new Exception(Lang::get('tournament-discussion.pin.pin_failed_limitation'), 400);
            }

            //unpin tournament which is not in array first
            $unpin = $this->unpinTournament($req);
            if($unpin['status'] == false){
                throw new Exception($unpin['message']);
            }

            if(!empty($req['room_id'])){

                if(PlayerPayment::where('id_tournament', $req['room_id'])->where('player_id', $player_id)->where('status','!=','Cancelled')->count() <= 0){
                        throw new Exception(Lang::get('tournament-discussion.pin.pin_failed'), 400);
                }

                $pinnedTour = [];
                foreach($request->room_id as $r_id){
                    $pinnedTour[] = [
                        'room_id' => $r_id,
                        'user_id' => $player_id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                }

                PinnedDiscussionModel::insert($pinnedTour);
            }
            DB::commit();
            return response()->json(['status' => true, 'message' => Lang::get('tournament-discussion.pin.pin_success')], $this->HTTP_OK);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json([
				'status' 	=> false,
				'message'   => $e->getMessage()
			], $e->getCode());
        }

    }

    /**
     * Unpin tournament discussion
     * This endpoint is used to unpin the tournament discussion
     *
     * @bodyParam room_id string required The ID of Tournament
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Berhasil membatalkan penyematan turnamen"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Gagal membatalkan penyematan turnamen"
        }
     */
    public function unpinTournament($request){
        try{

            $player_id = $request['user_id'];
            $req = $request;

            $unpin = PinnedDiscussionModel::where('user_id', $player_id);

            $unpin->delete();
            return ['status' => true, 'message' => Lang::get('tournament-discussion.unpin.unpin_success')];
        }catch(\Exception $e){

            return [
				'status' 	=> false,
				'message'   => $e->getMessage()
			];
        }
    }


}
