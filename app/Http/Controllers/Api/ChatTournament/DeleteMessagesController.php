<?php

namespace App\Http\Controllers\Api\ChatTournament;

use App\Http\Controllers\Controller;
use App\Model\Management;
use App\Model\MessageTournamentModel;
use Illuminate\Http\Request;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;

/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Tournament (User already join) | Endpoint untuk diskusi Tournament (pengguna yang sudah join)
 */
class DeleteMessagesController extends Controller
{


    /**
     * Delete Messages Tournament.
     * <aside class="notice">APIs Method for delete Message on Tournament Chat| Endpoint untuk hapus pesan di chatroom Tournament </aside>
     * @urlParam message_id string required The ID of Messages
     * @bodyParam stream_id string required The ID of Stream_ID Account
     * @response scenario="true" status="200" {
            "status": true,
            "message": "messages delete successfuly"
        }
     * @response scenario="failed" status="500" {
            "status": true,
            "message": "messages delete failed",
            "error": "isi error"
        }
     * @response scenario="not-member" status="409" {
			"status": false,
			"message": "Anda bukan admin"
		}
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'stream_id'  => 'required|exists:management_access,stream_id',
            'message_id' => 'required|exists:message_tournaments,id'
        ]);

        try {
            $message = MessageTournamentModel::find($request->message_id);

            $result = (object) [
                'chat' => (object) [
                    'room_id'    => $message->room_id,
                    'message_id' => $message->id,
                    'is_delete'  => true,
                ],
            ];

            $this->sendFirebase($result);

            $message->delete();

            return response()->json([
                'status'  => true,
                'message' => trans('message.delete.success')
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => false,
                'message' => trans('message.delete.failed'),
                'error'   => $th->__toString()
            ],500);
        }
    }

    private function sendFirebase($content)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()->getDatabase();

        return $firebase->getReference('tournament-delete/' . $content->chat->room_id)
            ->set($content)->getvalue();
    }
}
