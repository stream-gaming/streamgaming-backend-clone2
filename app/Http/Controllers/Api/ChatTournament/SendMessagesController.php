<?php

namespace App\Http\Controllers\Api\ChatTournament;

use App\Events\MessageTeam;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\MyApps;
use App\Helpers\Validation\CheckTeam;
use App\Http\Controllers\Api\Notification\SendNotification;
use App\Http\Requests\SendMessagesTournamentRequest;
use App\Model\TeamModel;
use App\Model\ForumFirebaseToken;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\Management;
use App\Model\MessageTournamentModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentListModel;
use App\Model\User;
use App\Services\UploadService;
use App\Traits\TournamentMessagesTraits;
use App\Transformer\TournamentMessagesTransform;
use Exception;
use Illuminate\Support\Facades\Lang;
use Intervention\Image\ImageManagerStatic as Image;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;


/**
 * @group Messages
 * @authenticated
 * APIs for the Messages Tournament | Endpoint untuk pesan Tournament
 */
class SendMessagesController extends Controller
{
    use TournamentMessagesTraits;

    public function __construct()
    {
        $this->MyApps    = new MyApps();
    }

    /**
     * Send Message Tournament
     *
     * @authenticated
     * This API let you to send message for your Tournament
     * @bodyParam tournament_id string required The ID of Tournament
     * @bodyParam image_only string required_if user just send a picture [0,1]. Example : 0
     * @bodyParam user_status string required The Status of user [0,1] [0 = User : 1 = Admin]
     * @bodyParam message string required the text message
     * @bodyParam stream_id string if User status Admin
     * @bodyParam xxx string required Online User
     * @bodyParam attach file the image attachment. The extension must be one of JPG, JPEG, PNG.
     *
     * @response 200 {
            "message": "OK",
            "data": {
                "chat": {
                    "room_id": "36516d38e375cb4ff9fac5001977d4f9401aad12",
                    "user": {
                        "username": "Jun_Darma4024",
                        "team": "HERIDITAIRE",
                        "picture": "https://lh5.googleusercontent.com/-07SA_GhKSrY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckeWxx3jL1E354vtPgokDZteQFRnQ/s96-c/photo.jpg"
                    },
                    "is_me": true,
                    "message": "Okeke Gatau Aha",
                    "created_at": "2021-04-24 10:47:40",
                    "attachment": null,
                    "team_id": "5ff6996bcb700",
                    "logo": "https://cmfg.streamgaming.id/banner/tournament/z7Vryf9X-21joWBZm-3Ur9SLeQ"
                }
            }
        }
     * @response scenario="not-member" status="403" {
			"status": false,
			"message": "Anda bukan anggota dari Team yang telah join"
		}
     * @response scenario="discussion-not-active" status="403" {
			"status": false,
			"message": "Room diskusi turnamen tidak aktif"
		}
     * @response scenario="not-leader" status="403" {
			"status": false,
			"message": "Anda bukan Leader"
		}
     */
    public function send(SendMessagesTournamentRequest $request)
    {
        try {
            switch ($request->user_status) {
                case '0':
                    $user    = User::where('user_id', Auth::user()->user_id)
                        ->with(['detail'])
                        ->first();

                    $user_id = $user->user_id;

                    break;

                case '1':
                    $user    = Management::where('stream_id', $request->stream_id)
                        ->with(['detail'])
                        ->first();

                    $user_id = $user->stream_id;

                    break;
            }

            $tournament = TournamentListModel::where('list_tournament.id_tournament', $request->tournament_id);

            if ($request->user_status == '0') {
                $tournament = $tournament->join('player_payment', function ($q) use ($user) {
                    $q->on('player_payment.id_tournament', '=', 'list_tournament.id_tournament');
                    $q->where('player_payment.player_id', '=', $user->user_id);
                });
            }

            $tournament = $tournament->where('statuss', '!=', '5')
                ->firstOr(function () {
                    throw new Exception('message.tournament.not-found');
                });

            if (is_null($tournament)) {
                throw new  Exception(Lang::get('message.tournament.not-member'), 403);
            }

            if ($request->user_status != '1') {
                if ($tournament->discussion_status == '0') {
                    throw new  Exception(Lang::get('message.tournament.discussion.not-active'), 403);
                }
            }

            if ($tournament->game_option != 1 and $request->user_status == '0') {
                $team     = TeamModel::find($tournament->id_team);
                $isLeader = CheckTeam::isLeader($team);
                if (is_null($isLeader)) {
                    throw new  Exception(Lang::get('message.tournament.not-leader'), 403);
                }
            }

            if ($request->attach != null) {
                $attachment = $this->upload($request);
                if ($attachment == false) {
                    throw new Exception(Lang::get('image.fail.upload'), 500);
                }
            } else {
                $attachment = null;
            }

            DB::beginTransaction();

            $message = MessageTournamentModel::create([
                'room_id'       => $tournament->id_tournament,
                'user_id'       => $user_id,
                'username'      => $user->username,
                'user_status'   => $request->user_status,
                'team_id'       => $tournament->game_option == 1 ? '' : $tournament->id_team,
                'message'       => $request->message,
                'attachment'    => $attachment,
                'user_read'     => $request->user_status == 0 ? $request->xxx . ',' : $user_id . ','  //user yang sedang online dan berada didalam room chat ini secara ototmatis dianggap sudah membaca pesan
            ]);

            DB::commit();

            switch ($request->user_status) {
                case '0':
                    $message = $message->load('user');
                    break;

                case '1':
                    $message = $message->load('admin');
                    break;
            }

            $result = (object) [
                'chat' => (object) [
                    'room_id'   => $message->room_id,
                    'message_id'=> $message->id,
                    'tournament_name' => $tournament->tournament_name,
                    'user'      => [
                        'username' => $message->user->username ?? $message->admin->username,
                        'team'     => $team->team_name ?? '-',
                        'picture'  => $this->getImage($user->detail, $request->user_status),
                        'status'   => $request->user_status == 0 ? "user" : "admin",
                    ],
                    'is_me'      => false,
                    'message'    => $message->message,
                    'created_at' => date('Y-m-d H:i:s', strtotime($message->created_at)),
                    'attachment' => $message->attachment,
                    'team_id'    => $message->team_id,
                    'logo'       => $this->MyApps->cdn($tournament->card_banner, 'encrypt', 'banner_tournament'),
                ],
            ];

            $this->sendFirebase($result);

            // broadcast(new MessageTeam($result))->toOthers();

            switch ($tournament->format_tournament) {
                case 'Bracket':
                    $participant = collect(TournamentGroupPlayerModel::where('id_group', $tournament->id_group)
                        ->get());

                    break;

                case 'Leaderboard':
                    $participant = collect(LeaderboardTeamPlayerModel::where('id_leaderboard', $tournament->id_tournament)
                        ->get());
                    break;
            }

            $array_player = $participant->map(function ($item) use ($tournament) {
                return $item[$tournament->format_tournament == 'Bracket' ? 'id_player' : 'id_teamplayer_leaderboard'];
            })->toArray();

            $key = array_search($user->user_id, $array_player);

            unset($array_player[$key]);

            $token = ForumFirebaseToken::whereIn('user_id', $array_player)->get();

            $all_token = $token->map(function ($item) {
                return $item['token'];
            })->toArray();

            $push = new SendNotification;

            $messages = $message->message == null ? "mengirim gambar" : $message->message;

            if (count($all_token)) {
                $push->multiSendPush([
                    'title' => "Stream Gaming",
                    'body'  => $user->username . '-' . ($team->team_name ?? '-') . " : " . $messages,
                    'image' => $message->attachment,
                    'icon'  => $this->MyApps->cdn($tournament->card_banner, 'encrypt', 'banner_tournament'),
                    'data'  => collect($result)->toArray(),
                    'tag'   => $request->team_id
                ], $all_token);
            }

            //requestnya orang front end
            $result->chat->is_me = true;
            return response()->json([
                "message" => "OK",
                "data"    => $result
            ], 201);
        } catch (\Throwable $th) {
            DB::rollBack();
            $code = $th->getCode() ?? '404';

            return response()->json([
                "status"  => false,
                "message" => $th->getMessage()
            ], $code);
        }
    }

    public function upload($request)
    {
        $upload = new UploadService;

        $image = $request->file('attach');

        $result = $upload->upload('public/content/chat/tournament', $image, null, [
            'options' => 'resize-ratio',
            'width'   => Image::make($image)->width(),
            'height'  => Image::make($image)->height()
        ]);

        if (!is_string($result)) {
            $result = false;
        }

        return $result;
    }

    private function sendFirebase($content)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()->getDatabase();

        return $firebase->getReference('tournament/' . $content->chat->room_id)
            ->set($content)->getvalue();
    }
}
