<?php

namespace App\Http\Controllers\Api;

use League\Fractal\Manager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use League\Fractal\Resource\Collection;
use App\Transformer\SponsorshipTransform;


/**
 * @group Sponsorship
 *
 *
 */
class SponsorshipController extends Controller
{

	/**
	 * Get Data Sponsorship
	 * @response {
		    "data": [
		        {
		            "title": "INDIHOME",
		            "image": "http://localhost/Github/Cmfg0/logo/sponsorship/s2qEFjzz0TWDHoKAO-gC0JzNBw",
		            "content": "&lt;p&gt;Indonesia&amp;nbsp;Digital&amp;nbsp;HOME&amp;nbsp;(disingkat&amp;nbsp;IndiHOME)&amp;nbsp;adalah&amp;nbsp;salah&amp;nbsp;satu&amp;nbsp;produk&amp;nbsp;layanan&amp;nbsp;dari&amp;nbsp;PT&amp;nbsp;Telekomunikasi&amp;nbsp;Indonesia&amp;nbsp;berupa&amp;nbsp;paket&amp;nbsp;layanan&amp;nbsp;komunikasi&amp;nbsp;dan&amp;nbsp;data&amp;nbsp;seperti&amp;nbsp;telepon&amp;nbsp;rumah&amp;nbsp;(voice),&amp;nbsp;internet&amp;nbsp;(Internet&amp;nbsp;on&amp;nbsp;Fiber&amp;nbsp;atau&amp;nbsp;High&amp;nbsp;Speed&amp;nbsp;Internet),&amp;nbsp;dan&amp;nbsp;layanan&amp;nbsp;televisi&amp;nbsp;interaktif&amp;nbsp;(USee&amp;nbsp;TV&amp;nbsp;Cable,&amp;nbsp;IP&amp;nbsp;TV).&lt;/p&gt;",
		            "link": "www.indihome.co.id"
		        },
		        {
		            "title": "STREAM UNIVERSE",
		            "image": "http://localhost/Github/Cmfg0/logo/sponsorship/s2qEFjzz0TKBTtaDarsC0JzNBw",
		            "content": "&lt;p&gt;Stream-Universe adalah kumpulan anak muda milenial Indonesia yang berdiri sejak tahun 2017 di Medan &amp;ndash; Sumatera Utara yang fokus dalam pengembangan komunitas milenial anak bangsa di seluruh Indonesia . Selain itu Stream-Universe juga berpengalaman dan aktif dalam mengadakan event yang berkatain dengan Gaming dan event J-Pop..&lt;/p&gt;",
		            "link": "www.stream-universe.id"
		        },
		        {
		            "title": "STREAM CASH",
		            "image": "http://localhost/Github/Cmfg0/logo/sponsorship/s2qEFjzz0T3WHIzTa7oC0JzNBw",
		            "content": "&lt;p&gt;Stream cash adalah matauang digital yang haya bisa digunakan pada stream&lt;/p&gt;\r\n",
		            "link": "STREAM CASH"
		        }
		    ]
		}
	 */
	public function get()
	{
        if (Redis::exists('sponsorship')) {
            return (string) Redis::get('sponsorship');
        }

		$sponsor = DB::table('sponsors')->get();
		$response   = new Collection($sponsor, new SponsorshipTransform);

        $data = (new Manager)->createData($response)->toArray();

        Redis::set('sponsorship', json_encode($data), 'EX', 24 * 3600);

        return $data;
	}

}
