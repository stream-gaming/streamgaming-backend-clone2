<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Notification;
use App\Helpers\Ticket\TeamTicketFactory;
use App\Helpers\Ticket\TicketRole;
use App\Helpers\Ticket\TicketValidation;
use App\Helpers\Ticket\UserTicketFactory;
use App\Helpers\Validation\CheckTeam;
use App\Http\Controllers\Controller;
use App\Http\Requests\BuyTicketRequest;
use App\Http\Requests\XenditCallbackRequest;
use App\Model\AllTicketModel;
use App\Model\HistoryTicketsModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\LeaderboardTicketPrizeModel;
use App\Model\PlayerPayment;
use App\Model\TIcketPurchaseHistoryModel;
use App\Model\TournamentModel;
use App\Model\TournamentTicketSequenceModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * @group Tournament
 *
 * APIs for the Tournament Leaderboard
 */
class TicketServiceController extends Controller
{
    //
    public function getUserTicket(Request $request){

        $competition = $request->competition;
        $mode = $request->mode;

        $get = AllTicketModel::where(function($q) use($competition){
            $q->where('competition', $competition)
              ->orWhere('competition', 'all');
        })
        ->whereDate('expired','>=', Carbon::now()->toDateString());

        if($mode == 1){
            $get = $get->where('mode', 1);
        }else if($mode == 2){
            $get = $get->where('mode', 2);
        }else if($mode == 3 OR $mode == 4){
            $get = $get->where('mode', 3);
        }

        $get = $get->get(['id','name', 'competition']);


        return response()->json($get);

    }
    public function getGiftTicket(Request $request){
        $competition = $request->competition;
        $mode = $request->mode;

        $get = AllTicketModel::where(function($q) use($competition){
            $q->where('competition', $competition)
            ->orWhere('competition', 'all');
        })
        ->Where('sub_type', 2)
        ->whereDate('expired','>=', Carbon::now()->toDateString());


        if($mode == 1){
            $get = $get->where('mode', 1);
        }else if($mode == 2){
            $get = $get->where('mode', 2);
        }else if($mode == 3 OR $mode == 4){
            $get = $get->where('mode', 3);
        }

        $get = $get->get(['id','name', 'competition']);


        return response()->json($get);
    }


    /**
     * Ticket Payment
     *
     * This is an api specification for ticket payment.
     * @authenticated
     * @bodyParam ticket_id string required The id of ticket. Example: 12345
     * @bodyParam payment_method string required cash or point choosen by user (only required for premium ticket).
     * @bodyParam pin string required (User's stream cash pin)
     * @responseFile status=200 scenario="Payment Success" responses/ticket.success.json
     * @responseFile status=400 scenario="Failed" responses/ticket.failed.json
     * @responseFile status=422 scenario="Failed " responses/ticket.failed2.json
     */
    public function buyTicket(BuyTicketRequest $request){

        DB::beginTransaction();
        try{
                    //ticket validation
                    $ticketValidation = new TicketValidation;
                    $getTicketInfo  = $ticketValidation->ticketValidation($request->ticket_id);
                    $ticketMode = '';
                    $quantity = $request->quantity;
                    $gameId = $request->game_id;

                    //type of payment
                    if($request->payment_method == 'point'){
                        throw new Exception('Saat ini pembelian menggunakan metode point belum tersedia!', 422);
                    }

                    //This condition will be executed incase ticket data not found
                    if(!$getTicketInfo){
                        throw new Exception('Terjadi kesalahan, segarkan halaman! (invalid ticket)', 400);
                    }

                    //This condition will be executed incase ticket tyoe out of the premium type
                    if($getTicketInfo->type != 2 ){
                        throw new Exception('Terjadi kesalahan, segarkan halaman! (invalid ticket 2)', 400);
                    }


                //Check if this this ticket has reached max given limit or not
                if($getTicketInfo->max_given != 0){
                    if($getTicketInfo->total_given >= $getTicketInfo->max_given){
                        throw new Exception('Tiket sudah habis terjual', 400);
                    }
                }

                //This condition is to validate ticket rules of user or team
                if($getTicketInfo->mode == 1){
                        //user validation
                        $userValidation = $ticketValidation->isThisUserAllowed($getTicketInfo, Auth::user()->user_id, $quantity);
                        $ticketMode = 'solo';

                }else if($getTicketInfo->mode == 2 OR $getTicketInfo->mode == 3){

                        //validate user's status on team
                        $checkTeam = (new CheckTeam)->isLeaderOnTeam($gameId, Auth::user()->user_id);
                        if(!$checkTeam) throw new Exception("Hanya ketua tim yang dapat membeli tiket", 401);

                        //team validation
                        $userValidation = $ticketValidation->isThisTeamAllowed($getTicketInfo, $checkTeam->team_id, $quantity);
                        $ticketMode = 'team';
                }

                    if(!$userValidation['status']){
                        throw new Exception($userValidation['message'], 500);
                    }


                        $host=config('app.streamcash_payment_api');
                        $username=config('app.streamcash_payment_username');
                        $password=config('app.streamcash_payment_pwd');


                        // Make Post Fields Array
                        $requestData = [
                            'user_id' => Auth::user()->user_id,
                            'pin' => $request->pin,
                            'description' => 'Pembelian tiket turnamen dengan id '.$request->ticket_id.' sejumlah '.$quantity.' unit',
                            'amount' => ($getTicketInfo->market_value * $quantity),
                        ];

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $host,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_USERPWD => $username.":".$password,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30000,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $requestData,
                            CURLOPT_HTTPHEADER => array("Accept: application/json"),
                        ));

                        $response = curl_exec($curl);
                        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        $err = curl_error($curl);
                        curl_close($curl);

                        if($err) {
                            throw new Exception('Terjadi kesalahan, segarkan halaman!', 500);
                        } else {

                            if($statusCode != '200'){
                                $resp = json_decode($response);
                                throw new Exception($resp->message, 400);
                            }

                            //add ticket process
                            if($ticketMode == 'solo'){
                                $user = (new UserTicketFactory)->addTicketV2(Auth::user()->user_id, $request->ticket_id, TicketRole::BUY_SOURCE, $quantity);
                                if($user == false) throw new Exception('Pembelian tiket gagal', 500);
                            }else if($ticketMode == 'team'){
                                $team = (new TeamTicketFactory)->addTicketV2($checkTeam->team_id, $request->ticket_id, TicketRole::BUY_SOURCE, $quantity);
                                if($team == false) throw new Exception('Pembelian tiket gagal', 500);
                            }

                            $notifData = (object) [
                                'users_id' => Auth::user()->user_id,
                                'unit' => $quantity,
                                'ticket_name' => $getTicketInfo->name,
                                'prefix' => $request->payment_method != 'point' ? 'Rp' : '',
                                'amount' => ($getTicketInfo->market_value * $quantity),
                                'payment_method' => $request->payment_method == 'cash' ? 'Stream Cash' : 'Stream Point'
                            ];
                            $notif = new UserNotificationController('ticket_payment', $notifData);

                            //insert invoice to our system
                            $purchaseInv = new TicketPurchaseHistoryModel;
                            $purchaseInv->order_id = '#'.strtotime(Carbon::now());
                            $purchaseInv->xendit_invoice_id = '#'.strtotime(Carbon::now());
                            $purchaseInv->user_id = Auth::user()->user_id;
                            if($ticketMode == 'team'){
                                $purchaseInv->team_id = $checkTeam->team_id;
                            }
                            $purchaseInv->ticket_id = $request->ticket_id;
                            $purchaseInv->quantity = $quantity;
                            $purchaseInv->amount = ($getTicketInfo->market_value * $quantity);
                            $purchaseInv->provider = 'Streamcash';
                            $purchaseInv->payment_method = 'STREAM CASH';
                            $purchaseInv->status = 1;
                            $purchaseInv->expired_at = Carbon::now();
                            $purchaseInv->save();

                        }
            //commit all process
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Pembelian tiket berhasil!'

            ], $statusCode);
        }catch(\Exception $e){
            DB::rollBack();
            $statusCode = $e->getCode() == 0 ? 500 : $e->getCode();
            $message = $e->getMessage();

            //save error log
            Log::debug($message);

            return Response()->json([
                'status' => false,
                'message' => $message
            ], $statusCode);
        }

    }

    public function updateTicketInvStatus(XenditCallbackRequest $req){

        DB::beginTransaction();
        try{

            //validate payment status from xendit
            $paymentStatus = $req->status == 'PAID' ? 1 : 0;
            if($paymentStatus == 0) throw new Exception('Status not paid');

            //Check if this invoice exists in our database
            $getInv = TIcketPurchaseHistoryModel::where('order_id', $req->external_id)->where('status', 0)->firstOrFail();
            $amount = $getInv->amount;
            if($amount != $req->paid_amount) throw new Exception('Paid amount not match');
            $ticket_id = $getInv->ticket_id;
            $quantity = $getInv->quantity;

            $getTicket = AllTicketModel::where('id', $getInv->ticket_id)->firstOrFail();
            //check if ticket quantity is enough for this transaction
            if($getTicket->max_given != 0){
                if($getTicket->max_given - ($getTicket->total_given + $quantity) < 0){
                    throw new Exception('Failed to distribute ticket, stok ticket has reached the limit');
                }
            }
            $ticket_mode = $getTicket->mode;
            //ticket source
            $source=TicketRole::BUY_SOURCE;

            //Distribute ticket to payer
            if($ticket_mode == 1){
                $giveTicket = (new UserTicketFactory)->addTicketV2($getInv->user_id, $ticket_id, $source, $quantity);
            }else if($ticket_mode == 2 OR $ticket_mode == 3){
                $giveTicket = (new TeamTicketFactory)->addTicketV2($getInv->team_id, $ticket_id, $source, $quantity);
            }
            if($giveTicket == false) throw new Exception('Failed to distribute ticket');

            //Update invoice status
            TIcketPurchaseHistoryModel::where('order_id', $req->external_id)->update([
                'status' => $paymentStatus,
                'payment_method' => $req->payment_channel,
                'updated_at' =>  Carbon::parse($req->paid_at)->setTimezone('Asia/Jakarta')
            ]);

            //send notification to the user
            $notifData = (object) [
                'users_id' => $getInv->user_id,
                'invoice_id' => $getInv->order_id,
                'link' => 'myticket/payment'
            ];
            $notif = new UserNotificationController('ticket_payment_xendit_paid', $notifData);

            //commit all process
            DB::commit();

            return response()->json([
                'status' => true,
                'message'=> 'Hi, we have received this event successfully',
            ], 200);
        }catch(\Exception $e){
            DB::rollBack();
            $statusCode = $e->getCode() == 0 ? 500 : $e->getCode();
            $message = $e->getMessage();

            //save error log
            Log::debug($message);

            return Response()->json([
                'status' => false,
                'message' => $message
            ], $statusCode);
        }


    }

    public function cancelTicketTournament($id_tournament){

        try{
            $getLeaderboard = LeaderboardModel::where('id_leaderboard', $id_tournament)->firstOrFail(['id_leaderboard', 'tournament_form', 'ticket', 'is_new_ticket_feature', 'status_tournament','game_option', 'is_ticket_prize_new', 'ticket_unit', 'url']);

            if($getLeaderboard->status_tournament == 3 OR $getLeaderboard->status_tournament == 4){
                throw new Exception('Tidak dapat membatalkan turnamen dikarenakan turnamen sudah dimulai!');
            }

            if($getLeaderboard->tournament_form != 'ticket' OR $getLeaderboard->is_new_ticket_feature != '1'){
                throw new Exception('Terjadi kesalahan, segarkan halaman! (tournament form registration invalid)');
            }


            DB::beginTransaction();

            if($getLeaderboard->is_ticket_prize_new == 1){

                $getTicketPrize = LeaderboardTicketPrizeModel::selectRaw('SUM(given_ticket) AS total_given_ticket, id_ticket ')
                                                              ->where('id_leaderboard', $id_tournament)
                                                              ->groupBy('id_leaderboard')
                                                              ->first();


                $updateTotalGiven = AllTicketModel::where('id', $getTicketPrize->id_ticket)->decrement('total_given', $getTicketPrize->total_given_ticket);
            }

            $getRegisteredTeam = PlayerPayment::where('id_tournament', $id_tournament)
                                               ->where('status', '!=', 'Cancelled')
                                               ->groupBy('id_team')
                                               ->get(['id_team', 'player_id', 'ticket_id']);


            $deleteTournament = LeaderboardModel::where('id_leaderboard',$id_tournament)->delete();
            $notification = new Notification();
            foreach($getRegisteredTeam as $userOrTeam){



                if($getLeaderboard->game_option == 1){

                    $userTicketBalance = (new UserTicketFactory)->updateTicket( $userOrTeam->player_id, $getLeaderboard->ticket, TicketRole::REFUND_SOURCE, $getLeaderboard->ticket_unit);

                    //notification
                    $link = 'tournament/'.$getLeaderboard->url;
                    $message = 'Pendaftaran Anda pada tournament '.$getLeaderboard->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';
                    $notification->singleNotification($userOrTeam->player_id, $link, $message);

                }else if($getLeaderboard->game_option == 2 OR $getLeaderboard->game_option == 3 OR $getLeaderboard->game_option == 4){

                    $teamTicketBalance = (new TeamTicketFactory)->updateTicket($userOrTeam->id_team, $getLeaderboard->ticket, TicketRole::REFUND_SOURCE,$getLeaderboard->ticket_unit);

                    //notification
                    $link = 'tournament/'.$getLeaderboard->url;
                    $message = 'Pendaftaran tim Anda pada tournament '.$getLeaderboard->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';
                    $notification->singleNotification($userOrTeam->player_id, $link, $message);
                }
            }

            $updatePlayerPayment = PlayerPayment::where('id_tournament', $id_tournament)
                                              ->where('status', '!=', 'Cancelled')
                                              ->update(['status' => 'Cancelled', 'refund_date' => date('Y-m-d H:i:s'), 'unique_flag' => null]);
            DB::commit();

            return response()->json([

                'status' => true,
                'message' => 'Berhasil membatalkan turnamen!'

            ], 200);

        }catch(\Exception $e){
                DB::rollBack();

                return response()->json([

                    'status' => false,
                    'message' => $e->getMessage()

                ], 400);
        }


    }
    public function cancelTicketTournamentBracket($id_tournament){

        try{
            $getBracket = TournamentModel::where('id_tournament', $id_tournament)->firstOrFail(['id_tournament', 'is_new_ticket_feature', 'status','game_option','id_ticket_squence','gift_ticket','system_payment','payment','tournament_name','url']);

            if($getBracket->status == "starting"){
                throw new Exception('Tidak dapat membatalkan turnamen dikarenakan turnamen sudah dimulai!');
            }

            if($getBracket->is_new_ticket_feature != '1'){
                throw new Exception('Terjadi kesalahan, segarkan halaman! (tournament form registration invalid)');
            }


            DB::beginTransaction();
            if($getBracket->is_ticket_prize_new == 1){

                $getTicketPrize = TournamentTicketSequenceModel::where('id_ticket_squence', $getBracket->id_ticket_squence)
                                                            ->groupBy('id_ticket_squence')
                                                            ->sum('given_ticket');


                $updateTotalGiven = AllTicketModel::where('id', $getBracket->gift_ticket)->decrement('total_given', $getTicketPrize->given_ticket);
            }
            $getRegisteredTeam = PlayerPayment::where('id_tournament', $id_tournament)
                                                ->where('status', '!=', 'Cancelled')
                                                ->groupBy('id_team')
                                                ->get(['id_team', 'player_id', 'ticket_id']);

            $deleteTournament = TournamentModel::where('id_tournament',$id_tournament)->delete();
            $notification = new Notification();
            $insertHistoryTicket = new HistoryTicketsModel;

            foreach($getRegisteredTeam as $userOrTeam){

                if($getBracket->game_option == 1){

                    $userTicketBalance = (new UserTicketFactory)->updateTicket( $userOrTeam->player_id, $getBracket->system_payment, TicketRole::REFUND_SOURCE, $getBracket->payment);

                    //notification
                    $link = 'tournament/'.$getBracket->url;
                    $message = 'Pendaftaran Anda pada tournament '.$getBracket->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';

                    $notification->singleNotification($userOrTeam->player_id, $link, $message);


                }else if($getBracket->game_option == 2 OR $getBracket->game_option == 3 OR $getBracket->game_option == 4){
                    $teamTicketBalance = (new TeamTicketFactory)->updateTicket( $userOrTeam->id_team, $getBracket->system_payment, TicketRole::REFUND_SOURCE, $getBracket->payment);

                    //notification
                    $link = 'tournament/'.$getBracket->url;
                    $message = 'Pendaftaran tim Anda pada tournament '.$getBracket->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';
                    $notification->singleNotification($userOrTeam->player_id, $link, $message);
                }
            }

            $updatePlayerPayment = PlayerPayment::where('id_tournament', $id_tournament)
                                                ->where('status', '!=', 'Cancelled')
                                                ->update(['status' => 'Cancelled', 'unique_flag' => null]);
            DB::commit();

            return response()->json([

                'status' => true,
                'message' => 'Berhasil membatalkan turnamen!'

            ], 200);

        }catch(\Exception $e){
                DB::rollBack();

                return response()->json([

                    'status' => false,
                    'message' => $e->getMessage()

                ], 400);
        }

    }

    public function kickParticipant(Request $request){
        $validated = $request->validate([
            'id_tournament' => 'required|max:255',
            'id_participant' => 'required|max:255',
        ]);

        try{

            $id_tournament = $request->id_tournament;
            $idParticipant = $request->id_participant;

            $getLeaderboard = LeaderboardModel::where('id_leaderboard', $id_tournament)->firstOrFail(['id_leaderboard', 'tournament_form', 'ticket', 'is_new_ticket_feature', 'status_tournament','game_option', 'ticket_unit', 'url']);

            if($getLeaderboard->status_tournament == 3 OR $getLeaderboard->status_tournament == 4){
                throw new Exception('Tidak dapat membatalkan turnamen dikarenakan turnamen sudah dimulai!');
            }

            if($getLeaderboard->tournament_form != 'ticket' OR $getLeaderboard->is_new_ticket_feature != '1'){
                throw new Exception('Terjadi kesalahan, segarkan halaman! (tournament form registration invalid)');
            }

            DB::beginTransaction();

            $getRegisteredTeam = PlayerPayment::where('id_tournament', $id_tournament)
                                               ->where('status', '!=', 'Cancelled')
                                               ->where('id_team', $idParticipant)
                                               ->groupBy('id_team')
                                               ->get(['id_team', 'player_id', 'ticket_id']);

            if(count($getRegisteredTeam) == 0){
                return response()->json([

                    'status' => false,
                    'message' => 'Data tidak ditemukan'

                ], 400);
            }

            $insertHistoryTicket = new HistoryTicketsModel;
            $notification = new Notification();

            foreach($getRegisteredTeam as $userOrTeam){



                if($getLeaderboard->game_option == 1){

                    $userTicketBalance = (new UserTicketFactory)->updateTicket($userOrTeam->player_id, $getLeaderboard->ticket, TicketRole::REFUND_SOURCE, $getLeaderboard->ticket_unit);

                    //notification
                    $link = 'tournament/'.$getLeaderboard->url;
                    $message = 'Pendaftaran Anda pada tournament '.$getLeaderboard->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';
                    $notification->singleNotification($userOrTeam->player_id, $link, $message);

                }else if($getLeaderboard->game_option == 2 OR $getLeaderboard->game_option == 3 OR $getLeaderboard->game_option == 4){

                    $teamTicketBalance = (new TeamTicketFactory)->updateTicket( $userOrTeam->id_team, $getLeaderboard->ticket, TicketRole::REFUND_SOURCE,$getLeaderboard->ticket_unit);

                    //notification
                    $link = 'tournament/'.$getLeaderboard->url;
                    $message = 'Pendaftaran tim Anda pada tournament '.$getLeaderboard->tournament_name.' telah dibatalkan, pengembalian tiket berhasil';
                    $notification->singleNotification($userOrTeam->player_id, $link, $message);
                }
            }

            $updatePlayerPayment = PlayerPayment::where('id_tournament', $id_tournament)
                                              ->where('id_team', $idParticipant)
                                              ->where('status', '!=', 'Cancelled')
                                              ->update(['status' => 'Cancelled', 'refund_date' => date('Y-m-d H:i:s'), 'unique_flag' => null]);

            $deleteTeam = LeaderboardTeamModel::where('id_team_leaderboard', $idParticipant)
                                              ->where('id_leaderboard', $id_tournament)
                                              ->delete();

            $deleteTeamPlayer = LeaderboardTeamPlayerModel::where('id_team_leaderboard', $idParticipant)
                                              ->where('id_leaderboard', $id_tournament)
                                              ->delete();

            $updateFilledSlot = LeaderboardModel::where('id_leaderboard',$id_tournament)
                                                ->update(['filled_slot' => DB::raw('filled_slot - 1')]);
            DB::commit();

            return response()->json([

                'status' => true,
                'message' => 'Berhasil mengeluarkan tim dari tournamen'

            ], 200);

        }catch(\Exception $e){
            return response()->json([

                'status' => false,
                'message' => $e

            ], 400);
        }
    }



}
