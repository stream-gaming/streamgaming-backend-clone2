<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;

/**
 * @group Account
 * @authenticated
 * APIs for the Account Information
 */
class CheckValidEmailController extends Controller
{

    /**
     * Check Email Registered/Verified
     *
     * @urlParam email required The email of user to check. Example: mramadhan687@gmail.com
     * @response {
            "status": true,
            "data": {
                "email": "mramadhan687@gmail.com",
                "is_registered": true,
                "is_verified": false
            }
        }
     */
    public function check($email)
    {
        $user = User::where('email', $email)->first(['email','status']);
        return response()->json([
            'status'    => true,
            'data'      => [
                'email' => $email,
                'is_registered' => is_null($user) ? false : true,
                'is_verified'   => !is_null($user) && ($user->status == 'player') ? true : false,
            ]
        ]);
    }
}
