<?php

namespace App\Http\Controllers\Api\Team;

use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamRequestModel;
use App\Model\TeamInviteModel;
use App\Model\TeamNameHistoryModel;
use App\Model\TeamPlayerNewModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class TeamHelperController extends Controller
{
	public function foundTeam($team_id) //found team
    {
        $team = TeamModel::find($team_id);

        if (is_null($team)) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.not')
            ], $this->HTTP_NOT_FOUND);
        }
        return $team;
    }

    public function foundRequestJoin($request_id)
    {
        $request = TeamRequestModel::find($request_id);

        if (is_null($request)) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.join.request.not')
            ], $this->HTTP_NOT_FOUND);
        }
        return $request;
    }

    public function foundRequestInvite($request_id)
    {
        $request = TeamInviteModel::find($request_id);

        if (is_null($request)) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.join.request.not')
            ], $this->HTTP_NOT_FOUND);
        }
        return $request;
    }

    public function hasTeam($game_id, $user_id=null /*optional user*/) //has team in same game
    {
        if (is_null($user_id)) {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
				            ->where('player_id', 'LIKE', '%'.Auth::user()->user_id.'%')
                            ->orWhere('game_id', $game_id)
                            ->where('substitute_id', 'LIKE', '%'.Auth::user()->user_id.'%')
				            ->count();
        } else {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
				            ->where('player_id', 'LIKE', '%'.$user_id.'%')
                            ->orWhere('game_id', $game_id)
                            ->where('substitute_id', 'LIKE', '%'.$user_id.'%')
				            ->count();
        }

        if ($team > 0) { //jika sudah memiliki tim di game ini
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.has')
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Check is user has team
     *
     * @param string $game_id game id
     * @param string|null $user_id user id (optional)
     * @return boolean
     */
    public function hasTeamV2($game_id, $user_id=null)
    {
        if(is_null($user_id)){
            $user_id = Auth::user()->user_id;
        }

        $teamPlayer = TeamPlayerNewModel::whereHas('team', function($q) use($game_id)
        {
            $q->where('game_id',$game_id);
        })->where('player_id',$user_id)->count();

        if($teamPlayer > 0){
            return true;
        }

        return false;
    }

    public function getTeamByGame($game_id, $user_id=null /*optional user*/) //has team in same game
    {
        if (is_null($user_id)) {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
				            ->where('player_id', 'LIKE', '%'.Auth::user()->user_id.'%')
                            ->orWhere('game_id', $game_id)
                            ->where('substitute_id', 'LIKE', '%'.Auth::user()->user_id.'%')
				            ->first();
        } else {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player', 'team.team_id', '=', 'team_player.team_id')
				            ->where('player_id', 'LIKE', '%'.$user_id.'%')
                            ->orWhere('game_id', $game_id)
                            ->where('substitute_id', 'LIKE', '%'.$user_id.'%')
				            ->first();
        }

        return $team;
    }

    public function hasRequestJoin($team_id) // has request join in this team
    {
        $requestJoin = TeamRequestModel::where('team_id', $team_id)
							        ->where('users_id', Auth::user()->user_id)
							        ->where('status', 1)
							        ->count();

        if ($requestJoin > 0) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.join.request.has')
            ], $this->HTTP_BAD);
        }
    }

    public function hasInviteRequest($data) // has request join in this team
    {
        $requestInvite = TeamInviteModel::where('team_id', $data->team_id)
								        ->where('users_id', $data->users_id)
								        ->where('identity_id', $data->identity_id)
								        ->where('status', 1)
								        ->count();

        if ($requestInvite > 0) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.invite.request.has')
            ], $this->HTTP_BAD);
        }
    }

    public function isFull($team)
    {
        if ($team->isFull()) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.full')
            ], $this->HTTP_BAD);
        }
    }

    /**
     * check is team full v2
     * @return bool
     */
    public function isFullv2(TeamModel $team)
    {
        $maxSize = $team->game->player_on_team + $team->game->substitute_player;

        $recentSize = TeamPlayerNewModel::where('team_id',$team->team_id)->count();

        return $recentSize >= $maxSize;
    }

    public function isLeader($team, $check = false)
    {
        if ($team->leader_id != Auth::user()->user_id) {
            if ($check == true) {
                return false;
            } else {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.leader')
                ], $this->HTTP_BAD);
            }
        } else {
            if ($check == true) {
                return true;
            }
        }
    }

    public function isMember($team, $users_id=null /*optional if want check someone user*/)
    {
        if (is_null($users_id)) {
            if (!in_array(Auth::user()->user_id, $team->allPlayer())) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.not-member')
                ], $this->HTTP_BAD);
            }
        } else {
            if (!in_array($users_id, $team->allPlayer())) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.not-member-other')
                ], $this->HTTP_BAD);
            }
        }
    }

    public function isMemberV2($team, $user_id=null)
    {
        if ($user_id==null) { $user_id = Auth::user()->user_id; }
        return (bool) TeamPlayerNewModel::where('team_id', $team->team_id)->where('player_id', $user_id)->count();
    }

    /**
     * Check is team allow to change the name
     * @param TeamModel $team
     */
    public function isAllow($team)
    {
        $date = new Carbon();
        $dayinmonth = $date->now()->subDays(30);

        $limited = TeamNameHistoryModel::where('team_id',$team->team_id)
                                     ->whereDate('created_at','>=',$dayinmonth)
                                     ->orderBy('created_at','DESC')
                                     ->first();

        if($limited){
            $diffdays = $date->parse($limited->created_at)->diffInDays($dayinmonth);

            return $diffdays;
        }

        return 0;
    }
}
