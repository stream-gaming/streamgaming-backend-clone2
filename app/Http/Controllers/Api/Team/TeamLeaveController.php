<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Support\Arr;
use App\Model\TeamInviteModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;
use App\Jobs\CacheTriggerPortalJob;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;

/**
 * @group Team
 *
 * APIs for managing team.
 */
class TeamLeaveController extends Controller
{
    private $leaveTeamV2;
    /**
     * Leave Team
     *
     * @urlParam team_id required The ID of the team. Example: 5f62f0f64a2d3
     *
     * @response status=400 {
            "status": false,
            "message": "Anda bukan anggota tim ini"
        }
     *
     * @response status=200 {
            "status": true,
            "message": "Anda berhasil keluar dari tim"
        }
     *
     */
    public function leave($team_id)
    {
        $TeamHelper = new TeamHelperController;
        $team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            return $team;
        }
        $allPlayerOriginal = $team->allPlayer();

        // start - old statement
        $isMember = $TeamHelper->isMember($team);
        if (!is_null($isMember)) {
            return $isMember;
        }
        // end - old statement

        // start - new statement
        if (config("app.team_player_new")) {

            $user_playerMember = TeamPlayerNewModel::where('team_id', $team->team_id)->where('player_id',Auth::user()->user_id)->first();
            if (!$user_playerMember) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.not-member')
                ], $this->HTTP_BAD);
            }
        }
        // end - new statement


        // start - old statement
        if ($team->isPrimaryPlayer()) { //jika pemain inti
            $player = $team->primaryPlayer();
            $key = array_search(Auth::user()->user_id, $player);
            unset($player[$key]);

            $player = implode(',', $player);
            $team->primary_temp1 = (empty($player)) ? null : $player;
            $team->primary_temp = $player;
            $team->substitute_temp = $team->substitute_temp1 = (is_null($team->substitutePlayer())) ? [] : implode(',', $team->substitutePlayer());

            /** Jika pemain cadangan tidak kosong
             * Pindahkan salah satu cadangan ke inti
             */
            if (!empty($team->substitute_temp)) {
                $pr_player = explode(',', trim($team->primary_temp, ','));
                // $sb_player = explode(',', trim($team->substitute_temp, ','));

                $sb_player = TeamPlayerNewModel::where('team_id',$team_id)->where('player_status','3')->get()->pluck('player_id')->all();

                $fdata_primary_player = collect($pr_player);
                $fdata_primary_player->push($sb_player[0]);
                $fdata_primary_player = implode(',', $fdata_primary_player->all());
                $team->primary_temp = $fdata_primary_player;

                $fdata_substitute_player = collect($sb_player);
                $fdata_substitute_player->shift();
                $fdata_substitute_player = implode(',', $fdata_substitute_player->all());
                $team->substitute_temp = $fdata_substitute_player;

                $this->leaveTeamV2 = true;
            }

            $isPri = true;
        } else if ($team->isSubstitutePlayer()) { //jika pemain cadangan
            $player = $team->substitutePlayer();
            $key = array_search(Auth::user()->user_id, $player);
            unset($player[$key]);

            $player = implode(',', $player);
            $team->primary_temp = $team->primary_temp1 = (is_null($team->primaryPlayer())) ? [] : implode(',', $team->primaryPlayer());
            $team->substitute_temp1 = (empty($player)) ? null : $player;
            $team->substitute_temp = $player;
            $isSub = true;
        }

        if ($TeamHelper->isLeader($team, true)) {
            $allPlayer = $team->allPlayer();
            $keyP = array_search(Auth::user()->user_id, $allPlayer);
            unset($allPlayer[$keyP]);
            $team->new_lead = Arr::first($allPlayer);
        } else {
            $team->mail = Auth::user()->user_id;
        }

        $team->newPlayer = collect(!empty($team->primary_temp1) ? explode(',', $team->primary_temp1) : [null])
            ->merge(!empty($team->substitute_temp1) ? explode(',', $team->substitute_temp1) : [null])
            ->all();
        // end - old statement


        // start - new statement
        if (config("app.team_player_new")) {

            $count_allTeamPlayer_id = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->count();
            $allTeamPlayer_id = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->get()->pluck('player_id')->all();

        }
        // end - new statement


        // start - old statement
        RedisHelper::destroyDataWithArray('my:team:user_id', $allPlayerOriginal);
        // end - old statement

        // start - new statement
        if (config("app.team_player_new")) {
            RedisHelper::destroyDataWithArray('my:team:user_id', $allTeamPlayer_id);
        }
        // end - new statement

        RedisHelper::destroyData('team:detail:team_id:' . $team_id);
        RedisHelper::destroyWithPattern('team:list:page:');

        try {
            DB::beginTransaction();

            // old statement
            if ($team->countMember() - 1 != 0 and $this->leaveTeamV2) {
                $this->leaveTeamV2($team);
            } elseif (isset($isSub)) {
                unset($team->substitute_temp1);
                unset($team->primary_temp);
            } elseif (isset($isPri)) {
                unset($team->primary_temp1);
                unset($team->substitute_temp);
            }

            if ($team->countMember() - 1 == 0) {
                //jika tidak ada lagi member tim
                //hapus tim
                if (!config("app.team_player_new")) {
                    $this->deleteTeam($team);
                }

            } else {
                // keluarkan dari tim
                // if ($this->leaveTeamV2) return $this->leaveTeamV2($team);
                $this->leaveTeam($team);
            }
            // new statement

            // start - new statement
            if (config("app.team_player_new")) {
                $is_leader = $user_playerMember->player_status == 1 ? true : false;

                if ($count_allTeamPlayer_id - 1 == 0) {
                    $user_playerMember->delete();

                    $this->deleteTeam($team);
                } else {
                    $move_subtitute = false;

                    if($user_playerMember->player_status == 2 || $is_leader) {
                        $move_subtitute = true;
                    }

                    $user_playerMember->forceDelete();

                    $notificationData = (object) [
                        'game_id' => $team->game_id,
                        'team_name' => $team->team_name
                    ];

                    $notificationData->newPlayer = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->get()->pluck('player_id')->all();

                    if ($is_leader) {
                        $team_leader = TeamModel::where('team_id',$team_id)->first(['leader_id']);

                        $team_player_leader = TeamPlayerNewModel::where('team_id',$team_id)->where('player_status',1)->first();

                        if (!$team_player_leader) {
                            $team_player_leader = TeamPlayerNewModel::where('team_id',$team_id)->where('player_id',$team_leader->leader_id)->first();
                            if (!$team_player_leader) {
                                $team_player_leader = TeamPlayerNewModel::where('team_id',$team_id)->first();
                            }
                            $notificationData->new_lead = $team_player_leader->player_id;

                            $team_player_leader->player_status = 1;
                            $team_player_leader->save();
                        } else {
                            if ($team_leader->leader != $team_player_leader->player_id) {
                                $team_player_leader->player_status = 2;
                                $team_player_leader->save();

                                $team_player_leader = TeamPlayerNewModel::where('team_id',$team_id)->where('player_id',$team_leader->leader_id)->first();

                                $notificationData->new_lead = $team_player_leader->player_id;

                                $team_player_leader->player_status = 1;
                                $team_player_leader->save();
                            }
                        }
                    } else {
                        $notificationData->mail = Auth::user()->user_id;
                    }

                    if ($move_subtitute) {
                        $subtitute = TeamPlayerNewModel::where('team_id',$team_id)->where('player_status','3')->first();
                        if ($subtitute) {
                            $subtitute->player_status = 2;
                            $subtitute->save();
                        }
                    }

                    new UserNotificationController('leave_team', $notificationData);
                }

            }
            // end - new statement

            DB::commit();

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.leave')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
        }


    }

    public function deleteTeam($team)
    {
            //delete team
            $team->delete();
            //delete team player

            // start - old statement : delete soon
            $team_player = TeamPlayerModel::find($team->team_id);
            $team_player->delete();
            // end - old statement : delete soon

            //delete request join team
            $request_join = TeamRequestModel::where('team_id', $team->team_id)
                ->delete();
            //delete invited join team
            $request_join = TeamInviteModel::where('team_id', $team->team_id)
                ->delete();
    }

    public function leaveTeam($team)
    {
            $team_player = TeamPlayerModel::find($team->team_id);
            // update data player
            if (isset($team->primary_temp)) {
                $team_player->player_id = $team->primary_temp;
                $team_player->save();
            } elseif (isset($team->substitute_temp)) {
                $team_player->substitute_id = $team->substitute_temp;
                $team_player->save();
            }

            //send notif
            // $Notification = new UserNotificationController('leave_team', $team);

            unset($team->newPlayer);
            unset($team->mail);
            if (isset($team->new_lead)) {
                $team->leader_id = $team->new_lead;
                unset($team->new_lead);
                unset($team->substitute_temp);
                unset($team->substitute_temp1);
                unset($team->primary_temp);
                unset($team->primary_temp1);
                $team->save();
            }
    }

    public function leaveTeamV2($team)
    {
            // $team_player = TeamPlayerModel::find($team->team_id);
            // update data player
            $team->player->player_id = $team->primary_temp;
            $team->player->substitute_id = $team->substitute_temp;
            $team->player->save();

            //send notif
            if (!config("app.team_player_new")) {
                $Notification = new UserNotificationController('leave_team', $team);
            }

            unset($team->newPlayer);
            unset($team->mail);
            if (isset($team->new_lead)) {
                $team->leader_id = $team->new_lead;
                unset($team->new_lead);
                unset($team->substitute_temp);
                unset($team->substitute_temp1);
                unset($team->primary_temp);
                unset($team->primary_temp1);
                $team->save();
            }
    }
}
