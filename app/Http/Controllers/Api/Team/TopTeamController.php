<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\MyApps;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameModel;
use App\Model\LeaderboardScoreTeamModel;
use App\Model\TeamModel;
use App\Model\TopTeamLeaderboardModel;
use App\Transformer\TopTeamTransform;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @group Team
 *
 * APIs for get Top team
 */
class TopTeamController extends Controller
{
    /**
     * @var Manager
     */
    private $fractal;
    function __construct(Manager $fractal)
    {
        $this->fractal           = $fractal;
        $this->team              = new TeamModel();
        $this->my_apps           = new MyApps();
    }

    /**
     * Get All Top Team.
     * <aside class="notice">APIs Method for get All Top Tim | untuk melihat semua data Top Tim</aside>
     * @urlParam limit string, for limit top team that wants to be taken | batas data tim teratas yang ingin diambil | masukan [all] jika ingin semua tim. Example: 10
     * @responseFile status=200 scenario="success" responses/top.team.json
     * @response scenario="failed" status="404" {
        "status": false,
        "message": "Top Tim Tidak ada",
        "error": "Undefined variable: dataa"
        }
     */
    public function getAllTeam(Request $request, $limit = 5)
    {

        if (Redis::exists('team.top.' . $limit)) {
            return response()->json(json_decode(Redis::get('team.top.' . $limit)));
        }


        try {

            if (strtolower($limit) == "all") {
                //- Get All Top Team
                $is_limit_all = true;

                $Bracket      = collect($this->getStatistikTypeBracket()->get());
                $LeaderBoards = collect($this->getStatistikTypeLeaderboard()->get());
            } else {
                //- Get Top team by Limit query and limit collect
                $is_limit_all = false;

                $Bracket      = collect(DB::table(DB::raw('(' . $this->my_apps->getEloquentSqlWithBindings($this->getStatistikTypeBracket()) . ') AS bc'))
                    ->where("bc.urutan", "<=", $limit)
                    ->get());

                $LeaderBoards  = collect(DB::table(DB::raw('(' . $this->my_apps->getEloquentSqlWithBindings($this->getStatistikTypeLeaderboard()) . ') AS ld'))
                    ->where("ld.urutan", "<=", $limit)
                    ->get());
            }

            $getGame = GameModel::whereNotIn('system_kompetisi', ['Multisystem'])->get();

            foreach ($getGame as $i => $data) {
                $game[$i] = [
                    'game_id'          => $data->id_game_list,
                    'game_name'        => $data->name,
                    'system_kompetisi' => $data->system_kompetisi,
                    'game_logo'        => $this->my_apps->cdn($data->logo, 'encrypt', 'game'),
                ];

                $system_kompetisi = $data->system_kompetisi;

                $top_team = $$system_kompetisi->where('game_id', $data->id_game_list)
                    ->sortByDesc('win_rate')
                    ->sortByDesc('total_win');

                if (!$is_limit_all) {
                    $top_team = $top_team->slice(0, $limit);
                }

                $response = new Collection($top_team->all(), new TopTeamTransform);
                $game[$i]['top_team'] = current($this->fractal->createData($response)->toArray());
            }

            $data = [
                "status" => true,
                "data"   => [
                    "title" => "Top Team",
                    "game"  => $game
                ],
            ];

            Redis::set('team.top.' . $limit, json_encode($data), 'EX', 24 * 3600);

            return response()->json($data);
        } catch (\Throwable $e) {
            return response()->json([
                "status"    => false,
                "message"   => trans("message.team.top.failed"),
                "error"     => $e->getMessage()
            ], $this->HTTP_NOT_FOUND);
        }
    }

    public function getStatistikTypeBracket()
    {
        //- Get Total Kill, from Another Table
        $total_kill = $this->team->selectRaw("sum( kills ) AS total_kill, id_team")
            ->leftjoin('tournament_score_matchs', 'tournament_score_matchs.id_team', '=', 'team.team_id')
            ->groupBy('id_team');

        //- Get team statistics data where the tournament type is the Bracket
        $team_statistic = $this->team->selectRaw('team.game_id, team.team_id, team.team_name, team. team_logo, SUM( tournament_group.win ) AS total_win, SUM( tournament_group.matchs ) AS total_match, IFNULL(tsm.total_kill, 0) as total_kill, ( CAST( SUM( tournament_group.win ) AS UNSIGNED ) / CAST( SUM( tournament_group.matchs ) AS UNSIGNED ) * 100 ) AS win_rate')
            ->leftjoin('tournament_group', 'tournament_group.id_team', '=', 'team.team_id')
            //- Join eloquent total kill
            ->leftJoin(DB::raw('(' . $this->my_apps->getEloquentSqlWithBindings($total_kill) . ') AS tsm'), 'tsm.id_team', '=', 'team.team_id')
            ->whereNull('team.deleted_at')
            ->groupBy('team.team_id');

        //- Get data for each Game
        $join = DB::table(DB::raw('(' . $this->my_apps->getEloquentSqlWithBindings($team_statistic) . ') AS bc'))
            ->where("bc.total_match", "!=", 0)
            ->selectRaw("bc.*, ( row_number() over ( partition by game_id  order by bc.total_win desc, bc.win_rate desc ) ) as urutan");

        return $join;
    }

    public function getStatistikTypeLeaderboard()
    {
        //- Get team statistics data where the tournament type is the Leaderboard
        $team_statistic = $this->team->selectRaw('team.game_id, team_id, team_name, team_logo, IFNULL( SUM( ( CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 END ) ), 0 ) AS total_win, SUM( leaderboard_score_team.total_kill ) AS total_kill, COUNT( leaderboard_score_team.id_team ) AS total_match, ( CAST( SUM( ( CASE WHEN  leaderboard_score_sequence.urutan = 1 THEN 1 END ) ) AS UNSIGNED ) / CAST( COUNT( leaderboard_score_team.id_team ) AS UNSIGNED ) * 100 ) AS win_rate')
            ->leftjoin('leaderboard_score_team', 'team.team_id', '=', 'leaderboard_score_team.id_team')
            ->leftjoin('leaderboard_score_sequence', 'leaderboard_score_sequence.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
            ->whereNull('team.deleted_at')
            ->whereNotNull('game_id')
            ->groupBy('team.team_id');

        //- Get data for each Game
        $join = DB::table(DB::raw('(' . $this->my_apps->getEloquentSqlWithBindings($team_statistic) . ') AS bc'))
            ->where("bc.total_match", "!=", 0)
            ->selectRaw("bc.*, ( row_number() over ( partition by game_id  order by bc.total_win desc, bc.win_rate desc ) ) as urutan");

        return $join;
    }
}
