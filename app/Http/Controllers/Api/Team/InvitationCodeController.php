<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Http\Requests\TeamInvitationCodeGneratorRequest;
use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Modules\Team\InvitationCodeGenerator;
use App\Modules\Team\Validators\InvitationCodeExpValidator;
use App\Modules\Team\Validators\LeaderTeamValidator;
use App\Modules\Team\Validators\TeamBanStatusValidator;
use App\Modules\Team\Validators\TournamentJoinStatusValidator;
use App\Transformer\TeamInvitationCodeTransform;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @group Team
 *
 * APIs for managing team.
 */
class InvitationCodeController extends Controller
{
    /**
     * Get Team Invitation Code
     *
     * Retrieve team invitation code data
     *
     * @urlParam team required The id of the team. Example:12345678ab
     *
     * @response status=200 scenario=state_is_active {
     *      "invitationCode" : "ABCD123",
     *      "expiredAt"      : "2022-03-08 22:43:43",
     *      "state"          : "ACTIVE"
     * }
     *
     * @response status=200 scenario=state_is_banned {
     *      "invitationCode" : "ABCD123",
     *      "expiredAt"      : "2022-03-08 22:43:43",
     *      "state"          : "BANNED"
     * }
     *
     * @response status=200 scenario=state_is_in_tournament {
     *      "invitationCode" : "ABCD123",
     *      "expiredAt"      : "2022-03-08 22:43:43",
     *      "state"          : "IN_TOURNAMENT"
     * }
     *
     * @response status=404 scenario=team_not_found {
     *      "status" : false,
     *      "message": "Resource item not found."
     * }
     *
     * @response status=403 scenario=not_leader_of_the_team {
     *      "status" : false,
     *      "message": "You are not the team leader"
     * }
     */
    public function getInvitationCode(TeamModel $team)
    {
        try {

            if (!(new LeaderTeamValidator($team, Auth::user()))->check()) {
                throw new AccessDeniedHttpException(Lang::get('message.team.leader'));
            }

            $invitationCode = InvitationCode::where('team_id', $team->team_id)->first();

            if (!$invitationCode) {
                $invitationCode = InvitationCodeGenerator::create($team);
            }

            $transformData = new Item($invitationCode, new TeamInvitationCodeTransform);

            $data = (new Manager())->setSerializer(new ArraySerializer)->createData($transformData)->toArray();

            return response()->json($data, 200);

        } catch (\Throwable $th) {
            Log::debug($th->getMessage());

            $statusCode = 500;

            if ($th instanceof AccessDeniedHttpException   ) {
                $statusCode = 403;
            }

            if ($th instanceof ModelNotFoundException) {
                $statusCode = 404;
            }

            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], $statusCode);
        }
    }

    /**
     * Generate Invitation Code
     *
     * Api to generate new invitation code if the last code is expired
     *
     * @bodyParam teamId string required the id of the team. Example: 12345678ab
     *
     * @response status=200 scenario=success {
     *      "status"         : true,
     *      "data"           : {
     *          "invitationCode" : "ABCD123",
     *          "expiredAt"      : "2022-03-08 22:43:43",
     *          "state"          : "ACTIVE"
     *      }
     * }
     *
     * @response status=400 scenario=bad_request {
     *      "status" : false,
     *      "message": "there is still an active code"
     * }
     *
     * @response status=404 scenario=Not_Found {
     *      "status" : false,
     *      "message": "Team not found"
     * }
     *
     * @response status=403 scenario=access_denied {
     *      "status" : false,
     *      "message": "This team is still on ongoing tournament"
     * }
     *
     * @response status=401 scenario=unauthorized {
     *      "status" : false,
     *      "message": "You are not the team leader"
     * }
     */
    public function generateInvitationCode(TeamInvitationCodeGneratorRequest $request)
    {
        $teamId = $request->teamId;

        try {
            $team = TeamModel::findOrFail($teamId);

            if (!(new TeamBanStatusValidator($team))->check()) {
                throw new AccessDeniedHttpException(Lang::get('message.team.banned',[
                    'time'=> $team->ban_until != 'forever' ? Carbon::parse($team->ban_until)->diffForHumans() :'forever'
                ]));
            }

            if (!(new LeaderTeamValidator($team, Auth::user()))->check()) {
                throw new UnauthorizedException(Lang::get('message.team.leader'));
            }

            if (!(new TournamentJoinStatusValidator($team))->check()) {
                throw new AccessDeniedHttpException(Lang::get('team.join_tournament'));
            }

            $invitationCode = InvitationCode::where('team_id', $team->team_id)->first();

            if ($invitationCode && (new InvitationCodeExpValidator($invitationCode))->check()) {
                throw new BadRequestException(Lang::get('team.invitation_code.not_expired'));
            }

            $invitationCode = InvitationCodeGenerator::create($team);

            $transformData = new Item($invitationCode, new TeamInvitationCodeTransform);

            $data = (new Manager())->setSerializer(new ArraySerializer)->createData($transformData)->toArray();

            return response()->json([
                'status' => true,
                'data' => $data
            ], 200);

        } catch (\Throwable $th) {
            Log::debug($th->getMessage());

            $statusCode = 500;
            $message = $th->getMessage();

            if ($th instanceof UnauthorizedException) {
                $statusCode = 401;
            }

            if ($th instanceof AccessDeniedHttpException) {
                $statusCode = 403;
            }

            if ($th instanceof ModelNotFoundException) {
                $statusCode = 404;
                $message = Lang::get('message.team.not');
            }

            if ($th instanceof BadRequestException) {
                $statusCode = 400;
            }

            return response()->json([
                'status' => false,
                'message' => $message
            ], $statusCode);
        }
    }
}
