<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\MyApps;
use App\Model\TeamModel;
use Illuminate\Http\Request;
use App\Model\IdentityGameModel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Traits\MyTraits;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamSearchUserInviteController extends Controller
{
    use MyTraits;

    public function __construct()
    {
        $this->MyApps = new MyApps();
    }

    /**
     * Search User To Invite
     *
     * @authenticated
     * @urlParam team_id required The id of the team. Example: 5f6328c95cfb2
     * @queryParam search required The keyword search, search with ID Ingame or Nickname Ingame. Example: banjo
     *
     * @response 404 {
            "status": false,
            "message": "Data tidak ditemukan"
        }
     *
     * @response {
            "data": [
                {
                    "game_id": "1086187021",
                    "users_id": "5f6216e363c3f",
                    "id_ingame": "00007",
                    "username_ingame": "anjayyyyyy07",
                    "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/private/5f6216e363c3f/UYRGKKXwYlPgPuby296AET1zEo7y6TnGkJOyA6el.jpg"
                }
            ]
        }
     */
    public function searchUser(Request $request, $team_id)
    {
        $request->validate([
            'search'    => 'required|string|min:3',
        ]);

        $TeamHelper = new TeamHelperController;
        $team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        try {
            $search = $request->search;
            $limit  = request()->query('limit') ? request()->query('limit') : 10;

            $identity = collect(IdentityGameModel::where('game_id', $team->game_id)
                ->with(['detail' => function ($q) {
                    $q->select(['user_id', 'image']);
                }])
                ->where(function ($q) use ($search) {
                    $q->where('username_ingame', 'like', "{$search}%")
                    ->orWhere('id_ingame', 'like', "{$search}%");
                })
                ->select(['game_id', 'users_id', 'id_ingame', 'username_ingame'])
                ->limit($limit)
                ->get()
            );

            $identity->transform(function ($data) {
                return [
                    'game_id'       => $data->game_id,
                    'users_id'      => $data->users_id,
                    'id_ingame'     => $data->id_ingame,
                    'username_ingame'   => $data->username_ingame,
                    'image'         => $this->getImage($data->detail)
                ];
            });

            if (count($identity) == 0) {
                return response()->json([
                    'status'    => false,
                    'message'   => 'Data tidak ditemukan'
                ], 404);
            }

            return response()->json([
                'data' => $identity
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => 'Data tidak ditemukan'
            ], 404);
        }
    }
}
