<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Services\UploadService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\CheckBanned;
use App\Http\Requests\ValidateCreateTeam;
use App\Http\Controllers\Api\GameController;
use App\Http\Controllers\Api\IdentityGameController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\TeamPlayerNewModel;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamCreateController extends Controller
{
    protected $UploadService;

    public function __construct(UploadService $upload)
    {
        $this->UploadService = $upload;
        $this->middleware('block.request');
    }
    /**
     * Create Team
     *
     * @authenticated
     *
     * @bodyParam team_name string required The name of team. Example: Bang Jago SQUAD
     * @bodyParam game_id string required The game of team. This get from API Simple Game. Example: 179415089
     * @bodyParam team_description string required The description of team. Example: sederhana namun indah
     * @bodyParam city string required The domicili/location of team. This get from API Address > Get City. Example: KOTA MEDAN
     * @bodyParam team_logo file required The logo of team. The extension must be one of JPG, JPEG, PNG. Max size
       is 2MB
     *
     * @response status=404 scenario="game not found" {
            "status": false,
            "message": "Permainan tidak ditemukan"
        }
     *
     * @response status=422 scenario="validation form" {
            "message": "The given data was invalid.",
            "errors": {
                "team_name": [
                    "Nama Tim wajib diisi."
                ]
            }
        }
     *
     * @response status=409 {
            "status": false,
            "message": "Tim gagal dibuat",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Tim berhasil dibuat"
        }
     */
	public function create(ValidateCreateTeam $request)
    {
        $foundGame = (new GameController)->foundGame($request->game_id, false);
        if (!is_null($foundGame)) { //check available game
            return $foundGame;
        }

        $hasIdentity = (new IdentityGameController)->hasIdentity($request->game_id);
        if (!is_null($hasIdentity)) { //check if has identity game
            return $hasIdentity;
        }

        $hasTeam = (new TeamHelperController)->hasTeam($request->game_id);
        if (!is_null($hasTeam)) { //check has team in this game
            return $hasTeam;
        }

        // start - Has team check v2
        if (config("app.team_player_new")) {

            $hasTeam = (new TeamHelperController)->hasTeamV2(($request->game_id));
            if($hasTeam){
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.has')
                ], $this->HTTP_BAD);
            }

        }
        // end - Has team check v2

        $checkValidation = (new CheckBanned)->isSoloBanned($request->game_id, null, auth()->user()->user_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */

        $data = $request;
        try {
            DB::beginTransaction();
            //file logo
            $file = $data->file('team_logo');
            $image_name = $this->UploadService->upload('public/content/team', $file, null, [
                'options' => 'resize',
                'width' => 150,
                'height' => 150
            ]);

            //check error when upload
            if (is_object($image_name)) {
                return $image_name;
            }
            //insert data team
            $Team = new TeamModel;
            $Team->team_id = uniqid();
            $Team->game_id = $data->game_id;
            $Team->leader_id = Auth::user()->user_id;
            $Team->team_name = $data->team_name;
            $Team->team_description = $data->team_description;
            $Team->team_logo = $image_name;
            $Team->venue = $data->city;
            $Team->save();

            //insert data player team
            $TeamPlayer = new TeamPlayerModel;
            $TeamPlayer->team_id = $Team->team_id;
            $TeamPlayer->player_id = $Team->leader_id;
            $TeamPlayer->save();


            // start - insert data player team v2
            if (config("app.team_player_new")) {

                TeamPlayerNewModel::create([
                    'team_id' => $Team->team_id,
                    'player_id' => $Team->leader_id,
                    'player_status' => 1,
                ]);

            }
            // end - insert data player team v2

            DB::commit();

            //save parent chat
            // (new PrivateChatModel)->create($Team);
            //Bismilah
            RedisHelper::destroyWithPattern('my:team:user_id:' . Auth::user()->user_id . ':');
            RedisHelper::destroyWithPattern('team:list:page:');

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.add.success')
            ],201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.add.failed'),
                'error'     => $e
            ], 409);
        }
    }
}
