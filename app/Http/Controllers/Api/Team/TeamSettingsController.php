<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Helpers\Team\LeaderManager;
use App\Helpers\Team\MainPlayerBatchManager;
use App\Helpers\Team\MainPlayerSingleManager;
use App\Helpers\Team\SubsPlayerBatchManager;
use App\Helpers\Team\SubsPlayerSingleManager;
use Illuminate\Http\Request;
use App\Model\TeamPlayerModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\ValidateSettingTeam;
use App\Http\Requests\ValidateChangeLogoTeam;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;
use App\Model\TeamNameHistoryModel;
use App\Model\TeamPlayerNewModel;
use App\Services\UploadService;
use Exception;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamSettingsController extends Controller
{

    /**
     * Update Data Team
     *
     * @authenticated
     * @urlParam team_id required Id of team you want to update data. Example: 5f62f0f64a2d3
     * @bodyParam team_name string required New team name, cannot be same with another team name. Example: Tim Keras
     * @bodyParam team_description string required New description team. Example: santai aja guys
     * @bodyParam city string required New city team, must get from API Get City data. Example: KOTA MEDAN
     *
     * @response status=404 scenario="team not found" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response status=422 scenario="validation form" {
            "message": "The given data was invalid.",
            "errors": {
                "team_name": [
                    "Nama Tim wajib diisi."
                ]
            }
        }
     *
     * @response 409 {
            "status": false,
            "message": "Data tim gagal diubah",
            "error": "message error"
        }
     *
     * @response status=409 scenario=conflict {
            "status": false,
            "message": "silahkan ubah nama tim setelah 30 hari lagi"
        }
     *
     * @response {
            "status": true,
            "message": "Data tim berhasil diubah"
        }
     */
	public function setting(ValidateSettingTeam $request, $team_id)
	{
		$TeamHelper = new TeamHelperController;

		$team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        $team_old_name = $team->team_name;

        if($request->team_name != $team_old_name){
            $isAllow = $TeamHelper->isAllow($team);
            if ($isAllow) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('team.name.limit',['days' => $isAllow])
                ], $this->HTTP_BAD);
            }
        }



        try{
            DB::beginTransaction();
        	$team->team_name = $request->team_name;
        	$team->team_description = $request->team_description;
        	$team->venue = $request->city;
            $team->save();

            if($request->team_name != $team_old_name){
                TeamNameHistoryModel::create([
                    'team_id' => $team->team_id,
                    'old_name' => $team_old_name,
                    'new_name' => $request->team_name
                ]);
            }

            DB::commit();

            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);
            RedisHelper::destroyWithPattern('team:list:page:');

        	return response()->json([
        		'status'	=> true,
        		'message'	=> Lang::get('message.team.edit.success')
        	]);
        } catch (\Exception $e) {
            DB::rollBack();
        	return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.edit.failed'),
                'error'     => $e
            ], 409);
        }
	}


    /**
     * Change Logo Team
     *
     * @authenticated
     * @urlParam team_id required Id of team you want to change logo. Example: 5f62f0f64a2d3
     * @bodyParam team_logo file required The new logo team you want to change, the extension must be one of JPG, JPEG, PNG. Max size is 2MB.
     *
     * @response status=404 scenario="team not found" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response status=422 scenario="validation form" {
            "message": "The given data was invalid.",
            "errors": {
                "team_logo": [
                    "Logo Tim harus berupa gambar.",
                    "Logo Tim harus berupa berkas berjenis: jpeg, png, jpg.",
                    "Logo Tim maksimal berukuran 2048 kilobita."
                ]
            }
        }
     *
     * @response 409 {
            "status": false,
            "message": "Data tim gagal diubah",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Data tim berhasil diubah",
            "data": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/DoNcIdoMjqfAHrIZk938XaMU6Cax7szd1MG63N40.png"
        }
     */
	public function changeLogo(ValidateChangeLogoTeam $request, $team_id)
	{
		$TeamHelper = new TeamHelperController;

		$team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        try{
        	//data logo lama
        	$file = $request->file('team_logo');
            $old_image = $team->team_logo;

            //upload file
            $upload = (new UploadService)->upload('public/content/team', $file, $old_image, [
                'options' => 'resize',
                'width' => 150,
                'height' => 150
            ]);

             //check error when upload
             if (is_object($upload)) {
                return $upload;
            }

            $team->team_logo = $upload;
            $team->save();

            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            RedisHelper::destroyWithPattern('team:list:page:');
            RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);

        	return response()->json([
        		'status'	=> true,
        		'message'	=> Lang::get('message.team.edit.success'),
        		'data'		=> $upload
        	]);
        } catch (\Exception $e) {
        	return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.edit.failed'),
                'error'     => $e
            ], 409);
        }
	}


    /**
     * Change Role Member
     *
     * @authenticated
     * @urlParam team_id required Id of team you want to change role member. Example: 5f62f0f64a2d3
     * @bodyParam primary_player array List of the new player in role primary.
     * @bodyParam primary_player.* string The id user.
     * @bodyParam substitute_player array List of the new player in role substitute.
     * @bodyParam substitute_player.* string The id user.
     *
     * @response status=404 scenario="team not found" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response 400 scenario="leader cannot be substitute player" {
            "status": false,
            "message": "Pemimpin tidak boleh menjadi pemain cadangan!",
        }
     * @response 400 scenario="total player not same total player in change role" {
            "status": false,
            "message": "Total pemain tidak sesuai!",
        }
     *
     * @response status=422 scenario="validation form request" {
            "message": "The given data was invalid.",
            "errors": {
                "primary_player": [
                    "Pemain inti harus berisi sebuah array.",
                    "Pemain inti maksimal terdiri dari 4 anggota."
                ]
            }
        }
     *
     * @response status=409 scenario="conflic" {
            "status": false,
            "message": "Data tim gagal diubah",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Daftar anggota tim berhasil diubah"
        }
     */
    public function changeRole(Request $request, $team_id)
    {

        $TeamHelper = new TeamHelperController;

        $team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            $this->team = $team;
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        $request->validate([
            'primary_player'        => 'nullable|array|max:'.$team->game->player_on_team,
            'substitute_player'     => 'nullable|array|max:'.$team->game->substitute_player,
        ]);

        $primary = is_null($request->primary_player) ? [] : $request->primary_player;
        $substitute = is_null($request->substitute_player) ? [] : $request->substitute_player;

        // check leader cannot to be substitute player
        if (collect($substitute)->contains($team->leader_id)) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.team.leader-cannot-substitute')
            ], 400);
        }

        $all = collect($primary)->merge($substitute)->all();

        // cek jumlah harus sama
        if ( count($all) != count($team->allPlayer()) ) {
            return response()->json([
                'status'    => false,
                'message' => Lang::get('message.team.total-not-same')
            ], 400);
        }
        //cek terdaftar sebagai anggota atau bukan
        foreach ($all as $allPlayer) {

            $isMember = $TeamHelper->isMember($team, $allPlayer);
            if (!is_null($isMember)) {
                return $isMember;
            }

        }

        $primary = count($primary) == 0 ? NULL : implode(',', $primary);
        $substitute = count($substitute) == 0 ? NULL : implode(',', $substitute);

        try {
            DB::beginTransaction();
            $team_player = TeamPlayerModel::find($team->team_id);
            $team_player->player_id = $primary;
            $team_player->substitute_id = $substitute;
            $team_player->save();

            //new data flow
            if(config('app.team_player_new')){
                    //filter leader from other player
                    if(($leaderIndex = array_search($team->leader_id, $request->primary_player)) >= 0){
                        $newPrimaryPlayer = $request->primary_player;
                        unset($newPrimaryPlayer[$leaderIndex]);
                        array_values($newPrimaryPlayer);
                    }
                    //set new main player formation
                    $mainPlayer = new MainPlayerBatchManager($team->team_id, $newPrimaryPlayer);
                    if(!$mainPlayer->changeStatus()){
                        throw new Exception(Lang::get('message.team.edit.failed'));
                    }
                    //set new subtitue player formation
                    $substitutePlayer = new SubsPlayerBatchManager($team->team_id, $request->substitute_player);
                    if(!$substitutePlayer->changeStatus()){
                        throw new Exception(Lang::get('message.team.edit.failed'));
                    }
            }

            DB::commit();
            //new data flow
            if(config('app.team_player_new')){
                $allPlayer = array_merge($request->primary_player, $request->substitute_player);
                RedisHelper::destroyDataWithArray('my:team:user_id', $allPlayer);
            }
            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            RedisHelper::destroyWithPattern('team:list:page:');
            RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.edit.role')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.edit.failed'),
                'error'     => $e
            ], 409);
        }
    }



    /**
     * Change Leader Team
     *
     * @authenticated
     * @urlParam team_id required Id of team you want to change role member. Example: 5f62f0f64a2d3
     * @bodyParam leader string The user_id to set as new leader. Example: 5f475dbfeb150
     *
     * @response status=404 scenario="team not found" {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response 400 {
            "status": false,
            "message": "Akses ditolak, coba lagi!",
        }
     *
     * @response status=409 scenario="conflic" {
            "status": false,
            "message": "Data tim gagal diubah",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Pemimpin tim berhasil diubah"
        }
     */
    public function changeLeader(Request $request, $team_id)
    {
        $TeamHelper = new TeamHelperController;

        $team = $TeamHelper->foundTeam($team_id);
        if (!is_null($team->original)) {
            $this->team = $team;
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        $isMember = $TeamHelper->isMember($team, $request->leader);
        if (!is_null($isMember)) {
            return $isMember;
        }

        try {
            DB::beginTransaction();

            //new data flow
            if(config('app.team_player_new')){
                $oldLeaderId = $team->leader_id;
                $oldPlayerStatus = TeamPlayerNewModel::select('player_status')
                                                    ->where('team_id', $team_id)
                                                    ->where('player_id', $request->leader)
                                                    ->firstOrFail();
                //Switch old leader status
                switch($oldPlayerStatus->player_status){
                    case '2':
                        $setPlayerStatus = new MainPlayerSingleManager($team_id, $oldLeaderId);
                    break;
                    case '3':
                        $setPlayerStatus = new SubsPlayerSingleManager($team_id, $oldLeaderId);
                    break;
                }

                if(!$setPlayerStatus->changeStatus()){
                    throw new Exception(Lang::get('message.team.edit.failed'));
                }

                //Set new leader
                $setLeader = new LeaderManager($team_id, $request->leader);
                if(!$setLeader->changeStatus()){
                    throw new Exception(Lang::get('message.team.edit.failed'));
                }
            }

            $team->leader_id = $request->leader;
            $team->save();

            //send notif
            $Notification = new UserNotificationController('change_leader', $team);
            DB::commit();

            //new data flow
            if(config('app.team_player_new')){
                $allPlayer = TeamPlayerNewModel::where('team_id', $team_id)->pluck('player_id');
                RedisHelper::destroyDataWithArray('my:team:user_id', $allPlayer);
            }
            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            RedisHelper::destroyData('team:detail:team_id:' . $team_id);

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.edit.leader')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.edit.failed'),
                'error'     => $e
            ], 409);
        }
    }
}
