<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Http\Request;
use App\Model\TeamInviteModel;
use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\CheckBanned;
use App\Http\Controllers\Api\IdentityGameController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;


/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamInviteRequestController extends Controller
{

    /**
     * Invite to Team
     *
     * @authenticated
     * @bodyParam team_id required The id of the team. Example: 5f6328c95cfb2
     * @bodyParam users_id required The id of user obtained from API Search User. Example: 5f475e7dcdc39
     *
     * @response 404 {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response 409 {
            "status": false,
            "message": "Undangan bergabung gagal terkirim",
            "error": "message"
        }
     *
     * @response {
            "status": true,
            "message": "Undangan bergabung berhasil terkirim"
        }
     */
	public function invite(Request $request)
    {
    	$TeamHelper = new TeamHelperController;
        $team = $TeamHelper->foundTeam($request->team_id);
        if (!is_null($team->original)) { //cari team
            return $team;
        }

        $hasIdentity = (new IdentityGameController)->hasIdentity($team->game_id, $request->users_id);
        if (!is_null($hasIdentity)) { //check if has identity game
            return $hasIdentity;
        }

        $hasTeam = $TeamHelper->hasTeam($team->game_id, $request->users_id);
        if (!is_null($hasTeam)) {
        	return $hasTeam;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        $isFull = $TeamHelper->isFull($team);
        if (!is_null($isFull)) { //check team is full
            return $isFull;
        }

        $request->identity_id = IdentityGameModel::where('game_id', $team->game_id)
										        ->where('users_id', $request->users_id)
										        ->first('id')->id;

        $hasInviteRequest = $TeamHelper->hasInviteRequest($request);
        if (!is_null($hasInviteRequest)) { //check status request previous
            return $hasInviteRequest;
        }

        $checkValidation = (new CheckBanned)->isSoloBanned($team->game_id, null, $request->users_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */

        $data = $request;
        try {
            DB::beginTransaction();
            $team_invite = new TeamInviteModel;
            $team_invite->invite_id = uniqid();
            $team_invite->team_id = $data->team_id;
            $team_invite->identity_id = $data->identity_id;
            $team_invite->users_id = $data->users_id;
            $team_invite->status = 1;
            $team_invite->save();

            //soft delete request
            $TeamRequest = TeamInviteModel::where('team_id', $data->team_id)
                                                    ->where('identity_id', $data->identity_id)
                                                    ->where('users_id', $data->users_id)
                                                    ->where('status', '!=', 1)
                                                    ->delete();
            DB::commit();

            //send notif
            $Notification = new UserNotificationController('request_invite', $data);

            RedisHelper::destroyWithPattern('my:team:user_id:' . $data->users_id . ':');

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.invite.request.success')
            ],201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
        }
    }
}
