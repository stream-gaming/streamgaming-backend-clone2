<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamRequestModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\CheckBanned;
use App\Http\Controllers\Api\IdentityGameController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamRequestJoinController extends Controller
{

    /**
     * Join Team
     *
     * @authenticated
     *
     * @urlParam team_id required The ID of target team. Example: 5f62f0f64a2d3
     *
     * @response 404 {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response 409 {
            "status": false,
            "message": "Permintaan bergabung gagal terkirim",
            "error": "message"
        }
     *
     * @response {
            "status": true,
            "message": "Permintaan bergabung berhasil terkirim"
        }
     *
     */
	public function join($team_id)
    {
    	$TeamHelper = new TeamHelperController;
        $team = $TeamHelper->foundTeam($team_id);

        if (!is_null($team->original)) {
            return $team;
        }

        $hasIdentity = (new IdentityGameController)->hasIdentity($team->game_id);
        if (!is_null($hasIdentity)) { //check if has identity game
            return $hasIdentity;
        }

        $hasTeam = $TeamHelper->hasTeam($team->game_id);
        if (!is_null($hasTeam)) { //check has team in this game
            return $hasTeam;
        }

        $hasRequestJoin = $TeamHelper->hasRequestJoin($team->team_id);
        if (!is_null($hasRequestJoin)) { //check status request previous
            return $hasRequestJoin;
        }

        $isFull = $TeamHelper->isFull($team);
        if (!is_null($isFull)) { //check team is full
            return $isFull;
        }

        $checkValidation = (new CheckBanned)->isSoloBanned($team->game_id, null, auth()->user()->user_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */

        $data = $team;
        try {
            DB::beginTransaction();
    		//save request
    		$TeamRequest = new TeamRequestModel;
    		$TeamRequest->request_id = uniqid();
    		$TeamRequest->team_id = $data->team_id;
    		$TeamRequest->users_id = Auth::user()->user_id;
    		$TeamRequest->status = 1;
            $TeamRequest->save();

            //soft delete request
            $TeamRequest = TeamRequestModel::where('team_id', $data->team_id)
                                                    ->where('users_id', Auth::user()->user_id)
                                                    ->where('status', '!=', 1)
                                                    ->delete();
            DB::commit();

    		// sent notification
    		$Notification = new UserNotificationController('request_join', $data);

            RedisHelper::destroyWithPattern('my:team:user_id:' . $data->leader_id . ':');
            RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);
            RedisHelper::destroyWithPattern('team:list:page:');

    		return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.join.request.success')
            ],201);
     	} catch (\Exception $e) {
            DB::rollBack();
    		return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.join.request.failed'),
                'error'     => $e
            ], 409);
    	}
    }
}
