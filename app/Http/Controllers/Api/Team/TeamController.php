<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use App\Serializer\TeamSerialize;
use App\Transformer\TeamTransform;
use App\Http\Controllers\Controller;
use App\Model\GameModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\LeaderboardScoreTeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redis;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @group Team
 *
 * APIs for managing team.
 */

class TeamController extends Controller
{

    public function __construct()
    {
        $this->middleware(\App\Http\Middleware\RefreshTokenManual::class, ['only' => ['getAll']]);
    }


    /**
     * Get All Team
     *
     * @queryParam limit view data per page. Defaults is 15. Example:1
     * @queryParam search search for a team by keyword of the team name and venue. Example: tester
     * @queryParam game sort team with game, enter the game_id from simple game. Defaults is all game. No-example
     *
     * @response status=200 scenario="with auth" {
            "data": [
                {
                    "team_id": "5f62f0f64a2d3",
                    "game": "PUBG MOBILE",
                    "game_id": "501085427"
                    "leader": "gia10",
                    "team_name": "Tester aja kali 4123",
                    "venue": "KAB. ACEH SELATAN",
                    "logo": "http://localhost/Github/Cmfg0/logo/team/s2qHFTz01mKCGNSEZ7sC0JzNBw",
                    "slot": {
                        "total": 6,
                        "member": 2
                    },
                    "full_url": "http://localhost:8000/api/team/detail/s2qHFTz01mKCGNSEOr8O",
                    "url": "s2qHFTz01mKCGNSEOr8O",
                    "created_at": "2020-09-17 12:15:34",
                    "is_member": true,
                    "is_waiting": true
                }
            ],
            "is_login": true,
            "meta": {
                "pagination": {
                    "total": 3,
                    "count": 3,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1,
                    "links": {
                        "default": "http://localhost:8000/api/team",
                        "next": "http://localhost:8000/api/team?limit=1&search=tester&page=2"
                    }
                }
            }
        }
     * @response status=200 scenario="no auth" {
            "data": [
                {
                    "team_id": "5f62f0f64a2d3",
                    "game": "PUBG MOBILE",
                    "game_id": "501085427"
                    "leader": "gia10",
                    "team_name": "Tester aja kali 4123",
                    "venue": "KAB. ACEH SELATAN",
                    "logo": "http://localhost/Github/Cmfg0/logo/team/s2qHFTz01mKCGNSEZ7sC0JzNBw",
                    "slot": {
                        "total": 6,
                        "member": 2
                    },
                    "full_url": "http://localhost:8000/api/team/detail/s2qHFTz01mKCGNSEOr8O",
                    "url": "s2qHFTz01mKCGNSEOr8O",
                    "created_at": "2020-09-17 12:15:34"
                }
            ],
            "is_login": false,
            "meta": {
                "pagination": {
                    "total": 3,
                    "count": 3,
                    "per_page": 15,
                    "current_page": 1,
                    "total_pages": 1,
                    "links": {
                        "default": "http://localhost:8000/api/team",
                        "next": "http://localhost:8000/api/team?limit=1&search=tester&page=2"
                    }
                }
            }
        }
     */
    public function getAll(Request $request)
    {
        $limit = request()->query('limit') ? request()->query('limit') : 15;
        $page  = request()->query('page') != "" ? request()->query('page') : 1;

        if (Auth::check()) {
            $user         = Auth::user();
            $user_request = TeamRequestModel::where('users_id', $user->user_id)
                        ->where('status', 1)
                        ->pluck('team_id')->toArray();
        }else{
            $user_request = [];
        }

        $cek   = !count(array_filter(($request->except(['page'])))) == 0;
        if (!$cek && Redis::exists('team:list:page:' . $page . ':limit:' . $limit)) {
            $cache = json_decode(Redis::get('team:list:page:' . $page . ':limit:' . $limit));
            if (!Auth::check()) {

                foreach ($cache->data as $k => $v) {
                    $cache->data[$k]->is_member  = false;
                    $cache->data[$k]->is_waiting = false;
                }
                $cache->is_login = false;

                return response()->json($cache);
            }
            $team_player    = TeamPlayerModel::where('player_id', 'LIKE', '%' . $user->user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . $user->user_id . '%')
                ->pluck('team_id')->toArray();

            foreach ($cache->data as $k => $v) {
                $cache->data[$k]->is_member  = false;
                $cache->data[$k]->is_waiting = false;

                if (collect($team_player)->contains($cache->data[$k]->team_id)) {
                    $cache->data[$k]->is_member  = true;
                }
                if (collect($user_request)->contains($cache->data[$k]->team_id)) {
                    $cache->data[$k]->is_waiting = true;
                }
            }
            $cache->is_login = Auth::check();

            return response()->json($cache);
        }
        try {
            $fractal = new Manager();
            $fractal->setSerializer(new TeamSerialize());

            $paginator = TeamModel::select(
                DB::raw(
                    'team.*,team_player.player_id,
                    team_player.substitute_id'
                )
            )
            ->when(request()->query('search'), function ($query) use ($request) {
                if ($request->game) {
                    $query->where('game_id', $request->game);
                }
                $query->where(function ($q) use ($request) {
                    $q->where('team_name', 'like', "%{$request->search}%")
                    ->orWhere('venue', 'like', "%{$request->search}%")
                    ->orWhere('team.team_id', 'like', "%{$request->search}%");
                });
            })->when($request->game, function ($query) use ($request) {
                $query->where('game_id', $request->game);
            })->latest()
            ->leftJoin('team_player', 'team_player.team_id', '=', 'team.team_id')
            ->paginate($limit);

            $team = $paginator->appends(request()->query())->getCollection();

            $resource = new Collection($team,(new TeamTransform($user_request)));
            $data     = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
            $data     = $fractal->createData($data)->toArray();
            if (!$cek) {
                RedisHelper::setData('team:list:page:' . $page . ':limit:' . $limit, $data, 1 * (24 * 3600));
            }
            return $data;
        }catch(\Exception $e){
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }


    /**
     * Get Detail Team
     *
     * @urlParam url required The Url of the team. Example: s2qHFzf0h2eNGtTTZ-kO
     *
     * @response status=404 {
            "status": false,
            "message": "Tim tidak ditemukan"
        }
     *
     * @response scenario="ringkasan"{
            "data": {
                "team_id": "5f49fac96ae9e",
                "game": "PUBG MOBILE",
                "leader": "gia10",
                "game_id": "501085427"
                "team_name": "Tester aja",
                "venue": "medan",
                "description": "Tidak ada yang penting",
                "logo": "http://localhost/Github/Cmfg0/logo/team/s2qHFzf0h2eNGtTTaLoC0JzNBw",
                "slot": {
                    "total": 6,
                    "member": 1
                },
                "created_at": "2020-08-29 13:50:49",
                "member": {
                    "primary": [
                        {
                            "username": "gia10",
                            "fullname": "Frankie Douglas",
                            "image": "http://localhost/Github/Cmfg0/private/user.png",
                            "bio": null,
                            "birthday": "-",
                            "age": "-",
                            "username_game": "GGS",
                            "socmed": [],
                            "is_leader": 1
                        }
                    ],
                    "substitute": []
                }
            }
        }
     * @responseFile status=200 scenario="ringkasan v2" responses/team.ringkasan_v2.json
     * @responseFile status=200 scenario="pertandingan" responses/team.history.json
     * @responseFile status=200 scenario="pertandingan v2" responses/team.history_v2.json
     *
     * @responseFile status=200 scenario="statistik" responses/team.statistic.json
     * @responseFile status=200 scenario="statistik v2" responses/team.statistic_v2.json
     *
     */

    public function getDetail($hash, $type='ringkasan')
    {
        $team_id = (new \App\Helpers\MyApps)->onlyDecrypt($hash);
        $team = (new TeamHelperController)->foundTeam($team_id);


        if (!is_null($team->original)) {
            return $team;
        }

        $menu = [
            'ringkasan' => route('team.detail', ['url' => $hash, 'type' => 'ringkasan']),
            'anggota' => route('team.detail', ['url' => $hash, 'type' => 'anggota']),
            'pertandingan' => route('team.detail', ['url' => $hash, 'type' => 'pertandingan']),
            'statistik' => route('team.detail', ['url' => $hash, 'type' => 'statistik'])
        ];

        $checkSystem = GameModel::select('system_kompetisi')->where('id_game_list', $team->game_id)->first();
        $isMultisystem = $checkSystem->system_kompetisi == 'Multisystem' ? true : false;

            if($type == 'ringkasan' OR $type =='anggota'){
                $team->dataTeam = $this->getDataTeam($team->team_id);
                return response()->json(
                    [
                        'active_menu' => $type,
                        'menu' => $menu,
                        'is_multisystem' => $isMultisystem,
                        'data'=>TeamTransform::detail($team, $isMultisystem)
                    ]
                );
            }elseif($type == 'statistik'){
                try{

                    $teamgamedata   = TeamModel::with(['game' => function($q){
                        $q->select('id_game_list','system_kompetisi');
                    }])->where('team_id',$team_id)->first();


                    if($teamgamedata->game->system_kompetisi == 'LeaderBoards'){

                        return response()->json(
                            [
                                'active_menu' => $type,
                                'menu' => $menu,
                                'is_multisystem' => $isMultisystem,
                                'data_statistic'=> $this->LeaderboardTeamStatistic($team_id)
                            ]
                        );

                    }elseif($teamgamedata->game->system_kompetisi == 'Bracket'){
                        return response()->json(
                            [
                                'active_menu' => $type,
                                'menu' => $menu,
                                'is_multisystem' => $isMultisystem,
                                'data_statistic'=> $this->BracketTeamStatistic($team_id)
                            ]
                        );
                    }elseif($teamgamedata->game->system_kompetisi == 'Multisystem'){
                        return response()->json(
                            [
                                'active_menu' => $type,
                                'menu' => $menu,
                                'is_multisystem' => $isMultisystem,
                                'data_statistic'=> null,
                                'data_statistic_multi' => ['bracket' => $this->BracketTeamStatistic($team_id), 'leaderboard' => $this->LeaderboardTeamStatistic($team_id)]
                            ]
                        );
                    }

                }catch(\Exception $e){
                    return response()->json(
                        $e
                    );
                }


            }elseif($type == 'pertandingan'){
                try{
                    $teamMatchHistory = (new TeamHistoryMatchs)->index($team_id);
                    return response()->json(
                        [
                            'active_menu' => $type,
                            'menu' => $menu,
                            'is_multisystem' => $isMultisystem,
                            'history_pertandingan'=> $teamMatchHistory,
                            'history_pertandingan_multi' => $isMultisystem ? (new TeamHistoryMatchs)->multiSystem($team_id) : null
                        ]
                    );

                }catch(\Exception $e){
                    return response()->json(
                        $e
                    );
                }

            }


    }

    public function getDataTeam($id_team){

        $teamgamedata   = TeamModel::with('game')->where('team_id',$id_team)->first();
        $SystemKompetisi=$teamgamedata->game->system_kompetisi;
        if($SystemKompetisi == "Bracket"){
            $team   =  TeamModel::selectRaw('SUM(tournament_group.matchs) AS Matcs,SUM(tournament_group.win) AS Wins')
                                ->leftjoin('tournament_group', 'tournament_group.id_team' ,'=' ,'team.team_id')
                                ->Where('id_team' ,$id_team)
                                ->first();
            $total  =   TeamModel::leftjoin('tournament_score_matchs', 'tournament_score_matchs.id_team' ,'=' ,'team.team_id')
                                ->Where('team.team_id' ,$id_team)
                                ->sum('tournament_score_matchs.kills');
            try{
                return
                    [   'total_match'   =>(int) $team->Matcs,
                        'total_win'     =>(int) $team->Wins,
                        'total_lose'    =>(int) $team->Matcs - $team->Wins,
                        'total_kills'   =>(int) $total
                    ];
            }catch(\Exception $e){
                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ], $this->HTTP_NOT_FOUND);
            }
        }else if($SystemKompetisi == "LeaderBoards"){
            //retieve team statistic
            $teamStatistic = (new LeaderboardScoreTeamModel)::selectRaw(' SUM((CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 END)) AS total_win ,SUM(leaderboard_score_team.total_kill) AS total_kill, COUNT(leaderboard_score_team.id_team) AS total_match')
                                                            ->leftjoin('leaderboard_score_sequence', 'leaderboard_score_sequence.id_score_sequence' ,'=' ,'leaderboard_score_team.id_score_sequence')
                                                            ->where('id_team',$id_team)
                                                            ->first();
            try{
                return
                    [   'total_match'   =>(int) $teamStatistic->total_match,
                        'total_win'     =>(int) $teamStatistic->total_win,
                        'total_lose'    =>(int) ($teamStatistic->total_match - $teamStatistic->total_win),
                        'total_kills'   =>(int) $teamStatistic->total_kill
                ];
            }catch(\Exception $e){
                return response()->json([
                    'status'    => false,
                    'message'   => $e->getMessage()
                ], $this->HTTP_NOT_FOUND);
            }

        }

    }


    public function LeaderboardTeamStatistic($team_id){

        $teamStatistic = (new LeaderboardScoreTeamModel)::selectRaw(' SUM((CASE WHEN leaderboard_score_sequence.urutan = 1 THEN 1 END)) AS total_win ,SUM(leaderboard_score_team.total_kill) AS total_kill, COUNT(leaderboard_score_team.id_team) AS total_match')
                        ->leftjoin('leaderboard_score_sequence', 'leaderboard_score_sequence.id_score_sequence' ,'=' ,'leaderboard_score_team.id_score_sequence')
                        ->where('id_team',$team_id)
                        ->first();

        if(!empty($teamStatistic)){
            return  [
                'total_win'     =>(int) ($teamStatistic->total_win != null ? $teamStatistic->total_win : 0),
                'total_match'   =>(int) ($teamStatistic->total_match != null ? $teamStatistic->total_match : 0) ,
                'win_rate'      =>(string) ($teamStatistic->total_match != 0 ? round((($teamStatistic->total_win / $teamStatistic->total_match) * 100),1) : 0).'%'
            ];
        }else{
            return  [
                'total_win'     =>(int) 0,
                'total_match'   =>(int) 0,
                'win_rate'      => (string) (0).'%'
            ];

        }




    }

    public function BracketTeamStatistic($team_id){
        $teamStatistic   =  TeamModel::selectRaw('SUM(tournament_group.matchs) AS matchs,SUM(tournament_group.win) AS Wins')
                                ->leftjoin('tournament_group', 'tournament_group.id_team' ,'=' ,'team.team_id')
                                ->Where('team.team_id' ,$team_id)
                                ->first();
        if(!empty($teamStatistic)){
            return  [
                'total_win'         => (int)($teamStatistic->Wins != null ? $teamStatistic->Wins : 0),
                'win_rate'          => (string) ($teamStatistic->matchs != 0 ? round((($teamStatistic->Wins / $teamStatistic->matchs) * 100),1) : 0).'%',
                'total_match'      =>  (int) ($teamStatistic->matchs != null ? $teamStatistic->matchs : 0) ,
            ];

        }else{
            return  [
                'total_win'     =>(int) 0,
                'win_rate'      => (string) (0).'%',
                'total_match'   =>(int) 0
            ];

        }
        // $dataStatistic = [];
        // return "ss";
        return $teamStatistic;
    }


}
