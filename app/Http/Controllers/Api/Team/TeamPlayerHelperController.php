<?php

namespace App\Http\Controllers\Api\Team;

use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use Illuminate\Support\Facades\Auth;

class TeamPlayerHelperController extends Controller
{
     /**
    * @param string $team - team_id
     * @param string $user - player_id || User_id
    * @return int
    */
   Public function isPrimaryPlayer($user,$team){
        $request = TeamPlayerNewModel::where('player_status','2')
                                        ->where('team_id', $team)
                                        ->where('player_id', $user)
                                        ->count();
        if ($request < 0) { 
            return false;
        }
        return $request;

        
   }
    /**
    * @param string $team - team_id
    */
   public function primaryPlayer($team){
        $request = TeamPlayerNewModel::where('player_status', '<=','2')
                                        ->where('team_id', $team)
                                        ->get();
        if ($request->isEmpty()) {
            return false;
        }
        return $request;
        
        
   }

   /**
    * @param string $team - team_id
     * @param string $user - player_id || User_id
    * @return int
    */
   public function isMember($user, $team){
        $request = TeamPlayerNewModel::where('team_id', $team)
                                        ->where('player_id', $user)
                                        ->count();
          return $request;
   }
   public function isSubstitutePlayer($user,$team){
        $request = TeamPlayerNewModel::where('player_status','3')
                                        ->where('team_id', $team)
                                        ->where('player_id', $user)
                                        ->count();
        if ($request < 0) { 
            return false;
        }

        return $request;
   }
   public function substitutePlayer($team){
     $request = TeamPlayerNewModel::where('player_status','3')
                                        ->where('team_id', $team)
                                        ->get();
        if ($request->isEmpty()) {
            
            return false;
        }
        return $request;
   }
   public function singleSubstitutePlayer($team){
        $request = TeamPlayerNewModel::where('player_status','3')
                                        ->where('team_id', $team)
                                        ->firstOrFail();
        return $request;
   }
   public function allPlayerTeam($team){
          $request = TeamPlayerNewModel::where('team_id', $team)->whereNull('deleted_at')->count();
         
          if ($request < 0) { 
            return false;
          }
          return $request;
   }
   public function isFull($team){
         $request = $this->allPlayerTeam($team->team_id);
         return ($request >= ($team->game->player_on_team + $team->game->substitute_player)) ? false : true;;
   }
   public function hasTeam($game_id, $user_id=null /*optional user*/) //has team in same game
    {
        if (is_null($user_id)) {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player_new', 'team.team_id', '=', 'team_player_new.team_id')
				            ->where('player_id', Auth::user()->user_id)
				            ->count();
        } else {
            $team = TeamModel::where('game_id', $game_id)
				            ->leftJoin('team_player_new', 'team.team_id', '=', 'team_player_new.team_id')
				            ->where('player_id', $user_id)
				            ->count();
                           
        }
        return $team;
    }
}
