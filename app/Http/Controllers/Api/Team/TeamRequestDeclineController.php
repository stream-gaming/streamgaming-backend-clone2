<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;


/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamRequestDeclineController extends Controller
{

    /**
     * Decline Join Request
     *
     * @authenticated
     * @urlParam req_id required The id of the request join. Example: 5f6328c95cfb2
     *
     * @response 404 {
            "status": false,
            "message": "Permintaan tidak ditemukan"
        }
     *
     * @response status=409 {
            "status": false,
            "message": "Akses ditolak, coba lagi!",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Permintaan bergabung berhasil ditolak"
        }
     */
	public function declineJoin($request_id)
    {
    	$TeamHelper = new TeamHelperController;
        $request = $TeamHelper->foundRequestJoin($request_id);

        if (!is_null($request->original)) {
            return $request;
        }

        $team = $TeamHelper->foundTeam($request->team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        if ($request->status != 1) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        try {
            DB::beginTransaction();
            $request->status = 0;
            $request->save();
            DB::commit();

            //send notif
            $Notification = new UserNotificationController('decline_join', $request);

            RedisHelper::destroyData('team:detail:team_id:' . $request->team_id);
            RedisHelper::destroyWithPattern('my:team:user_id:' . $team->leader_id . ':');
            RedisHelper::destroyWithPattern('team:list:page:');

            return response()->json([
            	'status'    => true,
            	'message'   => Lang::get('message.team.join.decline')
            ],201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
        }
    }
}
