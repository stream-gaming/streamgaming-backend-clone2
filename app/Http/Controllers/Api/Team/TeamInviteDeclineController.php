<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\UserNotificationController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;


/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamInviteDeclineController extends Controller
{

    /**
     * Decline Invitation to The Team
     *
     * @authenticated
     * @urlParam inv_id required The id of invitation. Example: 5f64365bc766a
     *
     * @response 404 {
            "status": false,
            "message": "Permintaan tidak ditemukan"
        }
     *
     * @response 409 {
            "status": false,
            "message": "Akses ditolak, coba lagi!",
            "error": "message"
        }
     *
     * @response {
            "status": true,
            "message": "Undangan bergabung berhasil ditolak"
        }
     */
	public function declineInvite($request_id)
    {
    	$TeamHelper = new TeamHelperController;
        $request = $TeamHelper->foundRequestInvite($request_id);

        if (!is_null($request->original)) {
            return $request;
        }

        $team = $TeamHelper->foundTeam($request->team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        if (Auth::user()->user_id != $request->users_id) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        if ($request->status != 1) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        try {
        	$request->status = 0;
        	$request->save();

   			//send notif
        	$Notification = new UserNotificationController('decline_invite', $request);

            RedisHelper::destroyWithPattern('my:team:user_id:' . Auth::user()->user_id . ':');

        	return response()->json([
        		'status'    => true,
        		'message'   => Lang::get('message.team.invite.decline')
        	],201);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
        }
    }
}
