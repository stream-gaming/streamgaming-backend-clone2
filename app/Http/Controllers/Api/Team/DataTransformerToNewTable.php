<?php

namespace App\Http\Controllers\Api\Team;

use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use Illuminate\Support\Facades\DB;

class DataTransformerToNewTable extends Controller
{
    //

    public function transformAndCopyData(){

        $teamInserted = 0;
        try{
            TeamModel::with(['player' => function($q){
                $q->withTrashed();
            }])->withTrashed()
                        ->chunk(200, function($teams) use (&$teamInserted){
                            foreach($teams as $team){

                                if(TeamPlayerNewModel::where('team_id', $team->team_id)->withTrashed()->count() == 0){
                                    DB::beginTransaction();

                                    //Check main player
                                    if(isset($team->player->player_id)){
                                        $players = explode(',', $team->player->player_id);
                                    }else{
                                        $players = [];
                                    }

                                    //Check substitute player
                                    if(isset($team->player->substitute_id)){
                                        $substitutes = explode(',', $team->player->substitute_id);
                                    }else{
                                        $substitutes = [];
                                    }

                                    //insert player and leader to new table
                                    foreach($players as $playerId){
                                            if($playerId != ''){
                                                if($playerId == $team->leader_id){
                                                    TeamPlayerNewModel::insert([
                                                        'team_id' => $team->team_id,
                                                        'player_id' => $playerId,
                                                        'player_status' => 1,
                                                        'created_at' => $team->player->created_at,
                                                        'updated_at' => $team->player->updated_at,
                                                        'deleted_at' => $team->player->deleted_at
                                                    ]);
                                                }else{
                                                    TeamPlayerNewModel::insert([
                                                        'team_id' => $team->team_id,
                                                        'player_id' => $playerId,
                                                        'player_status' => 2,
                                                        'created_at' => $team->player->created_at,
                                                        'updated_at' => $team->player->updated_at,
                                                        'deleted_at' => $team->player->deleted_at
                                                    ]);
                                                }
                                        }
                                    }

                                    //insert substitutes player to new table
                                    if(count($substitutes) > 0){
                                        foreach($substitutes as $substituteId){
                                            if($substituteId != ''){
                                                TeamPlayerNewModel::insert([
                                                    'team_id' => $team->team_id,
                                                    'player_id' => $substituteId,
                                                    'player_status' => $team->leader_id == $substituteId ? 1 : 3,
                                                    'created_at' => $team->player->created_at,
                                                    'updated_at' => $team->player->updated_at,
                                                    'deleted_at' => $team->player->deleted_at
                                                ]);
                                            }

                                        }
                                    }

                                    $teamInserted++;
                                    DB::commit();
                                }

                            }
                        });

            return [
                'status' => true,
                'msg' => 'Migrasi data berhasil, '.$teamInserted.' tim telah dimigrasikan.'
            ];
        }catch(\Exception $e){
            return [
                'status' => false,
                'msg' => $e->getMessage()
            ];
        }

    }
}
