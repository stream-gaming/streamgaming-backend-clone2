<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\CheckBanned;
use App\Http\Controllers\Api\IdentityGameController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\Team\TeamPlayerHelperController;
use App\Http\Controllers\Api\UserNotificationController;
use App\Model\TeamPlayerNewModel;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamRequestAcceptController extends Controller
{

    /**
     * Accept Join Request
     *
     * @authenticated
     * @urlParam req_id required The id of the request join. Example: 5f6328c95cfb2
     *
     * @response 404 {
            "status": false,
            "message": "Permintaan tidak ditemukan"
        }
     *
     * @response status=409 {
            "status": false,
            "message": "Akses ditolak, coba lagi!",
            "error": "message error"
        }
     *
     * @response {
            "status": true,
            "message": "Permintaan bergabung berhasil disetujui"
        }
     */
	public function acceptJoin($request_id)
    {
       
    	$TeamHelper = new TeamHelperController;
        $TeamHelperNew  = new TeamPlayerHelperController;
        $request = $TeamHelper->foundRequestJoin($request_id);
     
        if (!is_null($request->original)) {
            return $request;
        }

        $team = $TeamHelper->foundTeam($request->team_id);
        if (!is_null($team->original)) {
            return $team;
        }
      
        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }
        if(config('app.team_player_new')){ 
            $isMember = $TeamHelperNew->isMember($request->users_id,$request->team_id);
            if ($isMember) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.has')
                ], $this->HTTP_BAD);
            }
        }
        if ($request->status != 1) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        $isFull = $TeamHelper->isFull($team);
        if (!is_null($isFull)) { //check team is full
            return $isFull;
        }
       
        $hasTeam = $TeamHelper->hasTeam($team->game_id, $request->users_id /*optional user*/);
        if (!is_null($hasTeam)) { //check has team in this game
            return $hasTeam;
        }

        $hasIdentity = (new IdentityGameController)->hasIdentity($team->game_id, $request->users_id/*optional*/);
        if (!is_null($hasIdentity)) { //check if has identity game
            return $hasIdentity;
        }
      
        $checkValidation = (new CheckBanned)->isSoloBanned($team->game_id, null, $request->users_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */
        
        try {
            
    		$team = TeamModel::find($request->team_id);
    		$team_player = TeamPlayerModel::find($request->team_id);
         
    		if ($team->game->player_on_team == count($team->primaryPlayer())) { //jika pemain inti sudah penuh
    			$player_temp = $team->substitutePlayer();
    			array_push($player_temp, $request->users_id);
    			$team_player->substitute_id = implode(',', $player_temp);
    		} else { //jika pemain inti belum penuh
    			$player_temp = $team->primaryPlayer();
    			array_push($player_temp, $request->users_id);
    			$team_player->player_id = implode(',', $player_temp);
            }
            DB::beginTransaction();
    		$team_player->save(); //save data player
    		$request->status = 2;
            $request->save(); //update request to acc
            if(config('app.team_player_new')){ 
                $isFull = $TeamHelperNew->isFull($team);

                if($isFull == false){
                    return response()->json([
                        'status'    => false,
                        'message'   => Lang::get('message.team.full')
                    ], $this->HTTP_BAD);
                }    
                $hasTeam = $TeamHelperNew->hasTeam($team->game_id, $request->users_id /*optional user*/);
                
                if($hasTeam == 1){ //check has team in this game
                    return response()->json([
                        'status'    => false,
                        'message'   => Lang::get('message.team.has')
                    ], $this->HTTP_BAD);
                }
                $TeamPlayerNew = new TeamPlayerNewModel();
                $TeamPlayerNew->team_id    = $team->team_id;
                $TeamPlayerNew->player_id  = $request->users_id;
             
                if ($team->game->player_on_team == count($TeamHelperNew->primaryPlayer($team->team_id))) { //jika pemain inti sudah penuh
                    $TeamPlayerNew->player_status = 3;
                
                } else { //jika pemain inti belum penuh
                    $TeamPlayerNew->player_status = 2;
                 
                }
                    $TeamPlayerNew->save();
            }
            DB::commit();

        	//send notif
    		$Notification = new UserNotificationController('accept_join', $request);

            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            if (config("app.team_player_new")) {
                $allTeamPlayer_id = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->get()->pluck('player_id')->all();
                RedisHelper::destroyDataWithArray('my:team:user_id', $allTeamPlayer_id);
            }
            RedisHelper::destroyData('team:detail:team_id:' . $request->team_id);
            RedisHelper::destroyWithPattern('my:team:user_id:' . $team->leader_id . ':');
            RedisHelper::destroyWithPattern('my:team:user_id:' . $request->users_id . ':');
            RedisHelper::destroyWithPattern('team:list:page:');

    		return response()->json([
    			'status'    => true,
    			'message'   => Lang::get('message.team.join.accept')
    		],201);
    	} catch (\Exception $e) {
            DB::rollBack();
    		return response()->json([
    			'status'    => false,
    			'message'   => Lang::get('message.team.denied'),
    			'error'     => $e
    		], 409);
    	}
    }
}
