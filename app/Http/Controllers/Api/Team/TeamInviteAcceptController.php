<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use Illuminate\Http\Request;
use App\Model\TeamPlayerModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\CheckBanned;
use App\Http\Controllers\Api\IdentityGameController;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;
use App\Model\TeamPlayerNewModel;

/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamInviteAcceptController extends Controller
{

    /**
     * Accept Invitation to The Team
     *
     * @authenticated
     * @urlParam inv_id required The id of invitation. Example: 5f64365bc766a
     *
     * @response 404 {
            "status": false,
            "message": "Permintaan tidak ditemukan"
        }
     *
     * @response 409 {
            "status": false,
            "message": "Akses ditolak, coba lagi!",
            "error": "message"
        }
     *
     * @response {
            "status": true,
            "message": "Undangan bergabung berhasil disetujui"
        }
     */
	public function acceptInvite($request_id)
    {
    	$TeamHelper = new TeamHelperController;
        $request = $TeamHelper->foundRequestInvite($request_id);

        if (!is_null($request->original)) {
            return $request;
        }

        $team = $TeamHelper->foundTeam($request->team_id);
        if (!is_null($team->original)) {
            return $team;
        }

        // start - old statement
        $isFull = $TeamHelper->isFull($team);
        if (!is_null($isFull)) { //check team is full
            return $isFull;
        }
        // end - old statement

        // start - new statement
        if (config("app.team_player_new")) {
            $isFull = $TeamHelper->isFullv2($team);
            if($isFull){
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.full')
                ], $this->HTTP_BAD);
            }
        }
        // end - new statement

        if (Auth::user()->user_id != $request->users_id) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        if ($request->status != 1) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }

        // start - old statement
        $hasTeam = $TeamHelper->hasTeam($team->game_id, $request->users_id /*optional user*/);
        if (!is_null($hasTeam)) { //check has team in this game
            return $hasTeam;
        }
        // end - old statement

        // start - new statement
        if (config("app.team_player_new")) {
            $hasTeam = $TeamHelper->hasTeamV2($team->game_id, $request->users_id);
            if($hasTeam){
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.has')
                ], $this->HTTP_BAD);
            }
        }
        // end - new statement

        $hasIdentity = (new IdentityGameController)->hasIdentity($team->game_id, $request->users_id/*optional*/);
        if (!is_null($hasIdentity)) { //check if has identity game
            return $hasIdentity;
        }

        $checkValidation = (new CheckBanned)->isSoloBanned($team->game_id, null, $request->users_id);
        if (!is_null($checkValidation)) return $checkValidation; /** check the account game is banned ? | solo banned */

        try {
            DB::beginTransaction();
    		$team = TeamModel::find($request->team_id);

            // start - old statement
    		$team_player = TeamPlayerModel::find($request->team_id);

    		if ($team->game->player_on_team == count($team->primaryPlayer())) { //jika pemain inti sudah penuh
    			$player_temp = $team->substitutePlayer();
    			array_push($player_temp, $request->users_id);
    			$team_player->substitute_id = implode(',', $player_temp);
    		} else { //jika pemain inti belum penuh
    			$player_temp = $team->primaryPlayer();
    			array_push($player_temp, $request->users_id);
    			$team_player->player_id = implode(',', $player_temp);
    		}

    		$team_player->save(); //save data player
            // end - old statement

            // start - new statement
            if (config("app.team_player_new")) {

                $primaryPlayer_size = TeamPlayerNewModel::where('team_id',$team->team_id)->whereIn('player_status',[1,2])->count();

                $player_status = $team->game->player_on_team == $primaryPlayer_size ? 3 : 2;

                TeamPlayerNewModel::create([
                    'team_id' => $team->team_id,
                    'player_id' => $request->users_id,
                    'player_status' => $player_status,
                ]);

            }
            // end - new statement

    		$request->status = 2;
    		$request->save(); //update request to acc
            DB::commit();

           	//send notif
    		$Notification = new UserNotificationController('accept_invite', $request);

            RedisHelper::destroyWithPattern('my:team:user_id:' . Auth::user()->user_id . ':');

            // start - old statement
            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            // end - old statement

            // start - new statement
            if (config("app.team_player_new")) {
                $allTeamPlayer_id = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->get()->pluck('player_id')->all();
                RedisHelper::destroyDataWithArray('my:team:user_id', $allTeamPlayer_id);
            }
            // end - new statement


            RedisHelper::destroyData('team:detail:team_id:' . $request->team_id);
            RedisHelper::destroyWithPattern('team:list:page:');

    		return response()->json([
    			'status'    => true,
    			'message'   => Lang::get('message.team.invite.accept')
    		],201);
    	} catch (\Exception $e) {
            DB::rollBack();
    		return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
    	}
    }
}
