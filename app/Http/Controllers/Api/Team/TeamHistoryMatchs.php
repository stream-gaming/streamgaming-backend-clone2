<?php

namespace App\Http\Controllers\Api\Team;
Use App\Transformer\TeamHistoryTransform;
use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use Illuminate\Http\Request;
use App\Model\BracketFinal;
use App\Serializer\TeamSerialize;
use App\Model\BracketOneTwoEight;
use App\Model\BracketQuarterFinal;
use App\Model\BracketSemiFinal;
use App\Model\BracketSixteen;
use App\Model\BracketSixtyFour;
use App\Model\BracketThirdPlace;
use App\Model\BracketThirtyTwo;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\LeaderboardScoreTeamModel;
use App\Transformer\TeamHistoryTransformLeaderboard;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use React\EventLoop\Factory;

use function PHPUnit\Framework\isNull;

class TeamHistoryMatchs extends Controller
{
    public function index($id_team, $limit=0, $SystemKompetisi = null){
        $data = [];
        //incase for multisystem
        $teamgamedata   =  $SystemKompetisi != null ? true : TeamModel::with('game')->where('team_id',$id_team)->first();

        if(!is_null($teamgamedata) ){
        //incase for multisystem
        $SystemKompetisi= $SystemKompetisi != null ? $SystemKompetisi : $teamgamedata->game->system_kompetisi;
        if($SystemKompetisi == "Bracket"){
            $round128 = (new BracketOneTwoEight)->HistoryTeam($id_team);
            if($round128){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round128);
            }
            $round64 = (new BracketSixtyFour)->HistoryTeamNext($id_team);
            if($round64){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round64);
            }
            $round64 = (new BracketSixtyFour)->HistoryTeam($id_team);
            if($round64){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round64);
            }
            $round32 = (new BracketThirtyTwo)->HistoryTeamNext($id_team);
            if($round32){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round32);
            }
            $round32 = (new BracketThirtyTwo)->HistoryTeam($id_team);
            if($round32){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round32);
            }
            $round16 = (new BracketSixteen)->HistoryTeamNext($id_team);
            if($round16){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round16);
            }
            $round16 = (new BracketSixteen)->HistoryTeam($id_team);
            if($round16){
                $data[]  = (new TeamHistoryTransform)->HistoryTeamBracket($round16);
            }
            $round8 = (new BracketQuarterFinal)->HistoryTeamNext($id_team);
            if($round8){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round8);
            }
            $round8 = (new BracketQuarterFinal)->HistoryTeam($id_team);
            if($round8){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round8);
            }
            $round4 = (new BracketSemiFinal)->HistoryTeamNext($id_team);
            if($round4){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round4);
            }
            $round4 = (new BracketSemiFinal)->HistoryTeam($id_team);
            if($round4){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round4);
            }
            $round3 = (new BracketThirdPlace)->HistoryTeamNext($id_team);
            if($round3){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round3);
            }
            $round2 = (new BracketFinal)->HistoryTeamNext($id_team);
            if($round2){
                $data[] = (new TeamHistoryTransform)->HistoryTeamBracket($round2);
            }
            $rows = (new TeamHistoryTransform)->HistoryBracketTeam($data);
            return [
                'data' => $rows
            ];
            // return $rows;
        }else if($SystemKompetisi == "LeaderBoards"){

                $history = LeaderboardScoreTeamModel::with('LeaderboardScoreSequence')
                        ->with('Leaderboard')
                        ->where('id_team',$id_team);

                if($limit > 0){
                    $history= $history->limit($limit);
                }

                $history= $history->get();


            // dd($history);
            $resource =  new Collection($history, (new TeamHistoryTransformLeaderboard));

            $result   = (new Manager())->createData($resource)->toArray();
            return  $result;

            }else if($SystemKompetisi == 'Multisystem'){
                return ['data' => []];
            }
        }

    }

    public function multiSystem($id_team, $limit=0){
            try{

                $getBracket = $this->index($id_team, $limit=0, $SystemKompetisi='Bracket');
                $getLeaderboard = $this->index($id_team, $limit=0, $SystemKompetisi='LeaderBoards');

                $data['bracket'] = $getBracket['data'] ? $getBracket['data'] : [];
                $data['leaderboard'] = $getLeaderboard['data'] ? $getLeaderboard['data'] : [];

                return $data;
            }catch(\Exception $e){

                return [
                    'bracket' => [],
                    'leaderboard' => []
                ];
            }
    }

    public function PlayerMatchs($id_team){
       
        $teamgamedata   = TeamModel::with('game')->where('team_id',$id_team)->first();


        if(!is_null($teamgamedata) ){
        $SystemKompetisi=$teamgamedata->game->system_kompetisi;
         if($SystemKompetisi == "Bracket"){
       
            $dataRound3     = (new BracketOneTwoEight())->HistoryTeam($id_team);
            $dataRound      = (new BracketSixtyFour())->HistoryTeamNext($id_team);
            $dataRound4     = (new BracketSixtyFour())->HistoryTeam($id_team);
            $dataRound5     = (new BracketThirtyTwo())->HistoryTeamNext($id_team);
            $dataRound6     = (new BracketThirtyTwo())->HistoryTeam($id_team);
            $dataRound7     = (new BracketSixteen())->HistoryTeamNext($id_team);
            $dataRound8     = (new BracketSixteen())->HistoryTeam($id_team);
            $dataRound9     = (new BracketQuarterFinal())->HistoryTeamNext($id_team);
            $dataRound10    = (new BracketQuarterFinal())->HistoryTeam($id_team);
            $dataRound11    = (new BracketSemiFinal())->HistoryTeamNext($id_team);
            $dataRound12    = (new BracketSemiFinal())->HistoryTeam($id_team);
            $dataRound13    = (new BracketThirdPlace())->HistoryTeamNext($id_team);
            $dataRound14    = (new BracketFinal())->HistoryTeamNext($id_team);
            
            $collection = collect([$dataRound,$dataRound3,$dataRound4,$dataRound5,$dataRound6,$dataRound7,$dataRound8,$dataRound9,$dataRound10,$dataRound11,$dataRound12,$dataRound13,$dataRound14])->collapse()->all();
            $data = (new TeamHistoryTransform)->HistoryTeamBracket($collection); 
            return $data;
        }else if($SystemKompetisi == "LeaderBoards"){


                $history = LeaderboardScoreTeamModel::with('LeaderboardScoreSequence')
                ->with('Leaderboard')
                ->where('id_team',$id_team)
                ->get();
            $resource =  new Collection($history, (new TeamHistoryTransformLeaderboard));

            $result   = (new Manager())->createData($resource)->toArray();
            return  $result;
            }
        }

    }

}
