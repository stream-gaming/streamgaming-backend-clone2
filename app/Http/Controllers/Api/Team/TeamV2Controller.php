<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamRequestModel;
use App\Transformer\AccountTransform;
use App\Transformer\DetailTeamV2Transform;
use App\Transformer\TeamRequestTransform;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use InvalidArgumentException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @group Team
 *
 * APIs for managing team.
 */
class TeamV2Controller extends Controller
{
    /**
     * Get Detail My Team V2.
     * <aside class="notice">APIs Method for Detail Team Information | Endpoint untuk melihat detail Team</aside>
     * @urlParam team_id required The Url of the team. Example: s2qHFzf0h2eNGtTTZ-kO
     * @responseFile status=200 scenario="not login" responses/team.detail.v2.json
     * @responseFile status=200 scenario="login not in team" responses/team.detail.v2.login.json
     * @responseFile status=200 scenario="login in team" responses/team.detail.v2.login-myteam.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Team not found"
        }
     */
    public function getDetailTeam($hash)
    {
        $team_id        = (new \App\Helpers\MyApps)->onlyDecrypt($hash);
        $team           = TeamModel::select('team_id',
                                            'team_description',
                                            'is_banned',
                                            'ban_until',
                                            'leader_id',
                                            'team_name',
                                            'venue',
                                            'team_logo',
                                            'game_id',
                                            'created_at'
                                        )->where('team_id',$team_id)->first();

        if ($cache = Redis::exists('team:detail:team_id:' . $team_id)) {
            $cache = json_decode(Redis::get('team:detail:team_id:' . $team_id));
            if (!Auth::check()) {
                $cache->data->id       = null;
                $cache->data->fullname = null;
                $cache->data->username = null;
                $cache->data->is_in_team = false;
                $cache->data->team->is_leader = false;
                $cache->data->team->request = [];

                return response()->json($cache);
            }
            $user = Auth::user();

            $cache->data->id       = $user->user_id;
            $cache->data->fullname = $user->fullname;
            $cache->data->username = $user->username;
            $cache->data->is_in_team = (new TeamHelperController)->isMemberV2($team, $user->user_id);
            $cache->data->team->is_leader = $team->leader_id == $user->user_id ? true : false;

            $user_request = TeamRequestModel::where('team_id', $team->team_id)
                                        ->where('users_id', $user->user_id)
                                        ->where('status', 1)
                                        ->count();

            $cache->data->is_waiting = boolval($user_request);

            if($cache->data->is_in_team){
                $request        = TeamRequestModel::where('team_id', $team->team_id)->orderByRaw('status = 0, status')->get();
                $response       = new Collection($request, new TeamRequestTransform);
                $cache->data->team->request = current((new Manager())->createData($response)->toArray());
            }

            return response()->json($cache);
        }
        try {

            if (is_null($team)) {
                throw new InvalidArgumentException(Lang::get('message.team.not'),$this->HTTP_NOT_FOUND);
            }

            $is_in_team = false;

            if(Auth::check()){
                $user_id = Auth::user()->user_id;
                $check_team = (new TeamHelperController)->isMember($team, $user_id);

                $is_in_team = true;

                if (!is_null($check_team)) {
                    $is_in_team = false;
                }

                if ((new TeamHelperController)->isMemberV2($team, $user_id)) {
                    if (!$is_in_team) {
                        Log::debug("conflict member team data with team id = $team->team_id and user_id = $user_id");
                    }
                } else {
                    Log::debug("conflict member team data with team id = $team->team_id and user_id = $user_id");
                }
            }

            $data = [
                'status'   => true,
                'data'     => (new DetailTeamV2Transform)->detail($team,$is_in_team),
            ];

            RedisHelper::setData('team:detail:team_id:' . $team_id, $data, 1 * (24 * 3600));

            return response()->json($data);

        } catch (\Throwable $exception) {

            $code = $exception->getCode() > 0 ? $exception->getCode() : $this->HTTP_CONFLIC;

            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ],$code);
        }
    }
}
// The End
