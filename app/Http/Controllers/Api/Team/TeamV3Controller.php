<?php

namespace App\Http\Controllers\Api\Team;

use App\Http\Controllers\Controller;
use App\Http\Requests\JoinTeamInvitationCodeRequest;
use App\Http\Requests\JoinTeamInvitationCodeSpecificRequest;
use App\Model\TeamModel;
use App\Modules\Team\Interfaces\InvitationCodeRepositoryInterface;
use App\Modules\Team\Interfaces\TeamServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
/**
 * @group Team
 *
 * APIs for managing team
 */
class TeamV3Controller extends Controller
{
    protected $teamService;

    public function __construct(TeamServiceInterface $teamService)
    {
        $this->teamService = $teamService;
    }

    /**
     * Join Using Invitation Code
     *
     * @authenticated
     * @bodyParam invitation_code required The id of the invitation code. Example: AS523JA7
     *
     * @response status=403 {
            "status": false,
            "message": "Anda sudah memiliki tim pada game ini"
        }
     *
     * @response status=500 {
            "status": false,
            "message": "Sedang terjadi gangguan pada server kami, silahkan coba beberapa saat lagi"
        }
     *
     * @response {
            "status": true,
            "message": "Berhasil bergabung ke dalam tim"
        }
     */
    public function joinUsingInvCode(
        JoinTeamInvitationCodeRequest $req,
        InvitationCodeRepositoryInterface $invCodeRepo
        ){

        try{
            $invitationCode = $invCodeRepo->findByCode($req->invitation_code);
            //Add player to the team
            $this->teamService->joinUsingInvCode($invitationCode, Auth::user());

            return response()->json([
                'status' => true,
                'message' => Lang::get('team.join_success')
            ],201);

        }catch(\Throwable $e){
            if($e->getCode() == 1){
                $joinTourLang = Lang::get('team.join_tournament');
                $invCodeExpLang = Lang::get('team.invitation_code.expired');

                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage() == $joinTourLang ? $invCodeExpLang : $e->getMessage()
                ],403);
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => Lang::get('server.error.500')
            ],500);

        }
    }

    /**
     * Join Using Invitation Code Specific
     *
     * @authenticated
     * @bodyParam team_id required The id of the team
     * @bodyParam invitation_code required The id of the invitation code. Example: AS523JA7
     *
     * @response status=403 {
            "status": false,
            "message": "Anda sudah memiliki tim pada game ini"
        }
     *
     * @response status=500 {
            "status": false,
            "message": "Sedang terjadi gangguan pada server kami, silahkan coba beberapa saat lagi"
        }
     *
     * @response {
            "status": true,
            "message": "Berhasil bergabung ke dalam tim"
        }
     */
    public function joinUsingInvCodeSpecific(
        JoinTeamInvitationCodeSpecificRequest $req,
        InvitationCodeRepositoryInterface $invCodeRepo
        ){

        try{
            $invitationCode = $invCodeRepo->findByCode($req->invitation_code);
            //Add player to the team
            $this->teamService->joinUsingInvCode($invitationCode, Auth::user());

            return response()->json([
                'status' => true,
                'message' => Lang::get('team.join_success')
            ],201);

        }catch(\Throwable $e){
            if($e->getCode() == 1){
                $joinTourLang = Lang::get('team.join_tournament');
                $invCodeExpLang = Lang::get('team.invitation_code.expired');

                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage() == $joinTourLang ? $invCodeExpLang : $e->getMessage()
                ],403);
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => Lang::get('server.error.500')
            ],500);

        }
    }

    /**
     * User Validation Before Joining Team
     *
     * @authenticated
     * @urlParam team required The id of the team. Example: 5eba6e85617de
     *
     * @response status=403 {
            "status": false,
            "message": "Anda sudah memiliki tim pada game ini"
        }
     *
     * @response status=500 {
            "status": false,
            "message": "Sedang terjadi gangguan pada server kami, silahkan coba beberapa saat lagi"
        }
     *
     * @response {
            "status": true,
            "message": "Memenuhi syarat untuk bergabung dengan tim ini"
        }
     */
    public function validateUserJoin(TeamModel $team){
        try{

            $this->teamService->validateUserJoin($team, Auth::user());

            return response()->json([
                'status' => true,
                'message' => Lang::get('team.user_join_accepted'),
            ]);

        }catch(\Throwable $e){

            if($e->getCode() == 1){
                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage()
                ],403);
            }

            //Get and Save error log
            Log::debug($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => Lang::get('server.error.500')
            ],500);

        }
    }
}
