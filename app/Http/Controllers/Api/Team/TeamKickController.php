<?php

namespace App\Http\Controllers\Api\Team;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Http\Controllers\Api\UserNotificationController;
use App\Model\TeamPlayerNewModel;
use Illuminate\Support\Facades\Auth;


/**
 * @group Team
 *
 * APIs for managing team.
 */
class TeamKickController extends Controller
{
    private $kickTeamV2 = false;
    /**
     * Kick Member Team
     *
     * @urlParam team_id required The ID of the team. Example: 5f62f0f64a2d3
     * @bodyParam users_id required The ID user to kick from team. Example: 5f6122164a
     *
     * @response status=400 {
            "status": false,
            "message": "Anda bukan pemimpin tim"
        }
     * @response status=400 {
            "status": false,
            "message": "Pengguna bukan anggota tim"
        }
     *
     * @response status=200 {
            "status": true,
            "message": "Anggota tim berhasil dikeluarkan"
        }
     *
     */
    public function kick(Request $request, $team_id)
    {
       
        $TeamHelper = new TeamHelperController;
        $TeamHelperNew  = new TeamPlayerHelperController;
        $team = $TeamHelper->foundTeam($team_id);
     
        if (!is_null($team->original)) {
            return $team;
        }

        $isLeader = $TeamHelper->isLeader($team);
        if (!is_null($isLeader)) {
            return $isLeader;
        }

        $isMember = $TeamHelper->isMember($team, $request->users_id);
        if (!is_null($isMember)) {
            return $isMember;
        }

        if ($request->users_id == $team->leader_id or is_null($request->users_id)) { //jika mencoba kick diri sendiri (leader)
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied')
            ], $this->HTTP_BAD);
        }
      
        if ($team->isPrimaryPlayer($request->users_id)) { //jika pemain inti
            $player = $team->primaryPlayer();
            $key = array_search($request->users_id, $player);
            unset($player[$key]);

            $player = implode(',', $player);
            $team->primary_temp1 = (empty($player)) ? null : $player;
            $team->primary_temp = $player;
            $team->substitute_temp = $team->substitute_temp1 = (is_null($team->substitutePlayer())) ? [] : implode(',', $team->substitutePlayer());

            /** Jika pemain cadangan tidak kosong
             * Pindahkan salah satu cadangan ke inti
             */
            if (!empty($team->substitute_temp)) {
                $pr_player = explode(',', trim($team->primary_temp, ','));
                $sb_player = TeamPlayerNewModel::where('team_id',$team_id)->where('player_status','3')->get()->pluck('player_id')->all();

                $fdata_primary_player = collect($pr_player);
                $fdata_primary_player->push($sb_player[0]);
                $fdata_primary_player = implode(',', $fdata_primary_player->all());
                $team->primary_temp = $fdata_primary_player;

                $fdata_substitute_player = collect($sb_player);
                $fdata_substitute_player->shift();
                $fdata_substitute_player = implode(',', $fdata_substitute_player->all());
                $team->substitute_temp = $fdata_substitute_player;

                $this->kickTeamV2 = true;
            }

            $isPri = true;
        } else if ($team->isSubstitutePlayer($request->users_id)) { //jika pemain cadangan
            $player = $team->substitutePlayer();
            $key = array_search($request->users_id, $player);
            unset($player[$key]);

            $player = implode(',', $player);
            $team->substitute_temp1 = (empty($player)) ? null : $player;
            $team->substitute_temp = $player;
            $isSub = true;
        }
      
        $team->users_dest = $request->users_id;
        
        try {
            DB::beginTransaction();
            if ($this->kickTeamV2) {
                $team->player->player_id = $team->primary_temp;
                $team->player->substitute_id = $team->substitute_temp;
                $team->player->save();
            } elseif (isset($team->primary_temp)) {
                $team->player->player_id = $team->primary_temp1;
                $team->player->save();
            } elseif (isset($team->substitute_temp)) {
                $team->player->substitute_id = $team->substitute_temp1;
                $team->player->save();
            }
          
            if(config('app.team_player_new')){ 
              
                $isMember = $TeamHelperNew->isMember($request->users_id, $team->team_id);
                if (!$isMember) {
                    return response()->json([
                        'status'    => false,
                        'message'   => Lang::get('message.team.not-member')
                    ], $this->HTTP_BAD);
                }
             
                if ($request->users_id == $team->leader_id or is_null($request->users_id)) { //jika mencoba kick diri sendiri (leader)
                    return response()->json([
                        'status'    => false,
                        'message'   => Lang::get('message.team.denied')
                    ], $this->HTTP_BAD);
                }
             
                if ($TeamHelperNew->isPrimaryPlayer($request->users_id,$team->team_id) > 0) {
                    $get = TeamPlayerNewModel::where('player_status','2')
                                    ->where('team_id', $team->team_id)
                                    ->where('player_id', $request->users_id);
                    $getPlayer =    $get->first();
                                    $get->forceDelete();  
                    if($TeamHelperNew->substitutePlayer($team->team_id)){
                        $substitutePlayer = $TeamHelperNew->singleSubstitutePlayer($team->team_id);
                        $ChangePrimary = TeamPlayerNewModel::find($substitutePlayer->id);
                        $ChangePrimary->player_status = '2';
                        $ChangePrimary->save();
                    }
                } else if ($TeamHelperNew->isSubstitutePlayer($request->users_id,$team->team_id) > 0 ) { //jika pemain cadangan
                    $get =TeamPlayerNewModel::where('player_status','3')
                                        ->where('team_id', $team->team_id)
                                        ->where('player_id', $request->users_id); 
                    $getPlayer =    $get->first();
                                    $get->forceDelete();
                }
                 
                $team->users_dest = $getPlayer->player_id;
            }
           
            DB::commit();
            $Notification = new UserNotificationController('kick_team', $team);

            RedisHelper::destroyData('my:team:user_id:' . Auth::user()->user_id . ':list');

            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());
            
            if (config("app.team_player_new")) {
                $allTeamPlayer_id = TeamPlayerNewModel::where('team_id',$team->team_id)->select('player_id')->get()->pluck('player_id')->all();
                RedisHelper::destroyDataWithArray('my:team:user_id', $allTeamPlayer_id);
            }
            RedisHelper::destroyData('team:detail:team_id:' . $team_id);
            RedisHelper::destroyWithPattern('my:team:user_id:' . $request->users_id . ':');
            RedisHelper::destroyWithPattern('team:list:page:');

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.team.kick')
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.team.denied'),
                'error'     => $e
            ], 409);
        }
    }
}
