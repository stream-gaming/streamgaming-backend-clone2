<?php

namespace App\Http\Controllers\Api\Leaderboard;

use App\Helpers\Validation\CheckAvailableGroup;
use App\Helpers\Validation\checkBalanceTicketTeam;
use App\Helpers\Validation\FreeRegist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\Helpers\Validation\hasJoinedTournament;
use App\Helpers\Validation\isSlotFull;
use App\Helpers\Validation\RegistMemberByCashOrPoint;
use App\Helpers\Validation\RegistTeamByTicket;
use App\Model\LeaderboardModel;
use App\Model\TournamentModel;

class JoinLeaderboardController extends Controller
{

    //
    public function join()
    {
        $user = Auth::user();

        //hasJoinedTournament
        // $validate = new hasJoinedTournament();


        // return $validate->check($user->user_id, '5f69b88dc06b1');


        // $validate = new isSlotFull();

        // return $validate->check('5f7562ac34b07', 'Leaderboard');


        // $validate = new RegistTeamByTicket();
        $validate = new RegistMemberByCashOrPoint();
        // $validate = new CheckAvailableGroup();
        // try{

        //     $tournament = LeaderboardModel::where('id_leaderboard', '5f6ee9a4a5651')->first();

        // }catch(\Exception $e){

        //     return false;
        // }

        try {

            $tournament = LeaderboardModel::where('id_leaderboard', '5f75659c43175')->first();
        } catch (\Exception $e) {

            return false;
        }


        // return $validate->regist($tournament,'5ebf915fd7dbc', '5f6eaf3503de3', '1', 'Bracket');
        return $validate->register($tournament, '5ebf915fd7dbc', 'Leaderboard', 'cash');
        // return $validate->check($tournament);

    }
}
