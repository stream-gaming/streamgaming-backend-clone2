<?php

namespace App\Http\Controllers;

use App\Model\ScoreMatchsModel;
use App\Model\IdentityGameModel;
use Illuminate\Http\Request;
use App\Helpers\Validation\hasJoinedOtherTournament;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\TeamModel;
class ScoreMatchsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_team, $id_user)
    {
        // $data = "";
        $teamgamedata   = TeamModel::with('game')->where('team_id',$id_team)->first();
        $SystemKompetisi=$teamgamedata->game->system_kompetisi;
        if($SystemKompetisi == "Bracket"){
            try {
                $data = (new ScoreMatchsModel)->ScoreMatchs_User($id_team,$id_user);
            } catch (\Throwable $th) {
                $data =   [
                    'kills' => (int) '0',
                    'death' => (int) '0'
                ];
            }
        }else if($SystemKompetisi == "LeaderBoards") {


            $getPlayerData = LeaderboardScoreMatchModel::where('id_player',$id_user)
                                                            ->where('id_team',$id_team)
                                                            ->groupBy('id_team')
                                                            ->sum('jumlah_kill');
            // dd($getPlayerData);
            if($getPlayerData){
            $data =   [
                'kills' => (int) $getPlayerData,
                'death' => (int) '0'
            ];
            }else{
                $data =   [
                    'kills' => (int) '0',
                    'death' => (int) '0'
                ];
            }

        }else if($SystemKompetisi == "Multisystem") {

            $data =   [
                'kills' => (int) '0',
                'death' => (int) '0'
            ];
        }

        return $data;
    }

    public function multiSystem($id_team, $id_user)
    {


            try {
                $data['bracket'] = (new ScoreMatchsModel)->ScoreMatchs_User($id_team,$id_user);
            } catch (\Throwable $th) {
                $data['bracket'] =   [
                    'kills' => (int) '0',
                    'death' => (int) '0'
                ];
            }



            $getPlayerData = LeaderboardScoreMatchModel::where('id_player',$id_user)
                                                            ->where('id_team',$id_team)
                                                            ->groupBy('id_team')
                                                            ->sum('jumlah_kill');
            // dd($getPlayerData);
            if($getPlayerData){
                $data['leaderboard'] =   [
                    'kills' => (int) $getPlayerData,
                    'death' => (int) '0'
                ];
            }else{
                $data['leaderboard'] =   [
                    'kills' => (int) '0',
                    'death' => (int) '0'
                ];
            }



        return $data;
    }

    public function tes($user_id)
    {
        $datass = IdentityGameModel::with('game')->where('users_id',$user_id)->get();
        $data = [
            "game" => []
        ];
        for($i=0;$i<count($datass);$i++) {
            $data['game'][$i][] = $datass[$i]['game']['name'];
        }
        return $data;
    }
    public function tes2($id_player,$tournament_date){
        $hasJoinedOtherTournament = (new hasJoinedOtherTournament)->check($id_player, $tournament_date);
        if($hasJoinedOtherTournament == true){
            return "True";
        }else if ($hasJoinedOtherTournament == false){
            return "False";
        }
        // return $hasJoinedOtherTournament;
    }

}
