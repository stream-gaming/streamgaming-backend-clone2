<?php

namespace App\Http\Controllers;

use App\Model\TournamentLiga;
use App\Model\TournamentListModel;
use App\Transformer\TournamentLigaTransform;
use App\Transformer\TournamentListTransform;
use Carbon\Carbon;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class TournamentListNew extends Controller
{

    private $date;
    private $dateformat;
    private $dateweekstart;
    private $dateweekend;

    public function __construct()
    {
        $this->date = new Carbon();
        $this->dateformat =  'Y-m-d H:i';
        $this->dateweekstart = $this->date->now()->startOfWeek(Carbon::MONDAY)->format($this->dateformat);
        $this->dateweekend  = $this->date->now()->endOfWeek(Carbon::SUNDAY)->format($this->dateformat);
    }

    /**
     * Tournament List V 2.0
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need,
     * with 10 datas per group
     * @queryParam limit retrieve data with limit per group. Default to 10. Example:5
     * @queryParam payment Filter by payment type (free,pay). Defaults to 'all'. Example: free
     * @queryParam games Filter by games (id_games). Defaults to 'all'. Example: 501085427
     * @queryParam status Filter by status tournament [pending=0], [open_registration=1], [end_registration=2], [starting=3], [complete=4]). Defaults to 'all'. Example: 1
     * @queryParam mode Filter by mode tournament (solo, duo, or squad). Defaults to 'all'. Example: solo
     * @queryParam search string Search tournament by title. No-example
     * @queryParam time string to retrieve data by time (this_week, upcoming, example). No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.new.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function allTouranament(Request $request)
    {



        $games    = filter_var($request->games, FILTER_SANITIZE_STRING);
        $status   = $request->status != null ? filter_var($request->status, FILTER_SANITIZE_STRING) : null;
        $mode     = filter_var($request->mode, FILTER_SANITIZE_STRING);
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);
        $time     = filter_var($request->time, FILTER_SANITIZE_STRING);
        $limit    = $request->limit != null ? $request->limit : 10;


        if($time == null || $time=='this_week'){
            $this_week = $this->this_week($games,$status,$mode,$search,$limit);
        }
        if($time == null || $time=='upcoming'){
            $upcoming = $this->upcoming($games,$status,$mode,$search,$limit);
        }
        if($time == null || $time=='last'){
            $last = $this->last($games,$status,$mode,$search,$limit);
        }
        if($time == null){
            $liga = $this->getLiga($games,$status,$mode,$search,$limit);
        }


        if($time == null){
            $result = [
                'this_week'         => $this_week,
                'upcoming'          => $upcoming,
                'last'              => $last,
                'tournament_liga'   => $liga
            ];
        }
        else{
            if($time=='this_week'){
                $result = $this_week;
            }
            else if($time=='upcoming'){
                $result = $upcoming;
            }
            else{
                $result = $last;
            }
        }



        return response()->json($result);
    }

    public function this_week($games,$status,$mode,$search,$limit)
    {
         $datas = TournamentListModel::with(['AvailableSlots' => function ($q)
                                    {
                                        $q->select('id_leaderboard','total_team')
                                          ->where('qualification_sequence',1);

                                    }])
                                    ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                                        FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                                            FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                                        GROUP BY tournament_id) prizes'),function($join){
                                                            $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                                        })
                                    ->where('statuss','!=',5);

        if($games!='all' && $games!=null){
            $datas->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $datas->where('statuss',$status);
        }else{
            $datas->where('statuss','!=',4);
        }

        if($mode!='all' && $mode!=null){
            $datas->where('game_option',$mode);
        }

        if($search!=null){
            $datas->where('tournament_name','LIKE','%'.$search.'%');
        }

        $retrieve = $datas->with(['game' => function ($q)
                            {
                                $q->select('id_game_list','name','games_on','logo_game_list');

                            }]);

        if($status != 4){
            $ordered = $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
        }
        else{
            $ordered = $retrieve->orderBy('tournament_date','desc');
        }


        $resource = $ordered->whereBetween('tournament_date',[$this->dateweekstart,$this->dateweekend])->limit($limit)->get();

        $tournament = new Collection($resource, (new TournamentListTransform));
        // - Buat Pagination
        return current((new Manager())->createData($tournament)->toArray());


    }

    public function upcoming($games,$status,$mode,$search,$limit)
    {
        try {
            $datas = TournamentListModel::with(['AvailableSlots' => function ($q)
                                       {
                                           $q->select('id_leaderboard','total_team')
                                             ->where('qualification_sequence',1);

                                       }])
                                       ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                                           FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                                               FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                                           GROUP BY tournament_id) prizes'),function($join){
                                                               $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                                           })
                                       ->where('statuss','!=',5);

           if($games!='all' && $games!=null){
               $datas->whereHas('game',function ($q) use($games)
               {
                   return $q->where('id_game_list',$games);
               });
           }

           if($status!='all' && $status!=null){
               $datas->where('statuss',$status);
           }else{
               $datas->where('statuss','!=',4);
           }

           if($mode!='all' && $mode!=null){
               $datas->where('game_option',$mode);
           }

           if($search!=null){
               $datas->where('tournament_name','LIKE','%'.$search.'%');
           }

           $retrieve = $datas->with(['game' => function ($q)
                               {
                                   $q->select('id_game_list','name','games_on','logo_game_list');

                               }]);

           if($status != 4){
               $ordered = $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
           }
           else{
               $ordered = $retrieve->orderBy('tournament_date','desc');
           }


           $resource   = $ordered->where('tournament_date','>',$this->dateweekend)->limit($limit)->get();

           $tournament = new Collection($resource, (new TournamentListTransform));
           // - Buat Pagination
           return current((new Manager())->createData($tournament)->toArray());

        } catch (\Exception $e) {
            return [];
        }



    }

    public function last($games,$status,$mode,$search,$limit)
    {
         $datas = TournamentListModel::with(['AvailableSlots' => function ($q)
                                    {
                                        $q->select('id_leaderboard','total_team')
                                          ->where('qualification_sequence',1);

                                    }])                                    ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                    FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                        FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                    GROUP BY tournament_id) prizes'),function($join){
                                        $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                    })
                                    ->where('statuss','!=',5);

        if($games!='all' && $games!=null){
            $datas->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $datas->where('statuss',$status);
        }
        // else{
        //     $datas->where('statuss','!=',4);
        // }

        if($mode!='all' && $mode!=null){
            $datas->where('game_option',$mode);
        }

        if($search!=null){
            $datas->where('tournament_name','LIKE','%'.$search.'%');
        }

        $retrieve = $datas->with(['game' => function ($q)
                            {
                                $q->select('id_game_list','name','games_on','logo_game_list');

                            }]);

        if($status != 4){
            $ordered = $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
        }
        else{
            $ordered = $retrieve->orderBy('tournament_date','desc');
        }

        $resource   =  $ordered->where('tournament_date','<',$this->dateweekstart)->limit($limit)->get();

        $tournament = new Collection($resource, (new TournamentListTransform));
        // - Buat Pagination
        return current((new Manager())->createData($tournament)->toArray());
    }

    public function finished($games,$mode,$search,$limit)
    {
        $status=4;
         $datas = TournamentListModel::with(['AvailableSlots' => function ($q)
                                    {
                                        $q->select('id_leaderboard','total_team')
                                          ->where('qualification_sequence',1);

                                    }])
                                    ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                    FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                        FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                    GROUP BY tournament_id) prizes'),function($join){
                                        $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                    })
                                    ->where('statuss','!=',5);

        if($games!='all' && $games!=null){
            $datas->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $datas->where('statuss',$status);
        }
        // else{
        //     $datas->where('statuss','!=',4);
        // }

        if($mode!='all' && $mode!=null){
            $datas->where('game_option',$mode);
        }

        if($search!=null){
            $datas->where('tournament_name','LIKE','%'.$search.'%');
        }

        $retrieve = $datas->with(['game' => function ($q)
                            {
                                $q->select('id_game_list','name','games_on','logo_game_list');

                            }]);



        $ordered = $retrieve->orderBy('tournament_date','desc');

        $resource   =  $ordered->where('tournament_date','<',$this->dateweekstart)->limit($limit)->get();

        $tournament = new Collection($resource, (new TournamentListTransform));
        // - Buat Pagination
        return current((new Manager())->createData($tournament)->toArray());
    }

    public function getLiga($games,$status,$mode,$search,$limit)
    {
        $data = TournamentLiga::with(['tournament' => function ($q) use($games,$status,$mode,$search)
        {
            $q->with(['AvailableSlots' => function ($q)
                        {
                        $q->select('id_leaderboard','total_team')
                        ->where('qualification_sequence',1);
                        }])->limit(10);

                        if($games!='all' && $games!=null){
            $q->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
            }

            if($status!='all' && $status!=null){
                $q->where('statuss',$status);
            }
            // else{
            //     $q->where('statuss','!=',4);
            // }

            if($mode!='all' && $mode!=null){
                $q->where('game_option',$mode);
            }

            if($search!=null){
                $q->where('tournament_name','LIKE','%'.$search.'%');
            }

            $retrieve = $q->with(['game' => function ($q)
                                {
                                    $q->select('id_game_list','name','games_on','logo_game_list');

                                }]);

            if($status != 4){
                $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
            }
            else{
                $retrieve->orderBy('tournament_date','desc');
            }
        }])->get();

        $tournament = new Collection($data,(new TournamentLigaTransform));

        $datas = current((new Manager)->createData($tournament)->toArray());
        // return response()->json($datas);
        return $datas;
    }


}
