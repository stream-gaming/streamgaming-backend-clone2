<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $HTTP_OK                  = 200;
    protected $HTTP_BAD                 = 400;
    protected $HTTP_CREATE              = 201;
    protected $HTTP_CONFLIC             = 409;
    protected $HTTP_NOT_FOUND           = 404;
    protected $HTTP_TOO_MANY_REQUESTS   = 429;
    protected $HTTP_UNAUTHORIZED        = 401;
}
