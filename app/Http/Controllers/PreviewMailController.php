<?php

namespace App\Http\Controllers;

use App\Model\User;
use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

/**
 * @hideFromAPIDocumentation
 */
class PreviewMailController extends Controller
{
	public function verify(Request $request)
	{
        $user = User::find('5ea1b790c7da0');
		return (new VerifyEmail())->toMail($user);
    }

	public function forget(Request $request)
	{
        $user = User::find('5ea1b790c7da0');
		return (new ResetPassword($user->user_id))->toMail($user);
	}

}
