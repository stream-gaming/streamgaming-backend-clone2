<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatusCodeController extends Controller
{
    function example(Int $code) {
        try {
            return response()->json([
                'message' => 'contoh status code '.$code
            ], $code);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'status code '.$code.' tidak ada'
            ], 404);
        }
    }
}
