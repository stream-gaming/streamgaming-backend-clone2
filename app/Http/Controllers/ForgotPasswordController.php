<?php

namespace App\Http\Controllers;

use App\Auth\Passwords\PasswordBrokerManager;
use App\Http\Requests\ValidateForgotPassword;
use App\Http\Requests\ValidateRequestEmail;
use App\Model\SendMailLog;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\ValidateResetPassword;

/**
 * @group Authentication
 *
 * APIs for the Reset Password process
 */
class ForgotPasswordController extends AuthController
{

    //- Batasan Percobaan
    protected $maxAttempts = 1;

    //- Waktu Blokir Per Menit
    protected $decayMinutes = 2;

    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Request Reset Password.
     * <aside class="notice">APIs Method for user request reset password | Endpoint untuk pengguna meminta lupa password</aside>
     * @bodyParam email email required Email | Email pengguna. Example: melly.tiara92@gmail.com
     * @response scenario="success send link" status="200" {
            "status": true,
            "email": "melly.tiara92@gmail.com",
            "message": "We have emailed your password reset link!",
         }
     * @response scenario="email not registered" status="400" {
            "status": false,
            "message": "Email Not Registered."
        }
     * @response scenario="email not verify" status="400" {
            "status": false,
            "message": "Please Verify Email."
        }
     * @response scenario="failed sent" status="400"  {
            "status" : false,
            "message" : "There was an error sending password reset email."
        }
     * @response scenario="too many requests" status="429" {
            "status": false,
            "time": 148,
            "waiting": true,
            "message": "Too many Requests attempts. Please try again in 148 seconds."
        }
     */
    public function resetPasswordRequest(ValidateRequestEmail $request)
    {
        $request->type = 'forget';
        //- Batas Permintaan 1x Untuk 2 Menit
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendResponse($request);
        }

        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('auth.login.not')
            ], $this->HTTP_NOT_FOUND);
        } else if ($user->is_verified == 0) {
            //- Jika Email Belum di verifikasi
            return response()->json([
                'status'       => false,
                'verification' => true,
                'message'      => Lang::get('auth.login.verify')
            ], $this->HTTP_BAD);
        }
        // - Panggil Method Reset Link
        $response = $this->broker()->sendResetLink($request->only('email'));
        // - Tambah Jumlah Permintaan Email
        $this->incrementLoginAttempts($request);
        // - Cek status
        if ($response !== Password::RESET_LINK_SENT) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('passwords.sent.error')
            ], $this->HTTP_BAD);
        }

        return response()->json([
            'status'  => true,
            'email'   => $request->input('email'),
            'message' => Lang::get('passwords.sent.success')
        ], $this->HTTP_OK);
    }

    /**
     * Handle Reset Password.
     * <aside class="notice">APIs Method for Handle Request reset password process | Endponint untuk menangani proses lupa password</aside>
     * @bodyParam token string required valid Token | token yang di dapat dari link. Example: 2274745be7b18832a5fe58d68facedf56082fde439a8e699966b68aa818d4a7b
     * @bodyParam email email required Email | email pengguna. Example: melly.tiara92@gmail.com
     * @bodyParam password password required Password | password baru. Example: streamgaming
     * @bodyParam password_confirmation password required Password Confirmation | konfirmasi password baru. Example: streamgaming
     * @response scenario="success reset password" status="200" {
            "status": true,
            "message": "Your password has been reset!",
        }
     * @response scenario="failed reset password" status="400" {
            "status": true,
            "message": "There was an error resetting the password'",
            "error": {
                "errorInfo": [
                    "42S02",
                    1146,
                    "Table 'stream_baru.password_resets' doesn't exist"
                ],
            }
        }
     */
    public function resetPassword(ValidateResetPassword $request)
    {
        try {
            $response = $this->broker()->reset(
                $request->only('email', 'password', 'token'),
                function ($user, $password) {
                    $user->password = Hash::make($password);
                    $user->save();
                }
            );
        } catch (\Exception $e) {
            //return error message gagal
            return response()->json([
                'status'    => true,
                'message'   => Lang::get('passwords.reset.error'),
                'error'     => $e
            ], $this->HTTP_CONFLIC);
        }
        if ($response !== Password::PASSWORD_RESET) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('passwords.reset.invalid')
            ], $this->HTTP_BAD);
        }
        return response()->json([
            'status'    => true,
            'message'   => Lang::get('passwords.reset.success')
        ], $this->HTTP_OK);
    }

    /**
     * Get the broker to be used during password reset.
     */
    private function broker()
    {
        $passwordBrokerManager = new PasswordBrokerManager(app());
        return $passwordBrokerManager->broker();
    }
}
