<?php

namespace App\Http\Controllers;

use App\Model\ExpressionFaqsModel;
use App\Model\FaqsModel;
use App\Model\MsFaqsModel;
use App\Transformer\ContentFaqsTransform;
use App\Transformer\FaqsSearchTransform;
use Illuminate\Http\Request;
use App\Transformer\FaqsTransform;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Lang;

/**
 * @group Public
 *
 * APIs for the FAQ
 */
class FaqsController extends Controller
{
    /**
     * Get Menu FAQ.
     * <aside class="notice">APIs Method for FAQ information | untuk melihat pertanyan yang biasa diajukan</aside>
     * @responseFile status=200 scenario="success" responses/faqV2.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Halaman FAQ tidak ada"
        }
     */
    public function index(Request $request)
    {
        try {
            $get_faqs = MsFaqsModel::with('faqS')->whereHas('faqS')->get();
            $data = new Collection($get_faqs, new FaqsTransform);
            $data = (new Manager())->createData($data)->toArray();
            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('faqs.index')
            ], $this->HTTP_NOT_FOUND);
        }
    }
    /**
     * Get Detail FAQ.
     * <aside class="notice">APIs Method for FAQ information | untuk melihat pertanyan yang biasa diajukan</aside>
    * @urlParam id_faqs required Example:FAQS0001
     * @response {
                    {
                        "id_faqs": "FAQS0001",
                        "title": "REGISTRASI",
                        "created_at": "2021-04-09T07:15:19.000000Z",
                        "content": [
                            {
                                "id": 5,
                                "id_faqs": "FAQS0001",
                                "content": "<p>1. Buka Website Streamgaming.idasd</p>\r\nasdlada\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff03c78c8.jpeg"
                            },
                            {
                                "id": 6,
                                "id_faqs": "FAQS0001",
                                "content": "<p>2. Klik Opsi Masuk Pada kanan atas</p>\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff0490bd6.jpeg"
                            },
                            {
                                "id": 7,
                                "id_faqs": "FAQS0001",
                                "content": "<p>3. Pilih Create New account</p>\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff0518a7c.jpeg"
                            },
                            {
                                "id": 8,
                                "id_faqs": "FAQS0001",
                                "content": "<p>4. Isi semua data yang diperlukan, jika sudah selesai klik create your account</p>\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff0594491.jpeg"
                            },
                            {
                                "id": 9,
                                "id_faqs": "FAQS0001",
                                "content": "<p>5. periksa email masuk pada akun gmail yang anda gunakan untuk register tadi</p>\r\n\r\n<p>&nbsp;</p>\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff061cbe6.jpeg"
                            },
                            {
                                "id": 10,
                                "id_faqs": "FAQS0001",
                                "content": "<p>6. Kemudian verifikasi akun terrsebut, selamat akun setream gaming anda dibuat</p>\r\n",
                                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/terms_and_condition/606fff069a8a0.jpeg"
                            }
                        ],
                        "expression": null
                    }
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Content FAQ tidak ditemukan"
        }
     */
    public function getDetail($url){
       if(Auth::check()){
            $user_id = Auth::user()->user_id;
        }else{
            $user_id = '';
        }
        try {
            $get_faqs = FaqsModel::with(['msfaqs','contentFaqs','MenuFaqs.SubMenuFaqs.faqs','SubMenuFaqs.MenuFaqs.faqs'])->where('url',$url)->first();
            $get_faqs->expression = $this->CheckExpression($get_faqs->id_faqs,$user_id);
            $data =  (new ContentFaqsTransform)->transform($get_faqs);
            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('faqs.detail_faqs')
            ], $this->HTTP_NOT_FOUND);
        }
    }
    public function CheckExpression($id, $user_id=''){
        try{
            $get_faqs = ExpressionFaqsModel::where('id_faqs','=',$id)
                        ->where('users_id','=',$user_id)
                        ->first(['like','dislike']);
            return $get_faqs;
        }catch (\Exception $exception) {
           return false;
        }
    }
    public function listSearch(Request $request){
            $search   = filter_var($request->search, FILTER_SANITIZE_STRING);

            if($search!=null){
                $faqs = FaqsModel::where('title','LIKE','%'.$search.'%')->get();
            }else{
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('faqs.search_faqs')
                ],$this->HTTP_NOT_FOUND);
            }
            $data = new Collection($faqs, new FaqsSearchTransform);
            $data = (new Manager())->createData($data)->toArray();
            return response()->json($data);
    }
}
