<?php

namespace App\Http\Controllers;

use App\Helpers\Pagination;
use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\TournamentListModel;
use App\Model\TournamentModel;
use App\Transformer\TournamentListBracketTransform;
use App\Transformer\TournamentListLeaderboardTransform;
use App\Transformer\TournamentListTransform;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class TournamenList extends Controller
{
    /**
     * Tournament List
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need
     * @queryParam payment Filter by payment type (free,pay). Defaults to 'all'. Example: free
     * @queryParam games Filter by games (id_games). Defaults to 'all'. Example: 501085427
     * @queryParam status Filter by status tournament [pending=0], [open_registration=1], [end_registration=2], [starting=3], [complete=4]). Defaults to 'all'. Example: open_registration
     * @queryParam mode Filter by mode tournament (solo, duo, or squad). Defaults to 'all'. Example: solo
     * @queryParam search string Search tournament by title. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function getAll(Request $request)
    {
        $payment  = filter_var($request->payment, FILTER_SANITIZE_STRING);
        $games    = filter_var($request->games, FILTER_SANITIZE_STRING);
        $status   = filter_var($request->status, FILTER_SANITIZE_STRING);
        $mode     = filter_var($request->mode, FILTER_SANITIZE_STRING);
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);


        // $limit = $request->limit;

        $leaderboard = LeaderboardModel::where('status_tournament','!=',5)
                                        ->where('status_tournament','!=',4)
                                        ->with(['AvailableSlots' => function ($q)
                                        {
                                            $q->where('qualification_sequence',1);
                                        }]);

        $tournament = TournamentModel::where('status','!=','canceled')
                                        ->where('status','!=','complete');

        if($games!='all' && $games!=null){
            $leaderboard->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });

            $tournament->whereHas('game', function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $leaderboard->where('status_tournament',$this->convertStatus($status));

            $tournament->where('status',$status);
        }

        if($mode!='all' && $mode!=null){
            $leaderboard->where('game_option',$mode);

            $tournament->where('game_option',$mode);
        }

        if($payment!='all' && $payment!=null){
            $operator = $payment == 'pay' ? '!=' : '=';
            $payment = $payment == 'pay' ? 'free' : $payment;
            $leaderboard->where('tournament_form',$operator,$payment);

            $tournament->where('system_payment',$operator,$payment);
        }

        if($search!=null){
            $leaderboard->where('tournament_name','LIKE','%'.$search.'%');

            $tournament->where('tournament_name','LIKE','%'.$search.'%');
        }

        $leaderboard = $leaderboard->with('game')->get();

        $tournament = $tournament->with('game')->get();

        $resource = new Collection($leaderboard, (new TournamentListLeaderboardTransform));

        $leaderboardData = current((new Manager())->createData($resource)->toArray());

        $collectLeaderboard = collect($leaderboardData);

        $resource = new Collection($tournament, (new TournamentListBracketTransform));

        $tournamentData = current((new Manager())->createData($resource)->toArray());

        $collectTournament = collect($tournamentData);

        $merged = $collectLeaderboard->merge($collectTournament);

        $mergedsorted = $merged->sortBy('status.number')->sortByDesc('time_value')->sortBy('slots.slotstatus')->values()->all();

        $mergedsorted = collect($mergedsorted)->groupBy('time')->toArray();

        if($mergedsorted != null){
            return response()->json([
                'status'    => true,
                'data'      => $mergedsorted
                ]);
        }

        return response()->json([
            'status'    => false,
            'message'   => lang::get('message.tournament.not')
        ],$this->HTTP_NOT_FOUND);

        // $limit = $limit == null ? 9 : $limit;

        // $page = (new Pagination())->paginate($mergedsorted, $limit ,null,['path' => 'tournament']);

        // $collection = new Collection($page,(new TournamentListTransform));

        // $pagination = $collection->setPaginator(new IlluminatePaginatorAdapter($page));

        // return  (new Manager())->createData($pagination)->toArray();

    }

    public function convertStatus($status)
    {
        switch ($status) {
            case 'pending':
                return 0;
                break;
            case 'open_registration':
                return 1;
                break;
            case 'end_registration':
                return 2;
                break;
            case 'starting':
                return 3;
                break;
            case 'complete':
                return 4;
                break;
            case 'canceled':
                return 5;
                break;
            default:
                return $status;
                break;
        }
    }

    /**
     * Tournament List Complete
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need
     * @queryParam payment Filter by payment type (free,pay). Defaults to 'all'. Example: free
     * @queryParam games Filter by games (id_games). Defaults to 'all'. Example: 501085427
     * @queryParam mode Filter by mode tournament (solo, duo, or squad). Defaults to 'all'. Example: solo
     * @queryParam search string Search tournament by title. No-example
     * @queryParam limit integer limit data per page default to 9. Example: 5
     * @responseFile status=200 scenario="success" responses/tournament.list.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function complete(Request $request)
    {
        $payment  = filter_var($request->payment, FILTER_SANITIZE_STRING);
        $games    = filter_var($request->games, FILTER_SANITIZE_STRING);
        $status   = 'complete';
        $mode     = filter_var($request->mode, FILTER_SANITIZE_STRING);
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);
        $limit    = $request->limit;


        // $limit = $request->limit;

        $leaderboard = LeaderboardModel::where('status_tournament','!=',5)
                                        ->where('status_tournament','=',4)
                                        ->with(['AvailableSlots' => function ($q)
                                        {
                                            $q->where('qualification_sequence',1);
                                        }]);

        $tournament = TournamentModel::where('status','!=','canceled')
                                        ->where('status','=','complete');

        if($games!='all' && $games!=null){
            $leaderboard->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });

            $tournament->whereHas('game', function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $leaderboard->where('status_tournament',$this->convertStatus($status));

            $tournament->where('status',$status);
        }

        if($mode!='all' && $mode!=null){
            $leaderboard->where('game_option',$mode);

            $tournament->where('game_option',$mode);
        }

        if($payment!='all' && $payment!=null){
            $operator = $payment == 'pay' ? '!=' : '=';
            $payment = $payment == 'pay' ? 'free' : $payment;
            $leaderboard->where('tournament_form',$operator,$payment);

            $tournament->where('system_payment',$operator,$payment);
        }

        if($search!=null){
            $leaderboard->where('tournament_name','LIKE','%'.$search.'%');

            $tournament->where('tournament_name','LIKE','%'.$search.'%');
        }

        $leaderboard = $leaderboard->with('game')->get();

        $tournament = $tournament->with('game')->get();

        $resource = new Collection($leaderboard, (new TournamentListLeaderboardTransform));

        $leaderboardData = current((new Manager())->createData($resource)->toArray());

        $collectLeaderboard = collect($leaderboardData);

        $resource = new Collection($tournament, (new TournamentListBracketTransform));

        $tournamentData = current((new Manager())->createData($resource)->toArray());

        $collectTournament = collect($tournamentData);

        $merged = $collectLeaderboard->merge($collectTournament);

        $mergedsorted = $merged->sortByDesc('time_value')->values()->all();

        /* if($mergedsorted != null){
        //     return response()->json([
        //         'status'    => true,
        //         'data'      => $mergedsorted
        //         ]);
        // }

        // return response()->json([
        //     'status'    => false,
        //     'message'   => lang::get('message.tournament.not')
        // ],$this->HTTP_NOT_FOUND); */

        $limit = $limit == null ? 9 : $limit;

        $page = (new Pagination())->paginate($mergedsorted, $limit ,null,['path' => 'tournament']);

        $collection = new Collection($page,(new TournamentListTransform));

        $pagination = $collection->setPaginator(new IlluminatePaginatorAdapter($page));

        return  (new Manager())->createData($pagination)->toArray();

    }


    /**
     * Tournament List
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need
     * @queryParam page retrieve data by page
     * @queryParam limit retrieve data with limit. Default to 15. Example:5
     * @queryParam payment Filter by payment type (free,pay). Defaults to 'all'. Example: free
     * @queryParam games Filter by games (id_games). Defaults to 'all'. Example: 501085427
     * @queryParam status Filter by status tournament [pending=0], [open_registration=1], [end_registration=2], [starting=3], [complete=4]). Defaults to 'all'. Example: 1
     * @queryParam mode Filter by mode tournament (solo, duo, or squad). Defaults to 'all'. Example: solo
     * @queryParam search string Search tournament by title. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function allTouranament(Request $request)
    {
        $games    = filter_var($request->games, FILTER_SANITIZE_STRING);
        $status   = $request->status != null ? filter_var($request->status, FILTER_SANITIZE_STRING) : null;
        $mode     = filter_var($request->mode, FILTER_SANITIZE_STRING);
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);
        $limit    = $request->limit != null ? $request->limit : 15;
        $page     = $request->page != null ? $request->page : 1;


        if ($cache = RedisHelper::getData('tournament:list:oncoming:page:' . $page)) {
            return $cache;
        }


        $datas = TournamentListModel::with(['AvailableSlots' => function ($q)
                                    {
                                        $q->select('id_leaderboard','total_team')
                                          ->where('qualification_sequence',1);

                                    }])

                                    ->where('statuss','!=',5);

        if($games!='all' && $games!=null){
            $datas->whereHas('game',function ($q) use($games)
            {
                return $q->where('id_game_list',$games);
            });
        }

        if($status!='all' && $status!=null){
            $datas->where('statuss',$status);
        }else{
            $datas->where('statuss','!=',4);
        }

        if($mode!='all' && $mode!=null){
            if($mode!=1){
                $mode=3;
                $datas->where('game_option','>=',$mode);
            }
            $datas->where('game_option',$mode);
        }

        if($search!=null){
            $datas->where('tournament_name','LIKE','%'.$search.'%');
        }

        $retrieve = $datas->with(['game' => function ($q)
                            {
                                $q->select('id_game_list','name','games_on','logo_game_list');

                            }]);

        if($status != 4){
            $ordered = $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
        }
        else{
            $ordered = $retrieve->orderBy('tournament_date','desc');
        }

        $paginator = $ordered->paginate($limit);

        $resource           = new Collection($paginator, (new TournamentListTransform));
        // - Buat Pagination
        $tournament         = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        $data               = (new Manager())->createData($tournament)->toArray();

        $grouped = collect($data['data'])->groupBy('time')->toArray();

        $data['data'] = $grouped;

        RedisHelper::setData('tournament:list:oncoming:page:' . $page, $data, 7200);

        return response()->json($data);
    }
}
