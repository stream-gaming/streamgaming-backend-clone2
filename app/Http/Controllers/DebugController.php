<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

class DebugController extends Controller
{
    public function index()
    { 
        throw new Exception('My first Sentry error!');
    }
}
