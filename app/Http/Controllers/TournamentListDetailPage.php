<?php

namespace App\Http\Controllers;

use App\Helpers\MyApps;
use App\Helpers\Pagination;
use App\Helpers\Redis\RedisHelper;
use App\Http\Controllers\Api\GameController;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\TicketClaimsModel;
use App\Model\TournamentListModel;
use App\Model\TournamentModel;
use App\Transformer\TournamentListBracketTransform;
use App\Transformer\TournamentListLeaderboardTransform;
use App\Transformer\TournamentListTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class TournamentListDetailPage extends Controller
{

    private $date;
    private $dateformat;
    private $dateweekstart;
    private $dateweekend;

    public function __construct()
    {
        $this->date = new Carbon();
        $this->dateformat =  'Y-m-d H:i';
        $this->dateweekstart = $this->date->now()->startOfWeek(Carbon::MONDAY)->format($this->dateformat);
        $this->dateweekend  = $this->date->now()->endOfWeek(Carbon::SUNDAY)->format($this->dateformat);
    }

    /**
     * Tournament List V3
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need.
     * new : no grouping data.
     * @queryParam page retrieve data by page
     * @queryParam limit retrieve data with limit. Default to 15. Example:5
     * @queryParam payment Filter by payment type (free,pay). Defaults to 'all'. Example: free
     * @queryParam games Filter by games (id_games). Defaults to 'all'. Example: 501085427
     * @queryParam status Filter by status tournament [pending=0], [open_registration=1], [end_registration=2], [starting=3], [complete=4]). Defaults to 'all'. Example: 1
     * @queryParam mode Filter by mode tournament (solo, duo, or squad). Defaults to 'all'. Example: solo
     * @queryParam search string Search tournament by title. No-example
     * @queryParam search string Search tournament by title. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.v.iii.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function allTournament(Request $request)
    {
        $games    = filter_var($request->games, FILTER_SANITIZE_STRING);
        $status   = $request->status != null ? filter_var($request->status, FILTER_SANITIZE_STRING) : null;
        $mode     = filter_var($request->mode, FILTER_SANITIZE_STRING);
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);
        $payment  = filter_var($request->payment, FILTER_SANITIZE_STRING);
        $limit    = $request->limit != null ? $request->limit : 15;
        $tab      = filter_var($request->tab, FILTER_SANITIZE_STRING);
        $page     = $request->page != null ? $request->page : 1;


        if(!$request->except(['page','limit'])){

            if ($cache = RedisHelper::getData('tournament:list:detail:page:' . $page . ':limit:' . $limit)) {
                return $cache;
            }
        }

        if($tab=="finished"){
            $status=4;
        }

        $data['header'] = [
            "title" => Lang::get('tournament.page.title'),
            "desc"  => Lang::get('tournament.page.description')
        ];

        $gamefilter = GameModel::all('id_game_list','name');

        $filtergame =[];
        foreach ($gamefilter as $val ) {
            $filtergame[] = [
                "text" => $val->name,
                "value"=> $val->id_game_list
            ];
        }


        $data["tab"] = [
            [
                "tabname"   => Lang::get('tournament.tabname.all'),
                "url"       => "all"
            ],
            [
                "tabname"   => Lang::get('tournament.tabname.thisweek'),
                "url"       => "this_week"
            ],
            [
                "tabname"   => Lang::get('tournament.tabname.upcoming'),
                "url"       => "upcoming"
            ],
            [
                "tabname"   => Lang::get('tournament.tabname.finished'),
                "url"       => "finished"
            ]
        ];

        $data['filter'] = [
            "game"      => $filtergame,
            "register"  => [
                [
                    "text" => "gratis",
                    "value"=> "free"
                ],
                [
                    "text" => "berbayar",
                    "value"=> "pay"
                ],
            ],
            "status"    => $this->status(),
            "mode"      => [
                [
                    "text" => "solo",
                    "value"=> 1
                ],
                [
                    "text" => "squad",
                    "value"=> 3
                ],
            ]
        ];

        try {
            $paginator = $this->getData($games,$status,$mode,$search,$tab,$payment,$limit);

            $resource           = new Collection($paginator, (new TournamentListTransform));
            // - Buat Pagination
            $tournament         = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

            $data = array_merge($data, (new Manager())->createData($tournament)->toArray());

            if(!$request->except(['page','limit'])){
                RedisHelper::setData('tournament:list:detail:page:' . $page . ':limit:' . $limit, $data, 7200);
            }
        } catch (\Exception $e) {
            $response = [ "data" => [],
                          "meta" => [
                              "pagination" => [
                                  "total" => 0,
                                  "count" => 0,
                                  "per_page" => 16,
                                  "current_page" => 1,
                                  "total_pages" => 1,
                                  "links" => []
                                ]
                            ]
                        ];
            $data = array_merge($data, $response);
        }

        return response()->json($data);
    }

    public function getData($games,$status,$mode,$search,$tab,$payment,$limit)
    {
        $free_ticket_id = TicketClaimsModel::select('ticket_id')->get()->pluck('ticket_id')->all();
        $datas = TournamentListModel::selectRaw('list_tournament.*, COALESCE(prizes.total_prize2,0) as total_prize2')
                                    ->with(['AvailableSlots' => function ($q)
                                    {
                                        $q->select('id_leaderboard','total_team')
                                          ->where('qualification_sequence',1);

                                    }])
                                    ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                                        FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                                            FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                                        GROUP BY tournament_id) prizes'),function($join){
                                                            $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                                        })
                                    ->where('statuss','!=',5);

        $datas->whereHas('game',function ($q) use($games)
                                    {
                if($games!='all' && $games!=null){
                    return $q->where('id_game_list',$games);
                }
        });

        if($status!='all' && $status!=null){
            $datas->where('statuss',$status);
        }else{
            $datas->where('statuss','!=',4);
        }
        if($mode!='all' && $mode!=null){
            if($mode!=1){
                $mode=3;
                $datas->where('game_option','>=',$mode);
            }
            $datas->where('game_option',$mode);
        }
        if($payment!='all' || $payment!=null){
            if($payment=="pay"){
                $datas->where('payment_type','!=','free')->WhereNotIn('ticket',$free_ticket_id);
            }
            elseif ($payment=="free") {
                $datas->where(function ($q) use($free_ticket_id) {
                    $q->whereIn('ticket',$free_ticket_id)
                      ->orWhere('payment_type','free');
                });
            }
        }
        if($search!=null){
            $datas->where('tournament_name','LIKE','%'.$search.'%');
        }
        $retrieve = $datas->with(['game' => function ($q)
                            {
                                $q->select('id_game_list','name','games_on','logo_game_list');

                            }]);
        if($status != 4){
            $ordered = $retrieve->orderBy('statuss')->orderBy('filled')->orderBy('tournament_date','asc');
        }
        else{
            $ordered = $retrieve->orderBy('tournament_date','desc');
        }

        if ($tab=="this_week"){
            $ordered = $ordered->whereBetween('tournament_date',[$this->dateweekstart,$this->dateweekend]);
        }
        elseif ($tab=="upcoming") {
            $ordered = $ordered->where('tournament_date','>',$this->dateweekend);
        }

        return $ordered->paginate($limit);
    }

    public function status()
    {
        return [
            [
                "text" => "akan datang",
                "value"=> 0
            ],
            [
                "text" => "buka pendaftaran",
                "value"=> 1
            ],
            [
                "text" => "tutup pendaftaran",
                "value"=> 2
            ],
            [
                "text" => "sedang berlangsung",
                "value"=> 3
            ],
            [
                "text" => "selesai",
                "value"=> 4
            ],
        ];
    }
}
