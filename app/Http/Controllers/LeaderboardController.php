<?php

namespace App\Http\Controllers;

use App\Helpers\MyApps;
use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use App\Model\LeaderboardGroupModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardMostKillModel;
use App\Model\LeaderboardQualificationModel;
use App\Model\LeaderboardScoreConfigModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\LeaderboardScoreTeamModel;
use App\Model\LeaderboardTeamModel;
use App\Model\PlayerPayment;
use App\Model\TeamPlayerModel;
use App\Model\User;
use App\Transformer\GameTransform;
use App\Serializer\GameSerialize;
use App\Transformer\AccountTransform;
use App\Transformer\LeaderboardGroupTransform;
use App\Transformer\LeaderboardScoreMatchSoloTransform;
use App\Transformer\LeaderboardScoreMatchTeamTransform;
use App\Transformer\LeaderboardScoreMostKillSoloTransform;
use App\Transformer\LeaderboardScoreMostKillTransform;
use App\Transformer\LeaderboardScoreTeamTransform;
use App\Transformer\LeaderboardStageTranform;
use App\Transformer\LeaderboardStageTransform;
use App\Transformer\LeaderboardTotalScoreSoloTransform;
use App\Transformer\LeaderboardTotalScoreTeamTransform;
use App\Transformer\LeaderboardTransform;
use App\Transformer\ParticipantSoloTransform;
use App\Transformer\ParticipantTransform;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Throwable;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * @group Tournament
 *
 * APIs for the Tournament Leaderboard
 */
class LeaderboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(\App\Http\Middleware\RefreshTokenManual::class);
        $this->my_apps               = new MyApps;
        $this->leaderboard_transform = new LeaderboardTransform;
    }

    /**
     * Tournament Detail - Leaderboard.
     *
     * This endpoint serves to get detailed information about the tournament type leaderboard.
     * URL parameters are required to retrieve data.
     * URL parameters can be seen from the existing tournament list data.
     * <aside class="notice">APIs Method for Leaderboard Information</aside>
     * @urlParam url required for url leaderboard what you want to see. Example: PUBG-BETA-VII
     * @urlParam data optional for type what you want to see for tab menu [overview,rules,participant,prizes,schedule,leaderboard]. Example: leaderboard
     * @urlParam stage optional for stage leaderboard what you want to see. Example: 1
     * @responseFile status=200 scenario="overview" responses/leaderboard.overview.json
     * @responseFile status=200 scenario="overview v2" responses/leaderboard.overview2.json
     * @responseFile status=200 scenario="rules" responses/leaderboard.rules.json
     * @responseFile status=200 scenario="participant" responses/leaderboard.participant.json
     * @responseFile status=200 scenario="prizes v1" responses/leaderboard.prizes.json
     * @responseFile status=200 scenario="prizes v2" responses/leaderboard.prizes2.json
     * @responseFile status=200 scenario="prizes v3" responses/leaderboard.prizes3.json
     * @responseFile status=200 scenario="schedule" responses/leaderboard.schedule.json
     * @responseFile status=200 scenario="leaderboard" responses/leaderboard.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Tournament not found"
        }
     */
    public function index($url, $type = 'overview', $stage = null)
    {



        $url         = filter_var($url, FILTER_SANITIZE_URL);
        try {
            $leaderboard = LeaderboardModel::where('url', $url)->firstOrFail();
            // Get Information SLot
            $slotsAvailable         = $leaderboard->total_team;
            $qualification_sequence = ($leaderboard->tournament_category) == 3 ? 1 : 0;
            $slotsAvailable         = $leaderboard->total_team;
            if ($qualification_sequence) {
                $slotsAvailable = $leaderboard->AvailableSlots->total_team;
            }
            $slotsFilled = $leaderboard->filled_slot;

            //check cache data
            $cachekey = !is_null($stage) ? 'leaderboard.'.$leaderboard->id_leaderboard.'.'.$type.'.'.$stage : 'leaderboard.'.$leaderboard->id_leaderboard.'.'.$type;
            $checkAndGetData =  RedisHelper::getDataCustom($cachekey);
            if($checkAndGetData != false){
                $redisData = (array) $checkAndGetData;
                $redisData['data']->head_info = $this->leaderboard_transform->headInfo($leaderboard, $slotsAvailable, $slotsFilled);
                $redisData['data']->ticket_info = $this->leaderboard_transform->ticketInfo($leaderboard);
                return response()->json($checkAndGetData);
            }




            $data['head_info'] = $this->leaderboard_transform->headInfo($leaderboard, $slotsAvailable, $slotsFilled);


            //- Get Detail Tournament
            if ($leaderboard->tournament_category == 3) {
                $leaderboard->stage = ($stage == null) ? 1 : $stage;
                if ($type == 'leaderboard') {
                    $getQualifyInfo     = (new LeaderboardQualificationModel)->getQualification($leaderboard);
                    $resource           = new Collection($getQualifyInfo, (new LeaderboardStageTransform));
                    $data_stage         = current((new Manager())->createData($resource)->toArray());
                    $data['stage']      = $data_stage;
                }
            } else {
                $leaderboard->stage = 0;
            }

            $url = [
                'overview'     => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'overview']),
                'rules'        => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'rules']),
                'participant'  => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'participant']),
                'prizes'       => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'prizes']),
                'schedule'     => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'schedule']),
                'leaderboard'  => $this->my_apps->getPathRoute('leaderboard.stage', ['url' => $leaderboard->url, 'data' => 'leaderboard']),
            ];

            switch ($type) {
                case 'overview':
                    $data["overview"]         = $this->overview($leaderboard, $slotsAvailable, $slotsFilled);
                    break;
                case 'rules':
                    $data["rules"]            = $this->rules($leaderboard);
                    break;
                case 'participant':
                    $data['team_leaderboard'] = $this->participant($leaderboard);
                    break;
                case 'prizes':
                    $data["prizes"]           = $this->leaderboard_transform->prizesV2($leaderboard);
                    break;
                case 'schedule':
                    $data["schedule"]         = $leaderboard->jadwal;
                    break;
                case 'leaderboard':

                    $leaderboard->limit = request()->query('limit') ? request()->query('limit') : 2;
                    $data['total_team'] = collect((new LeaderboardTeamModel)->getTotalTeam($leaderboard))->count();
                    $data['group_leaderboard'] = $this->leaderboard($leaderboard);
                    break;
                default;
                    throw new Exception();
                    break;
            }
            // Get Side Info
            $data['side_info'] = $this->leaderboard_transform->sideInfo($leaderboard);

            //Get Ticket Info
            $data['ticket_info'] = $this->leaderboard_transform->ticketInfo($leaderboard);

            //Cache data
            $expTime = $leaderboard->status_tournament == 4 ? ((24*7)*3600) : ($stage != null && $leaderboard->status_tournament < 4 ? (3*3600) : ((24*7)*3600));
            RedisHelper::setData($cachekey,[
                'status'        => true,
                'id_leaderboard'=> $leaderboard->id_leaderboard,
                'active_menu'   => $type,
                'menu'          => $url,
                'data'          => $data,
            ], $expTime);
            return response()->json([
                'status'        => true,
                'id_leaderboard'=> $leaderboard->id_leaderboard,
                'active_menu'   => $type,
                'menu'          => $url,
                'data'          => $data,
            ]);
        } catch (\Throwable $exception) {
            return response()->json([
                'status' => false,
                // 'message' => Lang::get('message.tournament.not')
                'message' => $exception
            ], $this->HTTP_NOT_FOUND);
        }
    }

    public function overview($leaderboard, $slotsAvailable, $slotsFilled)
    {
        return ($this->leaderboard_transform->overview($leaderboard, $slotsAvailable, $slotsFilled));
    }

    public function rules($leaderboard)
    {
        return ($this->leaderboard_transform->rules($leaderboard));
    }

    public function prizes($leaderboard)
    {
        return ($this->leaderboard_transform->prizes($leaderboard));
    }

    public function participant($leaderboard)
    {
       if($leaderboard->game_option == 1){
        //Participant for solo mode

            $paginator          = (new LeaderboardTeamModel())->getSoloLeaderboardPaginate($leaderboard);

            $resource           = new Collection($paginator, (new ParticipantSoloTransform));
            // - Buat Pagination
            // $participant        = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
            // $data               = (new Manager())->createData($participant)->toArray();

       }else if($leaderboard->game_option == 3 OR $leaderboard->game_option == 4 ){
        //Participant for squad mode

            $paginator          = (new LeaderboardTeamModel())->getTeamLeaderboardPaginate($leaderboard);
            $resource           = new Collection($paginator, (new ParticipantTransform));
            // - Buat Pagination
            // $participant        = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
            // $data               = (new Manager())->createData($participant)->toArray();

       }


        $data               = (new Manager())->createData($resource)->toArray();

        return $data;
    }

    public function leaderboard($leaderboard)
    {
        if ($leaderboard->stage != 0 && $leaderboard->stage != 1) {
            $paginator          = (new LeaderboardGroupModel)->getLeaderboardGroup($leaderboard);
        } else {
            $paginator          = (new LeaderboardGroupModel)->getByLeadeboardSequence($leaderboard);
        }

        $resource           = new Collection($paginator, (new LeaderboardGroupTransform));
        // $data               = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        $data               = (new Manager())->createData($resource)->toArray();
        // $data               = (new Manager())->createData($data)->toArray();

        return $data;
    }

    /**
     * Matchs Overview - Leaderboard.
     *
     * This endpoint is used to get detailed information about the match in the leaderboard type tournament
     * <aside class="notice">APIs Method for Detail Leaderboard Information</aside>
     * @urlParam url url required for url leaderboard what you want to see. Example: PUBG-BETA-VII
     * @urlParam stage number optional for stage leaderboard what you want to see. Example: 1
     * @urlParam group number optional for Group Team leaderboard what you want to see. Example: 0
     * @urlParam match string optional for Match Team leaderboard what you want to see. Example: 2
     * @responseFile status=200 scenario="match" responses/leaderboard.detail.match.json
     * @responseFile status=200 scenario="total" responses/leaderboard.detail.total.json
     * @responseFile status=200 scenario="most_kill" responses/leaderboard.detail.most_kill.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Tournament not found"
        }
     */
    public function detailLeaderboard($url, $stage = null, $group = null, $match_id = null)
    {


        //filtering the parameter
        $url   = filter_var($url, FILTER_SANITIZE_STRING);
        $stage = filter_var($stage, FILTER_SANITIZE_STRING);
        $group = filter_var($group, FILTER_SANITIZE_STRING);

        try {
            $leaderboard        = LeaderboardModel::where('url', $url)->firstOrFail();
            $leaderboard->stage = $stage;

            //check cache data
            if($match_id != 'most_kill'){
                $cachekey = 'leaderboard.score.'.$leaderboard->id_leaderboard.'.'.$stage.'.'.$group.'.'.$match_id;
                $checkAndGetData =  RedisHelper::getData($cachekey);
                if($checkAndGetData != false) return $checkAndGetData;
            }
            // if ($leaderboard) :
            if ($leaderboard->tournament_category == 3) :
                $isFullCostum = true;
                $isFinal      = false;
                $status       = false;

                // if ($isFullCostum) :
                if (strtolower($leaderboard->stage) == 'final') :
                    $leaderboard->stage = 0;
                    $isFinal = true;
                endif;

                $qualify        = new LeaderboardQualificationModel;
                $getQualifyInfo = ($leaderboard->stage == 1) ? $qualify->getQualifyInfo($leaderboard) : $qualify->getQualifyInfoStage($leaderboard);

                if ($getQualifyInfo) :
                    $match = $getQualifyInfo->matchs;
                    $status = true;
                endif;

            // elseif ($stage == 1) :
            //     $getQualifyInfo = (new LeaderboardQualificationModel)->getQualifyInfo($leaderboard);

            //     $match = $getQualifyInfo->matchs;
            //     $status = true;
            // endif;

            elseif ($leaderboard->tournament_category < 3) :
                $isFullCostum = false;
                $status = true;
            endif;

            // else :
            //     $isFullCostum = false;
            //     $data["isFullCostum"] = $isFullCostum;
            //     $status = false;
            // endif;

            if ($leaderboard->status_tournament >= 1 and $status == true) {

                if (strtoupper($group) == "FINAL" or $group == 100) {
                    $leaderboard->group_sequence = "100";
                } elseif ($group === "0") {
                    $leaderboard->group_sequence = $group;
                } elseif ($group > 0 and $group < 100) {
                    $leaderboard->group_sequence = $group;
                } else {
                    $leaderboard->group_sequence = true;
                }

                $leaderboard->group_sequence = ($isFullCostum && $isFinal) ? '1000' : $leaderboard->group_sequence;


                $cekGroup = (new LeaderboardGroupModel)->cekGroup($leaderboard);
                if (!$cekGroup) :
                    return response()->json([
                        'status' => false,
                        'message' => Lang::get('message.tournament.not'),
                    ]);
                endif;

                $cek_leaderboard = (new LeaderboardModel)->cekLeaderboard($url, $leaderboard->group_sequence);

                //MATCH FOR QUALIFICATION
                if ($isFullCostum) :
                    if ($isFinal) :
                        $data_final = $match;
                    else :
                        $cek_leaderboard->total_match = $match;
                    endif;
                endif;

                $isFinal      = (($isFinal) ?? false);
                $isFullCostum = (($isFullCostum) ?? false);

                $data["leaderboard_info"] = $this->leaderboard_transform->leaderboardInfo($url);

                if ($isFullCostum) {
                    $data['stage'] = (strtolower($stage) == 'final') ? Lang::get('message.tournament.final') : Lang::get('message.tournament.stage') . ' ' . $stage;
                }

                $data['title'] = $this->my_apps->getStatusCardLeaderboard($leaderboard->group_sequence);

                $idScoreConfig = $cek_leaderboard->score_config;

                $scoreConfig   = LeaderboardScoreConfigModel::find($idScoreConfig);

                if (isset($data_final)) :
                    $scoreConfig->max_match = $data_final;
                endif;

                if ($leaderboard->group_sequence == 1000 or $leaderboard->group_sequence == 100) :
                    $leaderboard->finalMatch = $scoreConfig->max_match - 1;
                    $leaderboard->totalMatch = $scoreConfig->max_match;
                else :
                    $leaderboard->finalMatch = $cek_leaderboard->total_match - 1;
                    $leaderboard->totalMatch = $cek_leaderboard->total_match;
                endif;

                $parameters   = ['url' => $leaderboard->url, 'stage' => $stage, 'group' => $leaderboard->group_sequence];
                //- Looping total match
                for ($i = 0; $i < $leaderboard->totalMatch; $i++) {
                    $parameters['match']   = $i;
                    $data['total_match'][] = [
                        'match'     => Lang::get('message.tournament.match') . ' ' . (int)($i + 1),
                        'path'      => $this->my_apps->getPathRoute('leaderboard.detail', $parameters)
                    ];
                }
                //- add total match
                $parameters['match']   = 'total';
                $data['total_match'][] = [
                    'match'     => Lang::get('message.tournament.total'),
                    'path'      => $this->my_apps->getPathRoute('leaderboard.detail', $parameters)
                ];


                switch ($match_id) {
                    case 'total':
                        $data['match'] = [
                            'title'      => Lang::get('message.tournament.total'),
                            'score_team' => $this->matchTotal($leaderboard),
                        ];
                        break;
                    case 'most_kill':
                        $leaderboard->is_full_custom = $isFullCostum;
                        $data['match'] = [
                            'title'      => Lang::get('message.tournament.most_kill'),
                            'score_team' => $this->matchMostKill($leaderboard),
                        ];
                        break;
                    default:
                        $leaderboard->match_id = (int) (isset($match_id) ? $match_id : 0);
                        $data['match'] = [
                            'title'      => Lang::get('message.tournament.match') . ' ' . (int)($leaderboard->match_id + 1),
                            'score_team' => $this->match($leaderboard),
                        ];
                        break;
                }
                $data['match']['active_match'] = $match_id;

                //- Jika stage final maka, munculkan tab most kill
                if ($leaderboard->most_kill > 0 and ($isFinal == true or ($leaderboard->group_sequence == '100' and $isFullCostum == false))) :
                    $parameters['match']   = 'most_kill';
                    $data['total_match'][] = [
                        'match'     => Lang::get('message.tournament.most_kill'),
                        'path'      => $this->my_apps->getPathRoute('leaderboard.detail', $parameters)
                    ];
                endif;

                //Cache data
                if($match_id != 'most_kill'){
                    $expTime = $leaderboard->status_tournament == 4 ? ((24*7)*3600) : (3*3600);
                    RedisHelper::setData($cachekey,[
                        'status' => true,
                        'data' => $data,
                    ], $expTime);
                }

                return response()->json([
                    'status' => true,
                    'data' => $data,
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => Lang::get('message.tournament.not'),
                ]);
            }
        } catch (\Throwable $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.tournament.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    public function match($leaderboard)
    {

        if($leaderboard->game_option == '1'){

            $data["getScoreSequence"] = (new LeaderboardScoreTeamModel)->getSoloScoreSequence($leaderboard);

            if (count($data["getScoreSequence"]) <= 0 and $leaderboard->group_sequence < 100) {
                $getTeam = (new LeaderboardTeamModel)->getSolo($leaderboard);
            } elseif (count($data["getScoreSequence"]) <= 0 and ($leaderboard->group_sequence == 100 or $leaderboard->group_sequence == 1000)) {
                $getTeam = (new LeaderboardTeamModel)->getSoloFinal($leaderboard);
            }

            if (count($data["getScoreSequence"]) > 0) {
                $score_team = new Collection($data["getScoreSequence"], new LeaderboardScoreMatchSoloTransform);
            } else {
                $score_team = new Collection($getTeam, new LeaderboardScoreMatchSoloTransform);
            }

        }elseif($leaderboard->game_option == 3 OR $leaderboard->game_option == 4){

            $data["getScoreSequence"] = (new LeaderboardScoreTeamModel)->getScoreSequence($leaderboard);

            if (count($data["getScoreSequence"]) <= 0 and $leaderboard->group_sequence < 100) {
                $getTeam = (new LeaderboardTeamModel)->getTeam($leaderboard);
            } elseif (count($data["getScoreSequence"]) <= 0 and ($leaderboard->group_sequence == 100 or $leaderboard->group_sequence == 1000)) {
                $getTeam = (new LeaderboardTeamModel)->getTeamFinal($leaderboard);
            }

            if (count($data["getScoreSequence"]) > 0) {
                $score_team = new Collection($data["getScoreSequence"], new LeaderboardScoreMatchTeamTransform);
            } else {
                $score_team = new Collection($getTeam, new LeaderboardScoreMatchTeamTransform);
            }
        }


        return (new Manager())->createData($score_team)->toArray();
    }

    public function matchTotal($leaderboard)
    {
        if($leaderboard->game_option == 1){
            $get_total_Score    = (new LeaderboardScoreTeamModel)->getScoreTotalSolo($leaderboard);
            $total_score        = new Collection($get_total_Score, new LeaderboardTotalScoreSoloTransform);
        }elseif($leaderboard->game_option == 3 OR $leaderboard->game_option == 4){
            $get_total_Score    = (new LeaderboardScoreTeamModel)->getScoreTotal($leaderboard);
            $total_score        = new Collection($get_total_Score, new LeaderboardTotalScoreTeamTransform);
        }


        return (new Manager())->createData($total_score)->toArray();
    }

    public function matchMostKill($leaderboard)
    {

        $leaderboard->winner = 3; //- 3 Pemenang teratas

        //most kill for solo tournament
        if($leaderboard->game_option == 1){

            if ($leaderboard->group_sequence == "100" and $leaderboard->is_full_custom == false) :
                $get_most_kill       = (new LeaderboardScoreMatchModel)->getMostKillSequenceSolo($leaderboard);
            else :
                $get_most_kill       = (new LeaderboardScoreMatchModel)->getMostKillSequenceFullCustomSolo($leaderboard);
            endif;
            $most_kill           = new Collection($get_most_kill, new LeaderboardScoreMostKillSoloTransform);

        //most kill for squad tournament
        }elseif($leaderboard->game_option == 3 OR $leaderboard->game_option == 4){

            if ($leaderboard->group_sequence == "100" and $leaderboard->is_full_custom == false) :
                $get_most_kill       = (new LeaderboardScoreMatchModel)->getMostKillSequence($leaderboard);
            else :
                $get_most_kill       = (new LeaderboardScoreMatchModel)->getMostKillSequenceFullCustom($leaderboard);
            endif;
            $most_kill           = new Collection($get_most_kill, new LeaderboardScoreMostKillTransform);

        }

        $data                = (new Manager())->createData($most_kill)->toArray();
        for ($i = 0; $i < count($data['data']); $i++) {
            $data['data'][$i]['status'] = ($i == 0) ? 'Winner' : '';
        }

        return $data;
    }
}
