<?php

namespace App\Http\Controllers;

use App\Helpers\JoinTournamentButton;
use App\Helpers\MyApps;
use App\Helpers\Ticket\TicketValidation;
use App\Model\BracketFinal;
use App\Model\TournamentModel;
use App\Model\BracketOneTwoEight;
use App\Model\BracketQuarterFinal;
use App\Model\BracketSemiFinal;
use App\Model\BracketSixteen;
use App\Model\BracketSixtyFour;
use App\Model\BracketThirdPlace;
use App\Model\BracketThirtyTwo;
use App\Model\SpectatorModel;
use App\Transformer\ScoreMatchsTransform;
use League\Fractal\Resource\Collection;
use App\Transformer\TournamenBracketTransform;
use Carbon\Carbon;
use League\Fractal\Manager;
use Exception;
use App\Helpers\Validation\hasJoinedOtherTournament;

use Illuminate\Support\Collection as SupportCollection;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\checkBalanceTicketUser;
use App\Helpers\Validation\CheckTournamentPaymentMethod;
use App\Http\Middleware\RefreshTokenManual;
use App\Model\AccountChallongeModel;
use App\Model\AllTicketModel;
use App\Model\TicketModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentMvpModel;
use App\Model\TournamentStreamingModel;
use App\Transformer\ScheduleChallongeTransform;
use App\Transformer\TournamenBracketSoloTransform;
use App\Transformer\TournamentMvpTransform;
use App\Transformer\TournamentStreamingTransform;
use Illuminate\Support\Facades\Http;
use Kreait\Firebase\Database\Query\Sorter\OrderByKey;
use Illuminate\Support\Facades\Redis;
/**
 * @group Tournament
 * APIs for Bracket's Tournament Details
 *
 */


class BracketController extends Controller
{
    public function __construct()
    {
        $this->middleware(RefreshTokenManual::class);
    }


    /**
     * Request Tournament Information - Bracket
     *
     * This endpoint serves to get detailed information about bracket type tournaments.
     * URL parameters are required to retrieve data.
     * URL parameters can be seen from the existing tournament list data.
     * @urlParam url required Tournament url required Example:WEEKLY-TOURNAMENT-MOBILE-LEGEND-STREAM-GAMING-IV
     * @urlParam type required Tournament type required Example:overview
     * @response status=404 {
            "status": false,
            "message": "Tournament yang anda cari tidak ditemukan"
        }
     * @responseFile status=200 scenario="overview" responses/bracket.detail.overview.json
     * @responseFile status=200 scenario="rules" responses/bracket.detail.rules.json
     * @responseFile status=200 scenario="prizes V1" responses/bracket.detail.prizesV1.json
        * @responseFile status=200 scenario="prizes V2" responses/bracket.detail.prizesV2.json
     * @responseFile status=200 scenario="participant" responses/bracket.detail.participant.json
     * @responseFile status=200 scenario="schedule" responses/bracket.detail.schedule.json
     * @responseFile status=200 scenario="bracket" responses/bracket.detail.bracket.json
     */
    public function getDetails($url=null,$type='overview')
    {

        $fractal = new Manager();
        $my_apps = new MyApps();

        $url         = filter_var($url, FILTER_SANITIZE_URL);



        try {
            $details = TournamentModel::where('url',$url)->with('game')
                                    ->with(['prize' => function ($q)
                                    {
                                        $q->orderBy('orders');
                                    }])
                                    ->with(['prizeTicket' => function ($q)
                                    {
                                        $q->with('ticketNew');
                                        $q->with('ticketOld');
                                        $q->orderBy('urutan');
                                    }])
                                    ->with(['CustomPrize' => function ($q)
                                    {
                                        $q->with('PrizeInvetory');
                                    }])
                                    ->firstOr(function () {
                                        throw new Exception('message.bracket.details.not');
                                    });
            $date    = new Carbon;
            $datenow = $date->now();

            if($details->status == "pending" && $datenow->gte($date->parse($details->start_registration))){
                $details->status = "open_registration";
                $details->save();
            }
            else if($details->status == "open_registration" && ($datenow->gte($date->parse($details->end_registration)) || $datenow->gte($date->parse($details->date)))){
                $details->status = "end_registration";
                $details->save();
            }

            $slotsFilled = (new TournamenGroupModel)->countFilledSlots($details->id_group);
            $is_full     = $details->total_team!=0 && $slotsFilled!=0 ? $details->total_team / $slotsFilled : 0;

            if (Redis::exists('bracket.'.$type.'.'.$url)) {

                $redisData = (array) json_decode(Redis::get('bracket.'.$type.'.'.$url));;
                $redisData['head_info']->button_join = JoinTournamentButton::getStatus($details->status, $is_full, $details->kompetisi, $details->id_tournament,'bracket' , $details);
                $redisData['ticket_info'] =  $this->ticketInfo($details->id_tournament);
                return response()->json($redisData);
            }

            $spectators         = SpectatorModel::whereIn('id_ingame',explode(',',$details->spectators))->get();
            $spectatorsFiltered = $spectators->makeHidden(['id_spectator','id_management']);

            $is_available   = false;

            $url_menu       = [
                'overview'      => $my_apps->getPathRoute('bracket',[$url,'overview']),
                'rules'         => $my_apps->getPathRoute('bracket',[$url,'rules']),
                'participant'   => $my_apps->getPathRoute('bracket',[$url,'participant']),
                'prizes'        => $my_apps->getPathRoute('bracket',[$url,'prizes']),
            ];


            if($details->status == "starting" || $details->status == "complete"){
                $url_menu['schedule']   = $my_apps->getPathRoute('bracket',[$url,'schedule']);
                $url_menu['bracket']    = $my_apps->getPathRoute('bracket',[$url,'bracket']);
                $is_available           = true;
            }

            $prizes = explode(',',$details->total_prize);
            $totalprize = array_sum($prizes) + $details->prize_mvp;

            switch ($type) {
                case 'overview':
                    $data = TournamenBracketTransform::overview($details);
                    break;
                case 'rules':
                    $data = [
                        'general_rules' => $details->peraturan_umum,
                        'special_rules' => $details->peraturan_khusus
                    ];
                    break;
                case 'participant':
                    $AllTeam = $details->AllTeam->makeHidden(['id_tournament_group','id_group','date','matchs','win']);
                    if($details->game_option == 1){
                        $resource = new Collection($AllTeam,(new TournamenBracketSoloTransform));
                        $data = $fractal->createData($resource)->toArray();
                    }
                    else{
                        $resource = new Collection($AllTeam,(new TournamenBracketTransform()));
                        $data = $fractal->createData($resource)->toArray();
                    }
                    break;
                case 'prizes':

                    $data = [
                        'total_prizes'  => (string) ($totalprize + $this->totalPrizes($details->CustomPrize))
                    ];
                    if($details->id_winner_sequence != null || $details->id_winner_sequence != ""){
                        $total_prizePool = count($details->prize);
                    }else{
                        $total_prizePool = 3;
                    }
                    if($total_prizePool >= count($details->prizeTicket)){
                        if(count($details->CustomPrize) <= $total_prizePool){
                            $data['prize'] = $this->PrizePoll($details);
                        }else{
                            $data['prize'] = $this->prizeCustom($details); 
                        }
                    }else if(count($details->CustomPrize) <= count($details->prizeTicket)){
                        $data['prize'] = $this->PrizeTicket($details);
                    }else{
                        $data['prize'] = $this->prizeCustom($details);
                    }
                    break;
                case 'schedule':
                    if($is_available):
                        $data = ($details->is_challonge == 1) ? $this->scheduleChallonge($details) : $this->bracket($details->id_tournament);
                    else:
                        throw new Exception('message.bracket.details.schedule.not');
                    endif;
                    break;
                case 'bracket':

                    if($is_available):
                        $data = ($details->is_challonge == 1) ? ['is_status' => true,'url' =>  (new MyApps)->GetUrlChallonge('moduleBracket',$details->url_challonge)]  :$this->bracket($details->id_tournament);
                    else:
                        throw new Exception('message.bracket.details.bracket.not');
                    endif;
                    break;
                default:
                    throw new Exception('message.bracket.details.not');
                    break;
            }


            $details->background_tournament = (new MyApps)->cdn($details->background_tournament, 'encrypt', 'banner_tournament');
            $tournamentStatus = (object) $details->tournamentStatus($details->status);

            $res = [
                'status'            => true,
                'active_menu'       => $type,
                'menu'              => $url_menu,
                'head_info'         => [
                    'id_tournament'     => $details->id_tournament,
                    'title'             => $details->tournament_name,
                    'game_name'         => $details->game->title,
                    'end_registration'  => date('Y-m-d H:i', strtotime($details->end_registration)),
                    'start_registration'=> date('Y-m-d H:i', strtotime($details->start_registration)),
                    'tournament_status' => $details->status,
                    'is_tournament_solo'=> (($details->game_option == 1) ? true : false),
                    'start_date'        => date('Y-m-d H:i', strtotime($details->date)),
                    'background'        => $details->background_tournament,
                    'live_stream'       => (($details->url_livestreams == '') ? $this->LiveStreaming($details->id_tournament) : $details->url_livestreams),

                    'register_fee'      => (new CheckTournamentPaymentMethod)->BracketPaymentMethod($details->id_tournament),
                    'button_join'       => JoinTournamentButton::getStatus($details->status, $is_full, $details->kompetisi, $details->id_tournament,'bracket' , $details),
                    'status'        => [
                        'text'      => $slotsFilled == $details->total_team ? (($tournamentStatus->text != Lang::get('tournament.status.in_progress') && $tournamentStatus->text != Lang::get('tournament.status.complete')) ? Lang::get('tournament.status.close_registration') : $tournamentStatus->text) : $tournamentStatus->text,
                        'number'    => $slotsFilled == $details->total_team ? (($tournamentStatus->number != 3 && $tournamentStatus->number != 4) ? 2 : $tournamentStatus->number) : $tournamentStatus->number
                        ],
                    'total_prizes'  => (string) ($totalprize + $this->totalPrizes($details->CustomPrize))
                ],
                'side_info'         => [
                    'important'         => $details->important,
                    'spectators'        => $spectatorsFiltered,
                ],
                'ticket_info'       => $this->ticketInfo($details->id_tournament),
                'data'              => $data
            ];
            $expTime = $details->status == "complete" ? ((24*7)*3600) : (3*3600) ;
            Redis::set('bracket.'.$type.'.'.$url, json_encode($res), 'EX', $expTime);
            return response()->json($res);

        } catch (\Exception $th) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get($th->getMessage())
            ], $this->HTTP_NOT_FOUND);
        }
    }
    public function V1Prize($details){
        $prizes = explode(',',$details->total_prize);
        $totalprize = array_sum($prizes);
        $data = [
            'total_prizes'  => (string) $totalprize
        ];

        if($details->id_winner_sequence != null || $details->id_winner_sequence != ""){
            foreach ($details->prize as $key) {
                $data['prize'][] = [
                    'sequence'        => (string) $key->orders,
                    'prize'           => (string) $key->amount_given
                ];
            }
        }
        else{
            $data['prize']= [
                    [
                        'sequence'        => (string) 1,
                        'prize'           => (string) $prizes[0]
                    ],
                    [
                        'sequence'        => (string) 2,
                        'prize'           => (string) $prizes[1]
                    ],
                    [
                        'sequence'        => (string) 3,
                        'prize'           => (string) $prizes[2]
                    ]
                ];
        }
    }
    public function PrizePoll($detail){

        if(count($detail->prizeTicket) > 0){
            if($detail->is_ticket_prize_new == 1){
                $ticket = AllTicketModel::find($detail->gift_ticket);
            }else if($detail->is_ticket_prize_new == 0){
                $ticket = TicketModel::find($detail->gift_ticket)->first(['names AS name']);
            }
        }

        if($detail->id_winner_sequence != null || $detail->id_winner_sequence != ""){

            for ($i=0; $i < count($detail->prize) ; $i++) {
                $data[] = [
                    'sequence' => (string) $detail->prize[$i]->orders,
                    'title' => 'Juara '.$detail->prize[$i]->orders,
                    'description' => '',
                    'data' => [],
                ];
                if($detail->prize[$i]->amount_given > 0){
                    $data[$i]['data'][] = [
                        "title"     => "Stream Cash",
                        "level"     => "Hadiah Utama",
                        "category"  => "stream_cash",
                        "unit"      => 0,
                        "prize"     => $detail->prize[$i]->amount_given
                    ];
                }
                if(count($detail->prizeTicket) > $i ){
                    $data[$i]['data'][] = [
                        "title"     => (($detail->is_ticket_prize_new == 1)? $detail->prizeTicket[$i]->ticketNew->name : $detail->prizeTicket[$i]->ticketOld->names),
                        "level"     => "Hadiah Utama",
                        "category"  => "ticket",
                        "unit"      => $detail->prizeTicket[$i]->given_ticket,
                        "prize"     => 0
                    ];
                }
                if($detail->is_custom_prize == 1){
                    if(count($detail->CustomPrize) > $i ){
                        $data[$i]['data'][] = [
                            "title"     => $detail->CustomPrize[$i]->PrizeInvetory->title,
                            "level"     => "Hadiah Utama",
                            "category"  => "custom_prize",
                            "image"     => $detail->CustomPrize[$i]->PrizeInvetory->image,
                            "unit"      => $detail->CustomPrize[$i]->quantity,
                            "prize"     => $detail->CustomPrize[$i]->PrizeInvetory->valuation
                        ];
                    }

                }
            }
        }else{

            $prizes = explode(',',$detail->total_prize);

            for ($i=0; $i < 3 ; $i++) {
                if((empty($prizes[$i]) OR $prizes[$i] == 0 )AND count($detail->prizeTicket) < $i AND count($detail->CustomPrize) <= $i){
                }else{ 
                    $data[] = [
                        'sequence' =>  $i+1 ,
                        'title' => 'Juara '.($i+1),
                        'description' => '',
                        'data' => [],
                    ];
                    if(!empty($prizes[$i])){
                        $data[$i]['data'][] = [
                            "title"     => "Stream Cash",
                            "level"     => "Hadiah Utama",
                            "category"  => "stream_cash",
                            "unit"      => 0,
                            "prize"     => (empty($prizes[$i]) ? 0 :$prizes[$i])
                        ];
                    }
                    if(count($detail->prizeTicket) > $i){
                        $data[$i]['data'][] = [
                            "title"     => (($detail->is_ticket_prize_new == 1)? $detail->prizeTicket[$i]->ticketNew->name : $detail->prizeTicket[$i]->ticketOld->names),
                            "level"     => "Hadiah Utama",
                            "category"  => "ticket",
                            "unit"      => $detail->prizeTicket[$i]->given_ticket,
                            "prize"     => 0
                        ];
                    }
                    if($detail->is_custom_prize == 1){
                        if(count($detail->CustomPrize) > $i ){
                            $data[$i]['data'][] = [
                                "title"     => $detail->CustomPrize[$i]->PrizeInvetory->title,
                                "level"     => "Hadiah Utama",
                                "category"  => "custom_prize",
                                "image"     => $detail->CustomPrize[$i]->PrizeInvetory->image,
                                "unit"      => $detail->CustomPrize[$i]->quantity,
                                "prize"     => $detail->CustomPrize[$i]->PrizeInvetory->valuation
                            ];
                        }

                    }
                }
            }
            return $data;
        }
        if($detail->prize_mvp > 0){
            $tournament_mvp = TournamentMvpModel::where('id_tournament',$detail->id_tournament);
            if($tournament_mvp){
                $detail_data[] = [
                    'title'     => 'Stream Cash',
                    'level'     =>'Hadiah Utama',
                    'category'  => 'stream_cash',
                    'unit'      =>'1',
                    'prize'     => $detail->prize_mvp
                ];
                $data[] = [
                    'sequence' => "mvp",
                    'title' => 'mvp',
                    'description' => '',
                    'data' => $detail_data,
                ];
            }
        }
        return $data;
    }
    public function PrizeTicket($detail){
        if(count($detail->prizeTicket) > 0){
            if($detail->is_ticket_prize_new == 1){
                $ticket = AllTicketModel::find($detail->gift_ticket);
            }else if($detail->is_ticket_prize_new == 0){
                $ticket = TicketModel::find($detail->gift_ticket)->first(['names AS name']);
            }
        }
        if($detail->id_ticket_squence != null || $detail->id_ticket_squence != ""){
            for ($i=0; $i < count($detail->prizeTicket) ; $i++) {
                $data[] = [
                    'sequence' => (string) $detail->prizeTicket[$i]->urutan,
                    'title' => 'Juara '.$detail->prizeTicket[$i]->urutan,
                    'description' => '',
                    'data' => [],
                ];
                if($detail->id_winner_sequence != null || $detail->id_winner_sequence != ""){
                    if(count($detail->prize) > $i){
                        if($detail->prize[$i]->amount_given > 0){
                            $data[$i]['data'][] = [
                                "title"     => "Stream Cash",
                                "level"     => "Hadiah Utama",
                                "category"  => "stream_cash",
                                "unit"      => 0,
                                "prize"     => $detail->prize[$i]->amount_given
                            ];
                        }
                    }
                }else{
                    if( 3 > $i){
                        $prizes = explode(',',$detail->total_prize);
                        if($prizes[$i] > 0){
                            $data[$i]['data'][] = [
                                "title"     => "Stream Cash",
                                "level"     => "Hadiah Utama",
                                "category"  => "stream_cash",
                                "unit"      => 0,
                                "prize"     => $prizes[$i]
                            ];
                        }
                    }
                }

                $data[$i]['data'][] = [
                    "title"     => $ticket->name,
                    "level"     => "Hadiah Utama",
                    "category"  => "ticket",
                    "unit"      => $detail->prizeTicket[$i]->given_ticket,
                    "prize"     => 0
                ];
                if($detail->is_custom_prize == 1){
                    if(count($detail->CustomPrize) > $i ){
                    $data[$i]['data'][] = [
                        "title"     => $detail->CustomPrize[$i]->PrizeInvetory->title,
                        "level"     => "Hadiah Utama",
                        "category"  => "custom_prize",
                        "image"     => $detail->CustomPrize[$i]->PrizeInvetory->image,
                        "unit"      => $detail->CustomPrize[$i]->quantity,
                        "prize"     => $detail->CustomPrize[$i]->PrizeInvetory->valuation
                    ];
                    }

                }

            }
        }
        if($detail->prize_mvp > 0){
            $tournament_mvp = TournamentMvpModel::where('id_tournament',$detail->id_tournament);
            if($tournament_mvp){
                $detail_data[] = [
                    'title'     => 'Stream Cash',
                    'level'     =>'Hadiah Utama',
                    'category'  => 'stream_cash',
                    'unit'      =>'1',
                    'prize'     => $detail->prize_mvp
                ];
                $data[] = [
                    'sequence' => "mvp",
                    'title' => 'mvp',
                    'description' => '',
                    'data' => $detail_data,
                ];
            }
        }
        return $data;
    }
    public function prizeCustom($detail){
        if(count($detail->prizeTicket) > 0){
            if($detail->is_ticket_prize_new == 1){
                $ticket = AllTicketModel::find($detail->gift_ticket);
            }else if($detail->is_ticket_prize_new == 0){
                $ticket = TicketModel::find($detail->gift_ticket)->first(['names AS name']);
            }
        }
        if($detail->is_custom_prize != null || $detail->is_custom_prize != "0"){
            for ($i=0; $i < count($detail->CustomPrize) ; $i++) {
                $data[] = [
                    'sequence' => (string) $detail->CustomPrize[$i]->sequence,
                    'title' => 'Juara '.$detail->CustomPrize[$i]->sequence,
                    'description' => '',
                    'data' => [],
                ];
                $data[$i]['data'][] = [
                    "title"     => $detail->CustomPrize[$i]->PrizeInvetory->title,
                    "level"     => "Hadiah Utama",
                    "category"  => "custom_prize",
                    "image"     => $detail->CustomPrize[$i]->PrizeInvetory->image,
                    "unit"      => $detail->CustomPrize[$i]->quantity,
                    "prize"     => $detail->CustomPrize[$i]->PrizeInvetory->valuation
                ];
                if(count($detail->prizeTicket) > $i ){
                    $data[$i]['data'][] = [
                        "title"     => (($detail->is_ticket_prize_new == 1)? $detail->prizeTicket[$i]->ticketNew->name : $detail->prizeTicket[$i]->ticketOld->names),
                        "level"     => "Hadiah Utama",
                        "category"  => "ticket",
                        "unit"      => $detail->prizeTicket[$i]->given_ticket,
                        "prize"     => 0
                    ];
                }
                if($detail->id_winner_sequence != null || $detail->id_winner_sequence != ""){
                    if(count($detail->prize) > $i){
                        if($detail->prize[$i]->amount_given > 0){
                            $data[$i]['data'][] = [
                                "title"     => "Stream Cash",
                                "level"     => "Hadiah Utama",
                                "category"  => "stream_cash",
                                "unit"      => 0,
                                "prize"     => $detail->prize[$i]->amount_given
                            ];
                        }
                    }
                }else{
                    if( 3 > $i){
                        $prizes = explode(',',$detail->total_prize);
                        if($prizes[$i] > 0){
                            $data[$i]['data'][] = [
                                "title"     => "Stream Cash",
                                "level"     => "Hadiah Utama",
                                "category"  => "stream_cash",
                                "unit"      => 0,
                                "prize"     => $prizes[$i]
                            ];
                        }
                    }
                }

            }
        }
        if($detail->prize_mvp > 0){
            $tournament_mvp = TournamentMvpModel::where('id_tournament',$detail->id_tournament);
            if($tournament_mvp){
                $detail_data[] = [
                    'title'     => 'Stream Cash',
                    'level'     =>'Hadiah Utama',
                    'category'  => 'stream_cash',
                    'unit'      =>'1',
                    'prize'     => $detail->prize_mvp
                ];
                $data[] = [
                    'sequence' => "mvp",
                    'title' => 'mvp',
                    'description' => '',
                    'data' => $detail_data,
                ];
            }
        }
        return $data;
    }
    public function LiveStreaming($id_tournament){
        try{
            $get_streming =  TournamentStreamingModel::where('id_tournament',$id_tournament)->get();
            if($get_streming->isEmpty()){
                return "";
            }
            $response   = new Collection($get_streming, new TournamentStreamingTransform);
            $data = (new Manager)->createData($response)->toArray();
            return $data;
        } catch (\Throwable $th) {
          return "";
        }
    }


    public function ticketInfo($idTournament){
        $ticket = new TicketValidation;
        $getTicket = $ticket->getTournamentTicketInfo($idTournament, 'Bracket');

        if($getTicket['ticket_status'] == true){
        $buttonValidation = $ticket->buttonValidation($getTicket['data']['game_id'], $getTicket['data']['game_option']);

        //check button validation

            $getTicket['data']['user_detail'] = $buttonValidation['user_detail'];


        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => $buttonValidation['button'] ,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];

     }else{
        $dataTicket = [
            'status' => $getTicket['ticket_status'],
            'button_status' => null,
            'data_ticket' => $getTicket['ticket_status'] == true ? $getTicket['data'] : null

        ];
     }

        return $dataTicket;
    }

    public function bracket($id_tournament)
    {
        $data = [];
        $start_at = 0;
        $round128 = (new BracketOneTwoEight)->getBracket($id_tournament);
        if($round128){
            $start_at = 128;
            $data['round_128'] = (new TournamenBracketTransform)->TeamData('128',$start_at,$round128,$id_tournament);
        }
        else{
            $round64 = (new BracketSixtyFour)->getBracket($id_tournament);
            if ($round64) {
                $start_at = 64;
                $data['round_64'] = (new TournamenBracketTransform)->TeamData('64',$start_at,$round64,$id_tournament);
            }
            else {
                $round32 = (new BracketThirtyTwo)->getBracket($id_tournament);
                if ($round32) {
                    $start_at = 32;
                    $data['round_32'] = (new TournamenBracketTransform)->TeamData('32',$start_at,$round32,$id_tournament);
                    }
                else {
                    $round16 = (new BracketSixteen)->getBracket($id_tournament);
                    if ($round16) {
                        $start_at = 16;
                        $data['round_16'] = (new TournamenBracketTransform)->TeamData('16',$start_at,$round16,$id_tournament);
                    }
                    else {
                        $round8 = (new BracketQuarterFinal)->getBracket($id_tournament);
                        if ($round8) {
                            $start_at = 8;
                            $data['round_8'] = (new TournamenBracketTransform)->TeamData('8',$start_at,$round8,$id_tournament);
                        }
                        else {
                            $round4 = (new BracketSemiFinal)->getBracket($id_tournament);
                            if ($round4) {
                                $start_at = 4;
                                $data['round_4'] = (new TournamenBracketTransform)->TeamData('4',$start_at,$round4,$id_tournament);
                            }
                        }
                    }
                }
            }
        }
  

        if ($start_at != 0) {

            if($start_at > 64){
                $round_64 = (new BracketSixtyFour)->getBracketAfter($id_tournament);
                if($round_64 != false){
                    $data['round_64'] = (new TournamenBracketTransform)->TeamData('64',$start_at,$round_64,$id_tournament);
                }
            }

            if($start_at > 32){
                $round_32 = (new BracketThirtyTwo)->getBracketAfter($id_tournament);
                if($round_32 != false){
                    $data['round_32'] = (new TournamenBracketTransform)->TeamData('32',$start_at,$round_32,$id_tournament);
                }
            }

            if($start_at > 16){
                $round_16 = (new BracketSixteen)->getBracketAfter($id_tournament);
                if($round_16 != false){
                    $data['round_16'] = (new TournamenBracketTransform)->TeamData('16',$start_at,$round_16,$id_tournament);
                }
            }

            if($start_at > 8){
                $round_8 = (new BracketQuarterFinal)->getBracketAfter($id_tournament);
                if($round_8 != false){
                    $data['round_8'] = (new TournamenBracketTransform)->TeamData('8',$start_at,$round_8,$id_tournament);
                }
            }

            if($start_at > 4){
                $round_4 = (new BracketSemiFinal)->getBracketAfter($id_tournament);
                if($round_4 != false){
                    $data['round_4'] = (new TournamenBracketTransform)->TeamData('4',$start_at,$round_4,$id_tournament);
                }
            }

            $round_3 = (new BracketThirdPlace)->getBracketAfter($id_tournament);
            if($round_3){
                $data['round_3'] = (new TournamenBracketTransform)->TeamData('3',$start_at,$round_3,$id_tournament);
            }

            $round_2 = (new BracketFinal)->getBracketAfter($id_tournament);
            if($round_2){
                $data['round_2'] = (new TournamenBracketTransform)->TeamData('2',$start_at,$round_2,$id_tournament);
            }

            $winner = (new BracketFinal)->getWinner($id_tournament);
            if($winner){
                $data['winner'] = (new TournamenBracketTransform)->winner($winner,$id_tournament);
            }

            $third = (new BracketThirdPlace)->thirdWinner($id_tournament);
            if($third){
                $data['third'] = (new TournamenBracketTransform)->winner($third,$id_tournament);
            }
        }

        
        $data['start_at'] = $start_at;
        $data['mvp']      = $this->GetMvp($id_tournament);

        return $data;
    }
    public function scheduleChallonge($data){
        $url = $data->url_challonge;
        $account= AccountChallongeModel::first('api_key');
        $dataPart = [
           'api_key' => $account['api_key']
        ];
        $response = (new MyApps)->GetUrlChallonge('matches',$url,$dataPart);
        $decode = json_decode($response,true);
        $resp = (new ScheduleChallongeTransform)->transform($decode);
        $collection = collect($resp);
        $grouped = $collection->groupBy('round');
        $grouped->toArray();
        $grouped['is_challonge']    = ($data->is_challonge > 0) ? true : false;
        $grouped['total']           = (int) $data->filled_slot;
        $grouped['system']          = $data->tourname_type;
        return $grouped;
    }
    /**
     * Request MatchOverview - Bracket
     *
     * This endpoint is used to get detailed information about the match in the bracket type tournament
     * @urlParam round required current round numbers Example:3
     * @urlParam start required the starting number of the round Example:64
     * @urlParam id required round id Example:swupalzW1jSFGJc
     * @responseFile status=200 scenario="success" responses/bracket.matchoverview.json
     * @response status=404 {
            "status": false,
            "message": "Match overview tidak tersedia"
        }
     */
    public function matchsOverview($round,$start,$id)
    {
        try {

            $fractal = new Manager();

            $id_round = (new MyApps)->onlyDecrypt($id);

            if($start<$round){
                return response()->json([
                    'status' => false,
                    'message' => 'no data found'
                ]);
            }

            $arr_roundtext = [
                8 => 'Quater-Final',
                4 => 'Semi-Final',
                3 => 'Third-Place',
                2 => 'Final'
            ];

            if($round > 8){
                $roundtext = "Round Of ".$round;
            }
            else{
               $roundtext = $arr_roundtext[$round];
            }

            if($round==128){
                $teaminfo = (new BracketOneTwoEight)->getTeamInfo($id_round);
            }
            if($round==64){
                $teaminfo = (new BracketSixtyFour)->getTeamInfo($id_round);
            }
            if($round==32){
                $teaminfo = (new BracketThirtyTwo)->getTeamInfo($id_round);
            }
            if($round==16){
                $teaminfo = (new BracketSixteen)->getTeamInfo($id_round);
            }
            if($round==8){
                $teaminfo = (new BracketQuarterFinal)->getTeamInfo($id_round);
            }
            if($round==4){
                $teaminfo = (new BracketSemiFinal)->getTeamInfo($id_round);
            }
            if($round==3){
                $teaminfo = (new BracketThirdPlace)->getTeamInfo($id_round);
            }
            if($round==2){
                $teaminfo = (new BracketFinal)->getTeamInfo($id_round);
            }

            $teaminfo->dataMatchs;

            $teamDatas = (new TournamenBracketTransform)->matchOverview($teaminfo);


            $scoreMatch = $teaminfo->dataMatchs->scoreMatchs;

            $scoreMatchs = new Collection($scoreMatch,(new ScoreMatchsTransform));

            $arrayScoreMatchs = current($fractal->createData($scoreMatchs)->toArray());

            $scoreMatchCollection = collect($arrayScoreMatchs);
            $playerData = $scoreMatchCollection->groupBy('index');
            $playerData->toArray();

            $team1Data = collect($playerData[$teamDatas['team1']['name']]);
            $team2Data = collect($playerData[$teamDatas['team2']['name']]);

            $killTeam1 = $team1Data->sum('kda.kills');
            $deathTeam1 = $team1Data->sum('kda.death');
            $assistTeam1 = $team1Data->sum('kda.assist');

            $killTeam2 = $team2Data->sum('kda.kills');
            $deathTeam2 = $team2Data->sum('kda.death');
            $assistTeam2 = $team2Data->sum('kda.assist');

            $totalKillsTeam1 = $killTeam1/($killTeam1+$killTeam2) * 100;
            $totalKillsTeam2 = $killTeam2/($killTeam1+$killTeam2) * 100;

            $totalDeathTeam1 = $deathTeam1/($deathTeam1+$deathTeam2) * 100;
            $totalDeathTeam2 = $deathTeam2/($deathTeam1+$deathTeam2) * 100;

            $totalAssistTeam1 = $assistTeam1/($assistTeam1+$assistTeam2) * 100;
            $totalAssistTeam2 = $assistTeam1/($assistTeam1+$assistTeam2) * 100;

            $teamDatas['team1']['total_kda'] = [
                'kills' => number_format($totalKillsTeam1, 2),
                'death' => number_format($totalDeathTeam1, 2),
                'assist' => number_format($totalAssistTeam1, 2)
            ];

            $teamDatas['team2']['total_kda'] = [
                'kills' => number_format($totalKillsTeam2, 2),
                'death' => number_format($totalDeathTeam2, 2),
                'assist' => number_format($totalAssistTeam2, 2)
            ];

            return response()->json([
                'overviewdata' => $teamDatas,
                'playerdata' => $playerData
                ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.bracket.matchoverview.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    public function totalPrizes($prizes)
    {
        $total = 0;
        foreach ($prizes as $prize) {
            $total += ($prize->quantity * $prize->PrizeInvetory->valuation);
        }

        return $total;
    }


    public function GetMvp($id_tournament){
        $tournament = TournamentModel::where('id_tournament','=',$id_tournament)->first('id_group');
        $id_group= $tournament->id_group;
        $details = TournamentMvpModel::with(['PlayerPayment' =>  function($playerPayment) use($id_tournament,$id_group) {
            $playerPayment->where('player_payment.id_tournament','=',$id_tournament);
            $playerPayment->with(['getTeam' => function($rowss) use($id_group){
                $rowss->where('tournament_group.id_group','=',$id_group);
            }]);

        }])
        ->whereHas('PlayerPayment' ,  function($playerPayment) use($id_tournament,$id_group) {
            $playerPayment->where('player_payment.id_tournament','=',$id_tournament);
            $playerPayment->with(['getTeam' => function($rowss) use($id_group){
                $rowss->where('tournament_group.id_group','=',$id_group);
            }]);

        })
        ->where('id_tournament','=',$id_tournament)
        ->first();

        if(!empty($details)){
        $data = (new TournamentMvpTransform)->transform($details);
        }else{
            $data = null;
        }
         return $data;
    }


}
//deploy