<?php

namespace App\Http\Controllers;

use App\Helpers\MyApps;
use App\Helpers\Redis\RedisHelper;
use App\Model\TournamentLiga;
use App\Model\TournamentListModel;
use App\Transformer\TournamentLigaCardTransform;
use App\Transformer\TournamentListTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class TournamentListViii extends Controller
{
    private $date;
    private $dateformat;
    private $dateweekstart;
    private $dateweekend;

    public function __construct()
    {
        $this->date = new Carbon();
        $this->dateformat =  'Y-m-d H:i';
        $this->dateweekstart = $this->date->now()->startOfWeek(Carbon::MONDAY)->format($this->dateformat);
        $this->dateweekend  = $this->date->now()->endOfWeek(Carbon::SUNDAY)->format($this->dateformat);
    }

    /**
     * Tournament List with Liga V2
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need.
     * new : Tournament Liga Card, Tournament Category
     * @queryParam tab string Get tournament by tab name. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.liga.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function frontPage(Request $request)
    {
        $myapps = new MyApps();
        $time = $request->tab != null ? $request->tab : "all";

        if ($cache = RedisHelper::getData('tournament:list:front:' . $time)) {
            return $cache;
        }



        $routename = 'tournament.list.lviii';
        $response = [
            "header" => [
                "title" => Lang::get('tournament.page.title'),
                "desc"  => Lang::get('tournament.page.description')
            ],
            "tab" => [
                [
                    "tabname"   => Lang::get('tournament.tabname.all'),
                    "url"       => "all"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.thisweek'),
                    "url"       => "this_week"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.upcoming'),
                    "url"       => "upcoming"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.finished'),
                    "url"       => "finished"
                ]
            ],
            "url_load_all_tournament"   => $myapps->getPathRoute($routename),
            "url_for_search_tournament" => $myapps->getPathRoute($routename,['search' => ""]),
            "url_filter_by_game"        => $myapps->getPathRoute($routename,['game' => ""]),
            "data"  => [
                "liga_card"                 => $this->ligacard($time),
                "tournament_with_category"  => $this->tournamentCategory($time),
                "tournament_no_category"    => $this->tournamentNoCategory()
            ]
        ];

        RedisHelper::setData('tournament:list:front:' . $time, $response, 7200);

        return response()->json($response);
    }


    /**
     * Tournament List Front Page
     *
     * This endpoint serves to retrieve tournament list data,
     * and you can also filter the data you need.
     * new : Remove Tournamen Liga Card
     * @queryParam tab string Get tournament by tab name. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.liga.v2.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function frontPageV2(Request $request)
    {
        $myapps = new MyApps();
        $time = $request->tab != null ? $request->tab : "all";

        if ($cache = RedisHelper::getData('tournament:list:front:v2:' . $time)) {
            return $cache;
        }



        $routename = 'tournament.list.lviii';
        $response = [
            "header" => [
                "title" => Lang::get('tournament.page.title'),
                "desc"  => Lang::get('tournament.page.description')
            ],
            "tab" => [
                [
                    "tabname"   => Lang::get('tournament.tabname.all'),
                    "url"       => "all"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.thisweek'),
                    "url"       => "this_week"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.upcoming'),
                    "url"       => "upcoming"
                ],
                [
                    "tabname"   => Lang::get('tournament.tabname.finished'),
                    "url"       => "finished"
                ]
            ],
            "url_load_all_tournament"   => $myapps->getPathRoute($routename),
            "url_for_search_tournament" => $myapps->getPathRoute($routename,['search' => ""]),
            "url_filter_by_game"        => $myapps->getPathRoute($routename,['game' => ""]),
            "data"  => [
                "tournament_with_category"  => $this->tournamentCategory($time),
                "tournament_no_category"    => $this->tournamentNoCategory()
            ]
        ];

        RedisHelper::setData('tournament:list:front:v2:' . $time, $response, 7200);

        return response()->json($response);
    }

    public function ligacard($time=null)
    {
        try {
            $data = TournamentLiga::with('game');

            if($time=="this_week"){
                $data = $data->whereBetween('start_tour',[$this->dateweekstart,$this->dateweekend])
                            ->orWhereDate('start_tour','<=',$this->dateweekstart)
                            ->whereDate('end_tour','>=',$this->dateweekend);
            }
            elseif ($time=="upcoming") {
                $data = $data->whereDate('start_tour','>',$this->dateweekend);
            }
            elseif ($time=="finished") {
                $data = $data->whereDate('end_tour','<',$this->dateweekstart);
            }

            $data = $data->where('on_view',1)->orderBy('start_tour','DESC')->get();

            $transform = new Collection($data,(new TournamentLigaCardTransform));
            $transform = (new Manager())->createData($transform)->toArray();

            return current($transform);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function tournamentCategory($time=null)
    {
        if($time=="this_week"){
            $data = [
                [
                    "category"  => "Tournament Minggu Ini",
                    "url"       => "this_week",
                    "data"      => (new TournamentListNew)->this_week(null,null,null,null,10)
                ]
            ];
        }
        elseif ($time=="upcoming") {
            $data = [
                [
                    "category"  => "Tournament Akan Datang",
                    "url"       => "upcoming",
                    "data"      => (new TournamentListNew)->upcoming(null,null,null,null,10)
                ]
            ];
        }
        elseif ($time=="finished") {
            $data = [
                [
                    "category"  => "Tournament Selesai",
                    "url"       => "finished",
                    "data"      => (new TournamentListNew)->finished(null,null,null,10)
                ]
            ];
        }
        else{
            $data = [];
        }

        return $data;
    }

    public function tournamentNoCategory()
    {
        try {
            $data = TournamentListModel::with(['AvailableSlots' => function ($q)
                                        {
                                            $q->select('id_leaderboard','total_team')
                                              ->where('qualification_sequence',1);

                                        }])
                                        ->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                                        FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                                        FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                                        GROUP BY tournament_id) prizes'),function($join){
                                           $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                                       })
                                        ->where('statuss','!=',5)
                                        ->with(['game' => function ($q)
                                                {
                                                    $q->select('id_game_list','name','games_on','logo_game_list');

                                                }])
                                        ->where('tournament_date','>=',$this->dateweekstart)
                                        ->orderBy('statuss')->orderBy('filled')
                                        ->orderBy('tournament_date','asc')
                                        ->limit(20)->get();




            $tournament = new Collection($data, (new TournamentListTransform));

            return current((new Manager())->createData($tournament)->toArray());

        } catch (\Exception $e) {
            return [];
        }
    }
}
