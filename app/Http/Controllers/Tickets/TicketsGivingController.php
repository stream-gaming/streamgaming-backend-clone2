<?php

namespace App\Http\Controllers\Tickets;

use App\Helpers\Ticket\TeamTicketFactory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Ticket\TicketRole;
use App\Helpers\Ticket\UserTicketFactory;
use App\Model\AllTicketModel;
use App\Model\TicketsModel;
use App\Traits\TicketClaimsTrait;
use Illuminate\Support\Facades\Auth;

/**
 * @group Ticket
 * @authenticated
 * APIs for the Giving Ticket  | Endpoint untuk pemberian tiket
 */
class TicketsGivingController extends Controller
{

    /**
     * Post Giving Ticket User.
     * <aside class="notice">APIs Method for Giving Ticket to Specific User | Untuk Memberikan Tiket ke Specific User</aside>
     * @bodyParam user_id string required the id of the user you want to provide. Example: 6778yt568
     * @bodyParam ticket_id string required the id of the Ticket you want to provide. Example: 7
     * @response scenario="success" status="200" {
            "status": true,
            "message": "ticket.5"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "failed"
        }
     */
    public function giveTicketUser(Request $request)
    {
        try {

            $max_has = AllTicketModel::where('id',$request->ticket_id)->first();

            $user_has = $this->getHas('user',$request->user_id,$max_has);

            $remaining = $request->quantity;

            if($max_has->max_has > 0){
                $remaining = $max_has->max_has - $user_has;
            }

            $give = $remaining >= $request->quantity ? $request->quantity : $remaining;

            $factory    = new UserTicketFactory();

            $data = $factory->addTicket($request->user_id, $request->ticket_id, TicketRole::ADMINGIFT_SOURCE, $give);

            return response()->json([
                'status'    => true,
                'message'   => 'success',
                'data'      => $data
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => 'failed',
                'data'      => $e->getMessage()
            ], $this->HTTP_BAD);
        }
    }

     /**
     * Post Giving Ticket Team.
     * <aside class="notice">APIs Method for Giving Ticket to Specific Team | Untuk Memberikan Tiket ke Specific Team</aside>
     * @bodyParam team_id string required the id of the team you want to provide. Example: 6778yt568
     * @bodyParam ticket_id string required the id of the Ticket you want to provide. Example: 7
     * @response scenario="success" status="200" {
            "status": true,
            "message": "ticket.5"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "failed"
        }
     */
    public function giveTicketTeam(Request $request)
    {
        try {
            $max_has = AllTicketModel::where('id',$request->ticket_id)->first();

            $team_has = $this->getHas('team',$request->team_id,$max_has);

            $remaining = $request->quantity;

            if($max_has->max_has >0){
                $remaining = $max_has->max_has - $team_has;
            }

            $give = $remaining >= $request->quantity ? $request->quantity : $remaining;

            $factory    = new TeamTicketFactory();

            $data = $factory->addTicket($request->team_id, $request->ticket_id, TicketRole::ADMINGIFT_SOURCE, $give);

            return response()->json([
                'status'    => true,
                'message'   => 'success',
                'data'      => $data
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => 'failed',
                'data'      => $e->getMessage()
            ], $this->HTTP_BAD); //update ya
        }
    }

    public function getHas($type, $id, $ticket)
    {
        switch ($type) {
            case 'user':
                $ticket_has = TicketsModel::where('user_id', $id);
                break;

            case 'team':
                $ticket_has = TicketsModel::where('team_id', $id);
                break;
        }

        return $ticket_has->where('ticket_id', $ticket->id)->count();
    }
}
