<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Ticket\InsertTicket;
use App\Model\AllTicketModel;
use App\Model\TicketsModel;
use App\Services\UploadService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Transformer\TicketsTransform;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

/**
 * @hideFromAPIDocumentation
 */
class TicketsController extends Controller
{
    public function InsertTicket(Request $request)
    {
        $request->validate([
            'image'  => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $check = (new InsertTicket)->InsertTicket($request);

        if ($check == false) {
            return false;
        }
        try {
            if ($request->file('image')) {
                $file = $request->file('image');
                $image = (new UploadService)->upload('public/content/ticket', $file, null, [
                    'options' => 'resize',
                    'width' => 336,
                    'height' => 146
                ]);
        
                //check error when upload
                if (is_object($image)) {
                    return $image;
                }
                $request->image          = $image;
             }
            $flight = new AllTicketModel;
            $flight->name           = $request->name;
            $flight->type           = $request->type_ticket;
            $flight->sub_type       = $request->sub_type;
            $flight->unit           = (int) $request->unit;
            $flight->max_given      = (int) $request->max_given;
            $flight->expired        = date('y-m-d', strtotime($request->date_expired)) . " " . date('h:i:s', strtotime($request->time_expired));
            $flight->event_end      = date('y-m-d', strtotime($request->date_event)) . " " . date('h:i:s', strtotime($request->time_event));
            $flight->competition    = $request->kompetisi;
            $flight->can_traded     = (int) $request->hidden_can_traded;
            $flight->market_value   = (int) $request->market_value;
            $flight->giving_target  = (int) $request->giving_target;
            $flight->image          = $request->image;
            $flight->mode           = (int) $request->mode;
            $flight->max_has        = (int) $request->max_has;

            $flight->save();
            return response()->json([
                'status'    => true,
                'message'   => 'success',
                'data'      => "s",
            ]);
        } catch (\Exception $e) {
            Log::debug($e);
        }
    }
    public function destroy(AllTicketModel $ticket)
    {
        try {
            DB::transaction(function () use ($ticket) {
                $ticket->delete();
            });

            return response()->json([
                'status'    => true,
                'message'   => 'success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'fail',
                'error' => $e->getMessage(),
                'full_error' => $e
            ], 409);
        }
    }

    public function show(Request $request)
    {
        $ticket = AllTicketModel::where('id', $request->ticket)
            ->with('game')
            ->first();

        $result = [
            'name'          =>  $ticket->name,
            'type'          =>  $ticket->type,
            'sub_type'      =>  $ticket->sub_type,
            'unit'          =>  $ticket->unit,
            'max_given'     =>  $ticket->max_given,
            'total_given'   =>  $ticket->total_given,
            'expired'       =>  date('Y-m-d H:i:s', strtotime($ticket->expired)),
            'event_end'     =>  date('Y-m-d H:i:s', strtotime($ticket->event_end)),
            'competition'   =>  $ticket->competition,
            'game_name'     =>  $ticket->competition != "all" ? $ticket->game->name : 'all',
            'can_traded'    =>  $ticket->can_traded,
            'market_value'  =>  $ticket->market_value,
            'giving_target' =>  $ticket->giving_target,
            'mode'          =>  $ticket->mode,
            'max_has'       =>  $ticket->max_has,
            'created_at'    =>  date('Y-m-d H:i:s', strtotime($ticket->created_at))
        ];


        return response()->json($result);
    }

}
