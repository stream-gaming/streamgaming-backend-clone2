<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AllTicketModel;
use App\Model\TicketModel;
use App\Model\TicketsModel;
use App\Model\TicketTeamModel;
use App\Model\TicketUserModel;

/**
 * @hideFromAPIDocumentation
 */
class TicketListController extends Controller
{
    /**
     * Get List User Who Have Ticket New.
     * <aside class="notice">APIs Method for Get List User Who Have Ticket | untuk melihat list user yang mempunyai tiket</aside>
     * @urlParam ticket required the ID of Ticket, for detail information | detail list user. Example: 1
     * @urlParam search the username of user. Example: mellydecyber
     * @responseFile status=200 scenario="success" responses/account.ticket.list.user.json
     */
    public function listUserHaveTicketNew(Request $request)
    {
        $limit    = $request->limit != null ? $request->limit : 10;
        $search   = $request->search;

        $ticket = TicketsModel::where('ticket_id', $request->ticket)
            ->where(function ($q) {
                $q->where("tickets.user_id", '!=', "");
                $q->WhereNotNull("tickets.user_id");
            })
            ->with('user')
            ->groupBy('tickets.user_id')
            ->selectRaw("user_id, sum(case when is_used='0' then 1 else 0 end) as available,
            sum(case when is_used='1' then 1 else 0 end) as used");

        if ($search != null) {
            $ticket = $ticket->whereHas('user', function ($q) use ($search) {
                $q->where('username', 'LIKE', '%' . $search . '%');
            });
        }

        $ticket = $ticket->paginate($limit);

        $ticket->transform(function ($data, $index) {
            return [
                'total'     => $data->available + $data->used,
                'available' => $data->available,
                'used'      => $data->used,
                'user_id'  => $data->user_id,
                'username' => $data->user->username,
                'fullname' => $data->user->fullname
            ];
        });

        $all_ticket = AllTicketModel::where('id', $request->ticket)
            ->with('game')
            ->first();

        $info = [
            'name'          => $all_ticket->name,
            'competition'   => $all_ticket->competition == 'all' ? 'all' : $all_ticket->game->name,
            'max_given'     => $all_ticket->max_given,
            'total_given'   => $all_ticket->total_given
        ];

        return response()->json([
            'data' => $ticket,
            'info' => $info,
            'type' => 'user'
        ]);
    }

    /**
     * Get List Team Who Have Ticket New.
     * <aside class="notice">APIs Method for Get List Team Who Have Ticket | untuk melihat list user yang mempunyai tiket</aside>
     * @urlParam ticket required the ID of Ticket, for detail information | detail list user. Example: 1
     * @urlParam search the name of team. Example: uburubur
     * @responseFile status=200 scenario="success" responses/account.ticket.list.team.json
     */
    public function listTeamHaveTicketNew(Request $request)
    {
        $limit    = $request->limit != null ? $request->limit : 10;
        $search   = $request->search;

        $ticket = TicketsModel::where('ticket_id', $request->ticket)
            ->where(function ($q) {
                $q->where("tickets.team_id", '!=', "");
                $q->WhereNotNull("tickets.team_id");
            })
            ->with('team')
            ->groupBy('tickets.team_id')
            ->selectRaw("sum(case when is_used='0' then 1 else 0 end) as available,
            sum(case when is_used='1' then 1 else 0 end) as used, team_id");

        if ($search != null) {
            $ticket = $ticket->whereHas('team', function ($q) use ($search) {
                $q->where('team_name', 'LIKE', $search . '%');
            });
        }

        $ticket = $ticket->paginate($limit);

        $ticket->transform(function ($data, $index) {
            return [
                'total'     => $data->available + $data->used,
                'available' => $data->available,
                'used'      => $data->used,
                'team_id'  => $data->team_id,
                'team'     => $data->team->team_name,
            ];
        });

        $all_ticket = AllTicketModel::where('id', $request->ticket)
            ->with('game')
            ->first();

        $info = [
            'name'          => $all_ticket->name,
            'competition'   => $all_ticket->competition == 'all' ? 'all' : $all_ticket->game->name,
            'max_given'     => $all_ticket->max_given,
            'total_given'   => $all_ticket->total_given
        ];

        return response()->json([
            'data' => $ticket,
            'info' => $info,
            'type' => 'team'
        ]);
    }

    /**
     * Get List User Who Have Ticket Old.
     * <aside class="notice">APIs Method for Get List User Who Have Ticket | untuk melihat list user yang mempunyai tiket</aside>
     * @urlParam ticket required the ID of Ticket, for detail information | detail list user. Example: 1
     * @urlParam search the username of user. Example: mellydecyber
     * @responseFile status=200 scenario="success" responses/account.ticket.list.user.json
     */
    public function listUserHaveTicketOld(Request $request)
    {
        $limit    = $request->limit != null ? $request->limit : 10;
        $search   = $request->search;

        $ticket = TicketUserModel::where('id_ticket', $request->ticket)
            ->with('user');

        if ($search != null) {
            $ticket = $ticket->whereHas('user', function ($q) use ($search) {
                $q->where('username', 'LIKE', '%' . $search . '%');
            });
        }

        $ticket = $ticket->paginate($limit);

        $ticket->transform(function ($data, $index) {
            return [
                'total'    => $data->ticket,
                'user_id'  => $data->id_users,
                'username' => $data->user->username ?? '-',
                'fullname' => $data->user->fullname ?? '-'
            ];
        });

        $all_ticket = TicketModel::where('id_ticket', $request->ticket)
            ->with('game')
            ->first();

        $info = [
            'name'          => $all_ticket->names,
            'competition'   => $all_ticket->kompetisi == 'all' ? 'all' : $all_ticket->game->name,
            'max_given'     => $all_ticket->limits,
            'total_given'   => $all_ticket->gived_ticket
        ];

        return response()->json([
            'data' => $ticket,
            'info' => $info,
            'type' => 'user'
        ]);
    }

    /**
     * Get List Team Who Have Ticket Old.
     * <aside class="notice">APIs Method for Get List Team Who Have Ticket | untuk melihat list user yang mempunyai tiket</aside>
     * @urlParam ticket required the ID of Ticket, for detail information | detail list user. Example: 1
     * @urlParam search the name of team. Example: uburubur
     * @responseFile status=200 scenario="success" responses/account.ticket.list.team.json
     */
    public function listTeamHaveTicketOld(Request $request)
    {
        $limit    = $request->limit != null ? $request->limit : 10;
        $search   = $request->search;

        $ticket = TicketTeamModel::where('id_ticket', $request->ticket)
            ->with('team')
            ->where('ticket', '!=', '0');

        if ($search != null) {
            $ticket = $ticket->whereHas('team', function ($q) use ($search) {
                $q->where('team_name', 'LIKE', $search . '%');
            });
        }

        $ticket = $ticket->paginate($limit);

        $ticket->transform(function ($data, $index) {
            return [
                'total'    => $data->ticket,
                'team_id'  => $data->id_team,
                'team'     => $data->team->team_name ?? '-',
            ];
        });

        $all_ticket = TicketModel::where('id_ticket', $request->ticket)
            ->with('game')
            ->first();

        $info = [
            'name'          => $all_ticket->names,
            'competition'   => $all_ticket->kompetisi == 'all' ? 'all' : $all_ticket->game->name,
            'max_given'     => $all_ticket->limits,
            'total_given'   => $all_ticket->gived_ticket
        ];

        return response()->json([
            'data' => $ticket,
            'info' => $info,
            'type' => 'team'
        ]);
    }
}
