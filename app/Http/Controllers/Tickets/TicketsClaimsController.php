<?php

namespace App\Http\Controllers\Tickets;

use App\Helpers\ThrottlesAccess;
use App\Helpers\ThrottlesLogins;
use App\Helpers\Ticket\TeamTicketFactory;
use App\Helpers\Ticket\TicketRole;
use App\Helpers\Ticket\UserTicketFactory;
use App\Http\Controllers\Api\UserNotificationController;
use App\Http\Controllers\Controller;
use App\Model\AllTicketModel;
use App\Model\HistoryTicketsModel;
use App\Model\TeamModel;
use App\Model\TicketClaimsModel;
use App\Traits\TicketClaimsTrait;
use App\Transformer\TicketsClaimsTransform;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @group Ticket
 * @authenticated
 * APIs for the Panel Claims Ticket  | Endpoint untuk panel klaim tiket
 */
class TicketsClaimsController extends Controller
{
    use TicketClaimsTrait, ThrottlesAccess;

    //- Max Attempts
    protected $maxAttempts;

    //- Block Time Per Minute
    protected $decayMinutes;

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Get Ticket Claims.
     * <aside class="notice">APIs Method for Panel Ticket Claims | Panel Klaim Tiket</aside>
     * @urlParam type required for type tab what you want to see [team,user] | kirim request data yang ingin ditampilkan. Example: user
     * @responseFile status="200" scenario="user" responses/ticket.claims.user.json
     * @responseFile status="200" scenario="team" responses/ticket.claims.team.json
     * @responseFile status="200" scenario="user V2" responses/ticket.claims.user.v2.json
     * @responseFile status="200" scenario="team V2" responses/ticket.claims.team.v2.json
     */
    public function viewClaims(Request $request)
    {
        $search = $request->search;
        //- get Tipe tab Klaim Tiket yang ingin ditampilkan
        switch ($request->type) {
            case 'team':

                //- Jika tipe tab nya Team, maka membutuhkan id team, dimana user yang sedang login adalah leader
                $team       = collect(TeamModel::where("leader_id", Auth::user()->user_id)->get(['game_id', 'team_id'])->toArray());

                $team_id = $team->mapWithKeys(function ($item) {
                    return [$item['game_id'] => $item['team_id']];
                });

                //- get All ticket yang bertipe team, dan yang competition(game_id di tiket) sesuai dengan team yang user berstatus leader, untuk ambil game_idnya
                $ticket_id  = AllTicketModel::team()
                    ->where('all_ticket.expired', '>=', Carbon::now()->format('Y-m-d'))
                    ->whereIn("competition", $team->map(function ($item) {
                        return $item['game_id'];
                    }));

                if($search!=null){
                    $ticket_id  =  $ticket_id->where('name','LIKE','%'.$search.'%');
                }
                $ticket_id  = $ticket_id->get(['id']);
                break;

            default:
                //- get All ticket yang bertipe User/Personal
                $ticket_id  = AllTicketModel::personal()
                    ->where('all_ticket.expired', '>=', Carbon::now()->format('Y-m-d'));

                if($search!=null){
                    $ticket_id  =  $ticket_id->where('name','LIKE','%'.$search.'%');
                }

                $ticket_id  = $ticket_id->get(['id']);
                $team_id    = null;
                break;
        }

        $data["active_tab"] = $request->type;

        $ticket_claims = TicketClaimsModel::active()
            ->leftJoin('all_ticket', function ($join) {
                $join->on('all_ticket.id', '=', 'ticket_claims.ticket_id');
            })
            ->whereIn('ticket_id', $ticket_id)
            ->current()
            // ->orderby("expired","ASC")
            ->get(['ticket_id', 'max_claim', 'ticket_claims.event_start', 'ticket_claims.event_end', 'mode', 'expired', 'period', 'name', 'max_given', 'total_given', 'competition', 'type', 'image']);

        $data["ticket"] = collect(current(fractal()
            ->collection($ticket_claims)
            ->transformWith(new TicketsClaimsTransform($request->type, $team_id))
            ->toArray()));
        $data["ticket"] = $data["ticket"]->collapse();
        $dataTicket = $data['ticket'];
        $data["ticket"] = $data["ticket"]->sortBy(function($dataTicket) {
            return  [$dataTicket['is_claim'],$dataTicket['expired']];
        })->sortByDesc('is_available')->values();
        return response()->json($data);

    }

    /**
     * Post Ticket Claims.
     * <aside class="notice">APIs Method for Ticket Claims | Klaim Tiket</aside>
     * @urlParam type required for type tab what you want to see [team,user] | kirim request data yang ingin ditampilkan. Example: user
     * @bodyParam ticket_id required The ID of ticket you wanna claim | ID tiket yang ingin anda klaim. Example: 2
     * @response scenario="success" status="200" {
            "status": true,
            "message": "ticket.5"
        }
     * @response scenario="max_claim reached" status="400" {
            "status": false,
            "message": "User sudah mencapai maksimal klaim di tiket ini"
        }
     * @response scenario="max_given reached" status="400" {
            "status": false,
            "message": "Tiket ini sudah mencapai maksimal klaim"
        }
     */
    public function claimTicket(Request $request)
    {
        try {
            DB::beginTransaction();

            //- Validate
            $all_ticket = AllTicketModel::where("id", $request->ticket_id)
                ->where('all_ticket.expired', '>=', Carbon::now()->format('Y-m-d'));

            switch ($request->type) {
                case 'team':
                    //- Team
                    $all_ticket = $all_ticket->team();
                    break;
                case 'user':
                    //- User
                    $all_ticket = $all_ticket->personal();
                    break;
            }

            $all_ticket = $all_ticket->firstOr(function () {
                throw new Exception(trans('message.ticket.not-found'), $this->HTTP_NOT_FOUND);
            });

            $ticket = TicketClaimsModel::active()
                ->leftJoin('all_ticket', function ($join) {
                    $join->on('all_ticket.id', '=', 'ticket_claims.ticket_id');
                })
                ->where('ticket_id', $request->ticket_id)
                ->current()
                ->select(['all_ticket.name', 'ticket_id', 'max_claim', 'ticket_claims.event_start', 'ticket_claims.event_end', 'mode', 'expired', 'period', 'name', 'max_given', 'total_given', 'competition', 'max_has'])
                ->firstOr(function () {
                    throw new Exception(trans('message.ticket.claims.not-found'), $this->HTTP_NOT_FOUND);
                });


            $this->decayMinutes = 5;
            $this->maxAttempts  = $ticket->max_claim;
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendResponse($request);
            }


            switch ($request->type) {
                case 'team':
                    //- Team
                    $team   = TeamModel::where("leader_id", Auth::user()->user_id)
                        ->where("game_id", $ticket->competition)
                        ->first(['team_id', 'game_id']);

                    $factory    = new TeamTicketFactory();
                    $has_claim  = $this->getClaim($request->type, $ticket, $team->team_id);
                    $id         = $team->team_id;

                    break;
                case 'user':
                    //- User
                    $factory    = new UserTicketFactory();
                    $has_claim  = $this->getClaim($request->type, $ticket);
                    $id         = Auth::user()->user_id;

                    break;
            }

            $get_has = $this->getHas($request->type, $id, $ticket)->count();

            if ($get_has >= $ticket->max_has) {
                throw new Exception(trans('message.ticket.claims.reached.has', ['ticket' => $ticket->name, 'amount' => $ticket->max_has]), $this->HTTP_BAD);
            }

            if ($has_claim >= $ticket->max_claim) {
                throw new Exception(trans('message.ticket.claims.reached.claim', ['ticket' => $ticket->name, 'amount' => $ticket->max_claim, 'period' => trans('message.ticket.period.' . $ticket->period)]), $this->HTTP_BAD);
            }

            if ($ticket->max_given != 0) {
                if ($ticket->total_given >= $ticket->max_given) {
                    throw new Exception(trans('message.ticket.claims.reached.given'), $this->HTTP_BAD);
                }
            }

            $this->incrementAttempts($request);

            $data = $factory->addTicket($id, $ticket->ticket_id, TicketRole::CLAIMS_SOURCE);

            new UserNotificationController('claim_ticket', $ticket);

            DB::commit();

            return response()->json($data);
        } catch (\Exception $e) {
            $code = $e->getCode() == 0 ? $this->HTTP_BAD : $e->getCode();
            DB::rollBack();
            return response()->json([
                'status'    => false,
                'message'   => $e->getMessage(),
            ], $code);
        } //update lagi yank
    }
}
