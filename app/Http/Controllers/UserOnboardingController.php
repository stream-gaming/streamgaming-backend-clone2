<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserOnboardingStatusRequest;
use App\Modules\Onboarding\UserOnboarding;
use App\Modules\Onboarding\UserTournamentOnboarding;
use App\Transformer\UserOnboardingInfoTransformer;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
/**
 * @group  User Onboarding
 *
 * APIs for managing user's onboarding
 */
class UserOnboardingController extends Controller
{
    /**
     * Get user onboarding info
     *
     * @authenticated
     *
     * @responseFile status=200 scenario="success" responses/onboarding.user.info.get.success.json
     * @responseFile status=500 scenario="failed" responses/onboarding.user.info.get.failed.json
     **/
    public function getOnboardingStatus(){
        try{
            $user = Auth::user();

            //Transform user's preference output
            $resource = new Item($user, new UserOnboardingInfoTransformer);
            $manager = (new Manager)->setSerializer(new ArraySerializer());
            $data = $manager->createData($resource)->toArray();

            return response()->json($data, 200);
        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            return response()->json([
                'status' => false,
                'message' => 'Internal server problem'
            ], 500);
        }
    }

    /**
     * Update onboarding status
     *
     * @authenticated
     * @bodyParam action in:finish,skip required the user's action
     * @responseFile status=200 scenario="finish action" responses/onboarding.user.action.skip.success.json
     * @responseFile status=200 scenario="skip action" responses/onboarding.user.action.finish.success.json
     * @responseFile status=500 scenario="failed" responses/onboarding.user.action.failed.json
     **/
    public function updateOnboardingStatus(UserOnboardingStatusRequest $request){
        try{
            $action = $request->action;
            $user = Auth::user();
            $userOnboardingClass = new UserOnboarding($user);

            if(!method_exists(UserOnboarding::class, $request->action)){
                throw new Exception("$action method not available", 500);
            }

            if(!$userOnboardingClass->$action()){
                throw new Exception("Failed to complete $action action");
            }

            return response()->json([
                'status' => true,
                'onboarding' => true,
                'progress' => '5'
            ], 200);

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            return response()->json([
                'status' => false,
                'message' => 'Internal server problem'
            ], 500);
        }
    }

    /**
     * Update onboarding tournament visitation status
     *
     * @authenticated
     *
     * @response status=204 scenario="success" {
     * }
     * @responseFile status=500 scenario="failed" responses/onboarding.user.tournament.visitation.failed.json
     **/
    public function updateOnboardingTournamentStatus(){
        try{
            $user = Auth::user();

            //Update user's tournament visitation
            $userOnboarding = new UserOnboarding($user);
            if(!$userOnboarding->update(new UserTournamentOnboarding)){
                throw new Exception('Failed to update user tournament visitation for user with id '.$user->user_id);
            }

            return response()->json([], 204);

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            return response()->json([
                'status' => false,
                'message' => 'Internal server problem'
            ], 500);
        }
    }

}
