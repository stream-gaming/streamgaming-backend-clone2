<?php

namespace App\Http\Controllers\Benchmark;

use App\Http\Controllers\Api\Team\TeamController as Team;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        return (new Team)->getAll($request);
    }
}
