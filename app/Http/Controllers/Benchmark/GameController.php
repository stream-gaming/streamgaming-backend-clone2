<?php

namespace App\Http\Controllers\Benchmark;

use App\Http\Controllers\Api\GameController as Game;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(Request $request)
    {
        return (new Game)->getAll();
    }
}
