<?php

namespace App\Http\Controllers\Benchmark;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TournamenList as Tournament;

class TournamentController extends Controller
{
    public function index(Request $request)
    {
        return (new Tournament)->allTouranament($request);
    }
}
