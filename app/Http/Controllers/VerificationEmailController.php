<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateRequestEmail;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

/**
 * @group Authentication
 *
 * APIs for the Verification email process
 */
class VerificationEmailController extends AuthController
{
    use Authenticatable;
    //- Batasan Percobaan
    protected $maxAttempts = 1;

    //- Waktu Blokir Per Menit
    protected $decayMinutes = 2;

    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Handle Verification Email.
     * <aside class="notice">APIs Method for Handle Verification Email process | Endpoint untuk menangani verifikasi email</aside>
     * @urlParam expires required for time expired | waktu expired yang di dapat dari url. Example: 1601104389
     * @urlParam hash required hash | hash yang di dapat dari url. Example: ef78d7474ecc9502056fdee7dafc31ccb81598c3
     * @urlParam id required id user | id pengguna. Example: 5f6ee681696fc
     * @urlParam signature required siganture | hash tanda tangan. Example: a525b48008ca33bb7cb65075f38efbf8c1a5364a5da97d79eb08c14f3970d041
     * @response scenario="success verification | sukses verifikasi" status="200" {
            "status": true,
            "message": "Successful email verification. melly.tiara92@gmail.com",
        }
     * @response scenario="expired link | link kadaluarsa" status="400" {
            "status": false,
            "message": "Link is expired."
        }
     * @response scenario="invalid link | link invalid" status="400" {
            "status": false,
            "message": "Invalid/Expired url provided."
        }
     * @response scenario="email already verified | email sudah di verifikasi" status="400" {
            "status": false,
            "message": "Email already verified"
        }
     */
    public function verify(Request $request)
    {
        //- Cek Expired
        if (time() > $request->input('expires')) {
            return response()->json([
                "success" => false,
                "message" => Lang::get('email.expired')
            ], $this->HTTP_BAD);
        }

        if (!$request->hasValidSignature()) {
            return response()->json([
                "success" => false,
                "message" => Lang::get('email.invalid')
            ], $this->HTTP_BAD);
        }

        $user = User::findOrFail($request->input('id'));

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        } else {
            return response()->json([
                "success" => false,
                "message" => Lang::get('email.already')
            ], $this->HTTP_BAD);
        }

        return response()->json([
            "success" => true,
            "message" => Lang::get('email.success') . ' ' . $user->email
        ], $this->HTTP_OK);
    }

    /**
     * Request Verification Email.
     * <aside class="notice">APIs Method for user Request Verification email | Endpoint untuk user meminta verifikasi email</aside>
     * @urlParam email email required Email | Email pengguna yang ingin di verifikasi. Example: melly.tiara92@gmail.com
     * @urlParam mobile boolean for status if request coming from mobile app. if it's not dont put on request Example: true
     * @response scenario="success send link | berhasil kirim link" status="200" {
            "status": true,
            "message": "Email verification link sent on your email. melly.tiara92@gmail.com",
         }
     * @response scenario="email not registered | email tidak terdaftar" status="400" {
            "status": false,
            "message": "Email Not Registered."
        }
     * @response scenario="email already verified | email sudah di verifikasi" status="400" {
            "status": false,
            "message": "Email already verified"
        }
     * @response scenario="too many requests | terlalu banyak meminta" status="429" {
            "status": false,
            "time": 148,
            "waiting": true,
            "message": "Too many Requests attempts. Please try again in 148 seconds."
        }
     */
    public function resend(ValidateRequestEmail $request)
    {
        $request->type = 'verify';
        //- Batas Permintaan 1x Untuk 2 Menit
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendResponse($request);
        }

        $user = User::where('email', $request->input('email'))->first();

        //- cek apakah email terdaftar
        if (!$user) {
            return response()->json([
                "success" => false,
                "message" => Lang::get('auth.login.not')
            ], $this->HTTP_BAD);
        }

        // - cek apakah email sudah status di verif
        if ($user->hasVerifiedEmail()) {
            return response()->json([
                "success" => false,
                "message" => Lang::get('email.resend.already')
            ], $this->HTTP_BAD);
        }
        //- kirim verifikasi email
        if (isset($request->mobile)) {
            $user->sendEmailVerificationNotifications('mobile');
        } else {
            $user->sendEmailVerificationNotifications();
        }

        //- Tambah Jumlah Permintaan Email
        $this->incrementLoginAttempts($request);

        return response()->json([
            "success" => true,
            "message" => Lang::get('email.resend.sent') . " " . $user->email
        ], $this->HTTP_OK);
    }

    /**
     * Check if an email address has been verified.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmailVerification(ValidateRequestEmail $request)
    {
        $user_class = new User();
        $user = $user_class->where('email', $request->input('email'))->first();
        if (!$user->hasVerifiedEmail()) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('email.check.not')
            ]);
        }
        return response()->json([
            'status' => true,
            'message' => Lang::get('email.check.has')
        ]);
    }

    /**
     * Return the currently authenticated user.
     *
     * @return \App\Model\User
     */
    private function getUser($user)
    {
        $user = Auth::login($user);
        return $user;
    }
}
