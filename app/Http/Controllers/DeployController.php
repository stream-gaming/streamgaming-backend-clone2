<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

/**
 * @hideFromAPIDocumentation
 */
class DeployController extends Controller
{
	public function deploy(Request $request)
	{
		$secretToken = $request->header('X-Gitlab-Token');
		$localToken = config('app.deploy_secret');
		if ($secretToken == $localToken) {
			$root_path = base_path();
			$process = Process::fromShellCommandline('cd ' . $root_path . '; ./deploy.sh');
			$process->run(function ($type, $buffer) {
				echo $buffer;
			});
			if (!$process->isSuccessful()) {
				return response()->json(['message' => new ProcessFailedException($process)], 500);
			}

			return ['output' => $process->getOutput()];
		}
		return response()->json(['message' => 'hash not equal'], 400);
	}

}
