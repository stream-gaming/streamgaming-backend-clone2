<?php

namespace App\Http\Controllers;

use App\Helpers\AuthenticatesUsers;
use App\Http\Requests\ValidateChangePassword;
use App\Http\Requests\ValidateUserLogin;
use App\Http\Requests\ValidateUserRegistration;
use App\Model\AccessLogsUsersModel;
use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\User;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Throwable;

/**
 * @group Authentication
 *
 * APIs for the authentication process
 */
class AuthController extends Controller
{
    use AuthenticatesUsers;

    //- Max Attempts
    protected $maxAttempts = 3;

    //- Block Time Per Minute
    protected $decayMinutes = 3;

    public function __construct()
    {
        $this->middleware('jwt', ['only' => ['profile', 'logout']]);

        $this->middleware('save.register.response', ['only' => ['register']]);
    }

    /**
     * Determine if the user has too many failed login attempts.
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Get the authenticated User.
     * @param  string $token, $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token, $user)
    {
        return response()->json([
            'status'       => true,
            'access_token' => $token,
            'token_type'   => 'bearer',
            //- Abaikan Error, sebenarnya ga error
            'expires_in'   => time() + auth('api')->factory()->getTTL() * 60, // Waktu Expired
            'user'         => $user
        ], $this->HTTP_OK);
        header('Authorization: Bearer ' . $token);
    }

    /**
     * Login.
     * <aside class="notice">APIs Method for user login process</aside>
     * @bodyParam email email required Email User | Email untuk login. Example: melly.tiara92@gmail.com
     * @bodyParam password password required Password User | Password untuk login. Example: streamgaming
     * @responseFile status=200 scenario="success login" responses/auth.login.json
     * @response scenario="wrong password | password salah" status="400" {
            "status": false,
            "message": "Password Salah."
        }
     * @response scenario="email not registered | email tidak terdaftar" status="400" {
            "status": false,
            "message": "Email Not Registered."
        }
     * @response scenario="email not verify | email belum verifikasi" status="400" {
            "status": false,
            "message": "Please Verify Email."
        }
     * @response scenario="account inactive | akun tidak aktif" status="400" {
            "status": false,
            "message": "Account is Inactive."
        }
     * @response scenario="too many requests | terlalu banyak login" status="429" {
            "status": false,
            "time": 148,
            "waiting": true,
            "message": "Too many login attempts. Please try again in 148 seconds."
        }
     */
    public function login(ValidateUserLogin $request)
    {
        $request->type = 'login';
        // Simpan Credentials Untuk Dijadikan Token
        $credentials   = $request->only('email', 'password');
        // Tambah Log User Login
        $log_user      = new AccessLogsUsersModel();

        // Block Login Session
        $minutes    = 10;
        $attempts   = $this->limiter()->attempts($this->throttleKey($request));
        $seconds    = $this->limiter()->availableIn($this->throttleKey($request));
        $waktu      = (($seconds < 0) ? 0 : $seconds);

        //- Block Login selama 10 menit, jika Gagal Login 5x ( has cache dan attempts 2x)
        if (Cache::get('attempts') == $this->throttleKey($request)) {
            $this->decayMinutes = $minutes;
            if ($attempts == 2) {
                Cache::put('attempts', $this->throttleKey($request), $waktu);
                $this->maxAttempts  = 2;
            }
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            if (!Cache::has('attempts')) {
                // Jika User Sudah Salah Login 3x, Simpan Cache selama 3 jam
                Cache::put('attempts', $this->throttleKey($request), 10800);
            }
            $this->fireLockoutEvent($request);
            return $this->sendResponse($request);
        }

        // Logic Login
        try {
            // Cari User berdasarkan email
            $user = User::where('email', $request->input('email'))->first();
            if (!$user) {
                //- Count Failed Attempts
                $this->incrementLoginAttempts($request);
                //- Pesan Error Jika Email tidak ada
                return response()->json([
                    'status'  => false,
                    'message' => Lang::get('auth.login.not')
                ], $this->HTTP_BAD);
            } else if ($user->is_verified == 0) {
                //- Count Failed Attempts
                $this->incrementLoginAttempts($request);
                //- Jika Email Belum di verifikasi
                return response()->json([
                    'status'  => false,
                    'message' => Lang::get('auth.login.verify'),
                    'verification' => true,
                ], $this->HTTP_BAD);
            } else if ($user->is_active == 0) {
                //- Count Failed Attempts
                $this->incrementLoginAttempts($request);
                //- Jika Akun Belum Aktif
                return response()->json([
                    'status'  => false,
                    'message' => Lang::get('auth.login.inactive')
                ], $this->HTTP_BAD);
            }

            //- Verifikasi password, dan generate token
            if (Hash::check($request->input('password'), $user->password)) {
                //- Create New Token
                $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt($credentials);
                //- Add Log Users Login Berhasil Status = 1
                $log_user->addAccessLogsUsers($user->user_id, $request, 1);
                //- Clear Count Failed Login
                $this->clearLoginAttempts($request);

                return $this->createNewToken($token, $user);
            }
        } catch (Throwable $e) {
            //- Munculkan Pesan Error
            return response()->json([
                'status'  => false,
                'message' => Lang::get('auth.login.failed'),
                'error'   => $e->getMessage()
            ], $this->HTTP_BAD);
        }

        //- Add Log Users Login Gagal Status = 0
        $log_user->addAccessLogsUsers($user->user_id, $request, 0);
        //- Count Failed Attempts
        $this->incrementLoginAttempts($request);

        return response()->json([
            'status'  => false,
            'message' => Lang::get('auth.login.incorrect')
        ], $this->HTTP_BAD);
    }

    /**
     * Register Account.
     * <aside class="notice">APIs Method for Register New User process | Endpoint untuk pendaftaran user baru</aside>
     * @bodyParam fullname string required Full Name | Nama lengkap. Example: Melli Tiara
     * @bodyParam username string required Username for register. Example: mellydecyber
     * @bodyParam jenis_kelamin string required The value must be one of laki-laki or perempuan | Nilai 'laki-laki','perempuan'. Example: perempuan
     * @bodyParam email email Email required for register. Example: melly.tiara92@gmail.com
     * @bodyParam tgl_lahir string required The value must be a valid date in the format "Y-m-d" | Format Tanggal "Y-m-d". Example: 1999-12-27
     * @bodyParam password password required Password for register. Example: streamgaming
     * @bodyParam password_confirmation password required Password Confirmation for register. Example: streamgaming
     * @bodyParam mobile boolean for status if request coming from mobile app. if it's not dont put on request | untuk status bahwa request dari mobile app atau tidak Example: true
     * @response scenario="success register" status="200" {
            "status": true,
            "message": "Pendaftaran User Baru Berhasil.",
            "email": "melly.tiara92@gmail.com"
        }
     * @response scenario="validation form input" status="422" {
            "message": "The given data was invalid.",
            "errors": {
                "email": [
                    "Email sudah digunakan."
                ],
                "password": [
                    "Format Password tidak valid."
                ]
            }
        }
     * @response scenario="register failed" status="409" {
            "status": false,
            "message": "User Registration Failed!",
            "error": {
                "errorInfo": [
                    "23000",
                    1048,
                    "Column 'alamat' cannot be null"
                ],
                "xdebug_message": "{message error}"
            }
        }
     */
    public function register(ValidateUserRegistration $request)
    {

        try {

            $user = User::where('email', $request->input('email'))->first();
            if ($user) {
                //- Pesan Error Jika Email Sudah Terdaftar
                return response()->json([
                    'status'  => false,
                    'message' => Lang::get('auth.register.already')
                ], $this->HTTP_BAD);
            }

            //- Status User Sebagai Guest, atau belum di verif
            $data = [
                'is_verified' => 0,
                'status'      => 'guest'
            ];
            DB::beginTransaction();
            //- Create User id, agar dapat digunakan di method yang lain
            $request->user_id = uniqid();
            $user = (new User())->addUser($request, $data);
            //- Input Data Tambahan User
            (new UsersDetailModel)->addUsersDetail($request);
            (new BalanceUsersModel)->addBalanceUsers($request);
            (new AuthenticateModel)->addAuthenticate($request);
            (new UsersGameModel)->addUsersGame($request);
            (new UserPreferencesModel)->addPreferencesUsers($request);

            //- Send Email Verification
            if (isset($request->mobile)) {
                $user->sendEmailVerificationNotifications('mobile');
            } else {
                $user->sendEmailVerificationNotifications();
            }

            DB::commit();
            //return respons berhasil
            return response()->json([
                'status'  => true,
                'message' => Lang::get('auth.register.success'),
                'email'   => $request->input('email')
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            //return error message gagal
            return response()->json([
                'status'   => false,
                'message'  => Lang::get('auth.register.failed'),
                'error'    => [
                    'message' => $e->getMessage(),
                    'file'     => $e->getFile(),
                    'line'     => $e->getLine(),
                    'trace'    => $e->getTrace()
                ]
                ],$this->HTTP_CONFLIC);
        }
    }

    /**
     * Logout.
     * @authenticated
     * <aside class="notice">APIs Method for Logout process | Endpoint untuk proses logout</aside>
     * @bodyParam token string required for invalidate token | token.
     * @response scenario="success logout" status="200" {
            "status": true,
            "message": "User logged out successfully."
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Sorry, the user cannot be logged out!"
        }
     */

    public function logout(Request $request)
    {
        $token = $request->header("Authorization");

        try {
            $request->headers->remove('Authorization');
            JWTAuth::invalidate($token);

            return response()->json([
                'status'   => true,
                'message'  => Lang::get('auth.logout.success')
            ], $this->HTTP_OK);
        } catch (JWTException $e) {
            return response()->json([
                'status'   => false,
                'message'  => Lang::get('auth.logout.failed'),
                'error'    => $e
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Change Password.
     * <aside class="notice">APIs Method for user to change password process | endpoint untuk user mengganti password</aside>
     * @bodyParam current_password password required The Current Password User | Password lama user. Example: streamgaming
     * @bodyParam new_password password required The new Password User | password baru. Example: streamuniverse
     * @bodyParam new_password_confirmation password required The COnfirmation new Password User | konfirmasi password baru. Example: streamuniverse
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Successfully changed password."
        }
     * @response scenario="incorrect current password" status="400" {
            "status": true,
            "message": "Current password is incorrect!!."
        }
     * @response scenario="does not match new password" status="422" {
            "message": "The given data was invalid.",
            "errors": {
                "new_password": [
                    "The New Password confirmation does not match."
                ]
            }
        }
     * @response scenario="failed" status="400" {
            "status": true,
            "message": "Failed changed password.",
            "error": {}
        }
     */
    public function changePassword(ValidateChangePassword $request)
    {
        try {
            $user =  Auth::user();
            if (!(Hash::check($request->input('current_password'), $user->password))) {
                return response()->json([
                    'status'   => true,
                    'message'  => Lang::get('auth.change-password.current')
                ], $this->HTTP_BAD);
            } else if ((Hash::check($request->input('new_password'), $user->password)) == true) {
                return response()->json([
                    'status'   => true,
                    'message'  => Lang::get('auth.change-password.similar')
                ], $this->HTTP_BAD);
            } else {
                User::find($user->user_id)->update(['password' => Hash::make($request->input('new_password'))]);
                return response()->json([
                    'status'   => true,
                    'message'  => Lang::get('auth.change-password.success')
                ], $this->HTTP_OK);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status'   => true,
                'message'  => Lang::get('auth.change-password.failed'),
                'error'    => $e
            ], $this->HTTP_BAD);
        }
    }
}
//deploy
