<?php

namespace App\Http\Controllers;

use App\Helpers\MyApps;
use App\Model\TournamenGroupModel;
use App\Model\TournamentModel;
use App\Transformer\BracketViiTransform;
use App\Transformer\TournamentBracketSoloViiTransform;
use App\Transformer\TournamentBracketViiTransform;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use App\Http\Middleware\RefreshTokenManual;
use League\Fractal\Resource\Collection;

/**
 * @group Tournament
 * APIs for Bracket's Tournament Details
 *
 */
class BracketVerIIController extends Controller
{
    public function __construct()
    {
        $this->middleware(RefreshTokenManual::class);
        $this->my_apps           = new MyApps;
        $this->bracket_transform = new BracketViiTransform;
        $this->fractal           = new Manager();
        $this->old_bracket       = new BracketController();
    }

    /**
     * Tournament Detail - Bracket v2
     *
     * This endpoint serves to get detailed information about bracket type tournaments.
     * URL parameters are required to retrieve data.
     * URL parameters can be seen from the existing tournament list data.
     * @urlParam url required Tournament url required Example:WEEKLY-TOURNAMENT-MOBILE-LEGEND-STREAM-GAMING-IV
     * @urlParam type required Tournament type required Example:overview
     * @response status=404 {
            "status": false,
            "message": "Tournament yang anda cari tidak ditemukan"
        }
     * @responseFile status=200 scenario="overview" responses/bracket.detail.v2.overview.json
     * @responseFile status=200 scenario="rules" responses/bracket.detail.v2.rules.json
     * @responseFile status=200 scenario="prizes V1" responses/bracket.detail.v2.prizes.json
     * @responseFile status=200 scenario="participant" responses/bracket.detail.v2.participant.json
     * @responseFile status=200 scenario="schedule" responses/bracket.detail.v2.schedule.json
     * @responseFile status=200 scenario="bracket" responses/bracket.detail.v2.bracket.json
     *
     */
    public function getDetails($url = null, $type = 'overview')
    {
        try {

            $bracket = TournamentModel::where('url', $url)->with('game')
                ->with(['prize' => function ($q) {
                    $q->orderBy('orders');
                }])
                ->with(['prizeTicket' => function ($q) {
                    $q->orderBy('urutan');
                }])
                ->firstOr(function () {
                    throw new Exception(Lang::get('message.bracket.details.not'));
                });

            $slotsFilled = (new TournamenGroupModel())->countFilledSlots($bracket->id_group);

            $url = [
                'overview'      => $this->my_apps->getPathRoute('bracketv2', [$url, 'overview']),
                'rules'         => $this->my_apps->getPathRoute('bracketv2', [$url, 'rules']),
                'participant'   => $this->my_apps->getPathRoute('bracketv2', [$url, 'participant']),
                'prizes'        => $this->my_apps->getPathRoute('bracketv2', [$url, 'prizes']),
                'bracket_schedule'      => $this->my_apps->getPathRoute('bracketv2', [$url, 'schedule']),
                'bracket'       => $this->my_apps->getPathRoute('bracketv2', [$url, 'bracket'])
            ];

            $data['head_info'] = $this->bracket_transform->headInfo($bracket, $bracket->total_team, $slotsFilled);

            $data['side_info'] = $this->bracket_transform->sideInfo($bracket);

            $data['ticket_info'] = $this->bracket_transform->ticketInfo($bracket);

            switch ($type) {
                case 'overview':
                    $data['overview'] = $this->bracket_transform->overview($bracket, $bracket->total_team, $slotsFilled);
                    break;
                case 'rules':
                    $data["rules"]    = $this->bracket_transform->rules($bracket);
                    break;
                case 'participant':
                    $AllTeam = $bracket->AllTeam->makeHidden(['id_tournament_group', 'id_group', 'date', 'matchs', 'win']);
                    if ($bracket->game_option == 1) {
                        $resource = new Collection($AllTeam, (new TournamentBracketSoloViiTransform));
                        $data['participant'] = $this->fractal->createData($resource)->toArray();
                    } else {
                        $resource = new Collection($AllTeam, (new TournamentBracketViiTransform));
                        $data['participant'] = $this->fractal->createData($resource)->toArray();
                    }
                    break;
                case 'prizes':

                    if ($bracket->id_winner_sequence != null || $bracket->id_winner_sequence != "") {
                        $total_prizePool = count($bracket->prize);
                    } else {
                        $total_prizePool = 3;
                    }

                    $data['prizes'] = ($total_prizePool >= count($bracket->prizeTicket) ? $this->old_bracket->PrizePoll($bracket) : $this->old_bracket->PrizeTicket($bracket));

                    break;
                case 'schedule':
                    if ($bracket->status == "starting" || $bracket->status == "complete") :
                        $data['schedule'] = $this->old_bracket->bracket($bracket->id_tournament);
                    else :
                        throw new Exception(Lang::get('message.bracket.details.schedule.not'));
                    endif;
                    break;
                case 'bracket':
                    if ($bracket->status == "starting" || $bracket->status == "complete") :
                        $data['bracket'] = $this->old_bracket->bracket($bracket->id_tournament);
                    else :
                        throw new Exception(Lang::get('message.bracket.details.bracket.not'));
                    endif;
                    break;
                default:
                    throw new Exception(Lang::get('message.bracket.details.not'));
                    break;
            }

            return response()->json([
                'status'         => true,
                'id_tournament'  => $bracket->id_tournament,
                'active_menu'    => $type,
                'menu'           => $url,
                'data'           => $data,
            ]);
        } catch (\Throwable $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ], $this->HTTP_NOT_FOUND);
        }
    }
}
