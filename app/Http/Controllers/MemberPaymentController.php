<?php

namespace App\Http\Controllers;

use App\Helpers\Validation\CheckAvailableGroup;
use App\Helpers\Validation\checkBalanceTicketTeam;
use App\Helpers\Validation\CheckTournamentSystem;
use App\Helpers\Validation\FreeRegist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\Helpers\Validation\hasJoinedTournament;
use App\Helpers\Validation\isSlotFull;
use App\Helpers\Validation\MemberPayment;
use App\Helpers\Validation\RegistMemberByCashOrPoint;
use App\Helpers\Validation\RegistTeamByTicket;
use App\Http\Requests\ValidateMemberPayment;
use App\Model\LeaderboardModel;
use App\Model\PlayerPayment;
use App\Model\TournamentModel;
use Illuminate\Support\Facades\Lang;


/**
 * @group Account
 * @authenticated
 * APIs for the Account Information
 */
class MemberPaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('checkpin');
    }

    /**
     * Member Payment Tournament.
     * <aside class="notice">APIs Method for Member Payment for tournament process | Method untuk membayar turnamen yang belum dibayar</aside>
     * @bodyParam id_player_payment primary_key required of player payment. | (id player payment yang akan dibayar) Example: 5f8fda4c57a79
     * @bodyParam tournament_type string required of type of tournaments to be paid | (tipe turnamen yang akan dibayar) contoh ['Bracket', 'Leaderboard']. Example: Bracket
     * @bodyParam type_payment string required of type payment The value must be one of cash or point | (tipe pembayaran menggunakan cash atau point). Example: cash
     * @response scenario="success payment" status="200" {
            "status": true,
            "message": "Pembayaran turnamen berhasil"
        }
     * @response scenario="already pay | sudah dibayar" status="200" {
            "status": true,
            "message": "Sudah Melakukan Pembayaran!"
        }
     * @response scenario="slot full | slot turnamen penuh" status="200" {
            "status": true,
            "message": "Gagal melakukan pendaftaran tournament dikarenakan slot sudah penuh!"
        }
     * @response scenario="cash not enough | cash tidak cukup" status="200" {
            "status": true,
            "message": "Uang tidak cukup!"
        }
     * @response scenario="point not enough | point tidak cukup" status="200" {
            "status": true,
            "message": "Point tidak cukup!"
        }
     * @response scenario="error" status="400" {
            "status": false,
            "message": "Pembayaran turnamen gagal!"
        }
     */
    public function tournament(ValidateMemberPayment $request)
    {
        try {
            $user           = Auth::user();
            $validate       = new MemberPayment();
            $player_payment = PlayerPayment::where('id_player_payment', $request->input('id_player_payment'))
                ->where('player_id', $user->user_id)->first();

            $tournament_type = (object)(new CheckTournamentSystem)->check($player_payment->id_tournament);
            $status          = (object) $validate->playerPayment($player_payment, $tournament_type->tournament_system, $request->input('type_payment'));

            return response()->json([
                'status'    => $status->status,
                'message'   => $status->message
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.payment.failed')
            ], $this->HTTP_BAD);
        }
    }
}
