<?php

namespace App\Http\Controllers;

use App\Helpers\ThrottlesAccess;
use App\Model\AboutContactUsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

/**
 * @group About Us
 *
 * The APIs for about us content
 */
class ContactUsController extends Controller
{
    use ThrottlesAccess;

    //- Max Attempts
    protected $maxAttempts;

    //- Block Time Per Minute
    protected $decayMinutes;

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $request->ip(),
            $this->maxAttempts,
            $this->decayMinutes
        );
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function incrementAttempts(Request $request)
    {
        $this->limiter()->hit(
            $request->ip(),
            $this->decayMinutes() * 60
        );
    }


    /**
     * Contact Us - Send
     *
     * @bodyParam name string required The name of user
     * @bodyParam email string required The email of user
     * @bodyParam message string required The message of user
     *
     * @response 201 {
     *      "status": true,
     *      "message": "Pesan anda berhasil dikirim"
     *  }
     *
     * @response 429 {
     *       "message": "Terjadi Kesalahan, coba beberapa saat lagi"
     *   }
     */
    public function send(Request $request)
    {
        $request->validate([
            'name'    => 'required|string',
            'email'   => 'required|email',
            'message' => 'required|string'
        ]);

        $this->decayMinutes = 60;

        $this->maxAttempts  = 3;

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return response()->json([
                "message" => Lang::get('message.block.error')
            ], 429);
        }


        DB::transaction(function () use($request){
            $message = AboutContactUsModel::create([
                'name'    => $request->name,
                'email'   => $request->email,
                'message' => $request->message,
            ]);
        });

        $this->incrementAttempts($request);

        return response()->json([
            "status"  => true,
            "message" => Lang::get('contactus.success')
        ], 201);

    }
}
