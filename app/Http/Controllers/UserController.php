<?php

namespace App\Http\Controllers;

use App\Helpers\UserDetailMenu;
use App\Transformer\AccountTransform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

/**
 * @group Public
 *
 * APIs for the Public Access
 */
class UserController extends Controller
{
    /**
     * Get Information User.
     * <aside class="notice">APIs Method for User Public information | untuk melihat detail data user secara publik</aside>
     * @urlParam username required username of user public, for detail information | username user yang akan dilihat. Example: mellydecyber
     * @responseFile status=200 scenario="success" responses/account.profile.public.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "User not found"
        }
     */
    public function overview($username, Request $request)
    {
        try {
            $user = (new AccountTransform)->overviewUser($username);
            //User detail menu
            $userDetailMenu = (new UserDetailMenu)->menu($username, $request->version);

            return response()->json([
                'status'   => true,
                'active_menu' => 'overview',
                'menu'     =>   $userDetailMenu,
                'data'     => $user
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.user.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }
}
