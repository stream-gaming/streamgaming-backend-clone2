<?php

namespace App\Http\Controllers;

use App\Model\TeamModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use InvalidArgumentException;

/**
 * @hideFromAPIDocumentation
 */
class HistoryTeamNameController extends Controller
{
    public function getHistory($teamId)
    {

        try {
            $history = TeamModel::with(['nameHistory' => function($q){
                $q->select('team_id','old_name','new_name','created_at');
                $q->orderBy('created_at', 'desc');
            }])->where('team_id',$teamId)->first(['team_id']);

            if($history == null){
                throw new InvalidArgumentException(Lang::get('message.team.not'),$this->HTTP_NOT_FOUND);
            }

            return response()->json([
                'status' => true,
                'data'   => $history,
                'message' => "Menampilkan data history team name"
            ], $this->HTTP_OK);

        } catch (\Throwable $th) {
            $code = $this->HTTP_BAD;

            if($th instanceof InvalidArgumentException){
                $code = $th->getCode();
            }

            return response()->json([
                'status' => false,
                'data'   => [],
                'message' => $th->getMessage()
            ], $code);
        }
    }
}
//up to development
