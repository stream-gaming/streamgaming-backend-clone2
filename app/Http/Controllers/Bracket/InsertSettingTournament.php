<?php

namespace App\Http\Controllers\Bracket;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Validation\InsertSettingSolo;
use Exception;

class InsertSettingTournament extends Controller
{
    public function SettingTournament(Request $request){
        $data = (new InsertSettingSolo)->InsertSettingSolo($request->id_group);
        return $data;
    }
}
