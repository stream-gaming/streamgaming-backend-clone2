<?php

namespace App\Http\Controllers\Bracket;

use App\Http\Controllers\Controller;
use App\Model\ListMvpModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use InvalidArgumentException;

/**
 * @group MVP
 */
class BracketGetMvpController extends Controller
{
    /**
     * Get Top 5 MVP
     *
     * Api for get top 5 mvp data and kda
     *
     * @urlParam tournamentId required the id of the bracket tournament
     * @response status=200 {
    "status": true,
    "data": [
        {
            "sequence": 1,
            "player_name": "ystrosin",
            "team_name": "lucy.lindgren",
            "mode": "strike_mvp",
            "strike_mvp": 2,
            "kda_score": 76.768,
            "profile_url": "/overview/Vita Gottlieb"
        },
        {
            "sequence": 4,
            "player_name": "torrey.muller",
            "team_name": "harris.luisa",
            "mode": "strike_mvp",
            "strike_mvp": 1,
            "kda_score": 75,
            "profile_url": "/overview/Dr. Breanna Hansen"
        },
        {
            "sequence": 4,
            "player_name": "bobby07",
            "team_name": "cierra.hoppe",
            "mode": "kda_score",
            "strike_mvp": 0,
            "kda_score": 48.61982608,
            "profile_url": "/overview/Ashton Collins"
        },
        {
            "sequence": 5,
            "player_name": "rbernhard",
            "team_name": "gerald49",
            "mode": "kda_score",
            "strike_mvp": 0,
            "kda_score": 32.8153,
            "profile_url": "/overview/Justyn Nolan"
        },
        {
            "sequence": 5,
            "player_name": "sprohaska",
            "team_name": "vern.effertz",
            "mode": "kda_score",
            "strike_mvp": 0,
            "kda_score": 26,
            "profile_url": "/overview/Lempi Kemmer"
        }
    ],
    "message": "success"
}
     * @response status=404 {
    "status": false,
    "data": [],
    "message": "Turnamen tidak ditemukan"
}
     */
    public function mvpList($tournamentId)
    {
        try {
            $list = ListMvpModel::with('user')->where('tournament_id',$tournamentId)->orderBy('sequence','ASC')->get();

            if($list->isEmpty()){
                throw new InvalidArgumentException(Lang::get('message.tournament.not'),$this->HTTP_NOT_FOUND);
            }

            $list->transform(function($list){
                return [
                    'sequence'    => $list->sequence,
                    'player_name' => $list->name_player,
                    'team_name'   => $list->team_name,
                    'mode'        => $list->mvp_type,
                    'strike_mvp'  => $list->strike_mvp,
                    'kda_score'   => $list->kda_score,
                    'profile_url' => '/overview/'.$list->user->username
                ];
            })->toArray();

            return response()->json([
                'status' => true,
                'data' => $list,
                'message' => trans('success')
            ], $this->HTTP_OK);

        } catch (\Throwable $th) {
            $http_code = $this->HTTP_CONFLIC;
            if($th instanceof InvalidArgumentException){
                $http_code = $th->getCode();
            }

            return response()->json([
                'status' => false,
                'data'   => [],
                'message' => $th->getMessage()
            ], $http_code);
        }
    }
}
