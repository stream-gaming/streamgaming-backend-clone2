<?php

namespace App\Http\Controllers\Bracket;

use App\Http\Controllers\Controller;
use App\Model\BracketDataMatchs;
use App\Model\ListMvpModel;
use App\Model\ScoreMatchsModel;
use App\Model\TournamentModel;
use App\Model\TournamentMvpModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function Clue\StreamFilter\fun;

/**
 * @group MVP
 *
 * API for get mvp information
 */
class BracketMVPController extends Controller
{
    public function __construct()
    {
        $this->middleware('portal.auth')->only('moveToMvp');
    }
    /**
     * @hideFromAPIDocumentation
     *
     * Get Top 5 MVP Portal
     *
     * Api for get top 5 mvp data and kill death assist data
     *
     * @urlParam tournamentId required the id of the bracket tournament
     * @response status=200 {
    "status": true,
    "data": [
        {
            "player_id": "60a6463a2cd7d",
            "id_team": "60a615915335c",
            "total_match": 1,
            "kills": "1",
            "death": "9",
            "assist": "113",
            "kda": "105",
            "kda_per_total_match": "105.0000",
            "tournament_group": {
                "id_tournament_group": 17545,
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a615915335c",
                "nama_team": "MUTANT eSports",
                "logo_team": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/oVLle7puxfpcXhq1u1o70Zq0wGo4lc4KJnB4BJbE.jpg",
                "nama_kota": "KOTA SURABAYA",
                "matchs": 1,
                "win": "",
                "date": ""
            },
            "tournament_group_player": {
                "id_player": "60a6463a2cd7d",
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a615915335c",
                "username_ingame": "Woosky",
                "id_ingame": "94324004",
                "player_status": "0"
            }
        },
        {
            "player_id": "60745c42c8297",
            "id_team": "60748bfb3b58a",
            "total_match": 4,
            "kills": "114",
            "death": "14",
            "assist": "21",
            "kda": "121",
            "kda_per_total_match": "30.2500",
            "tournament_group": {
                "id_tournament_group": 17519,
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60748bfb3b58a",
                "nama_team": "Yth. Squad",
                "logo_team": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/rTjD8G5XZvv0uuOIpZbj59IazrLnViPJbg3c3uyW.png",
                "nama_kota": "KAB. BADUNG",
                "matchs": 4,
                "win": "3",
                "date": ""
            },
            "tournament_group_player": {
                "id_player": "60745c42c8297",
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60748bfb3b58a",
                "username_ingame": "potatoe",
                "id_ingame": "57140554",
                "player_status": "1"
            }
        },
        {
            "player_id": "60a5e155cb47f",
            "id_team": "60a5e699715b1",
            "total_match": 7,
            "kills": "116",
            "death": "15",
            "assist": "58",
            "kda": "159",
            "kda_per_total_match": "22.7143",
            "tournament_group": {
                "id_tournament_group": 17539,
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a5e699715b1",
                "nama_team": "KING Exalibur",
                "logo_team": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/JD9h7KKfruDGLsRZAgBQEIuxBDAanZYEX201jGLq.jpg",
                "nama_kota": "KOTA ADM. JAKARTA UTARA",
                "matchs": 7,
                "win": "5",
                "date": ""
            },
            "tournament_group_player": {
                "id_player": "60a5e155cb47f",
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a5e699715b1",
                "username_ingame": "V I N N",
                "id_ingame": "64845196",
                "player_status": "1"
            }
        },
        {
            "player_id": "60a61861f36b1",
            "id_team": "60a615915335c",
            "total_match": 1,
            "kills": "15",
            "death": "3",
            "assist": "3",
            "kda": "15",
            "kda_per_total_match": "15.0000",
            "tournament_group": {
                "id_tournament_group": 17545,
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a615915335c",
                "nama_team": "MUTANT eSports",
                "logo_team": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/oVLle7puxfpcXhq1u1o70Zq0wGo4lc4KJnB4BJbE.jpg",
                "nama_kota": "KOTA SURABAYA",
                "matchs": 1,
                "win": "",
                "date": ""
            },
            "tournament_group_player": {
                "id_player": "60a61861f36b1",
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "60a615915335c",
                "username_ingame": "<ZHT> ~ R A P HH ~",
                "id_ingame": "361145101",
                "player_status": "0"
            }
        },
        {
            "player_id": "608699b67b9a9",
            "id_team": "6085949aadce0",
            "total_match": 7,
            "kills": "67",
            "death": "10",
            "assist": "36",
            "kda": "93",
            "kda_per_total_match": "13.2857",
            "tournament_group": {
                "id_tournament_group": 17906,
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "6085949aadce0",
                "nama_team": "Aegeus Priam",
                "logo_team": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/website/public/content/team/4nL2X8eHDBneoyje6MB3Rm78SPONLD67IArCajO2.jpg",
                "nama_kota": "KAB. BANGKA BARAT",
                "matchs": 6,
                "win": "6",
                "date": ""
            },
            "tournament_group_player": {
                "id_player": "608699b67b9a9",
                "id_group": "91b8f63f70af3eb48a6529a03108b271e52f1039",
                "id_team": "6085949aadce0",
                "username_ingame": "Josee Koeriakkk.",
                "id_ingame": "54053568",
                "player_status": "0"
            }
        }
    ]
}
     */
    public function getFiveMVP($tournamentId)
    {

        $matchData = $this->getAllMvp($tournamentId, 5);

        return response()->json([
            'status' => true,
            'data' => $matchData
        ], 200);
    }


    public function getAllMvp($tournamentId, $limit = null)
    {
        $idDataMatch = BracketDataMatchs::where('id_tournament', $tournamentId)->pluck('id_data_matchs');
        $idTournamentGroup = TournamentModel::where('id_tournament', $tournamentId)->first(['id_group']);

        $idTournamentGroup = $idTournamentGroup->id_group;

        $scorematchs = ScoreMatchsModel::selectRaw('player_id,
                                                    id_team,
                                                    count(player_id) as total_match,
                                                    SUM(kills) as kills,
                                                    SUM(death) as death,
                                                    SUM(assist) as assist,
                                                    (SUM(kills) - SUM(death) + SUM(assist)) as kda,
                                                    ((SUM(kills) - SUM(death) + SUM(assist))/count(player_id)) as kda_per_total_match')
            ->whereIn('id_data_matchs', $idDataMatch)
            ->with(['tournamentGroup' => function ($q) use ($idTournamentGroup) {
                $q->where('id_group', $idTournamentGroup);
            }, 'tournamentGroupPlayer' => function ($q) use ($idTournamentGroup) {
                $q->where('id_group', $idTournamentGroup);
            }])
            ->groupBy('player_id')
            ->orderBy('kda_per_total_match', 'DESC');
        if ($limit != null) {
            $scorematchs = $scorematchs->limit($limit)
                ->get();
        } else {
            $scorematchs = $scorematchs->first();
        }

        return $scorematchs;
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function moveToMvp($tournamentId)
    {
        $getFisrtMVP = $this->getAllMvp($tournamentId);
        $this->listMvp($tournamentId);

        $move = TournamentMvpModel::firstOrCreate(['id_tournament' => $tournamentId]);

        if($move->id_player == $getFisrtMVP->player_id && $move->nama_player == $getFisrtMVP->tournamentGroupPlayer->username_ingame
            && $move->jumlah == NULL &&  $move->kda_rate = $getFisrtMVP->kda_per_total_match) {
                return response()->json([
                    'status'  => false,
                    'message' => 'Gagal memilih MVP',
                    'data'    => $move
                ], $this->HTTP_CONFLIC);
            }

        $move->id_player = $getFisrtMVP->player_id;
        $move->nama_player = $getFisrtMVP->tournamentGroupPlayer->username_ingame;
        $move->jumlah = NULL;
        $move->kda_rate = $getFisrtMVP->kda_per_total_match;

        $move->save();

        return response()->json([
            'status'  => true,
            'message' => 'Berhasil memilih MVP',
            'data'    => $move
        ], $this->HTTP_OK);
    }

    public function listMvp($tournamentId){
        $matchData      = $this->getAllMvp($tournamentId, 5);
        ListMvpModel::where('tournament_id', $tournamentId)->delete();

        $angka = 1;
        foreach ($matchData as $rows) {
            $insertList = new ListMvpModel();
            $insertList->tournament_id  =   $tournamentId;
            $insertList->name_player    =   $rows->tournamentGroupPlayer->username_ingame;
            $insertList->user_id        =   $rows->player_id;
            $insertList->team_name      =   $rows->tournamentGroup->nama_team;
            $insertList->team_id        =   $rows->id_team;
            $insertList->kda_score      =   $rows->kda_per_total_match;
            $insertList->mvp_type       =   'kda_score';
            $insertList->sequence       =   $angka;
            $insertList->save();
          $angka++;
        }
    }
}
