<?php

namespace App\Http\Controllers\Admin;

use App\Model\Management;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidatePortalToken;


/**
 * @hideFromAPIDocumentation
 */
class AdminTokenController extends Controller
{
    public function create(ValidatePortalToken $request)
    {
        auth()->shouldUse('admins');
        $credentials = JWTAuth::fromUser(Management::findorFail($request->stream_id));

        return response()->json(['token' => $credentials]);
    }

    public function profile()
    {
        return response()->json(['data' => auth()->user()]);
    }
}
