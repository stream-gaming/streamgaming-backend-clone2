<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Redis\RedisHelper;
use Exception;
use Throwable;
use Illuminate\Http\Request;
use App\Model\IdentityGameModel;
use App\Model\HistoryBannedModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Helpers\Validation\CheckTeam;
use App\Http\Requests\ValidateBannedGame;
use App\Http\Controllers\Api\UserNotificationController;

class BannedController extends Controller
{
    public function ban_game(ValidateBannedGame $request)
    {
        try {
            DB::beginTransaction();
            $identity = IdentityGameModel::find($request->identity_id);

            if ($identity->is_banned == 1) {
                throw new Exception("Access Denied");
            }

            $request->banned_until = date("Y-m-d H:i:s", strtotime($request->banned_until));

            $identity->is_banned = 1;
            $identity->ban_until = $request->banned_until;
            $identity->reason_ban = $request->reason;
            $identity->save();

            $getTeam = (new CheckTeam())->getTeamPlayer($identity->game_id, $identity->users_id);

            $history = new HistoryBannedModel;
            $history->identity_id = $request->identity_id;
            $history->team_id = $getTeam != false ? $getTeam->team_id : null;
            $history->ban_until = $request->banned_until;
            $history->reason_ban = $request->reason;
            $history->admin_id = auth()->user()->stream_id;
            $history->save();

            new UserNotificationController('game_banned', $identity);

            RedisHelper::destroyData('my:game:user_id:' . $identity->users_id);
            if($getTeam){
                RedisHelper::destroyData('team:detail:team_id:' . $getTeam->team_id);
            }

            DB::commit();

            return response()->json([
                'status'    => true,
                'message'   => 'Berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status'    => false,
                'message'   => 'Something Error: ' . $e->getMessage(),
                'errors'    => $e
            ], 409);
        }
    }
}
