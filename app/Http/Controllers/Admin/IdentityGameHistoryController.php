<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\IdentityGameModel;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use InvalidArgumentException;

/**
 * @hideFromAPIDocumentation
 */
class IdentityGameHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('portal.auth');
    }

    public function get($userId)
    {
        try {
            $user = User::where('user_id',$userId)->first();

            if($user==null){
                throw new InvalidArgumentException(Lang::get('message.user.not'),$this->HTTP_NOT_FOUND);
            }

            $data = IdentityGameModel::with(['game' => function($game){
                $game->select('id_game_list','name');
            },'history' => function($history){
                $history->orderBy('created_at','desc');
            }])->where('users_id',$userId)->get(['users_id','game_id']);

            return response()->json([
                'status' => true,
                'data'   => $data,
                'message'=> "Sukses mengambil data riwayat perubahan akun game"
            ], $this->HTTP_OK);

        } catch (\Throwable $th) {
            $http_code = $this->HTTP_BAD;

            if($th instanceof InvalidArgumentException){
                $http_code = $th->getCode();
            }

            return response()->json([
                'status' => false,
                'data'   => [],
                'message'=> $th->getMessage()
            ], $http_code);
        }
    }
}
