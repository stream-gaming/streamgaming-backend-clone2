<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Redis\RedisHelper;
use Exception;
use Throwable;
use Illuminate\Http\Request;
use App\Model\IdentityGameModel;
use App\Model\HistoryBannedModel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\UserNotificationController;
use App\Http\Requests\ValidateBannedTeamSoloGame;
use App\Model\HistoryBannedTeamModel;
use App\Model\TeamModel;
use App\Model\User;

/**
 * @hideFromAPIDocumentation
 */
class BannedTeamController extends Controller
{
    public function banTeamWithIdentityGame(ValidateBannedTeamSoloGame $request)
    {
        try {
            DB::beginTransaction();
           
            $banned_until = (request()->banForever == 1) ? 'forever' : request()->banned_until;

            $team = TeamModel::find($request->team_id);

            if ($team->is_banned == 1) {
                throw new Exception("Access Denied");
            }

            $team->update([
                "is_banned"  => '1',
                "ban_until"  => $banned_until,
                "reason_ban" => $request->reason,
            ]);

            $history = HistoryBannedTeamModel::create([
                "team_id"       => $request->team_id,
                "ban_until"     => $banned_until,
                "reason_ban"    => $request->reason,
                "admin_id"      => auth()->user()->stream_id,
            ]);

            new UserNotificationController('team_banned', $team);

            //- Banned Solo
            $array_identity = $request->identity_id;

            if ($array_identity > 0) {
                foreach ($array_identity as $value) {
                    $identity = IdentityGameModel::find($value);

                    if ($identity->is_banned != 1) {

                        $identity->is_banned = 1;
                        $identity->ban_until = $banned_until;
                        $identity->reason_ban = $request->reason;
                        $identity->save();

                        $history = new HistoryBannedModel;
                        $history->identity_id = $identity->id;
                        $history->team_id = $request->team_id;
                        $history->ban_until = $banned_until;
                        $history->reason_ban = $request->reason;
                        $history->admin_id = auth()->user()->stream_id;
                        $history->save();

                        new UserNotificationController('game_banned', $identity);
                        RedisHelper::destroyData('my:game:user_id:'. $identity->users_id);
                    }
                }
            }

            RedisHelper::destroyData('team:detail:team_id:' . $team->team_id);
            RedisHelper::destroyDataWithArray('my:team:user_id', $team->allPlayer());

            DB::commit();

            return response()->json([
                'status'    => true,
                'message'   => 'Berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status'    => false,
                'message'   => 'Something Error: ' . $e->getMessage(),
                'errors'    => $e
            ], 409);
        }
    }

    public function getListMemberByTeamID(Request $request)
    {
        $team = TeamModel::find($request->team_id);

        if ($team == null) {
            return response()->json([
                'status' => false,
                'data'   => trans('message.team.not')
            ], 400);
        }

        $result = [
            'team_id'       => (string) $team->team_id,
            'game'          => $team->game->name,
            'description'   => $team->team_description,
            'is_banned'     => ($team->is_banned) ? true : false,
            'ban_until'     => $team->ban_until,
            'team_name'     => $team->team_name,
            'created_at'    => $team->created_at
        ];

        for ($index = 0; $index < count($team->allPlayer()); $index++) {
            $user = User::find($team->allPlayer()[$index]);

            $identity = IdentityGameModel::where('users_id', $user->user_id)
                ->where('game_id', $team->game_id)
                ->first();

            $result['member'][$index] = [
                'identity_id' => $identity->id,
                'users_id'  => $user->user_id,
                'username'  => $user->username,
                'fullname'  => $user->fullname,
                'username_game' => $identity->username_ingame,
                'id_ingame'     => $identity->id_ingame,
                'is_banned'     => ($identity->is_banned) ? true : false,
                'is_leader'     => ($team->leader_id == $user->user_id) ? 1 : 0,
            ];
        }

        return response()->json([
            'status' => true,
            'data'   => $result
        ]);
    }
}
