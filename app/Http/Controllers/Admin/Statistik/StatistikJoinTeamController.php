<?php

namespace App\Http\Controllers\Admin\Statistik;

use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;

/**
 * @hideFromAPIDocumentation
 */
class StatistikJoinTeamController extends Controller
{
    public function data()
    {
        $totalPlayerHasTeam = [];

        TeamPlayerModel::select('player_id', 'substitute_id')
            ->chunk(200, function ($users) use (&$totalPlayerHasTeam) {
                //this will count the total of rows
                foreach ($users as $data) {
                    $players = explode(',', $data->player_id);
                    $substitute = explode(',', $data->substitute_id);
                    foreach ($players as $player) {
                        if ($player != '') {
                            $totalPlayerHasTeam[] = $player;
                        }
                    }
                    if (count($substitute) > 0) {
                        foreach ($substitute as $s) {
                            if ($s != '') {
                                $totalPlayerHasTeam[] = $s;
                            }
                        }
                    }
                }
            });

        $total_semua = TeamPlayerModel::selectRaw("CONCAT((CHAR_LENGTH( player_id ) - CHAR_LENGTH(REPLACE ( player_id, ',', '' )) + 1) +
        IF ( LENGTH( substitute_id ) > 0, LENGTH( substitute_id ) - LENGTH( REPLACE ( substitute_id, ',', '' )) + 1, 0 ),' ','Pemain') AS name,
        count( team_id ) AS total")
            ->where("player_id", "!=", "")
            ->groupBy('name')
            ->orderBy("name")
            ->get();

        $total_leader = count(TeamModel::select('leader_id')
            ->groupBy("leader_id")
            ->get());

        return response()->json([
            'total_player_has_team' => count(array_unique($totalPlayerHasTeam)),
            'total_leader' => $total_leader,
            'total_semua' => $total_semua
        ]);
    }
}

