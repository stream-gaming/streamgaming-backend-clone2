<?php

namespace App\Http\Controllers\Admin\Users;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\User;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @group Public
 *
 * APIs for the Public Access
 */
class ListTeamUserController extends Controller
{
    /**
     * Get List Teams User.
     * @urlParam username required username of user public, for detail information | username user yang akan dilihat. Example: mellydecyber
     * @responseFile status=200 scenario="success" responses/account.profile.team.json
     *
     */
    public function getListTeamUsers($userName)
    {
        try {
            $userId = User::where('username',$userName)->first()->user_id;

            $team_player    = TeamPlayerModel::where('player_id', 'LIKE', '%' . $userId . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . $userId . '%')
                ->pluck('team_id')->toArray();

            $team             = TeamModel::whereIn('team_id', $team_player)->get();

            $resource = new Collection($team, function (TeamModel $team) use($userId){
                return [
                    'game'          => $team->game->name,
                    'team_name'     => $team->team_name,
                    'team_id'       => $team->team_id,
                    'description'   => $team->team_description,
                    'is_banned'     => ($team->is_banned) ? true : false,
                    'ban_until'     => $team->ban_until,
                    'is_leader'     => ($team->leader_id == $userId) ? true : false,
                    'logo'          => (new MyApps)->cdn($team->team_logo, 'encrypt', 'team'),
                    'game'          => [
                        'id_game' => $team->game_id,
                        'name'    => $team->game->name,
                    ],
                    'slot'          => [
                        'total'      => $team->game->player_on_team + $team->game->substitute_player,
                        'primary'    => $team->game->player_on_team,
                        'substitute' => $team->game->substitute_player,
                        'member'     => $team->countMember()
                    ],
                    'url'           => (new MyApps)->onlyEncrypt($team->team_id),
                    'created_at'    => $team->created_at
                ];
            });

            return response()->json([
                'status' => true,
                'data'   => current((new Manager())->createData($resource)->toArray())
            ], $this->HTTP_OK);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'data' => [],
                'error'  => [
                    'message'  => $th->getMessage(),
                    'file'     => $th->getFile(),
                    'line'     => $th->getLine(),
                    'trace'    => $th->getTrace()
                ]
            ], $this->HTTP_CONFLIC);
        }
    }
}
