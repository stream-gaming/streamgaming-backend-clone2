<?php

namespace App\Http\Controllers\Admin\Users;

use App\Helpers\MyApps;
use App\Http\Controllers\Controller;
use App\Model\HistoryBannedModel;
use App\Model\HistoryBannedTeamModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @hideFromAPIDocumentation
 */
class HistoryBannedController extends Controller
{

    public function getListUser($user_id)
    {
        try {
            $users = User::find($user_id);

            if ($users == null) {
                throw new Exception(trans("message.user.not"));
            }

            $history_banned = HistoryBannedModel::join('identity_ingame', function ($q) {
                $q->on('history_banned.identity_id', '=', 'identity_ingame.id');
                $q->join('game_list', function ($q) {
                    $q->on('game_list.id_game_list', '=', 'identity_ingame.game_id');
                });
            })
                ->where('identity_ingame.users_id', $users->user_id)
                ->with('admin')
                ->select(['history_banned.id', 'identity_id', 'game_list.name as game', 'identity_ingame.username_ingame', 'identity_ingame.id_ingame', 'history_banned.ban_until', 'history_banned.reason_ban', 'admin_id', 'history_banned.created_at'])
                ->orderBy('history_banned.created_at', 'DESC')
                ->get();

            $history_banned->transform(function ($data, $index) {
                return [
                    'id'            => $data->id,
                    'identity_id'   => $data->identity_id,
                    'game'          => $data->game,
                    'username_ingame' => $data->username_ingame,
                    'id_ingame'     => $data->id_ingame,
                    'ban_until'     => $data->ban_until != 'forever' ? date('Y-m-d', strtotime($data->ban_until)) : 'forever',
                    'reason_ban'    => $data->reason_ban,
                    'admin_id'      => $data->admin_id,
                    'created_at'    =>  date('Y-m-d', strtotime($data->created_at)),
                    'admin'         => $data->admin
                ];
            });

            return response()->json([
                'status' => true,
                'data'   => $history_banned
            ], $this->HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'data' => [],
                'error'  => [
                    'message'  => $th->getMessage(),
                    'file'     => $th->getFile(),
                    'line'     => $th->getLine(),
                    'trace'    => $th->getTrace()
                ]
            ], $this->HTTP_NOT_FOUND);
        }
    }

    public function getListTeam($team_id)
    {
        try {
            $team = TeamModel::find($team_id);

            if ($team == null) {
                throw new Exception(trans("message.team.not"));
            }

            $history_banned = HistoryBannedTeamModel::join('team', function ($q) {
                $q->on('history_banned_team.team_id', '=', 'team.team_id');
                $q->join('game_list', function ($q) {
                    $q->on('game_list.id_game_list', '=', 'team.game_id');
                });
            })
                ->where('team.team_id', $team->team_id)
                ->with('admin')
                ->select(['history_banned_team.id', 'team.team_id', 'game_list.name as game', 'team.team_name', 'history_banned_team.ban_until', 'history_banned_team.reason_ban', 'admin_id', 'history_banned_team.created_at'])
                ->orderBy('history_banned_team.created_at', 'DESC')
                ->get();


            $history_banned->transform(function ($data, $index) {
                return [
                    'id'            => $data->id,
                    'team_id'       => $data->team_id,
                    'game'          => $data->game,
                    'team_name'     => $data->team_name,
                    'ban_until'     => $data->ban_until != 'forever' ? date('Y-m-d', strtotime($data->ban_until)) : 'forever',
                    'reason_ban'    => $data->reason_ban,
                    'admin_id'      => $data->admin_id,
                    'created_at'    => date('Y-m-d', strtotime($data->created_at)),
                    'admin'         => $data->admin
                ];
            });

            return response()->json([
                'status' => true,
                'data'   => $history_banned
            ], $this->HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'data' => [],
                'error'  => [
                    'message'  => $th->getMessage(),
                    'file'     => $th->getFile(),
                    'line'     => $th->getLine(),
                    'trace'    => $th->getTrace()
                ]
            ], $this->HTTP_NOT_FOUND);
        }
    }
}
