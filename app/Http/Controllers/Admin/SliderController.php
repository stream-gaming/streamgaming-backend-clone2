<?php

namespace App\Http\Controllers\Admin;

use App\Model\SliderModel;
use League\Fractal\Manager;
use App\Services\UploadService;
use App\Helpers\Redis\RedisHelper;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Transformer\SliderTransform;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\ValidateBanner;
use League\Fractal\Resource\Collection;
use App\Http\Requests\ValidateEditBanner;

/**
* @group Admin - Slider
*
* APIs for managing team.
*/
class SliderController extends Controller
{

    protected $cache_key = 'slider:list';
    protected $cache_key_detail = 'slider:show:';
    protected $cache_time = 604800 /** cached up to a week */;

    /**
     * All Slider
     * @response {
            "data": [
                {
                    "id": 4,
                    "title": "Slider 3",
                    "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/slider/ctjgcQ3BOhQPKLZVHc2WyRqwyh1SiPmQ2mEWqdji.jpeg",
                    "status": true,
                    "pop_up": true,
                    "url": "https://instagram.com",
                    "in_app": false,
                    "last_updated": "2020-11-18 15:38:37"
                },
                {
                    "id": 3,
                    "title": "Slider 2",
                    "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/slider/MVIH9MFCAPHrgvLJKGFvxmjnhu793pTtrInfpDTT.jpeg",
                    "status": false,
                    "pop_up": false,
                    "url": "/team",
                    "in_app": true,
                    "last_updated": "2020-11-18 15:38:32"
                }
            ]
        }
     */
    public function index()
    {
        if ($cache = RedisHelper::getData($this->cache_key)) {
            return $cache;
        }

        $slider = SliderModel::latest()->get();
        $response = new Collection($slider, new SliderTransform);

        $data = (new Manager)->createData($response)->toArray();

        RedisHelper::setData($this->cache_key, $data, $this->cache_time );

        return $data;
    }

    /**
     * Add Slider
     *
     * @bodyParam title string required The title of slider. Example: Promosi Stream Cash
     * @bodyParam image file required The image of slider. The extension must be one of JPG, JPEG, PNG. Max size is 2MB
     * @bodyParam status integer required The status of slider. Status 0=inactive, 1=active. Example: 1
     *
     * @response 200 {
            "status": true,
            "message": "berhasil",
            "data": [
                {
                    "id": 4,
                    "title": "Slider 3",
                    "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/slider/ctjgcQ3BOhQPKLZVHc2WyRqwyh1SiPmQ2mEWqdji.jpeg",
                    "status": "1",
                    "last_updated": "2020-11-18 15:38:37"
                }
            ]
        }
     *
     * @response 409 {
            "status": false,
            "message": "gagal",
            "error": "line 1 error"
        }
     */
    public function store(ValidateBanner $request)
    {
        //1280x576
        $file = $request->file('image');

        $image = (new UploadService)->upload_v2('public/content/slider', $file, 1280, 576);

        //check error when upload
        if (is_object($image)) {
            return $image;
        }

        try {
            $slider = new SliderModel;
            DB::transaction(function () use ($image, $request, $slider) {
                $slider->title = $request->title;
                $slider->image = $image;
                $slider->status = $request->status;
                $slider->url = $request->url;
                $slider->in_app = $request->in_app;
                $slider->save();
            });

            RedisHelper::destroyData($this->cache_key);

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.slider.add.success'),
                'data'      => $slider
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.slider.add.failed'),
                'error' => $e->getMessage(),
                'full_error' => $e
            ], 409);
        }
    }

    /**
    * Detail Slider
    * @urlParam slider required The ID of the slider. Example: 4
    * @response {
        "data": [
            {
                "id": 4,
                "title": "Slider 3",
                "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/slider/ctjgcQ3BOhQPKLZVHc2WyRqwyh1SiPmQ2mEWqdji.jpeg",
                "status": "1",
                "last_updated": "2020-11-18 15:38:37"
            }
            ]
        }
    */
    public function show(SliderModel $slider)
    {
        if ($cache = RedisHelper::getData($this->cache_key_detail. $slider->id)) {
            return $cache;
        }

        $response = new Collection([$slider], new SliderTransform);

        $data = (new Manager)->createData($response)->toArray();

        RedisHelper::setData($this->cache_key_detail. $slider->id, $data, $this->cache_time);

        return $data;
    }

    /**
     * Update Slider
     *
     * @urlParam slider required The ID of slider. Example: 1
     * @bodyParam title string required The title of slider. Example: Promosi Stream Cash
     * @bodyParam image file required The image of slider. The extension must be one of JPG, JPEG, PNG. Max size is 2MB
     * @bodyParam status integer required The status of slider. Status 0=inactive, 1=active. Example: 1
     *
     * @response status=200 {
            "status": true,
            "message": "berhasil",
            "data": [
                {
                    "id": 4,
                    "title": "Slider 3",
                    "image": "https://stream-gaming.s3.ap-southeast-1.amazonaws.com/alpha-dev/public/content/slider/ctjgcQ3BOhQPKLZVHc2WyRqwyh1SiPmQ2mEWqdji.jpeg",
                    "status": "1",
                    "last_updated": "2020-11-18 15:38:37"
                }
            ]
        }
     *
     * @response 409 {
            "status": false,
            "message": "gagal",
            "error": "line 1 error"
        }
     */
    public function update(ValidateEditBanner $request, SliderModel $slider)
    {
        if (!is_null($request->file('image'))) {
            $file = $request->file('image');
            $uploadService = new UploadService();
            $image = $uploadService->upload_v2('public/content/slider', $file, 1280, 576);

            //delete old image for mobile
            $uploadService->delete($slider->image);
            //delete old image for web
            $uploadService->delete($slider->image.'.webp');

            //check error when upload
            if (is_object($image)) {
                return $image;
            }
            $slider->image = $image;
        }

        try {
            DB::transaction(function () use ($request, $slider) {
                $slider->title = $request->title;
                $slider->status = $request->status;
                $slider->url = $request->url;
                $slider->in_app = $request->in_app;
                $slider->save();
            });

            RedisHelper::destroyData($this->cache_key);
            RedisHelper::destroyData($this->cache_key_detail. $slider->id);

            return response()->json([
                'status' => true,
                'message' => Lang::get('message.slider.update.success'),
                'data' => $slider
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.slider.update.failed'),
                'error' => $e->getMessage(),
                'full_error' => $e
            ], 409);
        }
    }

    /**
     * Delete Slider
     *
     * @urlParam slider required The ID of slider. Example:1
     *
     * @response status=200 {
            "status": true,
            "message": "berhasil"
        }
     *
     * @response 409 {
            "status": false,
            "message": "gagal",
            "error": "line 1 error"
        }
     */
    public function destroy(SliderModel $slider)
    {
        try {
            $uploadService = new UploadService();

            //delete old image for web
            $delete=$uploadService->delete($slider->image.'.webp');
            //delete old image for mobile
            $delete=$uploadService->delete($slider->image);


            if (is_object($delete)) {
                return $delete;
            }

            RedisHelper::destroyData($this->cache_key);
            RedisHelper::destroyData($this->cache_key_detail. $slider->id);

            DB::transaction(function () use ($slider) {
                $slider->delete();
            });

            return response()->json([
                'status'    => true,
                'message'   => 'success'
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'message' => 'fail',
                    'error' => $e->getMessage(),
                    'full_error' => $e
                ], 409);
            }
        }
    }
