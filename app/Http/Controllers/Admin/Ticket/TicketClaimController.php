<?php

namespace App\Http\Controllers\Admin\Ticket;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsertClaimTicketDataRequest;
use App\Model\AllTicketModel;
use App\Model\TicketClaimsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @hideFromAPIDocumentation
 */
class TicketClaimController extends Controller
{
    public function data()
    {
        $data = TicketClaimsModel::with('ticket')->get();

        return response()->json($data, 200);
    }

    public function allTicket()
    {
        $data = AllTicketModel::select('id','name')->whereDate('expired','>',Carbon::now()->toDateTimeString())->get();

        return response()->json($data);
    }

    public function insert(InsertClaimTicketDataRequest $request)
    {
        $data = new TicketClaimsModel();

        $data->ticket_id   = $request->ticket_id;
        $data->max_claim   = $request->max_claim;
        $data->period      = $request->period;
        $data->event_start = $request->event_start;
        $data->event_end   = $request->event_end;
        $data->is_active   = $request->is_active;

        $saved = $data->save();

        if($saved){
            $result = [
                'status'  => true,
                'message' => 'OK'
            ];
            $code = 201;
        }
        else{
            $result = [
                'status'  => false,
                'message' => 'NOT OK'
            ];
            $code = 500;
        }

        return response()->json($result, $code);
    }

    public function edit($id)
    {
        $data = TicketClaimsModel::with('ticket')->find($id);

        return response()->json($data, 200);
    }

    public function update($id, InsertClaimTicketDataRequest $request)
    {
        $data = TicketClaimsModel::find($id);

        $data->max_claim   = $request->max_claim;
        $data->period      = $request->period;
        $data->event_start = $request->event_start;
        $data->event_end   = $request->event_end;
        $data->is_active   = $request->is_active;

        $saved = $data->save();

        if($saved){
            $result = [
                'status'  => true,
                'message' => 'OK'
            ];
            $code = 200;
        }
        else{
            $result = [
                'status'  => false,
                'message' => 'NOT OK'
            ];
            $code = 500;
        }

        return response()->json($result, $code);
    }

    public function delete($id)
    {
        $data = TicketClaimsModel::where('id',$id)->delete();

        if($data){
            $result = [
                'status'  => true,
                'message' => 'OK'
            ];
            $code = 200;
        }
        else{
            $result = [
                'status'  => false,
                'message' => 'NOT OK'
            ];
            $code = 500;
        }

        return response()->json($result, $code);
    }
}
//keep
