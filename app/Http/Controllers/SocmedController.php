<?php
namespace App\Http\Controllers;

use App\Http\Requests\ValidateSocmed;
use App\Model\SocmedModel;
use App\Transformer\AccountTransform;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

/**
 * @group Social Media
 * @authenticated
 * APIs for the Social Media CRUD
 */
class SocmedController extends Controller
{
    /**
     * Get Social Media.
     * <aside class="notice">APIs Method get social media by user login | melihat semua data social media dari login user</aside>
     * @responseFile status=200 scenario="success" responses/socmed.index.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Failed get social media"
        }
     */
    public static function getSocmed($user_id = null)
    {
        try {
            $data = AccountTransform::socmed(Auth::user()->user_id);

            return response()->json([
                'status' => true,
                'data'   => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.socmed.failed')
            ], 400);
        }
    }

    /**
     * Get detail Social Media.
     * <aside class="notice">APIs Method get social media by id | melihat detail sosial media dari id sosial media</aside>
     * @urlParam id required id of Social Media | id sosial media. Example: 115015
     * @response scenario="success" status="200" {
            "status": true,
            "data": {
                "id": 115015,
                "sosmed": "facebook",
                "url": "https://facebook.com/mellydecyber",
                "created_at": "2020-10-02T08:35:15.000000Z",
                "updated_at": "2020-10-02T08:35:33.000000Z"
            }
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Failed get social media"
        }
     */
    public function read($id)
    {
        try {
            $socmed = SocmedModel::findOrFail($id);
            return response()->json([
                'status' => true,
                'data'   => $socmed,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.socmed.failed')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    /**
     * Add Social Media.
     * <aside class="notice">APIs Method for user to add social media | Tambah sosial media di data pengguna</aside>
     * @bodyParam sosmed string required Name of Social Media for add new | Nama sosial media. Example: Instagram
     * @bodyParam url string required url to social media user | url sosial media. Example: https://instagram.com/this_ismelly
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Success add social media",
            "data": {
            "sosmed": "Github",
            "url": "https://instagram.com/this_ismelly",
            "updated_at": "2020-09-05T03:59:49.000000Z",
            "created_at": "2020-09-05T03:59:49.000000Z",
            "id": 15
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed add social media."
        }
     */
    public function create(ValidateSocmed $request)
    {
        try {
            $socmed = SocmedModel::create([
                'sosmed'    => $request->input('sosmed'),
                'url'       => $request->input('url'),
                'users_id'  => (Auth::user())->user_id
            ]);

            return response()->json([
                'status' => true,
                'message' => Lang::get('message.socmed.add.success'),
                'data'   => $socmed,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.socmed.add.failed')
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Update Social Media.
     * <aside class="notice">APIs Method for user to edit social media | Endpoint untuk merubah sosial mediia</aside>
     * @bodyParam sosmed string required Name of Social Media for edit social media. Example: Instagram
     * @bodyParam url string required url to social media user | url sosial media pengguna. Example: https://instagram.com/this_ismelly7
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Success edit social media",
            "data": {
            "sosmed": "Github",
            "url": "https://instagram.com/this_ismelly",
            "updated_at": "2020-09-05T03:59:49.000000Z",
            "created_at": "2020-09-05T03:59:49.000000Z",
            "id": 15
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed edit social media."
        }
     */
    public function update(ValidateSocmed $request)
    {
        try {
            $socmed = SocmedModel::where('users_id', (Auth::user())->user_id)
                ->where('sosmed', $request->input('sosmed'))
                ->update([
                    'url' => $request->input('url'),
                ]);

            return response()->json([
                'status'   => true,
                'message'  => Lang::get('message.socmed.edit.success'),
                'data'   => $socmed,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.socmed.edit.failed')
            ]);
        }
    }


    /**
     * Delete Social Media.
     * <aside class="notice">APIs Method for user to Delete social media | Endpoint untuk menghapus sosial media</aside>
     * @bodyParam sosmed required sosmed of Social Media | nama sosial media. Example: facebook
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Failed delete social media."
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed delete social media."
        }
     */
    public function delete(HttpRequest $request)
    {
        try {
            $socmed = SocmedModel::where('users_id', (Auth::user())->user_id)
                ->where('sosmed', $request->input('sosmed'))
                ->delete();


            return response()->json([
                'status' => true,
                'message' => Lang::get('message.socmed.delete.success'),
                'data'    => $socmed
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.socmed.delete.failed'),
            ]);
        }
    }
}
