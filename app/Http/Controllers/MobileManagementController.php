<?php

namespace App\Http\Controllers;

use App\Http\Requests\MobileManagementStoreRequest;
use App\Http\Requests\MobileManagementUpdateRequest;
use App\Model\MobileVersion;
use App\Modules\MobileManagement\MobileAppId;
use Http\Discovery\Exception\NotFoundException;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule as ValidationRule;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @group Mobile Versions
 */
class MobileManagementController extends Controller
{
    /**
     * @hideFromAPIDocumentation
     */
    public function index($app='android')
    {
        try {

            $appId = (new MobileAppId)->get($app);

            if (!$appId) {
                throw new NotFoundException('Not Found', 404);
            }

        $data = MobileVersion::where('appId', $appId)->latest()->get();

        return response()->json($data);

        } catch (\Exception $e) {
            $code = 500;

            if($e instanceof NotFoundException){
                $code = $e->getCode();
            }
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], $code);
        }
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function edit(MobileVersion $mobileVersion)
    {
        return $mobileVersion;
    }

    /**
     * Get Latest Version
     *
     * this endpoint use to retrieve data about latest versions
     * @urlParam url optional android/ios, default to android Example:ios
     * @response scenario="success" status="200" {
            "appId": 1,
            "versionName": "1.2.3-ggs",
            "versionCode": 1,
            "recommendUpdate": false,
            "forceUpdate": true,
            "changeLogs": "test"
     * }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Not Found"
        }
     */
    public function show($app = 'android')
    {
        try {
            $appId = (new MobileAppId)->get($app);

            if (!$appId) {
                throw new NotFoundException('Not Found', 404);
            }

        $data = MobileVersion::where('appId', $appId)->latest()->first(['appId','versionName','versionCode','recommendUpdate','forceUpdate','changeLogs']);

        if ($data) {
            $data = [
                'appId'             => (int) $data->appId,
                'versionName'       => $data->versionName,
                'versionCode'       => (int) $data->versionCode,
                'recommendUpdate'   => (bool) $data->recommendUpdate,
                'forceUpdate'       => (bool) $data->forceUpdate,
                'changeLogs'        => $data->changeLogs,
            ];
        }

        return response()->json($data);

        } catch (\Exception $e) {
            $code = 500;

            if($e instanceof NotFoundException){
                $code = $e->getCode();
            }
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], $code);
        }
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function store(MobileManagementStoreRequest $req, $app = 'android')
    {
        $appId = (new MobileAppId)->get($app);

        try {
            DB::beginTransaction();

            if (!$appId) {
                throw new NotFoundException('Not Found', 404);
            }

            MobileVersion::create([
                'appId'             => $appId,
                'versionName'       => $req->name,
                'versionCode'       => $req->code,
                'recommendUpdate'   => (bool) $req->recommend,
                'forceUpdate'       => (bool) $req->force,
                'changeLogs'        => $req->changeLogs,
            ]);

            DB::commit();

            return response()->json([
                'status'  => true,
                'message' => 'berhasil disimpan'
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();

            $code = 500;

            if($e instanceof NotFoundException){
                $code = $e->getCode();
            }
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], $code);
        }
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function update(MobileManagementUpdateRequest $req, MobileVersion $mobileVersion)
    {
        try {
            DB::beginTransaction();

                if($mobileVersion->versionName != $req->name) {
                    $check = MobileVersion::where('appId',$mobileVersion->appId)->where('versionName', $req->name)->count();
                    if ($check) {
                        throw new UnprocessableEntityHttpException(Lang::get('validation.unique', ['attribute' => 'versionName']));
                    }
                }

                if($mobileVersion->versionCode != $req->code) {
                    $check = MobileVersion::where('appId',$mobileVersion->appId)->where('versionCode', $req->code)->count();
                    if ($check) {
                        throw new UnprocessableEntityHttpException(Lang::get('validation.unique', ['attribute' => 'versionCode']));
                    }
                }

                $mobileVersion->versionName       = $req->name;
                $mobileVersion->versionCode       = $req->code;
                $mobileVersion->recommendUpdate   = (bool) $req->recommend;
                $mobileVersion->forceUpdate       = (bool) $req->force;
                $mobileVersion->changeLogs        = $req->changeLogs;
                $mobileVersion->save();

            DB::commit();

            return response()->json([
                'status'  => true,
                'message' => 'berhasil disimpan'
            ], 200);

        } catch (\Exception $e) {
            DB::rollBack();

            $code = 500;

            if($e instanceof NotFoundException){
                $code = $e->getCode();
            }

            if($e instanceof UnprocessableEntityHttpException){
                $code = 422;
            }

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], $code);
        }
    }
}
