<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Model\User;
use App\Model\IdentityGameModel;
use App\Helpers\Redis\RedisHelper;
use App\Model\TeamModel;
use App\Model\TicketsModel;
use League\Fractal\Manager;
use App\Model\PlayerPayment;
use Illuminate\Http\Request;
use App\Model\UsersDetailModel;
use App\Services\UploadService;
use App\Model\UserPreferencesModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Transformer\AccountTransform;
use Illuminate\Support\Facades\Redis;
use App\Transformer\MyTicketTransform;
use App\Transformer\MyPaymentTransform;
use League\Fractal\Resource\Collection;
use App\Http\Requests\ValidateAccountSettings;
use App\Http\Requests\ValidateRequestUploadImage;
use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\TeamPlayerModel;
use App\Model\TicketTeamModel;
use App\Model\TicketUserModel;
use App\Traits\FirebaseTraits;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Account
 * @authenticated
 * APIs for the Account Information | Endpoint untuk informasi akun
 */
class AccountController extends Controller
{
    use FirebaseTraits;

    public function __construct()
    {
        $this->middleware(\App\Http\Middleware\RefreshToken::class);
        $this->account_transform = new AccountTransform();
    }

    /**
     * Get Profile.
     * <aside class="notice">APIs Method for User information login | informasi akun yang sedang login </aside>
     * @responseFile status=200 scenario="success" responses/account.profile.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "User not found"
        }
     */
    public function profile()
    {
        try {
            $user = (new AccountTransform)->transform();

            return response()->json(
                [
                    'status'   => true,
                    'data'      => $user,
                ]
            );
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.user.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    /**
     * Get My Ticket.
     * <aside class="notice">APIs Method for tickets owned by the user | informasi tiket yang dimiliki user</aside>
     * @urlParam type string required type tab what you want to see [user,team,expired] | tab tiket yang ingin anda liat. Example: user
     * @response scenario="user, team, expired" {
            "status": true,
            "data": [
                {
                    "ticket_id": 6,
                    "is_used": 0,
                    "name": "Tiket Final TikTok",
                    "expired": "2021-04-02 11:51:15"
                },
                {
                    "ticket_id": 5,
                    "is_used": 0,
                    "name": "Tiket Kualifikasi TikTok",
                    "expired": "2021-04-01 12:22:32"
                },
                {
                    "ticket_id": 5,
                    "is_used": 0,
                    "name": "Tiket Kualifikasi TikTok",
                    "expired": "2021-04-01 12:22:32"
                }
            ]
        }
       * @response scenario="user, team, expired v2" {
            "status": true,
            "data": [
                {
                    "ticket_id": 6,
                    "name": "Tiket Final TikTok",
                    "expired": "2021-04-02 11:51:15",
                    "image": null,
                    "ticket_type": "PREMIUM",
                    "game": "ALL GAME"
                },
                {
                    "ticket_id": 5,
                    "name": "Tiket Kualifikasi TikTok",
                    "expired": "2021-04-01 12:22:32",
                    "image": null,
                    "ticket_type": "PREMIUM",
                    "game": "ALL GAME"
                }
            ]
        }
     */
    public function getMyTicket(Request $request)
    {
        try {
            $user = Auth::user();

            $ticket_v2  = TicketsModel::leftJoin('all_ticket', function ($join) {
                $join->on('all_ticket.id', '=', 'tickets.ticket_id');
            })->where('is_used', 0);

            $team_id = TeamModel::where("leader_id", Auth::user()->user_id)
                ->get(['team_id']);

            switch ($request->type) {
                case 'user':
                    $ticket_v2 = $ticket_v2->where('user_id', $user->user_id)
                        ->where('expired', '>=', Carbon::now()->format('Y-m-d'));

                    $ticket_v1 = TicketUserModel::where('id_users', Auth::user()->user_id)
                        ->join('ticket', function ($join) {
                            $join->on('ticket.id_ticket', '=', 'ticket_users.id_ticket');
                        })
                        ->where('ticket.expired', '>=', Carbon::now()->format('Y-m-d'))
                        ->get();

                    break;
                case 'team':
                    $ticket_v2 = $ticket_v2->whereIn('team_id', $team_id)
                        ->where('expired', '>=', Carbon::now()->format('Y-m-d'));

                    $ticket_v1 = TicketTeamModel::whereIn('id_team', $team_id)
                        ->join('ticket', function ($join) {
                            $join->on('ticket.id_ticket', '=', 'ticket_team.id_ticket');
                        })
                        ->where('ticket.expired', '>=', Carbon::now()->format('Y-m-d'))
                        ->get();

                    break;
                case 'expired':
                    $endDate   = Carbon::now()->format('Y-m-d');
                    $startDate = Carbon::today()->subDays(3)->format('Y-m-d');

                    $ticket_v2 = $ticket_v2->where(function ($query) use ($user, $team_id) {
                        $query->where('user_id', $user->user_id);
                        $query->orWhereIn('team_id', $team_id);
                    })
                        ->where('expired', '<=', $endDate)
                        ->whereBetween('expired', [$startDate, $endDate]);

                    $ticket_v1_user = TicketUserModel::where('id_users', Auth::user()->user_id)
                        ->join('ticket', function ($join) {
                            $join->on('ticket.id_ticket', '=', 'ticket_users.id_ticket');
                        })
                        ->where('ticket.expired', '<=', $endDate)
                        ->whereBetween('ticket.expired', [$startDate, $endDate])
                        ->get();

                    $ticket_v1_team = TicketTeamModel::whereIn('id_team', $team_id)
                        ->join('ticket', function ($join) {
                            $join->on('ticket.id_ticket', '=', 'ticket_team.id_ticket');
                        })
                        ->where('expired', '<=', Carbon::now()->format('Y-m-d'))
                        ->whereBetween('expired', [$startDate, $endDate])
                        ->get();

                    $ticket_v1 = collect([$ticket_v1_user, $ticket_v1_team])->collapse()->all();

                    break;
            }

            $transform_ticket_v2 = current(fractal()
                ->collection($ticket_v2->get())
                ->transformWith(new MyTicketTransform("new"))
                ->toArray());

            $transform_ticket_v1 = collect(current(fractal()
                ->collection($ticket_v1)
                ->transformWith(new MyTicketTransform("old"))
                ->toArray()))->collapse();

            $data = collect([$transform_ticket_v2, $transform_ticket_v1])->collapse()->all();

            return response()->json([
                'status'   => true,
                'data'     => $data,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.ticket.not'),
                'error'   => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get Total My Ticket.
     * <aside class="notice">APIs Method for total tickets owned by the user | informasi total tiket yang dimiliki user</aside>
     * @response scenario="user, team, expired" {
            "status": true,
            "title": "Jumlah Total Tiket per Tab",
            "data": {
                "user": 0,
                "team": 0,
                "expired": 3
            }
        }
     */
    public function getTotalMyTicket(Request $request)
    {
        try {
            $user = Auth::user();

            $team_id = TeamModel::where("leader_id", Auth::user()->user_id)
                ->get(['team_id']);

            //Count Ticket User
            $ticket_v2_user = TicketsModel::join('all_ticket', function ($join) {
                $join->on('all_ticket.id', '=', 'tickets.ticket_id');
            })->where('is_used', 0)
                ->where('user_id', $user->user_id)
                ->where('expired', '>=', Carbon::now()->format('Y-m-d'))
                ->count();

            $ticket_v1_user = TicketUserModel::where('id_users', Auth::user()->user_id)
                ->join('ticket', function ($join) {
                    $join->on('ticket.id_ticket', '=', 'ticket_users.id_ticket');
                })
                ->where('ticket.expired', '>=', Carbon::now()->format('Y-m-d'))
                ->sum('ticket');

            $total['user'] = $ticket_v1_user + $ticket_v2_user;

            //count ticket team
            $ticket_v2_team = TicketsModel::join('all_ticket', function ($join) {
                $join->on('all_ticket.id', '=', 'tickets.ticket_id');
            })->where('is_used', 0)->whereIn('team_id', $team_id)
                ->where('expired', '>=', Carbon::now()->format('Y-m-d'))
                ->count();

            $ticket_v1_team = TicketTeamModel::whereIn('id_team', $team_id)
                ->join('ticket', function ($join) {
                    $join->on('ticket.id_ticket', '=', 'ticket_team.id_ticket');
                })
                ->where('ticket.expired', '>=', Carbon::now()->format('Y-m-d'))
                ->sum('ticket');

            $total['team'] = $ticket_v1_team + $ticket_v2_team;

            //count ticket expired

            $endDate   = Carbon::now()->format('Y-m-d');
            $startDate = Carbon::today()->subDays(3)->format('Y-m-d');

            $ticket_v2_expired = TicketsModel::join('all_ticket', function ($join) {
                $join->on('all_ticket.id', '=', 'tickets.ticket_id');
            })->where('is_used', 0)->where(function ($query) use ($user, $team_id) {
                $query->where('user_id', $user->user_id);
                $query->orWhereIn('team_id', $team_id);
            })
                ->where('expired', '<=', $endDate)
                ->whereBetween('expired', [$startDate, $endDate])
                ->count();

            $ticket_v1_user_expired = TicketUserModel::where('id_users', Auth::user()->user_id)
                ->join('ticket', function ($join) {
                    $join->on('ticket.id_ticket', '=', 'ticket_users.id_ticket');
                })
                ->where('ticket.expired', '<=', $endDate)
                ->whereBetween('expired', [$startDate, $endDate])
                ->sum('ticket');

            $ticket_v1_team_expired = TicketTeamModel::whereIn('id_team', $team_id)
                ->join('ticket', function ($join) {
                    $join->on('ticket.id_ticket', '=', 'ticket_team.id_ticket');
                })
                ->where('expired', '<=', $endDate)
                ->whereBetween('expired', [$startDate, $endDate])
                ->sum('ticket');

            $total['expired'] = $ticket_v2_expired + $ticket_v1_user_expired + $ticket_v1_team_expired;

            return response()->json([
                'status'   => true,
                'title'    => trans('message.ticket.info'),
                'data'     => $total,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.ticket.not'),
                'error'   => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get Balance.
     * <aside class="notice">APIs Method for User balance information | informasi balance user seperti stream cash dan point</aside>
     * @responseFile status=200 scenario="success" responses/account.balance.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Balance not found"
        }
     */
    public function balanceUsers()
    {
        $user_id = auth()->user()->user_id;
        if (Redis::exists('balance.' . $user_id)) {
            return response()->json(json_decode(Redis::get('balance.' . $user_id)));
        }

        try {
            $user = (new AccountTransform)->balance();

            $data = [
                'status'   => true,
                'data'      => $user,
            ];

            Redis::set('balance.' . $user_id, json_encode($data), 'EX', 60);

            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.balance.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    /**
     * Get Notif My Team.
     * <aside class="notice">APIs Method for Notification My Team | Endpoint untuk melihat notifikasi pada my team</aside>
     * @responseFile status=200 scenario="success" responses/account.notif.my.team.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Notification not found"
        }
     */
    public function getNotifMyTeam()
    {
        $user = Auth::user();
        if ($cache = RedisHelper::getData('my:team:user_id:' . $user->user_id . ':notif')) {
            return $cache;
        }
        try {
            $data = [
                'status'   => true,
                'data'     => (new AccountTransform)->notifMyTeam(),
            ];

            RedisHelper::setData('my:team:user_id:' . $user->user_id . ':notif', $data);
            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.notif.not')
            ], $this->HTTP_NOT_FOUND);
        }
    }

    /**
     * Update Profile User.
     * <aside class="notice">APIs Method for Update profile User | untuk ubah profil pengguna</aside>
     * @bodyParam fullname string required for edit Full Name | ubah nama lengkap. Example: Melli Tiara
     * @bodyParam bio string required for Edit Bidoata | ubah biodata. Example: - Bukan Gamer -
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Success edit profile"
        }
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Failed edit profile"
        }
     */
    public function editProfile(Request $request)
    {
        try {
            $user              = User::findOrFail(Auth::user()->user_id);
            $user->fullname    = $request->input('fullname');
            $user->bio         = $request->input('bio');
            $user->save();

            return response()->json([
                'status'  => true,
                'message' => Lang::get('message.user.profile.success'),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.user.profile.failed')
            ]);
        }
    }

    /**
     * Update Account Settings.
     * <aside class="notice">APIs Method for Update Account Settings | Endpoint untuk merubah pengaturan akun</aside>
     * @bodyParam jenis_kelamin string The value must be one of laki-laki or perempuan | Nilai 'laki-laki','perempuan'. Example: perempuan
     * @bodyParam tgl_lahir string The value must be a valid date in the format "Y-m-d" | Format Tanggal "Y-m-d". Example: 1999-12-27
     * @bodyParam alamat string Address for Address Value | alamat pengguna. Example: Marelan
     * @bodyParam provinsi number Id Province from dropdown Province | id pada data provinsi. Example: 12
     * @bodyParam kota number Id Town from dropdown Town | id pada data kota. Example: 1271
     * @bodyParam nomor_hp number Number Phone for Number Phone Value | nomor telepon. Example: Marelan
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Success update account settings"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed update account settings"
        }
     */
    public function accountSettings(ValidateAccountSettings $request)
    {
        try {
            $user = (new AccountTransform)->user((new User)->editAccount($request));

            return response()->json([
                'status'  => true,
                'message' => Lang::get('message.user.account.success'),
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.user.account.failed'),
            ], $this->HTTP_BAD);
        }
    }


    /**
     * Update User Preferences.
     * <aside class="notice">APIs Method for Update User Preferences | Endpoint untuk merubah preferensi user</aside>
     * @bodyParam email_notification boolean required The value must be one of true or false, for email notification | nilai Boolean 'true' atau 'false'. Example: true
     * @response scenario="success" status="200" {
            "status": true,
            "message": "Success update user preferences"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed update user preferences"
        }
     */
    public function userPreferences(Request $request)
    {
        try {
            $user_preferences = UserPreferencesModel::where('user_id', Auth::user()->user_id)->first();
            if (!$user_preferences) {
                (new UserPreferencesModel)->addPreferencesUsers(Auth::user());
            } else {
                $user_preferences->email_notification = ($request->boolean('email_notification') == 1 ? 1 : 0);
                $user_preferences->save();
            }

            return response()->json([
                'status'  => true,
                'message' => Lang::get('message.user.preferences.success'),
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'  => false,
                'message' => Lang::get('message.user.preferences.failed'),
            ], $this->HTTP_BAD);
        }
    }

    /**
     * Get My Game.
     * <aside class="notice">APIs Method for My Game Information | Endpoint untuk ambil list permainan yang kita punya</aside>
     * @responseFile status=200 scenario="success" responses/account.my.game.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Game not found"
        }
     */
    public function getMyGame()
    {
        $user    = Auth::user();
        if ($cache = RedisHelper::getData('my:game:user_id:' . $user->user_id)) {
            return $cache;
        }
        try {
            $game    = IdentityGameModel::where('users_id', $user->user_id)->with(['role' => function ($q) use ($user) {
                $q->join('game_users_role', function ($q) {
                    $q->on('game_role.id', '=', 'game_users_role.game_role_id');
                })
                    ->where('game_users_role.user_id', $user->user_id)
                    ->select(['role_name', 'game_id', 'game_role.id', 'game_role_id']);
            }])
                ->whereHas('game')
                ->get();

            $data = [
                'status'   => true,
                'data'     => $this->account_transform->myGame($game),
            ];

            RedisHelper::setData('my:game:user_id:' . $user->user_id, $data, 7 * (24 * 3600));
            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.game.not'),
                'error' => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get My Team.
     * <aside class="notice">APIs Method for My Team Information | Endpoint untuk ambil list team yang kita punya</aside>
     * @responseFile status=200 scenario="success" responses/account.my.team.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Team not found"
        }
     */
    public function getMyTeam()
    {

        $user  = Auth::user();

        if ($cache = RedisHelper::getData('my:team:user_id:' . $user->user_id . ':list')) {
            return $cache;
        }
        try {
            $data = [
                'status'   => true,
                'data'     => (new AccountTransform)->myTeam(),
            ];
            RedisHelper::setData('my:team:user_id:' . $user->user_id . ':list', $data);
            return response()->json($data);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.team.not')
            ]);
        }
    }

    /**
     * Get My Payment.
     * <aside class="notice">APIs Method for List Payment Information | Endpoint untuk melihat data pembayaran kita</aside>
     * @urlParam type required for type tab what you want to see [waiting,success,failed,refund] | kirim request data yang ingin ditampilkan. Example: waiting
     * @responseFile status=200 scenario="tab waiting | tab menunggu" responses/account.my.payment.waiting.json
     * @responseFile status=200 scenario="tab success | tab pembayaran sukses" responses/account.my.payment.success.json
     * @responseFile status=200 scenario="tab failed | tab pembayaran gagal" responses/account.my.payment.failed.json
     * @responseFile status=200 scenario="tab refund | tab pengembalian" responses/account.my.payment.refund.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Payment not found"
        }
     */
    public function getMyPayment(String $type = 'waiting')
    {
        try {
            $user             = Auth::user();
            $payment          = PlayerPayment::where('player_id', $user->user_id)->get();
            $response_payment = new Collection($payment, new MyPaymentTransform);
            $payment_collect  = collect(current((new Manager())->createData($response_payment)->toArray()));

            switch ($type) {
                case 'waiting':
                    $payment = $payment_collect->where('status', 'Belum-Lunas')->values();
                    break;
                case 'success':
                    $payment = $payment_collect->whereIn('status', ['Waiting', 'Lunas'])->values();
                    break;
                case 'failed':
                    $payment = $payment_collect->where('status', 'Cancelled')->values();
                    break;
                case 'refund':
                    $payment = $payment_collect->where('refund_date', '!=', NULL)->values();
                    break;
                default;
                    throw new Exception(trans('message.payment.not'));
                    break;
            }

            $data = [
                'id'            => $user->user_id,
                'fullname'      => $user->fullname,
                'username'      => $user->username,
                'active_tab'    => $type,
                'payment'       => $payment

            ];

            return response()->json([
                'status'   => true,
                'data'      => $data,
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.payment.not')
            ]);
        }
    }

    /**
     * Get Detail My Team.
     * <aside class="notice">APIs Method for Detail My Team Information | Endpoint untuk melihat detail Team Kita</aside>
     * @urlParam team_id required The Url of the team. Example: s2qHFzf0h2eNGtTTZ-kO
     * @responseFile status=200 scenario="success" responses/account.detail.my.team.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Team not found"
        }
     * @response scenario="failed, not member" status="400" {
            "status": false,
            "message": "Anda bukan anggota tim ini"
        }
     */
    public function getDetailMyTeam($hash)
    {
        $user = Auth::user();
        $team_id        = (new \App\Helpers\MyApps)->onlyDecrypt($hash);
        if ($cache = RedisHelper::getData('my:team:team_id:' . $team_id . ':user_id:' . $user->user_id)) {
            return $cache;
        }
        try {

            $team             = TeamModel::find($team_id);

            if (is_null($team)) {
                throw new Exception();
            }

            $check_team = (new TeamHelperController)->isMember($team);

            if (!is_null($check_team)) {
                return $check_team;
            }

            $data = [
                'status'   => true,
                'data'     => (new AccountTransform)->myDetailTeam($team),
            ];

            RedisHelper::setData('my:team:team_id:' . $team_id . ':user_id:' . $user->user_id, $data);

            return response()->json($data);
        } catch (\Throwable $exception) {
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.team.not')
            ]);
        }
    }

    /**
     * Update Avatar User.
     * <aside class="notice">APIs Method for Update User Avatar(Picture) | Endpoint untuk merubah gambar profil</aside>
     * @bodyParam image file required The Picture of User. The extension must be one of JPG, JPEG, PNG. Max size
       is 2MB
     *@response scenario="success" status="200" {
            "status": true,
            "message": "Success update user picture"
            "image": "http://localhost/cmfg/picture/user/6n2UUGvguW3QDo-Ua-oflcLJHOPIYDBz_q_vT808lrUZCifkfuJ0rqBIxDpChVE_eodRVSa9k6598quu"
        }
     * @response scenario="failed" status="400" {
            "status": false,
            "message": "Failed update user picture"
        }
     */
    public function changeAvatar(ValidateRequestUploadImage $request)
    {
        try {
            //file avatar
            $file               = $request->file('image');
            $user_detail        = UsersDetailModel::where('user_id', Auth::user()->user_id)->first();

            //upload and resize image
            $upload = (new UploadService)->mini(80, 80)->upload("private/{$request->user()->user_id}", $file, $user_detail->image, [
                'options' => 'resize',
                'width' => 250,
                'height' => 250
            ]);

            //check error when upload
            if (is_object($upload)) {
                return $upload;
            }

            //- Delete caching detail team
            $team_id    = TeamPlayerModel::where('player_id', 'LIKE', '%' . request()->user()->user_id . '%')
                ->orWhere('substitute_id', 'LIKE', '%' . request()->user()->user_id . '%')
                ->select('team_id')
                ->pluck('team_id')->toArray();

            RedisHelper::destroyDataWithArray('team:detail:team_id', $team_id, false);

            $user_detail->image = $upload;
            $user_detail->save();

            $this->firebase()
                ->getReference('tournament/comment/user_profile/' . Auth::user()->user_id)
                ->set($upload);

            return response()->json([
                'status'    => true,
                'message'   => Lang::get('message.user.profile.upload.success'),
                'image'     => $upload
            ], $this->HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.user.profile.upload.failed'),
                'error'     => $e
            ], $this->HTTP_BAD);
        }
    }
}
