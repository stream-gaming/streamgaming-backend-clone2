<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;

/**
 * @group Authentication
 */
class CheckIfItIsUsedController extends Controller
{
    /**
     * Check Email & Username
     *
     * This endpoint let you to check if the email or username is used.
     * URL parameters are required to retrieve data.
     *
     * @urlParam type required field type (email,username) Example:username
     * @urlParam value required value of field Example:lordfahdan
     * @response status=200 {
            "status": true,
            "message": "Username sudah digunakan"
        }
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            "type" => 'required|in:email,username',
            "value" => 'required'
        ]);

        if($request->type == "email"){
            $request->validate([
                'value' => 'email|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'
            ]);
            $data = $this->email($request->value);
        }
        elseif ($request->type == "username") {
            $data = $this->username($request->value);
        }

        $unique     = trans('validation.unique', ['attribute' => $request->type]);
        $non_unique = trans('validation.non_unique', ['attribute' => $request->type]);

        return response()->json([
            "status"  => $data,
            "message" => ($data ? $non_unique : $unique),
        ]);

    }

    public function email($email)
    {
        $check = User::where('email',$email)->doesntExist();

        return $check;
    }

    public function username($username)
    {
        $check = User::where('username',$username)->doesntExist();

        return $check;

    }
}//keep
