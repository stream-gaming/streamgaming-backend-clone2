<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserPreferenceOnboardingRequest;
use App\Modules\Onboarding\UserOnboarding;
use App\Modules\Onboarding\UserPreferenceOnboarding;
use App\Transformer\UserPreferenceOnboardingTransformer;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;

/**
 * @group  User Onboarding
 *
 * APIs for managing user's onboarding
 */
class PreferenceOnboardingController extends Controller
{
    /**
     * Update user onboarding preference
     *
     * @authenticated
     * @bodyParam preference in:solo,team required the user's preference
     * @responseFile status=200 scenario="success" responses/onboarding.preference.update.success.json
     * @responseFile status=500 scenario="failed" responses/onboarding.preference.update.failed.json
     **/
    public function update(UserPreferenceOnboardingRequest $request){
        try{

            $user = Auth::user();

            //Update user's preference
            $userOnboarding = new UserOnboarding($user);
            if(!$userOnboarding->update(new UserPreferenceOnboarding($request->preference))){
                throw new Exception('user preference onboarding validator error for user with id '.$user->user_id);
            }
            //Load new user data after updating the preference
            $user->load('userOnboarding');

            //Transform user's preference output
            $resource = new Item($user, new UserPreferenceOnboardingTransformer(['status', 'progress']));
            $manager = (new Manager)->setSerializer(new ArraySerializer());
            $data = $manager->createData($resource)->toArray();

            return response()->json($data, 200);

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            return response()->json([
                'status' => false,
                'message' => Lang::get('onboarding.failed.preference')
            ], 500);

        }


    }

    /**
     * Get user onboarding preference
     *
     * @authenticated
     * @responseFile status=200 scenario="success" responses/onboarding.preference.get.success.json
     **/
    public function get(){

        $user = Auth::user();

        //Transform user's preference output
        $resource = new Item($user, new UserPreferenceOnboardingTransformer);
        $manager = (new Manager)->setSerializer(new ArraySerializer());
        $data = $manager->createData($resource)->toArray();

        return response()->json($data, 200);
    }
}
