<?php

namespace App\Http\Controllers;

use App\Model\TournamentLiga;
use App\Transformer\TournamentLigaNewTransform;
use App\Transformer\TournamentLigaTransform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Exception;
use Illuminate\Support\Facades\DB;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * @group Tournament
 *
 * API for get tournament information
 */
class TournamentLigaController extends Controller
{
    private $date;
    private $dateformat;
    private $dateweekstart;
    private $dateweekend;

    public function __construct()
    {
        $this->date = new Carbon();
        $this->dateformat =  'Y-m-d H:i';
        $this->dateweekstart = $this->date->now()->startOfWeek(Carbon::MONDAY)->format($this->dateformat);
        $this->dateweekend  = $this->date->now()->endOfWeek(Carbon::SUNDAY)->format($this->dateformat);
    }


    /**
     * Tournament Liga List
     *
     * this endpoint retrieve list of tournament league data with pagination, limit and tab filter
     * @queryParam tab string filter data by tab ['this_week', 'upcoming', 'finished']
     * @queryParam page integer filter data by page number
     * @queryParam limit integer limit retrieve data for each page
     * @responseFile status=200 scenario="success" responses/tournament.league.list.json
     */
    public function liga_list(Request $request)
    {
        $time = $request->query('tab');
        $limit = abs($request->query('limit', 6));

        $data = TournamentLiga::with(['game' => function($q) {
            $q->select('id_game_list', 'name', 'logo', 'system_kompetisi');
        }])->whereHas('game');


            if($time=="this_week"){
                $data = $data->whereBetween('start_tour',[$this->dateweekstart,$this->dateweekend])
                            ->orWhereDate('start_tour','<=',$this->dateweekstart)
                            ->whereDate('end_tour','>=',$this->dateweekend);
            }
            elseif ($time=="upcoming") {
                $data = $data->whereDate('start_tour','>',$this->dateweekend);
        }
            elseif ($time=="finished") {
            $data = $data->whereDate('end_tour','<',$this->dateweekstart);
        }

        $data = $data->where('on_view',1)->orderBy('start_tour','DESC')->paginate($limit);

        $resource = new Collection($data,(new TournamentLigaNewTransform));

        $transform = (new Manager())->createData($resource->setPaginator(new IlluminatePaginatorAdapter($data)))->toArray();

        $tab = [
            "tab" => [
                [
                    "tabname" => "semua",
                    "url" => "all"
                ],
                [
                    "tabname" => "minggu ini",
                    "url" => "this_week"
                ],
                [
                    "tabname" => "akan datang",
                    "url" => "upcoming"
                ],
                [
                    "tabname" => "selesai",
                    "url" => "finished"
                ]
            ]
        ];

        $data = array_merge($transform, $tab);

        return $data;
    }


    /**
     * Tournament Liga Show Detail
     *
     * this endpoint use to retrieve tournament list data at one Tournament Liga Event
     * you can also search the data you need
     * @urlParam url required Liga URL is required. Example:abc
     * @queryParam search string search tournament by title. No-example
     * @responseFile status=200 scenario="success" responses/tournament.list.liga.detail.json
     * @response scenario="failed" status="404" {
            "status": false,
            "message": "Turnamen tidak ditemukan"
        }
     */
    public function show_liga($url, Request $request)
    {
        $search   = filter_var($request->search, FILTER_SANITIZE_STRING);
        $tab      = filter_var($request->tab, FILTER_SANITIZE_STRING);


        try {
            $data = TournamentLiga::with('tabmenu')->with(['tournament' => function ($q) use($search,$tab)
            {
                $q->with(['AvailableSlots' => function ($q)
                            {
                            $q->select('id_leaderboard','total_team')
                            ->where('qualification_sequence',1);
                            }]);
                $q->leftJoin(DB::raw('(SELECT COALESCE(SUM(nilai),0) as total_prize2, tournament_id
                             FROM (SELECT (cp.quantity * pi2.valuation ) as nilai,tournament_id
                             FROM costum_prizes cp join prize_inventory pi2 on cp.pi_id = pi2.id) tb
                             GROUP BY tournament_id) prizes'),function($join){
                                $join->on(DB::raw('prizes.tournament_id COLLATE utf8mb4_general_ci'),'list_tournament.id_tournament');
                            });
                $q->where('statuss','!=',5);


                if($search!=null){
                   $q->where('tournament_name','LIKE','%'.$search.'%');
                }

                if ($tab=="this_week"){
                    $q->whereBetween('tournament_date',[$this->dateweekstart,$this->dateweekend]);
                }
                elseif ($tab=="upcoming") {
                    $q->where('tournament_date','>',$this->dateweekend);
                }
                elseif ($tab=="finished") {
                    $q->where('statuss',4);
                }


                if($tab!="finished"){
                    $q->orderBy('statuss')->orderBy('tournament_date','asc')->orderBy('filled');
                }
                else{
                    $q->orderBy('tournament_date','desc');
                }

            }])->where('url',$url)->firstOr(function ()
            {
                throw new Exception(Lang::get('tournamentliga.failed'));
            });
            $datas = (new TournamentLigaTransform)->single($data);
            return response()->json($datas);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => false,
                'message'   => $e->getMessage()
            ], $this->HTTP_NOT_FOUND);
        }






    }
}
