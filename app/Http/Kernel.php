<?php

namespace App\Http;

use App\Http\Middleware\CheckPin;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Illuminate\Session\Middleware\StartSession::class,
        // \PragmaRX\Tracker\Vendor\Laravel\Middlewares\Tracker::class,
        // Middleware\JwtMiddleware::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:120,1',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'jwt.refresh' => \Tymon\JWTAuth\Middleware\RefreshToken::class,
        'localization' => \App\Http\Middleware\Localization::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'jwt' => \App\Http\Middleware\RefreshToken::class,
        'players' => \App\Http\Middleware\PlayerStatus::class,
        'json.response' => \App\Http\Middleware\JsonResponse::class,
        'team.banned' => \App\Http\Middleware\TeamBanned::class,
        'team.banned.inv.code' => \App\Http\Middleware\TeamBannedInvCode::class,
        'save.api' => \App\Http\Middleware\SaveApiLog::class,
        'checkpin' => \App\Http\Middleware\CheckPin::class,
        'auth.admin' => \App\Http\Middleware\AdminAuthenticate::class,
        'is.join.tour' => \App\Http\Middleware\HandleIfJoinTournament::class,
        'block.request' => \App\Http\Middleware\BlockRequest::class,
        'portal.auth' => \App\Http\Middleware\PortalAuthentication::class,
        'guard.request' => \App\Http\Middleware\GuardRequestMiddleware::class,
        'save.register.response' => \App\Http\Middleware\SaveRegisterResponse::class,
        'xendit.auth' => \App\Http\Middleware\EnsureXenditTokenIsValid::class,
        'set.user.onboarding' => \App\Http\Middleware\SetUserOnboarding::class,
        'add.game.onboarding' => \App\Http\Middleware\AddGameOnboardingProgress::class,
        'create.team.onboarding' => \App\Http\Middleware\CreateTeamOnboardingProgress::class,
    ];
}
