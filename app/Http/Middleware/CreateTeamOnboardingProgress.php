<?php

namespace App\Http\Middleware;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\UserOnboarding;
use App\Modules\Onboarding\UserTeamOnboarding;
use Closure;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CreateTeamOnboardingProgress
{
    /**
     * Update user onboarding after successfully create team
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        //Update user onboarding once team is created successfully
        try{
            $content = $response->getContent();
            $resp = json_decode($content);

            if($resp->status){
                $user = Auth::user();
                $userOnboarding = $user->userOnboarding;

                //Check if this user is new user or not
                if($user && $userOnboarding){

                    //Update progress if user onboarding status has not finished yet
                    if($userOnboarding->status == OnboardingEnum::ONPROGRESS_STATUS){
                        $userOnboarding = new UserOnboarding($user);
                        $userOnboarding->update(new UserTeamOnboarding);
                    }

                }

            }
        }catch(Exception $e){
            Log::debug($e->getMessage());
        }

        return $response;
    }
}
