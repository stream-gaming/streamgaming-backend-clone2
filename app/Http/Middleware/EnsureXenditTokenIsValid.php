<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\UnauthorizedException;

class EnsureXenditTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('X-CALLBACK-TOKEN') === config('app.xendit_callback_token') ){
            return $next($request);
        }

        return response()->json([
            'status'    => false,
            'message'    => 'This token is invalid!',
        ], 401);
    }
}
