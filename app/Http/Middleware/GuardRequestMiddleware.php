<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;

class GuardRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Cache::get($request->path()) == true){
            abort(400, Lang::get('message.block.error'));
        }else{
            Cache::put($request->path(), true, 2);
        }

        return $next($request);
    }
}
