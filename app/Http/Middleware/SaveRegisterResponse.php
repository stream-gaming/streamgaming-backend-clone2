<?php

namespace App\Http\Middleware;

use App\Helpers\MyApps;
use App\Model\ApiLogModel;
use Closure;

class SaveRegisterResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestAll = $request->all();

        $MyApps = new MyApps;

        if ($request->password) {
            $requestAll['password'] = $MyApps->onlyEncrypt($request->password);
        } elseif ($request->pin) {
            $requestAll['pin'] = $MyApps->onlyEncrypt($request->pin);
        }

        $requestData = [
            'fullUrl' => $request->fullUrl(),
            'input' => $requestAll,
            'bearerToken' => $request->bearerToken()
        ];
        $response = $next($request);
        $responseData = [
            'status'    => $response->status(),
            'ip'        => $request->ip(),
            'method'    => $request->method(),
            'content'   => $response->content()
        ];

        if($response->status() != 200){
            ApiLogModel::create([
                'request'   => json_encode($requestData),
                'response'  => json_encode($responseData),
                'type'      => 1
            ]);
        }




        return $response;
    }
}
