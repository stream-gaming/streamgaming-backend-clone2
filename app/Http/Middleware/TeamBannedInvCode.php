<?php

namespace App\Http\Middleware;

use App\Model\TeamModel;
use App\Modules\Team\Interfaces\InvitationCodeRepositoryInterface;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;

class TeamBannedInvCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!is_null($request->invitation_code))
        {
            $invCodeRepo = App::make(InvitationCodeRepositoryInterface::class);

            //join using invitation code for specific team
            if(!is_null($request->team_id))
            {
                $invCode = $invCodeRepo->findByCodeAndTeamId($request->invitation_code, $request->team_id);
            }else{
                $invCode = $invCodeRepo->findByCodeNoException($request->invitation_code);
            }

            if (!is_null($invCode))
            {
                $team = TeamModel::find($invCode->team_id);
                if ($team->is_banned) {
                    return response()->json([
                        'status'    => false,
                        'message'   => Lang::get('message.team.banned')
                    ], 400);
                }
            }

        }

        return $next($request);
    }
}
