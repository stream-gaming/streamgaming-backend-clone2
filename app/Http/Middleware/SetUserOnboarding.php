<?php

namespace App\Http\Middleware;

use App\Modules\Onboarding\UserOnboarding;
use App\Model\User;
use Closure;
use Exception;
use Illuminate\Support\Facades\Log;

class SetUserOnboarding
{
    /**
     * Set user onboarding after successfully register account
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        //Set user onboarding once the registration process is success
        try{
            $content = $response->getContent();
            $resp = json_decode($content);
            //Give time for read&write database to update the data
            usleep(500*1000);

            if($resp->status){
                $user = User::where('email', $request->input('email'))->first();

                if($user){
                    $userOnboarding = new UserOnboarding($user);
                    $userOnboarding->set();
                }
            }

        }catch(Exception $e){
            Log::debug($e->getMessage());
        }

        return $response;
    }
}
