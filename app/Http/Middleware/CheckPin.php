<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class CheckPin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->payment_method != 'cash' && $request->payment_method != 'point'){
            return $next($request);
        }

        if ($request->pin == null) {
            return response()->json(['message' => 'pin is required'], 400);
        }
        $user = $request->user();
        $pin = $user->pin;
        if ($pin->activity == 0) {
            return response()->json(['message' => 'your pin blocked'], 400);
        }
        if (!Hash::check($request->pin, $pin->authenticate)) {
            $count_pin_check = Cache::get($user->user_id . '-pin-check', 0);
            $count_pin_check++;
            Cache::put($user->user_id . '-pin-check', $count_pin_check);
            // if ($count_pin_check > 2) {
            //     // $pin->activity = 0;
            //     // $pin->save();
            //     return response()->json(['message' => 'we block your pin for security reason.'], 403);
            // }
            return response()->json(['message' => 'pin invalid'], 403);
        }
        Cache::put($user->user_id . '-pin-check', 0);
        return $next($request);
    }
}
