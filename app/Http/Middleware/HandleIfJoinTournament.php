<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\TeamModel;
use App\Model\TeamInviteModel;
use App\Model\TeamRequestModel;
use Illuminate\Support\Facades\Lang;
use App\Helpers\Validation\TeamTournamentRegistrationStatus;
use App\Http\Controllers\Api\Team\TeamHelperController;

class HandleIfJoinTournament
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //kirim id leader di semua request ($request->user()->user_id) ke helper teamregis
        //untuk req dari game cari id leader di timnya
        //blok akses acc request
        $id = $request->route()->parameter('id'); //id from game request
        $gameTeam = (new TeamHelperController)->getTeamByGame($id);
        $requestUserId = is_null($gameTeam) ? null : $gameTeam->leader_id; //return string:jika edit game dan punya tim|null:jika edit game dan tidak punya tim

        if (!is_null($id) and gettype($requestUserId) == 'string') {
            //jika dari edit game dan punya tim
            //maka cek leader nya
            if ((new TeamTournamentRegistrationStatus)->check($requestUserId, $id)) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.tournament.is_registered')
                ], 400);
            }
        } else if (is_null($id) and is_null($requestUserId)) {
            //jika dari tim
            if (!is_null($request->route()->parameter('team_id')) or !is_null($request->team_id)) { //handle request in team
                $team_id = $request->route()->parameter('team_id') ?? $request->team_id;
                $team = TeamModel::find($team_id);
                if($team){
                    $id = $team->game_id;
                    $requestUserId = $team->leader_id;
                }
            } else if (!is_null($request->route()->parameter('req_id'))) { // Acc or Decl Request Join
                $request_id = $request->route()->parameter('req_id');
                $request_team = TeamRequestModel::with('team')
                    ->find($request_id);
                $id = $request_team->team->game_id;
                $requestUserId = $request_team->team->leader_id;
            } else if (!is_null($request->route()->parameter('inv_id'))) { // Acc or Decl Invited Join (User)
                $invite_id = $request->route()->parameter('inv_id');
                $invite_team = TeamInviteModel::with('team')
                    ->find($invite_id);
                $id = $invite_team->team->game_id;
                $requestUserId = $invite_team->team->leader_id;
            }
            if ((new TeamTournamentRegistrationStatus)->check($requestUserId, $id)) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.tournament.is_registered')
                ], 400);
            }
        }

        return $next($request);
    }
}


