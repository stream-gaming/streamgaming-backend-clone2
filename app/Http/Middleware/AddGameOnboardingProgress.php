<?php

namespace App\Http\Middleware;

use App\Modules\Onboarding\UserAddGameOnboarding;
use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\UserOnboarding;
use Closure;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AddGameOnboardingProgress
{
    /**
     * Update user onboarding after successfully add game
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        //Update user onboarding once game is added successfully
        try{
            $content = $response->getContent();
            $resp = json_decode($content);

            if($resp->status){
                $user = Auth::user();
                $userOnboarding = $user->userOnboarding;

                //Check if this user is new user or not
                if($user && $userOnboarding){

                    //Update progress if user onboarding status has not finished yet
                    if($userOnboarding->status == OnboardingEnum::ONPROGRESS_STATUS){
                        $userOnboarding = new UserOnboarding($user);
                        $userOnboarding->update(new UserAddGameOnboarding);
                    }

                }

            }
        }catch(Exception $e){
            Log::debug($e->getMessage());
        }

        return $response;
    }
}
