<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $payload = JWTAuth::payload();
            $ip = $payload['boy'];
            if(!Hash::check($request->ip(), $ip)){
                return response()->json([
                    'status'    => false,
                    'message'    => 'This token is invalid. Please Login',
                ],401);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    'status'    => false,
                    'message'    => 'This token is invalid. Please Login',
                ]);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                // If the token is expired, then it will be refreshed and added to the headers
                try {
                    $token = JWTAuth::getToken();
                    $newToken = JWTAuth::refresh($token);
                } catch (JWTException $e) {
                    return response()->json([
                        'status'    => false,
                        'message' => 'Token cannot be refreshed, please Login again'
                    ]);
                }
            } else {
                return response()->json([
                    'status'    => false,
                    'message' => 'Authorization Token not found'
                ], 404);
            }
        }
        return $next($request);
    }
}
