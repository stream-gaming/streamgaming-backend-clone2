<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\MyApps;
use App\Model\ApiLogModel;
use Illuminate\Support\Facades\Log;

class SaveApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $requestAll = $request->all();
            $MyApps = new MyApps;

            if ($request->password) {
                $requestAll['password'] = $MyApps->onlyEncrypt($request->password);
            } elseif ($request->pin) {
                $requestAll['pin'] = $MyApps->onlyEncrypt($request->pin);
            }        
            $requestData = [
                'fullUrl' => $request->fullUrl(),
                'input' => $requestAll,
                'bearerToken' => $MyApps->onlyEncrypt($request->bearerToken())
            ];
            $response = $next($request);
            $responseData = [
                'status'    => $response->status(),
                'ip'        => $request->ip(),
                'method'    => $request->method(),
                'content'   => json_decode($response->content())
            ];
            $Allresponse = [
                'request'   => $requestData,
                'response'  => $responseData,
                'type'      => 1
            ];
       
            Log::channel('cloudwatchApi')->info("API LOGS", $Allresponse);
            
            return $response;
        } catch(\Exception $e) {
            Log::debug($e);
        }

      
    }
}
