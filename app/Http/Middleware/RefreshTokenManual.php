<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class RefreshTokenManual
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            // $payload = JWTAuth::payload();
            // $ip = $payload['boy'];
            // if(!Hash::check($request->ip(), $ip)){
            //     return response()->json([
            //         'status'    => false,
            //         'message'    => Lang::get('auth.refresh.token.unauthenticated', ['code' => '#cdcrts_xrfrshmn01']),
            //     ],401);
            // }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return  $next($request);

            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                // Jika Token Expired, Maka Akan Refresh Token
                try {
                    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                    $user = JWTAuth::setToken($refreshed)->toUser();
                    // header('Authorization: Bearer ' . $refreshed);
                } catch (JWTException $e) {
                    return  $next($request);
                }
            } else {
                return  $next($request);

            }
        }

        return  $next($request);
    }
}
