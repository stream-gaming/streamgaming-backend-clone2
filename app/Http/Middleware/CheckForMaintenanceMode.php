<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
// use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    protected $request;
    protected $app;

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
    }

    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance()){
            return response()->json([
            	'status'	=> false,
            	'message'	=> is_null(json_decode(file_get_contents(storage_path('framework/down')), true)['message']) ?
            				 	'Server is maintenance' : 
            				 	json_decode(file_get_contents(storage_path('framework/down')), true)['message']
            ], 503);
        }

        return $next($request);
    }

}
