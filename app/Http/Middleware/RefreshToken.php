<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Model\User;
use App\Model\Management;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class RefreshToken extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorization = $request->headers->get('Authorization');
        $user = explode(' ', $authorization);

        if ($user[0] == 'Users') {
            $user = User::where('user_id', $user[1])->first();
            if ($user) {
                Auth::login($user, false);
                return  $next($request);
            } else {
                return response()->json([
                    'status'    => false,
                    'message' => Lang::get('auth.refresh.token.unauthenticated', ['code' => '#decyber_xrfrsh01']),
                ], 401);
            }
        } elseif ($user[0] == 'Admin') {
            $user = Management::find($user[1]);
            if ($user) {
                auth()->shouldUse('admins');
                Auth::login($user, false);
                return $next($request);
            } else {
                return response()->json([
                    'status'    => false,
                    'message' => Lang::get('auth.refresh.token.unauthenticated', ['code' => '#arvn_xrfrsh01']),
                ], 401);
            }
        } else {
            try {
                $user = JWTAuth::parseToken()->authenticate();
                // $payload = JWTAuth::payload();
                // $ip = $payload['boy'];
                // if(!Hash::check($request->ip(), $ip)){
                //     return response()->json([
                //         'status'    => false,
                //         'message'    => Lang::get('auth.refresh.token.unauthenticated', ['code' => '#cdcrts_xrfrsh01']),
                //     ],401);
                // }
            } catch (Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                    return response()->json([
                        'status'    => false,
                        'message'    => Lang::get('auth.refresh.token.invalid', ['code' => '#decyber_xrfrsh02']),
                    ], 401);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                    return response()->json([
                        'status'    => false,
                        'message'    => Lang::get('auth.refresh.token.blacklist'),
                    ], 401);
                } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                    // Jika Token Expired, Maka Akan Refresh Token
                    try {
                        $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                        $user = JWTAuth::setToken($refreshed)->toUser();
                        header('Authorization: Bearer ' . $refreshed);
                    } catch (JWTException $e) {
                        return response()->json([
                            'status'    => false,
                            'message' => Lang::get('auth.refresh.token.failed', ['code' => '#decyber_xrfrsh03']),
                        ], 401);
                    }
                } else {
                    return response()->json([
                        'status'    => false,
                        'message' => Lang::get('auth.refresh.token.unauthenticated', ['code' => '#decyber_xrfrsh04']),
                    ], 401);
                }
            }
           
            return  $next($request);
        }
    }
}
