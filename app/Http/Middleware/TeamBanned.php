<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Model\TeamModel;
use App\Model\TeamInviteModel;
use App\Model\TeamRequestModel;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\Api\Team\TeamHelperController;

class TeamBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $team_id = is_null($request->team_id) ? $request->route()->parameter('team_id') : $request->team_id;
        $request_id = is_null($request->route()->parameter('req_id')) ?
                        $request->route()->parameter('inv_id') :
                        $request->route()->parameter('req_id');
        $gameTeam = (new TeamHelperController)->getTeamByGame($request->id);

        $team = TeamModel::find($team_id) ?? $gameTeam;
        $invite_ = TeamInviteModel::find($request_id);
        $request_ = TeamRequestModel::find($request_id);

        if (!is_null($team)) {
            if ($team->is_banned) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.banned', [
                        'time'=> $team->ban_until != 'forever' ? Carbon::parse($team->ban_until)->diffForHumans() :
                                                                'forever'
                    ])
                ], 400);
            }
        } elseif (!is_null($invite_)) {
            $team = TeamModel::find($invite_->team_id);
            if ($team->is_banned) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.banned', [
                        'time'=> $team->ban_until != 'forever' ? Carbon::parse($team->ban_until)->diffForHumans() :
                                                                'forever'
                    ])
                ], 400);
            }
        } elseif (!is_null($request_)) {
            $team = TeamModel::find($request_->team_id);
            if ($team->is_banned) {
                return response()->json([
                    'status'    => false,
                    'message'   => Lang::get('message.team.banned', [
                        'time'=> $team->ban_until != 'forever' ? Carbon::parse($team->ban_until)->diffForHumans() :
                                                                'forever'
                    ])
                ], 400);
            }
        }

        return $next($request);
    }
}
