<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuyTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //
            'ticket_id'       => 'required|max:255',
            'payment_method'  => 'required|in:cash,point',
            'quantity'        => 'required|integer|min:1',
            'pin'             => 'required|max:255',
            'game_id'         => 'required|string|max:50|exists:game_list,id_game_list'

        ];



    }
}
