<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class XenditCallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       return [
            //
            'id' => 'required',
            'external_id'=> 'required',
            'payment_method'=> 'required',
            'status'=> 'required',
            'amount'=> 'required',
            'paid_amount'=> 'required|same:amount',
            'paid_at'=> 'required',
            'currency'=> 'required',
            'payment_channel'=> 'required',

        ];
    }
}
