<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateBannedGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
    * @return array
     */
    public function rules()
    {
        return [
            'banned_until'  => 'required|string',
            'reason'    => 'required|string|max:255',
            'identity_id'   => 'required|exists:identity_ingame,id'
        ];
    }
}
