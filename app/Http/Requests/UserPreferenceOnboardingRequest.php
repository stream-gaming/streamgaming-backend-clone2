<?php

namespace App\Http\Requests;

use App\Modules\Onboarding\Enum\OnboardingEnum;
use Illuminate\Foundation\Http\FormRequest;

class UserPreferenceOnboardingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'preference' => 'required|in:1,2'
        ];
    }

    protected function prepareForValidation()
    {
        //Transform preference
        if($this->preference == 'solo') $this->merge(['preference' => OnboardingEnum::SOLO_PREFERENCE]);
        if($this->preference == 'team') $this->merge(['preference' => OnboardingEnum::TEAM_PREFERENCE]);

    }
}
