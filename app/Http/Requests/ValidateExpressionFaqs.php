<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateExpressionFaqs extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(){
        return [
            'id_faqs'       => 'required',
            'expression'    => 'required|in:like,dislike',
        ];
    }
}
