<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateRequestEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'   => 'required|email',
        ];
    }

    public function attributes()
    {
        return [
            'email'   => Lang::get('validation.attributes.email'),
        ];
    }

    public function messages()
    {
        return [
            //- Email
            'email.required' => Lang::get('validation.required'),
            'email.email'    => Lang::get('validation.email'),
        ];
    }
}
