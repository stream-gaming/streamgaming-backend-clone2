<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateSocmed extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //- Validasi User Login
            'sosmed'     => 'required',
            'url'        => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'sosmed'     => Lang::get('validation.attributes.sosmed'),
            'url'        => Lang::get('validation.attributes.url'),
        ];
    }

    public function messages()
    {
        return [
            //- Email
            'sosmed.required' => Lang::get('validation.required'),
            //- Password
            'url.required'  => Lang::get('validation.required'),

        ];
    }
}
