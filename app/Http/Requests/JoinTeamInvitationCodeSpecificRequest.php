<?php

namespace App\Http\Requests;

use App\Rules\InvCodeValidation;
use Illuminate\Foundation\Http\FormRequest;

class JoinTeamInvitationCodeSpecificRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id' => 'bail|required',
            'invitation_code' => ['bail','required', new InvCodeValidation],
        ];
    }
}
