<?php

namespace App\Http\Requests;

use App\Modules\Chat\TimeModeEnum;
use App\Rules\TimeStampCheck;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LatestMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => [
                'numeric',
                'digits:10',
                new TimeStampCheck
            ],
            'limit' => 'numeric|min:1'
        ];
    }
}
