<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'ticket_id' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1|max:1000',
            'game_id' => 'required|string|max:50|exists:game_list,id_game_list'
        ];
    }
}
