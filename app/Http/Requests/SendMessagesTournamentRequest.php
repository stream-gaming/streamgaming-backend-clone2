<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SendMessagesTournamentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_status'   => 'required|in:1,0',
            'image_only'    => 'required|in:1,0',
            'message'       => 'required_if:image_only,0|string',
            'attach'        => 'nullable|image|mimes:jpeg,png,jpg',
            'tournament_id' => 'required|string',
            'stream_id'     => 'required_if:user_status,1|string',
        ];
    }
}
