<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateAddGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'id_ingame'      => 'required|max:255|unique:App\Model\IdentityGameModel,id_ingame', 
            'username_ingame'=> 'required|max:255',
            'game_role_id'   => 'array|max:3'
        ]; 
    }

    public function attributes()
    {
        return [
            'id_ingame'         => Lang::get('validation.attributes.id_ingame'),
            'username_ingame'   => Lang::get('validation.attributes.username_ingame'),
            'game_role_id'      => Lang::get('validation.attributes.game_role_id') 
        ];
    }

    public function messages()
    {
        return [
            'id_ingame.required'        => Lang::get('validation.required'),
            'username_ingame.required'  => Lang::get('validation.required'),
            'id_ingame.max'             => Lang::get('validation.max.string'),
            'username_ingame.max'       => Lang::get('validation.max.string'),
            'game_role_id.max'          => Lang::get('validation.max.role'),
        ];
    }
}
