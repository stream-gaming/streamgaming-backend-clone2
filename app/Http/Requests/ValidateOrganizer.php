<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateOrganizer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return 
        [
            'first_name'        => 'required|string',
            'last_name'         => 'required|string',
            'email'             => 'required|email',
            'company'           => 'required|string',
            'industry'          => 'required|string',
            'position'          => 'required|string',
            'city'              => 'required|string',
            'phone_number'      => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:9',
            'manager_tour'      => 'required|string'
        ];
    }
    public function messages()
    {
    
        return  [
            'manager_tour.required' =>  Lang::get('organizer.required.manager_tour'),
            'phone_number.regex'    =>  Lang::get('organizer.regex.phone_number'),
            'phone_number.not_regex'=>  Lang::get('organizer.regex.phone_number'),
            'phone_number.min'      =>  Lang::get('organizer.min.phone_number'),
        ];
    }
}
