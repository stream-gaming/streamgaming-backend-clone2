<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateJoinTournament extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'id_tournament'  => 'required|max:512',
            'payment_method' => 'in:,cash,point',
            'treat'          => 'required_if:payment_method,cash,point|in:self,all'
        ];
    }
}
