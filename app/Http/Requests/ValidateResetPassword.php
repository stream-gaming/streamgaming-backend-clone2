<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //- Validasi Lupa Password
            'token'     => 'required',
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|confirmed|min:8',
        ];
    }
    public function attributes()
    {
        return [
            'token'         => Lang::get('validation.attributes.token'),
            'email'         => Lang::get('validation.attributes.email'),
            'password'      => Lang::get('validation.attributes.password'),
        ];
    }
    public function messages()
    {
        return [
            //- Token
            'token.required' => Lang::get('validation.required'),
            //- Email
            'email.required' => Lang::get('validation.required'),
            'email.email'    => Lang::get('validation.email'),
            'email.max'      => Lang::get('validation.max.string'),
            'email.string'   => Lang::get('validation.string'),
            //- Password
            'password.required'  => Lang::get('validation.required'),
            'password.min'       => Lang::get('validation.min.string'),
            'password.confirmed' => Lang::get('validation.confirmed'),

        ];
    }
}
