<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateCreateTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_name' => 'required|unique:team,team_name,NULL,team_id,deleted_at,NULL|string|max:255',
            'game_id'   => 'required|exists:App\Model\GameModel,id_game_list',
            'team_description'  => 'required|string|max:255',
            'city'      => 'required|exists:App\Model\CityModel,nama',
            'team_logo' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ];
    }

    public function attributes()
    {
        return [
            'team_name' => Lang::get('validation.attributes.team_name'),
            'game_id'   => Lang::get('validation.attributes.game_id'),
            'team_description'  => Lang::get('validation.attributes.team_description'),
            'city'      => Lang::get('validation.attributes.city'),
            'team_logo' => Lang::get('validation.attributes.team_logo')
        ];
    }
}
