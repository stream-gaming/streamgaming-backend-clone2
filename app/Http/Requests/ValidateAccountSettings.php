<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateAccountSettings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //-- Validasi Edit Account Settings
            //- Melly
            'jenis_kelamin' => 'required|in:laki-laki,perempuan',
            'tgl_lahir'     => 'required|date_format: "Y-m-d"',
            'kota'          => 'required|exists:App\Model\CityModel,id_kab',
            'provinsi'      => 'required|exists:App\Model\ProvinceModel,id_prov',
            // 'nomor_hp'      => 'nullable|regex:/(0)[0-9]{9}/'
        ];
    }

    public function attributes()
    {
        return [
            'jenis_kelamin' => Lang::get('validation.attributes.jenis_kelamin'),
            'tgl_lahir'     => Lang::get('validation.attributes.tgl_lahir'),
            // 'nomor_hp'      => Lang::get('validation.attributes.nomor_hp'),
            'kota'          => Lang::get('validation.attributes.kota'),
            'provinsi'      => Lang::get('validation.attributes.provinsi'),
        ];
    }

    public function messages()
    {
        return [
            //- Jenis Kelamin
            'jenis_kelamin.required'    => Lang::get('validation.required'),
            'jenis_kelamin.in'          => Lang::get('validation.in'),
            //- Tgl Lahir
            'tgl_lahir.required'        => Lang::get('validation.required'),
            'tgl_lahir.date_format'     => Lang::get('validation.date_format'),
            //- Nomor Hp
            // 'nomor_hp.regex'             => Lang::get('validation.regex'),
        ];
    }
}
