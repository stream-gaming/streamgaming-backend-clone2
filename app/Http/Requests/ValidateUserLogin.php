<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateUserLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //- Validasi User Login
            'email'     => 'required|email',
            'password'  => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'email'         => Lang::get('validation.attributes.email'),
            'password'      => Lang::get('validation.attributes.password'),
        ];
    }

    public function messages()
    {
        return [
            //- Email
            'email.required' => Lang::get('validation.required'),
            'email.email'    => Lang::get('validation.email'),
            //- Password
            'password.required'  => Lang::get('validation.required'),

        ];
    }

    public function bodyParameters()
    {
        return [
            'email' => [
                'description' => 'Email user for login',
            ],
            'password' => [
                'description' => 'Password user for login',
            ],
        ];
    }
}
