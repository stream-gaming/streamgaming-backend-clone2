<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateCommentsTournament extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_status'     => 'required|in:user,admin',
            'stream_id'       => 'required_if:user_status,admin',
            // 'reply_id'        => 'exists:App\Model\CommentsModel,id',
            // 'parent_id'       => 'exists:App\Model\CommentsModel,id',
            'comment'         => 'required|max:1000',
        ];
    }

    public function attributes()
    {
        return [
            'user_status'     => Lang::get('validation.attributes.user_status'),
            'parent_id'        => Lang::get('validation.attributes.parent_id'),
            'reply_id'        => Lang::get('validation.attributes.reply_id'),
            'comment'        => Lang::get('validation.attributes.comment'),
        ];
    }

    public function messages()
    {
        return [
            //- Email
            'user_status.required' => Lang::get('validation.required'),
            //- Password
            'parent_id.required'  => Lang::get('validation.required'),
            'reply_id.required'  => Lang::get('validation.required'),
            'parent_id.required'  => Lang::get('validation.required'),

        ];
    }
}
