<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidatePostComment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment'   => 'required|max:255|string',
            'comment_id'    => 'nullable|string'
        ];
    }

    public function attributes()
    {
        return [
            'comment'         => Lang::get('validation.attributes.comment') 
        ];
    }

    public function messages()
    {
        return [
            'comment.required'        => Lang::get('validation.required'),
            'comment.max'             => Lang::get('validation.max.string'), 
        ];
    }
}
