<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateBanner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|string',
            'image'     => 'required|mimes:jpeg,jpg,png|max:2048',
            'status'    => 'required|in:0,1,2',
            'url'       => 'nullable|string',
            'in_app'    => 'required_with:url|in:0,1'
        ];
    }
}
