<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateUserSocialLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //-- Validasi Registrasti User
            //- Melly
            'fullname'      => 'required',
            'email'         => 'required',
            'link_image'    => 'required',
            'oauth_id'      => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'fullname'      => Lang::get('validation.attributes.fullname'),
            'email'         => Lang::get('validation.attributes.email'),
            'link_image'    => Lang::get('validation.attributes.link_image'),
            'oauth_id'      => Lang::get('validation.attributes.oauth_id'),
        ];
    }

    public function messages()
    {
        return [
            //- Full Name
            'fullname.required'         => Lang::get('validation.required'),
            //- Email
            'email.required'            => Lang::get('validation.required'),
            'link_image.required'       => Lang::get('validation.required'),
            'oauth_id.required'         => Lang::get('validation.required'),

        ];
    }
}
