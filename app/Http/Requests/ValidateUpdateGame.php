<?php

namespace App\Http\Requests;

use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\Lang;
use Illuminate\Foundation\Http\FormRequest;

class ValidateUpdateGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $Identity = IdentityGameModel::where('users_id', $this->user()->user_id)
                                    ->where('game_id', $this->route()->parameter('id'))
                                    ->first();
        return [
            'id_ingame'      => 'required|max:255|unique:App\Model\IdentityGameModel,id_ingame,' . $Identity->id,
            'username_ingame'=> 'required|max:255'
        ];
    }

    public function attributes()
    {
        return [
            'id_ingame'         => Lang::get('validation.attributes.id_ingame'),
            'username_ingame'   => Lang::get('validation.attributes.username_ingame')
        ];
    }

    public function messages()
    {
        return [
            'id_ingame.required'        => Lang::get('validation.required'),
            'username_ingame.required'  => Lang::get('validation.required'),
            'id_ingame.max'             => Lang::get('validation.max.string'),
            'username_ingame.max'       => Lang::get('validation.max.string'),
        ];
    }
}
