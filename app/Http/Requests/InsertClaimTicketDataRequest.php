<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertClaimTicketDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_id'  => 'required|exists:all_ticket,id',
            'max_claim'  => 'required|integer|min:1',
            'period'     => 'required|string|in:Daily,Weekly,Monthly',
            'event_start' => 'required|date|date_format:Y-m-d',
            'event_end'   => 'required|date|date_format:Y-m-d',
            'is_active'  => 'required|integer|in:0,1'
        ];
    }
}//keep
