<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\UrlGenerator;

class ValidateSettingTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'team_name'     => 'required|unique:App\Model\TeamModel,team_name,'.$this->team_id,
            'team_description'  => 'required|max:100',
            'city'              => 'required|exists:App\Model\CityModel,nama'
        ];
    }
}
