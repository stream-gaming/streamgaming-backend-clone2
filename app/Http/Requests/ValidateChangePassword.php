<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //- Validasi Change Password
            'current_password'  => 'required',
            'new_password'      => 'required|regex:/^(?=.*\d)(?=.*[a-zA-Z]).{1,}$/|min:8|confirmed',
        ];
    }
    public function attributes()
    {
        return [
            'current_password'     => Lang::get('validation.attributes.current_password'),
            'new_password'         => Lang::get('validation.attributes.new_password'),
        ];
    }
    public function messages()
    {
        return [
            'current_password.required' => Lang::get('validation.required'),
            'new_password.required'     => Lang::get('validation.required'),
            'new_password.min'          => Lang::get('validation.min.string'),
        ];
    }
}
