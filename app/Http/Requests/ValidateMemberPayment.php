<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateMemberPayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //- Validasi Member Payment
            'id_player_payment' => 'required',
            'type_payment'      => 'required|in:cash,point',
        ];
    }
    public function attributes()
    {
        return [
            'id_player_payment' => Lang::get('validation.attributes.id_player_payment'),
            'type_payment'      => Lang::get('validation.attributes.type_payment'),
        ];
    }
    public function messages()
    {
        return [
            'id_player_payment.required'    => Lang::get('validation.required'),
            'type_payment.required'         => Lang::get('validation.required'),
        ];
    }
}
