<?php

namespace App\Http\Requests;

use App\Rules\IsValidDate;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class ValidateUserRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //-- Validasi Registrasti User
            //- Melly
            'fullname'      => 'required|string|max:255',
            'username'      => 'required|regex:/^[a-zA-Z0-9_-]+$/u|max:255|unique:users,username',
            'email'         => 'required|email|max:255|unique:users,email',
            'password'      => 'required|regex:/^(?=.*\d)(?=.*[a-zA-Z]).{1,}$/|min:8|confirmed',
            'jenis_kelamin' => 'in:laki-laki,perempuan',
            'tgl_lahir'     => [
                'date_format: "Y-m-d"',
                'before:' . Carbon::now()->subYears(4),
                'after:' . Carbon::parse("1970-01-01"),
            ],
            'refferal_code' => 'exists:App\Model\User,refferal_code'
            // 'nomor_hp'      => 'nullable|regex:/(0)[0-9]{9}/'
        ];
    }

    public function attributes()
    {
        return [
            'fullname'      => Lang::get('validation.attributes.fullname'),
            'username'      => Lang::get('validation.attributes.username'),
            'email'         => Lang::get('validation.attributes.email'),
            'password'      => Lang::get('validation.attributes.password'),
            'jenis_kelamin' => Lang::get('validation.attributes.jenis_kelamin'),
            'tgl_lahir'     => Lang::get('validation.attributes.tgl_lahir'),
            // 'nomor_hp'      => Lang::get('validation.attributes.nomor_hp'),
        ];
    }

    public function messages()
    {
        return [
            //- Full Name
            'fullname.required'         => Lang::get('validation.required'),
            'fullname.string'           => Lang::get('validation.string'),
            'fullname.max'              => Lang::get('validation.max.string'),
            //- User Name
            'username.required'         => Lang::get('validation.required'),
            'username.regex'            => Lang::get('validation.regex'),
            'username.max'              => Lang::get('validation.max.string'),
            'username.unique'           => Lang::get('validation.unique'),
            //- Email
            'email.required'            => Lang::get('validation.required'),
            'email.email'               => Lang::get('validation.email'),
            'email.max'                 => Lang::get('validation.max.string'),
            'email.unique'              => Lang::get('validation.unique'),
            //- Password
            'password.required'         => Lang::get('validation.required'),
            'password.regex'            => Lang::get('validation.regex'),
            'password.min'              => Lang::get('validation.min.string'),
            'password.confirmed'        => Lang::get('validation.confirmed'),
            //- Jenis Kelamin
            'jenis_kelamin.required'    => Lang::get('validation.required'),
            'jenis_kelamin.in'          => Lang::get('validation.in'),
            //- Tgl Lahir
            'tgl_lahir.required'        => Lang::get('validation.required'),
            'tgl_lahir.date_format'     => Lang::get('validation.invalid_date_format'),
            'tgl_lahir.before'          => Lang::get('validation.before_4_years'),
            'tgl_lahir.after'          => Lang::get('validation.after_custom'),
            //- Nomor Hp
            // 'nomor_hp.regex'             => Lang::get('validation.regex'),

        ];
    }
}
