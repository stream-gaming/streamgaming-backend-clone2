<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BalanceHistoryModel extends Model
{
    protected $table    = 'balance_history';
    public $timestamps  = false;

	protected $fillable = ['transactions_id', 'users_id', 'date', 'type', 'transaction_type', 'balance', 'information', 'id_tournament'];


}
