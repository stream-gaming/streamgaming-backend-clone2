<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardTicketPrizeModel extends Model
{
    //
    protected $table	 	= 'leaderboard_ticketprize';
	protected $primaryKey 	= 'id_ticket_prize';

	public $incrementing 	= false;
    public $timestamps 		= false;



}
