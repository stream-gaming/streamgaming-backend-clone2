<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Jenssegers\Agent\Agent;

class AccessLogsUsersModel extends Model
{
    //- Tabel User
    protected $table = "access_logs_users";
    //- Primary Key Tabel User
    protected $primaryKey = 'user_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
    // Disabled Time Stamp
    public $timestamps = false;


    public static function addAccessLogsUsers($user_id, Request $request, $status)
    {
        $agent = new Agent();

        try {

            // Cari User berdasarkan email
            $log_users = AccessLogsUsersModel::where('user_id', $user_id)
                ->where('flag', $status)
                ->first();

            if ($log_users) {
                //- Update Log
                DB::table('access_logs_users')
                    ->where('user_id', $user_id)
                    ->where('flag', $status)
                    ->update([
                        'time'         => gmdate('Y-m-d H:i:s', mktime(date('H') + 7)),
                        'device'       => ($agent->isDesktop() ? 'Computer' : 'Phone'),
                        'ip'           => $request->getClientIp(),
                        'os'           => $agent->platform(),
                        'browser'      => $agent->browser(),
                        'brand'        => $agent->device(),
                        'count'        => $log_users->count + 1
                    ]);
                //- Pesan
                return response()->json([
                    'status' => true,
                    'message' => Lang::get('message.data.edit.success', [
                        'data' => 'Access Log Users',
                    ])
                ]);
            } else {
                // Initiate Class
                $log_users = new AccessLogsUsersModel();
                $log_users->user_id      = $user_id;
                $log_users->time         = gmdate('Y-m-d H:i:s', mktime(date('H') + 7));
                $log_users->device       = ($agent->isDesktop() ? 'Computer' : 'Phone');
                $log_users->ip           = $request->getClientIp();
                $log_users->os           = $agent->platform();
                $log_users->browser      = $agent->browser();
                $log_users->brand        = $agent->device();
                $log_users->count        = 1;
                $log_users->flag         = $status;
                $log_users->save();
                //- Pesan
                return response()->json([
                    'status'   => true,
                    'data'      => $log_users,
                    'message'   => Lang::get('message.data.add.success', [
                        'data' => 'Access Log Users',
                    ])
                ]);
            }
        } catch (\Exception $e) {
            //return error message gagal
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.data.add.failed', [
                    'data' => 'Access Log Users',
                ]),
                'error' => $e
            ], 409);
        }
    }
}
