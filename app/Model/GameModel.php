<?php

namespace App\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class GameModel extends Model
{
    protected $table        = 'game_list';
    protected $primaryKey   = 'id_game_list';
    public $incrementing    = false;
    protected $keyType      = 'string';
    public $timestamps = false;

    public function gamesOn($games_on)
    {
        switch ($games_on) {
            case 1:
                return 'PC';
                break;
            case 2:
                return 'Mobile';
                break;
            case 3:
                return 'Console';
                break;
            default:
                return $games_on;
        }
    }

    public function role()
    {
        return $this->hasMany(GameRoleModel::class, 'game_id', 'id_game_list');
    }

    public function players()
    {
        return $this->hasMany(IdentityGameModel::class, 'game_id');
    }

    public function countPlayers()
    {
        return $this->players->count();
    }

    public function identityGame()
    {
        return $this->belongsTo(IdentityGameModel::class, 'id_game_list', 'id_game');
    }

    public function leaderboard()
    {
        return $this->belongsTo(LeaderboardModel::class, 'game_competition');
    }

    public function total_tournament($id_game)
    {
        $total = AllTournamentModel::from('all_tournament AS a')
                    ->select(DB::raw("SUM(l.firstprize + l.secondprize + l.thirdprize) AS total_prizepool,
                                    COUNT(l.id_leaderboard) AS leaderboards,
                                    COUNT(t.id_tournament) AS bracket"))
                    ->leftJoin('leaderboard AS l', 'l.id_leaderboard', '=', 'a.id_tournament')
                    ->leftJoin('tournament AS t', 't.id_tournament', '=', 'a.id_tournament')
                    ->where('l.game_competition', $id_game)
                    ->orWhere('t.kompetisi', $id_game)
                    ->first();

        return $total;
    }

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'game_id', 'id_game_list');
    }
}
