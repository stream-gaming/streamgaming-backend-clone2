<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamPlayerModel extends Model
{
    use SoftDeletes;

    protected $table = 'team_player';
    protected $primaryKey = 'team_id';
    public $incrementing = false;
    protected $keyType = 'string';


    public function getByLeaderboard($leaderboard, $user)
    {
        return $this->leftJoin('team', function ($join) {
            $join->on('team_player.team_id', '=', 'team.team_id');
        })
            ->leftJoin('game_list', 'team.game_id', '=', 'game_list.id_game_list')
            ->where('team_player.player_id', 'LIKE', '%' . $user->user_id . '%')
            ->where('team.game_id', $leaderboard->game_competition)
            ->where('team_player.deleted_at', '')
            ->first();
    }
    public function ExplodeTeam($team_player_id)
    {
        $data = explode(',', $team_player_id);
        return $data;
    }

    public function team()
    {
        return $this->belongsTo(TeamModel::class,'team_id','team_id');
    }

}



