<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MobileVersion extends Model
{
    protected $fillable = [
        'appId',
        'versionName',
        'versionCode',
        'recommendUpdate',
        'forceUpdate',
        'changeLogs',
    ];
}
