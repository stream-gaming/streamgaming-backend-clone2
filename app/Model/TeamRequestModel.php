<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TeamRequestModel extends Model
{
	use SoftDeletes;

	protected $table = 'team_request';
	protected $primaryKey = 'request_id';
	public $incrementing = false;
	protected $keyType = 'string';

	public function countRequestByTeamId($team_id)
	{
		return $this->where('team_id', $team_id)
			->where('status', 1)
			->count();
    }

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'team_id', 'team_id');
    }
}
