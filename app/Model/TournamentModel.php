<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Claims\Custom;

class TournamentModel extends Model
{
	use SoftDeletes;

	protected $table        = 'tournament';
	protected $primaryKey   = 'id_tournament';
    protected $keyType      = 'string';
    public $timestamps      =  false;

    public function game()
    {
        return $this->hasOne(GameModel::class,'id_game_list','kompetisi');
    }

    public function AllTeam()
    {
        return $this->hasMany(TournamenGroupModel::class,'id_group','id_group');
    }

    public function prize()
    {
        return $this->hasMany(BracketPrizePoolModel::class,'id_winner_sequence','id_winner_sequence');
    }
    public function prizeTicket(){
        $select = [
            'tournament_ticket_sequence.urutan AS orders',
            'tournament_ticket_sequence.given_ticket AS amount_given'
        ];
        return $this->hasMany(BracketPrizeTicketModel::class,'id_ticket_squence','id_ticket_squence');
    }
    public function tournamentStatus($status)
    {
        switch ($status) {
            case 'pending':
                return [
                    'number'    => 0,
                    'text'      => Lang::get('tournament.status.incoming')
                    ];
                break;
            case 'open_registration':
                return [
                    'number'    => 1,
                    'text'  	=> Lang::get('tournament.status.open_registration')
                    ];
                break;
            case 'end_registration':
                return [
                    'number'    => 2,
                    'text'      => Lang::get('tournament.status.close_registration')
                    ];
                break;
            case 'starting':
                return [
                    'number'    => 3,
                    'text'      => Lang::get('tournament.status.in_progress')
                    ];
                break;
            case 'complete':
                return [
                    'number'    => 4,
                    'text'      => Lang::get('tournament.status.complete')
                    ];
                break;
            case 'canceled':
                return 'canceled';
                break;
            default:
                return $status;
                break;
        }
    }
    public function winnerTour(){
        $select = [
            'tournament_winner.id_tournament',
            'tournament_winner.id_winner',
            'tournament_winner.juara1',
            'tournament_winner.juara2',
            'tournament_winner.juara3',
            'player_payment.id_team',
            'player_payment.player_id',
            'balance_history.transaction_type',
            'balance_history.balance',
            'balance_history.date'
        ];

        return $this->hasmany(TournamentWinnerModel::class,'id_tournament','id_tournament')->select($select);
    }
    public function winnerTourSequence(){

        return $this->hasmany(tournamentWinnnerSequence::class,'id_winner_sequence','id_winner_sequence');
    }
    public function CustomPrize(){
        return $this->hasmany(CustomPrizesModel::class,'tournament_id','id_tournament');
    }
}