<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use function Clue\StreamFilter\fun;

class TeamModel extends Model
{
	use SoftDeletes;

	protected $table = 'team';
	protected $primaryKey = 'team_id';
	public $incrementing = false;
	protected $keyType = 'string';

    protected $fillable = [
        'game_id',
        'leader_id',
        'team_name',
        'team_description',
        'team_logo',
        'venue',
        'is_banned',
        'ban_until',
        'reason_ban',
    ];

	public function game()
	{
		return $this->hasOne(GameModel::class, 'id_game_list', 'game_id');
	}

	public function leaderboardTeam()
	{
		return $this->belongsTo(LeaderboardTeamModel::class, 'id_team_leaderboard', 'team_id');
	}

	public function identityInGameLeader()
	{
		return $this->hasOne(IdentityGameModel::class, 'users_id', 'leader_id');
	}

	public function leader()
	{
		return $this->hasOne(User::class, 'user_id', 'leader_id');
	}

	public function player()
	{
		return $this->hasOne(TeamPlayerModel::class, 'team_id');
	}

	public function allPlayer()
	{
		return collect($this->primaryPlayer())->merge($this->substitutePlayer())->all();
	}

	public function primaryPlayer()
	{
		return (empty($this->player->player_id)) ? [] : explode(',' ,$this->player->player_id);
	}

	public function substitutePlayer()
	{
		return (empty($this->player->substitute_id)) ? [] : explode(',' ,$this->player->substitute_id);
	}

	public function countRequestByLeaderTeam()
	{
		return $this->join('team_request', 'team.team_id', '=', 'team_request.team_id')
			->where('leader_id', (Auth::user())->user_id)
			->where('status', 1)
			->count();
	}

	public function countMember()
	{
		return collect($this->primaryPlayer())->count()+collect($this->substitutePlayer())->count();
	}

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d H:i:s', strtotime($value));
    }

    public function isFull()
    {
        return ($this->countMember() >= ($this->game->player_on_team + $this->game->substitute_player)) ? true : false;
    }

    public function isPrimaryPlayer($users_id = null)
    {
        if (is_null($users_id)) {
            return in_array(Auth::user()->user_id, $this->primaryPlayer());
        } else {
            return in_array($users_id, $this->primaryPlayer());
        }
    }

    public function isSubstitutePlayer($users_id = null)
    {
        if (is_null($users_id)) {
            return in_array(Auth::user()->user_id, $this->substitutePlayer());
        } else {
            return in_array($users_id, $this->substitutePlayer());
        }
    }
    public function getLeaderTeam($leader_id, $game_id)
    {
        try {
            //code...

        return $this->leftJoin('team_player', function ($join) {
            $join->on('team_player.team_id', '=', 'team.team_id');
        })
            ->where('team.leader_id', $leader_id)
            ->where('team.game_id', $game_id)
            ->firstOrFail(['team_player.player_id']);
        } catch (\Throwable $th) {
            return false;
        }
        // return $game_id;
    }

    public function request_join()
    {
        return $this->hasOne(TeamRequestModel::class, 'request_id', 'team_id');
    }

	public function lastMessage()
	{
		return $this->hasOne(MessageTeamModel::class, 'team_id');
	}

	public function messages()
	{
		return $this->hasMany(MessageTeamModel::class, 'team_id');
	}

    public function playerNew()
    {
        return $this->hasMany(TeamPlayerNewModel::class, 'team_id');
    }

    public function role($user_id)
    {
        if ($this->leader_id == $user_id) {
            $role = 1; //- Leader
        } else if ($this->isPrimaryPlayer($user_id)) {
            $role = 2; //- Pemain Utama
        } else if ($this->isSubstitutePlayer($user_id)) {
            $role = 3; // Pemain cadangan
        } else {
            $role = null; //- Kalau Gajelas
        }
        return $role;
    }

    public function nameHistory()
    {
        return $this->hasMany(TeamNameHistoryModel::class,'team_id','team_id');
    }
    public function pengurutanTeamPrimary(){
        $dataPlayer = $this->primaryPlayer();
        $data = [];
        foreach ($dataPlayer as $key => $val) {
            if( $val == $this->leader->user_id AND $key > 0){
                $data[$key] = $data[0];
                $data[0] = $val;
            }else{
                $data[$key] = $val;
            }
        }
        return $data;
    }
    public function pengurutanAllTeam(){
        $dataPlayer = $this->allPlayer();
        $data = [];
        foreach ($dataPlayer as $key => $val) {
            if( $val == $this->leader->user_id AND $key > 0){
                $data[$key] = $data[0];
                $data[0] = $val;
            }else{
                $data[$key] = $val;
            }
        }
        return $data;
    }

    public function invitationCode()
    {
        return $this->hasOne(InvitationCode::class, 'team_id', 'team_id');
    }

    static public function boot()
    {
        parent::boot();

        static::deleting(function($team) {
            $team->invitationCode()->delete();
        });
    }
}
