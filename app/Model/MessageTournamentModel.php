<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageTournamentModel extends Model
{
    use SoftDeletes;

    protected $table = 'message_tournaments';

    protected $fillable = [
        'user_id',
        'room_id',
        'team_id',
        'username',
        'message',
        'attachment',
        'user_read',
        'user_status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(Management::class, 'user_id', 'stream_id');
    }

    public function detail_user()
    {
        return $this->hasOne(UsersDetailModel::class, 'user_id', 'user_id');
    }

    public function detail_admin()
    {
        return $this->hasOne(ManagementDetailModel::class, 'access_id', 'user_id');
    }

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'team_id', 'team_id');
    }
}
