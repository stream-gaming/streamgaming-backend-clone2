<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionModel extends Model
{
    use SoftDeletes;
    protected $table = 'promotion';
    protected $guarded = [];

    public function user_promotion()
    {
        return $this->hasOne(UserPromotionModel::class, 'promotion_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_promotion', 'promotion_id', 'user_id')
        ->select('users.user_id');
    }
}
