<?php

namespace App\Model;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Database\Eloquent\Model;

class PrivateChatModel extends Model
{
	protected $table = 'private_chat';  

	const CREATED_AT = 'timestamp';
    const UPDATED_AT = 'timestamp';

    public function create($data)
    {
    	try {
    		$PrivateChatModel = new PrivateChatModel;
    		$PrivateChatModel->secret_code = uniqid();
    		$PrivateChatModel->type = 'team';
    		$PrivateChatModel->users = Auth::user()->user_id;
    		$PrivateChatModel->is_read = 0;
    		$PrivateChatModel->status = 1;
    		$PrivateChatModel->save();
    	} catch (\Exception $e) {
    		return response()->json([
                'status'    => false,
                'message'   => Lang::get('message.private_chat.add.failed'),
                'error'     => $e
            ], 409);
    	}
    }
}
