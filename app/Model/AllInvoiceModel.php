<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllInvoiceModel extends Model
{
    // use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'all_invoice';

    public function getBracket(){
        $select = [
            'id_tournament',
            'tournament_name',
            'kompetisi',
            'game_list.name'
        ];
        return $this->hasOne(TournamentModel::class,'id_tournament','id_tournament')->select($select);
    }
    public function getLeaderboard(){
        $select = [
                'id_leaderboard',
                'tournament_name',
                'game_competition',
                'game_list.name',
        ];
        return $this->hasOne(LeaderboardModel::class,'id_leaderboard','id_tournament')->select($select);
    }
    public function getTourBracket(){
        return $this->hasOne(TournamentModel::class,'id_tournament','id_tournament')->where('system_tournament',5)->first();
    }
}
