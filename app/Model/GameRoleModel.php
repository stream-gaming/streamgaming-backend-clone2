<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GameRoleModel extends Model
{
    protected $table      = 'game_role';
    protected $primaryKey = 'id';

    public function game()
    {
        return $this->hasOne(GameModel::class, 'id_game_list', 'game_id');
    }
}
