<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryBroadcastTicketModel extends Model
{
    //
    protected $table = 'history_broadcast_ticket';

}
