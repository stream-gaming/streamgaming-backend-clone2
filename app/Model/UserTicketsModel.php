<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTicketsModel extends Model
{
    protected $table        = 'ticket_users';

    public function ticket($userId)
    {
        return $this->selectRaw('ticket_users.*,ticket.*')
                    ->join('ticket','ticket.id_ticket','=','ticket_users.id_ticket')
                    ->where('id_users',$userId)->get();
    }
}
