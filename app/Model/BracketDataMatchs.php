<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BracketDataMatchs extends Model
{
    protected $table = 'tournament_data_matchs';
	protected $primaryKey 	= 'id_data_matchs';
	protected $keyType 		= 'string';
    public $timestamps = false;

    public function scoreMatchs()
    {
        return $this->hasMany(BracketScoreMatchs::class,'id_data_matchs','id_data_matchs');
    }
}
