<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class UsersDetailModel extends Model
{
    //- Tabel User
    protected $table = "users_detail";
    //- Primary Key Tabel User
    protected $primaryKey = 'user_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
    // Disabled Time Stamp
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'id_card',
        'id_card',
        'image',
        'link_image',
        'id_status',
    ];


    public static function addUsersDetail($data)
    {
            // Initiate Class
            $users_detail = new UsersDetailModel();
            $users_detail->user_id         = $data->user_id;
            $users_detail->id_card         = '';
            $users_detail->id_card_number  = '';
            $users_detail->image           = '';
            $users_detail->link_image      = $data->input('link_image', '');
            $users_detail->id_status       = 0;
            $users_detail->save();

           return $users_detail;
    }
}
