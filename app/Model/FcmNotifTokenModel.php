<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FcmNotifTokenModel extends Model
{
    protected $table = 'fcm_notif_token';

    protected $fillable = [
        'user_id',
        'token',
        'device',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','user_id');
    }
}
