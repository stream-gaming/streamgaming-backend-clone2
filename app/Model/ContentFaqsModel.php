<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContentFaqsModel extends Model
{
    protected $table      = 'content_faqs';
    protected $primaryKey = 'id';

    public function faqs()
    {
        return $this->belongsTo(FaqsModel::class, 'id_faqs', 'id_faqs');
    }
}
