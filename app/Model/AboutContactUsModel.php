<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutContactUsModel extends Model
{
    use SoftDeletes;

    protected $table = 'user_guest_message';

    protected $fillable = [
        'name',
        'email',
        'message'
    ];
}
