<?php

namespace App\Model;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class ScoreMatchsModel extends Model
{
    use Compoships;
    protected $table = "tournament_score_matchs";
	protected $primaryKey 	= 'id_score_matchs';
	protected $keyType 		= 'string';

    public $timestamps = false;

    public function ScoreMatchs_User($id_team,$id_user){
    $getdata = $this->selectRaw('SUM(tournament_score_matchs.kills) AS kills, SUM(tournament_score_matchs.death) AS death ')
                    ->where('id_team',$id_team)
                    ->where('player_id',$id_user)
                    ->first();
        return
        [
            'kills' => (int) $getdata->kills,
            'death' => (int) $getdata->death
        ];
    }

    public function tournamentGroup()
    {
        return $this->hasOne(TournamenGroupModel::class,'id_team','id_team');
    }

    public function tournamentGroupPlayer()
    {
        return $this->hasOne(TournamentGroupPlayerModel::class,['id_player','id_team'],['player_id','id_team']);
    }
}

