<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BracketPrizePoolModel extends Model
{
    protected $table = 'tournament_winner_sequence';
}
