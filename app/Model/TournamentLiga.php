<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentLiga extends Model
{
    protected $table        = 'tournament_liga';
    protected $primaryKey   = 'id_liga';
    public $incrementing    = false;
    protected $keyType      = 'string';
    public $timestamps      = false;


    public function tournament()
    {
        return $this->hasMany(TournamentListModel::class,'id_liga','id_liga');
    }

    public function game()
    {
        return $this->hasOne(GameModel::class,'id_game_list','kompetisi');
    }

    public function tabmenu()
    {
        return $this->hasMany(TournamentMenuLiga::class,'id_tab_liga','id_tab_liga');
    }

}
