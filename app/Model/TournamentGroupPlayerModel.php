<?php

namespace App\Model;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class TournamentGroupPlayerModel extends Model
{
    use Compoships;
    //

    protected $table = 'tournament_group_player';
    protected $primaryKey = null;

    public $timestamps = false;
    public $incrementing = false;
}
