<?php

namespace App\Model;

use App\Model\IdentityGameModel;
use App\Notifications\ResetPassword;
use App\Traits\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,
        MustVerifyEmail;

    //- Tabel User
    protected $table = "users";
    //- Primary Key Tabel User
    protected $primaryKey = 'user_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'fullname',
        'username',
        'jenis_kelamin',
        'password',
        'email',
        'tgl_lahir',
        'alamat',
        'bio',
        'provinsi',
        'kota',
        'status',
        'is_verified',
        'is_active',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'alamat','provinsi', 'kota', 'nomor_hp'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'user' => $this,
        ];
    }

    public function addUser($request, $data)
    {
        // Registrasi User
        $user = new User();
        $user->user_id          = $request->user_id;
        $user->fullname         = $request->input('fullname');
        $user->username         = $request->input('username');
        $plainPassword          = $request->input('password');
        $user->jenis_kelamin    = $request->input('jenis_kelamin');
        $user->email            = $request->input('email');
        $user->tgl_lahir        = $request->input('tgl_lahir');
        $user->alamat           = $request->input('alamat');
        $user->bio              = $request->input('bio');
        $user->provinsi         = $request->input('provinsi');
        $user->kota             = $request->input('kota');
        // $user->nomor_hp         = $request->input('nomor_hp');
        $user->status           = $data['status'];
        $user->is_verified      = $data['is_verified'];
        $user->is_active        = 1;
        $user->created_at       = gmdate('Y-m-d H:i:s', mktime(date('H') + 7));
        $user->updated_at       = '';
        $user->password         = password_hash($plainPassword, PASSWORD_DEFAULT);
        $user->save();

        return $user;
    }

    public function editAccount($request)
    {
        // Edit Account
        $user                   = User::findOrFail(Auth::user()->user_id);
        // $user->nomor_hp         = $request->input('nomor_hp');
        $user->tgl_lahir        = $request->input('tgl_lahir');
        $user->jenis_kelamin    = $request->input('jenis_kelamin');
        $user->provinsi         = $request->input('provinsi');
        $user->kota             = $request->input('kota');
        $user->alamat           = $request->input('alamat');
        $user->updated_at       = gmdate('Y-m-d H:i:s', mktime(date('H') + 7));
        $user->save();

        return $user;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        //Tambah Log
        $data = [
            'email' => $this->email,
            'type' => 'reset'
        ];
        (new SendMailLogModel())->addLogMail($data);
        //- Kirim Email
        $this->notify(new ResetPassword($token));
    }

    public function socmed()
    {
        return $this->hasMany(SocmedModel::class, 'users_id');
    }

    public function getTglLahirAttribute($value)
    {
        $value = empty($value) ? '' : date('Y-m-d', strtotime($value));
        return $value;
    }

    public function game()
    {
        return $this->hasMany(IdentityGameModel::class, 'users_id', 'user_id');
    }

    public function gameRole()
    {
        return $this->hasMany(GameUsersRoleModel::class, 'user_id', 'user_id');
    }

    public function isFriend($friend_id)
    {
        return $this->hasMany(UsersFriendModel::class, 'users_id', 'user_id')
                    ->where('friend_id', $friend_id)
                    ->count() > 0 ?
                    true : false;
    }

    public function province()
    {
        return $this->hasOne(ProvinceModel::class,'id_prov','provinsi');
    }

    public function city()
    {
        return $this->hasOne(CityModel::class,'id_kab','kota');
    }

    public function pin()
    {
        return $this->hasOne(AuthenticateModel::class,'users_id','user_id');
    }

    public function detail()
    {
        return $this->hasOne(UsersDetailModel::class, 'user_id');
    }

    public function balance()
    {
        return $this->hasOne(BalanceUsersModel::class, 'users_id', 'user_id');
    }

    public function authenticate()
    {
        return $this->hasOne(AuthenticateModel::class, 'users_id', 'user_id');
    }

    public function usersGame()
    {
        return $this->hasOne(UsersGameModel::class, 'users_id', 'user_id');
    }

    public function preferences()
    {
        return $this->hasOne(UserPreferencesModel::class, 'user_id', 'user_id');
    }

    public function oAuth()
    {
        return $this->hasOne(OAuthModel::class, 'users_id', 'user_id');
    }

    public function messageTeam()
    {
        return $this->hasMany(MessageTeamModel::class, 'user_id','user_id');
    }

    public function firebaseToken()
    {
        return $this->hasMany(ForumFirebaseToken::class, 'user_id', 'user_id');
    }

    public function fcmFirebaseToken()
    {
        return $this->hasMany(FcmNotifTokenModel::class, 'user_id', 'user_id');
    }

    public function userOnboarding(){
        return $this->hasOne(UserOnboardingModel::class, 'user_id', 'user_id');
    }

    public function userPromotions()
    {
        return $this->belongsToMany(PromotionModel::class, 'user_promotion', 'user_id', 'promotion_id')
        ->withPivot('action_read', 'action_delete');
    }
}
