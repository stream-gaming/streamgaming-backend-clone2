<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class LeaderboardModel extends Model
{
	use SoftDeletes;

	protected $table 		= 'leaderboard';
	protected $primaryKey 	= 'id_leaderboard';
    protected $keyType 		= 'string';
    protected $guarded = [];
    public $timestamps = false;
    public $incrementing = false;

	public function game()
	{
		return $this->hasOne(GameModel::class, 'id_game_list', 'game_competition');
	}

	public function stage()
	{
		return $this->hasMany(LeaderboardQualificationModel::class, 'id_leaderboard', 'id_leaderboard');
	}

	//- Checking if this user has already been registered on other tournament
	public function cekTour($tourDate, $user)
	{
		return PlayerPayment::where('player_payment.player_id', $user->user_id)
			->leftJoin('tournament', 'player_payment.id_tournament', '=', 'tournament.id_tournament')
			->leftJoin('leaderboard', 'player_payment.id_tournament', '=', 'leaderboard.id_leaderboard')
			->where('player_payment.status', '!=', 'Cancelled')
			->where(DB::raw("( ( leaderboard.tournament_date = '$tourDate' AND leaderboard.status_tournament < 4 ) OR ( ( tournament.date = '$tourDate' AND tournament.status != 'complete' ) ) )"))
			->groupBy('player_payment.player_id')
			->count();
	}

	public function cekLeaderboard($url, $group_sequence)
	{
		return $this->leftJoin('leaderboard_group as lg', 'leaderboard.id_leaderboard', '=', 'lg.id_leaderboard')
			->where('leaderboard.url', $url)
			->where('lg.group_sequence', $group_sequence)
			->first();
	}

	public function leaderboardInfo($url)
	{
		return $this->where('leaderboard.url', $url)
			->with('game')
			->first();
	}

    public function AvailableSlots()
    {
        return $this->hasOne(LeaderboardQualificationModel::class,'id_leaderboard');
    }

    public function tournamentStatus($status)
    {
        switch ($status) {
            case '0':
                return [
                    'number'    => 0,
                    'text'      => Lang::get('tournament.status.incoming')
                    ];
                break;
            case '1':
                return [
                    'number'    => 1,
                    'text'  	=> Lang::get('tournament.status.open_registration')
                    ];
                break;
            case '2':
                return [
                    'number'    => 2,
                    'text'      => Lang::get('tournament.status.close_registration')
                    ];
                break;
            case '3':
                return [
                    'number'    => 3,
                    'text'      => Lang::get('tournament.status.in_progress')
                    ];
                break;
            case '4':
                return [
                    'number'    => 4,
                    'text'      => Lang::get('tournament.status.complete')
                    ];
                break;
            case '5':
                return 'cancel';
                break;
            default:
                return $status;
                break;
        }
    }

    public function getGroup(){

        return $this->hasMany(LeaderboardGroupModel::class,'id_leaderboard');
    }


}
