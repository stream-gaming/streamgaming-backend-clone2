<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FaqsModel extends Model
{
    protected $table      = 'faqs';
    protected $primaryKey = 'id_faqs';
    protected $keyType    = 'string';

    public function msfaqs()
    {
        return $this->belongsTo(MsFaqsModel::class, 'id_ms_faqs', 'id');
    }
    public function contentFaqs(){
        return $this->hasMany(ContentFaqsModel::class, 'id_faqs','id_faqs');
    }
    public function MenuFaqs(){
        return $this->hasMany(MenuFaqsModel::class, 'id_menu_faqs','id_menu_faqs');
    }
    public function SubMenuFaqs(){
        return $this->hasOne(SubMenuFaqsModel::class, 'id_faqs','id_faqs');
    }
}
