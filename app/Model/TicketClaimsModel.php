<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class TicketClaimsModel extends Model
{
    use SoftDeletes;

    protected $table        = 'ticket_claims';

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeCurrent($query)
    {
        return $query->where('ticket_claims.event_start', '<=', Carbon::now()->format('Y-m-d'))
            ->where('ticket_claims.event_end', '>=', Carbon::now()->format('Y-m-d'));
    }

    public function ticket()
    {
        return $this->belongsTo(AllTicketModel::class, 'ticket_id', 'id');
    }

    public function claimDate()
    {
        $date = Carbon::now();
        if ($this->period == "Daily") {
            $data = Carbon::now()->format('Y-m-d H:i');
        } else if ($this->period == "Weekly") {
            $data = $date->endOfWeek()->format('Y-m-d H:i');
        } else if ($this->period == "Monthly") {
            $data = $date->endOfMonth()->format('Y-m-d H:i');
        }

        if ($data > $this->event_end) {
            $data = date('Y-m-d H:i', strtotime($this->event_end));
        }
        return $data;
    }

    public function claimJunk()
    {
        $date = Carbon::now();
        if ($this->period == "Daily") {
            $data = Carbon::now()->yesterday()->format('Y-m-d H:i');
        } else if ($this->period == "Weekly") {
            $data = $date->startOfWeek(Carbon::SUNDAY)->format('Y-m-d H:i');
        } else if ($this->period == "Monthly") {
            $date = new Carbon('last day of last month');
            $data = $date->format('Y-m-d H:i');
        }

        if ($data > $this->event_end) {
            $data = date('Y-m-d H:i', strtotime($this->event_end));
        }
        return $data;
    }
}
//keep
