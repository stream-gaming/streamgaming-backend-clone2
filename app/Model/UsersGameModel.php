<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class UsersGameModel extends Model
{
    //- Tabel User
    protected $table = "users_game";
    //- Primary Key Tabel User
    protected $primaryKey = 'users_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
     // Disabled Time Stamp
     public $timestamps = false;

    protected $fillable = [
        'users_id',
        'game_id',
    ];


    public static function addUsersGame($data)
    {

            // Initiate Class
            $users_game = new UsersGameModel();
            $users_game->users_id   = $data->user_id;
            $users_game->game_id    = '';
            $users_game->save();
            
            //return respons berhasil
           return $users_game;
    } 
}
