<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardTeamPlayerModel extends Model
{
    //
    protected $table 		= 'leaderboard_team_player';
	protected $primaryKey 	= null;
    public $timestamps = false;
    public $incrementing = false;
}
