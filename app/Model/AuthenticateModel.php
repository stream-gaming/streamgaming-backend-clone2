<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class AuthenticateModel extends Model
{
    //- Tabel User
    protected $table = "authenticate";
    //- Primary Key Tabel User
    protected $primaryKey = 'users_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
     // Disabled Time Stamp
     public $timestamps = false;

    protected $fillable = [
        'users_id',
        'authenticate',
        'activity',
    ];

    public static function addAuthenticate($data)
    {

            // Initiate Class
            $authenticate = new AuthenticateModel();
            $authenticate->users_id        = $data->user_id;
            $authenticate->authenticate    = '';
            $authenticate->activity        = 1;
            $authenticate->save();

            //return respons berhasil
         return $authenticate;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','users_id');
    }
}
