<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'kabupaten';
	protected $primaryKey = 'id_kab';
    protected $keyType = "string";

    public $timestamps = false;
}
