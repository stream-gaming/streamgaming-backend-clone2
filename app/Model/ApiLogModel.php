<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApiLogModel extends Model
{
	protected $table = 'api_logs';
	protected $guarded = []; 
}
