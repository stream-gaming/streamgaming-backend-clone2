<?php

namespace App\Model;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class ListMvpModel extends Model
{
    protected $table = "list_mvp";
    protected $guard = ['id'];
    
    public function tournament()
    {
        return $this->belongsTo(TournamentModel::class,'id_tournament','tournament_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','user_id');
    }

    public function team()
    {
        return $this->belongsTo(TeamModel::class,'team_id','team_id');
    }
}
