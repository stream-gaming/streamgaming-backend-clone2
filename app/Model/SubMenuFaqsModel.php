<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubMenuFaqsModel extends Model
{
    protected $table      = 'sub_menu_faqs';
    protected $primaryKey = 'id';

    public function MenuFaqs()
    {
        return $this->belongsTo(MenuFaqsModel::class, 'id_menu', 'id_menu');
    }
    public function faqs()
    {
        return $this->hasOne(FaqsModel::class, 'id_faqs', 'id_faqs');
    }
}
