<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MessageTeamModel extends Model
{
    protected $table = 'message_teams';

protected $fillable = [
        'user_id',
        'team_id',
        'username',
        'message',
        'attachment',
        'user_read'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','user_id');
    }

    public function detailUser()
    {
        return $this->hasOne(UsersDetailModel::class,'user_id','user_id');
    }

    public function userInTeamPlayer()
    {
        return $this->hasOne(TeamPlayerNewModel::class, 'player_id', 'user_id');
    }
}
