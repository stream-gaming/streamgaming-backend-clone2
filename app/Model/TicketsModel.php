<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketsModel extends Model
{
    protected $table = 'tickets';
    protected $fillable = ['is_used'];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'team_id', 'team_id');
    }
}
