<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrizeInventoryHistoryModel extends Model
{
    //
    protected $table = 'prize_inventory_history';
}
