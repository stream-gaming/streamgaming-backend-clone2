<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BracketPrizeTicketModel extends Model
{
    protected $table = 'tournament_ticket_sequence';

    public function ticketNew()
    {
        return $this->hasOne(AllTicketModel::class,'id','id_ticket_given');
    }
    public function ticketOld(){
        return $this->hasOne(TicketModel::class,'id_ticket','id_ticket_given');
    }
}