<?php

namespace App\Model;

use Illuminate\Support\Facades\Auth; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class NewsModel extends Model
{
	protected $table = 'news_game'; 
	protected $primaryKey = 'id_news_game';
	public $incrementing = false;
	protected $keyType = 'string';
    public $timestamps = false; 

    protected $columns = array('id_news_game','judul','penulis', 'label', 'gambar', 'isi', 'tag', 'url', 'views', 'komentar', 'created_at', 'updated_at'); // add all columns from you table

    public function scopeExclude($query,$value = array()) 
    {
        return $query->select( array_diff( $this->columns,(array) $value) );
    } 

	public function label()
    {
        return $this->hasOne(LabelModel::class, 'id_kategori', 'label');
    }

    public function author()
    { 
        return $this->hasOne(Management::class, 'stream_id', 'penulis');
    } 

    public function authorDetail()
    {
        return $this->hasOne(ManagementDetailModel::class, 'access_id', 'penulis');        
    }  

    public function comment()
    {
        return $this->hasMany(NewsCommentModel::class, 'news_id', 'id_news_game')
                                ->where('reply_comment_id', NULL)
                                ->latest();
    }

    public function like()
    {
        return $this->hasMany(NewsLike::class, 'news_id', 'id_news_game')
                        ->count();
    } 

    public function is_like()
    {
        return $this->hasMany(NewsLike::class, 'news_id', 'id_news_game')
                        ->where('users_id', Auth::user()->user_id)
                        ->count() > 0 ?
                        true : false 
                        ; 
    }
}