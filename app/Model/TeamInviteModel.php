<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TeamInviteModel extends Model
{
	use SoftDeletes;

	protected $table 		= 'team_invite';
	protected $primaryKey 	= 'invite_id';
	public $incrementing 	= false;
	protected $keyType 		= 'string';

	public function inviteByUserLogin()
	{
		$count = $this->where('users_id', (Auth::user())->user_id)
            // ->where('status', 1)
            ->orderByRaw('status = 0, status')
			->get();

		return $count;
    }

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'team_id', 'team_id');
    }
}
