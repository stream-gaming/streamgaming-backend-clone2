<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentTicketSequenceModel extends Model
{
    protected $table        = 'tournament_ticket_sequence';
	protected $primaryKey   = 'id';
    public $timestamps      = false;
}
