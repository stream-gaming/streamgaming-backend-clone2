<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class TournamentListModel extends Model
{
	// use SoftDeletes;

	protected $table 		= 'list_tournament';
	protected $primaryKey 	= 'id_tournament';
	protected $keyType 		= 'string';

	public function game()
	{
		return $this->hasOne(GameModel::class, 'id_game_list', 'game_competition');
    }

    public function AvailableSlots()
    {
        return $this->hasOne(LeaderboardQualificationModel::class,'id_leaderboard','id_tournament');
    }

    public function liga()
    {
        return $this->belongsTo(TournamentLiga::class,'id_liga','id_liga');
    }

}
