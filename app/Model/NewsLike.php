<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsLike extends Model
{
	protected $table = 'news_like';  
	public $incrementing = false;
	protected $keyType = 'string';
    // public $timestamps = false; 
    // protected $dateFormat = 'Y-m-d';
    const CREATED_AT = 'date';
    const UPDATED_AT = 'date';

    public function getDateAttribute($value)
    {
        return date('Y-m-d H:i:s', strtotime($value)); 
    }
}
