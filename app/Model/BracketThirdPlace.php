<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Validation\AutoCodeIdRound;
class BracketThirdPlace extends Model
{
    protected $table = 'tournament_third_place';
    public $timestamps   = false;

    public function getBracketAfter($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_third_place.id_third_place,tournament_third_place.team1,tournament_third_place.team2,tournament_third_place.score1,tournament_third_place.score2,tournament_third_place.winner,tournament_third_place.date,tournament_third_place.id_data_kill')
                    ->leftJoin('tournament as t','tournament_third_place.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_semi_finals AS A','tournament_third_place.team1','=','A.id_semi_finals')
                    ->leftJoin('tournament_semi_finals AS B','tournament_third_place.team2','=','B.id_semi_finals')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('A.lose','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('B.lose','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_third_place.id_tournament','=',$id_tournament)
                    ->orderBy('id_third_place','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function thirdWinner($id_tournament)
    {
        $get = $this->selectRaw('C.id_team AS id_team1,D.id_team AS id_team2,C.nama_team AS team_1,C.logo_team AS logo_1,C.nama_kota AS nama_kota_1,C.id_team AS id_team1,tournament_third_place.date')
                    ->leftJoin('tournament as t','tournament_third_place.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS C',function($join){
                        $join->on('tournament_third_place.winner','=','C.id_team')
                            ->on('C.id_group','=','t.id_group');
                    })
                    ->join('tournament_group AS D',function($join){
                        $join->on('tournament_third_place.winner','=','D.id_team')
                            ->on('D.id_group','=','t.id_group');
                    })
                    ->where('tournament_third_place.id_tournament','=',$id_tournament)
                    ->first();

        if($get == null || $get->count() == 0 ){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getTeamInfo($id_round)
    {
        $get =  $this->selectRaw('a.id_team as team1, b.id_team as team2, a.nama_team AS a,b.nama_team AS b,a.logo_team AS logo_1,b.logo_team AS logo_2,a.nama_kota AS nama_kota_1,b.nama_kota AS nama_kota_2,tournament_third_place.id_third_place,tournament_third_place.team1,tournament_third_place.team2,tournament_third_place.score1,tournament_third_place.score2,tournament_third_place.winner as id_a,tournament_third_place.lose as id_b,tournament_third_place.date,tournament_third_place.id_data_kill')
                    ->join('tournament as t','tournament_third_place.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS a','tournament_third_place.winner','=','a.id_team')
                    ->join('tournament_group AS b','tournament_third_place.lose','=','b.id_team')
                    ->where('tournament_third_place.id_third_place','=',$id_round)
                    ->first();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function dataMatchs()
    {
        return $this->hasOne(BracketDataMatchs::class,'id_data_kill','id_data_kill');
    }

    public function HistoryTeam($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_third_place.date,tournament_third_place.winner,tournament_third_place.lose,"Round3" As rounds,tournament_third_place.id_third_place AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_third_place.id_tournament','=','t.id_tournament')
        ->whereNotNull('tournament_third_place.winner')
        ->where(function($q) use ($id_team){
            $q->where("tournament_third_place.team1" , "=" , $id_team)
                ->orWhere("tournament_third_place.team2","=", $id_team);
        })
        // ->where(function($a) {
        //     $a->where("tournament_third_place.team2","!=","")
        //     ->WhereNotNull('tournament_third_place.team2');
        // })
        ->orderBy('id_third_place','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeamNext($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_third_place.date,tournament_third_place.winner,tournament_third_place.lose,"Round2" As rounds,tournament_third_place.id_third_place AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_third_place.id_tournament','=','t.id_tournament')
        ->leftjoin('tournament_semi_finals AS A','tournament_third_place.team1','=','A.id_semi_finals')
        ->leftjoin('tournament_semi_finals AS B','tournament_third_place.team2','=','B.id_semi_finals')
        ->leftJoin('tournament_group AS C', function($join){
                $join->on('A.winner', '=', 'C.id_team');
                $join->on('C.id_group', '=', 't.id_group');
        })
        ->leftJoin('tournament_group AS D', function($joins){
            $joins->on('B.winner', '=', 'D.id_team');
            $joins->on('D.id_group', '=', 't.id_group');
        })
        ->whereNotNull('tournament_third_place.winner')
        ->where(function($q) use ($id_team){
            $q->where("A.lose" , "=" , $id_team)
                ->orWhere("B.lose","=", $id_team);
        })
        ->whereNotNull('A.winner')
        ->whereNotNull('B.winner')
        ->orderBy('tournament_third_place.id_third_place','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function InsertSetting($data,$id_tournament){
        foreach ($data as $row) {
            $id_4[] = $row['id_semi_finals'];
        }
        $hitung4    = count($id_4);
        $bagi4 	    = $hitung4 / 2 ;

        for ($i=0; $i < $hitung4 ; $i++) {
            if ($i%2==0) {
                $team2_1[] = $id_4[$i];
            }else{
                $team2_2[] = $id_4[$i];
            }
        }
        for ($i=0; $i < $bagi4; $i++) {
            $hash = (new AutoCodeIdRound)->AutoCode('tournament_third_place','THIRD','id_third_place');
            $round3 = new BracketThirdPlace();
            $round3->id_third_place   = $hash;
            $round3->id_tournament    = $id_tournament;
            $round3->team1            = $team2_1[$i];
            $round3->team2            = $team2_2[$i];
            $round3->save();
        }
    }
}
