<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommentsModel extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'user_status',
        'user_id',
        'parent_id',
        'reply_id',
        'comment',
        'commentable_id',
        'commentable_type',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }

    public function admin()
    {
        return $this->hasOne(Management::class, 'stream_id', 'user_id');
    }

    public function parent()
    {
        return $this->hasOne(CommentsModel::class, 'id', 'parent_id');
    }

    public function reply()
    {
        return $this->hasOne(CommentsModel::class, 'id', 'reply_id');
    }

}
