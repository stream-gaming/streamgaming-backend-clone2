<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardScoreSequenceModel extends Model
{
    //
    protected $table 		= 'leaderboard_score_sequence';
	protected $primaryKey 	= 'id_score_sequence';

}
