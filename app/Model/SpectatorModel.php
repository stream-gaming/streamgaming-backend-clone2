<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpectatorModel extends Model
{
	protected $table 		= 'spectator';
	protected $primaryKey 	= 'id_spectator';
	protected $keyType 		= 'string';

	public function getSpectatorByTournament($id_leaderboard)
	{
		return $this->where('leaderboard_spectator.id_leaderboard', $id_leaderboard)
			->leftJoin('leaderboard_spectator', 'spectator.id_spectator', '=', 'leaderboard_spectator.id_spectator')
			->get();
	}
}
