<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamenGroupModel extends Model
{
    protected $table      = 'tournament_group';
    protected $primaryKey = 'id_tournament_group';
    public $timestamps    = false;

    public function countFilledSlots($id_group)
    {
        return $this->where('id_group',$id_group)->count();
    }
    public function Team(){
        return $this->hasOne(TeamModel::class,'team_id','id_team');
    }
}
