<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPromotionModel extends Model
{

    protected $table = 'user_promotion';
    protected $guarded = [];

    public function promotion()
    {
        return $this->belongsTo(PromotionModel::class, 'promotion_id', 'id');
    }
}
