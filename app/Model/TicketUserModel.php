<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketUserModel extends Model
{
    protected $table = 'ticket_users';
    protected $primaryKey = null;
    public $timestamps = false;
    public $incrementing = false;

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'id_users');
    }

}
