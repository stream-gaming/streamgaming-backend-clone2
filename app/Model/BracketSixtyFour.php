<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Validation\AutoCodeIdRound;

class BracketSixtyFour extends Model
{
    protected $table = 'tournament_round64';
    public $timestamps   = false;

    public function getBracket($id_tournament)
    {
        $get =  $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_round64.id_round64,tournament_round64.team1,tournament_round64.team2,tournament_round64.score1,tournament_round64.score2,tournament_round64.winner,tournament_round64.date,tournament_round64.id_data_kill')
                    ->leftJoin('tournament as t','tournament_round64.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('tournament_round64.team1','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('tournament_round64.team2','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_round64.id_tournament','=',$id_tournament)
                    ->orderBy('id_round64','asc')
                    ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getBracketAfter($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_round64.id_round64,tournament_round64.team1,tournament_round64.team2,tournament_round64.score1,tournament_round64.score2,tournament_round64.winner,tournament_round64.date,tournament_round64.id_data_kill')
                    ->leftJoin('tournament as t','tournament_round64.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_round128 AS A','tournament_round64.team1','=','A.id_round128')
                    ->leftJoin('tournament_round128 AS B','tournament_round64.team2','=','B.id_round128')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('A.winner','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('B.winner','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_round64.id_tournament','=',$id_tournament)
                    ->orderBy('id_round64','asc')
                    ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getTeamInfo($id_round)
    {
        $get =  $this->selectRaw('a.nama_team AS a,b.nama_team AS b,a.logo_team AS logo_1,b.logo_team AS logo_2,a.nama_kota AS nama_kota_1,b.nama_kota AS nama_kota_2,tournament_round64.id_round64,tournament_round64.team1,tournament_round64.team2,tournament_round64.score1,tournament_round64.score2,tournament_round64.winner as id_a,tournament_round64.lose as id_b, tournament_round64.date,tournament_round64.id_data_kill')
                    ->join('tournament as t','tournament_round64.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS a','tournament_round64.winner','=','a.id_team')
                    ->join('tournament_group AS b','tournament_round64.lose','=','b.id_team')
                    ->where('tournament_round64.id_round64','=',$id_round)
                    ->first();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function dataMatchs()
    {
        return $this->hasOne(BracketDataMatchs::class,'id_data_kill','id_data_kill');
    }

    public function HistoryTeam($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_round64.date,tournament_round64.winner,tournament_round64.lose,"Round64" As rounds,tournament_round64.id_round64 AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_round64.id_tournament','=','t.id_tournament')
        ->whereNotNull('tournament_round64.winner')
        ->where(function($q) use ($id_team){
            $q->where("tournament_round64.team1" , "=" , $id_team)
                ->orWhere("tournament_round64.team2","=", $id_team);
        })
        // ->where(function($a) {
        //     $a->where("tournament_round64.team2","!=","")
        //     ->WhereNotNull('tournament_round64.team2');
        // })
        ->orderBy('id_round64','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeamNext($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_round64.date,tournament_round64.winner,tournament_round64.lose,"Round64" As rounds,tournament_round64.id_round64 AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_round64.id_tournament','=','t.id_tournament')
        ->leftjoin('tournament_round128 AS A','tournament_round64.team1','=','A.id_round128')
        ->leftjoin('tournament_round128 AS B','tournament_round64.team2','=','B.id_round128')
        ->leftJoin('tournament_group AS C', function($join){
                $join->on('A.winner', '=', 'C.id_team');
                $join->on('C.id_group', '=', 't.id_group');
        })

        ->leftJoin('tournament_group AS D', function($joins){
            $joins->on('B.winner', '=', 'D.id_team');
            $joins->on('D.id_group', '=', 't.id_group');
        })
        ->whereNotNull('tournament_round64.winner')
        ->where(function($q) use ($id_team){
            $q->where("A.winner" , "=" , $id_team)
                ->orWhere("B.winner","=", $id_team);
        })
        ->whereNotNull('A.winner')
        ->whereNotNull('B.winner')
        ->orderBy('tournament_round64.id_round64','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function InsertSetting($data,$id_tournament){
        foreach ($data as $row) {
            $id_128[] = $row['id_round128'];
        }
        $hitung128   = count($id_128);
        $bagi128     = $hitung128 / 2 ;
        for ($i=0; $i < $hitung128 ; $i++) {
            if ($i%2 ==0) {
                $team64_1[] = $id_128[$i];
            }else{
                $team64_2[] = $id_128[$i];
            }

        }
        for ($i=0; $i < $bagi128 ; $i++) {
            $hash = (new AutoCodeIdRound)->AutoCode('tournament_round64','Round64','id_round64');
            $round64 = new BracketSixtyFour();
            $round64->id_round64       = $hash;
            $round64->id_tournament    = $id_tournament;
            $round64->team1            = $team64_1[$i];
            $round64->team2            = $team64_2[$i];
            $round64->save();
        }
    }

}
