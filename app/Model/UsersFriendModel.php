<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersFriendModel extends Model
{
	//- Tabel User
    protected $table = "users_friend";
    //- Primary Key Tabel User
    protected $primaryKey = 'id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
}
