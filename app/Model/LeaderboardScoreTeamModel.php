<?php

namespace App\Model;

use App\Helpers\MyApps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LeaderboardScoreTeamModel extends Model
{
	protected $table 		= 'leaderboard_score_team';
	protected $primaryKey 	= 'id_leaderboard_score_team';
	protected $keyType 		= 'string';

	public function getTopScore($leaderboard)
	{
		return $this->selectRaw('SUM(l.sequence_score) AS sequence_score,lt.*,leaderboard_score_team.id_team,SUM(leaderboard_score_team.total_kill) AS total_kill,SUM(leaderboard_score_team.total_score) AS total_score, (SUM(leaderboard_score_team.total_score) + SUM(l.sequence_score)) AS total')
			->leftJoin('leaderboard_score_sequence as l', 'l.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('lt.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->where('lt.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total', 'desc')
			->first();
	}

	public function getScoreSequence($leaderboard)
	{
		return $this->selectRaw('t.team_logo as team_logos,SUM(l.sequence_score) AS sequence_score,leaderboard_score_team.id_team,lt.nama_team,SUM(leaderboard_score_team.total_kill) AS total_kill,SUM(leaderboard_score_team.total_score) AS total_score, (SUM(leaderboard_score_team.total_score) + SUM(l.sequence_score)) AS total')
			->leftJoin('leaderboard_score_sequence as l', 'l.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('lt.qualification_sequence', '=', DB::raw($leaderboard->stage));
				$join->on('lt.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
            })
            ->leftJoin('team as t', function($join){
                $join->on('t.team_id', '=', 'leaderboard_score_team.id_team');

            })
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.match_sequence', $leaderboard->match_id)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total', 'desc')
			->orderBy('total_kill', 'desc')
			->get();
    }

    public function getSoloScoreSequence($leaderboard)
	{
		return $this->selectRaw('ud.image AS image,SUM(l.sequence_score) AS sequence_score,leaderboard_score_team.id_team,lt.nama_team,SUM(leaderboard_score_team.total_kill) AS total_kill,SUM(leaderboard_score_team.total_score) AS total_score, (SUM(leaderboard_score_team.total_score) + SUM(l.sequence_score)) AS total')
			->leftJoin('leaderboard_score_sequence as l', 'l.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('lt.qualification_sequence', '=', DB::raw($leaderboard->stage));
				$join->on('lt.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
            })

            ->leftJoin('users_detail as ud', function ($join) {
                $join->on('leaderboard_score_team.id_team', '=', 'ud.user_id');

            })
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.match_sequence', $leaderboard->match_id)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total', 'desc')
			->orderBy('total_kill', 'desc')
			->get();
	}

	public function getScoreTotal($leaderboard)
	{

		$query_select_join = $this->selectRaw('leaderboard_score_team.qualification_sequence,leaderboard_score_team.id_leaderboard_score_team AS id_leaderboard_score_team2,lg2.status_group AS status_group2, lb2.total_match AS total_match2 ,SUM(l2.sequence_score) AS sequence_score2,t2.nama_team AS nama_team2,leaderboard_score_team.id_team AS id_team2, SUM(leaderboard_score_team.total_kill) AS total_kill2,SUM(leaderboard_score_team.total_score) AS total_score2, (SUM(leaderboard_score_team.total_score) + SUM(l2.sequence_score)) AS total2')
			->leftJoin('leaderboard_score_sequence as l2', 'l2.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as t2', function ($join) use ($leaderboard) {
				$join->on('t2.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('t2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('t2.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->leftJoin('leaderboard as lb2', 'lb2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard')
			->leftJoin('leaderboard_group as lg2', function ($join) use ($leaderboard) {
				$join->on('lg2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('lg2.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.match_sequence', $leaderboard->finalMatch)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->where('lg2.id_leaderboard', $leaderboard->id_leaderboard)
			->where('lg2.group_sequence', $leaderboard->group_sequence)
			->where('lg2.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total_kill2', 'desc');


		return $this->selectRaw('leaderboard_score_team.id_leaderboard_score_team,lg.status_group, lb.total_match,SUM(l.sequence_score) AS sequence_score,t.nama_team,tt.team_logo as team_logos,leaderboard_score_team.id_team, SUM(leaderboard_score_team.total_kill) AS total_kill,SUM(leaderboard_score_team.total_score) AS total_score, (SUM(leaderboard_score_team.total_score) + SUM(l.sequence_score)) AS total')
			->leftJoin('leaderboard_score_sequence as l', 'l.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as t', function ($join) use ($leaderboard) {
				$join->on('t.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('t.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('t.qualification_sequence', '=', DB::raw($leaderboard->stage));
            })

            ->leftJoin('team as tt', function($join){
                $join->on('tt.team_id', '=', 'leaderboard_score_team.id_team');
            })
			->leftJoin('leaderboard as lb', 'lb.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard')
			->leftJoin('leaderboard_group as lg', function ($join) use ($leaderboard) {
				$join->on('lg.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('lg.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->leftJoin(DB::raw('(' . (new MyApps)->getEloquentSqlWithBindings($query_select_join) . ') AS previous'), 'previous.id_team2', '=', 'leaderboard_score_team.id_team')
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->where('lg.id_leaderboard', $leaderboard->id_leaderboard)
			->where('lg.group_sequence', $leaderboard->group_sequence)
			->where('lg.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total', 'desc')
			->orderBy('total_kill', 'desc')
			->orderBy('previous.total2', 'desc')
			->get();
    }

    public function getScoreTotalSolo($leaderboard)
	{

		$query_select_join = $this->selectRaw('leaderboard_score_team.qualification_sequence,leaderboard_score_team.id_leaderboard_score_team AS id_leaderboard_score_team2,lg2.status_group AS status_group2, lb2.total_match AS total_match2 ,SUM(l2.sequence_score) AS sequence_score2,t2.nama_team AS nama_team2,leaderboard_score_team.id_team AS id_team2, SUM(leaderboard_score_team.total_kill) AS total_kill2,SUM(leaderboard_score_team.total_score) AS total_score2, (SUM(leaderboard_score_team.total_score) + SUM(l2.sequence_score)) AS total2')
			->leftJoin('leaderboard_score_sequence as l2', 'l2.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as t2', function ($join) use ($leaderboard) {
				$join->on('t2.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('t2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('t2.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->leftJoin('leaderboard as lb2', 'lb2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard')
			->leftJoin('leaderboard_group as lg2', function ($join) use ($leaderboard) {
				$join->on('lg2.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('lg2.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.match_sequence', $leaderboard->finalMatch)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->where('lg2.id_leaderboard', $leaderboard->id_leaderboard)
			->where('lg2.group_sequence', $leaderboard->group_sequence)
			->where('lg2.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total_kill2', 'desc');


		return $this->selectRaw('leaderboard_score_team.id_leaderboard_score_team,lg.status_group, lb.total_match,SUM(l.sequence_score) AS sequence_score,t.nama_team,tt.image as image,leaderboard_score_team.id_team, SUM(leaderboard_score_team.total_kill) AS total_kill,SUM(leaderboard_score_team.total_score) AS total_score, (SUM(leaderboard_score_team.total_score) + SUM(l.sequence_score)) AS total')
			->leftJoin('leaderboard_score_sequence as l', 'l.id_score_sequence', '=', 'leaderboard_score_team.id_score_sequence')
			->leftJoin('leaderboard_team as t', function ($join) use ($leaderboard) {
				$join->on('t.id_team_leaderboard', '=', 'leaderboard_score_team.id_team');
				$join->on('t.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('t.qualification_sequence', '=', DB::raw($leaderboard->stage));
            })

            ->leftJoin('users_detail as tt', function ($join) {
                $join->on('leaderboard_score_team.id_team', '=', 'tt.user_id');

            })
			->leftJoin('leaderboard as lb', 'lb.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard')
			->leftJoin('leaderboard_group as lg', function ($join) use ($leaderboard) {
				$join->on('lg.id_leaderboard', '=', 'leaderboard_score_team.id_leaderboard');
				$join->on('lg.qualification_sequence', '=', DB::raw($leaderboard->stage));
			})
			->leftJoin(DB::raw('(' . (new MyApps)->getEloquentSqlWithBindings($query_select_join) . ') AS previous'), 'previous.id_team2', '=', 'leaderboard_score_team.id_team')
			->where('leaderboard_score_team.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_team.group_sequence', $leaderboard->group_sequence)
			->where('leaderboard_score_team.qualification_sequence', $leaderboard->stage)
			->where('lg.id_leaderboard', $leaderboard->id_leaderboard)
			->where('lg.group_sequence', $leaderboard->group_sequence)
			->where('lg.qualification_sequence', $leaderboard->stage)
			->groupBy('leaderboard_score_team.id_team')
			->orderBy('total', 'desc')
			->orderBy('total_kill', 'desc')
			->orderBy('previous.total2', 'desc')
			->get();
    }

    public function LeaderboardScoreSequence()
    {
        # code...
        return $this->hasOne(LeaderboardScoreSequenceModel::class,"id_score_sequence","id_score_sequence")->select(['id_score_sequence','urutan']);
    }

    public function Leaderboard()
    {
        # code...
        return $this->hasOne(LeaderboardModel::class,"id_leaderboard","id_leaderboard")->select(['id_leaderboard','tournament_name','game_competition','tournament_category']);

    }

}
