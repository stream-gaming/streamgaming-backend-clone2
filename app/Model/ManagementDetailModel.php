<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ManagementDetailModel extends Model
{
	protected $table = 'management_docs';
	protected $primaryKey = 'access_id';
	protected $keyType = 'string';
	public $incrementing = false; 
	public $timestamps = false;

	protected $guarded = [];

	public function admin()
	{
		return $this->belongsTo(Management::class, 'access_id', 'stream_id');
	}
}
