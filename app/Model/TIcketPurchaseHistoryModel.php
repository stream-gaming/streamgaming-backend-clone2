<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TIcketPurchaseHistoryModel extends Model
{
    protected $table = 'ticket_purchase_history';

    public function ticket()
    {
        return $this->HasOne(AllTicketModel::class, 'id', 'ticket_id');
    }
}
