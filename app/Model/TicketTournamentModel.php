<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketTournamentModel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'ticket_tournament';
}
