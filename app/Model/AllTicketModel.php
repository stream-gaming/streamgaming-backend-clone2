<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllTicketModel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'all_ticket';

	protected $fillable = ['name', 'type', 'sub_type', 'unit', 'competition', 'can_traded', 'market_value', 'giving_target', 'mode','total_given' , 'max_given', 'max_has', 'event_end', 'expired'];


    public function ticketClaims()
    {
        return $this->hasOne(TicketClaimsModel::class, 'ticket_id', 'id');
    }

    public function game()
    {
        return $this->hasOne(GameModel::class, 'id_game_list', 'competition');
    }

    public function scopePersonal($query)
    {
        return $query->where('mode', 1);
    }

    public function scopeTeam($query)
    {
        return $query->whereIn('mode', [2, 3])->where('competition', '!=', "all");
    }
}
