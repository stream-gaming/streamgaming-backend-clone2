<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Management extends Authenticatable implements JWTSubject
{
	protected $table = 'management_access';
	protected $primaryKey = 'stream_id';
	protected $keyType = 'string';
    public $incrementing = false; 

	protected $hidden = ['password', 'role_access'];

	public function news()
	{
		return $this->hasMany(News::class, 'stream_id');
	}

	public function detail()
	{
		return $this->hasOne(ManagementDetailModel::class, 'access_id', 'stream_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
    * Return a key value array, containing any custom claims to be added to the JWT.
    *
    * @return array
    */
    public function getJWTCustomClaims()
    {
        return ['user'  => $this];
    }
}
