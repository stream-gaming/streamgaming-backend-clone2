<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryBannedModel extends Model
{
    protected $table = 'history_banned';

    public function admin()
	{
		return $this->hasOne(Management::class, 'stream_id', 'admin_id');
	}

}
