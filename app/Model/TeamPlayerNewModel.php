<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamPlayerNewModel extends Model
{
    use SoftDeletes;
    protected $table = 'team_player_new';
    protected $guarded = ['id'];

    public function team(){
        return $this->belongsTo(TeamModel::class, 'team_id', 'team_id');
    }
}
