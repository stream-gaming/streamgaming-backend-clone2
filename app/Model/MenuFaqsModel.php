<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuFaqsModel extends Model
{
    protected $table      = 'menu_faqs';
    protected $primaryKey = 'id';

    public function SubMenuFaqs(){
        return $this->hasMany(SubMenuFaqsModel::class, 'id_menu','id_menu');
    }
    public function faqs()
    {
        return $this->belongsTo(FaqsModel::class, 'id_menu_faqs', 'id_menu_faqs');
    }
}
