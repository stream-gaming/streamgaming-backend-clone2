<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CustomPrizesModel extends Model
{
    //
    protected $table = 'costum_prizes';

    public function PrizeInvetory(){
        return $this->hasOne(PrizeInventoryModel::class,'id','pi_id');
    }

    public function prizeInventory(){
        return $this->belongsTo(PrizeInventoryModel::class, 'pi_id')->select('id', 'valuation');
    }

}
