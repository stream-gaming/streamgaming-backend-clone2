<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryBannedTeamModel extends Model
{
    protected $table = 'history_banned_team';

    protected $fillable = [
        'team_id',
        'ban_until',
        'reason_ban',
        'admin_id',
    ];

    public function admin()
	{
		return $this->hasOne(Management::class, 'stream_id', 'admin_id');
	}
}
