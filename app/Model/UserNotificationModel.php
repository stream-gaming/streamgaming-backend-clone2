<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotificationModel extends Model
{
    use SoftDeletes;

    protected $table = 'notification';
	protected $primaryKey = 'id_notification';
	public $incrementing = false;
	protected $keyType = 'string';

	const CREATED_AT = 'date';
    const UPDATED_AT = 'date';

    public function fcmFirebaseToken()
    {
        return $this->hasMany(FcmNotifTokenModel::class, 'user_id', 'id_user');
    }

    public function getUpdatedAtColumn() {
        return null;
    }

}
