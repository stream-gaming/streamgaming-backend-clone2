<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Validation\AutoCodeIdRound;
class BracketQuarterFinal extends Model
{
    protected $table = 'tournament_quarter_finals';
    public $timestamps   = false;

    public function getBracket($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_quarter_finals.id_quarter_finals,tournament_quarter_finals.team1,tournament_quarter_finals.team2,tournament_quarter_finals.score1,tournament_quarter_finals.score2,tournament_quarter_finals.winner,tournament_quarter_finals.date,tournament_quarter_finals.id_data_kill')
                    ->leftJoin('tournament as t','tournament_quarter_finals.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('tournament_quarter_finals.team1','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('tournament_quarter_finals.team2','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_quarter_finals.id_tournament','=',$id_tournament)
                    ->orderBy('id_quarter_finals','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getBracketAfter($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_quarter_finals.id_quarter_finals,tournament_quarter_finals.team1,tournament_quarter_finals.team2,tournament_quarter_finals.score1,tournament_quarter_finals.score2,tournament_quarter_finals.winner,tournament_quarter_finals.date,tournament_quarter_finals.id_data_kill')
                    ->leftJoin('tournament as t','tournament_quarter_finals.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_round16 AS A','tournament_quarter_finals.team1','=','A.id_round16')
                    ->leftJoin('tournament_round16 AS B','tournament_quarter_finals.team2','=','B.id_round16')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('A.winner','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('B.winner','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_quarter_finals.id_tournament','=',$id_tournament)
                    ->orderBy('id_quarter_finals','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getTeamInfo($id_round)
    {
        $get =  $this->selectRaw('a.nama_team AS a,b.nama_team AS b,a.logo_team AS logo_1,b.logo_team AS logo_2,a.nama_kota AS nama_kota_1,b.nama_kota AS nama_kota_2,tournament_quarter_finals.id_quarter_finals,tournament_quarter_finals.team1,tournament_quarter_finals.team2,tournament_quarter_finals.score1,tournament_quarter_finals.score2,tournament_quarter_finals.winner  as id_a,tournament_quarter_finals.lose as id_b, tournament_quarter_finals.date,tournament_quarter_finals.id_data_kill')
                    ->join('tournament as t','tournament_quarter_finals.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS a','tournament_quarter_finals.winner','=','a.id_team')
                    ->join('tournament_group AS b','tournament_quarter_finals.lose','=','b.id_team')
                    ->where('tournament_quarter_finals.id_quarter_finals','=',$id_round)
                    ->first();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeam($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_quarter_finals.date,tournament_quarter_finals.winner,tournament_quarter_finals.lose,"Round8" As rounds,tournament_quarter_finals.id_quarter_finals AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_quarter_finals.id_tournament','=','t.id_tournament')
        ->where(function($q) use ($id_team){
            $q->where("tournament_quarter_finals.team1" , "=" , $id_team)
                ->orWhere("tournament_quarter_finals.team2","=", $id_team);
        })
        ->whereNotNull('tournament_quarter_finals.winner')
        ->where('tournament_quarter_finals.winner','!=','')

        // ->where(function($a) {
        //     $a->where("tournament_quarter_finals.team2","!=","")
        //     ->WhereNotNull('tournament_quarter_finals.team2');
        // })
        ->orderBy('id_quarter_finals','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeamNext($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_quarter_finals.date,tournament_quarter_finals.winner,tournament_quarter_finals.lose,"Round8" As rounds,tournament_quarter_finals.id_quarter_finals AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_quarter_finals.id_tournament','=','t.id_tournament')
        ->leftjoin('tournament_round16 AS A','tournament_quarter_finals.team1','=','A.id_round16')
        ->leftjoin('tournament_round16 AS B','tournament_quarter_finals.team2','=','B.id_round16')
        ->leftJoin('tournament_group AS C', function($join){
                $join->on('A.winner', '=', 'C.id_team');
                $join->on('C.id_group', '=', 't.id_group');
        })
        ->leftJoin('tournament_group AS D', function($joins){
            $joins->on('B.winner', '=', 'D.id_team');
            $joins->on('D.id_group', '=', 't.id_group');
        })
        ->whereNotNull('tournament_quarter_finals.winner')
        ->where(function($q) use ($id_team){
            $q->where("A.winner" , "=" , $id_team)
                ->orWhere("B.winner","=", $id_team);
        })
        ->whereNotNull('A.winner')
        ->whereNotNull('B.winner')
        ->orderBy('tournament_quarter_finals.id_quarter_finals','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function dataMatchs()
    {
        return $this->hasOne(BracketDataMatchs::class,'id_data_kill','id_data_kill');
    }
    public function InsertSetting($data,$id_tournament){
        foreach ($data as $row) {
            $id_16[] = $row['id_round16'];
        }
        $hitung16   = count($id_16);
        $bagi16 	= $hitung16 / 2 ;

        for ($i=0; $i < $hitung16 ; $i++) {
            if ($i%2 ==0) {
                $team8_1[] = $id_16[$i];
            }else{
                $team8_2[] = $id_16[$i];
            }

        }
        for ($i=0; $i < $bagi16; $i++) {
            $hash = (new AutoCodeIdRound)->AutoCode('tournament_quarter_finals','QTF','id_quarter_finals');
            $round8 = new BracketQuarterFinal();
            $round8->id_quarter_finals= $hash;
            $round8->id_tournament    = $id_tournament;
            $round8->team1            = $team8_1[$i];
            $round8->team2            = $team8_2[$i];
            $round8->save();
        }
    }
}
