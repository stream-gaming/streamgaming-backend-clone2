<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentMenuLiga extends Model
{
    protected $table        = 'tournament_menu_liga';
    protected $primaryKey   = 'id';
    public $timestamps      = false;

    public function liga()
    {
        return $this->belongsTo(TournamentLiga::class,'id_tab_liga','id_tab_liga');
    }

}
