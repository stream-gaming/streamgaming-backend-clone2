<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserOnboardingModel extends Model
{
    protected $table = "user_onboarding";
    protected $guarded = [];

    public function user(){
        $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
