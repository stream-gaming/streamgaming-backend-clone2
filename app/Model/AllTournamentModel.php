<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AllTournamentModel extends Model
{
	protected $table = 'all_tournament';
    protected $guarded = [];
    public $timestamps = false;
}
