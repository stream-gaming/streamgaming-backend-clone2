<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProvinceModel extends Model
{
	protected $table = 'provinsi';

    public $timestamps = false;

    protected $primaryKey = 'id_prov';
}
