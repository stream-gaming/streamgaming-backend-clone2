<?php

namespace App\Model;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class IdentityIngameHistoryModel extends Model
{
    use Compoships;

    protected $table = 'identity_ingame_history';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','user_id');
    }
}
