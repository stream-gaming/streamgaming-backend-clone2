<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;



class hasJoinedTournamentModel extends Model
{
    //
    protected $table = 'player_payment';
    protected $primaryKey = 'id_player_payment';
    protected $keyType = 'string';

    public function checkRegistration($user_id, $idTournament){
        $data = $this->where('id_tournament', $idTournament)
        ->where('player_id', $user_id)
        ->where('status', '!=' , 'Cancelled')
        ->exists();

            return $data;

    }

}
