<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GameUsersRoleModel extends Model
{
    protected $table      = 'game_users_role';
    protected $primaryKey = 'id';

    protected $fillable = [
        'game_role_id', 'user_id'
    ];

    public function gameRole()
    {
        return $this->hasOne(GameRoleModel::class, 'id', 'game_role_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
