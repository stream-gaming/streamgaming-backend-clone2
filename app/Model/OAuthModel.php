<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class OAuthModel extends Model
{
    //- Tabel User
    protected $table = "oauth";
    //- Primary Key Tabel User
    protected $primaryKey = 'users_id';
    // Tipe Data Primary Key
    protected $keyType = 'string';
    // Disabled Time Stamp
    public $timestamps = false;

    protected $fillable = [
      'users_id',
      'provider_id',
      'oauth_id'
    ];


    public static function addOauth($request)
    {

            // Initiate Class
            $users_game = new OAuthModel();
            $users_game->users_id    = $request->user_id;
            $users_game->provider_id = $request->input('provider_id');
            $users_game->oauth_id    = $request->input('oauth_id');
            $users_game->save();

            //return respons berhasil
          return $users_game;
    }
}
