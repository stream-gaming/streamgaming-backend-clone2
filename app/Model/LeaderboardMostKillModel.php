<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardMostKillModel extends Model
{
	protected $table 		= 'leaderboard_mostkill';
	protected $primaryKey 	= 'id_leaderboard';
	protected $keyType 		= 'string';

}
