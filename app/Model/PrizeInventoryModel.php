<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrizeInventoryModel extends Model
{
    //
    use SoftDeletes;
    protected $table = 'prize_inventory';
}
