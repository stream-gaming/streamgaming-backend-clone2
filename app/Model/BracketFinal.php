<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Validation\AutoCodeIdRound;

class BracketFinal extends Model
{
    protected $table = 'tournament_final';
    public $timestamps   = false;

    public function getBracketAfter($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_final.id_final,tournament_final.team1,tournament_final.team2,tournament_final.score1,tournament_final.score2,tournament_final.winner,tournament_final.date,tournament_final.id_data_kill')
                    ->leftJoin('tournament as t','tournament_final.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_semi_finals AS A','tournament_final.team1','=','A.id_semi_finals')
                    ->leftJoin('tournament_semi_finals AS B','tournament_final.team2','=','B.id_semi_finals')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('A.winner','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('B.winner','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_final.id_tournament','=',$id_tournament)
                    ->orderBy('id_final','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getWinner($id_tournament)
    {
        $get = $this->selectRaw('C.id_team AS id_team1,D.id_team AS id_team2,C.nama_team AS team_1,C.logo_team AS logo_1,C.nama_kota AS nama_kota_1,tournament_final.date')
                    ->leftJoin('tournament as t','tournament_final.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS C',function($join){
                        $join->on('tournament_final.winner','=','C.id_team')
                            ->on('C.id_group','=','t.id_group');
                    })
                    ->join('tournament_group AS D',function($join){
                        $join->on('tournament_final.winner','=','D.id_team')
                            ->on('D.id_group','=','t.id_group');
                    })
                    ->where('tournament_final.id_tournament','=',$id_tournament)
                    ->firstOr(function(){
                        return false;
                    });

        return $get;

    }

    public function getTeamInfo($id_round)
    {
        $get =  $this->selectRaw('a.nama_team AS a,b.nama_team AS b,a.logo_team AS logo_1,b.logo_team AS logo_2,a.nama_kota AS nama_kota_1,b.nama_kota AS nama_kota_2,tournament_final.id_final,tournament_final.team1,tournament_final.team2,tournament_final.score1,tournament_final.score2,tournament_final.winner as id_a,tournament_third_place.lose as id_b, tournament_final.date,tournament_final.id_data_kill')
                    ->join('tournament as t','tournament_final.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS a','tournament_final.winner','=','a.id_team')
                    ->join('tournament_group AS b','tournament_final.lose','=','b.id_team')
                    ->where('tournament_final.id_final','=',$id_round)
                    ->firstOr(function(){
                        return false;
                    });

        return $get;
    }

    public function HistoryTeam($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_final.date,tournament_final.winner,"Round1" As rounds,tournament_final.id_final AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_final.id_tournament','=','t.id_tournament')
        ->where(function($q) use ($id_team){
            $q->where("tournament_final.team1" , "=" , $id_team)
                ->orWhere("tournament_final.team2","=", $id_team);
        })
        ->whereNotNull('tournament_final.winner')
        ->where('tournament_final.winner','!=','')
        // ->where(function($a) {
        //     $a->where("tournament_final.team2","!=","")
        //     ->WhereNotNull('tournament_final.team2');
        // })
        ->orderBy('id_final','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeamNext($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_final.date,tournament_final.winner,tournament_final.lose,"Round2" As rounds,tournament_final.id_final AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_final.id_tournament','=','t.id_tournament')
        ->leftjoin('tournament_semi_finals AS A','tournament_final.team1','=','A.id_semi_finals')
        ->leftjoin('tournament_semi_finals AS B','tournament_final.team2','=','B.id_semi_finals')
        ->leftJoin('tournament_group AS C', function($join){
                $join->on('A.winner', '=', 'C.id_team');
                $join->on('C.id_group', '=', 't.id_group');
        })
        ->leftJoin('tournament_group AS D', function($joins){
            $joins->on('B.winner', '=', 'D.id_team');
            $joins->on('D.id_group', '=', 't.id_group');
        })
        ->whereNotNull('tournament_final.winner')
        ->where(function($q) use ($id_team){
            $q->where("A.winner" , "=" , $id_team)
                ->orWhere("B.winner","=", $id_team);
        })
        ->whereNotNull('A.winner')
        ->whereNotNull('B.winner')
        ->orderBy('tournament_final.id_final','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function dataMatchs()
    {
        return $this->hasOne(BracketDataMatchs::class,'id_data_kill','id_data_kill');
    }
    public function InsertSetting($data,$id_tournament){
        foreach ($data as $row) {
            $id_4[] = $row['id_semi_finals'];
        }
        $hitung4    = count($id_4);
        $bagi4 	    = $hitung4 / 2 ;

        for ($i=0; $i < $hitung4 ; $i++) {
            if ($i%2 ==0) {
                $team2_1[] = $id_4[$i];
            }else{
                $team2_2[] = $id_4[$i];
            }
        }
        for ($i=0; $i < $bagi4; $i++) {
            $hash = (new AutoCodeIdRound)->AutoCode('tournament_final','FINAL','id_final');
            $round2 = new BracketFinal();
            $round2->id_final         = $hash;
            $round2->id_tournament    = $id_tournament;
            $round2->team1            = $team2_1[$i];
            $round2->team2            = $team2_2[$i];
            $round2->save();
        }
    }
}
