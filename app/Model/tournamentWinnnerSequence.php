<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class tournamentWinnnerSequence extends Model
{
    protected $table        = 'tournament_winner_sequence';
	protected $primaryKey   = 'id';
    public $timestamps      = false;
}
