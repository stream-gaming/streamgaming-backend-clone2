<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExpressionFaqsModel extends Model
{
    protected $table      = 'expression_faqs';
    protected $primaryKey = 'id';

}
