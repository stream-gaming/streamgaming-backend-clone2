<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PinnedDiscussionModel extends Model
{
    //
    protected $table = 'pinned_discussion';
    protected $fillable = [
        'room_id',
        'user_id'
    ];
}
