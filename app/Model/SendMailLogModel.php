<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class SendMailLogModel extends Model
{

    //- Tabel User
    protected $table = "send_mail_logs";

    public function addLogMail($data)
    {

        try {

            // Cari User berdasarkan email
            $user = User::where('email', $data['email'])->first();

            // Cari User berdasarkan email
            $log = SendMailLogModel::where('email', $data['email'])
                ->where('type', $data['type'])
                ->first();

            if ($log) {

                $log->users_id      = $user->user_id;
                $log->email         = $user->email;
                $log->count         = $log->count + 1;
                $log->type          = $data['type'];
                $log->updated_at    = gmdate('Y-m-d H:i:s', mktime(date('H') + 7));
                $log->save();

                //- Pesan
                return response()->json([
                    'status' => false,
                    'message' => Lang::get('message.data.edit.success', [
                        'data' => 'Send Mail Log',
                    ])
                ]);
            } else {
                // Tambah Log Email
                $log = new SendMailLogModel();
                $log->users_id      = $user->user_id;
                $log->email         = $user->email;
                $log->count         = 1;
                $log->type          = $data['type'];
                $log->created_at    = gmdate('Y-m-d H:i:s', mktime(date('H') + 7));
                $log->updated_at    = '';
                $log->save();
                //- Pesan
                return response()->json([
                    'status' => false,
                    'data'    => $log,
                    'message' => Lang::get('message.data.add.success', [
                        'data' => 'Send Mail Log',
                    ])
                ]);
            }
        } catch (\Exception $e) {
            //return error message gagal
            return response()->json([
                'status' => false,
                'message' => Lang::get('message.data.add.failed', [
                    'data' => 'Send Mail Log',
                ]),
                'error' => $e
            ], 409);
        }
    }
}
