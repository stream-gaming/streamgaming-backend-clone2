<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlayerPayment extends Model
{
	// const UPDATED_AT 		= 'date_payment';
	protected $table	 	= 'player_payment';
	protected $primaryKey 	= 'id_player_payment';
	protected $keyType 		= 'string';
    protected $guarded = [];
	public $incrementing 	= false;
    public $timestamps 		= false;


	public function cekRegistrasi($leaderboard, $id_team)
	{
		return $this->where('id_tournament', $leaderboard->id_leaderboard)
			->where('id_team', $id_team)
			->where('status', '!=', 'Cancelled')
			->first();
	}

	public function isPaidOff($player_payment)
	{
		return $this->where('id_tournament', $player_payment->id_tournament)
			->where('id_team', $player_payment->id_team)
			->where('status', '!=', 'Cancelled')
			->where('status', 'Waiting');
    }


    public function getLeaderboard(){
        $select = [
                'id_leaderboard',
                'tournament_category',
                'is_no_qualification',
                'tournament_name',
                'tournament_mode',
                'tournament_form',
                'tournament_fee',
                'firstprize',
                'secondprize',
                'thirdprize',
                'organizer_name',
                'tournament_date',
                'card_banner',
                'game_competition',
                'game_option',
                'prize_type',
                'total_team',
                'status_tournament',
                'filled_slot',
                'leaderboard.url',
                'game_list.name',
                'game_list.logo_game_list',
                'game_list.games_on'
            ];

        return $this->hasOne(LeaderboardModel::class,'id_leaderboard','id_tournament')->select($select);
    }

    public function getBracket(){
        $select = [
            'id_tournament',
            'tournament_name',
            'date',
            'kompetisi',
            'system_tournament',
            'game_option',
            'banner_tournament',
            'total_team',
            'status',
            'organizer_name',
            'total_prize',
            'system_payment',
            'tournament.url',
            'game_list.name',
            'game_list.logo_game_list',
            'game_list.games_on'


        ];
        return $this->hasOne(TournamentModel::class,'id_tournament','id_tournament')->select($select);
    }

    public function getPinned(){

        return $this->hasOne(PinnedDiscussionModel::class,'room_id','id_tour');
    }

    public function lastMessage(){

        return $this->hasOne(MessageTournamentModel::class,'room_id','id_tour')->withTrashed();
    }
    public function getTeam(){
        return $this->hasOne(TournamenGroupModel::class,'id_team','id_team');
    }

}
