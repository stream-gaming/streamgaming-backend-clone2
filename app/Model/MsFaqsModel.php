<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MsFaqsModel extends Model
{
    protected $table      = 'ms_faqs';
    protected $primaryKey = 'id';

    public function faqS()
    {
        return $this->hasOne(FaqsModel::class, 'id_ms_faqs', 'id');
    }
}
