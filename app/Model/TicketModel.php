<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketModel extends Model
{
    protected $table        = 'ticket';
    protected $primaryKey   = 'id_ticket';
    public $timestamps = false;
    protected $keyType      = 'string';

    public function game()
    {
        return $this->hasOne(GameModel::class, 'id_game_list', 'kompetisi');
    }
}
