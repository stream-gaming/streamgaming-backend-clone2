<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PortalNotificationModel extends Model
{
    //
    protected $table = 'portal_notification';
    protected $guarded = [];
    public $timestamps = false;



}
