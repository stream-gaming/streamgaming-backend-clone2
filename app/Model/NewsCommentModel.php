<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsCommentModel extends Model
{
    protected $table = 'komentar'; 
	protected $primaryKey = 'comment_id';
	public $incrementing = false;
	protected $keyType = 'string'; 
	public $timestamps = false;

    const CREATED_AT = 'date'; 
    const UPDATED_AT = 'date'; 

    public function author()
    { 
        return $this->hasOne(User::class, 'user_id', 'users_id');
    } 

    public function authorDetail()
    {
        return $this->hasOne(UsersDetailModel::class, 'user_id', 'users_id');        
    }  

    public function replied($comment_id)
    {
        return $this->where('reply_comment_id', $comment_id)->latest()->get();
    }
}
