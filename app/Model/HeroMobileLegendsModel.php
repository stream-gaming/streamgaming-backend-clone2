<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HeroMobileLegendsModel extends Model
{
    protected $table = 'hero_mobile_legends';
}
