<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardGroupModel extends Model
{
	protected $table 		= 'leaderboard_group';
	protected $primaryKey 	= 'id_group_leaderboard';

	public function getByLeadeboardSequence($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('qualification_sequence', $leaderboard->stage)
			// ->selectRaw('id_leaderboard, group_sequence')
			->groupBy('group_sequence')
            // ->paginate($leaderboard->limit);
            ->get();
	}

	public function getLeaderboardGroup($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			// ->selectRaw('id_group_leaderboard')
			->where('group_sequence', '!=', 1000)
			->where('qualification_sequence', $leaderboard->stage)
			->groupBy('id_group_leaderboard')
			->orderBy('group_sequence')
            // ->paginate($leaderboard->limit);
            ->get();

	}

	public function cekGroup($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('group_sequence', $leaderboard->group_sequence)
			->where('qualification_sequence', $leaderboard->stage)
			->first();
    }


}
