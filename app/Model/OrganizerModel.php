<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrganizerModel extends Model
{
    protected $table = "organizer";
    protected $primaryKey 	= 'id';
    protected $keyType = 'string';
}
