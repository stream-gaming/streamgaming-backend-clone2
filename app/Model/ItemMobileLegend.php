<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemMobileLegend extends Model
{
    protected $table = 'gear_mobile_legends';
}
