<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Lang;
use Tymon\JWTAuth\Claims\Custom;

class TournamentMvpModel extends Model
{
    protected $table        = 'tournament_mvp';
    public $timestamps      = false;
    protected $guarded = [];

    public function PlayerPayment(){
        return $this->hasOne(PlayerPayment::class,'player_id','id_player');
    }
    public function tournament_group_player(){
        return $this->hasOne(TournamentGroupPlayerModel::class,'id_player','id_player');
    }
}
