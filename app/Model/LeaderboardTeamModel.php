<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LeaderboardTeamModel extends Model
{
	protected $table 		= 'leaderboard_team';
	protected $primaryKey 	= 'id_team_leaderboard';
	protected $keyType 		= 'string';
    public $timestamps      = false;

	public function team()
	{
		return $this->hasMany(TeamModel::class, 'team_id', 'id_team_leaderboard');
	}

	public function getTeamLeaderboardPaginate($leaderboard)
	{
		return $this->where('leaderboard_team.id_leaderboard', $leaderboard->id_leaderboard)
			->leftJoin('team', 'leaderboard_team.id_team_leaderboard', '=', 'team.team_id')
			->leftJoin('identity_ingame', function ($join) {
				$join->on('team.leader_id', '=', 'identity_ingame.users_id');
				$join->on('identity_ingame.game_id', '=', 'team.game_id');
            })
            ->leftJoin('player_payment as pp', function ($join) {

                $join->on('leaderboard_team.id_team_leaderboard', '=', 'pp.id_team');
                $join->on('leaderboard_team.id_leaderboard', '=', 'pp.id_tournament');

            })
            ->where('leaderboard_team.qualification_sequence', $leaderboard->stage)
            ->orderBy('pp.date_payment', 'asc')
            ->groupBy('pp.id_team')
			// ->paginate($leaderboard->limit);
			->get();
    }

    public function getSoloLeaderboardPaginate($leaderboard){
        return $this->where('leaderboard_team.id_leaderboard', $leaderboard->id_leaderboard)
        ->leftJoin('users_detail', function ($join) {
            $join->on('leaderboard_team.id_team_leaderboard', '=', 'users_detail.user_id');

        })
        ->leftJoin('user_city_view', function ($join) {

            $join->on('leaderboard_team.id_team_leaderboard', '=', 'user_city_view.user_id');
        })
        ->leftJoin('player_payment as pp', function ($join) {

            $join->on('leaderboard_team.id_team_leaderboard', '=', 'pp.id_team');
            $join->on('leaderboard_team.id_leaderboard', '=', 'pp.id_tournament');
        })


        ->where('leaderboard_team.qualification_sequence', $leaderboard->stage)
        ->orderBy('pp.date_payment', 'asc')
        ->groupBy('pp.id_team')
        // ->paginate($leaderboard->limit);
        ->get();

    }

	public function getTotalTeamGroup($leaderboard)
	{

		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('urutan_grup', $leaderboard->group_sequence)
			->where('qualification_sequence', $leaderboard->stage)
			->count();
	}

	public function getTotalFinalTeamGroup($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('is_final', 1)
			->count();
	}

	public function getTeamFinal($leaderboard)
	{
        return $this->selectRaw('leaderboard_team.*,t.team_logo as team_logos')
            ->leftJoin('team as t', function($join){
                $join->on('t.team_id', '=', 'leaderboard_team.id_team_leaderboard');

             })
            ->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('qualification_sequence', $leaderboard->stage)
            ->where('is_final', 1)
            ->orderBy('nama_team','asc')
			->get();
	}


	public function getTeam($leaderboard)
	{
        return $this->selectRaw('leaderboard_team.*,t.team_logo as team_logos')
            ->leftJoin('team as t', function($join){
                    $join->on('t.team_id', '=', 'leaderboard_team.id_team_leaderboard');

                 })
            ->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('urutan_grup', $leaderboard->group_sequence)
            ->where('qualification_sequence', $leaderboard->stage)
            ->orderBy('nama_team','asc')
			->get();
    }

    public function getSolo($leaderboard)
	{
        return $this->selectRaw('leaderboard_team.*,ud.image as image')
            ->leftJoin('users_detail as ud', function ($join) {
                    $join->on('leaderboard_team.id_team_leaderboard', '=', 'ud.user_id');
            })
            ->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('urutan_grup', $leaderboard->group_sequence)
            ->where('qualification_sequence', $leaderboard->stage)
            ->orderBy('nama_team','asc')
			->get();
    }

    public function getSoloFinal($leaderboard)
	{
        return $this->selectRaw('leaderboard_team.*,ud.image as image')
            ->leftJoin('users_detail as ud', function ($join) {
                $join->on('leaderboard_team.id_team_leaderboard', '=', 'ud.user_id');
            })
            ->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('qualification_sequence', $leaderboard->stage)
            ->where('is_final', 1)
            ->orderBy('nama_team','asc')
			->get();
	}

	public function getTotalTeam($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('qualification_sequence', $leaderboard->stage)
			->get();
	}

	public function countFilledSlots($id_leaderboard, $qualification_sequence)
	{

		return $this->where('id_leaderboard', $id_leaderboard)
			->where('qualification_sequence', $qualification_sequence)
			->count();
	}
}
