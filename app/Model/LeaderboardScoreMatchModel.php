<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LeaderboardScoreMatchModel extends Model
{
	protected $table 		= 'leaderboard_score_match';
	protected $primaryKey 	= 'id_score_match';
    public $timestamps 		= false;

	public function getMostKillSequenceFullCustom($leaderboard)
	{
		return $this->selectRaw('SUM(leaderboard_score_match.jumlah_kill) as total_kill,lt.nama_team,lt.nama_team,t.team_logo,leaderboard_score_match.id_player, ltp.player_name, ltp.id_team_leaderboard as id_team')
			->leftJoin('leaderboard_team_player as ltp', function ($join) use ($leaderboard) {
				$join->on('ltp.id_teamplayer_leaderboard', '=', 'leaderboard_score_match.id_player');
				$join->on('ltp.id_leaderboard', '=', 'leaderboard_score_match.id_leaderboard');
			})
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'ltp.id_team_leaderboard');
				$join->on('lt.id_leaderboard', '=', 'ltp.id_leaderboard');
            })
            ->leftJoin('team as t', function($join){
                $join->on('t.team_id', '=', 'ltp.id_team_leaderboard');

             })
			->where('leaderboard_score_match.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_match.qualification_sequence', '0')
			->where('lt.qualification_sequence', '0')
			->where('lt.urutan_grup', $leaderboard->group_sequence)
			->where('leaderboard_score_match.group_sequence', $leaderboard->group_sequence)
			->groupBy('leaderboard_score_match.id_player')
			->orderBy('total_kill', 'desc')
			->limit($leaderboard->winner)
			->get();
    }

    public function getMostKillSequenceFullCustomSolo($leaderboard)
	{
		return $this->selectRaw('SUM(leaderboard_score_match.jumlah_kill) as total_kill,lt.nama_team,lt.nama_team,ud.image,leaderboard_score_match.id_player, ltp.player_name, ltp.id_team_leaderboard as id_team')
			->leftJoin('leaderboard_team_player as ltp', function ($join) use ($leaderboard) {
				$join->on('ltp.id_teamplayer_leaderboard', '=', 'leaderboard_score_match.id_player');
				$join->on('ltp.id_leaderboard', '=', 'leaderboard_score_match.id_leaderboard');
			})
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'ltp.id_team_leaderboard');
				$join->on('lt.id_leaderboard', '=', 'ltp.id_leaderboard');
            })

            ->leftJoin('users_detail as ud', function ($join) {
                $join->on('ltp.id_team_leaderboard', '=', 'ud.user_id');

            })
			->where('leaderboard_score_match.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_match.qualification_sequence', '0')
			->where('lt.qualification_sequence', '0')
			->where('lt.urutan_grup', $leaderboard->group_sequence)
			->where('leaderboard_score_match.group_sequence', $leaderboard->group_sequence)
			->groupBy('leaderboard_score_match.id_player')
			->orderBy('total_kill', 'desc')
			->limit($leaderboard->winner)
			->get();
    }


    public function Leaderboard()
    {
        # code...
        return $this->hasOne(LeaderboardModel::class,"id_leaderboard","id_leaderboard")->select(['id_leaderboard','tournament_name','game_competition','tournament_category']);

    }

    public function LeaderboardScoreSequence()
    {
        # code...
        return $this->hasOne(LeaderboardScoreSequenceModel::class,"id_score_sequence","id_urutan_team")->select(['id_score_sequence','urutan']);
    }


	public function getMostKillSequence($leaderboard)
	{
		return $this->selectRaw('SUM(leaderboard_score_match.jumlah_kill) as total_kill,lt.nama_team,lt.nama_team,t.team_logo,leaderboard_score_match.id_player, ltp.player_name, ltp.id_team_leaderboard as id_team')
			->leftJoin('leaderboard_team_player as ltp', function ($join) use ($leaderboard) {
				$join->on('ltp.id_teamplayer_leaderboard', '=', 'leaderboard_score_match.id_player');
				$join->on('ltp.id_leaderboard', '=', 'leaderboard_score_match.id_leaderboard');
			})
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'ltp.id_team_leaderboard');
				$join->on('lt.id_leaderboard', '=', 'ltp.id_leaderboard');
            })
            ->leftJoin('team as t', function($join){
                $join->on('t.team_id', '=', 'ltp.id_team_leaderboard');

             })
			->where('leaderboard_score_match.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_match.qualification_sequence', '0')
			->where('lt.qualification_sequence', '0')
			->where('leaderboard_score_match.group_sequence', $leaderboard->group_sequence)
			->groupBy('leaderboard_score_match.id_player')
			->orderBy('total_kill', 'desc')
			->limit($leaderboard->winner)
			->get();


    }
    public function getMostKillSequenceSolo($leaderboard)
	{
		return $this->selectRaw('SUM(leaderboard_score_match.jumlah_kill) as total_kill,lt.nama_team,lt.nama_team,ud.image,leaderboard_score_match.id_player, ltp.player_name, ltp.id_team_leaderboard as id_team')
			->leftJoin('leaderboard_team_player as ltp', function ($join) use ($leaderboard) {
				$join->on('ltp.id_teamplayer_leaderboard', '=', 'leaderboard_score_match.id_player');
				$join->on('ltp.id_leaderboard', '=', 'leaderboard_score_match.id_leaderboard');
			})
			->leftJoin('leaderboard_team as lt', function ($join) use ($leaderboard) {
				$join->on('lt.id_team_leaderboard', '=', 'ltp.id_team_leaderboard');
				$join->on('lt.id_leaderboard', '=', 'ltp.id_leaderboard');
            })
            ->leftJoin('users_detail as ud', function ($join) {
                $join->on('ltp.id_team_leaderboard', '=', 'ud.user_id');

            })
			->where('leaderboard_score_match.id_leaderboard', $leaderboard->id_leaderboard)
			->where('leaderboard_score_match.qualification_sequence', '0')
			->where('lt.qualification_sequence', '0')
			->where('leaderboard_score_match.group_sequence', $leaderboard->group_sequence)
			->groupBy('leaderboard_score_match.id_player')
			->orderBy('total_kill', 'desc')
			->limit($leaderboard->winner)
			->get();


    }
}
