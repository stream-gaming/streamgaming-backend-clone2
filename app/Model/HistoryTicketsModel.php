<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryTicketsModel extends Model
{
    //
    protected $table = 'history_tickets';
}
