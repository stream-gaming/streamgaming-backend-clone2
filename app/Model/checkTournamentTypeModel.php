<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class checkTournamentTypeModel extends Model
{
    public function check ($id_tournament,$formatT){
        $get = DB::table('all_tournament AS at');
            if($formatT == 'Bracket')
            $get->leftJoin('tournament AS t','t.id_tournament','=','at.id_tournament');
            if($formatT == 'Leaderboard')
            $get->leftJoin('leaderboard AS l','l.id_leaderboard','=','at.id_tournament');
        $get->where('at.id_tournament','=',$id_tournament);
        $result = $get->value('game_option');
    
        return $result;
    }
}
