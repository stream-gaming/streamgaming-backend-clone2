<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardQualificationModel extends Model
{
	protected $table 		= 'leaderboard_qualification';
	protected $primaryKey 	= 'id_qualification';
	protected $keyType 		= 'string';

	public function leaderboard()
	{
		return $this->belongsTo(LeaderboardModel::class, 'id_leaderboard', 'id_leaderboard');
	}

	public function getQualifyInfo($leaderboard)
	{
		return $this->where('qualification_sequence', $leaderboard->stage)
			->where('id_leaderboard', $leaderboard->id_leaderboard)
			->first();
	}
	public function getQualification($leaderboard)
	{
		return $this->where('id_leaderboard', $leaderboard->id_leaderboard)
			->with('leaderboard')
			// ->where('qualification_sequence', '!=', 0)
			->get();
	}
	public function getQualifyInfoStage($leaderboard)
	{
		return $this->where('qualification_sequence', $leaderboard->stage)
			->where('id_leaderboard', $leaderboard->id_leaderboard)
			->where('stage_status', '>', 0)
			->first();
    }
    public function getAvailableSlots($id_leaderboard, $category)
    {
        $result = $this->where('id_leaderboard',$id_leaderboard)
                        ->where('qualification_sequence', $category)
                        ->first(['total_team']);
        return $result['total_team'];
    }

    public function AvailableSlots()
    {
        return $this->belongsTo(LeaderboardModel::class,'id_leaderboard');
    }
}
