<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\HasOne;
// use Illuminate\Database\Eloquent\SoftDeletes;

class hasJoinedOtherTournamentModel  extends Model
{
    // use SoftDeletes;
    protected $table = 'player_payment';
    protected $primaryKey = 'id_player_payment';
    protected $keyType = 'string';
    // protected $dates = ['deleted_at'];
    public function hasJoinedOtherTournament($id_player,$tourDate){
            $get =  $this->leftjoin('tournament AS t', function ($join) {
                    $join->on('t.id_tournament', '=', 'player_payment.id_tournament')
                        ->whereNull('t.deleted_at');
                        // ->where('t.deleted_at', '=', null);
                    })
                    ->leftjoin('leaderboard AS l', function ($joinl) {
                            $joinl->on('l.id_leaderboard', '=', 'player_payment.id_tournament')
                            ->whereNull('l.deleted_at');
                                // ->where('l.deleted_at', '=', null);
                            })
                    ->where('player_payment.player_id','=',$id_player)
                    ->where('player_payment.status','!=','Cancelled')
                    ->where(function($query2) use ($tourDate){
                        $query2->where('t.status','!=','complete')
                                ->where('t.date','=',$tourDate)
                                ->orWhere('l.status_tournament','<',4)
                                ->where('l.tournament_date','=',$tourDate);
                    })

                    ->exists();
            return $get;
    }
}
