<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class LeaderboardScoreConfigModel extends Model
{

	protected $table 		= 'leaderboard_score_config';
	protected $primaryKey 	= 'id_score_config';
}
