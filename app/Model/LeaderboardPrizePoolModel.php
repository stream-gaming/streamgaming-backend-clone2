<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LeaderboardPrizePoolModel extends Model
{
    protected $table 		= 'leaderboard_prizepool';
	protected $primaryKey 	= 'id_prizepool';
    protected $guarded = ['id_prizepool'];
    public $timestamps = false;
}
