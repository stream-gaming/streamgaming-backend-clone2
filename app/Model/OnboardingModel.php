<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class OnboardingModel extends Model
{
       protected $table = "onboardings";
       protected $keyType = 'string';

       protected $fillable = [
              'user_id',
              'app',
              'status'
       ];
}
