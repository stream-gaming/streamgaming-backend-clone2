<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Validation\AutoCodeIdRound;

class BracketSemiFinal extends Model
{
    protected $table = 'tournament_semi_finals';
    public $timestamps   = false;

    public function getBracket($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_semi_finals.id_semi_finals,tournament_semi_finals.team1,tournament_semi_finals.team2,tournament_semi_finals.score1,tournament_semi_finals.score2,tournament_semi_finals.winner,tournament_semi_finals.date,tournament_semi_finals.id_data_kill')
                    ->leftJoin('tournament as t','tournament_semi_finals.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('tournament_semi_finals.team1','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('tournament_semi_finals.team2','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_semi_finals.id_tournament','=',$id_tournament)
                    ->orderBy('id_semi_finals','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getBracketAfter($id_tournament)
    {
        $get = $this->selectRaw('team_1.id_team AS id_team1,team_2.id_team AS id_team2,team_1.nama_team AS team_1,team_2.nama_team AS team_2,team_1.logo_team AS logo_1,team_2.logo_team AS logo_2,team_1.nama_kota AS nama_kota_1,team_2.nama_kota AS nama_kota_2,tournament_semi_finals.id_semi_finals,tournament_semi_finals.team1,tournament_semi_finals.team2,tournament_semi_finals.score1,tournament_semi_finals.score2,tournament_semi_finals.winner,tournament_semi_finals.date,tournament_semi_finals.id_data_kill')
                    ->leftJoin('tournament as t','tournament_semi_finals.id_tournament','=','t.id_tournament')
                    ->leftJoin('tournament_quarter_finals AS A','tournament_semi_finals.team1','=','A.id_quarter_finals')
                    ->leftJoin('tournament_quarter_finals AS B','tournament_semi_finals.team2','=','B.id_quarter_finals')
                    ->leftJoin('tournament_group AS team_1',function($join){
                        $join->on('A.winner','=','team_1.id_team')
                            ->on('team_1.id_group','=','t.id_group');
                    })
                    ->leftJoin('tournament_group AS team_2',function($join){
                        $join->on('B.winner','=','team_2.id_team')
                            ->on('team_2.id_group','=','t.id_group');
                    })
                    ->where('tournament_semi_finals.id_tournament','=',$id_tournament)
                    ->orderBy('id_semi_finals','asc')
                    ->get();

        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function getTeamInfo($id_round)
    {
        $get =  $this->selectRaw('a.nama_team AS a,b.nama_team AS b,a.logo_team AS logo_1,b.logo_team AS logo_2,a.nama_kota AS nama_kota_1,b.nama_kota AS nama_kota_2,tournament_semi_finals.id_semi_finals,tournament_semi_finals.team1,tournament_semi_finals.team2,tournament_semi_finals.score1,tournament_semi_finals.score2,tournament_semi_finals.winner as id_a,tournament_third_place.lose as id_b, tournament_semi_finals.date,tournament_semi_finals.id_data_kill')
                    ->join('tournament as t','tournament_semi_finals.id_tournament','=','t.id_tournament')
                    ->join('tournament_group AS a','tournament_semi_finals.winner','=','a.id_team')
                    ->join('tournament_group AS b','tournament_semi_finals.lose','=','b.id_team')
                    ->where('tournament_semi_finals.id_semi_finals','=',$id_round)
                    ->first();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeam($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_semi_finals.date,tournament_semi_finals.winner,tournament_semi_finals.lose,"Round4" As rounds,tournament_semi_finals.id_semi_finals AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_semi_finals.id_tournament','=','t.id_tournament')
        ->where(function($q) use ($id_team){
            $q->where("tournament_semi_finals.team1" , "=" , $id_team)
                ->orWhere("tournament_semi_finals.team2","=", $id_team);
        })
        ->whereNotNull('tournament_semi_finals.winner')
        ->where("tournament_semi_finals.winner","!=","")
        // ->where(function($a) {
        //     $a->where("tournament_semi_finals.team2","!=","")
        //     ->WhereNotNull('tournament_semi_finals.team2');
        // })
        ->orderBy('id_semi_finals','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }
    public function HistoryTeamNext($id_team){
        $get =  $this->selectRaw('t.tournament_name,tournament_semi_finals.date,tournament_semi_finals.winner,tournament_semi_finals.lose,"Round4" As rounds,tournament_semi_finals.id_semi_finals AS id_round,"'.$id_team.'"AS id_team')
        ->leftjoin('tournament as t','tournament_semi_finals.id_tournament','=','t.id_tournament')
        ->leftjoin('tournament_quarter_finals AS A','tournament_semi_finals.team1','=','A.id_quarter_finals')
        ->leftjoin('tournament_quarter_finals AS B','tournament_semi_finals.team2','=','B.id_quarter_finals')
        ->leftJoin('tournament_group AS C', function($join){
                $join->on('A.winner', '=', 'C.id_team');
                $join->on('C.id_group', '=', 't.id_group');
        })
        ->leftJoin('tournament_group AS D', function($joins){
            $joins->on('B.winner', '=', 'D.id_team');
            $joins->on('D.id_group', '=', 't.id_group');
        })
        ->whereNotNull('tournament_semi_finals.winner')
        ->where(function($q) use ($id_team){
            $q->where("A.winner" , "=" , $id_team)
                ->orWhere("B.winner","=", $id_team);
        })
        ->whereNotNull('A.winner')
        ->whereNotNull('B.winner')
        ->orderBy('tournament_semi_finals.id_semi_finals','ASC')
        ->get();
        if($get->count() == 0){
            return false;
        }
        else{
            return $get;
        }
    }

    public function dataMatchs()
    {
        return $this->hasOne(BracketDataMatchs::class,'id_data_kill','id_data_kill');
    }
    public function InsertSetting($data,$id_tournament){
        foreach ($data as $row) {
            $id_8[] = $row['id_quarter_finals'];
        }
        $hitung8    = count($id_8);
        $bagi8 	    = $hitung8 / 2 ;

        for ($i=0; $i < $hitung8 ; $i++) {
            if ($i%2 ==0) {
                $team4_1[] = $id_8[$i];
            }else{
                $team4_2[] = $id_8[$i];
            }
        }
        for ($i=0; $i < $bagi8; $i++) {
            $hash = (new AutoCodeIdRound)->AutoCode('tournament_semi_finals','SMF','id_semi_finals');
            $round4 = new BracketSemiFinal();
            $round4->id_semi_finals   = $hash;
            $round4->id_tournament    = $id_tournament;
            $round4->team1            = $team4_1[$i];
            $round4->team2            = $team4_2[$i];
            $round4->save();
        }
    }

}
