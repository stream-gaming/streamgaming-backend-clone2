<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentWinnerModel extends Model
{
    protected $table        = 'tournament_winner';
	protected $primaryKey   = 'id_winner';
    protected $keyType      = 'string';
    public $timestamps      = false;
}
