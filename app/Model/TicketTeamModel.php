<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketTeamModel extends Model
{
    //
    protected $table='ticket_team';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    public function team()
    {
        return $this->hasOne(TeamModel::class, 'team_id', 'id_team');
    }

}
