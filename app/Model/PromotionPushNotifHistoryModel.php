<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromotionPushNotifHistoryModel extends Model
{
    protected $table='promotion_push_notif_history';
    protected $guarded = [];
}
