<?php

namespace App\Model;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class IdentityGameModel extends Model
{
    use Compoships;

	protected $table 		= 'identity_ingame';
	protected $primaryKey 	= 'id';
	protected $keyType 		= 'string';
	public $incrementing 	= false;

	protected $fillable = ['id', 'users_id', 'game_id', 'id_ingame', 'username_ingame'];

	public function game()
	{
		return $this->hasOne(GameModel::class, 'id_game_list', 'game_id');
	}

	public function role()
	{
		return $this->hasMany(GameRoleModel::class, 'game_id', 'game_id');
	}

	public function usernameInGame($user_id, $game_id)
	{
		$username = $this->where('users_id', $user_id)
			->where('game_id', $game_id)
			->first();
		return (isset($username) ? $username->username_ingame : false);
	}

    public function usernameAndIdInGame($user_id, $game_id)
	{
		$username = $this->where('users_id', $user_id)
			->where('game_id', $game_id)
			->first();
		return (isset($username) ? $username : false);
	}

	public function team()
	{
		return $this->belongsTo(TeamModel::class, 'leader_id', 'users_id');
	}

    public function history()
    {
        return $this->hasMany(IdentityIngameHistoryModel::class,['user_id','game_id'],['users_id','game_id']);
    }

    public function history_banned()
    {
        return $this->hasOne(HistoryBannedModel::class, 'identity_id', 'id');
    }

	public function detail()
    {
        return $this->hasOne(UsersDetailModel::class, 'user_id', 'users_id');
    }

}
//keep
