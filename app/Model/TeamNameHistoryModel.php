<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeamNameHistoryModel extends Model
{
    protected $table = 'team_name_history';

    protected $fillable = [
        'team_id',
        'old_name',
        'new_name'
    ];

    public function team()
    {
        return $this->belongsTo(TeamModel::class,'team_id','team_id');
    }
}
