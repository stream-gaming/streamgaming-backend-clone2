<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvitationCode extends Model
{
    protected $fillable = [
        'team_id',
        'code',
        'expired_at'
    ];

    public function team()
    {
        return $this->belongsTo(TeamModel::class, 'team_id', 'team_id');
    }
}
