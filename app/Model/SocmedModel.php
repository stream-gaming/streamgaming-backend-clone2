<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class SocmedModel extends Model
{
    protected $table      = 'users_sosmed';
    protected $primaryKey = 'id';

    protected $hidden = [
        'users_id'
    ];

    protected $fillable = [
        'sosmed',
        'url',
        'users_id'
    ];

    //-Array Socmed
    public static $sosmed = ['facebook', 'instagram', 'youtube', 'tiktok'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
