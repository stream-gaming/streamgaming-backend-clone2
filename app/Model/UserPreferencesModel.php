<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPreferencesModel extends Model
{
	protected $table 		= 'users_preferences';
	protected $primaryKey 	= 'users_preferences_id';
	protected $keyType 		= 'string';

	protected $fillable = [
        'user_id',
        'users_preferences_id',
		'email_notification'
    ];

	public static function addPreferencesUsers($data)
	{
		// Initiate Class
		$user_preferences 							= new UserPreferencesModel();
		$user_preferences->users_preferences_id    	= uniqid();
		$user_preferences->user_id    				= $data->user_id;
		$user_preferences->save();

		return $user_preferences;
	}
}
