<?php

namespace App\Logging;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Illuminate\Support\Facades\Log;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Formatter\JsonFormatter;
use Monolog\Logger;

class CloudWatchApiLoggerFactory
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        try {

            $sdkParams = $config["sdk"];
            $tags = $config["tags"] ?? [];
            $name = $config["name"] ?? 'cloudwatchApi';

            // Instantiate AWS SDK CloudWatch Logs Client
            $client = new CloudWatchLogsClient($sdkParams);

            // Log group name, will be created if none
            $groupName = $config["group"];

            // Log stream name, will be created if none
            $streamName = $config["stream"];

            // Days to keep logs, 14 by default. Set to `null` to allow indefinite retention.
            $retentionDays = $config["retention"];
            $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 1000, $tags);
            $handler->setFormatter(new JsonFormatter());

            // Create a log channel
            $logger = new Logger($name);
            // Set handler
            $logger->pushHandler($handler);

            return $logger;
        } catch (\Exception $e) {
            Log::error([
                'message' => $e->getMessage(),
                'error' => $e
            ]);
        }

    }
}
