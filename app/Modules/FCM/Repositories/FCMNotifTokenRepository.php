<?php
namespace App\Modules\FCM\Repositories;

use App\Helpers\PushNotification;
use App\Model\FcmNotifTokenModel;
use App\Modules\FCM\Interfaces\FCMNotifTokenRepositoryInterface;

class FCMNotifTokenRepository implements FCMNotifTokenRepositoryInterface
{
    protected $fcmNotifTokenModel;

    public function __construct(FcmNotifTokenModel $fcmNotifTokenModel)
    {
        $this->fcmNotifTokenModel = $fcmNotifTokenModel;
    }

    public function chunkWithPushNotif($count, $data)
    {
        $notification = new PushNotification();
        //for mobile
        $notification->sendPushToMobile([
            'data' => [
                'title' => (string) $data->title,
                'body'  => (string) '',
                'thumbnail' => (string) $data->thumbnail,
                'destination' => (string) $data->destination,
            ]
        ], '/topics/global');

        //for web
        $notifToken = FcmNotifTokenModel::select('token')->where('device', 2)->orderBy('created_at', 'DESC')
        ->chunk(200, function ($tokens) use($notification, $data){

            $userToken = [];
            foreach ($tokens as $token) {
                array_push($userToken,$token->token);

            }

            $notification->multiSendPush2([
                'title' => (string) $data->title,
                'body'  => (string) '',
                'thumbnail' => (string) $data->thumbnail,
                'destination' => (string) $data->destination,
                'data'  => [
                    'title' => (string) $data->title,
                    'body'  => (string) '',
                    'thumbnail' => (string) $data->thumbnail,
                    'destination' => (string) $data->destination,
                ],
            ], $userToken);
        });

        return $notifToken;
    }
}
