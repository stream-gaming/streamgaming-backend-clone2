<?php

namespace App\Modules\FCM\Interfaces;

interface FCMNotifTokenRepositoryInterface
{
    public function chunkWithPushNotif($count, $data);
}
