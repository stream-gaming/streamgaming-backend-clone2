<?php
namespace App\Modules\Onboarding;

use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingStepInterface;

class UserTournamentOnboarding implements OnboardingStepInterface{

    public function updateProgress(User $user):bool{

        $update = UserOnboardingModel::where('user_id', $user->user_id)->update([
            'tournament_page' => OnboardingEnum::HAS_VISITED_TOURNAMENT
        ]);

        if($update){
            return true;
        }

        return false;
    }
}
