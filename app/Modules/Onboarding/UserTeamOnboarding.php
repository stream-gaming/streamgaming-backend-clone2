<?php
namespace App\Modules\Onboarding;

use App\Http\Controllers\Api\UserNotificationController;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingStepInterface;
use App\Modules\Onboarding\Validator\UserTeamOnboardingValidator;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserTeamOnboarding implements OnboardingStepInterface{

    public function updateProgress(User $user):bool{

        DB::beginTransaction();
        try{
            $validator = new UserTeamOnboardingValidator;
            if(!$validator->check($user)){
                $update = UserOnboardingModel::where('user_id', $user->user_id)->update([
                    'step' => OnboardingEnum::CREATE_OR_FIND_TEAM_STEP,
                    'status' => OnboardingEnum::FINISHED_STATUS
                ]);

                if($update){
                    //Send notification
                    new UserNotificationController('finish_onboarding', (object) [
                        'user_id' => $user->user_id,
                        'ticket_link' => 'klaim-tiket'
                    ]);
                    DB::commit();
                    return true;
                }
            }

            throw new Exception();

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            DB::rollBack();
            return false;
        }

    }
}
