<?php
namespace App\Modules\Onboarding\Validator;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingValidatorInterface;

class UserTeamOnboardingValidator implements OnboardingValidatorInterface{

    /**
     * Check user onboarding find/create team step status
     * return true if the user has passed the find/create team step
     * return false if the user has not passed the find/create team step
     * */
    public function check(User $user):bool{

        $userOnboarding = $user->userOnboarding;
        $preferenceStep = OnboardingEnum::ROLE_PREFERENCE_STEP;
        $progressStatus = OnboardingEnum::ONPROGRESS_STATUS;

        if($userOnboarding){
            if($userOnboarding->step == $preferenceStep && $userOnboarding->status == $progressStatus){
                return false;
            }
        }

        return true;
    }
}
