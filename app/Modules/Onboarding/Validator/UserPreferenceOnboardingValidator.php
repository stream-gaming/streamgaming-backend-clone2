<?php
namespace App\Modules\Onboarding\Validator;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingValidatorInterface;

class UserPreferenceOnboardingValidator implements OnboardingValidatorInterface{

    /**
     * Check user onboarding preference step status
     * return true if the user has passed the onboarding preference step
     * return false if the user has not passed the onboarding preference step
     * */
    public function check(User $user):bool{
        $userOnboarding = $user->userOnboarding;

        if($userOnboarding){
            $userGame = $user->game;
            $preferenceStep = OnboardingEnum::ROLE_PREFERENCE_STEP;
            $progressStep = OnboardingEnum::ONPROGRESS_STATUS;

            if($userOnboarding->step <= $preferenceStep && count($userGame) == 1 && $userOnboarding->status == $progressStep){
                return false;
            }
        }
        return true;
    }
}
