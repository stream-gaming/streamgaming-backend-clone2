<?php
namespace App\Modules\Onboarding\Validator;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingValidatorInterface;

class AddGameOnboardingValidator implements OnboardingValidatorInterface{

    /**
     * Check user onboarding add game step status
     * return true if the user has passed the onboarding add game step
     * return false if the user has not passed the onboarding add game step
     * */
    public function check(User $user):bool{

        $userOnboarding = $user->userOnboarding;

        if($userOnboarding){
            $step = OnboardingEnum::STARTING_STEP;
            $status = OnboardingEnum::ONPROGRESS_STATUS;

            if($userOnboarding->step == $step && $userOnboarding->status == $status){
                return false;
            }
        }

        return true;
    }
}
