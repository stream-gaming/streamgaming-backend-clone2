<?php
namespace App\Modules\Onboarding\Validator;

use App\Model\User;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingValidatorInterface;

class FinishActionOnboardingValidator implements OnboardingValidatorInterface{

    /**
     * Check user onboarding finish action
     * return true if the user proper to finish his step
     * return false if the user not proper to finish his step
     * */
    public function check(User $user):bool{
        $userOnboarding = $user->userOnboarding;

        if($userOnboarding){
            $preference = OnboardingEnum::TEAM_PREFERENCE;
            $onProgressStatus = OnboardingEnum::ONPROGRESS_STATUS;

            if($userOnboarding->preference == $preference && $userOnboarding->status == $onProgressStatus){
                return true;
            }
        }
        return false;
    }

}
