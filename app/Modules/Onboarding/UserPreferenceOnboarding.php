<?php
namespace App\Modules\Onboarding;

use App\Http\Controllers\Api\UserNotificationController;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingStepInterface;
use App\Modules\Onboarding\Validator\UserPreferenceOnboardingValidator;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserPreferenceOnboarding implements OnboardingStepInterface{
    protected $preference;

    public function __construct($preference){
        $this->preference = $preference;
    }

    public function updateProgress(User $user):bool{

        DB::beginTransaction();
        try{
            $validator = new UserPreferenceOnboardingValidator;
            if(!$validator->check($user)){

                $data = [
                    'preference' => $this->preference,
                    'step' => OnboardingEnum::ROLE_PREFERENCE_STEP
                ];

                //Set finished status for solo preference
                if($this->preference == OnboardingEnum::SOLO_PREFERENCE){
                    $data['status'] = OnboardingEnum::FINISHED_STATUS;
                    //Send notification
                    new UserNotificationController('finish_onboarding',(object) [
                        'user_id' => $user->user_id,
                        'ticket_link' => 'klaim-tiket'
                    ]);
                }

                $update = UserOnboardingModel::where('user_id', $user->user_id)->update($data);
                if($update){
                    DB::commit();
                    return true;
                }

            }

            throw new Exception('Validator error for user with id '.$user->user_id);

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            DB::rollBack();
            return false;
        }

    }
}
