<?php
namespace App\Modules\Onboarding;

use App\Http\Controllers\Api\UserNotificationController;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Modules\Onboarding\Interfaces\OnboardingInterface;
use App\Modules\Onboarding\Interfaces\OnboardingStepInterface;
use App\Modules\Onboarding\Validator\FinishActionOnboardingValidator;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserOnboarding implements OnboardingInterface{
    protected $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function set():bool{

        $userOnboarding = UserOnboardingModel::create([
            'user_id' => $this->user->user_id,
        ]);

        if($userOnboarding) return true;

        return false;
    }

    public function update(OnboardingStepInterface $onboardingStep):bool{

          $progress = $onboardingStep->updateProgress($this->user);
          if($progress){
              return true;
          }

        return false;
    }

    public function finish():bool{

        DB::beginTransaction();
        try{
            $validator = new FinishActionOnboardingValidator;
            if($validator->check($this->user)){

                $updateStatus = UserOnboardingModel::where('user_id', $this->user->user_id)
                ->update([
                    'status' => OnboardingEnum::FINISHED_STATUS,
                    'step' => OnboardingEnum::CREATE_OR_FIND_TEAM_STEP
                ]);


                if($updateStatus){
                    //Send notification
                    new UserNotificationController('finish_onboarding', (object) [
                        'user_id' => $this->user->user_id,
                        'ticket_link' => 'klaim-tiket'
                    ]);
                    DB::commit();
                    return true;
                }
            }

            throw new Exception();

        }catch(Exception $e){
            //Get and Save error log
            Log::debug($e->getMessage());

            DB::rollBack();
            return false;
        }


    }

    public function skip():bool{

        $updateStatus = UserOnboardingModel::where('user_id', $this->user->user_id)
        ->update(['status' => OnboardingEnum::SKIPPED_STATUS]);

        if($updateStatus){
            return true;
        }

        return false;
    }

}
