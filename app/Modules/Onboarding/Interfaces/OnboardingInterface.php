<?php
namespace App\Modules\Onboarding\Interfaces;

Interface OnboardingInterface{
    public function set():bool;
    public function finish():bool;
    public function skip():bool;
    public function update(OnboardingStepInterface $onboarding):bool;
}
