<?php
namespace App\Modules\Onboarding\Interfaces;

use App\Model\User;

Interface OnboardingValidatorInterface{
    public function check(User $user):bool;
}
