<?php
namespace App\Modules\Onboarding\Interfaces;

use App\Model\User;

Interface OnboardingStepInterface{
    public function updateProgress(User $user):bool;
}
