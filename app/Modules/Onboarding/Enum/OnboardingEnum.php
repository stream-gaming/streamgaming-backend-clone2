<?php
namespace App\Modules\Onboarding\Enum;

class OnboardingEnum{

    //Onboarding steps
    const STARTING_STEP = 0;
    const ADD_GAME_STEP = 1;
    const ROLE_PREFERENCE_STEP = 2;
    const CREATE_OR_FIND_TEAM_STEP = 3;

    //onboarding status
    const ONPROGRESS_STATUS = 0;
    const FINISHED_STATUS = 1;
    const SKIPPED_STATUS = 2;

    //User preference
    const SOLO_PREFERENCE = 1;
    const TEAM_PREFERENCE = 2;
    const UNDEFINED_PREFERENCE = 0;

    //User Tournament Page
    const NOT_VISITED_TOURNAMENT = 0;
    const HAS_VISITED_TOURNAMENT = 1;
}
