<?php
namespace App\Modules\Onboarding;

use App\Modules\Onboarding\Enum\OnboardingEnum;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Interfaces\OnboardingStepInterface;
use App\Modules\Onboarding\Validator\AddGameOnboardingValidator;

class UserAddGameOnboarding implements OnboardingStepInterface{

    public function updateProgress(User $user):bool{

        $validator = new AddGameOnboardingValidator;
        if(!$validator->check($user)){

            $update = UserOnboardingModel::where('user_id', $user->user_id)->update([
                'step' => OnboardingEnum::ADD_GAME_STEP
            ]);

            if($update){
                return true;
            }

        }

        return false;
    }
}
