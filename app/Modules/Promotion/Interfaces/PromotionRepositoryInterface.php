<?php

namespace App\Modules\Promotion\Interfaces;

use App\Model\User;

interface PromotionRepositoryInterface
{
    public function get();
    public function getByUserId(User $user);
    public function findById($id);
    public function count();
    public function whereDoesntHaveUsers(User $user): array;
}
