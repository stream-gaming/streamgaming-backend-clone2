<?php

namespace App\Modules\Promotion\Interfaces;

use App\Model\PromotionModel;
use App\Model\User;

interface UserPromotionRepositoryInterface
{
    public function all(User $user);
    public function count(User $user);
    public function findByPromoIdAndUserId($promotionId, $userId);
    public function updateRead($promotionId, User $user);
    public function updateDelete($promotionId, User $user);
    public function updateAll(User $user);

}
