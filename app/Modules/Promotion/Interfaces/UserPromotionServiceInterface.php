<?php

namespace App\Modules\Promotion\Interfaces;

use App\Model\PromotionModel;
use App\Model\User;

interface UserPromotionServiceInterface
{
    /**
     * Get all user's promotion
     *
     * @param User $user
     */
    public function getAllUserPromotion(User $user);
    /**
     * Read specific user's promotion
     *
     * @param PromotionModel $promotion
     * @param User $user
     * @return void
     */
    public function readUserPromotion(PromotionModel $promotion, User $user);
    /**
     * Read all of user's promotion
     *
     * @param User $user
     * @return void
     */
    public function readAllUserPromotion(User $user);
    /**
     * Delete specific user's promotion
     *
     * @param PromotionModel $promotion
     * @param User $user
     * @return void
     */
    public function deleteUserPromotion(PromotionModel $promotion, User $user);
}
