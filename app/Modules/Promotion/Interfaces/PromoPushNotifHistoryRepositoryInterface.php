<?php

namespace App\Modules\Promotion\Interfaces;

interface PromoPushNotifHistoryRepositoryInterface
{
    public function findById($promotionId);
    public function increaseAttempt($promotionId);
    public function create($promotionId);
}
