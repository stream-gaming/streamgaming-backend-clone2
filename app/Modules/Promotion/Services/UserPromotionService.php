<?php
namespace App\Modules\Promotion\Services;

use App\Helpers\Firebase\FirebaseFactoryInterface;
use App\Model\User;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use App\Modules\Promotion\Interfaces\UserPromotionRepositoryInterface;
use App\Modules\Promotion\Interfaces\UserPromotionServiceInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use Symfony\Component\Translation\Exception\InvalidResourceException;

class UserPromotionService implements UserPromotionServiceInterface
{
    protected $userPromotionRepo;
    protected $firebaseDBInstance;
    protected $promotionRepo;

    public function __construct(
        UserPromotionRepositoryInterface $userPromotionRepo,
        FirebaseFactoryInterface $firebaseDBInstance,
        PromotionRepositoryInterface $promotionRepo
        )
    {
        $this->userPromotionRepo = $userPromotionRepo;
        $this->firebaseDBInstance = $firebaseDBInstance;
        $this->promotionRepo = $promotionRepo;
    }

    /**
     * Get all user's promotion
     */
    public function getAllUserPromotion(User $user)
    {
        $userPromotion = $this->userPromotionRepo->all($user);
        return $userPromotion;
    }

    /**
     * Read specific user's promotion
     *
     * @param $promotionId
     * @param User $user
     * @return void
     */
    public function readUserPromotion($promotionId, User $user)
    {
        $userPromotionDetail = $this->userPromotionRepo->findByPromoIdAndUserId($promotionId, $user->user_id);
        if($userPromotionDetail)
        {
            if($userPromotionDetail->action_delete == 1)throw new ModelNotFoundException(Lang::get('exeptions.model_not_found'), 404);
        }

        //update user's promotion read state on database
        $userPromotion = $this->userPromotionRepo->updateRead($promotionId, $user);
        //update counter promotion on firebase
        $this->firebaseDBInstance->updateData($user, [
            'seen_promotion_counter' => $this->userPromotionRepo->count($user)
        ]);

        if(!$userPromotion) throw new InvalidResourceException('Failed to update data');

        return true;
    }

    /**
     * Read all of user's promotion
     *
     * @param User $user
     * @return void
     */
    public function readAllUserPromotion(User $user)
    {
        //update all of user's promotion read state on database
        $userPromotion = $this->userPromotionRepo->updateAll($user);
        //update counter promotion on firebase
        $this->firebaseDBInstance->updateData($user, [
            'seen_promotion_counter' => $this->promotionRepo->count()
        ]);

        if(!$userPromotion) throw new InvalidResourceException('Failed to update data');

        return true;
    }

    /**
     * Delete specific user's promotion
     *
     * @param $promotionId
     * @param User $user
     * @return void
     */
    public function deleteUserPromotion($promotionId, User $user)
    {
        $userPromotion = $this->userPromotionRepo->updateDelete($promotionId, $user);

        //update counter promotion on firebase
        $this->firebaseDBInstance->updateData($user, [
            'seen_promotion_counter' => $this->userPromotionRepo->count($user)
        ]);

        if(!$userPromotion) throw new InvalidResourceException('Failed to update data');

        return true;
    }
}
