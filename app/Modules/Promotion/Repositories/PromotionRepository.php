<?php
namespace App\Modules\Promotion\Repositories;

use App\Model\PromotionModel;
use App\Model\User;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class PromotionRepository implements PromotionRepositoryInterface
{
    protected $model;

    public function __construct(PromotionModel $model)
    {
        $this->model = $model;
    }

    public function get()
    {
        return $this->model->where('status', 1)->get();
    }

    public function getByUserId(User $user)
    {
        $promotion = $this->model->select(
            'promotion.id',
            'promotion.title',
            'promotion.link',
            'promotion.created_at',
            'user_promotion.action_read',
            'user_promotion.action_delete'
            )
            ->leftJoin('user_promotion', function ($join) use($user){
                $join->on('promotion.id', '=', 'user_promotion.promotion_id')
                    ->where('user_promotion.user_id', '=', $user->user_id);
            })
            ->where('promotion.status', 1)
            ->where(function($q){
                $q->where('user_promotion.action_delete', '!=', 1)
                ->orWhere('user_promotion.action_delete', null);
            })
            ->orderBy('promotion.created_at', 'DESC')
            ->get();

            return $promotion;
    }

    public function findById($id)
    {
        $promotion = $this->model->where('status', 1)->findOrFail($id);
        return $promotion;
    }

    public function count()
    {
        return $this->model->where('status', 1)->count();
    }

    public function whereDoesntHaveUsers(User $user): array
    {
        $promotion = PromotionModel::where('status', 1)->whereDoesntHave('users', function(Builder $q) use($user){
            $q->where('user_promotion.user_id', $user->user_id);
        })
        ->get()->map(function($promotion) use($user){
            return [
                'promotion_id' => $promotion->id,
                'user_id' => $user->user_id,
                'action_read' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        })->toArray();

        return $promotion;
    }
}
