<?php
namespace App\Modules\Promotion\Repositories;

use App\Model\User;
use App\Model\UserPromotionModel;
use App\Modules\Promotion\Interfaces\PromotionRepositoryInterface;
use App\Modules\Promotion\Interfaces\UserPromotionRepositoryInterface;

class UserPromotionRepository implements UserPromotionRepositoryInterface
{
    protected $model;
    protected $promotionRepo;

    public function __construct(UserPromotionModel $userPromotionModel, PromotionRepositoryInterface $promotionRepo)
    {
        $this->model = $userPromotionModel;
        $this->promotionRepo = $promotionRepo;
    }

    public function count(User $user)
    {
        return $this->model->where('user_id', $user->user_id)->count();
    }

    public function all(User $user)
    {
        $userPromotion = $this->promotionRepo->getByUserId($user);

        return $userPromotion;
    }

    public function findByPromoIdAndUserId($promotionId, $userId)
    {
        return $this->model->where('promotion_id', $promotionId)
        ->where('user_id', $userId)
        ->first();
    }

    public function updateRead($promotionId, User $user)
    {
        $data = $this->model->where('action_delete', '!=', 1)->firstOrCreate([
            'promotion_id' => $promotionId,
            'user_id' => $user->user_id,
        ])->update([
            'action_read' => 1
        ]);

        return $data;
    }

    public function updateDelete($promotionId, User $user)
    {
        $data = $this->model->firstOrCreate([
            'promotion_id' => $promotionId,
            'user_id' => $user->user_id,
        ])->update([
            'action_delete' => 1
        ]);

        return $data;
    }

    public function updateAll(User $user)
    {
        $promotion = $this->promotionRepo->whereDoesntHaveUsers($user);

        $data = $this->model->insert($promotion);
        return $data;
    }

}
