<?php
namespace App\Modules\Promotion\Repositories;

use App\Model\PromotionPushNotifHistoryModel;
use App\Modules\Promotion\Interfaces\PromoPushNotifHistoryRepositoryInterface;

class PromoPushNotifHistoryRepository implements PromoPushNotifHistoryRepositoryInterface{

    protected $promoPushNotifHistoryModel;

    public function __construct(PromotionPushNotifHistoryModel $promoPushNotifHistoryModel)
    {
        $this->promoPushNotifHistoryModel = $promoPushNotifHistoryModel;
    }

    public function findById($promotionId)
    {
        return $this->promoPushNotifHistoryModel->where('promotion_id', $promotionId)->first();
    }

    public function increaseAttempt($promotionId)
    {
        return $this->promoPushNotifHistoryModel->where('promotion_id', $promotionId)
        ->increment('attempt');
    }

    public function create($promotionId)
    {
        return $this->promoPushNotifHistoryModel->create([
            'promotion_id' => $promotionId,
            'attempt' => 0
        ]);
    }
}
