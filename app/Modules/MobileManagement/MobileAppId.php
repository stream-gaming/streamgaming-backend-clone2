<?php

namespace App\Modules\MobileManagement;

class MobileAppId {

    public function get($device = 'android') :int
    {
        if (method_exists(MobileAppId::class, $device)) {
            return $this->$device();
        }

        return 0;
    }

    public function android() :int
    {
        return MobileEnum::android;
    }

    public function ios() :int
    {
        return MobileEnum::ios;
    }
}
