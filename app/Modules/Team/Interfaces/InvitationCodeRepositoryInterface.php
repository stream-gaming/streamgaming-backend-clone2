<?php
namespace App\Modules\Team\Interfaces;

use App\Model\InvitationCode;

interface InvitationCodeRepositoryInterface
{
    public function findByCode($invCode): InvitationCode;
    public function findByCodeNoException($invCode);
    public function findByCodeAndTeamId($invCode, $teamId);
}
