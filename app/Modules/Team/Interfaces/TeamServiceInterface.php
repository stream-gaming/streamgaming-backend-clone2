<?php
namespace App\Modules\Team\Interfaces;

use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\User;

interface TeamServiceInterface{
    public function joinUsingInvCode(InvitationCode $invitationCode, User $user);
    public function validateUserJoin(TeamModel $team, User $user);
}
