<?php
namespace App\Modules\Team\Interfaces;

interface ICheckOrFailAbleValidator {
    /**
     * return void for valid condition. Example: if invitation code not expired return void
     * throw for invalid condition. Example: if invitation code expired, throw
     */
    public function checkOrFail();
}
