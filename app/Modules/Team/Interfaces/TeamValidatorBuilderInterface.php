<?php
namespace App\Modules\Team\Interfaces;

use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\User;

interface TeamValidatorBuilderInterface{
    public function joinUsingInvCode(TeamModel $team, User $user, InvitationCode $invCode);
    public function joinTeam(TeamModel $team, User $user);
}
