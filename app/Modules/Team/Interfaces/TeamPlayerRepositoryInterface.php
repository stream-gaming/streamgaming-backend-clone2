<?php
namespace App\Modules\Team\Interfaces;

interface TeamPlayerRepositoryInterface{
    public function addToMainSlot($teamId, $userId);
    public function addToSubstituteSlot($teamId, $userId);
    public function getAllTeamPlayer($teamId);
    public function getPrimaryPlayer($teamId);
    public function getSubstitutePlayer($teamId);

}
