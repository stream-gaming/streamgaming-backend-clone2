<?php
namespace App\Modules\Team\Interfaces;

interface ICheckAbleValidator {
    /**
     * return true for valid condition. Example: if invitation code not expired return true
     * return false for invalid condition. Example: if invitation code expired return false
     */
    public function check();
}
