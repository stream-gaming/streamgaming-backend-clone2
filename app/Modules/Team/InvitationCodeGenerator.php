<?php

namespace App\Modules\Team;

use App\Model\InvitationCode;
use App\Model\TeamModel;
use Carbon\Carbon;
use Illuminate\Support\Str;

class InvitationCodeGenerator {
    static function create(TeamModel $teamModel) :InvitationCode
    {
        $code = self::generateRandomUnique(7);

        $invitationCode =  InvitationCode::updateOrCreate([
            'team_id' => $teamModel->team_id
        ],
        [
            'code' => $code,
            'expired_at' => Carbon::now()->addHours(6)->toDateTimeString(),
        ]);

        return $invitationCode;

    }

    static function generateRandomUnique(int $length) :string
    {
        $code = strtoupper(Str::random($length));

        if (InvitationCode::where('code', $code)->count()) {
            return self::generateRandomUnique($length);
        } else {
            return $code;
        }
    }
}
