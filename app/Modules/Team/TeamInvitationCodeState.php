<?php

namespace App\Modules\Team;

use App\Model\TeamModel;
use App\Modules\Team\Validators\TeamBanStatusValidator;
use App\Modules\Team\Validators\TournamentJoinStatusValidator;

class TeamInvitationCodeState {
    const BANNED = 'BANNED';
    const IN_TOURNAMENT = 'IN_TOURNAMENT';
    const ACTIVE = 'ACTIVE';

    static function getCurrentState(TeamModel $teamModel)
    {
        if (!(new TournamentJoinStatusValidator($teamModel))->check()) {
            return self::IN_TOURNAMENT;
        }

        if(!(new TeamBanStatusValidator($teamModel))->check()) {
            return self::BANNED;
        }

        return self::ACTIVE;
    }
}
