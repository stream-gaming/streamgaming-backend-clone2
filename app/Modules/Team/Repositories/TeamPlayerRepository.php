<?php
namespace App\Modules\Team\Repositories;

use App\Model\TeamPlayerModel;
use App\Modules\Team\Interfaces\TeamPlayerRepositoryInterface;

class TeamPlayerRepository implements TeamPlayerRepositoryInterface{


    public function addToMainSlot($teamId, $userId)
    {
        $teamPlayer = TeamPlayerModel::where('team_id', $teamId)->first();
        //Update new main player formation
        $mainPlayer = (empty($teamPlayer->player_id)) ? [] : explode(',' ,$teamPlayer->player_id);
        array_push($mainPlayer, $userId);
        $teamPlayer->player_id = implode(',', $mainPlayer);

        return $teamPlayer->save();
    }

    public function addToSubstituteSlot($teamId, $userId)
    {
        $teamPlayer = TeamPlayerModel::where('team_id', $teamId)->first();
        //Update new substitute player formation
        $substitutePlayer = (empty($teamPlayer->substitute_id)) ? [] : explode(',' ,$teamPlayer->substitute_id);
        array_push($substitutePlayer, $userId);
        $teamPlayer->substitute_id = implode(',', $substitutePlayer);

        return $teamPlayer->save();
    }

    public function getAllTeamPlayer($teamId)
    {
        $teamPlayer = TeamPlayerModel::where('team_id', $teamId)->first();

        $mainPlayer = (empty($teamPlayer->player_id)) ? [] : explode(',' ,$teamPlayer->player_id);
        $substitutePlayer = (empty($teamPlayer->substitute_id)) ? [] : explode(',' ,$teamPlayer->substitute_id);
        $allPlayer = collect($mainPlayer)->merge($substitutePlayer)->all();

        return $allPlayer;
    }

    public function getPrimaryPlayer($teamId)
    {
        $teamPlayer = TeamPlayerModel::where('team_id', $teamId)->first();

        return (empty($teamPlayer->player_id)) ? [] : explode(',' ,$teamPlayer->player_id);
    }

    public function getSubstitutePlayer($teamId)
    {
        $teamPlayer = TeamPlayerModel::where('team_id', $teamId)->first();

        return (empty($teamPlayer->substitute_id)) ? [] : explode(',' ,$teamPlayer->substitute_id);
    }

}
