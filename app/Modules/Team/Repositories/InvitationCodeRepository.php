<?php
namespace App\Modules\Team\Repositories;

use App\Model\InvitationCode;
use App\Modules\Team\Interfaces\InvitationCodeRepositoryInterface;

class InvitationCodeRepository implements InvitationCodeRepositoryInterface
{
    protected $invCodeModel;

    public function __construct(InvitationCode $invCodeModel)
    {
        $this->invCodeModel = $invCodeModel;
    }

    public function findByCode($invCode) : InvitationCode
    {
        return $this->invCodeModel->where('code', strtoupper($invCode))->firstOrFail();
    }

    public function findByCodeNoException($invCode)
    {
        return $this->invCodeModel->where('code', strtoupper($invCode))->first();
    }

    public function findByCodeAndTeamId($invCode, $teamId)
    {
        return $this->invCodeModel->where('team_id', $teamId)->where('code', strtoupper($invCode))->first();
    }
}
