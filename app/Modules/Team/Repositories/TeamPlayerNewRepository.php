<?php

namespace App\Modules\Team\Repositories;

use App\Model\TeamPlayerNewModel;
use App\Modules\Team\Interfaces\TeamPlayerNewRepositoryInterface;

class TeamPlayerNewRepository implements TeamPlayerNewRepositoryInterface {

    public function addToMainSlot($teamId, $userId)
    {
        return TeamPlayerNewModel::create([ 'team_id' => $teamId, 'player_id' => $userId, 'player_status' => 2 ]);
    }

    public function addToSubstituteSlot($teamId, $userId)
    {
        return TeamPlayerNewModel::create([ 'team_id' => $teamId, 'player_id' => $userId, 'player_status' => 3 ]);
    }

    public function getAllTeamPlayer($teamId)
    {
        return TeamPlayerNewModel::where('team_id', $teamId)->get();

    }

    public function getPrimaryPlayer($teamId)
    {
        return TeamPlayerNewModel::where('team_id', $teamId)->where('player_status', '<', 3)->get();
    }

    public function getSubstitutePlayer($teamId)
    {
        return TeamPlayerNewModel::where('team_id', $teamId)->where('player_status', '=', 3)->get();
    }
}
