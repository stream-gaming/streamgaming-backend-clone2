<?php
namespace App\Modules\Team\Validators;

use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class HavingTeamValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator{
    protected $team;
    protected $user;

    public function __construct(TeamModel $team, User $user) {
        $this->team = $team;
        $this->user = $user;
    }

    public function check(){
        $TeamHelper = new TeamHelperController;

        $hasTeam = $TeamHelper->hasTeam($this->team->game_id, $this->user->user_id);
        if (!is_null($hasTeam)) { //check if this user has team in this game
            return false;
        }

        return true;
    }

    public function checkOrFail(){
        if(!$this->check()){
            throw new Exception(Lang::get('team.has_team'), 1);
        }
    }
}
