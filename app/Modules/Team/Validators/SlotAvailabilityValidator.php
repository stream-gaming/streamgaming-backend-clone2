<?php
namespace App\Modules\Team\Validators;

use App\Model\TeamModel;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class SlotAvailabilityValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator{
    protected $team;

    public function __construct(TeamModel $team) {
        $this->team = $team;
    }

    public function check(){
        $teamGame = $this->team->game;
        $teamSlot = $this->team->countMember();

        return ($teamGame->player_on_team + $teamGame->substitute_player) > $teamSlot;
    }

    public function checkOrFail(){
        if(!$this->check()){
            throw new Exception(Lang::get('team.slot_availability.full'), 1);
        }
    }
}
