<?php
namespace App\Modules\Team\Validators;

use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class LeaderTeamValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator {

    protected $team;
    protected $user;

    public function __construct(TeamModel $teamModel, User $user) {
        $this->team = $teamModel;
        $this->user = $user;
    }

    public function check() :bool
    {
        return $this->team->leader_id == $this->user->user_id;
    }

    public function checkOrFail()
    {
        if (!$this->check()) {
            throw new Exception(Lang::get('message.team.leader'), 1);
        }
    }
}
