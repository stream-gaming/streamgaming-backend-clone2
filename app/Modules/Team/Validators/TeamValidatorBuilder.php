<?php
namespace App\Modules\Team\Validators;

use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Interfaces\TeamValidatorBuilderInterface;

class TeamValidatorBuilder implements TeamValidatorBuilderInterface{


    public function joinUsingInvCode(TeamModel $team, User $user,  InvitationCode $invCode){
        $validators = [
            new InvitationCodeExpValidator($invCode),
            new SlotAvailabilityValidator($team),
            new RelatedGameValidator($team, $user),
            new TournamentJoinStatusValidator($team),
            new HavingTeamValidator($team, $user),
            new HavingTeamV2Validator($team, $user),
        ];

        foreach($validators as $validator){
            $validator->checkOrFail();
        }

    }

    public function joinTeam(TeamModel $team, User $user)
    {
        $validators = [
            new RelatedGameValidator($team, $user),
            new HavingTeamValidator($team, $user),
            new HavingTeamV2Validator($team, $user),
        ];

        foreach($validators as $validator){
            $validator->checkOrFail();
        }
    }


}
