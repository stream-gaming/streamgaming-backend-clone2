<?php

namespace App\Modules\Team\Validators;

use App\Model\TeamModel;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Lang;

class TeamBanStatusValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator {

    protected $team;

    public function __construct(TeamModel $teamModel) {
        $this->team = $teamModel;
    }

    public function check()
    {
        return !boolval($this->team->is_banned);
    }

    public function checkOrFail()
    {
        if (!$this->check()) {
            throw new Exception(Lang::get('message.team.banned',  [
                'time'=> $this->team->ban_until != 'forever' ? Carbon::parse($this->team->ban_until)->diffForHumans() :
                                                        'forever'
            ]), 1);

        }
    }
}
