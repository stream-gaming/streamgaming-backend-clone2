<?php
namespace App\Modules\Team\Validators;

use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class HavingTeamV2Validator implements ICheckAbleValidator, ICheckOrFailAbleValidator{
    protected $team;
    protected $user;

    public function __construct(TeamModel $team, User $user) {
        $this->team = $team;
        $this->user = $user;
    }

    public function check(){

        $hasTeam = TeamModel::leftJoin('team_player_new', 'team.team_id', '=', 'team_player_new.team_id')
        ->where('player_id', $this->user->user_id)
        ->where('game_id', $this->team->game_id)
        ->count();

        //check if this user has team in this game
        return $hasTeam <= 0;
    }

    public function checkOrFail(){
        if(!$this->check()){
            throw new Exception(Lang::get('team.has_team'), 1);
        }
    }
}
