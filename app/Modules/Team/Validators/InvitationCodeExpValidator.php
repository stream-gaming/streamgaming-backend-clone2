<?php

namespace App\Modules\Team\Validators;

use App\Model\InvitationCode;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Lang;

class InvitationCodeExpValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator {

    protected $invitationCode;

    public function __construct(InvitationCode $invitationCode) {
        $this->invitationCode = $invitationCode;
    }

    public function check() :bool
    {
        return !Carbon::now()->greaterThan($this->invitationCode->expired_at);
    }

    public function checkOrFail()
    {
        if (!$this->check()) {
            throw new Exception(Lang::get('team.invitation_code.expired'), 1);
        }
    }
}
