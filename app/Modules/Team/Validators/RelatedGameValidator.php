<?php
namespace App\Modules\Team\Validators;

use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\User;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class RelatedGameValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator {
    protected $team;
    protected $user;

    public function __construct(TeamModel $team, User $user) {
        $this->team = $team;
        $this->user = $user;
    }

    public function check(){

        $userGame = IdentityGameModel::where('game_id', $this->team->game_id)
                ->where('users_id', $this->user->user_id)
                ->count();

        return $userGame >= 1;
    }

    public function checkOrFail(){
        if(!$this->check()){
            throw new Exception(Lang::get('team.not_have_related_game'), 1);
        }
    }
}
