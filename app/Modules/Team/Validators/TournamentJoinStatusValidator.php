<?php
namespace App\Modules\Team\Validators;

use App\Helpers\Validation\TeamTournamentRegistrationStatus;
use App\Model\TeamModel;
use App\Modules\Team\Interfaces\ICheckAbleValidator;
use App\Modules\Team\Interfaces\ICheckOrFailAbleValidator;
use Exception;
use Illuminate\Support\Facades\Lang;

class TournamentJoinStatusValidator implements ICheckAbleValidator, ICheckOrFailAbleValidator{
    protected $team;

    public function __construct(TeamModel $team) {
        $this->team = $team;
    }

    public function check(){
        $tourJoinStatusChecker = new TeamTournamentRegistrationStatus;

        return $tourJoinStatusChecker->check($this->team->leader_id, $this->team->game_id) ? false : true;
    }

    public function checkOrFail(){
        if(!$this->check()){
            throw new Exception(Lang::get('team.join_tournament'), 1);
        }
    }

}
