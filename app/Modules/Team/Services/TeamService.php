<?php
namespace App\Modules\Team\Services;

use App\Helpers\Builders\NotificationBuilderInterface;
use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use App\Modules\Team\Interfaces\TeamPlayerNewRepositoryInterface;
use App\Modules\Team\Interfaces\TeamPlayerRepositoryInterface;
use App\Modules\Team\Interfaces\TeamServiceInterface;
use App\Modules\Team\Interfaces\TeamValidatorBuilderInterface;
use App\Traits\TeamTrait;
use Illuminate\Support\Facades\DB;


class TeamService implements TeamServiceInterface{

    use TeamTrait;

    protected $teamPlayerRepository;
    protected $teamPlayerNewRepository;
    protected $teamValidator;
    protected $notificationBuilder;

    public function __construct(
        TeamPlayerRepositoryInterface $teamPlayerRepository,
        TeamPlayerNewRepositoryInterface $teamPlayerNewRepository,
        TeamValidatorBuilderInterface $teamValidator,
        NotificationBuilderInterface $notificationBuilder
        )
    {
        $this->teamPlayerRepository = $teamPlayerRepository;
        $this->teamValidator = $teamValidator;
        $this->notificationBuilder = $notificationBuilder;
        $this->teamPlayerNewRepository = $teamPlayerNewRepository;
    }

    public function joinUsingInvCode(InvitationCode $invitationCode, User $user)
    {
        $team = TeamModel::where('team_id', $invitationCode->team_id)->firstOrFail();
        //Validate if this user matches the acceptance criteria
        $this->teamValidator->joinUsingInvCode($team, $user, $invitationCode);
        $playerOnTeam = (int) $team->game->player_on_team;

        DB::beginTransaction();

        //Old team player table
        if($playerOnTeam == count($this->teamPlayerRepository->getPrimaryPlayer($team->team_id)))
        {
            //Add to substitute player slot if team doesnt have main player slot left
            $newPlayer = $this->teamPlayerRepository->addToSubstituteSlot($team->team_id, $user->user_id);
        }else{
            //Add to main player slot if team has main player slot left
            $newPlayer = $this->teamPlayerRepository->addToMainSlot($team->team_id, $user->user_id);
        }

        //New team player table
        if(config('app.team_player_new'))
        {
            if($playerOnTeam == count($this->teamPlayerNewRepository->getPrimaryPlayer($team->team_id)))
            {
                //Add to substitute player slot if team doesnt have main player slot left
                $newPlayer = $this->teamPlayerNewRepository->addToSubstituteSlot($team->team_id, $user->user_id);
            }else{
                //Add to main player slot if team has main player slot left
                $newPlayer = $this->teamPlayerNewRepository->addToMainSlot($team->team_id, $user->user_id);
            }
        }

        //Update user request join status if exists
        TeamRequestModel::where('team_id', $team->team_id)
        ->where('users_id', $user->user_id)
        ->where('status', 1)
        ->update(['status' => 2]);

        //Send notification to user
        $this->notificationBuilder->setMethod('accept_join')->setMessage([
            'team_id' => $team->team_id,
            'users_id' => $user->user_id,
            'team_name' => $team->team_name,
        ])->sendNotif();

        //Destroy any related cache
        $this->destroyCacheForNewPlayer($team, $user, $this->teamPlayerNewRepository);

        //Commit if everything is ok
        $newPlayer ? DB::commit() : DB::rollBack();

        return $newPlayer;

    }

    public function validateUserJoin(TeamModel $team, User $user)
    {
        $userJoinTeamValidation = $this->teamValidator->joinTeam($team, $user);

        return $userJoinTeamValidation;
    }

}

