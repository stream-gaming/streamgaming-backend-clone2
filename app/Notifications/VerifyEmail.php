<?php

namespace App\Notifications;

use App\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Notifications\Action;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;

class VerifyEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;
    protected $status;

    public function __construct($status = "desktop")
    {
        $this->status = $status;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->status == 'mobile') {
            //- Url FrontEnd Redirect Mobile
            $verificationUrl = config('app.url_frontend') . "/app/" . config('app.url_verify'). "?verifyLink=";
        } else {
            //- Url FrontEnd
            $verificationUrl = config('app.url_frontend') . "/" . config('app.url_verify')."?verifyLink=";
        }
        //- Generate Url Dinamis
        $verificationUrl .= $this->verificationUrl($notifiable);
        //- Url Kebijakan Privasi
        $url = config('app.url_terms_condition');

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::get('email.notify.verify.subject'))
            ->greeting(Lang::get('email.notify.greeting') . $notifiable->email)
            ->line(Lang::get('email.notify.verify.one'))
            ->action(Lang::get('email.notify.button.verify'), $verificationUrl)
            ->line(Lang::get('email.notify.verify.two'))
            ->salutation(Lang::get('email.notify.verify.title'))
            ->line(($this->makeActionIntoLine(new Action(Lang::get('email.notify.verify.three'), $url))));
    }

    private function makeActionIntoLine(Action $action): Htmlable
    {
        return new class ($action) implements Htmlable
        {
            private $action;

            public function __construct(Action $action)
            {
                $this->action = $action;
            } // end __construct()

            public function toHtml()
            {
                return $this->strip($this->btn());
            } // end toHtml()

            private function btn()
            {
                return sprintf(
                    '<a
                        href="%s"
                        style="color: #00ff5b;"
                    >%s</a>',
                    htmlspecialchars($this->action->url),
                    htmlspecialchars($this->action->text)
                );
            } // end btn()

            private function strip($text)
            {
                return str_replace("\n", ' ', $text);
            } // end strip()

        };
    } // end makeActionIntoLine()


    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        //Expired Dalam 15 Menit
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 15)),
            [
                'id' => $notifiable['user_id'],
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
