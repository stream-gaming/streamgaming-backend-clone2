<?php

namespace Tests\Feature;

use App\Model\PlayerPayment;
use App\Model\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetMyTournamentFromPortalTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testgetMyTournamentFromPortalSuccess()
    {
        $user = factory(User::class)->create();

        factory(PlayerPayment::class)->create(['player_id' => $user->user_id, 'id_team' => $user->user_id]);
        $response = $this->post('/api/user/my-tournament-portal', ['user_id' => $user->user_id], []);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'data' => [
                'on_going'
            ],

        ]);
    }
}
