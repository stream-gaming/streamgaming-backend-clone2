<?php

namespace Tests\Feature;

use App\Model\FcmNotifTokenModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class SaveFcmTokenTest extends TestCase
{
    /**
     * Test Save token success scenario.
     *
     * @return void
     */
    public function testSaveTokenSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        FcmNotifTokenModel::truncate();
        Schema::enableForeignKeyConstraints();

        User::insert([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);


        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do something
        $response = $this->post('/api/notification/firebase',
                    [ "firebase_token" => "test" ],
                    ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(201)
            ->assertJsonStructure([
                "status",
                "message",
            ])
            ->assertExactJson([
                "status" => true,
                "message" => trans("notification.create.token.success"),
            ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        FcmNotifTokenModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Save token success exist scenario.
     *
     * @return void
     */
    public function testSaveTokenSuccessExist()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        FcmNotifTokenModel::truncate();
        Schema::enableForeignKeyConstraints();

        User::insert([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        FcmNotifTokenModel::insert([
            'user_id' => '2712abcd',
            'token'   => 'test'
        ]);


        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do something
        $response = $this->post('/api/notification/firebase',
                    [ "firebase_token" => "test" ],
                    ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(201)
            ->assertJsonStructure([
                "status",
                "message",
            ])
            ->assertExactJson([
                "status" => true,
                "message" => trans("notification.create.token.success"),
            ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        FcmNotifTokenModel::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
