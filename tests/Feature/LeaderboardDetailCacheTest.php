<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use Tests\TestCase;

class LeaderboardDetailCacheTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLeaderboardDetailOverviewCache()
    {

        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/overview');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.overview');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);

    }

    public function testLeaderboardDetailRulesCache(){
        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/rules');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.rules');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }

    public function testLeaderboardDetailParticipantCache(){
        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/participant');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.participant');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }

    public function testLeaderboardDetailPrizesCache(){
        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/prizes');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.prizes');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }

    public function testLeaderboardDetailScheduleCache(){
        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/schedule');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.schedule');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }

    public function testLeaderboardDetailLeaderboardCache(){
        $data = factory(LeaderboardModel::class)->create(['url' => 'tes-caching-leaderboard-v1']);

        //Set cache
        $response = $this->get('/api/leaderboard/tes-caching-leaderboard-v1/leaderboard');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);

        //Get cache
        $getDataCahe = RedisHelper::getData('leaderboard.'.$data['id_leaderboard'].'.leaderboard');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        LeaderboardModel::where('url',$data['url'])->delete();
        GameModel::where('id_game_list', $data['game_competition'])->delete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }
}
