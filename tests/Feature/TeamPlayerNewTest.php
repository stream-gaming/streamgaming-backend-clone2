<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamInviteModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

use function PHPUnit\Framework\assertTrue;

class TeamPlayerNewTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTriggerDeleteCachingTeamPlayerAndMyTeamWhenKickMemberTeamPlayer()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);


        $user    = User::first();
        $user_kick = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);
        $user_id = User::pluck('user_id')->toArray();

       $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $teamPlayer->team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }
        }
        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_kick->user_id
        ]);

        $response = $this->delete('api/team/kick/' . $team_id, [
            'users_id' => $user_kick->user_id
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenAcceptRequestTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TeamInviteModel::truncate();

        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(1)->pluck('user_id')->toArray();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
            
        ]);
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => implode(',', $user_id),
            'player_status' => 1
        ]);
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user->user_id,
            'player_status' => 2
        ]);
        $team = TeamModel::first();

        $user = User::where('user_id', $team->leader_id)->first();

        $invitation = factory(TeamRequestModel::class)->create([
            'team_id'       => $team->team_id,
            'users_id'      => $identity->users_id
        ]);
    
        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';
        $redis_key_team_detail = 'team:detail:team_id:' . $team->team_id;
        $redis_key_list = 'team:list:page:1:limit:15';

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_team_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_user) == 1);
        assertTrue(Redis::exists($redis_key_team_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user    ->email, 'password' => 'admin']);

        $response = $this->post('api/team/request/accepted/' . $invitation->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_user) != 1);
        assertTrue(Redis::exists($redis_key_team_detail) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
    }
}
