<?php

namespace Tests\Feature;

use App\Helpers\Firebase\FirebaseFactoryInterface;
use App\Model\PromotionModel;
use App\Model\User;
use App\Model\UserPromotionModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Mockery\MockInterface;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPromotionDeleteTest extends TestCase
{

    public function testUpdateUserPromotionDeleteStatusAsAnAuthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //Make sure action_delete of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

        //Delete one of user promotion
        $promotion = $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

    }

    public function testUpdateUserPromotionDeleteStatusAsUnauthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = 'faketoken912371237';
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

        //Delete one of user promotion
        $promotion = $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(401);
        $promotion->assertJson([
            'status' => false
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

    }

    public function testUpdateUserPromotionDeleteStatusOnReadPromotion()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //Make sure action_delete of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 1
        ]);

        //read promotion
        $promotion = $this->put(Route('promotion.read.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        //Delete one of user promotion
        $promotion = $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 1
        ]);

    }

    public function testUpdateUserPromotionDeleteStatusOnDraftPromotion()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,2)->create(['status' => 0]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //Make sure action_delete of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 1
        ]);

        //read promotion
        $promotion = $this->put(Route('promotion.read.single', ['promotionId' => $promotions[1]->id]),[],['Authorization' => 'Bearer '.$token]);
        //Delete one of user promotion
        $promotion = $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[1]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(404);
        $promotion->assertJson([
            'status' => false,
            'message' => Lang::get('exeptions.model_not_found')
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 1
        ]);
    }
}
