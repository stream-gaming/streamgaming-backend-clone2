<?php

namespace Tests\Feature;

use App\Jobs\ProcessFirebaseCounter;
use App\Model\User;
use App\Model\UserNotificationModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class OverviewUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAddCityAndProvinceOnOverviewUserResponses()
    {
        User::truncate();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $response = $this->get('/api/user/overview/' . $user->username, []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "provinsi",
                    "nama_provinsi",
                    "kota",
                    "nama_kota",
                ]
            ]);
    }
}
