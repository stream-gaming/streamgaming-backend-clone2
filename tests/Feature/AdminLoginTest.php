<?php

namespace Tests\Feature;

use App\Model\Management;
use App\Model\UsersDetailModel;
use App\Traits\UserHelperTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class AdminLoginTest extends TestCase
{
    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginManual()
    {
        $user = $this->createUser();

        $response = $this->post('api/auth/login', [
            'email' => $user->email,
            'password'  => 'admin'
        ]);
        $response->assertStatus(200);
    }

    public function testAccessProfileWithAdminID()
    {
        $admin = $this->createAdmin();
        $response = $this->post('api/admin/get-token', ['stream_id' => $admin->stream_id]);
        // $token = $getToken->getData()->token;

        // $response = $this->get('api/account/profile', ['Authorization' => 'Bearer '.$token]);
        // $response->dump();


        $response->assertStatus(200);
    }
}
