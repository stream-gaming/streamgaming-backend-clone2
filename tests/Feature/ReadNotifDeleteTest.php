<?php

namespace Tests\Feature;

use App\Jobs\ProcessFirebaseCounter;
use App\Model\User;
use App\Model\UserNotificationModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ReadNotifDeleteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testNotificationReadDeletedAt()
    {
        //mock condtition\
        UserNotificationModel::truncate();
        User::truncate();
        $notifications = factory(UserNotificationModel::class,2)->create();
        User::insert(['user_id' => 'asdf', 'fullname' => 'test','username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        $user = User::first();
        // $user->createToken();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);
        Bus::fake();

        //do something
        $response = $this->post('/api/notification/read/'.$notifications[0]->id_notification, [], ['Authorization' => 'Bearer '. $token]);

        //validate result
        $response->assertStatus(200);
        Bus::assertDispatched(function (ProcessFirebaseCounter $job) use ($user) {
            return $job->user_id === $user->user_id;
        });
    }
}
