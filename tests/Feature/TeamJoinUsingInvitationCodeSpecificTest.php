<?php

namespace Tests\Feature;

use App\Helpers\Builders\NotificationBuilderInterface;
use App\Helpers\Redis\RedisHelper;
use App\Model\AllTournamentModel;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\HistoryBannedTeamModel;
use App\Model\IdentityGameModel;
use App\Model\InvitationCode;
use App\Model\LeaderboardModel;
use App\Model\Management;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Mockery\MockInterface;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class TeamJoinUsingInvitationCodeSpecificTest extends TestCase
{

    use WithFaker;

    public function testJoinTeamWithAvailableSlotUsingValidInvitationCode()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });


        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();


        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);
        //Join using invitation code
                $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(201);
        $joinUsingInvCode->assertJson([
            'status' => true,
            'message' => Lang::get('team.join_success')
        ]);

        //Check if the user has been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id.','.$getUser->user_id
        ]);

        $this->assertTrue(config('app.team_player_new'));

        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamWithAvailableSlotUsingInvalidInvitationCode()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);


        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'invitation_code' =>  'WrongCode',
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(422);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has not been added to team_player_new table
        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamWithAvailableSlotUsingExpInvitationCode()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2',
            'expired_at' => '2021-03-09 10:09:50'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(403);
        $joinUsingInvCode->assertJson([
            'status' => false,
            'message' => Lang::get('team.invitation_code.expired')
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has not been added to team_player_new table
        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamWithOnlyAvailableSubstituteSlotUsingValidInvitationCode()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 1,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);


        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(201);
        $joinUsingInvCode->assertJson([
            'status' => true,
            'message' => Lang::get('team.join_success')
        ]);

        //Check if the user has been added to team_player table on substitute player slot
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'substitute_id' => $getUser->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has been added to team_player_new table on substitute player slot
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 3
        ]);
    }

    public function testJoinTeamUsingValidInvitationCodeWhenUserAlreadyHasTeam()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 1,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        $user2 = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test2', 'email' => 'test2@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user2->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        $teamId2 = uniqid();
        //first team
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list]);
        factory(TeamPlayerModel::class)->create(['team_id' => $teamId, 'player_id' => $user->user_id, 'substitute_id' => '']);
        //For team player new
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamId,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        //second team
        factory(TeamModel::class)->create(['team_id' => $teamId2,'leader_id' => $user2->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list]);
        factory(TeamPlayerModel::class)->create(['team_id' => $teamId2, 'player_id' => $user2->user_id, 'substitute_id' => '']);
        //For team player new
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamId2,
            'player_id' => $user2->user_id,
            'player_status' => 1
        ]);

        //Generate invitation code for first team
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        //Generate invitation code for second team
        $invCode2 = factory(InvitationCode::class)->create([
            'team_id' => $teamId2,
            'code' => 'BHGJ7AE'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Join to the first team using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(201);
        $joinUsingInvCode->assertJson([
            'status' => true,
            'message' => Lang::get('team.join_success')
        ]);

        //Check if the user has been added to team_player table on substitute player slot
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'substitute_id' => $getUser->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 3
        ]);

        $joinUsingInvCode2 = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId2,
            'invitation_code' =>  $invCode2->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode2->assertStatus(403);
        $joinUsingInvCode2->assertJson([
            'status' => false,
            'message' => Lang::get('team.has_team')
        ]);

        //Check if the user has not been added to team_player table on substitute player slot
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId2,
            'substitute_id' => ''
        ]);

        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId2,
            'player_id' => $getUser->user_id,
            'player_status' => 3
        ]);
    }

    public function testJoinTeamUsingValidInvitationCodeWhenTeamAlreadyJoinTournament()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user,$teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        //Insert team to the tournament
        $createTour = factory(LeaderboardModel::class)->create([
            'tournament_date' => $this->faker->dateTimeBetween('+1 days', '+1 month'),
            'game_competition' => $game->id_game_list,

        ]);
        AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament' => $createTour->id_leaderboard,
            'format' => 'Leaderboard',
            'create_at' => now()
        ]);
        factory(PlayerPayment::class)->create(
            [
                'id_tournament' => $createTour->id_leaderboard,
                'id_team' => $teamId,
                'player_id' => $user->user_id
            ]
        );

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);


        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(403);
        $joinUsingInvCode->assertJson([
            'status' => false,
            'message' => Lang::get('team.invitation_code.expired')
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id,
            'substitute_id' => ''
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has not been added to team_player_new table
        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $user->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamWithAvailableSlotUsingValidInvitationCodeWhenUserNotAddedGameYet()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user,$teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(403);
        $joinUsingInvCode->assertJson([
            'status' => false,
            'message' => Lang::get('team.not_have_related_game')
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has not been added to team_player_new table
        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamUsingValidInvitationCodeWhenUserNotAuthenticatedYet(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user,$teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);


        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer faketokehasjdhasgdyasgdh']);
        $joinUsingInvCode->assertStatus(401);
        $joinUsingInvCode->assertJson([
            'status' => false
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);
        $this->assertTrue(config('app.team_player_new'));
        //Check if the user has not been added to team_player_new table
        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_status' => 2
        ]);
    }

    public function testTriggerCacheWhenJoinTeamUsingInvitationCode(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });


        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();


        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Set caching
        $cachingData = [
            'tes' => 'tes team caching'
        ];
        RedisHelper::setData('team:list:page:1:limit:15', $cachingData, 1 * (24 * 3600));
        RedisHelper::setData('team:detail:team_id:'.$teamId, $cachingData, 1 * (24 * 3600));
        RedisHelper::setData('my:team:user_id:'.$user->user_id.':list', $cachingData, 1 * (24 * 3600));
        RedisHelper::setData('my:team:user_id:'.$getUser->user_id.':list', $cachingData, 1 * (24 * 3600));

        //Make sure caching already exist
        $getDataCahe = RedisHelper::getData('team:list:page:1:limit:15');
        $this->assertNotFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('team:detail:team_id:'.$teamId);
        $this->assertNotFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('my:team:user_id:'.$user->user_id.':list');
        $this->assertNotFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('my:team:user_id:'.$getUser->user_id.':list');
        $this->assertNotFalse($getDataCahe);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(201);
        $joinUsingInvCode->assertJson([
            'status' => true,
            'message' => Lang::get('team.join_success')
        ]);

        //Check if the user has been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id.','.$getUser->user_id
        ]);

        $this->assertTrue(config('app.team_player_new'));

        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);

        //Make sure caching destroyed successfully
        $getDataCahe = RedisHelper::getData('team:list:page:1:limit:15');
        $this->assertFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('team:detail:team_id:'.$teamId);
        $this->assertFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('my:team:user_id:'.$user->user_id.':list');
        $this->assertFalse($getDataCahe);
        $getDataCahe = RedisHelper::getData('my:team:user_id:'.$getUser->user_id.':list');
        $this->assertFalse($getDataCahe);
    }

    public function testJoinTeamWithAvailableSlotUsingValidInvitationCodeWhenTeamOnBannedStatus()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        HistoryBannedTeamModel::truncate();
        Management::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });


        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();


        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);
        //Set Team Ban Status
        TeamModel::find($teamId)->update([
            "is_banned"  => '1',
            "ban_until"  => 'forever',
            "reason_ban" => 'idk',
        ]);
        HistoryBannedTeamModel::create([
            "team_id"       => $teamId,
            "ban_until"     => 'forever',
            "reason_ban"    => 'idk',
            "admin_id"      => factory(Management::class)->create()->stream_id,
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(400);
        $joinUsingInvCode->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.banned')
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);

        $this->assertTrue(config('app.team_player_new'));

        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamWithAvailableSlotUsingInvalidInvitationCodeWhenTeamOnBannedStatus()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        HistoryBannedTeamModel::truncate();
        Management::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });


        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();


        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $teamId) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));

            //For team player new
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $teamId,
                'player_id' => $user->user_id,
                'player_status' => 1
            ]);
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);
        //Set Team Ban Status
        TeamModel::find($teamId)->update([
            "is_banned"  => '1',
            "ban_until"  => 'forever',
            "reason_ban" => 'idk',
        ]);
        HistoryBannedTeamModel::create([
            "team_id"       => $teamId,
            "ban_until"     => 'forever',
            "reason_ban"    => 'idk',
            "admin_id"      => factory(Management::class)->create()->stream_id,
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'invitation_code' =>  '324SDU',
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(422);
        $joinUsingInvCode->assertJsonStructure([
            'status',
            'errors',
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);

        $this->assertTrue(config('app.team_player_new'));

        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }

    public function testJoinTeamUsingValidInvitationCodeWhenTeamOnBannedStatusFromOtherTeamEntryPoint()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        HistoryBannedTeamModel::truncate();
        Management::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock notification service
        $this->mock(NotificationBuilderInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('setMethod->setMessage->sendNotif')
            ->andReturn('true');
        });


        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        $user2 = factory(User::class)->create(['fullname' => 'test2', 'status' => 'player', 'username' => 'test2', 'email' => 'test2@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        $teamId2 = uniqid();

        //first team
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list]);
        factory(TeamPlayerModel::class)->create(['team_id' => $teamId, 'player_id' => $user->user_id, 'substitute_id' => '']);
        //For team player new
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamId,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        //second team
        factory(TeamModel::class)->create(['team_id' => $teamId2,'leader_id' => $user2->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list]);
        factory(TeamPlayerModel::class)->create(['team_id' => $teamId2, 'player_id' => $user2->user_id, 'substitute_id' => '']);
        //For team player new
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamId2,
            'player_id' => $user2->user_id,
            'player_status' => 1
        ]);

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        //Generate second team invitation code
        $invCode2 = factory(InvitationCode::class)->create([
            'team_id' => $teamId2,
            'code' => 'BHGJ8Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);
        //Set Team Ban Status
        TeamModel::find($teamId)->update([
            "is_banned"  => '1',
            "ban_until"  => 'forever',
            "reason_ban" => 'idk',
        ]);
        HistoryBannedTeamModel::create([
            "team_id"       => $teamId,
            "ban_until"     => 'forever',
            "reason_ban"    => 'idk',
            "admin_id"      => factory(Management::class)->create()->stream_id,
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code.specific'), [
            'team_id' => $teamId2,
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(422);
        $joinUsingInvCode->assertJson([
            'status' => false,
            'message' => 'Data yang diberikan tidak valid.'
        ]);

        //Check if the user has not been added to team_player table
        $this->assertDatabaseHas('team_player', [
            'team_id' => $teamId,
            'player_id' => $user->user_id
        ]);

        $this->assertTrue(config('app.team_player_new'));

        $this->assertDatabaseMissing('team_player_new', [
            'team_id' => $teamId,
            'player_id' => $getUser->user_id,
            'player_status' => 2
        ]);
    }
}
