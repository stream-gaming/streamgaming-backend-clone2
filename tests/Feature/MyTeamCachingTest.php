<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\IdentityGameModel;
use App\Model\Management;
use App\Model\ProvinceModel;
use App\Model\TeamInviteModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use App\Model\UsersGameModel;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use function PHPUnit\Framework\assertTrue;

class MyTeamCachingTest extends TestCase
{

    public function testCachingListTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');



        factory(GameModel::class)->create();
        $game_id = GameModel::first()->id_game_list;
        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        factory(TeamModel::class, 5)->create();

        $user_id = User::pluck('user_id')->toArray();


        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $redis_key = 'team:list:page:1:limit:15';

        Redis::del($redis_key);

        $response = $this->get('api/team');

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) == 1);
    }

    /**
     * Test get Detail team with caching.
     *
     * @return void
     */
    public function testCachingDetailTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');



        factory(GameModel::class)->create();
        $game_id = GameModel::first()->id_game_list;
        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();


        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $redis_key = 'team:detail:team_id:' . $team_id;

        Redis::del($redis_key);

        $team_id = (new \App\Helpers\MyApps)->onlyEncrypt($team_id);

        $response = $this->get('api/team/v2/detail/' . $team_id);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) == 1);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'id',
                'fullname',
                'username',
                'is_in_team',
                'team' => [
                    'team_id',
                    'game',
                    'description',
                    'leader',
                    'is_banned',
                    'ban_until',
                    'is_leader',
                    'team_name',
                    'venue',
                    'notif_request',
                    'logo',
                    'slot' => [
                        'total',
                        'primary',
                        'substitute',
                        'member'
                    ],
                    'url',
                    'created_at',
                    'day',
                    'member' => [
                        'primary' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ],
                        'substitute' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ]
                    ],
                    'all_member' => [
                        '*' => [
                            'users_id',
                            'username',
                            'fullname',
                            'image',
                            'bio',
                            'birthday',
                            'age',
                            'username_game',
                            'is_banned',
                            'ban_until',
                            'is_leader',
                            'is_primary',
                            'socmed',
                            'role'
                        ]
                    ],
                    'request'
                ]
            ],
        ]);
    }

    /**
     * Test get Detail my team with caching.
     *
     * @return void
     */
    public function testCachingDetailMyTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create();
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();


        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $redis_key = 'team:detail:team_id:' . $team_id;

        Redis::del($redis_key);

        $team_id = (new \App\Helpers\MyApps)->onlyEncrypt($team_id);

        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $response = $this->get('api/team/v2/detail/' . $team_id, ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) == 1);

        $response->assertJsonStructure([
            'status',
            'data'
        ]);

        $response->assertJsonPath('data.is_in_team', true);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenJoinTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(2)->pluck('user_id')->toArray();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $redis_key_detail = 'team:detail:team_id:' . $team_id;

        Redis::set($redis_key_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_detail) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'stream']);

        $response = $this->post('api/team/join/' . $team_id, [], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_detail) != 1);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenCreateTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $game_id = factory(GameModel::class)->create([
            'player_on_team' => 5
        ])->id_game_list;

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        $team = factory(TeamModel::class)->make();
        $city = factory(CityModel::class)->create();

        $redis_key_detail = 'my:team:user_id:' . $user->user_id . ':list';
        $redis_key_list   = 'team:list:page:1:limit:15';

        Redis::set($redis_key_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'stream']);

        Storage::fake('avatars');

        $response = $this->post('api/team/create', [
            'team_name' => $team->team_name,
            'game_id'   => $game_id,
            'team_description' => $team->team_description,
            'city'      => $city->nama,
            'team_logo' => UploadedFile::fake()->image('avatar.png')
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_detail) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);

        Redis::set($redis_key_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        // test with fail scenario
        $response = $this->post('api/team/create', [
            'team_name' => $team->team_name,
            'game_id'   => $game_id,
            'team_description' => $team->team_description,
            'city'      => $city->nama,
            'team_logo' => UploadedFile::fake()->image('avatar.png')
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(400);

        assertTrue(Redis::exists($redis_key_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);


    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenKickMemberTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);


        $user    = User::first();
        $user_kick = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);
        $user_id = User::pluck('user_id')->toArray();


        $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $teamPlayer->team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }
        }
        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_kick->user_id
        ]);

        $response = $this->delete('api/team/kick/' . $team_id, [
            'users_id' => $user_kick->user_id
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenLeaveTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);


        $user    = User::first();
        $user_kick = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);
        $user_id = User::pluck('user_id')->toArray();

        $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user_kick->user_id,
            'player_status' => 2
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user_kick->email, 'password' => 'stream']);

        $redis_key = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_kick->user_id
        ]);

        $response = $this->delete('api/team/leave/' . $team_id, [], ['Authorization' => 'Bearer ' . $token]);
        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) != 1);

    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenLeaveTeamLeader()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;


        $user_kick = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user_kick->user_id,
            'game_id' => $game_id
        ]);

        $user    = User::first();
        $user_id = User::pluck('user_id')->toArray();


        $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);


        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user_kick->email, 'password' => 'stream']);

        $redis_key = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_kick->user_id
        ]);

        $response = $this->delete('api/team/leave/' . $team_id, [], ['Authorization' => 'Bearer ' . $token]);
        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) != 1);


    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenInviteToTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;


        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $user_invite = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $redis_key = 'my:team:user_id:' . $user_invite->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);


        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_invite->user_id
        ]);

        $response = $this->post('api/team/invite', [
            'team_id' => $team_id,
            'users_id' => $user_invite->user_id
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);

        $response->assertStatus(201);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenAcceptInviteTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TeamInviteModel::truncate();

        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(2)->pluck('user_id')->toArray();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $invitation = factory(TeamInviteModel::class)->create([
            'team_id'       => $team_id,
            'identity_id'   => $identity->id,
            'users_id'      => $identity->users_id
        ]);

        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';
        $redis_key_team_detail = 'team:detail:team_id:' . $team_id;
        $redis_key_list = 'team:list:page:1:limit:15';

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_team_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_user) == 1);
        assertTrue(Redis::exists($redis_key_team_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'stream']);

        $response = $this->post('api/team/invite/accepted/' . $invitation->invite_id, [], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_user) != 1);
        assertTrue(Redis::exists($redis_key_team_detail) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenDeclineInviteTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TeamInviteModel::truncate();

        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(2)->pluck('user_id')->toArray();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $invitation = factory(TeamInviteModel::class)->create([
            'team_id'       => $team_id,
            'identity_id'   => $identity->id,
            'users_id'      => $identity->users_id
        ]);

        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':notif';

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_user) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'stream']);

        $response = $this->post('api/team/invite/declined/' . $invitation->invite_id, [], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_user) != 1);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenAcceptRequestTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TeamInviteModel::truncate();

        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(1)->pluck('user_id')->toArray();


        $user_request = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_request->user_id
        ]);

        $teamPlayer = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''

        ]);
        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $teamPlayer->team_id,
            'player_id' => implode(',', $user_id),
            'player_status' => 1
        ]);


        $team = TeamModel::first();

        $user = User::where('user_id', $team->leader_id)->first();

        $invitation = factory(TeamRequestModel::class)->create([
            'team_id'       => $team->team_id,
            'users_id'      => $identity->users_id
        ]);

        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';
        $redis_key_team_detail = 'team:detail:team_id:' . $team->team_id;
        $redis_key_list = 'team:list:page:1:limit:15';

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_team_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_user) == 1);
        assertTrue(Redis::exists($redis_key_team_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $response = $this->post('api/team/request/accepted/' . $invitation->request_id, [], ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_user) != 1);
        assertTrue(Redis::exists($redis_key_team_detail) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenDeclineRequestTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        TeamInviteModel::truncate();

        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::limit(2)->pluck('user_id')->toArray();


        $user_request = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user_request->user_id
        ]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team = TeamModel::first();

        $user = User::where('user_id', $team->leader_id)->first();

        $invitation = factory(TeamRequestModel::class)->create([
            'team_id'       => $team->team_id,
            'users_id'      => $identity->users_id
        ]);

        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';
        $redis_key_team_detail = 'team:detail:team_id:' . $team->team_id;
        $redis_key_list = 'team:list:page:1:limit:15';

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_team_detail, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);
        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key_user) == 1);
        assertTrue(Redis::exists($redis_key_team_detail) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $response = $this->post('api/team/request/declined/' . $invitation->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key_user) != 1);
        assertTrue(Redis::exists($redis_key_team_detail) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenChangeDataTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;
        $team = factory(TeamModel::class)->make();
        $city = factory(CityModel::class)->create();

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key      = 'team:detail:team_id:' . $team_id;
        $redis_key_list = 'team:list:page:1:limit:15';
        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);
        assertTrue(Redis::exists($redis_key_user) == 1);

        $response = $this->put('api/team/setting/' . $team_id, [
            'team_name' => $team->team_name,
            'team_description' => 'Kami Programmer',
            'city' => $city->nama
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
        assertTrue(Redis::exists($redis_key_user) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenChangeLogoTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;
        Storage::fake('avatars');

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key      = 'team:detail:team_id:' . $team_id;
        $redis_key_list = 'team:list:page:1:limit:15';
        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);
        assertTrue(Redis::exists($redis_key_user) == 1);

        $response = $this->post('api/team/logo/' . $team_id, [
            'team_logo' => UploadedFile::fake()->image('avatar.png'),
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
        assertTrue(Redis::exists($redis_key_user) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenChangeLeaderTeam()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user = User::first();

        $leader = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $leader->user_id
        ]);

        $user_id = User::pluck('user_id')->toArray();


        factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key      = 'team:detail:team_id:' . $team_id;
        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);
        assertTrue(Redis::exists($redis_key_user) == 1);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team_id,
            'player_id' => $leader->user_id,
            'player_status' => 2
        ]);
        $response = $this->put('api/team/change/leader/' . $team_id, [
            'leader' => $leader->user_id,
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);
        assertTrue(Redis::exists($redis_key_user) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingTeamAndMyTeamWhenChangeRoleMember()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 3
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $player = explode(',', $team_player->player_id);

        $subtitute_player = [$player[2],$player[1]];
        $primary_player = [$player[0],$player[3],$player[4]];

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $redis_key      = 'team:detail:team_id:' . $team_id;
        $redis_key_list = 'team:list:page:1:limit:15';
        $redis_key_user = 'my:team:user_id:' . $user->user_id . ':list';

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_list, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_key_user, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);
        assertTrue(Redis::exists($redis_key_list) == 1);
        assertTrue(Redis::exists($redis_key_user) == 1);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }

        }

        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);
        $response = $this->put('api/team/change/role/' . $team_id, [
            'primary_player' => $primary_player,
            'substitute_player' => $subtitute_player,
        ], ['Authorization' => 'Bearer ' . $token]);

        assertTrue(Redis::exists($redis_key) != 1);
        assertTrue(Redis::exists($redis_key_list) != 1);
        assertTrue(Redis::exists($redis_key_user) != 1);

        $response->assertStatus(200);
    }

    public function testTriggerDeleteCachingDetailTeamWhenUsersOrTeamInBanned()
    {
        // truncate database
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        GameUsersRoleModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Management::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');



        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        $game = GameModel::first();

        $user_id = User::pluck('user_id')->toArray();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game->id_game_list]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //- Mock redis
        $redis_game = 'my:game:user_id:' . $user->user_id;
        $redis_team = 'team:detail:team_id:' . $team_player->team_id;

        Redis::set($redis_game, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_team, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_game) == 1);
        assertTrue(Redis::exists($redis_team) == 1);

        $admin = factory(Management::class)->create();

        $response = $this->post('/api/admin/ban/team/'.$team_player->team_id, [
            'banned_until' => Carbon::now()->tomorrow()->format('Y-m-d'),
            'reason' => 'Test Uyyy',
            'identity_id' => [$identity->id]
        ], ['Authorization' => 'Admin ' . $admin->stream_id]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_game) != 1);
        assertTrue(Redis::exists($redis_team) != 1);
    }


    public function testTriggerDeleteDetailTeamWhenEditGamePlayer()
    {
        // truncate database
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');


        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        $game = GameModel::first();
        $user_id = User::pluck('user_id')->toArray();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game->id_game_list]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //- Mock redis
        $redis_key = 'team:detail:team_id:' . $team_player->team_id;

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'stream']);

        $game_role = factory(GameRoleModel::class)->create(['game_id' => $game->id_game_list]);

        $response = $this->put('/api/game/edit/' . $game->id_game_list, [
            'id_ingame' => '12345679',
            'username_ingame' => 'HELLOGUYS1',
            'game_role_id' => [$game_role->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) != 1);
    }
}
