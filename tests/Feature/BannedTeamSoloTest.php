<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Jobs\ProcessFirebaseCounter;
use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\Management;
use App\Model\ProvinceModel;
use App\Model\RefferalConfig;
use App\Model\RefferalHistory;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use App\Model\UserNotificationModel;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class BannedTeamSoloTest extends TestCase
{

    public function testGetListMemberTeamSuccess()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $admin = factory(Management::class)->create();

        $game = factory(GameModel::class)->create();


        $user = factory(IdentityGameModel::class)->create([
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $user2 = factory(IdentityGameModel::class)->create([
            'game_id'  => $game->id_game_list,
            'id_ingame' => '11221',
            'username_ingame' => 'user11221'
        ]);

        factory(TeamModel::class)->create(['is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $user2) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->users_id . ',' . $user2->users_id, 'substitute_id' => '']));
        });

        $team     = TeamModel::first()->team_id;
        $response = $this->get('/api/admin/ban/team/list-member/' . $team, ['Authorization' => 'Admin ' . $admin->stream_id]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "team_id",
                    "game",
                    "description",
                    "is_banned",
                    "ban_until",
                    "team_name",
                    "created_at",
                    "member" => [
                        "*" => [
                            "identity_id",
                            "users_id",
                            "username",
                            "fullname",
                            "username_game",
                            "id_ingame",
                            "is_banned",
                            "is_leader",
                        ]
                    ]
                ]
            ]);
    }

    public function testGetListMemberTeamFailed()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $admin = factory(Management::class)->create();

        $response = $this->get('/api/admin/ban/team/list-member/' . rand(1111, 99999), ['Authorization' => 'Admin ' . $admin->stream_id]);

        //validate result
        $response->assertStatus(400)
            ->assertJsonStructure([
                "status",
                "data"
            ]);
    }

    public function testBannedTeamSoloSuccess()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $admin = factory(Management::class)->create();

        $game = factory(GameModel::class)->create();


        $user = factory(IdentityGameModel::class)->create([
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $user2 = factory(IdentityGameModel::class)->create([
            'game_id'  => $game->id_game_list,
            'id_ingame' => '11221',
            'username_ingame' => 'user11221'
        ]);

        factory(TeamModel::class)->create(['is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $user2) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->users_id . ',' . $user2->users_id, 'substitute_id' => '']));
        });

        $team     = TeamModel::first()->team_id;
        $response = $this->post('/api/admin/ban/team/' . $team, [
            'banned_until' => Carbon::now()->addMonth()->format("Y-m-d"),
            'reason' => 'Menggunakan Cheat',
            'identity_id' => [$user->id, $user2->id]
        ], ['Authorization' => 'Admin ' . $admin->stream_id]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "message"
            ]);
    }
}
