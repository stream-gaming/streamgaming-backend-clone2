<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Helpers\Validation\CheckBanned;
use App\Model\User;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class MyGameTest extends TestCase
{

    public function testAddLogo2BackgroundLogo1BackgroundLogo2OnMyGameResponses()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();


        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);


        $identityGame = factory(IdentityGameModel::class)
            ->create([
                'users_id' => 'asdf',
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)->create([
            'users_id'  => 'asdf',
            'game_id'   => $identityGame->game_id
        ]);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        //do something
        $response = $this->get('/api/account/my-game', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "game" => [
                        "*" => [
                            "game" => [
                                "logo2",
                                "background_logo1",
                                "background_logo2"
                            ],
                        ],
                    ]
                ],
            ]);
    }

    public function testAddUrlOnMyGameAvailableResponses()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => 'asdf',
                'is_banned' => 0
            ]);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        //do something
        $response = $this->get('/api/account/my-game', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "fullname",
                    "username",
                    "game" => [
                        "*" => [
                            "ban_until",
                            "game" => [
                                "background_logo1",
                                "background_logo2",
                                "id_game",
                                "logo",
                                "logo2",
                            ],
                            "id",
                            "id_ingame",
                            "is_banned",
                            "role",
                            "url",
                            "username_ingame",
                        ]
                    ],
                    "game_available" => [
                        "*" => [
                            "background_logo1",
                            "background_logo2",
                            "id_game",
                            "logo",
                            "name",
                            "url",
                        ]
                    ],
                ],
            ]);
    }
}
