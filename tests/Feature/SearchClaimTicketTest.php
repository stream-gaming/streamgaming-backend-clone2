<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Traits\UserHelperTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class SearchClaimTicketTest extends TestCase
{
    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSearchClaimTicketPersonal()
    {
        $user = $this->createUser();
        $ticket = factory(AllTicketModel::class)->create(['name' => "testing",'total_given' => 0, 'mode' => 1,'sub_type' => 2,'event_end' =>date('Y-m-d H:i:s', strtotime('+7 day'))]);

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin',]);

        $response = $this->get('api/ticket/v3/claims/user?search=testing', ['Authorization' => 'Bearer '.$token]);
        $response->assertJsonStructure([
            "active_tab" ,
            "ticket" => [
                           '*' => [
                                "type" ,
                                "is_availablse",
                                "ticket_id",
                                "name",
                                "expired",
                                "period",
                                "is_claim",
                                "claim_until",
                                "image",
                                "ticket_type",
                                "game"
                            ]
                        ]
    ]);
        $response->assertStatus(200);
    }
    public function testEmptySearchClaimTicketPersonal()
    {
        $user = $this->createUser();
        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin',]);

        $response = $this->get('api/ticket/v3/claims/user?search='.rand(), ['Authorization' => 'Bearer '.$token]);
        $response->assertJsonStructure([
            "active_tab" ,
            "ticket" => []
        ]);
        $response->assertStatus(200);
    }
    public function testSearchClaimTicketTeam()
    {
        $user = $this->createUser();
        $ticket = factory(AllTicketModel::class)->create(['name' => "testingTeam",'total_given' => 0, 'mode' => 3,'sub_type' => 2,'event_end' =>date('Y-m-d H:i:s', strtotime('+7 day'))]);

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin',]);

        $response = $this->get('api/ticket/v3/claims/team?search=testingTeam', ['Authorization' => 'Bearer '.$token]);
        $response->assertJsonStructure([
            "active_tab" ,
            "ticket" => [
                           '*' => [
                                "type" ,
                                "is_availablse",
                                "ticket_id",
                                "name",
                                "expired",
                                "period",
                                "is_claim",
                                "claim_until",
                                "image",
                                "ticket_type",
                                "game"
                            ]
                        ]
    ]);
        $response->assertStatus(200);
    }
    public function testEmptySearchClaimTicketTeam()
    {
        $user = $this->createUser();
        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin',]);

        $response = $this->get('api/ticket/v3/claims/team?search='.rand(), ['Authorization' => 'Bearer '.$token]);
        $response->assertJsonStructure([
            "active_tab" ,
            "ticket" => []
        ]);
        $response->assertStatus(200);
    }
}
