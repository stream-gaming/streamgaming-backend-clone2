<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class TeamPlayerNewTableTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTeamDataFlowToNewTableWhenChangeLeaderTeamWithCorrectPayload()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();


        factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user = User::first();

        $leader = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Qasdadfd Tes',
            'username'  => 'FFFF66677',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'email'     => 'ssss.sss@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $leader->user_id
        ]);

        $user_id = User::pluck('user_id')->toArray();


        $team = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $team->team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }

        }

        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //check current leader before change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $response = $this->put('api/team/change/leader/' . $team_id, [
            'leader' => $leader->user_id,
        ], ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(200);
        $response->assertJson([
            "status" => true,
            "message" => "Pemimpin tim berhasil diubah"
        ]);

        //check new leader after change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $leader->user_id,
            'player_status' => 1
        ]);
    }

    public function testTeamDataFlowToNewTableWhenChangeLeaderTeamWithIncorrectPayload()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();


        factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 2)->create([
            'game_id' => $game_id
        ]);

        $user = User::first();

        $leader = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Qasdadfd Tes',
            'username'  => 'FFFF66677',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'email'     => 'ssss.sss@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $leader->user_id
        ]);

        $user_id = User::pluck('user_id')->toArray();


        $team = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $team->team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }

        }

        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //check current leader before change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $response = $this->put('api/team/change/leader/' . $team_id, [
            'leader' => '3123123123',
        ], ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(400);
        $response->assertJson([
            "status" => false,
            "message" => "Pengguna bukan anggota tim ini"
        ]);

        //check new leader after change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);
    }

    public function testTeamDataFlowToNewTableWhenChangeRoleTeamWithCorrectPayload()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();


        factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 3
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $player = explode(',', $team_player->player_id);

        $subtitute_player = [$player[2],$player[1]];
        $primary_player = [$player[0],$player[3],$player[4]];

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }

        }

        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //check current player formation before change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $user_id[2],
            'player_status' => 2
        ]);

        $response = $this->put('api/team/change/role/' . $team_id, [
            'primary_player' => $primary_player,
            'substitute_player' => $subtitute_player,
        ], ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(200);
        $response->assertJson([
            "status" => true,
            "message" => "Daftar anggota tim berhasil diubah"
        ]);

        //check new player formation after change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $player[2],
            'player_status' => 3
        ]);
    }

    public function testTeamDataFlowToNewTableWhenChangeRoleTeamWithIncorrectPayload()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();


        factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 3
        ]);
        $game_id = GameModel::first()->id_game_list;

        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $user = User::first();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id'   => factory(TeamModel::class)->create(['game_id' => $game_id, 'leader_id' => $user->user_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $player = explode(',', $team_player->player_id);

        $subtitute_player = [$player[2],$player[1]];
        $primary_player = [$player[0],$player[3], uniqid()];

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //set main player to team_player_new table
        foreach($user_id as $u_id){
            if($u_id != $user->user_id){
                TeamPlayerNewModel::create([
                    'team_id' => $team_id,
                    'player_id' => $u_id,
                    'player_status' => 2
                ]);
            }

        }

        //set leader to team_player_new table
        TeamPlayerNewModel::create([
            'team_id' => $team_id,
            'player_id' => $user->user_id,
            'player_status' => 1
        ]);

        $team_id = TeamModel::first()->team_id;

        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //check current player formation before change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $user_id[2],
            'player_status' => 2
        ]);

        $response = $this->put('api/team/change/role/' . $team_id, [
            'primary_player' => $primary_player,
            'substitute_player' => $subtitute_player,
        ], ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(400);
        $response->assertJson([
            "status" => false,
            "message" => "Pengguna bukan anggota tim ini"
        ]);

        //check new player formation after change
        $this->assertDatabaseHas('team_player_new', [
            'team_id' => $team_id,
            'player_id' => $player[2],
            'player_status' => 2
        ]);
    }
}
