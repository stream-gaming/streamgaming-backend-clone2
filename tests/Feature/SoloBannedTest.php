<?php

namespace Tests\Feature;

use App\Model\User;
use Tests\TestCase;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\TeamModel;
use Illuminate\Support\Str;
use App\Model\ProvinceModel;
use App\Model\UsersGameModel;
use App\Model\TeamInviteModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Traits\UserHelperTrait;
use App\Model\IdentityGameModel;
use Illuminate\Http\UploadedFile;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Schema;

class SoloBannedTest extends TestCase
{
    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testUpdateDataGameWhenAccountGameIsBanned()
    /** Testing ketika user ubah data game pada saat akun game sedang di banned */
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        UsersGameModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user utama
        $user = $this->createUser();

        //buat id akun game untuk user utama
        $id_game = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        // mock job
        Bus::fake();
        // send data dan validasi testing
        $getGame = $this->put('/api/game/edit/' . GameModel::first()->id_game_list, [
            'id_ingame'         => '100',
            'username_ingame'   => 'New Username'
        ], ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }


    public function testJoinTeamWhenAccountGameisBanned()
    /** Testing ketika user meminta bergabung ke tim pada saat akun game sedang di banned */
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user utama
        $user = $this->createUser();

        //buat id akun game untuk user utama
        $id_game = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);

        //buat tim di user lain
        $team = factory(TeamModel::class)->create()->each(function ($team) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $team->leader_id, 'substitute_id' => null]));
        });

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        // mock job
        Bus::fake();
        // send data dan validasi testing
        $joinTeam = $this->post('/api/team/join/' . TeamModel::first()->team_id, [], ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }

    public function testCreateTeamWhenAccountGameisBanned()
    /** Testing ketika user membuat tim saat akun game sedang di banned */
    {
        //truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user utama
        $user = $this->createUser();

        //buat id akun game untuk user utama
        $id_game = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);

        $city = factory(CityModel::class)->create();

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        // mock fake upload
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.png');

        // mock job
        Bus::fake();
        //send data with validation testing
        $createTeam = $this->post('/api/team/create', [
            'team_name' => 'Testing Team',
            'game_id'   => GameModel::first()->id_game_list,
            'team_description'  => 'Gadak',
            'city'      => $city->nama,
            'team_logo' => $file
        ], ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }

    public function testInviteSomeoneWhenHisAccountGameisBanned()
    /** Testing ketika lead tim menginvite user yg akun gamenya di banned */
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user utama
        $user = $this->createUser();
        //buat user secondary | pemilik tim | invite user utama ke tim
        $user2 = $this->createAdditionalUser();

        //buat id akun game untuk user utama
        $identity = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);
        //buat id akun game untuk secondary user | leader tim yang akan ngeinvite user utama
        $identity2 = factory(IdentityGameModel::class)->create(['users_id' => $user2->user_id]);

        //buat tim dengan leader team user secondary
        $team = factory(TeamModel::class)->create(['leader_id' => $user2->user_id])->each(function ($team) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $team->leader_id, 'substitute_id' => null]));
        });

        // generate token user secondary
        $token2 = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user2->email, 'password' => 'admin']);

        // mock job
        Bus::fake();
        // invite and validate
        $invite = $this->post('/api/team/invite', [
            'team_id'   => TeamModel::first()->team_id,
            'users_id'  => $user->user_id
        ], ['Authorization' => 'Bearer ' . $token2])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }

    public function testAccRequestJoinWhenAccountGameRequesterisBanned()
    /** Testing ketika leader tim menerima permintaan join dari user yg akun gamenya dibanned */
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user #1
        $user = $this->createUser();
        //buat user #2 | leader tim | acc permintaan join dari user #1
        $user2 = $this->createAdditionalUser();

        //buat id akun game untuk user #1
        $identity = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);
        //buat id akun game untuk user #2 | leader tim yang akan menerima permintaan dari user #1
        $identity2 = factory(IdentityGameModel::class)->create(['users_id' => $user2->user_id]);

        //buat tim dengan leader team user #2
        $team = factory(TeamModel::class)->create(['leader_id' => $user2->user_id])->each(function ($team) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $team->leader_id, 'substitute_id' => null]));
        });
        $team = TeamModel::first();

        //buat permintaan join
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id'   => $team->team_id,
            'users_id'  => $user->user_id,
            'status'    => 1
        ]);

        $encr_team_id = (new \App\Helpers\MyApps)->onlyEncrypt($team->team_id);

        // generate token user #2 | leader tim
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user2->email, 'password' => 'admin']);

        // mock job
        Bus::fake();
        // acc and validate
        $get = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }

    public function testAccInvitationtoTeamWhenAccountGameisBanned()
    /** Testing ketika user menyetujui undangan tim jika akun game sedang dibanned  */
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //buat user utama | acc invitation
        $user = $this->createUser();
        //buat user secondary | pemilik tim | invite user utama ke tim
        $user2 = $this->createAdditionalUser();

        //buat id akun game untuk user utama
        $identity = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 1, 'ban_until' => now()]);
        //buat id akun game untuk secondary user | leader tim yang akan ngeinvite user utama
        $identity2 = factory(IdentityGameModel::class)->create(['users_id' => $user2->user_id]);

        //buat tim dengan leader team user secondary
        $team = factory(TeamModel::class)->create(['leader_id' => $user2->user_id])->each(function ($team) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $team->leader_id, 'substitute_id' => null]));
        });

        //buat request invitation
        $invitation = factory(TeamInviteModel::class)->create([
            'team_id'   => TeamModel::first()->team_id,
            'identity_id'    => $identity->id,
            'users_id'  => $identity->users_id
        ]);

        // generate token user secondary
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        // mock job
        Bus::fake();
        //acc and validate
        $get = $this->post('/api/team/invite/accepted/' . $invitation->invite_id, [], ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'message'
            ]);
    }
}
