<?php

namespace Tests\Feature;

use App\Model\TournamentStreamingModel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TournamentStreamingCRUDTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testInsertTournamentStreamingWithCorrectValidation()
    {

        $dataTest = [
            "id_tournament" => uniqid(),
            "title" => "youtube",
            "url" => "https://www.youtube.com/watch?v=xhaI-lLiUFA",
            "sequence" => 1
        ];

        $response = $this->post('/api/tournament-streaming/insert', $dataTest);

        $response->assertStatus(200);
        $response->assertJsonStructure(['status','message']);
    }

    public function testInsertTournamentStreamingWithErrorValidation(){
        $dataTest = [
            "id_tournament" => uniqid(),
            "title" => "youtube",
        ];

        $response = $this->post('/api/tournament-streaming/insert', $dataTest);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message','errors']);
    }

    public function testUpdateTournamentStreamingWithCorrectValidation()
    {
        $data = factory(TournamentStreamingModel::class)->create(['title' => 'youtube']);
        $dataTest = [
            "title" => "tiktok",
            "url" => "www.tiktok.com",

        ];

        $response = $this->post('/api/tournament-streaming/update/'.$data->id, $dataTest);

        $response->assertStatus(200);
        $response->assertJsonStructure(['status','message']);
        $this->assertDatabaseHas('tournament_streaming', ['title' => 'tiktok']);
    }

    public function testUpdateTournamentStreamingWithErrorValidation(){
        $data = factory(TournamentStreamingModel::class)->create(['title' => 'youtube']);


        $response = $this->post('/api/tournament-streaming/update/qhsd');

        $response->assertStatus(404);
        $response->assertJsonStructure(['status','message']);

    }

    public function testDeleteTournamentStreamingWithCorrectValidation()
    {
        $data = factory(TournamentStreamingModel::class)->create(['title' => 'youtube']);

        $response = $this->post('/api/tournament-streaming/delete/'.$data->id);

        $response->assertStatus(200);
        $response->assertJsonStructure(['status','message']);
    }

    public function testDeleteTournamentStreamingWithErrorValidation()
    {
        $data = factory(TournamentStreamingModel::class)->create(['title' => 'youtube']);

        $response = $this->post('/api/tournament-streaming/delete/fdasfds');

        $response->assertStatus(404);
        $response->assertJsonStructure(['status','message']);

    }
}
