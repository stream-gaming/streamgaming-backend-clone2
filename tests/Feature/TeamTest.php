<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\CityModel;
use App\Model\User;
use App\Model\GameModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class TeamTest extends TestCase
{

    public function testAddGameIdOnDetailTeamResponses()
    {
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'asdfasdf']);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $MyApps = new MyApps();
        $url = $MyApps->onlyEncrypt(TeamModel::first()->team_id);
        $response = $this->get('/api/team/detail/' . $url, ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "game_id",
                ]
            ]);
    }

    public function testAddGameIdOnMyDetailTeamResponses()
    {
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'asdfasdf']);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $MyApps = new MyApps();
        $url = $MyApps->onlyEncrypt(TeamModel::first()->team_id);
        $response = $this->get('/api/account/my-team/detail/' . $url, ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "team" => [
                        "game_id",
                    ]
                ]
            ]);
    }
}
