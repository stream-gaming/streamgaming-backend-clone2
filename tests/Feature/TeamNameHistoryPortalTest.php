<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamNameHistoryModel;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TeamNameHistoryPortalTest extends TestCase
{
    /**
     * Test get data team name history with scenario success
     *
     * @return void
     */
    public function testGetTeamNameHistorySuccess()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        factory(TeamNameHistoryModel::class,5)->create([
            'team_id' => $team->team_id
        ]);

        $response = $this->get('/api/admin/team/history/'.$team->team_id);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'team_id',
                'name_history' => [
                    '*' => [
                        'team_id',
                        'old_name',
                        'new_name',
                        'created_at'
                    ]
                ]
            ],
            'message'
        ]);

        $response->assertJsonPath('status',true);

        $response->assertJsonCount(5,'data.name_history');
    }

    /**
     * Test get data team name history with scenario failed
     *
     * @return void
     */
    public function testGetTeamNameHistoryFailed()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->get('/api/admin/team/history/exampleNotFoundTest');

        $response->assertStatus(404);

        $response->assertJsonStructure([
            'status',
            'data',
            'message'
        ]);

        $response->assertJsonPath('status',false);

        $response->assertJson([
            'status' => false,
            'data' => [],
            'message' => Lang::get('message.team.not')
        ]);
    }
}
