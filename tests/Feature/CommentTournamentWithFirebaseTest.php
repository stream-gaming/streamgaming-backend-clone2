<?php

namespace Tests\Feature;

use App\Model\AllTournamentModel;
use App\Model\CommentsModel;
use App\Model\LeaderboardModel;
use App\Model\Management;
use App\Model\User;
use App\Model\UserNotificationModel;
use App\Traits\UserHelperTrait;
use Kreait\Firebase\ServiceAccount;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use Kreait\Firebase\Factory;


class CommentTournamentWithFirebaseTest extends TestCase
{
    use UserHelperTrait;

    const TOURNAMENT_ID = 't0urn4m3nt';
    const EMAIL         = 'melly.tiara92@gmail.com';
    const PASSWORD      = 'stream';

    public function firebase()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path(config('firebase.service_account')));

        return (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('firebase.database_url'))
            ->create()
            ->getDatabase();
    }

    public function createAsUser()
    {
        return factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => self::EMAIL,
            'password'  => bcrypt(self::PASSWORD)
        ]);
    }

    public function createTournament()
    {
        $createTournament = factory(LeaderboardModel::class)->create(['url' => 'url-comment-tes', 'id_leaderboard' => self::TOURNAMENT_ID]);
        return AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament'     => $createTournament->id_leaderboard,
            'format'            => 'Leaderboard',
            'create_at'         => now()
        ]);
    }

    public function testAddCommentAsUser()
    {
        CommentsModel::truncate();
        User::truncate();
        UserNotificationModel::truncate();
        LeaderboardModel::truncate();
        AllTournamentModel::truncate();

        $firebase = $this->firebase();

        $firebase
            ->getReference('tournament/comment/list/' . self::TOURNAMENT_ID)
            ->remove();

        $this->createAsUser();
        $this->createTournament();


        $token = JWTAuth::attempt(['email' => self::EMAIL, 'password' => self::PASSWORD]);

        $comment = [
            'user_status'     => 'user',
            'comment'         => 'First Komen User',
        ];

        $response = $this->post(
            '/api/comments/list/' . self::TOURNAMENT_ID . '/add',
            $comment,
            ['Authorization' => 'Bearer ' . $token]
        );

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);
    }

    public function testAddReplyCommentAsUser()
    {
        $token = JWTAuth::attempt(['email' => self::EMAIL, 'password' => self::PASSWORD]);

        $firebase = $this->firebase();

        $user_profile = $firebase
            ->getReference('tournament/comment/list/' . self::TOURNAMENT_ID)
            ->getSnapshot()
            ->getValue();

        $parent_id = array_keys($user_profile)[0];

        $reply_comment = [
            'user_status'     => 'user',
            'parent_id'       => $parent_id,
            'reply_id'        => $parent_id,
            'reply_user_id'   => $user_profile[$parent_id]['user']['user_id'],
            'reply_user_status' => $user_profile[$parent_id]['role'],
            'comment'         => 'Test Reply Test User',
        ];

        $response = $this->post(
            '/api/comments/list/' . self::TOURNAMENT_ID . '/add',
            $reply_comment,
            ['Authorization' => 'Bearer ' . $token]
        );

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);
    }

    public function testAddCommentAsAdmin()
    {
        $admin = $this->createAdmin();

        $comment = [
            'user_status'     => 'admin',
            'comment'         => 'Test First Komen Admin',
            'stream_id'       => $admin->stream_id
        ];

        $response = $this->post(
            '/api/comments-portal/list/' . self::TOURNAMENT_ID . '/add',
            $comment,
            []
        );

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);
    }

    public function testAddReplyCommentAsAdmin()
    {
        $admin = Management::first();

        $firebase = $this->firebase();

        $user_profile = $firebase
            ->getReference('tournament/comment/list/' . self::TOURNAMENT_ID)
            ->getSnapshot()
            ->getValue();

        $parent_id = array_keys($user_profile)[1];

        $reply_comment = [
            'user_status'     => 'admin',
            'stream_id'       => $admin->stream_id,
            'parent_id'       => $parent_id,
            'reply_id'        => $parent_id,
            'reply_user_id'   => $user_profile[$parent_id]['user']['user_id'],
            'reply_user_status' => $user_profile[$parent_id]['role'],
            'comment'         => 'Test Reply Test Admin',
        ];

        $response = $this->post(
            '/api/comments-portal/list/' . self::TOURNAMENT_ID . '/add',
            $reply_comment,
            []
        );

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);
    }

    public function testGetListCommentTournament()
    {
        $token = JWTAuth::attempt(['email' => self::EMAIL, 'password' => self::PASSWORD]);

        $response = $this->get(
            '/api/comments/list/' . self::TOURNAMENT_ID,
            ['Authorization' => 'Bearer ' . $token]
        );

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "*" => [
                        "comments_id",
                        "role",
                        "comment",
                        "created_at",
                        "user" => [
                            "user_id",
                            "full_name",
                            "username",
                            "picture",
                        ],
                        "replies" => [
                            "*" => [
                                "comments_id",
                                "role",
                                "parent_id",
                                "comment",
                                "created_at",
                                "user" => [
                                    "user_id",
                                    "full_name",
                                    "username",
                                    "picture",
                                ],
                                "reply_from" => [
                                    "user_id",
                                    "reply_id",
                                    "full_name",
                                    "username",
                                    "user_stat",
                                ],
                                "action_menu" => [
                                    "delete",
                                ]
                            ]
                        ],
                        "more",
                    ],
                ],
                "meta" => [
                    "pagination" => [
                        "total",
                        "count",
                        "per_page",
                        "current_page",
                        "total_pages",
                        "links"
                    ],
                ]
            ]);
    }

    public function testDeleteCommentAsUser()
    {
        $token = JWTAuth::attempt(['email' => self::EMAIL, 'password' => self::PASSWORD]);

        $firebase = $this->firebase();

        $user_profile = $firebase
            ->getReference('tournament/comment/list/' . self::TOURNAMENT_ID)
            ->getSnapshot()
            ->getValue();

        $parent_id = array_keys($user_profile)[0];
        $replies_id = array_keys($user_profile[$parent_id]['replies'])[0];

        $reply_comment = [
            'user_status'     => 'user',
            'parent_id'       => $parent_id,
            'reply_id'        => $parent_id,
            'reply_user_id'   => $user_profile[$parent_id]['user']['user_id'],
            'reply_user_status' => $user_profile[$parent_id]['role'],
            'comment'         => 'Test Reply Test',
        ];

        $path = $parent_id . '/replies/' . $replies_id;

        $response = $this->delete(
            '/api/comments/list/' . self::TOURNAMENT_ID . '/delete/' . $replies_id . '?path=' . $path,
            $reply_comment,
            ['Authorization' => 'Bearer ' . $token]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure(['status', 'message']);
    }

    
}
