<?php

namespace Tests\Feature;

use App\Model\User;
use App\Model\UserNotificationModel;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class NotificationTest extends TestCase
{

    public function testNotificationTimeStampWhenMakingAsRead()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserNotificationModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Create user
        $user = factory(User::class)->create(['password' => bcrypt('Admin12345!')]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        //Create Notification
        $notification = factory(UserNotificationModel::class)->create([
            'id_user' => $user->user_id,
            'keterangan' => 'testing timestamp',
            'deleted_at' => null,
            'date' => '2021-01-26 14:46:24'
        ]);

        //mark as read
        $this->post('/api/notification/read/'.$notification->id_notification,
                                                [],
                                                ['Authorization' => 'Bearer ' . $token]);

        //Check if date still same after updating is_read
        $this->assertDatabaseHas('notification', [
            'id_user' => $user->user_id,
            'keterangan' => 'testing timestamp',
            'is_read' => 1,
            'date' => '2021-01-26 14:46:24'

        ]);
    }
}
