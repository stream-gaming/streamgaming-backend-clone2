<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class DetailTeamV2Test extends TestCase
{
    /**
     * Test get Detail team with status success.
     *
     * @return void
     */
    public function testGetDetailTeamSuccess()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        factory(IdentityGameModel::class,5)->create([
            'game_id' => $game->id_game_list
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $team = factory(TeamModel::class)->create(['game_id'=>$game->id_game_list]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => $team,
            'player_id' => implode(',',$user_id),
            'substitute_id' => ''
        ]);

        foreach($user_id as $v){
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $team->team_id,
                'player_id' => $v
            ]);
        }


        $team_id = TeamModel::first()->team_id;

        $team_id = (new \App\Helpers\MyApps)->onlyEncrypt($team_id);

        $response = $this->get('api/team/v2/detail/'.$team_id);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'id',
                'fullname',
                'username',
                'is_in_team',
                'is_waiting',
                'team' => [
                    'team_id',
                    'game',
                    'game_logo',
                    'game_url',
                    'description',
                    'leader',
                    'is_banned',
                    'ban_until',
                    'is_leader',
                    'team_name',
                    'venue',
                    'notif_request',
                    'logo',
                    'slot' => [
                        'total',
                        'primary',
                        'substitute',
                        'member'
                    ],
                    'url',
                    'created_at',
                    'day',
                    'member' => [
                        'primary' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ],
                        'substitute' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ]
                    ],
                    'all_member' => [
                        '*' => [
                            'users_id',
                            'username',
                            'fullname',
                            'image',
                            'bio',
                            'birthday',
                            'age',
                            'username_game',
                            'is_banned',
                            'ban_until',
                            'is_leader',
                            'is_primary',
                            'socmed',
                            'role'
                        ]
                    ],
                    'request'
                ]
            ],
        ]);

        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test get Detail my team with status success.
     *
     * @return void
     */
    public function testGetDetailMyTeamSuccess()
    {
        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        factory(IdentityGameModel::class,5)->create([
            'game_id' => $game->id_game_list
        ]);

        $user_id = User::pluck('user_id')->toArray();

        $team = factory(TeamModel::class)->create(['game_id'=>$game->id_game_list]);

        factory(TeamPlayerModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => implode(',',$user_id),
            'substitute_id' => ''
        ]);

        foreach($user_id as $v){
            factory(TeamPlayerNewModel::class)->create([
                'team_id' => $team->team_id,
                'player_id' => $v
            ]);
        }

        $team_id = TeamModel::first()->team_id;

        $team_id = (new \App\Helpers\MyApps)->onlyEncrypt($team_id);

        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $response = $this->get('api/team/v2/detail/'.$team_id, ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'id',
                'fullname',
                'username',
                'is_in_team',
                'is_waiting',
                'team' => [
                    'team_id',
                    'game',
                    'game_logo',
                    'game_url',
                    'description',
                    'leader',
                    'is_banned',
                    'ban_until',
                    'is_leader',
                    'team_name',
                    'venue',
                    'notif_request',
                    'logo',
                    'slot' => [
                        'total',
                        'primary',
                        'substitute',
                        'member'
                    ],
                    'url',
                    'created_at',
                    'day',
                    'member' => [
                        'primary' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ],
                        'substitute' => [
                            '*' => [
                                'users_id',
                                'username',
                                'fullname',
                                'image',
                                'bio',
                                'birthday',
                                'age',
                                'username_game',
                                'is_banned',
                                'ban_until',
                                'is_leader',
                                'is_primary',
                                'socmed',
                                'role'
                            ]
                        ]
                    ],
                    'all_member' => [
                        '*' => [
                            'users_id',
                            'username',
                            'fullname',
                            'image',
                            'bio',
                            'birthday',
                            'age',
                            'username_game',
                            'is_banned',
                            'ban_until',
                            'is_leader',
                            'is_primary',
                            'socmed',
                            'role'
                        ]
                    ],
                    'request'
                ]
            ],
        ]);

        $response->assertJsonPath('data.is_in_team',true);

        Schema::disableForeignKeyConstraints();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamPlayerNewModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test get Detail team with status failed.
     *
     * @return void
     */
    public function testGetDetailTeamFailed()
    {

        $response = $this->get('api/team/v2/detail/test');

        $response->assertStatus(404);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.not')
        ]);
    }
}
