<?php

namespace Tests\Feature;

use App\Model\User;
use Tests\TestCase;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\TeamModel;
use Illuminate\Support\Arr;
use App\Model\ProvinceModel;
use App\Model\UsersGameModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Model\IdentityGameModel;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Bus;
use App\Jobs\ProcessFirebaseCounter;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MovePositionPlayerTeamTest extends TestCase
{
    // use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLeaveSomeonePrimaryPlayerFromTeamWhenHaveMuchSubstitutePlayer()
    {
        /**
         * Salah satu pemain inti keluar dari tim dan setelah itu sistem otomatis memindahkan salah
         * satu pemain cadangan menjadi pemain inti (apabila ada pemain cadangan)
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 2 (penuh)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 7)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_leave = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 2) $user_leave = $user_->users_id;
        }
        //mock a job
        Bus::fake();

        $team = factory(TeamModel::class)->create(['leader_id' => $id_game[0]->users_id, 'game_id' => $game->id_game_list])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($user_leave);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        $request = $this->delete('/api/team/leave/' . $team->team_id, [], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $fdata_primary_player->forget(2);
        $fdata_primary_player->push($sb_player[0]);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->shift();
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => $fdata_substitute_player,
        ]);
    }

    public function testLeaveSomeonePrimaryPlayerFromTeamWhenHaveOneSubstitutePlayer()
    {
        /**
         * Salah satu pemain inti keluar dari tim dan setelah itu sistem otomatis memindahkan salah
         * satu pemain cadangan menjadi pemain inti (apabila ada pemain cadangan)
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 1 (tidak penuh)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 6)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_leave = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 2) $user_leave = $user_->users_id;
        }

        //mock a job
        Bus::fake();
        $team = factory(TeamModel::class)->create(['leader_id' => $id_game[0]->users_id, 'game_id' => $game->id_game_list])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($user_leave);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        $request = $this->delete('/api/team/leave/' . $team->team_id, [], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $fdata_primary_player->forget(2);
        $fdata_primary_player->push($sb_player[0]);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->shift();
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => $fdata_substitute_player,
        ]);
    }

    public function testLeaveSomeonePrimaryPlayerFromTeamWhenDontHaveSubstitutePlayer()
    {
        /**
         * Salah satu pemain inti keluar dari tim dan jika ada pemain cadangan
         * maka pemain cadangan akan menjadi pemain inti (apabila ada pemain cadangan)
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 0 (kosong)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 7)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_leave = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 2) $user_leave = $user_->users_id;
        }

        $team = factory(TeamModel::class)->create(['leader_id' => $id_game[0]->users_id])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => null]));
        });

        //get token
        $user = User::find($user_leave);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        $request = $this->delete('/api/team/leave/' . $team->team_id, [], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $fdata_primary_player->forget(2);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => null,
        ]);
    }

    public function testLeaveSomeonePrimaryPlayerAsLeaderFromTeamWhenHaveMuchSubstitutePlayer()
    {
        /**
         * Leader tim keluar dari tim dan setelah itu sistem otomatis memindahkan salah
         * satu pemain cadangan menjadi pemain inti (apabila ada pemain cadangan)
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 2 (penuh)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 7)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_leave = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 0) $user_leave = $user_->users_id;
        }
        //mock a job
        Bus::fake();

        $team = factory(TeamModel::class)->create(['leader_id' => $id_game[0]->users_id, 'game_id' => $game->id_game_list])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($user_leave);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        $request = $this->delete('/api/team/leave/' . $team->team_id, [], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $fdata_primary_player->forget(0);
        $fdata_primary_player->push($sb_player[0]);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->shift();
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => Arr::first($team->primaryPlayer()),
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => $fdata_substitute_player,
        ]);
    }

    public function testLeaveSomeoneSubstitutePlayerFromTeam()
    {
        /**
         * Salah satu pemain cadangan keluar dari tim
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 2 (penuh)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 7)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_leave = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 6) $user_leave = $user_->users_id;
        }

        $team = factory(TeamModel::class)->create(['leader_id' => $id_game[0]->users_id])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($user_leave);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        $request = $this->delete('/api/team/leave/' . $team->team_id, [], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        // $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->forget(1);
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => rtrim($primary_player, ','),
            'substitute_id' => $fdata_substitute_player,
        ]);
    }

    public function testKickSomeonePrimaryPlayerFromTeamWhenHaveMuchSubstitutePlayer()
    {
        /**
         * Salah satu pemain inti dikick dari tim dan jika ada pemain cadangan
         * maka pemain cadangan akan menjadi pemain inti (apabila ada pemain cadangan)
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 2 (kosong)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 7)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_target = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 2) $user_target = $user_->users_id;
        }
        $leader = $id_game[0]->users_id;

        $team = factory(TeamModel::class)->create(['leader_id' => $leader])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($leader);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        //mock a job
        Bus::fake();

        $request = $this->delete('/api/team/kick/' . $team->team_id, [
            'users_id'  => $user_target
        ], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $key = array_search($user_target, $pr_player);
        $fdata_primary_player->forget($key);
        $fdata_primary_player->push($sb_player[0]);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->shift();
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => $fdata_substitute_player,
        ]);
    }

    public function testKickSomeoneSubstitutePlayerFromTeamWhenHaveOneSubstitutePlayer()
    {
        /**
         * Salah satu cadangan dikick dari tim
         * Pemain inti : 5 (penuh)
         * Pemain cadangan : 2 (kosong)
         */

        //mock
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        //mock identity user
        $id_game = factory(IdentityGameModel::class, 6)->create(['game_id' => $game->id_game_list]);

        //mock data team
        $primary_player = '';
        $substitute_player = '';
        $user_target = '';
        foreach ($id_game as $row => $user_) {

            if ($row <= 4) {
                $primary_player .= $user_->users_id . ',';
            } else {
                $substitute_player .= $user_->users_id . ',';
            }

            if ($row == 2) $user_target = $user_->users_id;
        }
        $leader = $id_game[0]->users_id;

        $team = factory(TeamModel::class)->create(['leader_id' => $leader])->each(function ($team) use ($primary_player, $substitute_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => trim($primary_player, ','), 'substitute_id' => trim($substitute_player, ',')]));
        });

        //get token
        $user = User::find($leader);
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $team = TeamModel::first();

        //mock a job
        Bus::fake();

        $request = $this->delete('/api/team/kick/' . $team->team_id, [
            'users_id'  => $user_target
        ], ['Authorization' => 'Bearer' . $token])
            ->assertOk();

        /** Cek data di database */
        $this->assertDatabaseHas('team', [
            'team_id'   => $team->team_id,
            'game_id'   => $team->game_id,
            'leader_id' => $team->leader_id,
            'team_name' => $team->team_name,
            'team_description'  => $team->team_description,
            'team_logo' => $team->team_logo,
            'venue'     => $team->venue
        ]);

        /** Olah data list pemain inti dan cadangan untuk dicek
         * Cek kesesuaian data yg diolah = data apabila pemain inti keluar maka pemain cadangan yg mengisinya
         */
        $pr_player = explode(',', trim($primary_player, ','));
        $sb_player = explode(',', trim($substitute_player, ','));

        $fdata_primary_player = collect($pr_player);
        $key = array_search($user_target, $pr_player);
        $fdata_primary_player->forget($key);
        $fdata_primary_player->push($sb_player[0]);
        $fdata_primary_player = implode(',', $fdata_primary_player->all());

        $fdata_substitute_player = collect($sb_player);
        $fdata_substitute_player->shift();
        $fdata_substitute_player = implode(',', $fdata_substitute_player->all());

        $this->assertDatabaseHas('team_player', [
            'team_id'   => $team->team_id,
            'player_id' => $fdata_primary_player,
            'substitute_id' => $fdata_substitute_player,
        ]);
    }
}
