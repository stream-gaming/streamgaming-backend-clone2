<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class TicketPaymentStreamCashTest extends TestCase
{
    /*  !!!!READ ME!!!
        Before running this test, kindly add env variable like bellow:

        STREAMCASH_PAYMENT_API=https://streamgamingticketpayment.free.beeceptor.com/partner/api/payment

        That endpoint acts as the mock api for streamcash which is the payment gateway on this feature
    */
    public function testBuyTicketWithValidRequestForSoloMode()
    {
        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        $createGame = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'name' => 'game test']);
        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $createGame->id_game_list
        ]);
        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketSolo->id,
                            'payment_method' => 'cash',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "message"
        ]);
        $response->assertJson([
            "status" => true,
            "message" => "Pembelian tiket berhasil!"
        ]);


    }

    public function testBuyTicketWithValidRequestForTeamMode()
    {
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);
        $createGame = factory(GameModel::class)->create(['id_game_list' => $gameId, 'name' => 'game test']);
        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);



        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $gameId
        ]);

        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketTeam->id,
                            'payment_method' => 'cash',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "message"
        ]);
        $response->assertJson([
            "status" => true,
            "message" => "Pembelian tiket berhasil!"
        ]);


    }

    public function testBuyTicketWithInvalidRequestForSoloMode()
    {
        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        $createGame = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'name' => 'game test']);
        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $createGame->id_game_list
        ]);
        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketSolo->id,
                            'payment_method' => 'point',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(422);
        $response->assertJsonStructure([
            "status",
            "message"
        ]);
        $response->assertJson([
            "status" => false,
            "message" => "Saat ini pembelian menggunakan metode point belum tersedia!"
        ]);


    }

    public function testBuyTicketWithInvalidRequestForTeamMode()
    {
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);
        $createGame = factory(GameModel::class)->create(['id_game_list' => $gameId, 'name' => 'game test']);
        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);



        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $gameId
        ]);

        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketTeam->id,
                            'payment_method' => 'point',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(422);
        $response->assertJsonStructure([
            "status",
            "message"
        ]);
        $response->assertJson([
            "status" => false,
            "message" => "Saat ini pembelian menggunakan metode point belum tersedia!"
        ]);

    }

    public function testBuyTicketWithExceedQuantityRequestForSoloMode()
    {
        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);
        $createGame = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'name' => 'game test']);

        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 10,
            'total_given' => 9,
            'market_value' => 11000,
            'competition' => $createGame->id_game_list
        ]);

        $sisaTiket = $createTicketSolo->max_given - $createTicketSolo->total_given;

        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketSolo->id,
                            'payment_method' => 'cash',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(500);
        $response->assertJson([
            "status" => false,
            "message" => "Hanya tersisa ".$sisaTiket." unit tiket yang dapat dibeli"
          ]);
    }

    public function testBuyTicketWithExceedQuantityRequestForTeamMode()
    {
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);
        $createGame = factory(GameModel::class)->create(['id_game_list' => $gameId, 'name' => 'game test']);
        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);



        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 10,
            'total_given' => 9,
            'market_value' => 11000,
            'competition' => $gameId
        ]);
        $sisaTiket = $createTicketTeam->max_given - $createTicketTeam->total_given;

         $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $createTicketTeam->id,
                            'payment_method' => 'cash',
                            'quantity' => 3,
                            'pin' => '123456',
                            'game_id' => $createGame->id_game_list
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(500);
        $response->assertJson([
            "status" => false,
            "message" => "Hanya tersisa ".$sisaTiket." unit tiket yang dapat dibeli"
          ]);
    }


    public function testBuyTicketWithNoTokenAuthorization()
    {

        $response = $this->post('/api/ticket/buy-ticket'
            ,[
                'ticket_id' => '1',
                'payment_method' => 'cash',
                'quantity' => 3,
                'pin' => '123456',
                'game_id' => '12355634324'
            ],
            []
        );


        $response->assertStatus(401);
        $response->assertJsonStructure([
            "status",
            "message"
        ]);
    }
}
