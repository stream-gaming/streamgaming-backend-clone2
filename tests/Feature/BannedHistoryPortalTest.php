<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\HistoryBannedModel;
use App\Model\HistoryBannedTeamModel;
use App\Model\IdentityGameModel;
use App\Model\Management;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamNameHistoryModel;
use App\Model\User;
use App\Model\UsersGameModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class BannedHistoryPortalTest extends TestCase
{
    /**
     * Test get data banned history user with scenario success
     *
     * @return void
     */
    public function testGetUserBannedHistorySuccess()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        User::truncate();
        UsersGameModel::truncate();
        HistoryBannedModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identitiy_ingame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id
        ]);

        factory(HistoryBannedModel::class, 5)->create([
            'identity_id' => $identitiy_ingame->id
        ]);

        $response = $this->get('/api/user/banned-history/' . $user->user_id, []);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'id',
                    'identity_id',
                    'game',
                    'username_ingame',
                    'id_ingame',
                    'ban_until',
                    'reason_ban',
                    'admin_id',
                    'created_at',
                    'admin' => [],
                ]
            ],
        ]);

        $response->assertJsonPath('status', true);
    }

    /**
     * Test get data banned history user with scenario failed
     *
     * @return void
     */
    public function testGetUserBannedHistoryFailed()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        User::truncate();
        UsersGameModel::truncate();
        HistoryBannedModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->get('/api/user/banned-history/' . uniqid(), []);

        $response->assertStatus(404);

        $response->assertJsonStructure([
            'status',
            'data' => [],
            'error' => [],
        ]);

        $response->assertJsonPath('status', false);
    }

    /**
     * Test get data banned history team with scenario success
     *
     * @return void
     */
    public function testGetTeamBannedHistorySuccess()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        User::truncate();
        UsersGameModel::truncate();
        HistoryBannedModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        factory(HistoryBannedTeamModel::class, 5)->create([
            'team_id' => $team->team_id
        ]);

        $management = Management::first();

        $response = $this->get('/api/team/banned-history/' . $team->team_id, ['Authorization' => 'Admin ' . $management->stream_id]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'id',
                    'team_id',
                    'game',
                    'team_name',
                    'ban_until',
                    'reason_ban',
                    'admin_id',
                    'created_at',
                    'admin' => [],
                ]
            ],
        ]);

        $response->assertJsonPath('status', true);
    }

    /**
     * Test get data banned history team with scenario failed
     *
     * @return void
     */
    public function testGetTeamBannedHistoryFailed()
    {
        Schema::disableForeignKeyConstraints();
        ProvinceModel::truncate();
        CityModel::truncate();
        User::truncate();
        UsersGameModel::truncate();
        HistoryBannedModel::truncate();
        Schema::enableForeignKeyConstraints();

        $management = Management::first();

        $response = $this->get('/api/team/banned-history/' . uniqid(), ['Authorization' => 'Admin ' . $management->stream_id]);

        $response->assertStatus(404);

        $response->assertJsonStructure([
            'status',
            'data' => [],
            'error' => [],
        ]);

        $response->assertJsonPath('status', false);
    }
}
