<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Jobs\CacheTriggerPortalJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class TriggerCachePortalTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTriggerRemoveSingleCacheFromPortal()
    {
        Queue::fake();
        //mock cache
        $mockCache = (new RedisHelper)->setData('leaderboard.test.cache', ['data'=> 'testttt'], 200);
        $dataTest = [
            'key' => 'leaderboard.test.cache',
            'source' => 'p0Rt4L-F4Hm1',
            'type' => 'single'
        ];
        $response = $this->post('/api/cache/destroy',  $dataTest);
        $response->assertStatus(200)
                 ->assertJson([
                    "status" => true,
                    "message" => "cache destroyed successfully"
                ]);

       Queue::assertPushed(CacheTriggerPortalJob::class, function ($job) {
            return $job->cacheKey == 'leaderboard.test.cache' ? true : false;
        });


    }

    public function testTriggerRemoveCacheWithPatternFromPortal()
    {
        Queue::fake();
        //mock cache
        (new RedisHelper)->setData('leaderboard.test.cache1', ['data'=> 'testttt11'], 200);
        (new RedisHelper)->setData('leaderboard.test.cache2', ['data'=> 'testttt22'], 200);

        $dataTest = [
            'key' => 'leaderboard.test',
            'source' => 'p0Rt4L-F4Hm1',
            'type' => 'pattern'
        ];
        $response = $this->post('/api/cache/destroy',  $dataTest);
        $response->assertStatus(200)
                 ->assertJson([
                    "status" => true,
                    "message" => "cache destroyed successfully"
                ]);

        Queue::assertPushed(CacheTriggerPortalJob::class, function ($job) {
            return $job->cacheKey == 'leaderboard.test' ? true : false;
        });

    }

    public function testTriggerRemoveCacheMultiDataFromPortal()
    {
        Queue::fake();
        //mock cache
        (new RedisHelper)->setData('leaderboard.test.cache1', ['data'=> 'testttt11'], 200);
        (new RedisHelper)->setData('leaderboard.test.cache2', ['data'=> 'testttt22'], 200);

        $dataTest = [
            'key' => json_encode(['leaderboard.test.cache1', 'leaderboard.test.cache2']),
            'source' => 'p0Rt4L-F4Hm1',
            'type' => 'multi'
        ];
        $response = $this->post('/api/cache/destroy-multi',  $dataTest);
        $response->assertStatus(200)
                 ->assertJson([
                    "status" => true,
                    "message" => "cache destroyed successfully"
                ]);

        Queue::assertPushed(CacheTriggerPortalJob::class, function ($job) {
            return $job->cacheKey == ['leaderboard.test.cache1', 'leaderboard.test.cache2'] ? true : false;
        });
    }
}
