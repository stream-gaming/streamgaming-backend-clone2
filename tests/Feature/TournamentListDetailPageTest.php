<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class TournamentListDetailPageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetData()
    {
        RedisHelper::destroyDataWithPattern('tournament:list:*');

        $response = $this->get('/api/tournament/listv3');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' =>
            [
              '*' =>
              [
                'title',
                'card_banner',
                'is_free',
                'match_start',
                'total_prize' => 0,
                'hosted_by',
                'match_info' =>
                [
                  'game',
                  'game_on',
                  'mode',
                ],
                'status' =>
                [
                  'text',
                  'number',
                ],
                'slots' =>
                [
                  'filled',
                  'available',
                  'slotstatus',
                ],
                'url',
                'full_url',
                'game_url',
                'create_at',
                'time_value',
                'time',
              ],
            ],
            'meta' =>
            [
              'pagination' =>
              [
                'total',
                'count',
                'per_page',
                'current_page',
                'total_pages',
                'links'
              ],
            ],
            'header' =>
            [
              'title',
              'desc',
            ],
            'tab' =>
            [
              '*' =>
              [
                'tabname',
                'url',
              ]
            ],
            'filter' =>
            [
              'game' =>
              [
                '*' =>
                [
                  'text',
                  'value',
                ]
              ],
              'register' =>
              [
                '*' =>
                [
                  'text',
                  'value',
                ]
              ],
              'status' =>
              [
                '*' =>
                [
                  'text',
                  'value',
                ],
              ],
              'mode' =>
              [
                '*' =>
                [
                  'text',
                  'value',
                ],
              ],
            ],
        ]);

        $redis_cache = RedisHelper::getData('tournament:list:detail:page:1:limit:15');

        $this->assertNotFalse($redis_cache);

        RedisHelper::destroyDataWithPattern('tournament:list:*');


    }

}
