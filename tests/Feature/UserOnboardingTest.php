<?php

namespace Tests\Feature;

use App\Model\OAuthModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserOnboardingTest extends TestCase
{
    use WithFaker;

    public function testSetOnboardingOnManualRegistrationWhenSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'message',
            'email'
        ]);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        //check on database if this user onboarding has been set
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => 0,
            'status' => 0
        ]);
    }

    public function testSetOnboardingOnManualRegistrationWhenFailed()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors'
        ]);

        //Make sure there is no any user onboarding inserted on this table
        $this->assertDatabaseCount('user_onboarding', 0);
    }

    public function testSetOnboardingOnThirdPartyRegistrationWhenSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06',
           'link_image' => $this->faker->url,
           'oauth_id'      => $this->faker->uuid(),
           'provider_id'   => $this->faker->uuid(),
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'message',
            'email'
        ]);

        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        //check on database if this user onboarding has been set
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => 0,
            'status' => 0
        ]);
    }

    public function testSetOnboardingOnThirdPartyRegistrationWhenFailed()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-',
           'link_image' => $this->faker->url,
           'oauth_id'      => $this->faker->uuid(),
           'provider_id'   => '',
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors'
        ]);

        //Make sure there is no any user onboarding inserted on this table
        $this->assertDatabaseCount('user_onboarding', 0);
    }

    public function testGetUserOnboardingInfoWhenAuthenticated(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'message',
            'email'
        ]);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        //check on database if this user onboarding has been set
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => 0,
            'status' => 0
        ]);
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);
        //Tes get user onboarding info

        $userOnboardingInfo = $this->get(Route('user.onboarding.status.get'), ['Authorization' => 'Bearer ' . $token]);
        $userOnboardingInfo->assertStatus(200);
    }

    public function testGetUserOnboardingInfoWhenUnauthenticated(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'message',
            'email'
        ]);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        //check on database if this user onboarding has been set
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => 0,
            'status' => 0
        ]);

        //Tes get user onboarding info
        $userOnboardingInfo = $this->get(Route('user.onboarding.status.get'), []);
        $userOnboardingInfo->assertStatus(401);
    }
}
