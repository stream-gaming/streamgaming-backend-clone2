<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\CityModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TicketModel;
use App\Model\TicketsModel;
use App\Model\TicketTeamModel;
use App\Model\TicketUserModel;
use App\Model\User;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TicketPortalTest extends TestCase
{
    public function testAddMaxGivenTotalGivenGameNameOnDetailTicketResponses()
    {
        //mock condtition\
        Schema::disableForeignKeyConstraints();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        User::truncate();
        TeamModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        $ticket = factory(AllTicketModel::class)->create(['competition' => 'all']);

        //do something
        $response = $this->get('/api/ticket/' . $ticket->id,  []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "max_given",
                "total_given",
                "game_name",
            ]);
    }

    public function testDetailTicketNewListUserWhoHave()
    {
        //mock condtition\
        Schema::disableForeignKeyConstraints();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        User::truncate();
        TeamModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        $ticket = factory(AllTicketModel::class)->create(['competition' => 'all']);
        factory(TicketsModel::class)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => ''
        ]);

        //do something
        $response = $this->get('/api/ticket/list-new/user/' . $ticket->id,  []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure(
                [
                    "data" => [
                        "current_page",
                        "data" => [
                            "*" => [
                                "total",
                                "available",
                                "used",
                                "user_id",
                                "username",
                                "fullname"
                            ],
                        ],
                        "first_page_url",
                        "from",
                        "last_page",
                        "last_page_url",
                        "next_page_url",
                        "path",
                        "per_page",
                        "prev_page_url",
                        "to",
                        "total",
                    ],
                    "info" => [
                        "name",
                        "competition",
                        "max_given",
                        "total_given"
                    ],
                    'type'
                ]
            );
    }

    public function testDetailTicketNewListTeamWhoHave()
    {
        //mock condtition\
        Schema::disableForeignKeyConstraints();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        User::truncate();
        TeamModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        $ticket = factory(AllTicketModel::class)->create(['competition' => 'all']);
        factory(TicketsModel::class)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'user_id'   => ''
        ]);

        //do something
        $response = $this->get('/api/ticket/list-new/team/' . $ticket->id,  []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure(
                [
                    "data" => [
                        "current_page",
                        "data" => [
                            "*" => [
                                "total",
                                "available",
                                "used",
                                "team_id",
                                "team"
                            ],
                        ],
                        "first_page_url",
                        "from",
                        "last_page",
                        "last_page_url",
                        "next_page_url",
                        "path",
                        "per_page",
                        "prev_page_url",
                        "to",
                        "total",
                    ],
                    "info" => [
                        "name",
                        "competition",
                        "max_given",
                        "total_given"
                    ],
                    'type'
                ]
            );
    }

    public function testDetailTicketOldListUserWhoHave()
    {
        //mock condtition\
        Schema::disableForeignKeyConstraints();
        TicketModel::truncate();
        TicketsModel::truncate();
        TicketUserModel::truncate();
        User::truncate();
        TeamModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketUserModel::class)->create([
            'id_ticket' => $id_ticket,
        ]);

        //do something
        $response = $this->get('/api/ticket/list-old/user/' . $id_ticket,  []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure(
                [
                    "data" => [
                        "current_page",
                        "data" => [
                            "*" => [
                                "total",
                                "user_id",
                                "username",
                                "fullname"
                            ],
                        ],
                        "first_page_url",
                        "from",
                        "last_page",
                        "last_page_url",
                        "next_page_url",
                        "path",
                        "per_page",
                        "prev_page_url",
                        "to",
                        "total",
                    ],
                    "info" => [
                        "name",
                        "competition",
                        "max_given",
                        "total_given"
                    ],
                    'type'
                ]
            );
    }

    public function testDetailTicketOldListTeamWhoHave()
    {
        //mock condtition\
        Schema::disableForeignKeyConstraints();
        TicketModel::truncate();
        TicketsModel::truncate();
        TicketUserModel::truncate();
        TicketTeamModel::truncate();
        User::truncate();
        TeamModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketTeamModel::class)->create([
            'id_ticket'   => 0,
        ]);

        //do something
        $response = $this->get('/api/ticket/list-old/team/' . $id_ticket,  []);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure(
                [
                    "data" => [
                        "current_page",
                        "data" => [
                            "*" => [
                                "total",
                                "team_id",
                                "team"
                            ],
                        ],
                        "first_page_url",
                        "from",
                        "last_page",
                        "last_page_url",
                        "next_page_url",
                        "path",
                        "per_page",
                        "prev_page_url",
                        "to",
                        "total",
                    ],
                    "info" => [
                        "name",
                        "competition",
                        "max_given",
                        "total_given"
                    ],
                    'type'
                ]
            );
    }
}
