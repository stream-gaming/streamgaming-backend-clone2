<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use Xendit\Xendit;

class TicketPaymentCreateInvoiceTest extends TestCase
{
    /*  !!!!READ ME!!!
        Before running this test, kindly add the token key on your env like bellow:

        XENDIT_AUTH_TOKEN=xnd_development_BI4Ia4n7kVSHX7kMXAhwrfS7dnFsByAxkIpv4r1V2sIpjptOn63VXkb2BCrPx
        XENDIT_CALLBACK_TOKEN=GehJyVOqTw80rGgcuY6A6oqVfGHTf1gKmNPeZbfh3z0R0pXs
    */

    public function testCreateInvoiceWithValidRequestForSoloMode()
    {

        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('game_list')->where('name', 'game test')->delete();
        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        $createGame = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'name' => 'game test']);

        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $createGame->id_game_list
        ]);



        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_id' => $createTicketSolo->id,
                                        'quantity' => 3,
                                        'game_id' => $createGame->id_game_list
                                    ],
                                    ['Authorization' => 'Bearer ' . $token]);


        $getMyInvoice->assertStatus(201);
        $getMyInvoice->assertJsonStructure([
            "status",
            "invoice_url"
        ]);
    }

    public function testCreateInvoiceWithValidRequestForSquadMode()
    {
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);
        $createGame = factory(GameModel::class)->create(['id_game_list' => $gameId, 'name' => 'game test']);
        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);



        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $gameId
        ]);

        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_id' => $createTicketTeam->id,
                                        'quantity' => 3,
                                        'game_id' => $createGame->id_game_list
                                    ],
                                    ['Authorization' => 'Bearer ' . $token]);


        $getMyInvoice->assertStatus(201);
        $getMyInvoice->assertJsonStructure([
            "status",
            "invoice_url"
        ]);
    }

    public function testCreateInvoiceWithExceedQuantityRequestForSoloMode()
    {

        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('game_list')->where('name', 'game test')->delete();
        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        $createGame = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'name' => 'game test']);

        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 10,
            'total_given' => 9,
            'market_value' => 11000,
            'competition' => $createGame->id_game_list
        ]);

        $sisaTiket = $createTicketSolo->max_given - $createTicketSolo->total_given;

        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_id' => $createTicketSolo->id,
                                        'quantity' => 2,
                                        'game_id' => $createGame->id_game_list
                                    ],
                                    ['Authorization' => 'Bearer ' . $token]);

        $getMyInvoice->assertStatus(500);
        $getMyInvoice->assertJson([
            "status" => false,
            "message" => "Hanya tersisa ".$sisaTiket." unit tiket yang dapat dibeli"
          ]);
    }

    public function testCreateInvoiceWithExceedQuantityRequestForSquadMode()
    {
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();
        DB::table('game_list')->where('name', 'game test')->delete();

        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);
        $createGame = factory(GameModel::class)->create(['id_game_list' => $gameId, 'name' => 'game test']);
        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);



        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 10,
            'total_given' => 9,
            'market_value' => 11000,
            'competition' => $gameId
        ]);
        $sisaTiket = $createTicketTeam->max_given - $createTicketTeam->total_given;
        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_id' => $createTicketTeam->id,
                                        'quantity' => 3,
                                        'game_id' => $createGame->id_game_list
                                    ],
                                    ['Authorization' => 'Bearer ' . $token]);


        $getMyInvoice->assertStatus(500);
        $getMyInvoice->assertJson([
            "status" => false,
            "message" => "Hanya tersisa ".$sisaTiket." unit tiket yang dapat dibeli"
            ]);
    }

    public function testCreateInvoiceWithNoTokenAuthorization()
    {

        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_isd' => '23e12wdes',
                                        'quantiwety' => 3
                                    ],
                                    []);


        $getMyInvoice->assertStatus(401);
        $getMyInvoice->assertJsonStructure([
            "status",
            "message"
        ]);
    }

    public function testCreateInvoiceWithInvalidRequest()
    {

        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();

        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);


        $getMyInvoice = $this->post('api/ticket/invoice/create',
                                    [
                                        'ticket_isd' => '23e12wdes',
                                        'quantiwety' => 3
                                    ],
                                    ['Authorization' => 'Bearer ' . $token]);


        $getMyInvoice->assertStatus(422);
        $getMyInvoice->assertJsonStructure([
            "message",
            "errors" => [
                "ticket_id",
                "quantity",
                "game_id"
            ]
        ]);
    }

}
