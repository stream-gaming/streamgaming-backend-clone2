<?php

namespace Tests\Feature;

use App\Model\User;
use App\Traits\UserHelperTrait;
use Illuminate\Support\Facades\Schema;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tests\TestCase;

class MyTournamentTest extends TestCase
{
    use UserHelperTrait;

    public function testAddOngoingAndFinishedKeyOnMyTournamentResponses()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::insert([
            "user_id"  => uniqid(),
            "fullname" => "Tester",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "password" => bcrypt('streamgaming27'),
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'streamgaming27']);
        //do something
        $response = $this->post('/api/user/my-tournament', [], ['Authorization' => 'Bearer ' . $token]);
        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "on_going",
                    "finished",
                ],
            ]);;
    }
}
