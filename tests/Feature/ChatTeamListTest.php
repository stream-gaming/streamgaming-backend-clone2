<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChatTeamListTest extends TestCase
{
    /**
     * Test get list team chat scenario success.
     *
     * @return void
     */
    public function testGetListTeamChatScenarioSuccess()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        $response = $this->get('/api/chat/team/list',['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'date',
                    'logo',
                    'team_id',
                    'team_name',
                    'team_url',
                    'total_member',
                    'typing',
                    'unread',
                    'url',
                    'last_chat' => [
                        'attachment',
                        'message',
                        'name'
                    ]
                ]
            ],
            'user' => [
                'user_id',
                'fullname',
                'username',
                'jenis_kelamin',
                'email',
                'refferal_code',
                'tgl_lahir',
                'bio',
                'status',
                'is_verified',
                'is_active',
                'created_at',
                'updated_at',
                'bri_va',
                'password_reset_token',
                'firebase_token',
                'is_partner',
                'is_phone_verified',
                'user_card_skin_id',
            ],
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test get list team chat scenario fail not login.
     *
     * @return void
     */
    public function testGetListTeamChatScenarioFailedNotLogin()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->get('/api/chat/team/list');

        $response->assertStatus(401);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
