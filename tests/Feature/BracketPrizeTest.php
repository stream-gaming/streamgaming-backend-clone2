<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\TournamentModel;
use App\Traits\UserHelperTrait;
use Tests\TestCase;

class BracketPrizeTest extends TestCase
{
    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testBracketCustomPrizeOnlyOneSequence(){
        TournamentModel::truncate();
        $data = factory(TournamentModel::class)->create([
            'system_tournament' => '4',
            'tourname_type'     => 'single',
            'game_option'       => '3',
            'total_prize'       => '1000,0,0'
        ]);
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/prizes";
        $response = $this->get($urlTesting);
        $response->assertStatus(200);
        $response->assertJsonStructure(['status']); 
        $response->assertJsonFragment([
            "prize" => [
                [
                    "sequence" => 1,
                    "title" => "Juara 1",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "1000"
                        ]
                    ]
                ]
            ]
        ]);
         //Check if there is no second sequence
         $response->assertJsonMissing([
            "prize" => [
                [
                    "sequence" => 2,
                    "title" => "Juara 2",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "0"
                        ]
                    ]
                ]
            ]
        ]);
        //Check if there is no third sequence
        $response->assertJsonMissing([
            "prizes" => [
                [
                    "sequence" => 3,
                    "title" => "Juara 3",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "0"
                        ]
                    ]
                ]
            ]
        ]);

        TournamentModel::where('url',$data['url'])->delete();
        RedisHelper::destroyWithPattern('bracket.bracket.'.$data['url']);

    }

    public function testBracketCustomPrizeAllSequence(){

        TournamentModel::truncate();
        $data = factory(TournamentModel::class)->create([
            'system_tournament' => '4',
            'tourname_type'     => 'single',
            'game_option'       => '3',
            'total_prize'       => '10000,5000,2500'
        ]);
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/prizes";
        $response = $this->get($urlTesting);
        $response->assertStatus(200);
        $response->assertJsonStructure(['status']); 

        $response->assertJsonFragment([
            "prize" => [
                [
                    "sequence" => 1,
                    "title" => "Juara 1",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "10000"
                        ]
                    ]
                ],
                [
                    "sequence" => 2,
                    "title" => "Juara 2",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "5000"
                        ]
                    ]
                ],
                [
                    "sequence" => 3,
                    "title" => "Juara 3",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "unit" => 0,
                            "prize" => "2500"
                        ]
                    ]
                ]
            ]
        ]);

        TournamentModel::where('url',$data['url'])->delete();
        RedisHelper::destroyWithPattern('bracket.bracket.'.$data['url']);
    }
    
}