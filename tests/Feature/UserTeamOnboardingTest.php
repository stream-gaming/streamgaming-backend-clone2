<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Model\UsersGameModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserTeamOnboardingTest extends TestCase
{

    public function testUpdateOnboardingProgressWhenCreatingTeamWithValidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        //get city
        $city = CityModel::first();

        //Create team
        $createTeam = $this->post(Route('create.team'), [
            'team_name' => 'team tes onboarding',
            'game_id' => $game->id_game_list,
            'team_description' => 'tesss',
            'city' => $city->nama,
            'team_logo' => UploadedFile::fake()->image('photo1.png')
        ], ['Authorization' => 'Bearer ' . $token]);

        $createTeam->assertStatus(201);
        $createTeam->assertJson([
            'status' => true,
            'message' => Lang::get('message.team.add.success')
        ]);

        //Check user onboarding after creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::CREATE_OR_FIND_TEAM_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);
    }

    public function testUpdateOnboardingProgressWhenCreatingTeamWithInvalidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        //Create team with invalid credential
        $createTeam = $this->post(Route('create.team'), [
            'team_name' => 'team tes onboarding',
            'game_id' => '',
            'team_description' => 'tesss',
            'city' => '',
            'team_logo' => ''
        ], ['Authorization' => 'Bearer ' . $token]);

        //Check failed response
        $createTeam->assertStatus(422);
        $createTeam->assertJsonStructure([
            'message',
            'errors'
        ]);

        //Check user onboarding after creating team if still same
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }

    public function testUpdateOnboardingProgressWhenCreatingTeamAsExistingUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        UsersGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Schema::enableForeignKeyConstraints();

        $existingUser = factory(User::class)->create([
            'email' => 'fahmiexistinguser@gmail.com'
        ]);

        $game = factory(GameModel::class)->create();
        UsersGameModel::insert(['users_id' => $existingUser->user_id, 'game_id' => '']);
        $token = JWTAuth::attempt(['email' => 'fahmiexistinguser@gmail.com', 'password' => 'admin']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseMissing('user_onboarding', [
            'user_id' => $existingUser->user_id
        ]);

        //get city
        $city = CityModel::first();

        //Create team
        $createTeam = $this->post(Route('create.team'), [
            'team_name' => 'team tes onboarding',
            'game_id' => $game->id_game_list,
            'team_description' => 'tesss',
            'city' => $city->nama,
            'team_logo' => UploadedFile::fake()->image('photo1.png')
        ], ['Authorization' => 'Bearer ' . $token]);

        $createTeam->assertStatus(201);
        $createTeam->assertJson([
            'status' => true,
            'message' => Lang::get('message.team.add.success')
        ]);

        //Check user onboarding after creating team
        $this->assertDatabaseMissing('user_onboarding', [
            'user_id' => $existingUser->user_id
        ]);
    }

    public function testUpdateOnboardingProgressWhenJoinTeamUsingValidInvitationCode(){

        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code'), [
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(201);

        //Check user onboarding after creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::CREATE_OR_FIND_TEAM_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);
    }

    public function testUpdateOnboardingProgressWhenJoinTeamUsingInvalidInvitationCode(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2'
        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code'), [
            'invitation_code' =>  'WrongInvCode',
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(422);

        //Check user onboarding after creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }

    public function testUpdateOnboardingProgressWhenJoinTeamUsingExpInvitationCode(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        InvitationCode::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Create team
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 2
        ]);
        $user = factory(User::class)->create(['fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);
        $teamId = uniqid();
        factory(TeamModel::class)->create(['team_id' => $teamId,'leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        //Generate team invitation code
        $invCode = factory(InvitationCode::class)->create([
            'team_id' => $teamId,
            'code' => 'AHGJ7Z2',
            'expired_at' => '2021-03-09 10:09:50'

        ]);

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        //Join using invitation code
        $joinUsingInvCode = $this->post(Route('join.team.inv.code'), [
            'invitation_code' =>  $invCode->code,
        ],['Authorization' => 'Bearer ' . $token]);
        $joinUsingInvCode->assertStatus(403);

        //Check user onboarding after creating team
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }
}
