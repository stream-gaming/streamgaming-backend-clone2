<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\TIcketPurchaseHistoryModel;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use Xendit\Invoice;
use Xendit\Xendit;

class TicketPaymentCallbackForXenditTest extends TestCase
{
    /*  !!!!READ ME!!!
        Before running this test, kindly add the token key on your env like bellow:

        XENDIT_AUTH_TOKEN=xnd_development_BI4Ia4n7kVSHX7kMXAhwrfS7dnFsByAxkIpv4r1V2sIpjptOn63VXkb2BCrPx
        XENDIT_CALLBACK_TOKEN=GehJyVOqTw80rGgcuY6A6oqVfGHTf1gKmNPeZbfh3z0R0pXs
    */
    public function testUpdateInvoiceStatusPaidWithValidRequestForSoloMode()
    {

        DB::table('users')->where('email', 'devjav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create(['email' => 'devjav113@gmail.com','password' => bcrypt('Admin12345!'), 'provinsi'=> 12]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        //create ticket
        $createTicketSolo = factory(AllTicketModel::class)->create([
            'mode' => 1,
            'max_given' => 0,
            'market_value' => 11000
        ]);
        $amount = ($createTicketSolo->market_value*2);
        $order_id = strtotime(Carbon::now());
        //generate invoice
        $createInvoice = Invoice::create([
            'for-user-id' => config('app.xendit_xenplatform_user_id'),
            'external_id' => '#'.$order_id.'-stream',
            'payer_email' => $user->email,
            'description' => 'This invoice generated by Feature Test Laravel!! StreamGaming~~ DevJav',
            'amount' => $amount,
            'customer' => ['email' => $user->email]
        ]);


        $ticket_purchase_history = factory(TIcketPurchaseHistoryModel::class)->create([
            'user_id' => $user->user_id,
            'order_id' => '#'.$order_id.'-stream',
            'xendit_invoice_id' => $createInvoice['id'],
            'team_id' => null,
            'ticket_id' => $createTicketSolo->id,
            'amount' => $amount,
            'status' => 0,
            'expired_at' => Carbon::parse($createInvoice['expiry_date'])->setTimezone('Asia/Jakarta')
        ]);

        $getMyInvoice = $this->post('api/ticket/invoice/callback',
                                    [
                                    'id' => $createInvoice['id'],
                                    'external_id'=> $createInvoice['external_id'],
                                    'payment_method'=> 'MANDIRI',
                                    'status'=> 'PAID',
                                    'amount'=> $createInvoice['amount'],
                                    'paid_amount'=> $createInvoice['amount'],
                                    'paid_at'=> strtotime(Carbon::now()),
                                    'currency'=> 'IDR',
                                    'payment_channel'=> 'Xendit'
                                ],
                                    ['X-CALLBACK-TOKEN' => env('XENDIT_CALLBACK_TOKEN')]);


        $getMyInvoice->assertStatus(200);
        $getMyInvoice->assertJson([
            'status' => true,
            'message'=> 'Hi, we have received this event successfully',
        ]);
    }
    public function testUpdateInvoiceStatusPaidWithValidRequestForSquadMode()
    {
        /* */
        $teamId = 'teamtest123!id22';
        $gameId = 'game1234tes!';
        $userId= 'userId!23';
        DB::table('users')->where('email', 'devJav113@gmail.com')->delete();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        DB::table('team')->where('team_id', $teamId)->delete();
        DB::table('team_player')->where('team_id', $teamId)->delete();
        DB::table('identity_ingame')->where('users_id', $userId)->where('game_id', $gameId)->delete();

        //Initialize api key
        Xendit::setApiKey(env('XENDIT_AUTH_TOKEN'));
        //Create user
        $user = factory(User::class)->create([
            'user_id' => $userId,
            'email' => 'devjav113@gmail.com',
            'password' => bcrypt('Admin12345!'),
            'provinsi'=> 12
        ]);

        //create identity game
        $addGame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id' => $gameId
        ]);
        //create team
        $createTeam = factory(TeamModel::class)->create([
            'team_id' => $teamId,
            'leader_id' => $user->user_id,
            'game_id' => $gameId,
            'venue' => 'medan'
        ]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);

        //create ticket
        $createTicketTeam = factory(AllTicketModel::class)->create([
            'mode' => 3,
            'max_given' => 0,
            'market_value' => 11000,
            'competition' => $gameId
        ]);
        $amount = ($createTicketTeam->market_value*2);
        $order_id = strtotime(Carbon::now());
        //generate invoice
        $createInvoice = Invoice::create([
            'for-user-id' => config('app.xendit_xenplatform_user_id'),
            'external_id' => '#'.$order_id.'-stream',
            'payer_email' => $user->email,
            'description' => 'This invoice generated by Feature Test Laravel!! StreamGaming~~ DevJav',
            'amount' => $amount,
            'customer' => ['email' => $user->email]
        ]);


        $ticket_purchase_history = factory(TIcketPurchaseHistoryModel::class)->create([
            'user_id' => $user->user_id,
            'order_id' => '#'.$order_id.'-stream',
            'xendit_invoice_id' => $createInvoice['id'],
            'team_id' => $createTeam->team_id,
            'ticket_id' => $createTicketTeam->id,
            'amount' => $amount,
            'status' => 0,
            'expired_at' => Carbon::parse($createInvoice['expiry_date'])->setTimezone('Asia/Jakarta')
        ]);

                $getMyInvoice = $this->post('api/ticket/invoice/callback',
                                    [
                                    'id' => $createInvoice['id'],
                                    'external_id'=> $createInvoice['external_id'],
                                    'payment_method'=> 'MANDIRI',
                                    'status'=> 'PAID',
                                    'amount'=> $createInvoice['amount'],
                                    'paid_amount'=> $createInvoice['amount'],
                                    'paid_at'=> strtotime(Carbon::now()),
                                    'currency'=> 'IDR',
                                    'payment_channel'=> 'Xendit'
                                ],
                                    ['X-CALLBACK-TOKEN' => env('XENDIT_CALLBACK_TOKEN')]);


        $getMyInvoice->assertStatus(200);
        $getMyInvoice->assertJson([
            'status' => true,
            'message'=> 'Hi, we have received this event successfully',
        ]);
    }

    public function testUpdateInvoiceStatusPaidWithInvalidTokenAuthorization()
    {
                       $getMyInvoice = $this->post('api/ticket/invoice/callback',
                                    [
                                    'id' => '21312312312',
                                    'external_id'=> 'asdasdasdas',
                                    'payment_method'=> 'MANDIRI',
                                    'status'=> 'PAID',
                                    'amount'=> 'asdasdasdasda',
                                    'paid_amount'=> 'asdasdasdasd',
                                    'paid_at'=> strtotime(Carbon::now()),
                                    'currency'=> 'IDR',
                                    'payment_channel'=> 'Xendit'
                                ],
                                    ['X-CALLBACK-TOKEN' => 'wrongtoken']);


        $getMyInvoice->assertStatus(401);
        $getMyInvoice->assertJsonStructure([
            "status",
            "message"
        ]);
    }
}
