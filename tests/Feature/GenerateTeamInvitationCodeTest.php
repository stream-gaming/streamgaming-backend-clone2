<?php

namespace Tests\Feature;

use App\Model\AllTournamentModel;
use App\Model\GameModel;
use App\Model\InvitationCode;
use App\Model\LeaderboardModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use App\Model\User;
use App\Modules\Team\TeamInvitationCodeState;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class GenerateTeamInvitationCodeTest extends TestCase
{
    /**
     * Test Generate Invitation Code Scenario Success.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 0
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => $team->team_id ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'invitationCode',
                'expiredAt',
                'state'
            ]
        ]);

        $response->assertJsonPath('status', true);
        $response->assertJsonPath('data.state', TeamInvitationCodeState::ACTIVE);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Generate Invitation Code Scenario Fail Team Not Found.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioFailTeamNotFound()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser(User::find($user->user_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => 'test' ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(404);

        $response->assertJsonStructure([
            'status',
            'message',
        ]);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.not')
        ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Generate Invitation Code Scenario Fail Not Leader.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioFailNotLeader()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 0
        ]);

        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser(User::find($user->user_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => $team->team_id ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(401);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.leader')
        ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Generate Invitation Code Scenario Last Code Not Expired Yet.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioLastCodeNotExpiredYet()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 0
        ]);

        factory(InvitationCode::class)->create([
            'team_id'    => $team->team_id,
            'expired_at' => Carbon::now()->addHours(6)->toDateTimeString()
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => $team->team_id ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(400);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('team.invitation_code.not_expired')
        ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Generate Invitation Code Scenario Team Banned.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioTeamBanned()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 1
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => $team->team_id ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(403);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.banned',[
                'time'=> $team->ban_until != 'forever' ? Carbon::parse($team->ban_until)->diffForHumans() :'forever'
            ])
        ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        InvitationCode::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Get Invitation Code Scenario Success Team State Is Join Tournament.
     *
     * @return void
     */
    public function testGenerateInvitationCodeScenarioTeamStateIsJoinTournament()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        PlayerPayment::truncate();
        LeaderboardModel::truncate();
        TournamentModel::truncate();
        AllTournamentModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        $team = factory(TeamModel::class)->create([
            'game_id' => $game->id_game_list
        ]);


        $leaderboard = factory(LeaderboardModel::class)->create([
            'game_competition' => $game->id_game_list
        ]);

        $all_tournament = factory(AllTournamentModel::class)->create([
            'id_tournament' => $leaderboard->id_leaderboard
        ]);

        factory(PlayerPayment::class)->create([
            'id_tournament' => $all_tournament->id_tournament,
            'id_team'       => $team->team_id,
            'player_id'     => $team->leader_id,
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->post('/api/team/invitation-code/generate', [ 'teamId' => $team->team_id ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(403);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('team.join_tournament')
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        PlayerPayment::truncate();
        LeaderboardModel::truncate();
        TournamentModel::truncate();
        AllTournamentModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
