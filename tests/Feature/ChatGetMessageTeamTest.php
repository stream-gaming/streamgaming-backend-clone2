<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChatGetMessageTeamTest extends TestCase
{
    /**
     * Test get message list scenario success.
     *
     * @return void
     */
    public function testGetMessageListScenarioSuccess()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        factory(MessageTeamModel::class, 16)->create([
            'team_id' => $team->team_id
        ]);

        $token = JWTAuth::fromUser($user);

        $response = $this->get('/api/chat/team/fetch/'.$teamId,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'header' => [
                'team_id', 'team_name', 'logo', 'member'
            ],
            'messages' => [
                'data' => [
                    '*' => [
                        'id',
                        'user' => [
                            'username', 'role', 'picture'
                        ],
                        'is_me',
                        'is_read',
                        'message',
                        'created_at',
                        'attachment'
                    ]
                ],
                'meta' => [
                    'pagination' => [
                        'total', 'count', 'per_page', 'current_page', 'total_pages', 'links'
                    ]
                ]
            ]
        ]);
    }

    public function testGetMessageListScenarioFailNotMember()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $token = JWTAuth::fromUser($user);

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        $response = $this->get('/api/chat/team/fetch/'.$teamId,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(403);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

}
