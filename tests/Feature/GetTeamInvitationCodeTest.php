<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\AllTournamentModel;
use App\Model\GameModel;
use App\Model\InvitationCode;
use App\Model\LeaderboardModel;
use App\Model\PlayerPayment;
use App\Model\TeamModel;
use App\Model\TournamentModel;
use App\Model\User;
use App\Modules\Team\TeamInvitationCodeState;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class GetTeamInvitationCodeTest extends TestCase
{
    /**
     * Test Get Invitation Code Scenario Success.
     *
     * @return void
     */
    public function testGetInvitationCodeScenarioSuccess()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 0
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->get('/api/team/invitation-code/'.$team->team_id,  ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'invitationCode',
            'expiredAt',
            'state'
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

     /**
     * Test Get Invitation Code Scenario Success Team State Is Banned.
     *
     * @return void
     */
    public function testGetInvitationCodeScenarioSuccessTeamStateIsBanned()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 1
        ]);

        $invitation_code = factory(InvitationCode::class)->create([
            'team_id' => $team->team_id
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->get('/api/team/invitation-code/'.$team->team_id,  ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'invitationCode',
            'expiredAt',
            'state'
        ]);

        $response->assertJson([
            'invitationCode' => $invitation_code->code,
            'expiredAt' => date_format($invitation_code->expired_at, 'Y-m-d H:i:s'),
            'state' => TeamInvitationCodeState::BANNED
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Get Invitation Code Scenario Success Team State Is Join Tournament.
     *
     * @return void
     */
    public function testGetInvitationCodeScenarioSuccessTeamStateIsJoinTournament()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        PlayerPayment::truncate();
        LeaderboardModel::truncate();
        TournamentModel::truncate();
        AllTournamentModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        $team = factory(TeamModel::class)->create([
            'game_id' => $game->id_game_list
        ]);

        $invitation_code = factory(InvitationCode::class)->create([
            'team_id' => $team->team_id
        ]);


        $leaderboard = factory(LeaderboardModel::class)->create([
            'game_competition' => $game->id_game_list
        ]);

        $all_tournament = factory(AllTournamentModel::class)->create([
            'id_tournament' => $leaderboard->id_leaderboard
        ]);

        factory(PlayerPayment::class)->create([
            'id_tournament' => $all_tournament->id_tournament,
            'id_team'       => $team->team_id,
            'player_id'     => $team->leader_id,
        ]);

        $token = JWTAuth::fromUser(User::find($team->leader_id));

        $response = $this->get('/api/team/invitation-code/'.$team->team_id,  ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'invitationCode',
            'expiredAt',
            'state'
        ]);

        $response->assertJson([
            'invitationCode' => $invitation_code->code,
            'expiredAt' => date_format($invitation_code->expired_at, 'Y-m-d H:i:s'),
            'state' => TeamInvitationCodeState::IN_TOURNAMENT
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        PlayerPayment::truncate();
        LeaderboardModel::truncate();
        TournamentModel::truncate();
        AllTournamentModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Get Invitation Code Scenario Fail Team Not Found.
     *
     * @return void
     */
    public function testGetInvitationCodeScenarioFailTeamNotFound()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser(User::find($user->user_id));

        $response = $this->get('/api/team/invitation-code/exampleNotfoundteamId',  ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(404);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJsonPath('status', false);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Test Get Invitation Code Scenario Fail Not Leader.
     *
     * @return void
     */
    public function testGetInvitationCodeScenarioFailNotLeader()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create([
            'is_banned' => 0
        ]);

        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser(User::find($user->user_id));

        $response = $this->get('/api/team/invitation-code/'.$team->team_id,  ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(403);

        $response->assertJsonStructure([
            'status',
            'message'
        ]);

        $response->assertJsonPath('status', false);

        $response->assertJson([
            'status' => false,
            'message' => Lang::get('message.team.leader')
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
