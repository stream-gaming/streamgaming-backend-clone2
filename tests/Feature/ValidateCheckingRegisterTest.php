<?php

namespace Tests\Feature;

use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\User;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Carbon\Carbon;
use Tests\TestCase;

class ValidateCheckingRegisterTest extends TestCase
{

    public function testCheckUsernameIfItIsAlreadyBeenTaken()
    {
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();

        $user = factory(User::class)->create([
            "user_id"  => uniqid(),
            "fullname" => "Tester",
            "username" => "testname",
            "email"    => "lordfahdan@gmail.com",
            "password" => bcrypt('stream'),
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
        ]);

        $type = 'username';

        $response = $this->get('/api/exist?type=' . $type . '&value=' . $user->username, []);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
            ]);
    }

    public function testCheckEmailIfItIsAlreadyBeenTaken()
    {
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();

        $user = factory(User::class)->create([
            "user_id"  => uniqid(),
            "fullname" => "Tester",
            "username" => "testname",
            "email"    => "lordfahdan@gmail.com",
            "password" => bcrypt('stream'),
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
        ]);

        $type = 'email';

        $response = $this->get('/api/exist?type=' . $type . '&value=' . $user->email, []);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
            ]);
    }
}
