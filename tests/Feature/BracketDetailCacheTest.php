<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\TournamentModel;
use Tests\TestCase;

class BracketDetailCacheTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBracketDetailOverviewCache()
    {
         $data = factory(TournamentModel::class)->create();
         $getUrl = $data->url;
         $urlTesting = "/api/tournament/".$getUrl."/overview";
         //Set cache
         $response = $this->get($urlTesting);

         $response->assertStatus(200);
         $response->assertJsonStructure(['status']);
 
         //Get cache
         $getDataCahe = RedisHelper::getData('bracket.overview.'.$data['url']);
         $this->assertNotFalse($getDataCahe);
 
         //Clear data and cache
         TournamentModel::where('url',$data['url'])->delete();
         RedisHelper::destroyWithPattern('bracket.overview.'.$data['url']);
    }
    public function testBracketDetailRulesCache(){
        $data = factory(TournamentModel::class)->create();
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/rules";
        //Set cache
        $response = $this->get($urlTesting);
        $response->assertStatus(200);
        $response->assertJsonStructure(['status']);

        //Get cache
        $getDataCahe = RedisHelper::getData('bracket.rules.'.$data['url']);
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        TournamentModel::where('url',$data['url'])->delete();
        RedisHelper::destroyWithPattern('bracket.rules.'.$data['url']);
    }

    public function testBracketDetailParticipantCache(){
        $data = factory(TournamentModel::class)->create();
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/participant";
        //Set cache
        $response = $this->get($urlTesting);
    
        $response->assertStatus(200);
        $response->assertJsonStructure(['status']);

        //Get cache
        $getDataCahe = RedisHelper::getData('bracket.participant.'.$data['url']);
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        TournamentModel::where('url',$data['url'])->delete();
        RedisHelper::destroyWithPattern('bracket.participant.'.$data['url']);
    }

    public function testBracketDetailPrizesCache(){
        $data = factory(TournamentModel::class)->create();
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/prizes";
        //Set cache
        $response = $this->get($urlTesting);
        $response->assertStatus(200);
        $response->assertJsonStructure(['status']);
  
        //Get cache
        $getDataCahe = RedisHelper::getData('bracket.prizes.'.$data['url']);
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        TournamentModel::where('url',$data['url'])->delete();
        RedisHelper::destroyWithPattern('bracket.prizes.'.$data['url']);
    }

    public function testBracketDetailScheduleCache(){
        $data = factory(TournamentModel::class)->create(['status' => 'starting']);
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/schedule";
             //Set cache
            $response = $this->get($urlTesting);
            $response->assertStatus(200);
            $response->assertJsonStructure(['status']);
  
            // Get cache
            $getDataCahe = RedisHelper::getData('bracket.schedule.'.$data['url']);
            $this->assertNotFalse($getDataCahe);

            //Clear data and cache
            TournamentModel::where('url',$data['url'])->delete();
            RedisHelper::destroyWithPattern('bracket.schedule.'.$data['url']);

    }

    public function testBracketDetailBracketCache(){
        $data = factory(TournamentModel::class)->create(['status' => 'starting']);
        $getUrl = $data->url;
        $urlTesting = "/api/tournament/".$getUrl."/bracket";
            //Set cache
            $response = $this->get($urlTesting);
            $response->assertStatus(200);
            $response->assertJsonStructure(['status']);


            //Get cache
            $getDataCahe = RedisHelper::getData('bracket.bracket.'.$data['url']);
            $this->assertNotFalse($getDataCahe);

            //Clear data and cache
            TournamentModel::where('url',$data['url'])->delete();
            RedisHelper::destroyWithPattern('bracket.bracket.'.$data['url']);
    }
    public function testDeleteBracketCache(){
        $data = factory(TournamentModel::class)->create();
        
        $getUrl = $data->url;
        $mockCache = (new RedisHelper)->setData('bracket.test.'.$getUrl, ['data'=> 'testttt'], 200);
        $data = [
            'url' => "bracket.test.".$getUrl
        ];
        $response = $this->post('/api/Redis/destroyData',  $data);
        $response->assertStatus(200);
    }
}
