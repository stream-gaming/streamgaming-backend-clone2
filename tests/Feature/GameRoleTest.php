<?php

namespace Tests\Feature;

use App\Helpers\Validation\CheckBanned;
use App\Model\User;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class GameRoleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAddGameRoleSucces()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        UsersGameModel::insert(['users_id' => 'asdf', 'game_id' => '']);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $game_role = factory(GameRoleModel::class, 2)->create(['game_id' => $game->id_game_list]);

        $response = $this->post('/api/game/add/' . $game->id_game_list, [
            'id_ingame' => '123456789',
            'username_ingame' => 'HELLOGUYS',
            'game_role_id' => [$game_role[0]->id, $game_role[1]->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUpdateGameRoleSucces()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = GameModel::first();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        UsersGameModel::insert(['users_id' => 'asdf', 'game_id' => '']);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $game_role = factory(GameRoleModel::class)->create(['game_id' => $game->id_game_list]);

        $response = $this->put('/api/game/edit/' . $game->id_game_list, [
            'id_ingame' => '12345679',
            'username_ingame' => 'HELLOGUYS1',
            'game_role_id' => [$game_role->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUpdateGameRoleFailed()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = GameModel::first();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        UsersGameModel::insert(['users_id' => 'asdf', 'game_id' => '']);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $game_role = factory(GameRoleModel::class, 4)->create(['game_id' => $game->id_game_list]);

        $response = $this->put('/api/game/edit/' . $game->id_game_list, [
            'id_ingame' => '12345679',
            'username_ingame' => 'HELLOGUYS1',
            'game_role_id' => [$game_role[0]->id, $game_role[1]->id, $game_role[2]->id, $game_role[3]->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(422);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAddGameRoleFailed()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);
        UsersGameModel::insert(['users_id' => 'asdf', 'game_id' => '']);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $game_role = factory(GameRoleModel::class, 4)->create(['game_id' => $game->id_game_list]);

        $response = $this->post('/api/game/add/' . $game->id_game_list, [
            'id_ingame' => '123456789',
            'username_ingame' => 'HELLOGUYS',
            'game_role_id' => [$game_role[0]->id, $game_role[1]->id, $game_role[2]->id, $game_role[3]->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(422);
    }
}
