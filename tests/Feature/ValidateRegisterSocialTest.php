<?php

namespace Tests\Feature;

use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\OAuthModel;
use App\Model\User;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

//- Testing
class ValidateRegisterSocialTest extends TestCase
{

    public function testRegisterIfFullnameNotFilled()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();


        $user = [
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-27-12",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'fullname' => []
                ]
            ]);
    }

    // public function testRegisterIfGenderNotFilled()
    // {
    //     Schema::disableForeignKeyConstraints();
    //     User::truncate();
    //     UsersDetailModel::truncate();
    //     BalanceUsersModel::truncate();
    //     AuthenticateModel::truncate();
    //     UsersGameModel::truncate();
    //     UserPreferencesModel::truncate();
    //     OAuthModel::truncate();
    //     Schema::enableForeignKeyConstraints();


    //     $user = [
    //         "fullname" => "Melli Tiara",
    //         "username" => "testname",
    //         "email"    => "melly.tiara92@gmail.com",
    //         "tgl_lahir" => "1999-27-12",
    //         "password" => "streamgaming27",
    //         "password_confirmation" => "streamgaming27",
    //         'link_image'    => 'link_image',
    //         'oauth_id'      => 'oauth_id',
    //         'provider_id'   => 'provider_id',
    //     ];

    //     $response = $this->post('/api/auth/register-social', $user, []);

    //     $response->assertStatus(422)
    //         ->assertJsonStructure([
    //             'message',
    //             'errors' => [
    //                 'jenis_kelamin' => []
    //             ]
    //         ]);
    // }

    public function testRegisterIfGenderInvalid()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();


        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "tgl_lahir" => "1999-27-12",
            "jenis_kelamin" => "bencong",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'jenis_kelamin' => []
                ]
            ]);
    }

    public function testRegisterIfUsernameAlreadyBeenTaken()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create([
            "user_id"  => uniqid(),
            "fullname" => "Tester",
            "username" => "testname",
            "email"    => "lordfahdan@gmail.com",
            "password" => bcrypt('stream'),
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
        ]);

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'username' => []
                ]
            ]);
    }

    public function testRegisterIfEmailAlreadyBeenTaken()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create([
            "user_id"  => uniqid(),
            "fullname" => "Tester",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "password" => bcrypt('stream'),
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
        ]);

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email' => []
                ]
            ]);
    }

    public function testRegisterIfDateOfBirthInvalid()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-02-29",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'tgl_lahir' => []
                ]
            ]);
    }

    public function testRegisterIfDateOfBirthToYoung()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => Carbon::now()->subYears(3),
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'tgl_lahir' => []
                ]
            ]);
    }

    public function testRegisterIfDateOfBirthToOld()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => Carbon::parse("1969-01-01"),
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'tgl_lahir' => []
                ]
            ]);
    }

    public function testRegisterIfPasswordInvalid()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "streamuniverse",
            "password_confirmation" => "streamuniverse",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'password' => []
                ]
            ]);
    }

    public function testRegisterIfPasswordIsNotLeast8Character()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "oke",
            "password_confirmation" => "oke",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'password' => []
                ]
            ]);
    }

    public function testRegisterIfPasswordConfirmPasswordDoesntMatch()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming20",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'password' => []
                ]
            ]);
    }

    public function testRegisterSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        OAuthModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = [
            "fullname" => "Melli Tiara",
            "username" => "testname",
            "email"    => "melly.tiara92@gmail.com",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password" => "streamgaming27",
            "password_confirmation" => "streamgaming27",
            'link_image'    => 'link_image',
            'oauth_id'      => 'oauth_id',
            'provider_id'   => 'provider_id',
        ];

        $response = $this->post('/api/auth/register-social', $user, []);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "access_token",
                "token_type",
                "expires_in",

            ]);
    }
}
