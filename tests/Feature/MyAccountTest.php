<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Jobs\ProcessFirebaseCounter;
use App\Model\AuthenticateModel;
use App\Model\BalanceUsersModel;
use App\Model\RefferalConfig;
use App\Model\RefferalHistory;
use App\Model\User;
use App\Model\UserNotificationModel;
use App\Model\UserPreferencesModel;
use App\Model\UsersDetailModel;
use App\Model\UsersGameModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class MyAccountTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegistration()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UsersDetailModel::truncate();
        BalanceUsersModel::truncate();
        AuthenticateModel::truncate();
        UsersGameModel::truncate();
        UserPreferencesModel::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->post('/api/auth/register', [
            "fullname" => "Melli Tiara",
            "username" => "mellydecyber",
            "email"    => "melly.tiara92@gmail.com",
            "password" => "streamgaming27",
            "jenis_kelamin" => "perempuan",
            "tgl_lahir" => "1999-12-27",
            "password_confirmation" => "streamgaming27",
        ], []);


        //validate result
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testVerifiedEmail()
    {
        $user = User::where('email', 'melly.tiara92@gmail.com')->first();

        $url = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 15)),
            [
                'id' => $user->user_id,
                'hash' => sha1($user->email),
            ]
        );

        //do something
        $response = $this->get($url);

        //validate result
        $response->assertStatus(200);
    }

}
