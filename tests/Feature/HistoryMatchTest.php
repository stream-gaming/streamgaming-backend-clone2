<?php

namespace Tests\Feature;

use App\Model\BracketDataMatchs;
use App\Model\BracketOneTwoEight;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardScoreMatchModel;
use App\Model\PlayerPayment;
use App\Model\ProvinceModel;
use App\Model\ScoreMatchsModel;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use App\Model\User;
use DateTime;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

use function PHPUnit\Framework\assertTrue;

class HistoryMatchTest extends TestCase
{

    public function testAddTimestampOnHistoryMatchBracketResponses()
    {
        Schema::disableForeignKeyConstraints();
        LeaderboardModel::truncate();
        LeaderboardScoreMatchModel::truncate();
        TournamentModel::truncate();
        TournamenGroupModel::truncate();
        TournamentGroupPlayerModel::truncate();
        BracketDataMatchs::truncate();
        ScoreMatchsModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        TeamModel::truncate();
        PlayerPayment::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create();

        // Mock Bracket
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id]);
        $identitiy_ingame = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 0]);
        $unique = uniqid();
        factory(TournamentModel::class)->create(['kompetisi' => $identitiy_ingame->game_id, 'id_tournament' => $unique]);

        factory(PlayerPayment::class)->create([
            'player_id' => $user->user_id,
            'id_team' => $team->team_id,
            'id_tournament' => $unique
        ]);

        factory(BracketOneTwoEight::class)->create(['id_tournament' => $unique, 'team1' => $team->team_id, 'winner' => $team->team_id]);

        $response = $this->get('api/user/match/' . $user->username, []);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" =>  [
                    "*" => [
                        "history" => [
                            "*" =>  [
                                "tanggal"
                            ]
                        ]
                    ]
                ]
            ]);


        foreach ($response->getData()->data as $val) {
            foreach ($val->history as $date) {
                assertTrue($this->validateDate($date->tanggal));
            }
        }
    }

    public function testAddTimestampOnHistoryMatchLeaderboardsResponses()
    {
        Schema::disableForeignKeyConstraints();
        LeaderboardModel::truncate();
        LeaderboardScoreMatchModel::truncate();
        TournamentModel::truncate();
        TournamenGroupModel::truncate();
        TournamentGroupPlayerModel::truncate();
        BracketDataMatchs::truncate();
        ScoreMatchsModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        TeamModel::truncate();
        PlayerPayment::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create();
        // Mock Leaderboard
        $game = factory(GameModel::class)->create(['player_on_team' => 5, 'substitute_player' => 2, 'system_kompetisi' => 'LeaderBoards']);
        $identitiy_ingame = factory(IdentityGameModel::class)->create(['users_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list]);
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id]);
        $unique = uniqid();
        factory(LeaderboardModel::class)->create(['game_competition' => $identitiy_ingame->game_id, 'id_leaderboard' => $unique]);

        factory(PlayerPayment::class)->create([
            'player_id' => $user->user_id,
            'id_team' => $team->team_id,
            'id_tournament' => $unique
        ]);

        factory(LeaderboardScoreMatchModel::class)->create(['id_leaderboard' => $unique, 'id_team' => $team->team_id, 'id_player' => $user->user_id, 'is_win' => '1']);

        $response = $this->get('api/user/match/' . $user->username, []);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" =>  [
                    "*" => [
                        "history" => [
                            "*" =>  [
                                "tanggal"
                            ]
                        ]
                    ]
                ]
            ]);

        foreach ($response->getData()->data as $val) {
            foreach ($val->history as $date) {
                assertTrue($this->validateDate($date->tanggal));
            }
        }
    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
