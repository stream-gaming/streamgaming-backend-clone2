<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class TournamentListFrontPageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetData()
    {
        RedisHelper::destroyDataWithPattern('tournament:list:*');
        $response = $this->get('/api/tournament/v3');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "header" => [
                "title",
                "desc"
            ],
            "tab" => [
                '*' => [
                    "tabname",
                    "url"
                ]
            ],
            "url_load_all_tournament",
            "url_for_search_tournament",
            "url_filter_by_game",
            "data" => [
                "liga_card" => [
                    '*' =>[
                        "image",
                        "url_page",
                        "date_start",
                        "date_end",
                        "name",
                        "prize",
                        "slot",
                        "type",
                        "organizer",
                        "logo",
                        "game_name",
                        "game_id"
                    ]
                ],
                "tournament_with_category"=> [],
                "tournament_no_category"=> [
                    '*' => [
                        "title",
                        "card_banner",
                        "is_free",
                        "match_start",
                        "total_prize",
                        "hosted_by",
                        "match_info" => [
                            "game",
                            "game_on",
                            "mode"
                        ],
                        "status" => [
                            "text",
                            "number"
                        ],
                        "slots"=> [
                            "filled",
                            "available",
                            "slotstatus"
                        ],
                        "url",
                        "full_url",
                        "game_url",
                        "create_at",
                        "time_value",
                        "time"
                    ]
                ]
            ]
        ]);


    $this->assertTrue((bool) Redis::exists('tournament:list:front:all'));
    RedisHelper::destroyDataWithPattern('tournament:list:*');


    }
}
