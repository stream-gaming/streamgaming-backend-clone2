<?php
namespace Tests\Feature;

use App\Helpers\Ticket\TeamTicketFactory;
use App\Helpers\Ticket\TicketRole;
use App\Model\AllTicketModel;
use App\Model\AllTournamentModel;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TournamentModel;
use App\Model\User;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class JointTournamentChallongeTest extends TestCase
{
    public function testJointTour()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        Schema::enableForeignKeyConstraints();
        
        $user               = factory(User::class)->create(['is_active' => 1]);
        $ticket             = factory(AllTicketModel::class)->create(['name' => 'ticket_riski',]);
        $game               = factory(GameModel::class)->create();

        $identitiy_ingame = factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
        ]);
        $unique             = uniqid();
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id,'game_id'   => $identitiy_ingame->game_id])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });
        $tournament         = factory(TournamentModel::class)->create(['id_tournament' => $unique,'kompetisi' => $identitiy_ingame->game_id, 'system_payment' => $ticket->id, 'status' => 'open_registration', 'is_new_ticket_feature' => 1,   'is_challonge' => 1, 'url' => "tes_challonge"]);
        AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament' => $unique,
            'format' => 'Bracket',
            'create_at' => now()
        ]);
        $factory    = new TeamTicketFactory();
        $token              = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        $data               = $factory->addTicket(TeamModel::first()->team_id, $ticket->id, TicketRole::ADMINGIFT_SOURCE, 1);

        $response = $this->post('/api/join-tournament', ['id_tournament' => $unique], ['Authorization' => 'Bearer ' . $token]);
        $response->assertStatus(200);
    }
}
