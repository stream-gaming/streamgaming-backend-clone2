<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use Tests\TestCase;
use App\Model\SliderModel;
use App\Traits\UserHelperTrait;
use Illuminate\Http\UploadedFile;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Controllers\Admin\SliderController;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SliderCacheTest extends TestCase
{
    use UserHelperTrait;

    protected $cache_key = 'slider:list';
    protected $cache_key_detail = 'slider:show:';
    protected $cache_time = 604800
        /** cached up to a week */
    ;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testCreateCacheWhenGetSlider()
    {
        // store 4 dummy data to database
        // redis will be cached data
        // after data cached delete one data
        // the result expected must be same before data delete (4 data)
        // its works only if cached is working

        SliderModel::truncate();
        Redis::del($this->cache_key);

        $slider = factory(SliderModel::class, 4)->create();

        $response = $this->get('api/admin/slider');

        $slider->first()->delete();

        $response->assertStatus(200)
            ->assertJsonCount(4, 'data');
    }

    public function testReadEmptyDataFromCacheWhenGetSlider()
    {
        // Delete data from redis and delete data from database
        // when get slider data the result must be empty data

        Redis::del($this->cache_key);
        SliderModel::truncate();
        $response = $this->get('api/admin/slider');

        $response->assertStatus(200)
            ->assertJsonCount(0, 'data');
    }

    public function testTriggeredToDeleteCacheWhenAddNewData()
    {
        // access endpoint get slider to cached current data
        // add new data
        // current data cached already destroyed
        // we must be access endpoint get sider to recache new data
        // the expect is new data already in new cache (access get slider)

        SliderModel::truncate();
        Redis::del($this->cache_key);
        $user = $this->createUser();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $slider = factory(SliderModel::class)->create();
        $this->get('api/admin/slider'); //to cached current data

        // mock fake upload
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.png');
        $this->post('api/admin/slider/create' , [
            'title'     => 'Madhan',
            'image'     => $file,
            'url'       => 'https://streamgaming.id',
            'in_app'    => 0,
            'status'    => 1
        ], [
            'Authorization' => 'Bearer '. $token
            ])->assertStatus(200); // add new data will destroyed current cache data


        $this->get('api/admin/slider') //cached new data and check new data already exist in new cache?
        ->assertJsonFragment(['title'=>'Madhan'])
        ->assertJsonCount(2, 'data');
    }

    public function testTriggeredToDeleteCacheWhenEditedData()
    {
        // store dummy data
        // access endpoint get slider to cached current data
        // edit current data
        // current data cached already destroyed
        // we will compare old data to new data
        // the result compare expect different data (cached data is empty)
        // its only work if old cached data successfully destroyed

        SliderModel::truncate();
        Redis::del($this->cache_key);
        $user = $this->createUser();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $slider = factory(SliderModel::class)->create();
        $this->get('api/admin/slider');

        $this->post('api/admin/slider/update/' . $slider->id, [
            'title'     => 'Madhan',
            'status'    => 0
        ], [
            'Authorization' => 'Bearer '. $token
            ])->assertStatus(200);


        $this->get('api/admin/slider')
        ->assertJsonFragment(['title'=>'Madhan']);

        $getDataCahe = RedisHelper::getData($this->cache_key_detail . $slider->id);
        $this->assertFalse($getDataCahe);
    }

    public function testTriggeredToDeleteCacheWhenOneofDataDeleted()
    {
        // store two dummy data to database
        // cache current data with access get slider endpoint
        // deleled one of data
        // current data cached already destroyed
        // cached again (get slider endpoint)
        // when get slider endpoint the result must be one data show

        SliderModel::truncate();
        Redis::del($this->cache_key);
        $user = $this->createUser();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $slider = factory(SliderModel::class, 2)->create();
        $this->get('api/admin/slider');

        $this->delete('api/admin/slider/' . $slider->first()->id, [], [
            'Authorization' => 'Bearer '. $token
            ])->assertStatus(200);


        $this->get('api/admin/slider')
        ->assertJsonCount(1, 'data');

    }

    public function testCheckCachingWhenShowSlider()
    {
        // store dummy data to database
        // get endpoint detail slider to cache current data
        // the result from endpoint must be the same with data in database
        // and check data with key from redis result not false

        SliderModel::truncate();
        Redis::del($this->cache_key_detail . 1);

        $slider = factory(SliderModel::class)->create();
        $user = $this->createUser();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $response = $this->get('api/admin/slider/' . $slider->id, ['Authorization' => 'Bearer' . $token])->assertStatus(200)->assertJsonFragment(['title'=>$slider->title]);

        $getDataCahe = RedisHelper::getData($this->cache_key_detail . 1);
        $this->assertNotFalse($getDataCahe);
    }
}
