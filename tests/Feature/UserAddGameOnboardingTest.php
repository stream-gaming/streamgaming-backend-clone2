<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Model\UsersGameModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAddGameOnboardingTest extends TestCase
{

    public function testUpdateOnboardingProgressWhenAddingNewGameWithValidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //check on database if this user onboarding add-game-step has been set successfully
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => OnboardingEnum::ADD_GAME_STEP,
            'status' => 0
        ]);
    }

    public function testUpdateOnboardingProgressWhenAddingNewGameWithInvalidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        //Set invalid game credential
        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            // 'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        //Check error code status
        $addGame->assertStatus(422);
        $addGame->assertJsonStructure([
            'message',
            'errors'
        ]);

        //check on database if this user onboarding add-game-step doesn't update
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => 0
        ]);
    }

    public function testUpdateOnboardingProgressForSecondGameAsFinishedOnboardingStatus(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        //set status as finished
        $userOnboarding = UserOnboardingModel::where('user_id', $getUser->user_id)->update([
            'preference' => OnboardingEnum::SOLO_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);

        $gameNew = factory(GameModel::class)->create();
        $addGame = $this->post(Route('add.game', ['id' => $gameNew->id_game_list]), [
            'id_ingame' =>  'idtes120099',
            'username_ingame' => 'usernametesnew01919'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::SOLO_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);

    }

    public function testUpdateOnboardingProgressWhenAddingNewGameAsExistingUser(){

        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $existingUser = factory(User::class)->create([
            'email' => 'fahmiexistinguser@gmail.com'
        ]);

        $game = factory(GameModel::class)->create();
        UsersGameModel::insert(['users_id' => $existingUser->user_id, 'game_id' => '']);
        $token = JWTAuth::attempt(['email' => 'fahmiexistinguser@gmail.com', 'password' => 'admin']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);

        $this->assertDatabaseMissing('user_onboarding', [
            'user_id' => $existingUser->user_id
        ]);
    }
}
