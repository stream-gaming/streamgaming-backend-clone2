<?php

namespace Tests\Feature;

use App\Model\User;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use App\Model\OnboardingModel;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class OnboardingTest extends TestCase
{

    public function testOnboardingSet()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        OnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create([
            'user_id' => 'asdf',
            'fullname' => 'test',
            'status' => 'player',
            'is_verified' => '1',
            'username' => 'test',
            'email' => 'test@email.com',
            'password' => bcrypt('asdfasdf')
        ]);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $response = $this->post('/api/account/onboarding/set', [
            'status' => "1",
            'app'    => 'web'
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "status"
            ]);
    }

    public function testOnboardingCheck()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        OnboardingModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create([
            'user_id' => 'asdf',
            'fullname' => 'test',
            'status' => 'player',
            'is_verified' => '1',
            'username' => 'test',
            'email' => 'test@email.com',
            'password' => bcrypt('asdfasdf')
        ]);

        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        $response = $this->get('/api/account/onboarding/check?app=ios', ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "status"
            ]);
    }
}
