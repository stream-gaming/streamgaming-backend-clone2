<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TicketModel;
use App\Model\TicketsModel;
use App\Model\TicketTeamModel;
use App\Model\TicketUserModel;
use App\Model\User;
use App\Model\UsersGameModel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;


class MyTicketTest extends TestCase
{
    // use DatabaseTransactions;

    public function testMyTicketUserResponses()
    {
        //- Truncate
        Schema::disableForeignKeyConstraints();
        User::truncate();
        IdentityGameModel::truncate();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        TicketModel::truncate();
        TicketUserModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        UsersGameModel::truncate();
        TeamPlayerModel::truncate();
        TicketTeamModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $ticket = factory(AllTicketModel::class)->create([
            'competition' => 'all'
        ]);

        factory(TicketsModel::class, 2)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => "",
            'user_id'   => $user->user_id
        ]);

        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketUserModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_users'  => $user->user_id,
            'ticket'    => "2"
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);


        //do request
        $response = $this->get('/api/account/my-ticket/info/user', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" =>  [
                    "*" => [
                        "ticket_id",
                        "name",
                        "expired",
                    ]
                ]
            ]);
    }

    public function testMyTicketTeamResponses()
    {
        //- Truncate
        Schema::disableForeignKeyConstraints();
        User::truncate();
        IdentityGameModel::truncate();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        TicketModel::truncate();
        TicketUserModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        UsersGameModel::truncate();
        TeamPlayerModel::truncate();
        TicketTeamModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $game = factory(GameModel::class)->create();

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $team = TeamModel::first();

        $ticket = factory(AllTicketModel::class)->create([
            'competition' => 'all'
        ]);

        factory(TicketsModel::class, 2)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => $team->team_id,
            'user_id'   => ''
        ]);

        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketTeamModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_team'   => $team->team_id,
            'ticket'    => "2"
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do request
        $response = $this->get('/api/account/my-ticket/info/team', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" =>  [
                    "*" => [
                        "ticket_id",
                        "name",
                        "expired",
                    ]
                ]
            ]);
    }

    public function testMyTicketExpiredResponses()
    {
        //- Truncate
        Schema::disableForeignKeyConstraints();
        User::truncate();
        IdentityGameModel::truncate();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        TicketModel::truncate();
        TicketUserModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        UsersGameModel::truncate();
        TeamPlayerModel::truncate();
        TicketTeamModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();


        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $game = factory(GameModel::class)->create();

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $team = TeamModel::first();

        $ticket = factory(AllTicketModel::class)->create([
            'competition' => 'all'
        ]);

        factory(TicketsModel::class, 2)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => $team->team_id,
            'user_id'   => ''
        ]);

        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketTeamModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_team'   => $team->team_id,
            'ticket'    => "2"
        ]);

        $ticket = factory(AllTicketModel::class)->create([
            'competition' => 'all',
            'expired'     => date('Y-m-d H:i:s', strtotime("-7 day"))
        ]);


        factory(TicketsModel::class, 2)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => "",
            'user_id'   => $user->user_id
        ]);

        $id_ticket = uniqid();

        factory(TicketModel::class)->create([
            'kompetisi' => 'all',
            'id_ticket' => $id_ticket,
            'expired'   => date('Y-m-d H:i:s', strtotime("-20 day"))
        ]);

        factory(TicketUserModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_users'  => $user->user_id,
            'ticket'    => "2"
        ]);


        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do something
        $response = $this->get('/api/account/my-ticket/info/expired', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" =>  [
                    "*" => [
                        "ticket_id",
                        "name",
                        "expired",
                    ]
                ]
            ]);
    }

    public function testMyTicketCountResponses()
    {
        //- Truncate
        Schema::disableForeignKeyConstraints();
        User::truncate();
        IdentityGameModel::truncate();
        AllTicketModel::truncate();
        TicketsModel::truncate();
        TicketModel::truncate();
        TicketUserModel::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        UsersGameModel::truncate();
        TeamPlayerModel::truncate();
        TicketTeamModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();


        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        //-Users Tiket Mock
        $id_ticket = uniqid();

        factory(TicketModel::class)->create(['kompetisi' => 'all', 'id_ticket' => $id_ticket]);

        factory(TicketUserModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_users'  => $user->user_id,
            'ticket'    => "2"
        ]);

        //- Tiket Team Mock
        $game = factory(GameModel::class)->create();

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $team = TeamModel::first();

        $ticket = factory(AllTicketModel::class)->create([
            'competition' => 'all'
        ]);

        factory(TicketsModel::class, 2)->create([
            'ticket_id' => $ticket->id,
            'is_used'   => 0,
            'team_id'   => $team->team_id,
            'user_id'   => ''
        ]);


        //- Expired Tiket Mock
        $id_ticket = uniqid();

        factory(TicketModel::class)->create([
            'kompetisi' => 'all',
            'id_ticket' => $id_ticket,
            'expired'   => date('Y-m-d H:i:s', strtotime("-20 day"))
        ]);

        factory(TicketUserModel::class)->create([
            'id_ticket' => $id_ticket,
            'id_users'  => $user->user_id,
            'ticket'    => "2"
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do request
        $response = $this->get('/api/account/my-ticket/count', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "title",
                "data" => [
                    "user",
                    "team",
                    "expired",
                ]
            ]);
    }
}
//- Test
