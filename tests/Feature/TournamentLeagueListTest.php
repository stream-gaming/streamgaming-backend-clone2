<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\GameModel;
use App\Model\TournamentLiga;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TournamentLeagueListTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetOne()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        TournamentLiga::truncate();
        Schema::enableForeignKeyConstraints();

        $myapps = new MyApps();
        $game = factory(GameModel::class)->create([
            'system_kompetisi' => 'bracket'
        ]);
        $data = factory(TournamentLiga::class)->create([
            'kompetisi' => $game->id_game_list
        ]);

        $response = $this->get('/api/tournament/liga');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "data" => [
                "*" => [
                    "title",
                    "type",
                    "total_prize",
                    "total_slot",
                    "image",
                    "date_start",
                    "date_end",
                    "url_page",
                    "organizer" => [
                        "name",
                        "logo"
                    ],
                    "game" => [
                        "id",
                        "name",
                        "logo"
                    ]
                ]
            ],
            "meta" => [
                "pagination" => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ],
            "tab" => [
                "*" => [
                    "tabname",
                    "url"
                ]
            ]
        ]);


        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        TournamentLiga::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
