<?php

namespace Tests\Feature;

use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class ListTeamChatTest extends TestCase
{
    /**
     * Get List Team Chat Not Login.
     *
     * @return void
     */
    public function testGetListTeamChatNotLogin()
    {
        $response = $this->get('/api/chat/team/list-team');

        $response->assertStatus(401);
    }

    /**
     * Get List Team ChatLogin.
     *
     * @return void
     */
    public function testGetListTeamChatLogin()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        Schema::enableForeignKeyConstraints();


        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        $response = $this->get('/api/chat/team/list-team',['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'team_id',
                    'unread'
                ]
            ],
            'user' => [
                'user_id',
                'username'
            ]
        ]);

        Schema::disableForeignKeyConstraints();
        User::truncate();
        TeamModel::truncate();
        TeamPlayerNewModel::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
