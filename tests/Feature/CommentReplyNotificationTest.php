<?php

namespace Tests\Feature;

use App\Model\AllTournamentModel;
use App\Model\CommentsModel;
use App\Model\LeaderboardModel;
use App\Model\Management;
use App\Model\User;
use App\Model\UserNotificationModel;
use App\Traits\UserHelperTrait;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class CommentReplyNotificationTest extends TestCase
{

    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testReplyCommentAsUserAndNotifyToParent()
    {
        Schema::disableForeignKeyConstraints();
        CommentsModel::truncate();
        User::truncate();
        UserNotificationModel::truncate();
        LeaderboardModel::truncate();
        AllTournamentModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = $this->createUser();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $createTournament = factory(LeaderboardModel::class)->create(['url' => 'url-comment-tes']);
        AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament' => $createTournament->id_leaderboard,
            'format' => 'Leaderboard',
            'create_at' => now()
        ]);
        $createParent = factory(CommentsModel::class)->create(['commentable_id' => $createTournament->id_leaderboard]);

        $childComment = [
            'user_status'     => 'user',
            'parent_id'       => $createParent->id,
            'reply_id'        => $createParent->id,
            'comment'         => 'Tesssssssss',
        ];

        $response = $this->post('/api/comments/list/'.$createParent->commentable_id.'/add',$childComment,
                        ['Authorization' => 'Bearer '.$token]);

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);

        $this->assertDatabaseHas('notification', ['id_user' => $createParent->user_id]);
    }

    public function testReplyCommentAsAdminAndNotifyToParent(){
        Schema::disableForeignKeyConstraints();
        CommentsModel::truncate();
        Management::truncate();
        UserNotificationModel::truncate();
        LeaderboardModel::truncate();
        AllTournamentModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = $this->createAdmin();

        $createTournament = factory(LeaderboardModel::class)->create(['url' => 'url-comment-tes']);
        AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament' => $createTournament->id_leaderboard,
            'format' => 'Leaderboard',
            'create_at' => now()
        ]);
        $createParent = factory(CommentsModel::class)->create(['commentable_id' => $createTournament->id_leaderboard]);

        $childComment = [
            'user_status'     => 'admin',
            'stream_id'       => $user->stream_id,
            'parent_id'       => $createParent->id,
            'reply_id'        => $createParent->id,
            'comment'         => 'Tesssssssss reply from admin',
        ];

        $response = $this->post('/api/comments-portal/list/'.$createParent->commentable_id.'/add',$childComment);

        $response->assertStatus(201);
        $response->assertJsonStructure(['status', 'message']);

        $this->assertDatabaseHas('notification', ['id_user' => $createParent->user_id]);

    }


}
