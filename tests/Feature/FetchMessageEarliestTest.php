<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\MessageTeamModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class FetchMessageEarliestTest extends TestCase
{
    /**
     * test Get Earliest Message Scenario Team Found.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioTeamFound()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        $date = strtotime('2022-02-23');

        $response = $this->get('/api/chat/team/fetch/'.$teamId.'/earliest?date='.$date,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * test Get Earliest Message Scenario Earliest Message Without Timestamp Parameter Not Reach Limit.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioEarliestMessageWithoutTimestampPrameter()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        factory(MessageTeamModel::class, 15)->create([
            'team_id' => $team->team_id,
            'created_at' => '2020-02-21',
            'updated_at' => '2020-02-21'
        ]);

        $response = $this->get('/api/chat/team/fetch/'.$teamId.'/earliest',['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message',
            'errors' => [
                'date'
            ]
        ]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * test Get Earliest Message Scenario Earliest Message With Timestamp Parameter Not Reach Limit.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioEarliestMessageWithTimestampPrameterNotReachLimit()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        $date = strtotime('2020-02-22');

        factory(MessageTeamModel::class, 15)->create([
            'team_id' => $team->team_id,
            'created_at' => '2020-02-21',
            'updated_at' => '2020-02-21',
        ]);

        $response = $this->get('/api/chat/team/fetch/'.$teamId.'/earliest?date='.$date,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'header' => [
                'team_id',
                'team_name',
                'logo',
                'member',
                'reachedLimit'
            ],
            'messages' => [
                'data' => [
                    '*' => [
                        'id',
                        'user' => [
                            'username',
                            'role',
                            'picture'
                        ],
                        'is_me',
                        'is_read',
                        'message',
                        'created_at',
                        'attachment'
                    ]
                ]
            ]
        ]);

        $response->assertJsonPath('header.reachedLimit', false);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * test Get Earliest Message Scenario Earliest Message With Timestamp Parameter Reach Limit.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioEarliestMessageWithTimestampPrameterReachLimit()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $team = factory(TeamModel::class)->create();

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id
        ]);

        $token = JWTAuth::fromUser($user);

        $date = strtotime('2020-02-22');

        factory(MessageTeamModel::class, 16)->create([
            'team_id' => $team->team_id,
            'created_at' => '2020-02-25',
            'updated_at' => '2020-02-25',
        ]);

        factory(MessageTeamModel::class, 14)->create([
            'team_id' => $team->team_id,
            'created_at' => '2020-02-21',
            'updated_at' => '2020-02-21',
        ]);

        $response = $this->get('/api/chat/team/fetch/'.$teamId.'/earliest?date='.$date,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'header' => [
                'team_id',
                'team_name',
                'logo',
                'member',
                'reachedLimit'
            ],
            'messages' => [
                'data' => [
                    '*' => [
                        'id',
                        'user' => [
                            'username',
                            'role',
                            'picture'
                        ],
                        'is_me',
                        'is_read',
                        'message',
                        'created_at',
                        'attachment'
                    ]
                ]
            ]
        ]);

        $response->assertJsonPath('header.reachedLimit', true);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * test Get Earliest Message Scenario Team Not Found.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioTeamNotFound()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $date = strtotime('2020-02-22');

        $user = User::first();

        $token = JWTAuth::fromUser($user);

        $response = $this->get('/api/chat/team/fetch/asdf/earliest?date='.$date,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(404);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * test Get Earliest Message Scenario Team Not Found.
     *
     * @return void
     */
    public function testGetEarliestMessageScenarioNotMember()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        $token = JWTAuth::fromUser($user);

        $team = factory(TeamModel::class)->create();

        $date = strtotime('2020-02-22');

        $teamId = (new MyApps)->onlyEncrypt($team->team_id);

        $response = $this->get('/api/chat/team/fetch/'.$teamId.'/earliest?date='.$date,['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(403);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        User::truncate();
        TeamPlayerNewModel::truncate();
        MessageTeamModel::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
