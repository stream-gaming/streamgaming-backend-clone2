<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\CityModel;
use App\Model\User;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class InfoBannedTest extends TestCase
{

    public function testAddInfoBannedOnMyGameResponses()
    {
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();

        User::insert(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => 'asdf',
                'is_banned' => 1
            ]);


        $token = JWTAuth::attempt(['email' => 'test@email.com', 'password' => 'asdfasdf']);

        //do something
        $response = $this->get('/api/account/my-game', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "game" => [
                        "*" => [
                            "is_banned",
                            "ban_until",
                        ],
                    ]
                ],
            ]);
    }


    public function testAddInfoBannedOnMyTeamResponses()
    {
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'asdfasdf']);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => '']));
        });

        $response = $this->get('/api/account/my-team', ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "team" => [
                        "*" => [
                            "is_banned",
                            "ban_until",
                        ]
                    ]
                ]
            ]);
    }

    public function testAddInfoBannedOnMyDetailTeamResponses()
    {
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'asdfasdf']);

        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => $user->user_id]));
        });

        $MyApps = new MyApps();
        $url = $MyApps->onlyEncrypt(TeamModel::first()->team_id);
        $response = $this->get('/api/account/my-team/detail/' . $url, ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "team" =>  [
                        "is_banned",
                        "ban_until",
                        "member" => [
                            "primary" => [
                                "*" => [
                                    "is_banned",
                                    "ban_until",
                                ],
                            ],
                            "substitute" => [
                                "*" => [
                                    "is_banned",
                                    "ban_until",
                                ]
                            ]
                        ],
                        "all-member" => [
                            "*" => [
                                "is_banned",
                                "ban_until",
                            ]
                        ],
                    ]
                ]
            ]);
    }

    public function testAddInfoBannedOnDetailTeamResponses()
    {
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create(['user_id' => 'asdf', 'fullname' => 'test', 'status' => 'player', 'username' => 'test', 'email' => 'test@email.com', 'password' => bcrypt('asdfasdf')]);

        factory(IdentityGameModel::class)->create([
            'users_id' => $user->user_id,
            'game_id'  => $game->id_game_list,
            'id_ingame' => '1122',
            'username_ingame' => 'user1122'
        ]);


        factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 0, 'game_id' => $game->id_game_list])->each(function ($team) use ($user) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id, 'substitute_id' => $user->user_id]));
        });

        $MyApps = new MyApps();
        $url = $MyApps->onlyEncrypt(TeamModel::first()->team_id);
        $response = $this->get('/api/team/detail/' . $url, []);

        //validate response
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "member" => [
                        "primary" => [
                            "*" => [
                                "is_banned",
                                "ban_until",
                            ],
                        ],
                        "substitute" => [
                            "*" => [
                                "is_banned",
                                "ban_until",
                            ]
                        ]
                    ],
                ]

            ]);
    }
}
