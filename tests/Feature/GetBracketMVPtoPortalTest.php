<?php

namespace Tests\Feature;

use App\Model\BracketDataMatchs;
use App\Model\ScoreMatchsModel;
use App\Model\TeamModel;
use App\Model\TournamenGroupModel;
use App\Model\TournamentGroupPlayerModel;
use App\Model\TournamentModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class GetBracketMVPtoPortalTest extends TestCase
{
    /**
     * Get data five MVP.
     *
     * @return void
     */
    public function testGetFiveMVP()
    {
        Schema::disableForeignKeyConstraints();
        TournamentModel::truncate();
        TournamenGroupModel::truncate();
        TournamentGroupPlayerModel::truncate();
        BracketDataMatchs::truncate();
        ScoreMatchsModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $tournament = factory(TournamentModel::class)->create();

        factory(TournamenGroupModel::class, 64)->create([
            'id_group' => $tournament->id_group
        ]);

        factory(TournamentGroupPlayerModel::class, 320)->create();
        factory(BracketDataMatchs::class,200)->create();
        factory(ScoreMatchsModel::class, 200)->create();

        $tournament = TournamentModel::first();
        $response = $this->get('api/get-bracket-mvp/'.$tournament->id_tournament,[]);

        $response->assertJsonStructure([
            "status",
            "data" => [
                "*" => [
                    "player_id",
                    "id_team",
                    "total_match",
                    "kills",
                    "death",
                    "assist",
                    "kda",
                    "kda_per_total_match",
                    "tournament_group" => [
                        "id_tournament_group",
                        "id_group",
                        "id_team",
                        "nama_team",
                        "logo_team",
                        "nama_kota",
                        "matchs",
                        "win",
                        "date"
                    ],
                    "tournament_group_player" => [
                        "id_player",
                        "id_group",
                        "id_team",
                        "username_ingame",
                        "id_ingame",
                        "player_status"
                ]
                ]
            ]
        ]);

        $response->assertStatus(200);
    }

    /**
     * move data five MVP success.
     *
     * @return void
     */
    public function testMoveMVPsuccess()
    {
        Schema::disableForeignKeyConstraints();
        TournamentModel::truncate();
        TournamenGroupModel::truncate();
        TournamentGroupPlayerModel::truncate();
        BracketDataMatchs::truncate();
        ScoreMatchsModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $tournament = factory(TournamentModel::class)->create();

        factory(TournamenGroupModel::class, 64)->create([
            'id_group' => $tournament->id_group
        ]);
        factory(TournamentGroupPlayerModel::class, 320)->create();
        factory(BracketDataMatchs::class,200)->create();
        factory(ScoreMatchsModel::class, 200)->create();
        $tournament = TournamentModel::first();

        $response   = $this->post('api/move-first-mvp-bracket/'.$tournament->id_tournament,[],[]);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Move data five MVP failed.
     *
     * @return void
     */
    public function testMoveMVPfailed()
    {
        Schema::disableForeignKeyConstraints();
        TournamentModel::truncate();
        TournamenGroupModel::truncate();
        TournamentGroupPlayerModel::truncate();
        BracketDataMatchs::truncate();
        ScoreMatchsModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $tournament = factory(TournamentModel::class)->create();

        factory(TournamenGroupModel::class, 64)->create([
            'id_group' => $tournament->id_group
        ]);
        factory(TournamentGroupPlayerModel::class, 320)->create();
        factory(BracketDataMatchs::class,200)->create();
        factory(ScoreMatchsModel::class, 200)->create();
        $tournament = TournamentModel::first();
        $this->post('api/move-first-mvp-bracket/'.$tournament->id_tournament,[],[]);
        $response   = $this->post('api/move-first-mvp-bracket/'.$tournament->id_tournament,[],[]);

        $response->assertStatus(409);
    }
}
