<?php

namespace Tests\Feature;

use App\Model\MobileVersion;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class MobileVersionTest extends TestCase
{
    /**
     * Get Latest Version Test.
     *
     * @return void
     */
    public function testGetLatestVersion()
    {
        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

        factory(MobileVersion::class)->create([
            'appId' => 1,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        // test android version
        $response = $this->get('/api/mobile/version');

        $response->assertStatus(200);

        $response->assertJson([
            'appId' => 1,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => 1,
            'forceUpdate' => 1,
            'changeLogs' => 'update'
        ]);

        factory(MobileVersion::class)->create([
            'appId' => 2,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        // test ios version
        $response = $this->get('/api/mobile/version/ios');

        $response->assertStatus(200);

        $response->assertJson([
            'appId' => 2,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => 1,
            'forceUpdate' => 1,
            'changeLogs' => 'update'
        ]);

        // test not found
        $response = $this->get('/api/mobile/version/ss');

        $response->assertStatus(404);

        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Get All Version for Portal Test.
     *
     * @return void
     */
    public function testGetAllVersionPortal()
    {
        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

        factory(MobileVersion::class, 3)->create([
            'appId' => 1,
        ]);

        // test android
        $response = $this->get('/api/admin/mobile/version/all');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "*" => [
                'id',
                'appId',
                'versionName',
                'versionCode',
                'recommendUpdate',
                'forceUpdate',
                'changeLogs',
                'url',
                'created_at',
                'updated_at'
            ]
        ]);

        $response->assertJsonCount(3);

        $response->assertJsonPath("0.appId","1");
        $response->assertJsonPath("1.appId","1");
        $response->assertJsonPath("2.appId","1");


        factory(MobileVersion::class, 3)->create([
            'appId' => 2,
        ]);

        // test ios
        $response = $this->get('/api/admin/mobile/version/all/ios');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "*" => [
                'id',
                'appId',
                'versionName',
                'versionCode',
                'recommendUpdate',
                'forceUpdate',
                'changeLogs',
                'url',
                'created_at',
                'updated_at'
            ]
        ]);

        $response->assertJsonCount(3);

        $response->assertJsonPath("0.appId","2");
        $response->assertJsonPath("1.appId","2");
        $response->assertJsonPath("2.appId","2");

        // test not found
        $response = $this->get('/api/admin/mobile/version/all/ioss');

        $response->assertStatus(404);


        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Get Version for Portal Test.
     *
     * @return void
     */
    public function testGetVersionPortal()
    {
        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

        $version = factory(MobileVersion::class)->create([
            'appId' => 1,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        $version = json_decode(json_encode($version));

        // get android
        $response = $this->get('/api/admin/mobile/version/edit/'.$version->id);

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $version->id,
            'appId' => "$version->appId",
            'versionName' => $version->versionName,
            'versionCode' => "$version->versionCode",
            'recommendUpdate' => "$version->recommendUpdate",
            'forceUpdate' => "$version->forceUpdate",
            'changeLogs' => $version->changeLogs,
            'url' => $version->url,
            'created_at' => $version->created_at,
            'updated_at' => $version->updated_at
        ]);

        $version = factory(MobileVersion::class)->create([
            'appId' => 2,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        $version = json_decode(json_encode($version));

        // get ios
        $response = $this->get('/api/admin/mobile/version/edit/'.$version->id);

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $version->id,
            'appId' => "$version->appId",
            'versionName' => $version->versionName,
            'versionCode' => "$version->versionCode",
            'recommendUpdate' => "$version->recommendUpdate",
            'forceUpdate' => "$version->forceUpdate",
            'changeLogs' => $version->changeLogs,
            'url' => $version->url,
            'created_at' => $version->created_at,
            'updated_at' => $version->updated_at
        ]);

        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Create Mobile Version for Portal Test.
     *
     * @return void
     */
    public function testCreateVersionPortal()
    {
        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

        // create android version success
        $response = $this->post('/api/admin/mobile/version/', [
            'name' => '1.1.1',
            'force' => true,
            'recommend' => true,
            'code' => 1,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('mobile_versions',[
            'appId' => 1,
            'versionName' => "1.1.1",
            'versionCode' => 1,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => "update",
        ]);

        // create android version failed
        $response = $this->post('/api/admin/mobile/version/', [
            'name' => '1.1.1',
            'force' => true,
            'recommend' => true,
            'code' => 1,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(422);

        // create ios version success
        $response = $this->post('/api/admin/mobile/version/ios', [
            'name' => '1.1.1',
            'force' => true,
            'recommend' => true,
            'code' => 1,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('mobile_versions',[
            'appId' => 2,
            'versionName' => "1.1.1",
            'versionCode' => 1,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => "update",
        ]);

        // create ios version failed
        $response = $this->post('/api/admin/mobile/version/ios', [
            'name' => '1.1.1',
            'force' => true,
            'recommend' => true,
            'code' => 1,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(422);

        // create not found
        $response = $this->post('/api/admin/mobile/version/iosss', [
            'name' => '1.1.1',
            'force' => true,
            'recommend' => true,
            'code' => 1,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(404);

        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Update Mobile Version for Portal Test.
     *
     * @return void
     */
    public function testUpdateVersionPortal()
    {
        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();

        $version = factory(MobileVersion::class)->create([
            'appId' => 1,
            'versionName' => '1.2.1',
            'versionCode' => 5,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        // update success
        $response = $this->post('/api/admin/mobile/version/update/'.$version->id, [
            'name' => '1.2.2',
            'force' => true,
            'recommend' => true,
            'code' => 6,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(200);

        factory(MobileVersion::class)->create([
            'appId' => 1,
            'versionName' => '1.2.4',
            'versionCode' => 7,
            'recommendUpdate' => true,
            'forceUpdate' => true,
            'changeLogs' => 'update'
        ]);

        // update success
        $response = $this->post('/api/admin/mobile/version/update/'.$version->id, [
            'name' => '1.2.4',
            'force' => true,
            'recommend' => true,
            'code' => 7,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(422);

        // update not found
        $response = $this->post('/api/admin/mobile/version/update/6', [
            'name' => '1.2.4',
            'force' => true,
            'recommend' => true,
            'code' => 7,
            'changeLogs' => 'update',
        ]);

        $response->assertStatus(404);

        Schema::disableForeignKeyConstraints();
        MobileVersion::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
