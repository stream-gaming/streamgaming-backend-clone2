<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\IdentityIngameHistoryModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class HistoryIdentityGameTest extends TestCase
{
    /**
     * Identity Game History test with scenario failed.
     *
     * @return void
     */
    public function testGetIdentityHistorySuccess()
    {
        Schema::disableForeignKeyConstraints();
        IdentityGameModel::truncate();
        GameModel::truncate();
        User::truncate();
        IdentityIngameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(IdentityGameModel::class)->create();

        factory(IdentityIngameHistoryModel::class,5)->create([
            'user_id' => $user->users_id,
            'game_id' => $user->game_id
        ]);

        $response = $this->get('/api/admin/user/game-idenity/'.$user->users_id);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'status',
            'data'=> [
                '*' => [
                    'users_id',
                    'game_id',
                    'game' => [
                        'id_game_list',
                        'name'
                    ],
                    'history' => [
                        '*' => [
                            'user_id',
                            'game_id',
                            'before',
                            'after',
                            'created_at',
                            'updated_at'
                        ]
                    ]
                ]
            ],
            'message'
        ]);
    }

    /**
     * Identity Game History test with scenario failed.
     *
     * @return void
     */
    public function testGetIdentityHistoryFailed()
    {
        $response = $this->get('/api/admin/user/game-idenity/test');

        $response->assertStatus(404);

        $response->assertJson([
            'status' => false,
            'data'   => [],
            'message'=> Lang::get('message.user.not')
        ]);
    }
}
