<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\AllTournamentModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardTeamModel;
use App\Model\LeaderboardTeamPlayerModel;
use App\Model\PlayerPayment;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class MyTournamentCacheTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMyTournamentCache()
    {
        //Create user
        $user = factory(User::class)->create(['password' => bcrypt('Admin12345!')]);
        //Generate user access token
        $token = JWTAuth::attempt(['email' => $user['email'], 'password' => 'Admin12345!']);
        //Create Tour
        $createTour = factory(LeaderboardModel::class)->create();
        AllTournamentModel::create([
            'id_all_tournament' => uniqid(),
            'id_tournament' => $createTour->id_leaderboard,
            'format' => 'Leaderboard',
            'create_at' => now()
        ]);
        factory(PlayerPayment::class)->create(
            [
                'id_tournament' => $createTour->id_leaderboard,
                'id_team' => $user->user_id,
                'player_id' => $user->user_id
            ]
        );

        //Get my tournament
        $getMyTour = $this->post('/api/user/my-tournament', [], ['Authorization' => 'Bearer ' . $token]);
        $getMyTour->assertStatus(200);
        $getMyTour->assertJsonStructure(['status','data']);

        //Get cache
        $getDataCahe = RedisHelper::getData('mytournament.'.$user['user_id']);
        $this->assertNotFalse($getDataCahe);

        //Clear data after test
        RedisHelper::destroyData('mytournament.'.$user['user_id']);
        GameModel::where('id_game_list', $createTour['game_competition'])->delete();
        LeaderboardModel::where('id_leaderboard', $createTour['id_leaderboard'])->delete();
        PlayerPayment::where('id_team', $user['user_id'])
                        ->where('id_tournament', $createTour['id_leaderboard'])->delete();
        User::where('user_id', $user['user_id'])->delete();

    }
}
