<?php

namespace Tests\Feature;

use App\Model\PromotionModel;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPromotionGetTest extends TestCase
{

    public function testGetAllUserPromotionWithNoDraftAsAnAuthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $promotion = $this->get(Route('promotion.get.all'),['Authorization' => 'Bearer ' . $token]);
        $promotion->assertStatus(200);
        $promotion->assertJsonFragment([
            "promotion_id" => $promotions[0]->id,
            "title" => $promotions[0]->title,
            "is_read" => false,
            "base_url" => $promotions[0]->link,
            "created_at" => date('Y-m-d H:i:s', strtotime($promotions[0]->created_at)),
        ]);
    }

    public function testGetAllUserPromotionWithDraftAsAnAuthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //create promotions
        factory(PromotionModel::class, 4)->create(['status' => 1]);
        //create promotion draft
        $draftPromotion = factory(PromotionModel::class)->create(['status' => 0]);

        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $promotion = $this->get(Route('promotion.get.all'),['Authorization' => 'Bearer ' . $token]);
        $promotion->assertStatus(200);
        $promotion->assertJsonMissing([
            "promotion_id" => $draftPromotion->id,
            "title" => $draftPromotion->title,
            "is_read" => false,
            "base_url" => $draftPromotion->link,
            "created_at" => date('Y-m-d H:i:s', strtotime($draftPromotion->created_at)),
        ], true);
    }

    public function testGetAllUserPromotionAsAnUnauthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //create promotions
        factory(PromotionModel::class, 4)->create(['status' => 1]);
        //create promotion draft
        factory(PromotionModel::class)->create(['status' => 0]);


        $promotion = $this->get(Route('promotion.get.all'),['Authorization' => 'Bearer ' . 'unauthorizedtoken kwekwje']);
        $promotion->assertStatus(401);
        $promotion->assertJson([
            'status' => false
        ]);

    }

    public function testGetDetailPromotionAsAnAuthorizedUser(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //create promotions
        $promotions = factory(PromotionModel::class, 4)->create(['status' => 1]);
        //create promotion draft
        factory(PromotionModel::class)->create(['status' => 0]);

        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        $promotion = $this->get(Route('promotion.get.detail', ['promotionId' => $promotions[2]->id]),['Authorization' => 'Bearer ' . $token]);
        $promotion->assertStatus(200);
        $promotion->assertJsonStructure([
            'promotion_id',
            'title',
            'content'
        ]);
        $promotion->assertJson([
            'promotion_id' => $promotions[2]->id,
            'title' => $promotions[2]->title,
            'content' => $promotions[2]->content
        ]);

    }

    public function testGetDetailPromotionAsUnauthorizedUser(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //create promotions
        $promotions = factory(PromotionModel::class, 4)->create(['status' => 1]);
        //create promotion draft
        factory(PromotionModel::class)->create(['status' => 0]);

        $promotion = $this->get(Route('promotion.get.detail', ['promotionId' => $promotions[2]->id]),['Authorization' => 'Bearer ' .'unauthorizeTokenasa']);
        $promotion->assertStatus(401);
        $promotion->assertJson([
            'status' => false
        ]);

    }
}
