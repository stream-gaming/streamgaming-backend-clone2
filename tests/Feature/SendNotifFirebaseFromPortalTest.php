<?php

namespace Tests\Feature;

use App\Model\UserNotificationModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SendNotifFirebaseFromPortalTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSendNotifFirebaseFromPortalSuccess()
    {
        factory(UserNotificationModel::class)->create(['source' => 1, 'notified_on_firebase' => 0, 'deleted_at' => null]);
        $response = $this->get('/api/notification/send-notification');

        $this->assertTrue($response['status']);
    }
}
