<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\GameRoleModel;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class GameDetailOnboardingTest extends TestCase
{

    public function testGetGameDetailOnboardingWithValidGameIdAndNotEmptyRole()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        GameRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'url' => uniqid()]);
        $role = factory(GameRoleModel::class)->create(['game_id' => $game->id_game_list]);
        $response = $this->get(Route('game.detail.onboarding', ['game_url' => $game->url]));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'data' => [
                'game' => [
                    'id_game',
                    'name',
                    'logo',
                    'tutorial'
                ],
                'role' => [
                    [
                        'role_id',
                        'role_name',
                        'game_id'
                    ]
                ]
            ]
        ]);

    }

    public function testGetGameDetailOnboardingWithValidGameIdAndEmptyRole()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        GameRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'url' => uniqid()]);
        $response = $this->get(Route('game.detail.onboarding', ['game_url' => $game->url]));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'data' => [
                'game' => [
                    'id_game',
                    'name',
                    'logo',
                    'tutorial'
                ],
                'role' => []
            ]
        ]);

    }

    public function testGetGameDetailOnboardingWithInvalidGameId()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $game = factory(GameModel::class)->create(['id_game_list' => uniqid(), 'url' => uniqid()]);
        $response = $this->get(Route('game.detail.onboarding', ['game_url' => 'invalidId']));

        $response->assertStatus(404);
        $response->assertJsonStructure([
            'status',
            'message'
        ]);
        $response->assertJson([
            'status' => false,
            'message' => Lang::get('game.not_found')
        ]);
    }
}
