<?php

namespace Tests\Feature;

use App\Model\OrganizerModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker\Generator as Faker;

class OrganizerTest extends TestCase
{
    use WithFaker;

    /**
     * Test get Data.
     *
     * @return void
     */
    public function testInsertSuccess()
    {
        $data = [
            // 'id'                => uniqid(),    
            'first_name'        => $this->faker()->firstName,
            'last_name'         => $this->faker()->lastName,
            'email'             => $this->faker()->email,
            'company'           => $this->faker()->company,
            'industry'          => $this->faker()->streetName,
            'position'          => $this->faker()->cityPrefix,
            'city'              => $this->faker()->city,
            'phone_number'      => "08131231367213",
            'manager_tour'      => uniqid(),
            'status'            => "pending"
        ];
        $response = $this->post('api/send-organizer', $data);
  
        $response->assertStatus(201);
    }
    public function testDataEmpty(){
        $data = [
        ];
        $response = $this->post('api/send-organizer', $data);

        $response->assertStatus(422);
    }
    public function testBruteforceInsert(){
        $data = [
            // 'id'                => uniqid(),    
            'first_name'        => $this->faker()->firstName,
            'last_name'         => $this->faker()->lastName,
            'email'             => $this->faker()->email,
            'company'           => $this->faker()->company,
            'industry'          => $this->faker()->streetName,
            'position'          => $this->faker()->cityPrefix,
            'city'              => $this->faker()->city,
            'phone_number'      => "08131231367213",
            'manager_tour'      => uniqid(),
            'status'            => "pending"
        ];
        for ($i=0; $i < 6 ; $i++) { 
            $response = $this->post('api/send-organizer', $data);
        }
        $response->assertStatus(429);
    }
}
