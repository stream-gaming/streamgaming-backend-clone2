<?php

namespace Tests\Feature;

use App\Helpers\Firebase\FirebaseFactoryInterface;
use App\Model\PromotionModel;
use App\Model\PromotionPushNotifHistoryModel;
use App\Model\User;
use App\Model\UserPromotionModel;
use App\Modules\FCM\Interfaces\FCMNotifTokenRepositoryInterface;
use Illuminate\Support\Facades\Schema;
use Mockery\MockInterface;
use Tests\TestCase;

class PromotionPushNotifTest extends TestCase
{

    public function testPushPromotionNotif()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        PromotionPushNotifHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateDataV2')
            ->andReturn('true');
        });
        $this->mock(FCMNotifTokenRepositoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('chunkWithPushNotif')
            ->andReturn('true');
        });

        //create promotion
        $promotion = factory(PromotionModel::class)->create(['status' => 1]);

        //Make sure this promotion push notification not set yet
        $this->assertDatabaseMissing('promotion_push_notif_history', [
            'promotion_id' => $promotion->id,
            'attempt' => 1
        ]);

        $promotionCall = $this->call('PUT', Route('promotion.push.notif', ['promotion' => $promotion->id]), [], [], [], ['PHP_AUTH_USER' => 'streamgamingportal', 'PHP_AUTH_PW' => '424F97DAADCD5F864BB29BAD434811ADB6E1E085']);
        $promotionCall->assertStatus(200);
        $promotionCall->assertJson([
            'status' => true
        ]);

        //Make sure this promotion push notification has set successfully
        $this->assertDatabaseHas('promotion_push_notif_history', [
            'promotion_id' => $promotion->id,
            'attempt' => 1
        ]);

    }

}
