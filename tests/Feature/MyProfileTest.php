<?php

namespace Tests\Feature;

use App\Jobs\ProcessFirebaseCounter;
use App\Model\User;
use App\Model\UserNotificationModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class MyProfileTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAddCityAndProvinceOnMyProfileResponses()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::insert([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);


        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        //do something
        $response = $this->get('/api/account/profile', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "data" => [
                    "nama_provinsi",
                    "nama_kota",
                ]
            ]);
    }
}
