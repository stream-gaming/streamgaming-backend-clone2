<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class GetListTeamUserTest extends TestCase
{
    use WithFaker;

    /**
     * Test get Data.
     *
     * @return void
     */
    public function testGetDataSuccess()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        GameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(User::class)->create();

        $user = User::first();

        factory(GameModel::class,5)->create();

        $gameId = GameModel::pluck('id_game_list')->toArray();

        foreach ($gameId as $val) {
            factory(TeamModel::class)->create([
                'game_id'   => $val,
                'leader_id' => $this->faker()->randomElement([$user->user_id, uniqid()]),
                'venue'     => 'Medan'
            ]);
        }

        $team = TeamModel::pluck('team_id')->toArray();

        foreach ($team as $val) {
            factory(TeamPlayerModel::class)->create([
                'team_id' => $val,
                'player_id' => $user->user_id
            ]);
        }

        $response = $this->get('/api/user/teams/'.$user->username);

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'game',
                    'team_name',
                    'description',
                    'is_banned',
                    'ban_until',
                    'is_leader',
                    'logo',
                    'game'          => [
                        'id_game',
                        'name',
                    ],
                    'slot'          => [
                        'total',
                        'primary',
                        'substitute',
                        'member'
                    ],
                    'url',
                    'created_at'
                ]
            ]
        ]);

        $response->assertStatus(200);
    }
}
