<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GameListCacheTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGameListCache()
    {
        RedisHelper::destroyData('game--all');
        $data = factory(GameModel::class)->create(['url' => 'tes-game']);

        //Set cache
        $response = $this->get('/api/game/all');
        $response->assertStatus(200);
        $response->assertJsonStructure(['data']);

        //Get cache
        $getDataCahe = RedisHelper::getData('game--all');
        $this->assertNotFalse($getDataCahe);

        //Clear data and cache
        GameModel::where('id_game_list', $data->id_game_list)->delete();
        RedisHelper::destroyData('game--all');
    }
}
