<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use Tests\TestCase;

class LeaderboardDynamicPrizeTest extends TestCase
{
    public function testLeaderboardDynamicPrizeWithFourSequence(){
        $data = factory(LeaderboardModel::class)->create([
            'url' => 'tes-prizes-leaderboard-v2',
            'prize_type' => 'dynamic',
            'deleted_at' => null
        ]);

        //Set dynamic prize
        for($i=1; $i<=4; $i++){
            LeaderboardPrizePoolModel::create([
                'id_leaderboard' => $data['id_leaderboard'],
                'sequence' => $i,
                'prize' => 12000/$i
            ]);
        }
        //Call API
        $response = $this->get('/api/leaderboard/tes-prizes-leaderboard-v2/prizes');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);
        //Check if there is first, second, third and fourth sequence
        $response->assertJsonFragment([
            "prizes" => [
                    [
                            "sequence" => "1",
                            "title" => "Juara 1",
                            "description" => "",
                            "data" => [
                                [
                                    "title" => "Stream Cash",
                                    "level" => "Hadiah Utama",
                                    "category" => "stream_cash",
                                    "image" => null,
                                    "unit" => "1",
                                    "prize" => "12000"
                                ]
                            ]
                    ],
                    [
                            "sequence" => "2",
                            "title" => "Juara 2",
                            "description" => "",
                            "data" => [
                                [
                                    "title" => "Stream Cash",
                                    "level" => "Hadiah Utama",
                                    "category" => "stream_cash",
                                    "image" => null,
                                    "unit" => "1",
                                    "prize" => "6000"
                                ]
                            ]
                    ],
                    [
                        "sequence" => "3",
                        "title" => "Juara 3",
                        "description" => "",
                        "data" => [
                            [
                                "title" => "Stream Cash",
                                "level" => "Hadiah Utama",
                                "category" => "stream_cash",
                                "image" => null,
                                "unit" => "1",
                                "prize" => "4000"
                            ]
                        ]
                    ],
                    [
                        "sequence" => "4",
                        "title" => "Juara 4",
                        "description" => "",
                        "data" => [
                            [
                                "title" => "Stream Cash",
                                "level" => "Hadiah Utama",
                                "category" => "stream_cash",
                                "image" => null,
                                "unit" => "1",
                                "prize" => "3000"
                            ]
                        ]
                    ]
            ]
        ]);

        //Clear data and cache
        LeaderboardModel::where('url','tes-prizes-leaderboard-v2')->forceDelete();
        GameModel::where('id_game_list', $data['game_competition'])->forceDelete();
        LeaderboardPrizePoolModel::where('id_leaderboard', $data['id_leaderboard'])->forceDelete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }
}
