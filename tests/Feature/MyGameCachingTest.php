<?php

namespace Tests\Feature;

use App\Helpers\Validation\CheckBanned;
use App\Model\CityModel;
use App\Model\User;
use App\Model\GameModel;
use App\Model\GameRoleModel;
use App\Model\GameUsersRoleModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use App\Model\Management;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;
use function PHPUnit\Framework\assertTrue;


class MyGameCachingTest extends TestCase
{

    public function testCachingMyGame()
    {
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::enableForeignKeyConstraints();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $redis_key = 'my:game:user_id:' . $user->user_id;

        Redis::del($redis_key);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);


        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'stream']);


        //do something
        $response = $this->get('/api/account/my-game', ['Authorization' => 'Bearer ' . $token]);

        //validate result
        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "game" => [
                        "*" => [
                            "game"
                        ],
                    ]
                ],
            ]);

        //cek Redis
        assertTrue(Redis::exists($redis_key) == 1);
    }

    public function testTriggerDeleteMyGameWhenAddGame()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::disableForeignKeyConstraints();

        $game = factory(GameModel::class)->create();

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        //- Mock redis
        $redis_key = 'my:game:user_id:' . $user->user_id;

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'stream']);

        $game_role = factory(GameRoleModel::class, 2)->create(['game_id' => $game->id_game_list]);

        $response = $this->post('/api/game/add/' . $game->id_game_list, [
            'id_ingame' => '123456789',
            'username_ingame' => 'HELLOGUYS',
            'game_role_id' => [$game_role[0]->id, $game_role[1]->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(201);

        assertTrue(Redis::exists($redis_key) != 1);
    }

    public function testTriggerDeleteMyGameWhenEditGame()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        IdentityGameModel::truncate();
        GameUsersRoleModel::truncate();
        Schema::disableForeignKeyConstraints();


        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        $game = GameModel::first();

        //- Mock redis
        $redis_key = 'my:game:user_id:' . $user->user_id;

        Redis::set($redis_key, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_key) == 1);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'stream']);

        $game_role = factory(GameRoleModel::class)->create(['game_id' => $game->id_game_list]);

        $response = $this->put('/api/game/edit/' . $game->id_game_list, [
            'id_ingame' => '12345679',
            'username_ingame' => 'HELLOGUYS1',
            'game_role_id' => [$game_role->id],
        ], ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_key) != 1);
    }

    public function testTriggerDeleteMyGameWhenUsersBannedHaveTeam()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        GameUsersRoleModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Management::truncate();
        Schema::disableForeignKeyConstraints();


        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        $game = GameModel::first();

        $user_id = User::pluck('user_id')->toArray();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game->id_game_list]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        //- Mock redis
        $redis_game = 'my:game:user_id:' . $user->user_id;
        $redis_team = 'team:detail:team_id:' . $team_player->team_id;

        Redis::set($redis_game, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_team, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_game) == 1);
        assertTrue(Redis::exists($redis_team) == 1);

        $admin = factory(Management::class)->create();

        $response = $this->post('/api/admin/ban/game/', [
            'banned_until' => Carbon::now()->tomorrow()->format('Y-m-d'),
            'reason' => 'Test Uyyy',
            'identity_id' => $identity->id
        ], ['Authorization' => 'Admin ' . $admin->stream_id]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_game) != 1);
        assertTrue(Redis::exists($redis_team) != 1);
    }

    public function testTriggerDeleteMyGameWhenUsersBannedDontHaveTeam()
    {
        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        GameRoleModel::truncate();
        UsersGameModel::truncate();
        GameUsersRoleModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        Management::truncate();
        Schema::enableForeignKeyConstraints();


        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $identity = factory(IdentityGameModel::class)
            ->create([
                'users_id' => $user->user_id,
                'is_banned' => 0
            ]);

        factory(UsersGameModel::class)
            ->create([
                'users_id'  => $user->user_id,
                'game_id'   => GameModel::first()->id_game_list
            ]);

        $game = GameModel::first();

        $team_player = factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game->id_game_list]),
            'substitute_id' => ''
        ]);

        //- Mock redis
        $redis_game = 'my:game:user_id:' . $user->user_id;
        $redis_team = 'team:detail:team_id:' . $team_player->team_id;

        Redis::set($redis_game, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        Redis::set($redis_team, json_encode([
            'status' => true,
            'data'   => '*'
        ]), 'EX', 24 * 3600);

        assertTrue(Redis::exists($redis_game) == 1);
        assertTrue(Redis::exists($redis_team) == 1);

        $admin = factory(Management::class)->create();

        $response = $this->post('/api/admin/ban/game/', [
            'banned_until' => Carbon::now()->tomorrow()->format('Y-m-d'),
            'reason' => 'Test Uyyy',
            'identity_id' => $identity->id
        ], ['Authorization' => 'Admin ' . $admin->stream_id]);

        $response->assertStatus(200);

        assertTrue(Redis::exists($redis_game) != 1);
        assertTrue(Redis::exists($redis_team) == 1);
    }
}
