<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\User;
use App\Traits\UserHelperTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class BuyTicketTest extends TestCase
{
    use UserHelperTrait;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBuySoloTicketFailed()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        AllTicketModel::truncate();
        Schema::enableForeignKeyConstraints();
        //buat user utama
        $user = $this->createUser();

        // generate token user utama
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //make ticket
        $ticket = factory(AllTicketModel::class)->create(['total_given' => 0, 'mode' => 1])->id;
        $response = $this->post('/api/ticket/buy-ticket'
                        ,[
                            'ticket_id' => $ticket,
                            'payment_method' => 'cash',
                            'pin' => '123456'
                        ],
                        ['Authorization' => 'Bearer '.$token]
                    );

        $response->assertStatus(422);

    }
}
