<?php

namespace Tests\Feature;

use App\Helpers\Redis\RedisHelper;
use App\Model\GameModel;
use App\Model\LeaderboardModel;
use App\Model\LeaderboardPrizePoolModel;
use Tests\TestCase;

class LeaderboardDefaultPrizeTest extends TestCase
{

    public function testLeaderboardPrizeWithOnlyOneSequence()
    {
        $data = factory(LeaderboardModel::class)->create([
            'url' => 'tes-prizes-leaderboard-v2',
            'firstprize' => 2000,
            'secondprize' => 0,
            'thirdprize' => 0,
            'deleted_at' => null
        ]);

        //Call API
        $response = $this->get('/api/leaderboard/tes-prizes-leaderboard-v2/prizes');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);
        //Check if there is first sequence
        $response->assertJsonFragment([
            "prizes" => [
                [
                    "sequence" => "1",
                    "title" => "Juara 1",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "image" => null,
                            "unit" => "1",
                            "prize" => "2000"
                        ]
                    ]
                ]
            ]
        ]);
        //Check if there is no second sequence
        $response->assertJsonMissing([
            "prizes" => [
                [
                    "sequence" => "2",
                    "title" => "Juara 2",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "image" => null,
                            "unit" => "1",
                            "prize" => "0"
                        ]
                    ]
                ]
            ]
        ]);
        //Check if there is no third sequence
        $response->assertJsonMissing([
            "prizes" => [
                [
                    "sequence" => "3",
                    "title" => "Juara 3",
                    "description" => "",
                    "data" => [
                        [
                            "title" => "Stream Cash",
                            "level" => "Hadiah Utama",
                            "category" => "stream_cash",
                            "image" => null,
                            "unit" => "1",
                            "prize" => "0"
                        ]
                    ]
                ]
            ]
        ]);

        //Clear data and cache
        LeaderboardModel::where('url','tes-prizes-leaderboard-v2')->forceDelete();
        GameModel::where('id_game_list', $data['game_competition'])->forceDelete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }

    public function testLeaderboardPrizeWithAllSequence()
    {
        $data = factory(LeaderboardModel::class)->create([
            'url' => 'tes-prizes-leaderboard-v2',
            'firstprize' => 5000,
            'secondprize' => 3000,
            'thirdprize' => 2000,
            'deleted_at' => null
        ]);

        //Call API
        $response = $this->get('/api/leaderboard/tes-prizes-leaderboard-v2/prizes');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status','id_leaderboard']);
        //Check if there is first, second and third sequence
        $response->assertJsonFragment([
            "prizes" => [
                    [
                            "sequence" => "1",
                            "title" => "Juara 1",
                            "description" => "",
                            "data" => [
                                [
                                    "title" => "Stream Cash",
                                    "level" => "Hadiah Utama",
                                    "category" => "stream_cash",
                                    "image" => null,
                                    "unit" => "1",
                                    "prize" => "5000"
                                ]
                            ]
                    ],
                    [
                            "sequence" => "2",
                            "title" => "Juara 2",
                            "description" => "",
                            "data" => [
                                [
                                    "title" => "Stream Cash",
                                    "level" => "Hadiah Utama",
                                    "category" => "stream_cash",
                                    "image" => null,
                                    "unit" => "1",
                                    "prize" => "3000"
                                ]
                            ]
                    ],
                    [
                        "sequence" => "3",
                        "title" => "Juara 3",
                        "description" => "",
                        "data" => [
                            [
                                "title" => "Stream Cash",
                                "level" => "Hadiah Utama",
                                "category" => "stream_cash",
                                "image" => null,
                                "unit" => "1",
                                "prize" => "2000"
                            ]
                        ]
                    ]
            ]
        ]);

        //Clear data and cache
        LeaderboardModel::where('url','tes-prizes-leaderboard-v2')->forceDelete();
        GameModel::where('id_game_list', $data['game_competition'])->forceDelete();
        RedisHelper::destroyWithPattern('leaderboard.'.$data['id_leaderboard']);
    }


}
