<?php

namespace Tests\Feature;

use App\Helpers\MyApps;
use App\Model\CityModel;
use App\Model\User;
use App\Model\GameModel;
use App\Model\UsersGameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class SearchByTeamIdTest extends TestCase
{
    public function testSearchTeamByTeamId()
    {
        CityModel::truncate();
        ProvinceModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        GameModel::truncate();

        factory(GameModel::class)->create();
        $game_id = GameModel::first()->id_game_list;
        factory(IdentityGameModel::class, 5)->create([
            'game_id' => $game_id
        ]);

        factory(TeamModel::class, 5)->create();

        $user_id = User::pluck('user_id')->toArray();

        factory(TeamPlayerModel::class)->create([
            'team_id' => factory(TeamModel::class)->create(['game_id' => $game_id]),
            'player_id' => implode(',', $user_id),
            'substitute_id' => ''
        ]);

        $team = TeamModel::first();

        $response = $this->get('api/team?search=' . $team->team_id);

        $response->assertJsonStructure([
            "data" => [
                "*" =>  [
                    "team_id",
                    "game",
                    "game_id",
                    "leader",
                    "team_name",
                    "venue",
                    "logo",
                    "slot" =>  [
                        "total",
                        "member",
                    ],
                    "full_url",
                    "url",
                    "created_at",
                ]
            ],
            "is_login",
            "meta" =>  [
                "pagination" =>  [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" =>  [
                        "default",
                    ]
                ]
            ]
        ]);

        $response->assertJsonPath('meta.pagination.count', 1);

        $response->assertStatus(200);
    }
}
