<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPreferenceOnboardingTest extends TestCase
{

    public function testUpdateOnboardingProgressWhenUpdatingUserPreferenceAsSoloWithValidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'solo'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Make sure user preference has updated successfully
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::SOLO_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);
    }

    public function testUpdateOnboardingProgressWhenUpdatingUserPreferenceAsTeamWithValidCredential()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Make sure user preference has updated successfully
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

    }

    public function testUpdateOnboardingProgressWhenUpdatingUserPreferenceWithInvalidCredential(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference with invalid credential
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'dfj321487'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(422);
        $updatePreference->assertJsonStructure([
            'message',
            'errors'
        ]);

        //Make sure user preference not updated
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => OnboardingEnum::ADD_GAME_STEP,
            'status' => 0
        ]);
    }

    public function testUpdateUserPreferenceWhenUserHasNotPassedAddGameStep(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);


        //Try updating preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'solo'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(500);
        $updatePreference->assertJson([
            'status' => false,
            'message' => Lang::get('onboarding.failed.preference')
        ]);

        //Make sure user preference not updated
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => 0,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => 0
        ]);
    }

    public function testUpdateUserPreferenceAsUnauthorizedUser(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Update preference as unauthorized user
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'solo'
        ], []);


        $updatePreference->assertStatus(401);
        $updatePreference->assertJson([
            'status' => false
        ]);

    }
}
