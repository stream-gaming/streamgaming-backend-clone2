<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\User;
use App\Model\UserOnboardingModel;
use App\Modules\Onboarding\Enum\OnboardingEnum;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserActionOnboardingTest extends TestCase
{

    public function testFinishOnboardingWhenTryingToFinishFindTeamStep()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $finishOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'finish'
        ], ['Authorization' => 'Bearer ' . $token]);

        $finishOnboarding->assertStatus(200);
        $finishOnboarding->assertJson([
            'status' => true,
            'onboarding' => true,
            'progress' => 5
        ]);

        //Check user onboarding after finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::CREATE_OR_FIND_TEAM_STEP,
            'status' => OnboardingEnum::FINISHED_STATUS
        ]);
    }

    public function testFinishOnboardingBeforeChoosingPreference(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);


        //Check user onboarding before finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $finishOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'finish'
        ], ['Authorization' => 'Bearer ' . $token]);

        $finishOnboarding->assertStatus(500);
        $finishOnboarding->assertJson([
            'status' => false,
            'message' => 'Internal server problem'
        ]);

        //Check user onboarding after finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }

    public function testFinishOnboardingWhenTryingToFinishFindTeamStepWithInvalidRequest(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $finishOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'fibihygf'
        ], ['Authorization' => 'Bearer ' . $token]);

        $finishOnboarding->assertStatus(422);

        //Check user onboarding after trying to finish
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }

    public function testSkipOnboardingOnStartingStep(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        //Check user onboarding before skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $skipOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'skip'
        ], ['Authorization' => 'Bearer ' . $token]);

        $skipOnboarding->assertStatus(200);
        $skipOnboarding->assertJson([
            'status' => true,
            'onboarding' => true,
            'progress' => 5
        ]);

        //Check user onboarding after skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::SKIPPED_STATUS
        ]);
    }

    public function testSkipOnboardingOnAddingGameStep(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Check user onboarding before skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::ADD_GAME_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $skipOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'skip'
        ], ['Authorization' => 'Bearer ' . $token]);

        $skipOnboarding->assertStatus(200);
        $skipOnboarding->assertJson([
            'status' => true,
            'onboarding' => true,
            'progress' => 5
        ]);

        //Check user onboarding after skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::ADD_GAME_STEP,
            'status' => OnboardingEnum::SKIPPED_STATUS
        ]);
    }

    public function testSkipOnboardingOnUserPreferenceStep(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $game = factory(GameModel::class)->create();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        $addGame = $this->post(Route('add.game', ['id' => $game->id_game_list]), [
            'id_ingame' =>  'idtes123123',
            'username_ingame' => 'usernametes12345'
        ],['Authorization' => 'Bearer ' . $token]);

        $addGame->assertStatus(201);
        $addGame->assertJson([
            'status'    => true,
            'message'   => Lang::get('message.game.add.success')
        ]);


        //Update preference
        $updatePreference = $this->put(Route('preference.update'), [
            'preference' => 'team'
        ], ['Authorization' => 'Bearer ' . $token]);

        $updatePreference->assertStatus(200);
        $updatePreference->assertJsonStructure([
            'status',
            'progress',
            'preference' => [
                'type',
                'game' => [
                    'id',
                    'name'
                ]
            ]
        ]);

        //Check user onboarding before finishing
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $skipOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'skip'
        ], ['Authorization' => 'Bearer ' . $token]);

        $skipOnboarding->assertStatus(200);
        $skipOnboarding->assertJson([
            'status' => true,
            'onboarding' => true,
            'progress' => 5
        ]);

        //Check user onboarding after skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::TEAM_PREFERENCE,
            'step' => OnboardingEnum::ROLE_PREFERENCE_STEP,
            'status' => OnboardingEnum::SKIPPED_STATUS
        ]);
    }

    public function testSkipOnboardingOnStartingStepWithInvalidRequest(){
        Schema::disableForeignKeyConstraints();
        User::truncate();
        UserOnboardingModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        Schema::enableForeignKeyConstraints();

        $register = $this->post(Route('regist.manual'), [
           'fullname' => 'fahmi fachrozi',
           'username' => 'fahmifachrozi',
           'email' => 'fahmi_onboarding@gmail.com',
           'password' => 'tes123456',
           'password_confirmation' => 'tes123456',
           'jenis_kelamin' => 'laki-laki',
           'tgl_lahir' => '1990-05-06'
        ]);

        $register->assertStatus(200);

        User::where('email', 'fahmi_onboarding@gmail.com')->update(['status' => 'player', 'is_verified' => 1]);
        $getUser = User::where('email', 'fahmi_onboarding@gmail.com')->first();
        $token = JWTAuth::attempt(['email' => 'fahmi_onboarding@gmail.com', 'password' => 'tes123456']);

        //Check user onboarding before skipping
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);

        $skipOnboarding = $this->put(Route('user.onboarding.status.update'), [
            'action' => 'skipn324jk2h'
        ], ['Authorization' => 'Bearer ' . $token]);

        $skipOnboarding->assertStatus(422);


        //Check user onboarding after skipping with invalid request
        $this->assertDatabaseHas('user_onboarding', [
            'user_id' => $getUser->user_id,
            'preference' => OnboardingEnum::UNDEFINED_PREFERENCE,
            'step' => OnboardingEnum::STARTING_STEP,
            'status' => OnboardingEnum::ONPROGRESS_STATUS
        ]);
    }

}
