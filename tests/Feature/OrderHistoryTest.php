<?php

namespace Tests\Feature;

use App\Model\AllTicketModel;
use App\Model\TIcketPurchaseHistoryModel;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrderHistoryTest extends TestCase
{
    /**
     * A test get ticket purchase history.
     *
     * @return void
     */
    public function testGetOrderHistory()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        Schema::enableForeignKeyConstraints();

        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        $ticket_purchase_history = factory(TIcketPurchaseHistoryModel::class,10)->create([
            'user_id' => '2712abcd',
        ]);

        $response = $this->get('/api/order-history', ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "data" => [
                "*" => [
                    "status",
                    "orderid",
                    "ticket",
                    "qty",
                    "price",
                    "payment_method",
                    "order_date",
                    "payment_date",
                    "expired_date"
                    ]
                ],
            "meta" => [
                "pagination"        => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ]
                ]);
    }

    /**
     * A test get ticket purchase history.
     *
     * @return void
     */
    public function testGetOrderHistoryWithFilter()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        DB::table('ticket_purchase_history')->delete();
        DB::table('all_ticket')->delete();
        Schema::enableForeignKeyConstraints();

        //- Mock Data
        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        $token = JWTAuth::attempt(['email' => 'melly.tiara92@gmail.com', 'password' => 'stream']);

        $ticket_purchase_history = factory(TIcketPurchaseHistoryModel::class,10)->create([
            'user_id' => '2712abcd',
            'created_at' => Carbon::now()->format('Y-m-d h:i:s')
        ]);

        $response = $this->get('/api/order-history?order_date=today', ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "data" => [
                "*" => [
                    "status",
                    "orderid",
                    "ticket",
                    "qty",
                    "price",
                    "payment_method",
                    "order_date",
                    "payment_date",
                    "expired_date"
                    ]
                ],
            "meta" => [
                "pagination"        => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ]
                ]);
        $response->assertJsonCount(10,'data');

        DB::table('ticket_purchase_history')->delete();

        $ticket_purchase_history = factory(TIcketPurchaseHistoryModel::class,10)->create([
            'user_id' => '2712abcd',
            'created_at' => Carbon::now()->subDays(7)->format('Y-m-d h:i:s')
        ]);

        $response = $this->get('/api/order-history?order_date=week', ['Authorization' => 'Bearer ' . $token]);


        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "data" => [
                "*" => [
                    "status",
                    "orderid",
                    "ticket",
                    "qty",
                    "price",
                    "payment_method",
                    "order_date",
                    "payment_date",
                    "expired_date"
                    ]
                ],
            "meta" => [
                "pagination"        => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ]
                ]);
        $response->assertJsonCount(10,'data');


        $response = $this->get('/api/order-history?order_date=today', ['Authorization' => 'Bearer ' . $token]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            "status",
            "data" => [
                "*" => [
                    "status",
                    "orderid",
                    "ticket",
                    "qty",
                    "price",
                    "payment_method",
                    "order_date",
                    "payment_date",
                    "expired_date"
                    ]
                ],
            "meta" => [
                "pagination"        => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ]
                ]);
        $response->assertJsonCount(0,'data');
    }
}
