<?php

namespace Tests\Feature;

use App\Helpers\Firebase\FirebaseFactoryInterface;
use App\Model\PromotionModel;
use App\Model\User;
use App\Model\UserPromotionModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Mockery\MockInterface;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPromotionReadTest extends TestCase
{

    public function testUpdateUserPromotionReadStatusAsAnAuthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true,
        ]);

        //Check if action_read of this user promotion set successfully
        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadStatusAsAnUnauthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = 'fakeToken123u128js';

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(401);
        $promotion->assertJson([
            'status' => false,
        ]);

        //Make sure action_read of this user promotion still not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadStatusWithWrongPromotionId()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,4)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.single', ['promotionId' => 'llsj']),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(404);
        $promotion->assertJson([
            'status' => false,
            'message' => Lang::get('exeptions.model_not_found')
        ]);

        //Make sure action_read of this user promotion still not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadAllStatusAsAnAuthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,2)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.all'),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        //Check if action_read of this user promotion set successfully
        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadAllStatusAsAnUnauthorizedUser()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,2)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = 'fakeToken123u128js';

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.all'),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(401);
        $promotion->assertJson([
            'status' => false
        ]);

        //Make sure action_read of this user promotion still not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadAllStatusIncludeDeletedPromotion()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,3)->create(['status' => 1]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

        //Delete one of user promotion
        $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);

        $promotion = $this->put(Route('promotion.read.all'),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        //Check if action_read of this user promotion set successfully
        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);
    }

    public function testUpdateUserPromotionReadAllStatusIncludeDraftPromotion()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,2)->create(['status' => 1]);
        //draft promotion
        $draft = factory(PromotionModel::class)->create(['status' => 0]);

        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $draft->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $promotion = $this->put(Route('promotion.read.all'),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        //Check if action_read of this user promotion set successfully
        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        //Make sure draft promotion not included as read
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $draft->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }

    public function testUpdateUserPromotionReadAllStatusIncludeDraftAndDeletedPromotion()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PromotionModel::truncate();
        UserPromotionModel::truncate();
        Schema::enableForeignKeyConstraints();

        //Mock firebase service
        $this->mock(FirebaseFactoryInterface::class, function(MockInterface $mock){
            $mock->shouldReceive('updateData')
            ->andReturn('true');
        });

        //create promotions
        $promotions = factory(PromotionModel::class,3)->create(['status' => 1]);
        //draft promotion
        $draft = factory(PromotionModel::class)->create(['status' => 0]);
        //create user
        $user = factory(User::class)->create(['is_active' => 1]);
        //generate user authorization token
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);

        //Make sure action_read of this user promotion not set yet
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $draft->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        //Delete one of user promotion
        $this->delete(Route('promotion.delete.single', ['promotionId' => $promotions[2]->id]),[],['Authorization' => 'Bearer '.$token]);

        $promotion = $this->put(Route('promotion.read.all'),[],['Authorization' => 'Bearer '.$token]);
        $promotion->assertStatus(200);
        $promotion->assertJson([
            'status' => true
        ]);

        //Check if action_read of this user promotion set successfully
        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[0]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[1]->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);

        $this->assertDatabaseHas('user_promotion', [
            'promotion_id' => $promotions[2]->id,
            'user_id' => $user->user_id,
            'action_read' => 0,
            'action_delete' => 1
        ]);

        //Make sure draft promotion not included as read
        $this->assertDatabaseMissing('user_promotion', [
            'promotion_id' => $draft->id,
            'user_id' => $user->user_id,
            'action_read' => 1,
            'action_delete' => 0
        ]);
    }
}
