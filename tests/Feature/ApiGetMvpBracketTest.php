<?php

namespace Tests\Feature;

use App\Model\GameModel;
use App\Model\ListMvpModel;
use App\Model\TournamentModel;
use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class ApiGetMvpBracketTest extends TestCase
{
    /**
     * Test get bracket mvp .
     *
     * @return void
     */
    public function testGetMvpSuccess()
    {
        Schema::disableForeignKeyConstraints();
        TournamentModel::truncate();
        ListMvpModel::truncate();
        GameModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $tournament = factory(TournamentModel::class)->create();

        factory(ListMvpModel::class, 5)->create([
            'tournament_id' => $tournament->id_tournament
        ]);

        $response = $this->get('/api/get-tournament-mvp/'.$tournament->id_tournament);

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'sequence',
                    'player_name',
                    'team_name',
                    'mode',
                    'strike_mvp',
                    'kda_score',
                    'profile_url'
                ]
            ],
            'message'
        ]);

        $response->assertJsonPath('status',true);

        $response->assertJsonCount(5,'data');

        $response->assertStatus(200);
    }

    /**
     * Test get bracket mvp .
     *
     * @return void
     */
    public function testGetMvpFailed()
    {
        Schema::disableForeignKeyConstraints();
        TournamentModel::truncate();
        ListMvpModel::truncate();
        GameModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $response = $this->get('/api/get-tournament-mvp/test');

        $response->assertJsonStructure([
            'status',
            'data' => [
                '*' => [
                    'sequence',
                    'player_name',
                    'team_name',
                    'mode',
                    'strike_mvp',
                    'kda_score',
                    'profile_url'
                ]
            ],
            'message'
        ]);

        $response->assertJsonPath('status',false);

        $response->assertJsonPath('message',Lang::get('message.tournament.not'));

        $response->assertStatus(404);
    }
}
