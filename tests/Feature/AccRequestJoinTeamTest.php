<?php

namespace Tests\Feature;

use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\ProvinceModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Model\User;
use App\Model\UsersGameModel;
use App\Model\UserTicketsModel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class AccRequestJoinTeamTest extends TestCase
{
    // use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testAcceptedSuccess()
    {
        //mock
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();
        //mock account user
        $game = factory(GameModel::class)->create(['player_on_team' => 5, 'substitute_player' => 2]);
        $user = factory(User::class)->create();
        factory(IdentityGameModel::class)->create([
            'users_id'  => $user->user_id,
            'game_id' => $game->id_game_list,
        ]);

        $token = JWTAuth::attempt(['email' => $user->email, 'password' => 'admin']);
        //mock data team
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
            factory(IdentityGameModel::class)->create([
                'users_id'  => $user_->user_id,
                'game_id' => $game->id_game_list
            ]);
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'game_id' => $game->id_game_list])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });

        //mock request team
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id' => TeamModel::first()->team_id,
            'status' => 1,
            'users_id' => factory(IdentityGameModel::class)->create(['game_id' => $game->id_game_list ])->users_id,
        ]);
        // mock job
        Bus::fake();
        //do something
        $response = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token]);


        //validate response
        $response->assertStatus(201);
    }

    public function testAccIfAlreadyinTeam()
    {
        //mock
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();
        //mock account user
        $id_game = factory(IdentityGameModel::class)->create();
        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //mock data team
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });
        //mock request team
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id' => TeamModel::first()->team_id,
            'users_id'  => $users->first()->user_id
        ]);

        // mock job
        Bus::fake();
        //do something
        $response = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(400);
    }

    public function testAccIfStatusRequestAcceptedBefore()
    {
        //mock
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();
        //mock account user
        $id_game = factory(IdentityGameModel::class)->create();
        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //mock data team
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });
        //mock request team
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id'   => TeamModel::first()->team_id,
            'status'    => 2
        ]);

        // mock job
        Bus::fake();
        //do something
        $response = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(400);
    }


    public function testAccIfStatusRequestRejectedBefore()
    {
        //mock
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();
        //mock account user
        $id_game = factory(IdentityGameModel::class)->create();
        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //mock data team
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });
        //mock request team
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id'   => TeamModel::first()->team_id,
            'status'    => 0
        ]);

        // mock job
        Bus::fake();
        //do something
        $response = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(400);
    }

    public function testAccIfStatusTeamisBanned()
    {
        //mock
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        CityModel::truncate();
        ProvinceModel::truncate();
        Schema::enableForeignKeyConstraints();
        //mock account user
        $id_game = factory(IdentityGameModel::class)->create();
        $user = User::first();
        $token = JWTAuth::claims(['boy' => Hash::make(request()->ip())])->attempt(['email' => $user->email, 'password' => 'admin']);
        //mock data team
        $users = factory(User::class, 4)->create();
        $team_player = '';
        foreach ($users as $user_) {
            $team_player .= $user_->user_id . ',';
        }
        $team = factory(TeamModel::class)->create(['leader_id' => $user->user_id, 'is_banned' => 1])->each(function ($team) use ($user, $team_player) {
            $team->player()->save(factory(TeamPlayerModel::class)->create(['player_id' => $user->user_id . ',' . trim($team_player, ',')]));
        });
        //mock request team
        $team_request = factory(TeamRequestModel::class)->create([
            'team_id'   => TeamModel::first()->team_id,
            'status'    => 0
        ]);

        // mock job
        Bus::fake();
        //do something
        $response = $this->post('/api/team/request/accepted/' . $team_request->request_id, [], ['Authorization' => 'Bearer ' . $token]);

        //validate response
        $response->assertStatus(400);
    }
}
