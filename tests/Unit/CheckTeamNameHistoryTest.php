<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\TeamModel;
use App\Model\TeamNameHistoryModel;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class CheckTeamNameHistoryTest extends TestCase
{

    /**
     * Test : still in the last 30 days
     *
     * @return void
     */
    public function testLastOneMonth()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();
        $team = factory(TeamModel::class)->create();

        $data = factory(TeamNameHistoryModel::class)->create([
            'team_id' => $team->team_id,
            'old_name' => $team->team_name,
            'new_name' => 'test',
            'created_at' => Carbon::now()->subDays(3)
        ]);

        $teamdata = TeamModel::where('team_id',$team->team_id)->first();

        $check = (new TeamHelperController)->isAllow($teamdata);

        $this->assertEquals(true, (bool) $check);

    }

    /**
     * Test : still in the last > 30 days
     *
     * @return void
     */
    public function testLastGreaterOneMonth()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        $data = factory(TeamNameHistoryModel::class)->create([
            'team_id' => $team->team_id,
            'old_name' => $team->team_name,
            'new_name' => 'test',
            'created_at' => Carbon::now()->subDays(31)
        ]);

        $teamdata = TeamModel::where('team_id',$team->team_id)->first();

        $check = (new TeamHelperController)->isAllow($teamdata);

        $this->assertEquals(false, (bool) $check);

    }

    /**
     * Test : today
     *
     * @return void
     */
    public function testToday()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        $data = factory(TeamNameHistoryModel::class)->create([
            'team_id' => $team->team_id,
            'old_name' => $team->team_name,
            'new_name' => 'test',
            'created_at' => Carbon::now()
        ]);

        $teamdata = TeamModel::where('team_id',$team->team_id)->first();

        $check = (new TeamHelperController)->isAllow($teamdata);

        $this->assertEquals(true, (bool) $check);

    }

    /**
     * Test : tomorrow
     *
     * @return void
     */
    public function testTomorrow()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        TeamNameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        $data = factory(TeamNameHistoryModel::class)->create([
            'team_id' => $team->team_id,
            'old_name' => $team->team_name,
            'new_name' => 'test',
            'created_at' => Carbon::now()->addDay()
        ]);

        $teamdata = TeamModel::where('team_id',$team->team_id)->first();

        $check = (new TeamHelperController)->isAllow($teamdata);

        $this->assertEquals(true, (bool) $check);

    }
}
