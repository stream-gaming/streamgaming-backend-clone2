<?php

namespace Tests\Unit;

use App\Helpers\NotifCounterFirebase;
use Tests\TestCase;

class NotifCounterFirebaseTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserIdValidation()
    {
        $notif = new NotifCounterFirebase;

        $user_id = "3423nlj32";

        $check = $notif->upData($user_id);

        $this->assertArrayHasKey("counter",$check);

        $user_id = "3423n#lj32";

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

        $user_id = "3423n.lj32";

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

        $user_id = "3423n[lj32";

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

        $user_id = "3423n]lj32";

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

        $user_id = "3423n[lj32";

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

        $user_id = '3423n$lj32';

        $check = $notif->upData($user_id);

        $this->assertEquals(false,$check);

    }
}
