<?php

namespace Tests\Unit;

use App\Model\GameModel;
use App\Model\InvitationCode;
use App\Model\TeamModel;
use App\Model\User;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

use function PHPUnit\Framework\assertTrue;

class TeamModelDeleteTeamTest extends TestCase
{
    /**
     * Delete Team Should Be Delete Invitation Code.
     *
     * @return void
     */
    public function testDeleteTeamShouldBeDeleteInvitationCode()
    {
        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        GameModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $team = factory(TeamModel::class)->create();

        factory(InvitationCode::class)->create([
            'team_id' => $team->team_id,
        ]);

        TeamModel::destroy($team->team_id);

        $this->assertDatabaseMissing('team', ['team_id' => $team->team_id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('invitation_codes', ['team_id' => $team->team_id]);

        Schema::disableForeignKeyConstraints();
        TeamModel::truncate();
        InvitationCode::truncate();
        GameModel::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
