<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use App\Model\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TeamHelperControllerTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test has Team v2 method.
     *
     * @return void
     */
    public function testHasTeamV2()
    {
        $game_id = factory(GameModel::class)->create([
            'player_on_team' => 5
        ])->id_game_list;

        $user = factory(User::class)->create([
            'user_id'   => '2712abcd',
            'fullname'  => 'Melli Tiara',
            'username'  => 'mdcxxvii',
            'is_verified' => '1',
            'status'    => 'player',
            'provinsi'  => '51',
            'kota'      => '5108',
            'jenis_kelamin' => "perempuan",
            'email'     => 'melly.tiara92@gmail.com',
            'password'  => bcrypt('stream')
        ]);

        factory(IdentityGameModel::class)->create([
            'game_id' => $game_id,
            'users_id' => $user->user_id
        ]);

        $team = factory(TeamModel::class)->create();
        $city = factory(CityModel::class)->create();

        $teamPlayer = factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id,
            'player_id' => $user->user_id,
        ]);

        $this->assertTrue((new TeamHelperController)->hasTeamV2($team->game_id,$user->user_id));

        $this->assertFalse((new TeamHelperController)->hasTeamV2('safsdf',$user->user_id));

        $this->assertTrue(true);
    }
}
