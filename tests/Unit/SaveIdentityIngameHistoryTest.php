<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\GameController;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\IdentityGameModel;
use App\Model\IdentityIngameHistoryModel;
use App\Model\ProvinceModel;
use App\Model\User;
use Illuminate\Support\Facades\Schema;
use PHPUnit\Framework\TestCase;
use Prophecy\Call\Call;
use Tests\TestCase as TestsTestCase;

class SaveIdentityIngameHistoryTest extends TestsTestCase
{
    /**
     * Test Save Identity In Game History.
     *
     * @return void
     */
    public function testSaveData()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        IdentityGameModel::truncate();
        GameModel::truncate();
        IdentityIngameHistoryModel::truncate();
        Schema::enableForeignKeyConstraints();

        factory(IdentityGameModel::class)->create([
            'id_ingame' => 'idingamebefore',
            'username_ingame' => 'usernameingamebefore'
        ]);

        $data = IdentityGameModel::first();

        $identity_before = [
            'id_ingame' => $data->id_ingame,
            'username_ingame' => $data->username_ingame
        ];

        $identity_after = [
            'id_ingame' => 'id_ingameafter',
            'username_ingame' => 'usernameingamebefore'
        ];

        (new GameController)->saveHistory($identity_before, $identity_after, $data->game_id, $data->users_id);

        $check = IdentityIngameHistoryModel::first();

        $this->assertEquals($data->users_id, $check->user_id);
        $this->assertEquals($data->game_id, $check->game_id);
        $this->assertEquals((object) $identity_before,json_decode($check->before));
        $this->assertEquals((object) $identity_after,json_decode($check->after));
    }
}
