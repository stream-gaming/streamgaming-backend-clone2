<?php

namespace Tests\Unit;

use Carbon\Carbon;
use App\Model\User;
use Tests\TestCase;
use App\Model\CityModel;
use App\Model\GameModel;
use App\Model\TeamModel;
use App\Model\ProvinceModel;
use App\Model\UsersGameModel;
use App\Model\TeamPlayerModel;
use App\Model\TeamRequestModel;
use App\Model\IdentityGameModel;
use App\Model\HistoryBannedModel;
use App\Helpers\UpdateStatusBanned;
use App\Jobs\CheckStatusSoloBanned;
use Illuminate\Support\Facades\Bus;
use App\Helpers\Validation\CheckBanned;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CheckSoloBannedTest extends TestCase
{
    /**
     *
     * Testing Banned Game Account with not as member team
     *
     */

    public function testCheckBannedForIdentityWithNotAsMemberTeam()
    {
        /**
         * Set waktu ban berakhir pada 2 menit lagi
         * Seharusnya pengecekannya menghasilkan True untuk banned
         *
         * Expected : True
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create(['is_banned' => 1]);

        $checkBanned = (new CheckBanned)->isSoloBanned($identity->game_id, null, $identity->users_id, false);

        $this->assertTrue($checkBanned);
    }

    public function testCheckNotBannedForIdentityWithNotAsMemberTeam()
    {
        /**
         *
         * Expected : False
         * Seharusnya pengecekannya menghasilkan False untuk banned
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create();

        $checkBanned = (new CheckBanned)->isSoloBanned($identity->game_id, null, $identity->users_id, false);

        $this->assertFalse($checkBanned);
    }








    /**
     *
     * Testing Banned Game Account with already as member in team
     *
     */

    public function testCheckBannedForIdentityOnePlayerOfTeamAndBannedTimeUIs2MinutesAgain()
    {
        /**
         * Set waktu ban berakhir pada 2 menit lagi
         * Seharusnya pengecekannya menghasilkan True untuk banned
         *
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create();
        $team = factory(TeamModel::class)->create(['leader_id' => $identity->users_id]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => $team->team_id, 'ban_until' => Carbon::now()->addMinutes(2), 'is_banned' => 1]);

        $checkBanned = (new CheckBanned)->isSoloBanned($team->game_id, $team->team_id, null, false);

        $this->assertTrue($checkBanned);
    }

    public function testCheckBannedForIdentityOnePlayerOfTeamAndBannedTimeIs2MinutesAgo()
    {
        /**
         * Set waktu ban berakhir pada 2 menit yang lalu
         * Seharusnya pengecekannya menghasilkan True untuk banned
         *
         *
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create();
        $team = factory(TeamModel::class)->create(['leader_id' => $identity->users_id]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => $team->team_id, 'ban_until' => Carbon::now()->subMinutes(2), 'is_banned' => 1]);

        $checkBanned = (new CheckBanned)->isSoloBanned($team->game_id, $team->team_id, null, false);

        $this->assertTrue($checkBanned);
    }



    /**
     * Testing Job of UnBannedSolo
     */

    // Ban Account As Already Member in Team
    public function testCheckJobBannedForIdentityWithtAsMemberTeamWhenTimeBannedIs2MinutesAgain()
    {
        /**
         *
         * Set waktu ban berakhir pada 2 menit lagi
         * Seharusnya pengecekannya menghasilkan True untuk banned
         * Dan status banned tidak berubah karena belum saatnya di unbanned
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create(['is_banned' => 1, 'ban_until' => Carbon::now()->addMinutes(2)]);
        $team = factory(TeamModel::class)->create(['leader_id' => $identity->users_id]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => $team->team_id, 'ban_until' => $identity->ban_until, 'is_banned' => $identity->is_banned]);

        //run schedule
        Bus::fake();
        CheckStatusSoloBanned::dispatch();
        (new UpdateStatusBanned)->solo();

        $checkBanned = (new CheckBanned)->isSoloBanned($team->game_id, $team->team_id, null, false);

        $this->assertTrue($checkBanned);
        $this->assertDatabaseHas('identity_ingame', [
            'id'    => $identity->id,
            'users_id'  => $identity->users_id,
            'is_banned' => $identity->is_banned,
            'ban_until' => $identity->ban_until
        ]);
        $this->assertDatabaseHas('history_banned', [
            'identity_id'    => $historyBanned->identity_id,
            'team_id'   => $historyBanned->team_id,
            'is_banned' => $historyBanned->is_banned,
            'ban_until' => $historyBanned->ban_until
        ]);
    }

    public function testCheckJobBannedForIdentityWithtAsMemberTeamWhenTimeBannedIs2MinutesAgo()
    {
        /**
         *
         * Set waktu ban berakhir pada 2 menit lalu
         * Seharusnya pengecekannya menghasilkan False untuk banned
         * Karena sudah diunbanned oleh helper di job
         *
         * Note:
         * - Pastikan job unbanned jalan
         * - Unbanned dengan helper
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create(['is_banned' => 1, 'ban_until' => Carbon::now()->subMinutes(2)]);
        $team = factory(TeamModel::class)->create(['leader_id' => $identity->users_id]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => $team->team_id, 'ban_until' => $identity->ban_until, 'is_banned' => $identity->is_banned]);

        //run schedule
        Bus::fake();
        CheckStatusSoloBanned::dispatch();
        (new UpdateStatusBanned)->solo();

        $checkBanned = (new CheckBanned)->isSoloBanned($team->game_id, $team->team_id, null, false);

        $this->assertFalse($checkBanned);
        $this->assertDatabaseHas('identity_ingame', [
            'id'    => $identity->id,
            'users_id'  => $identity->users_id,
            'is_banned' => 0,
            'ban_until' => NULL
        ]);
        $this->assertDatabaseHas('history_banned', [
            'identity_id'    => $historyBanned->identity_id,
            'team_id'   => $historyBanned->team_id,
            'is_banned' => 0,
            'ban_until' => $historyBanned->ban_until
        ]);
    }

    // Ban Account As Not Member in Team
    public function testCheckJobBannedForIdentityWhenHaveNotTeamAndBannedTimeIs2MinutesAgain()
    {
        /**
         *
         * Set waktu ban berakhir pada 2 menit lagi.
         * Seharusnya pengecekannya menghasilkan True untuk banned
         * Status banned tidak berubah walaupun dijalankan helper untuk update unbanned
         * karena belum waktunya untuk diunbanned
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create(['is_banned' => 1, 'ban_until' => Carbon::now()->addMinutes(2)]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => null, 'ban_until' => $identity->ban_until, 'is_banned' => $identity->is_banned]);

        //run schedule
        Bus::fake();
        CheckStatusSoloBanned::dispatch();
        (new UpdateStatusBanned)->solo();

        $checkBanned = (new CheckBanned)->isSoloBanned($identity->game_id, null, $identity->users_id, false);

        $this->assertTrue($checkBanned);
        $this->assertDatabaseHas('identity_ingame', [
            'id'    => $identity->id,
            'users_id'  => $identity->users_id,
            'is_banned' => $identity->is_banned,
            'ban_until' => $identity->ban_until
        ]);
        $this->assertDatabaseHas('history_banned', [
            'identity_id'    => $historyBanned->identity_id,
            'team_id'   => $historyBanned->team_id,
            'is_banned' => $historyBanned->is_banned,
            'ban_until' => $historyBanned->ban_until
        ]);
    }

    public function testCheckJobBannedForIdentityWhenHaveNotTeamAndBannedTimeIs2MinutesAgo()
    {
        /**
         *
         * Set waktu ban berakhir pada 2 menit lalu
         * Seharusnya pengecekannya menghasilkan False untuk banned
         * karena sudah jadwalnya dibanned
         *
         */

        // truncate database
        Schema::disableForeignKeyConstraints();
        GameModel::truncate();
        User::truncate();
        IdentityGameModel::truncate();
        TeamModel::truncate();
        TeamPlayerModel::truncate();
        TeamRequestModel::truncate();
        UsersGameModel::truncate();
        ProvinceModel::truncate();
        CityModel::truncate();
        Schema::enableForeignKeyConstraints();

        $identity = factory(IdentityGameModel::class)->create(['is_banned' => 1, 'ban_until' => Carbon::now()->subMinutes(2)]);
        $historyBanned = factory(HistoryBannedModel::class)->create(['identity_id' => $identity->id, 'team_id' => null, 'ban_until' => $identity->ban_until, 'is_banned' => $identity->is_banned]);

        //run schedule
        Bus::fake();
        CheckStatusSoloBanned::dispatch();
        (new UpdateStatusBanned)->solo();

        $checkBanned = (new CheckBanned)->isSoloBanned($identity->game_id, null, $identity->users_id, false);

        $this->assertFalse($checkBanned);
        $this->assertDatabaseHas('identity_ingame', [
            'id'    => $identity->id,
            'users_id'  => $identity->users_id,
            'is_banned' => 0,
            'ban_until' => NULL
        ]);
        $this->assertDatabaseHas('history_banned', [
            'identity_id'    => $historyBanned->identity_id,
            'team_id'   => $historyBanned->team_id,
            'is_banned' => 0,
            'ban_until' => $historyBanned->ban_until
        ]);
    }
}
