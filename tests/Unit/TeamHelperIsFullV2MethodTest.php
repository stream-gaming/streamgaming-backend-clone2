<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\Team\TeamHelperController;
use App\Model\GameModel;
use App\Model\TeamModel;
use App\Model\TeamPlayerNewModel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TeamHelperIsFullV2MethodTest extends TestCase
{
    // use DatabaseTransactions;
    /**
     * Test team helper, isFullv2 method.
     *
     * @return void
     */
    public function testIsFullv2()
    {
        $game = factory(GameModel::class)->create([
            'player_on_team' => 5,
            'substitute_player' => 3,
        ]);

        $team = factory(TeamModel::class)->create([
            'game_id' => $game->id_game_list
        ]);

        factory(TeamPlayerNewModel::class)->create([
            'team_id' => $team->team_id
        ]);

        $this->assertFalse((new TeamHelperController)->isFullv2(TeamModel::find($team->team_id)));

        factory(TeamPlayerNewModel::class,7)->create([
            'team_id' => $team->team_id
        ]);

        $this->assertTrue((new TeamHelperController)->isFullv2(TeamModel::find($team->team_id)));


        $this->assertTrue(true);
    }
}
