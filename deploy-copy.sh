#printenv HOME
# activate maintenance mode
#echo $USER;
#echo $HOME;
export COMPOSER_HOME="/home/ubuntu/.config/composer";
#echo $COMPOSER_HOME;
php artisan down
# update source code
ssh-agent bash -c 'ssh-add /var/www/.ssh/stream-gaming-backend-development; git pull origin development'
# update PHP dependencies
composer install --no-interaction --no-dev --prefer-dist
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.
# update database
php artisan migrate --force
# --force  Required to run when in production.
# stop maintenance mode
php artisan up
php artisan scribe:generate
