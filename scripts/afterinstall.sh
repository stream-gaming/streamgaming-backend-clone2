#!/bin/bash

chown -R www-data:www-data /var/www/stream-gaming-backend/bootstrap
chown -R www-data:www-data /var/www/stream-gaming-backend/storage
cd  /var/www/stream-gaming-backend/ && sudo php artisan cache:clear
cd  /var/www/stream-gaming-backend/ && sudo php artisan view:clear
cd  /var/www/stream-gaming-backend/ && sudo php artisan config:cache
cd  /var/www/stream-gaming-backend/ && sudo php artisan migrate --force
cd  /var/www/stream-gaming-backend/ && sudo -H -u www-data composer install --optimize-autoloader
cd  /var/www/stream-gaming-backend/ && sudo -H -u www-data php artisan optimize
cd  /var/www/stream-gaming-backend/ && sudo -H -u www-data php artisan route:clear

cd /var/www/stream-gaming-backend/ && php artisan up
sudo service supervisor restart
sleep 10
sudo supervisorctl restart streamgaming-queue-worker:*
sudo supervisorctl restart streamgaming-websocket-worker:*

service php7.4-fpm restart
service nginx restart
